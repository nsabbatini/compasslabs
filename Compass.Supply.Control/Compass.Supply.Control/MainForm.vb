﻿Imports Compass.Global
Public Class MainForm
    Private Sub btnReSupply_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnReSupply.ItemClick
        Using frm As New frmMain
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        If Not LoggedinUserName = "RUFUS.ANDERSON" AndAlso Not LoggedinUserName = "L.MCLAUGHLIN" AndAlso Not LoggedinUserName = "NED.SABBATINI" AndAlso Not LoggedinUserName = "REMOND.YOUNG" Then
            MsgBox("You do not have access to this module!", MsgBoxStyle.Information, "Access Denied")
            Exit Sub
        End If
        Using frm As New frmCreateBarCodes

            frm.ShowDialog()
        End Using
    End Sub
    Private Sub btnSignOut_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSignOut.ItemClick
        LoggedinUserName = String.Empty
        frmLogin.Show()
        Me.Close()

    End Sub

    Private Sub MainForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Not Me Is Nothing Then
            End
        End If
    End Sub
End Class