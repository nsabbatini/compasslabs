﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Report.Library
Imports GrapeCity.ActiveReports
Imports System.Drawing.Printing

Public Class frmCreateBarCodes
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private ReadOnly _fileDir As String = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dtItems As DataTable
    Private _dtItems2 As DataTable
    Private _selectedItem As String
    Private _editValue As Int16 = 0
    Private Sub frmCreateBarCodes_Load(sender As Object, e As EventArgs) Handles Me.Load
        chkLotNum.Checked = False
        LoadForm()
        _dtItems = New DataTable
        _dtItems.TableName = "Items"

        _dtItems2 = New DataTable
        _dtItems2.TableName = "Items"

        _dtItems = CreateTable()
        _dtItems2 = CreateTable2()
        GridView1.OptionsView.ShowGroupPanel = False
        GridView1.OptionsView.ShowIndicator = False

        GridView2.OptionsView.ShowGroupPanel = False
        GridView2.OptionsView.ShowIndicator = False

    End Sub
    Private Sub LoadForm()
        Dim results = GetInventoryDb()

        lookupItems.Properties.DataSource = results
        lookupItems.Properties.DisplayMember = "ItemDesc"
        lookupItems.Properties.ValueMember = "Id"

        If Not chkLotNum.Checked Then
            grdLotNums.Visible = False
            grdItems.Visible = True
        Else
            grdLotNums.Visible = True
            grdItems.Visible = False
        End If

    End Sub
    Private Function GetInventoryDb() As Object

        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & InventorySqlItems
                getInventory._sqlText = CType(ReadSqlFile(_fileName), String)
                Return getInventory.GetInventory.Where(Function(x) x.NeedsLotNum = chkLotNum.Checked).ToList

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmCreateBarCodes.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Function

    'Private Sub btnAddtoGrid_Click(sender As Object, e As EventArgs) Handles btnAddtoGrid.Click
    '    LoadGrid()
    'End Sub
    Private Sub LoadGrid()
        Dim arow As DataRow
        Dim theId As Int16

        With _dtItems
            theId = CShort(lookupItems.EditValue)
            If Not CheckIfAlreadyInGrid(theId) Then
                arow = .NewRow
                arow(0) = theId
                arow(1) = _selectedItem
                arow(2) = 1
                .Rows.Add(arow)
            End If

        End With
        grdItems.DataSource = _dtItems.DefaultView
        lookupItems.Focus()

    End Sub
    Private Sub LoadGrid2()
        Dim arow As DataRow
        Dim theId As Int16

        With _dtItems2
            theId = CShort(lookupItems.EditValue)
            If Not CheckIfAlreadyInGrid(theId) Then
                arow = .NewRow
                arow(0) = theId
                arow(1) = _selectedItem
                arow(2) = 1
                arow(3) = DBNull.Value
                .Rows.Add(arow)
            End If

        End With
        grdLotNums.DataSource = _dtItems2.DefaultView
        lookupItems.Focus()

    End Sub
    Private Function CheckIfAlreadyInGrid(ByVal theid As Int16) As Boolean

        For i = 0 To GridView1.DataRowCount - 1
            If theid = CType(GridView1.GetRowCellValue(i, "Id"), Int16) Then
                Return True
            End If
        Next

    End Function
    Private Function CheckIfAlreadyInGrid2(ByVal theid As Int16) As Boolean

        For i = 0 To GridView2.DataRowCount - 1
            If theid = CType(GridView2.GetRowCellValue(i, "Id"), Int16) Then
                Return True
            End If
        Next

    End Function

    Private Function CreateTable() As DataTable

        Dim _dtItems As DataTable = New DataTable

        With _dtItems.Columns

            .Add("Id", GetType(Int16))
            .Add("ItemDesc", GetType(String))
            .Add("Qty", GetType(Int16))

        End With

        Return _dtItems

    End Function
    Private Function CreateTable2() As DataTable

        Dim _dtItems As DataTable = New DataTable

        With _dtItems.Columns

            .Add("Id", GetType(Int16))
            .Add("ItemDesc", GetType(String))
            .Add("Qty", GetType(Int16))
            .Add("ExpDate", GetType(Date))

        End With

        Return _dtItems

    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Try
            Dim rpt As New rptPOBarCodes
            Dim sectionDocument = rpt.Document
            sectionDocument.Printer.PrinterSettings.PrinterName = "ZDesigner ZD410-203dpi ZPL"
            If Not chkLotNum.Checked Then
                For i = 0 To GridView1.DataRowCount - 1

                    For x = 1 To CType(GridView1.GetRowCellValue(i, "Qty"), Int16)
                        Dim barcode As String = CType(GridView1.GetRowCellValue(i, "Id"), String) & "," & DateTime.Now.ToString("MM/dd/yyyy")
                        rpt._barcode = barcode
                        rpt._description = CType(GridView1.GetRowCellValue(i, "ItemDesc"), String)
                        rpt.Run()

                        sectionDocument.Print(False, False, False)

                    Next
                Next
                _dtItems.Clear()
            Else
                For i = 0 To GridView2.DataRowCount - 1

                    For x = 1 To CType(GridView2.GetRowCellValue(i, "Qty"), Int16)
                        Dim itemDate As String = CType(GridView2.GetRowCellValue(i, "ExpDate"), Date).ToString("MM/dd/yyyy")

                        Dim barcode As String = CType(GridView2.GetRowCellValue(i, "Id"), String) & "," &
                            itemDate
                        rpt._barcode = barcode
                        rpt._description = CType(GridView2.GetRowCellValue(i, "ItemDesc"), String)
                        rpt.Run()

                        sectionDocument.Print(False, False, False)

                    Next
                Next
                _dtItems2.Clear()
            End If

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmCreateBarCodes.btnSave_Click", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try



    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub chkLotNum_CheckedChanged(sender As Object, e As EventArgs) Handles chkLotNum.CheckedChanged
        LoadForm()

    End Sub

    Private Sub lookupItems_EditValueChanged(sender As Object, e As EventArgs) Handles lookupItems.EditValueChanged
        Dim arow = lookupItems.GetSelectedDataRow()

        _selectedItem = CType((arow.GetType().GetProperty("ItemDesc")?.GetValue(arow)), String)
        If Not chkLotNum.Checked Then
            LoadGrid()
        Else
            LoadGrid2()
        End If
    End Sub

    Private Sub RepositoryItemButtonEdit1_Click(sender As Object, e As EventArgs) Handles RepositoryItemButtonEdit1.Click
        GridView1.DeleteSelectedRows()
    End Sub

    Private Sub RepositoryItemButtonEdit2_Click(sender As Object, e As EventArgs) Handles RepositoryItemButtonEdit2.Click
        GridView2.DeleteSelectedRows()
    End Sub
End Class