﻿Imports Compass.Global
Public Class frmLogin
    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click

        Using validate As New AuthHelper() With {._userName = username.Text.Trim(), ._password = password.Text.Trim()}
            If validate.CheckUserCreds Then
                LoggedinUserName = username.Text.Trim().ToUpper
                username.Text = String.Empty
                password.Text = String.Empty
                username.Focus()

                Using frm As New MainForm
                    Me.Hide()
                    frm.ShowDialog()
                End Using

            Else
                MessageBox.Show(Me, "Invalid User Name or Password, Try Again!", "Invalid Combinations", MessageBoxButtons.OK, MessageBoxIcon.Error)
                username.Text = String.Empty
                password.Text = String.Empty
            End If
        End Using


    End Sub

    Private Sub password_KeyDown(sender As Object, e As KeyEventArgs) Handles password.KeyDown
        If e.KeyCode = Keys.Enter Then
            SimpleButton1.PerformClick()
        End If
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        End
    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles Me.Load
        username.Text = String.Empty
        password.Text = String.Empty
        username.Focus()

    End Sub
End Class