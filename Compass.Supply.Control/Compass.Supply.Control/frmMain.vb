﻿Imports System.ComponentModel
Imports System.Text
Imports Compass.MySql.Database
Imports Compass.Global
Imports System.Collections

Partial Public Class frmMain
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private ReadOnly _fileDir As String = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _itemId As Int16
    Private _departmentId As Int16 = 0
    Private _dtItems As DataTable
    Private _expDate As Date

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtBarcode.Focus()

        _dtItems = New DataTable
        _dtItems.TableName = "Items"

        _dtItems = CreateTable()

        Dim departments = GetDepartments()

        lookupDepartments.Properties.DataSource = departments
        lookupDepartments.Properties.DisplayMember = "DepartmentName"
        lookupDepartments.Properties.ValueMember = "DepartmentId"
        lookupDepartments.EditValue = _departmentId

        Dim codes = GetReasonCodes()
        lookupReason.Properties.DataSource = codes
        lookupReason.Properties.DisplayMember = "ReasonName"
        lookupReason.Properties.ValueMember = "ReasonId"
        lookupReason.EditValue = _departmentId

        GridView1.OptionsView.ShowGroupPanel = False
        GridView1.OptionsView.ShowIndicator = False

    End Sub
    Private Function GetDepartments() As Object
        Try

            Using getDepartmentinfo As New Departments()
                _fileName = _fileDir & GetDepartmentsSql
                getDepartmentinfo._sqlText = CType(ReadSqlFile(_fileName), String)
                Return getDepartmentinfo.GetDepartmentInfo.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMain.GetDepartments", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Sub txtBarcode_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBarcode.KeyDown
        Dim s As String = String.Empty
        Dim s1() As String

        If e.KeyCode = Keys.Enter Then
            s = txtBarcode.Text
            s1 = s.Split(","c)
            _itemId = CShort(s1(0))
            If s1.Length >= 2 Then
                _expDate = CDate(s1(1))
                If _expDate < CDate(DateTime.Now.ToString("MM/dd/yyyy")) Then
                    MessageBox.Show(Me, "This item expired on " & _expDate.ToString("MM/dd/yyyy") & "!", "Expired Item", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If

            LoadGrid()
            txtBarcode.Text = String.Empty

        End If
    End Sub
    Private Sub LoadGrid()
        Dim arow As DataRow
        Dim theId As Int16
        Dim theCount As Int16 = 1
        Dim results = GetInventoryDb(_itemId)

        With _dtItems
            theId = CShort(results(0).GetType().GetProperty("Id")?.GetValue(results(0)))
            If Not CheckIfAlreadyInGrid(theId) Then
                arow = .NewRow
                arow(0) = theId
                arow(1) = results(0).GetType().GetProperty("ItemDesc")?.GetValue(results(0))
                arow(2) = 1
                .Rows.Add(arow)
            End If

        End With
        grdItems.DataSource = _dtItems.DefaultView


    End Sub
    Private Function CheckIfAlreadyInGrid(ByVal theid As Int16) As Boolean

        For i = 0 To GridView1.DataRowCount - 1
            If theid = CType(GridView1.GetRowCellValue(i, "Id"), Int16) Then
                Return True
            End If
        Next

    End Function
    Private Function CreateTable() As DataTable

        Dim _dtItems As DataTable = New DataTable

        With _dtItems.Columns

            .Add("Id", GetType(Int16))
            .Add("ItemDesc", GetType(String))
            .Add("Qty", GetType(Int16))

        End With

        Return _dtItems

    End Function
    Private Function GetInventoryDb(ByVal itemId As Int16) As Object

        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & InventorySqlItems
                getInventory._sqlText = CType(ReadSqlFile(_fileName), String)
                Return getInventory.GetInventory.Where(Function(x) x.Id = itemId).ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMain.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Function
    Private Function GetReasonCodes() As Object

        Try

            Using getReasons As New ReasonCodes()

                _fileName = _fileDir & GetReasonsCodeSql
                getReasons._sqlText = CType(ReadSqlFile(_fileName), String)
                Return getReasons.GetReasonCodesInfo.ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMain.GetReasonCodes", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Function
    Private Function GetMaxTrackingId() As Int16

        Try

            Using tracking As New GetTrackingInformation()

                _fileName = _fileDir & GetTrackingSql
                tracking._sqlText = CType(ReadSqlFile(_fileName), String)
                Return tracking.GetTracking.Max(Function(x) x.TrackingId)

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMain.GetMaxTrackingId", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Function
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If lookupDepartments.EditValue = 0 Then
            MsgBox("Must select a department!", MsgBoxStyle.Exclamation, "Department Required")
            Exit Sub
        ElseIf lookupReason.EditValue = 0 Then
            MsgBox("Must select a reason!", MsgBoxStyle.Exclamation, "Department Required")
            Exit Sub
        ElseIf GridView1.DataRowCount = 0 Then
            MsgBox("You haven't added anything to save!", MsgBoxStyle.Exclamation, "Items Required")
            Exit Sub
        End If
        If Not ReduceTheInventory() Then
            MessageBox.Show(Me, "Something went wrong while saving the items, an error has been recorded and the IT team notified!", "Inventory", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else
            _dtItems.Clear()
            lookupReason.EditValue = _departmentId
            lookupDepartments.EditValue = _departmentId
            MessageBox.Show(Me, "All items have been successfully recorded!", "Reduce Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub
    Private Function TrackSupplies() As Boolean
        Dim arr As New ArrayList
        Try
            _fileName = _fileDir & SetTrackingInformationSql
            Using tracking As New InventoryControl() With {
                ._departmentId = CShort(lookupDepartments.EditValue),
                ._reasonId = CShort(lookupReason.EditValue),
                ._userName = LoggedinUserName,
                ._sqlText = CStr(ReadSqlFile(_fileName))
                }
                tracking.TrackingInformation()

            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMain.TrackSupplies", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

        Try
            Dim maxid = GetMaxTrackingId()

            _fileName = _fileDir & SetTrackingInformationDetailsSql

            Using details As New InventoryControl() With {
                ._sqlText = CStr(ReadSqlFile(_fileName))
                }

                For i = 0 To GridView1.DataRowCount - 1

                    arr.Add(CType(GridView1.GetRowCellValue(i, "Id"), Int16))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "Qty"), Int16))
                    arr.Add(maxid)

                    details.TrackingInformationDetails(arr)

                    arr.Clear()
                Next

            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMain.TrackSupplies", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try


    End Function
    Private Function ReduceTheInventory() As Boolean

        _fileName = _fileDir & SetItemsCurrentQtySql
        Dim x As Int16
        'reduce the onhand inventory
        Try
            Using items As New InventoryControl() With {
                ._sqlText = CType(ReadSqlFile(_fileName), String)
            }
                For i = 0 To GridView1.DataRowCount - 1
                    items._itemId = CType(GridView1.GetRowCellValue(i, "Id"), Int16)
                    items._qty = CType(GridView1.GetRowCellValue(i, "Qty"), Int16)
                    x = items.ReduceInventory
                Next
                If Not x = GridView1.DataRowCount Then
                    Return False
                Else
                    TrackSupplies()
                End If
            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMain.ReduceTheInventory", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try


        Return True


    End Function

    Private Sub lookupDepartments_EditValueChanged(sender As Object, e As EventArgs) Handles lookupDepartments.EditValueChanged
        txtBarcode.Focus()
    End Sub

    Private Sub lookupReason_EditValueChanged(sender As Object, e As EventArgs) Handles lookupReason.EditValueChanged
        txtBarcode.Focus()
    End Sub
End Class
