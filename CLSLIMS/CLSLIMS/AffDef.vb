﻿
Imports System.Data.SqlClient
Imports System
Imports System.Data

Imports System.Drawing.Printing

Public Class AffDef
    Public Shared strcon As String = "Server=10.65.1.12;Database=TNCL_PROD;User Id=sa;Password=C0mp@SS**;MultipleActiveResultSets=true"

    Dim sql As String = ""
    Dim rownumber As Integer
    Dim lbreader As SqlDataReader
    Dim vdescription As New ArrayList()
    Public Property Specnum As Integer
    Dim NeedtoSaveNotes As Boolean = False
    Dim selectedItemValue As String
    Dim numofline As Integer
    Dim NewItem As Boolean = False
    Private Sub DetailNotes_Load(sender As Object, e As EventArgs) Handles Me.Load
        cblist.Items.Clear()
        lblNew.Text = ""
        NeedtoSaveNotes = False
        Me.Refresh()

        Try
            Using Conn As New SqlConnection(strcon)
                sql = "Select distinct TemplateName From XAffTemplate "
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim lbreader As SqlDataReader = Cmd.ExecuteReader
                rownumber = 0
                While lbreader.Read()
                    cblist.Items.Add(lbreader(0).ToString)
                    vdescription.Add(lbreader(0).ToString)
                    rownumber = rownumber + 1
                End While
                lbreader.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading Template List")
        End Try
        cblist.Focus()
        Me.Refresh()
        Me.Show()

    End Sub

    Private Sub txtNotes_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNotes.KeyPress
        Dim x As Integer
        If Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 13 Then
            Try
                Dim numofchar() = Split(txtNotes.Text, vbCrLf)
                For x = 0 To UBound(numofchar)
                    If Len(numofchar(x)) > 79 Then
                        MsgBox("Reached number of characters allowed on line " & x + 1)
                    End If
                Next
            Catch ex As Exception
                MsgBox("Error calculating characters")
            End Try
            Try
                Dim lineCount As Integer = txtNotes.Lines.Count
                If lineCount > 28 Then
                    MsgBox("Reached number of allowables lines ")
                End If
            Catch ex As Exception
                MsgBox("Error calculating lines")
            End Try
        End If

    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Call SaveTxtNotes()
        NeedtoSaveNotes = False
    End Sub
    Sub SaveTxtNotes()

        If cblist.Text = "" Then
            MsgBox("No Template Name Defined")
            Exit Sub

        End If
        Dim ComDt As String = "", txtCom As String = "", iSpc As Integer = 0, Cmt(50) As String
        Dim i As Integer = 1
        Dim x As Integer
        Dim templatename As String

        Dim TempID As Integer
        If txtNotes.Text <> "" Then

            Dim Line1 As String = txtNotes.Text()
            Dim splitline1() As String = Split(Line1, vbCrLf)

            For j = 0 To UBound(splitline1)
                If Len(splitline1(j)) > 80 Then
                    MsgBox("Line is > 80 characters:" & splitline1(j))
                    Exit Sub
                End If
            Next

            Using Conn As New SqlConnection(strcon)
                Conn.Open()
                If NewItem = False Then
                    Try
                        Dim Cmd As New SqlCommand
                        Cmd.Connection = Conn
                        Cmd.CommandText = "Delete from xafftemplate where templatename = '" & cblist.SelectedItem.ToString() & "'"
                        Cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("Error.  New Note not Saved.  Hit Enter after keying new note name.")
                        Exit Sub
                    End Try
                End If

                Try
                    Dim Cmd1 As New SqlCommand(sql, Conn)
                    Cmd1.CommandText = "Select top 1 ID from xafftemplate order by id desc "

                    Dim cRS1 As SqlDataReader = Cmd1.ExecuteReader
                    While cRS1.Read()
                        TempID = (cRS1(0).ToString)

                    End While
                    TempID = TempID + 1
                Catch ex As Exception
                    MsgBox("Error Reading index")
                End Try

                Dim Tempseq As Integer = 1
                x = 1
                If NewItem = False Then
                    templatename = cblist.SelectedItem.ToString()
                Else
                    templatename = cblist.Text

                End If
                Try

                    Dim Cmd3 As New SqlCommand
                    Cmd3.Connection = Conn
                    For j = 0 To UBound(splitline1)
                        Do While Len(splitline1(j)) > 80
                            iSpc = FirstSpace(Mid(splitline1(j), 1, 80))
                            splitline1(j) = Replace(splitline1(j), "'", "")
                            Cmd3.CommandText = "Insert into xafftemplate values (" & "'" & TempID & "','" & UCase(templatename) & "','" & Tempseq & "','" & Mid(splitline1(j), 1, iSpc) & "')"
                            Cmd3.ExecuteNonQuery()
                            splitline1(j) = Mid(splitline1(j), iSpc + 1)
                            i = i + 1
                            Tempseq = Tempseq + 1
                            TempID = TempID + 1
                        Loop
                        splitline1(j) = Replace(splitline1(j), "'", "")
                        Cmd3.CommandText = "Insert into xafftemplate values (" & "'" & TempID & "','" & UCase(templatename) & "','" & Tempseq & "','" & (splitline1(j)) & "')"
                        Cmd3.ExecuteNonQuery()
                        i = i + 1
                        Tempseq = Tempseq + 1
                        TempID = TempID + 1
                    Next

                Catch ex As Exception
                    MsgBox("Error Writing Notes")
                End Try
                Conn.Close()

            End Using
            MsgBox("Notes Saved")
        End If
    End Sub

    Private Sub cblist_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cblist.SelectedIndexChanged
        txtNotes.Clear()
        selectedItemValue = cblist.SelectedItem.ToString()

        Try
            Using Conn As New SqlConnection(strcon)
                sql = "Select Templatetext From XAffTemplate where templatename = '" & selectedItemValue & "'"
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim x As SqlDataReader = Cmd.ExecuteReader
                rownumber = 0
                While x.Read()
                    txtNotes.Text = txtNotes.Text & x(0) & vbCrLf
                    rownumber = rownumber + 1
                End While
                x.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading Notes")
        End Try
        lblNew.Text = ""
    End Sub

    Private Sub cblist_KeyDown(sender As Object, e As KeyEventArgs) Handles cblist.KeyDown
        If e.KeyCode = Keys.Enter Then  'Or e.KeyCode = Keys.Tab Then
            If vdescription.Contains(UCase(cblist.Text)) = True Then
                MsgBox("Template name already exists")
                Exit Sub
            Else
                cblist.Items.Add(UCase(cblist.Text))
                selectedItemValue = cblist.Text
                lblNew.Text = "NEW"
                txtNotes.Clear()
                NewItem = True
                Me.Refresh()
            End If
        End If
    End Sub
    Public Function FirstSpace(ByVal str As String) As Integer
        FirstSpace = 0
        For i = Len(str) To 1 Step -1
            If Asc(Mid(str, i, 1)) = 32 Then
                FirstSpace = i
                Exit For
            End If
        Next i
    End Function
    Public Function LJ(ByVal strLJ As String, ByVal iLen As Integer) As String
        LJ = ""
        If Len(strLJ) > iLen Then
            LJ = strLJ
        Else
            LJ = strLJ & Space(iLen - Len(strLJ))
        End If
    End Function

    Private Sub btnClose_Click_1(sender As Object, e As EventArgs) Handles btnClose.Click
        If NeedtoSaveNotes = True Then
            Dim result As Integer = MessageBox.Show("Save Notes?", "Save Option", MessageBoxButtons.YesNoCancel)
            If result = DialogResult.Cancel Then
                Exit Sub
            ElseIf result = DialogResult.No Then
                cblist.SelectedItem = ""
                Me.Dispose()
                Me.Close()

            ElseIf result = DialogResult.Yes Then
                Call SaveTxtNotes()
                NeedtoSaveNotes = False
            End If
        Else
            Me.Dispose()
            cblist.Items.Clear()
            Me.Close()
        End If
    End Sub

    Private Sub txtNotes_ModifiedChanged(sender As Object, e As EventArgs) Handles txtNotes.ModifiedChanged
        NeedtoSaveNotes = True

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        NeedtoSaveNotes = False
        cblist.Items.Clear()
        lblNew.Text = ""
        NeedtoSaveNotes = False
        Me.Refresh()

        Try
            Using Conn As New SqlConnection(strcon)
                sql = "Select distinct TemplateName From XAffTemplate "
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim lbreader As SqlDataReader = Cmd.ExecuteReader
                rownumber = 0
                While lbreader.Read()
                    cblist.Items.Add(lbreader(0).ToString)
                    vdescription.Add(lbreader(0).ToString)
                    rownumber = rownumber + 1
                End While
                lbreader.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading Template List")
        End Try
        cblist.Focus()
        Me.Refresh()
        Me.Show()

    End Sub

    Private Sub btnDel_Click(sender As Object, e As EventArgs) Handles btnDel.Click
        If cblist.Text = "" Then
            MsgBox("No Template selected.")
            Exit Sub
        End If
        Dim ok As Integer = MsgBox("Are you sure you want to delete template: " & cblist.Text, vbOKCancel)
        If ok = vbOK Then
            Using Conn As New SqlConnection(strcon)
                sql = "delete from XAffTemplate where templatename='" & cblist.Text & "'"
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Cmd.ExecuteNonQuery()
                Conn.Close()
            End Using
        End If
        cblist.Items.Clear()
        lblNew.Text = ""
        cblist.Text = ""
        NeedtoSaveNotes = False
        txtNotes.Text = ""
        Me.Refresh()

        Try
            Using Conn As New SqlConnection(strcon)
                sql = "Select distinct TemplateName From XAffTemplate "
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim lbreader As SqlDataReader = Cmd.ExecuteReader
                rownumber = 0
                While lbreader.Read()
                    cblist.Items.Add(lbreader(0).ToString)
                    vdescription.Add(lbreader(0).ToString)
                    rownumber = rownumber + 1
                End While
                lbreader.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading Template List")
        End Try
        cblist.Focus()
        Me.Refresh()
        Me.Show()
    End Sub
End Class



