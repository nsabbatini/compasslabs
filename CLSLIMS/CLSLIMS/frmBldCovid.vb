﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmBldCovid

    Private Sub btnBuild_Click(sender As Object, e As EventArgs) Handles btnBuild.Click
        If ValidateTray() Then
            BuildCovidWorklist()
        Else
            MsgBox("Invalid Tray Number")
            txtTray.SelectAll()
            txtTray.Focus()
        End If
    End Sub
    Private Function ValidateTray() As Boolean
        Dim Sel As String
        ValidateTray = False
        Sel = "Select Count(*) From XTRAYS Where TRAY = '" & txtTray.Text & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            rs.Read()
            If rs(0) > 0 Then
                ValidateTray = True
            End If
            rs.Close()
            Conn.Close()
        End Using
    End Function
    Public Sub BuildCovidWorklist()
        Dim sql As String, sql2 As String, insPBI As String, insPB As String, cnt As Integer
        Dim newBatch As String, wrklst As String, wrklstef As String, mbatch As String, befdt As String
        'Batch Effective date used on all batches
        befdt = Format(Now, "yyyyMMddhhmmss")
        'determine worklists to build
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        sql = "Select Distinct WL From QWL Where PSPECNO in (Select PSPECNO From XTRAYS Where TRAY = '" & txtTray.Text & "') and WL in ('COVID19')"
        Dim Cmd As New SqlCommand(sql, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Dim CmdIns As New SqlCommand
        CmdIns.Connection = Conn
        Do While rs.Read()
            wrklst = rs(0)
            'Get new batch number for worklist = YMMDDSEQ
            newBatch = GetNewBatchNum(wrklst)
            'Get current Worklist Effective Date for worklist
            wrklstef = WorkListEffDate(wrklst)
            'Get Sequential batch number for batch
            mbatch = GetMBATCH()
            ListBox1.Items.Add(wrklst & ": " & newBatch & ", " & mbatch)
            'List of samples for particular Worklist


            'Need to order them by Folder Spreadsheet - use select case in PRTWRKLST
            'Now ordered by trayx NS 03/07/2018
            sql2 = "Select A.PSPECNO, TRAYx From XTRAYS A, QWL B Where A.PSPECNO=B.PSPECNO And TRAY = '" & txtTray.Text & "' And WL = '" & wrklst & "' Order By Trayx"
            Dim Cmd2 As New SqlCommand(sql2, Conn)
            Dim rs2 As SqlDataReader = Cmd2.ExecuteReader
            cnt = 1
            Do While rs2.Read()
                insPBI = "Insert into PBI (WL, BATCH, BEFDT, PBIx, PBITYPE, PSPECNO, PCONT, QCSPECNO, QCID, PBIRTXT) " &
                             "Values ('" & wrklst & "','" & newBatch & "','" & befdt & "'," & cnt & ",1,'" & rs2(0) & "','01',null,null,null)"
                CmdIns.CommandText = insPBI
                CmdIns.ExecuteNonQuery()
                CmdIns.CommandText = "Delete From QWL Where PSPECNO='" & rs2(0) & "' And WL='" & wrklst & "'"
                CmdIns.ExecuteNonQuery()
                cnt = cnt + 1
            Loop
            rs2.Close()
            rs2.Dispose()
            Cmd2.Dispose()
            insPB = "Insert into PB (WL, BATCH, BEFDT, BACT, PBUID, WEFDT, PBNOS, PBBQCRUN, PBEQCRUN, MBATCH, PBCLOSE, PBCLOSEID, PBCLOSEDT) " &
                  "Values ('" & wrklst & "','" & newBatch & "','" & befdt & "',1,'" & CLSLIMS.UID & "','" & wrklstef & "'," & cnt - 1 & ",null,null," & mbatch & ",0,null,null)"
            CmdIns.CommandText = insPB
            CmdIns.ExecuteNonQuery()
        Loop
        rs.Close()
        Conn.Close()
        ListBox1.Items.Add("DONE")
    End Sub
    Private Function Eligible(wrk As String, wrkef As String, specno As String) As Boolean
        Eligible = False
        Dim sel As String = "Select count(*) From qwl Where wl='" & wrk & "' and wefdt='" & wrkef & "' and pspecno='" & specno & "'"
        Dim ConnE As New SqlConnection(CLSLIMS.strCon)
        Dim CmdE As New SqlCommand(sel, ConnE)
        Dim rse As SqlDataReader = CmdE.ExecuteReader
        If rse.Read() Then
            If rse(0) > 0 Then
                Eligible = True
            End If
        End If
        rse.Close()
        ConnE.Close()
        CmdE.Dispose()
    End Function
    Public Function GetNewBatchNum(ByRef wrklst As String) As String
        GetNewBatchNum = ""
        Dim preBatch = Mid(Format(Now, "yyyy"), 4, 1) & Format(Now, "MMdd")
        Dim sel As String = "Select Max(BATCH) From PB Where Substring(BATCH,1,5)='" & preBatch & "' and wl='" & wrklst & "'"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        rsf.Read()
        If IsDBNull(rsf(0)) Then
            GetNewBatchNum = preBatch & "100"
        Else
            GetNewBatchNum = preBatch & (Mid(rsf(0), Len(rsf(0)) - 2, 3) + 1)
        End If
        rsf.Close()
        ConnF.Close()
    End Function
    Public Function WorkListEffDate(ByRef wrklst As String) As String
        WorkListEffDate = ""
        Dim sel As String = "Select MAX(wefdt) From WL Where WL='" & wrklst & "'"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        rsf.Read()
        WorkListEffDate = rsf(0)
        rsf.Close()
        ConnF.Close()
    End Function
    Public Function GetMBATCH() As String
        GetMBATCH = ""
        Dim sel As String = "Select SYSVALVAL + 1 From SYSVAL Where SYSVALKEY='MBATCH'"
        Dim upd As String = "Update SYSVAL Set SYSVALVAL = SYSVALVAL + 1 Where SYSVALKEY='MBATCH'"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        rsf.Read()
        GetMBATCH = rsf(0)
        rsf.Close()
        Dim CmdUpd As New SqlCommand
        CmdUpd.Connection = ConnF
        CmdUpd.CommandText = upd
        CmdUpd.ExecuteNonQuery()
        ConnF.Close()
    End Function

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class