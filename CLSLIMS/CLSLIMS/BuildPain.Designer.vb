﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BuildPain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBuild = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFolderNum = New System.Windows.Forms.TextBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.rdSEIA = New System.Windows.Forms.RadioButton()
        Me.rdLCMS = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'btnBuild
        '
        Me.btnBuild.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuild.Location = New System.Drawing.Point(32, 171)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "Build"
        Me.btnBuild.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Tray:"
        '
        'txtFolderNum
        '
        Me.txtFolderNum.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFolderNum.Location = New System.Drawing.Point(67, 15)
        Me.txtFolderNum.Name = "txtFolderNum"
        Me.txtFolderNum.Size = New System.Drawing.Size(100, 23)
        Me.txtFolderNum.TabIndex = 0
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(134, 171)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(10, 67)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(243, 69)
        Me.ListBox1.TabIndex = 4
        '
        'rdSEIA
        '
        Me.rdSEIA.AutoSize = True
        Me.rdSEIA.Checked = True
        Me.rdSEIA.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdSEIA.Location = New System.Drawing.Point(194, 15)
        Me.rdSEIA.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.rdSEIA.Name = "rdSEIA"
        Me.rdSEIA.Size = New System.Drawing.Size(51, 17)
        Me.rdSEIA.TabIndex = 5
        Me.rdSEIA.TabStop = True
        Me.rdSEIA.Text = "SEIA"
        Me.rdSEIA.UseVisualStyleBackColor = True
        '
        'rdLCMS
        '
        Me.rdLCMS.AutoSize = True
        Me.rdLCMS.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdLCMS.Location = New System.Drawing.Point(194, 37)
        Me.rdLCMS.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.rdLCMS.Name = "rdLCMS"
        Me.rdLCMS.Size = New System.Drawing.Size(55, 17)
        Me.rdLCMS.TabIndex = 6
        Me.rdLCMS.TabStop = True
        Me.rdLCMS.Text = "LCMS"
        Me.rdLCMS.UseVisualStyleBackColor = True
        '
        'BuildPain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(274, 216)
        Me.Controls.Add(Me.rdLCMS)
        Me.Controls.Add(Me.rdSEIA)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.txtFolderNum)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnBuild)
        Me.Name = "BuildPain"
        Me.Text = "Build Pain Worklists"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnBuild As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFolderNum As System.Windows.Forms.TextBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents rdSEIA As System.Windows.Forms.RadioButton
    Friend WithEvents rdLCMS As System.Windows.Forms.RadioButton
End Class
