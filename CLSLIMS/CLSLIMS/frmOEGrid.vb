﻿Public Class frmOEGrid
    Private Sub btnOrd_Click(sender As Object, e As EventArgs) Handles btnOrd.Click
        'If (cbHeroConf.Checked Or cbHeroScr.Checked) And (cbOpiConf.Checked Or cbOpiScr.Checked) Then
        '    cbHeroConf.Checked = False
        '    cbHeroScr.Checked = False
        'End If
        If QuickOE.txtAcct.Text <> "388" Then
            If cbChiral.Checked And (cbAmpConf.Checked = False And cbAmpScr.Checked = False) Then
                Dim fnt As Font
                fnt = frmMsgBox.lblMsg.Font
                frmMsgBox.lblMsg.Font = New Font("Tahoma", 12, FontStyle.Bold)
                'MsgBox("Chiral cannot be ordered without Amphetamine.", vbOKOnly, "        N E E D  A F F I D A V I T")
                frmMsgBox.BackColor = Color.Red
                frmMsgBox.lblMsg.Text = "Chiral cannot be ordered without Amphetamine"
                frmMsgBox.Text = "AFFIDAVIT NEEDED"
                frmMsgBox.ShowDialog()
            End If
        End If

        Dim Hero As Boolean = False
        QuickOE.lbOC.Items.Clear()
        If cbAlcConf.Checked Then
            QuickOE.lbOC.Items.Add("350000" & " - " & QuickOE.GetOCAbbr("350000"))
        End If
        If cbAlcScr.Checked Then
            QuickOE.lbOC.Items.Add("351000" & " - " & QuickOE.GetOCAbbr("351000"))
        End If
        If cbAlk.Checked Then
            QuickOE.lbOC.Items.Add("395000" & " - " & QuickOE.GetOCAbbr("395000"))
        End If
        If cbAmpConf.Checked Then
            QuickOE.lbOC.Items.Add("352000" & " - " & QuickOE.GetOCAbbr("352000"))
        End If
        If cbAmpScr.Checked Then
            QuickOE.lbOC.Items.Add("353000" & " - " & QuickOE.GetOCAbbr("353000"))
        End If
        If cbChiral.Checked Then
            QuickOE.lbOC.Items.Add("301027" & " - " & QuickOE.GetOCAbbr("301027"))
        End If
        If cbADSer.Checked Then
            QuickOE.lbOC.Items.Add("354000" & " - " & QuickOE.GetOCAbbr("354000"))
        End If
        If cbADTri.Checked Then
            QuickOE.lbOC.Items.Add("355000" & " - " & QuickOE.GetOCAbbr("355000"))
        End If
        If cbAD.Checked Then
            QuickOE.lbOC.Items.Add("356000" & " - " & QuickOE.GetOCAbbr("356000"))
        End If
        If cbAntiEp.Checked Then
            QuickOE.lbOC.Items.Add("357000" & " - " & QuickOE.GetOCAbbr("357000"))
        End If
        If cbAntiPys.Checked Then
            QuickOE.lbOC.Items.Add("358000" & " - " & QuickOE.GetOCAbbr("358000"))
        End If
        If cbBarbConf.Checked Then
            QuickOE.lbOC.Items.Add("359000" & " - " & QuickOE.GetOCAbbr("359000"))
        End If
        If cbBarbScr.Checked Then
            QuickOE.lbOC.Items.Add("360000" & " - " & QuickOE.GetOCAbbr("360000"))
        End If
        If cbBenzConf.Checked Then
            QuickOE.lbOC.Items.Add("361000" & " - " & QuickOE.GetOCAbbr("361000"))
        End If
        If cbBenzScr.Checked Then
            QuickOE.lbOC.Items.Add("362000" & " - " & QuickOE.GetOCAbbr("362000"))
        End If
        If cbBupConf.Checked Then
            QuickOE.lbOC.Items.Add("363000" & " - " & QuickOE.GetOCAbbr("363000"))
        End If
        If cbBupScr.Checked Then
            QuickOE.lbOC.Items.Add("364000" & " - " & QuickOE.GetOCAbbr("364000"))
        End If
        If cbTHCConf.Checked Then
            QuickOE.lbOC.Items.Add("365000" & " - " & QuickOE.GetOCAbbr("365000"))
        End If
        If cbTHCScr.Checked Then
            QuickOE.lbOC.Items.Add("366000" & " - " & QuickOE.GetOCAbbr("366000"))
        End If
        If cbCOCConf.Checked Then
            QuickOE.lbOC.Items.Add("367000" & " - " & QuickOE.GetOCAbbr("367000"))
        End If
        If cbCOCScr.Checked Then
            QuickOE.lbOC.Items.Add("368000" & " - " & QuickOE.GetOCAbbr("368000"))
        End If
        If cbFent.Checked Then
            QuickOE.lbOC.Items.Add("369000" & " - " & QuickOE.GetOCAbbr("369000"))
        End If
        If cbGab.Checked Then
            QuickOE.lbOC.Items.Add("370000" & " - " & QuickOE.GetOCAbbr("370000"))
        End If
        If cbHeroConf.Checked Then
            QuickOE.lbOC.Items.Add("371000" & " - " & QuickOE.GetOCAbbr("371000"))
        End If
        If cbHeroScr.Checked Then
            QuickOE.lbOC.Items.Add("372000" & " - " & QuickOE.GetOCAbbr("372000"))
        End If
        If cbMtdConf.Checked Then
            QuickOE.lbOC.Items.Add("373000" & " - " & QuickOE.GetOCAbbr("373000"))
        End If
        If cbMtdScr.Checked Then
            QuickOE.lbOC.Items.Add("374000" & " - " & QuickOE.GetOCAbbr("374000"))
        End If
        If cbMethAmpConf.Checked Then
            QuickOE.lbOC.Items.Add("375000" & " - " & QuickOE.GetOCAbbr("375000"))
        End If
        If cbMethAmpScr.Checked Then
            QuickOE.lbOC.Items.Add("376000" & " - " & QuickOE.GetOCAbbr("376000"))
        End If
        If cbMethy.Checked Then
            QuickOE.lbOC.Items.Add("377000" & " - " & QuickOE.GetOCAbbr("377000"))
        End If
        If cbNicConf.Checked Then
            QuickOE.lbOC.Items.Add("378000" & " - " & QuickOE.GetOCAbbr("378000"))
        End If
        If cbNicScr.Checked Then
            QuickOE.lbOC.Items.Add("379000" & " - " & QuickOE.GetOCAbbr("379000"))
        End If
        If cbOpiConf.Checked Then
            QuickOE.lbOC.Items.Add("380000" & " - " & QuickOE.GetOCAbbr("380000"))
        End If
        If cbOpiScr.Checked Then
            QuickOE.lbOC.Items.Add("381000" & " - " & QuickOE.GetOCAbbr("381000"))
        End If
        If cbOpiAnal.Checked Then
            QuickOE.lbOC.Items.Add("382000" & " - " & QuickOE.GetOCAbbr("382000"))
        End If
        If cbOxyConf.Checked Then
            QuickOE.lbOC.Items.Add("383000" & " - " & QuickOE.GetOCAbbr("383000"))
        End If
        If cbOxyScr.Checked Then
            QuickOE.lbOC.Items.Add("384000" & " - " & QuickOE.GetOCAbbr("384000"))
        End If
        If cbPhenConf.Checked Then
            QuickOE.lbOC.Items.Add("385000" & " - " & QuickOE.GetOCAbbr("385000"))
        End If
        If cbPhenScr.Checked Then
            QuickOE.lbOC.Items.Add("386000" & " - " & QuickOE.GetOCAbbr("386000"))
        End If
        If cbPreGab.Checked Then
            QuickOE.lbOC.Items.Add("387000" & " - " & QuickOE.GetOCAbbr("387000"))
        End If
        If cbProConf.Checked Then
            QuickOE.lbOC.Items.Add("388000" & " - " & QuickOE.GetOCAbbr("388000"))
        End If
        If cbProScr.Checked Then
            QuickOE.lbOC.Items.Add("389000" & " - " & QuickOE.GetOCAbbr("389000"))
        End If
        If cbSedHyp.Checked Then
            QuickOE.lbOC.Items.Add("390000" & " - " & QuickOE.GetOCAbbr("390000"))
        End If
        If cbSkelConf.Checked Then
            QuickOE.lbOC.Items.Add("391000" & " - " & QuickOE.GetOCAbbr("391000"))
        End If
        If cbSkelScr.Checked Then
            QuickOE.lbOC.Items.Add("392000" & " - " & QuickOE.GetOCAbbr("392000"))
        End If
        If cbTap.Checked Then
            QuickOE.lbOC.Items.Add("393000" & " - " & QuickOE.GetOCAbbr("393000"))
        End If
        If cbTram.Checked Then
            QuickOE.lbOC.Items.Add("394000" & " - " & QuickOE.GetOCAbbr("394000"))
        End If
        If cbLevo.Checked Then
            QuickOE.lbOC.Items.Add("308130" & " - " & QuickOE.GetOCAbbr("308130"))
        End If
        If cbXREF.Checked Then
            QuickOE.lbOC.Items.Add("800000" & " - " & QuickOE.GetOCAbbr("800000"))
        End If
        If cbSVT.Checked Then
            QuickOE.lbOC.Items.Add("209000" & " - " & QuickOE.GetOCAbbr("209000"))
        End If
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        ClearBoxes
    End Sub
    Public Sub ClearBoxes()
        cbAD.Checked = False
        cbADSer.Checked = False
        cbADTri.Checked = False
        cbAlcConf.Checked = False
        cbAlcScr.Checked = False
        cbAlk.Checked = False
        cbAmpConf.Checked = False
        cbAmpScr.Checked = False
        cbAntiEp.Checked = False
        cbAntiPys.Checked = False
        cbBarbConf.Checked = False
        cbBarbScr.Checked = False
        cbBenzConf.Checked = False
        cbBenzScr.Checked = False
        cbBupConf.Checked = False
        cbBupScr.Checked = False
        cbCOCConf.Checked = False
        cbCOCScr.Checked = False
        cbFent.Checked = False
        cbGab.Checked = False
        cbHeroConf.Checked = False
        cbHeroScr.Checked = False
        cbMethAmpConf.Checked = False
        cbMethAmpScr.Checked = False
        cbMethy.Checked = False
        cbMtdConf.Checked = False
        cbMtdScr.Checked = False
        cbNicConf.Checked = False
        cbNicScr.Checked = False
        cbOpiAnal.Checked = False
        cbOpiConf.Checked = False
        cbOpiScr.Checked = False
        cbOxyConf.Checked = False
        cbOxyScr.Checked = False
        cbPhenConf.Checked = False
        cbPhenScr.Checked = False
        cbPreGab.Checked = False
        cbProConf.Checked = False
        cbProScr.Checked = False
        cbSedHyp.Checked = False
        cbSkelConf.Checked = False
        cbSkelScr.Checked = False
        cbTap.Checked = False
        cbTHCConf.Checked = False
        cbTHCScr.Checked = False
        cbTram.Checked = False
        cbLevo.Checked = False
        cbXREF.Checked = False
        cbSVT.Checked = True
        cbChiral.Checked = False

    End Sub

    Private Sub frmOEGrid_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClearBoxes()
        For i = 0 To QuickOE.lbOC.Items.Count - 1
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "350000" Then cbAlcConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "351000" Then cbAlcScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "395000" Then cbAlk.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "352000" Then cbAmpConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "353000" Then cbAmpScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "309010" Then cbChiral.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "301027" Then cbChiral.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "354000" Then cbADSer.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "355000" Then cbADTri.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "356000" Then cbAD.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "357000" Then cbAntiEp.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "358000" Then cbAntiPys.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "359000" Then cbBarbConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "360000" Then cbBarbScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "361000" Then cbBenzConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "362000" Then cbBenzScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "363000" Then cbBupConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "364000" Then cbBupScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "365000" Then cbTHCConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "366000" Then cbTHCScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "367000" Then cbCOCConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "368000" Then cbCOCScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "369000" Then cbFent.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "370000" Then cbGab.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "371000" Then cbHeroConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "372000" Then cbHeroScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "373000" Then cbMtdConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "374000" Then cbMtdScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "375000" Then cbMethAmpConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "376000" Then cbMethAmpScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "377000" Then cbMethy.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "378000" Then cbNicConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "379000" Then cbNicScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "380000" Then cbOpiConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "381000" Then cbOpiScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "382000" Then cbOpiAnal.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "383000" Then cbOxyConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "384000" Then cbOxyScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "385000" Then cbPhenConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "386000" Then cbPhenScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "387000" Then cbPreGab.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "388000" Then cbProConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "389000" Then cbProScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "390000" Then cbSedHyp.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "391000" Then cbSkelConf.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "392000" Then cbSkelScr.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "393000" Then cbTap.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "394000" Then cbTram.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "308130" Then cbLevo.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "800000" Then cbXREF.Checked = True
            If CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1) = "209000" Then cbSVT.Checked = True
        Next i
    End Sub
End Class