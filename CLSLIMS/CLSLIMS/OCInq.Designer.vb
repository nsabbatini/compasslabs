﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OCInq
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtOC = New System.Windows.Forms.TextBox()
        Me.lblOC = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.cbDetail = New System.Windows.Forms.CheckBox()
        Me.btnLstAll = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Ordering Code:"
        '
        'txtOC
        '
        Me.txtOC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOC.Location = New System.Drawing.Point(147, 13)
        Me.txtOC.Name = "txtOC"
        Me.txtOC.Size = New System.Drawing.Size(99, 27)
        Me.txtOC.TabIndex = 1
        '
        'lblOC
        '
        Me.lblOC.AutoSize = True
        Me.lblOC.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOC.Location = New System.Drawing.Point(252, 20)
        Me.lblOC.Name = "lblOC"
        Me.lblOC.Size = New System.Drawing.Size(0, 20)
        Me.lblOC.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(498, 343)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Exit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 18
        Me.ListBox1.Location = New System.Drawing.Point(12, 46)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(630, 274)
        Me.ListBox1.TabIndex = 5
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(417, 343)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 6
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'cbDetail
        '
        Me.cbDetail.AutoSize = True
        Me.cbDetail.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbDetail.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDetail.Location = New System.Drawing.Point(12, 346)
        Me.cbDetail.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.cbDetail.Name = "cbDetail"
        Me.cbDetail.Size = New System.Drawing.Size(70, 21)
        Me.cbDetail.TabIndex = 7
        Me.cbDetail.Text = "Detail:"
        Me.cbDetail.UseVisualStyleBackColor = True
        '
        'btnLstAll
        '
        Me.btnLstAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLstAll.Location = New System.Drawing.Point(336, 343)
        Me.btnLstAll.Name = "btnLstAll"
        Me.btnLstAll.Size = New System.Drawing.Size(75, 23)
        Me.btnLstAll.TabIndex = 8
        Me.btnLstAll.Text = "Search"
        Me.btnLstAll.UseVisualStyleBackColor = True
        '
        'OCInq
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 378)
        Me.Controls.Add(Me.btnLstAll)
        Me.Controls.Add(Me.cbDetail)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblOC)
        Me.Controls.Add(Me.txtOC)
        Me.Controls.Add(Me.Label1)
        Me.Name = "OCInq"
        Me.Text = "Ordering Code Inquiry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtOC As System.Windows.Forms.TextBox
    Friend WithEvents lblOC As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents cbDetail As System.Windows.Forms.CheckBox
    Friend WithEvents btnLstAll As System.Windows.Forms.Button
End Class
