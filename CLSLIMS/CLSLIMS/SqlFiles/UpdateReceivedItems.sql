﻿SELECT currentqty INTO @cqty from items_1 where id = @ItemId;

set @aqty = (@cqty + @Qty);

UPDATE ITEMS_1
SET CURRENTQTY = @aqty
WHERE id = @ItemId;


SELECT MAX(id) into @maxid FROM received_items;

INSERT INTO received_items_details(receive_id, item_received, qty_received) VALUES(@maxid, @ItemDesc, @Qty);

