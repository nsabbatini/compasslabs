﻿select p1.did, p.pid, p1.orderqty, p1.unitprice, p1.itemcost,
(select itemdesc from items_1 where id = p1.itemid) as "ItemDesc", 
p1.received_qty, p1.back_ordered, p1.received,
p1.itemid as "ItemId", p.ponumber, p.vid,
(select needslotnum from items_1 where id = p1.itemId) as "Expiration" ,
(select itemunit from items_1 where id = p1.itemId) as "Units"
from purchase_orders p left JOIN purchase_order_details p1 on p.pid = p1.pid
where p.NonInventory = 0;
