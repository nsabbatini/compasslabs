﻿select p.pid, p.ponumber, p.podate, p.totalpo, p.salestax, (select v.vendorname from vendors_1 v where v.id = p.vid) as "Vendor"
from purchase_orders p
where p.status = 1 order by  p.ponumber desc,  p.podate desc;

select p.pid, (select itemdesc from items_1 where id = p.itemid) as "Item Description", p.orderqty, p.unitprice, p.itemcost
from purchase_order_details p
where p.received = 1;