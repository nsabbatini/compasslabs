﻿select id, itemdesc, i.minqty, currentqty, itemunit, ifnull((select max(orderqty) from purchase_order_details pd where pd.itemid = i.id and pd.received =0 ), 0) as "OnOrder", itemvendorcost, (select vendorname from vendors_1 where id = i.vid) as "VendorName"
from items_1 i
order by itemdesc