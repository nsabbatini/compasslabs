﻿INSERT INTO orders_new(OrderNumber, Acct, Aname,Contact,Address1,Address2,City,State,Zip,Phone, Fax, Courier, UID, 
OrderDate, OrderStatus)
VALUES(@OrderNumber, @Customer, @Name, @Contact, @Address1, @Address2, @City, @State, @Zip, @Phone, @Fax, @Courier, @UID, 
CURRENT_TIMESTAMP, @OrderStatus);