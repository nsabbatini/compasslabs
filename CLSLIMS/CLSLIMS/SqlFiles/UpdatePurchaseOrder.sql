﻿UPDATE purchase_orders
set totalpo = @Total,
	notes = @Notes,
	department = @Department,
	salestax = @SalesTax,
	salesperson = @SalesPerson,
	quotenumber = @QuoteNumber,
	terms = @PaymentTerms
WHERE pid = @PId;

