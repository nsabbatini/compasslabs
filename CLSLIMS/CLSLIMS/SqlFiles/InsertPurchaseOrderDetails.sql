﻿SELECT MAX(pid) INTO @PId FROM purchase_orders;


INSERT INTO purchase_order_details(pid, itemid, orderqty, itemcost, unitprice, noninventory)
VALUES(@PId, @ItemId, @Qty, @ItemCost, @UnitCost, @ItemDesc);