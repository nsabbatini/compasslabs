﻿SELECT distinct [ACCT],
      [AEFDT]
      ,[AACT]
      ,ISNULL([ANAME], '') AS ANAME
      ,ISNULL([ANAME2], '') AS ANAME2
      ,ISNULL([AAD1], '') AS AAD1
      ,ISNULL([AAD2], '') AS AAD2
      ,ISNULL([ACITY], '') AS ACITY
      ,ISNULL([ACNTY], '') AS ACNTY
      ,ISNULL([ASTATE], '') AS ASTATE
      ,ISNULL([ACNTRY], '') AS ACNTRY
      ,ISNULL([AZIP], '') AS AZIP
      ,ISNULL([APHONE], '') AS APHONE
      ,ISNULL([AFAX], '') AS AFAX
      ,ISNULL([ACONTACT], '') AS ACONTACT
      ,ISNULL([AEMAIL], '') AS AEMAIL
  FROM [TNCL_PROD].[dbo].[ACCT]
  where aact =1
  order by ACCT
