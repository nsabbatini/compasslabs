﻿UPDATE vendors_1
set vendorName = @VendorName
, Address = @Address
, Address2 = @Address2
, city = @City
, state = @State
, zip = @Zip
, contact = @Contact
, phone = @Phone
, fax = @Fax
, notes = @Notes
WHERE id = @Id;
