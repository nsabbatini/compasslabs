﻿DELIMITER $$

DROP PROCEDURE IF EXISTS UpdatePoDetails $$
CREATE PROCEDURE UpdatePoDetails(IN Qty int, IN PId int, IN ItemId int, IN ItemCost FLOAT(10,2), IN UnitCost FLOAT(10,2), IN ItemDesc Varchar(400), IN DId int)
BEGIN
update purchase_order_details
set orderqty = Qty
where did = DId;

IF DId = 0 THEN
	INSERT INTO purchase_order_details(pid, itemid, orderqty, itemcost, unitprice, noninventory)
	VALUES(PId, ItemId, Qty, ItemCost, UnitCost, ItemDesc);
END IF;

END $$

DELIMITER;