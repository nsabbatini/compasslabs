﻿INSERT INTO purchase_orders(ponumber, vid, podate, poterms, notes, department, status, approved, totalpo, shipto, salestax, salesperson, quotenumber, terms, NonInventory)
VALUES(@PoNumber, @Vid, CURRENT_TIMESTAMP, @Terms, @Notes, @Department, 4, @NeedsApproval, @Total, @ShipTo, @SalesTax, @SalesPerson, @QuoteNumber, @PaymentTerms, @NonInventory);

Update purchase_order_format
set ponumber = @PoNumber;
