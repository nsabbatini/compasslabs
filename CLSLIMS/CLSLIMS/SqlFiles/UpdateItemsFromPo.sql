﻿SELECT currentqty INTO @cqty from items_1 where id = @ItemId;

set @aqty = (@cqty + @Qty);

UPDATE ITEMS_1
SET CURRENTQTY = @aqty
WHERE id = @ItemId;