﻿select p.pid, p1.orderqty, p1.unitprice, p1.itemcost,
case when p1.itemid = 0 then p1.noninventory else (select concat(itemdesc, ' - ', vendoritemid) from items_1 where id = p1.itemid) END as "ItemDesc"
from purchase_orders p left JOIN purchase_order_details p1 on p.pid = p1.pid
where p.ponumber = @PoNumber;