﻿select id, invoice_number, received_by, date_received, vendor
from received_items
order by date_received desc;

select detail_id, receive_id, item_received, qty_received, (SELECT currentqty from items where itemdesc = item_received) as "OnHand"
from received_items_details;