﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid.Views.Grid
Imports Compass.Report.Library
Imports GrapeCity.ActiveReports

Public Class frmReconcilePo
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dtPO As DataTable = New DataTable
    Private _dtView As DataView
    Private vendorId As Int16
    Private _PId As Int16
    Private _newItemId As Int16
    Private _isNonInvnentory As Int16 = 0
    Private Sub frmReconcilePo_Load(sender As Object, e As EventArgs) Handles Me.Load
        _isNonInvnentory = 0
        LoadPoGrid()

    End Sub
    Private Sub LoadPoGrid()

        Dim results = GetPurchaseOrders()

        grdLookup.Properties.DataSource = results
        grdLookup.Properties.DisplayMember = "PoNumber"
        grdLookup.Properties.ValueMember = "PId"


    End Sub
    Private Function GetPurchaseOrders() As Object

        Try

            Using getPos As New PurchaseOrders()

                _fileName = _fileDir & GetPurchaseOrdersSql
                getPos._sqlText = ReadSqlFile(_fileName)
                Return getPos.GetPurchaseOrder.Where(Function(x) x.Status <> 1 And x.NonInventory = _isNonInvnentory).ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReconcilePo.GetPurchaseOrders", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function
    Private Function GetPurchaseOrdersDetails() As Object

        Try

            Using getPos As New PurchaseOrders()

                _fileName = _fileDir & GetReconcilePoSql
                getPos._sqlText = ReadSqlFile(_fileName)
                getPos._pid = grdLookup.EditValue
                Return getPos.ReconcilePo.Where(Function(x) x.PId = grdLookup.EditValue And x.Received = False).ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReconcilePo.GetPurchaseOrdersDetails", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function

    Private Sub grdLookup_EditValueChanged(sender As Object, e As EventArgs) Handles grdLookup.EditValueChanged

        LoadGrid()

    End Sub
    Private Sub LoadGrid()
        Dim results = GetPurchaseOrdersDetails()

        vendorId = results(0).GetType().GetProperty("VendorId")?.GetValue(results(0))

        _PId = results(0).GetType().GetProperty("PId")?.GetValue(results(0))

        Dim View As ColumnView = grdPODetails.MainView
        Dim fieldNames() As String = New String() {
            "DId", "PId", "ItemDesc", "OrderQty", "UnitPrice", "ItemCost",
            "ReceivedQty", "BackOrdered", "Received", "ItemId", "ExpirationDate", "Units"
        }

        Dim column As DevExpress.XtraGrid.Columns.GridColumn
        View.Columns.Clear()

        For i = 0 To fieldNames.Length - 1
            column = View.Columns.AddField(fieldNames(i))
            column.VisibleIndex = i

            If fieldNames(i) = "DId" Then
                column.Visible = False
            ElseIf fieldNames(i) = "PId" Then
                column.Visible = False
            ElseIf fieldNames(i) = "UnitPrice" Then
                column.Visible = False
            ElseIf fieldNames(i) = "ItemCost" Then
                column.Visible = False
            ElseIf fieldNames(i) = "Received" Then
                column.Visible = False
            ElseIf fieldNames(i) = "ReceivedQty" Then
                column.Caption = "Partial Order"
            ElseIf fieldNames(i) = "ItemId" Then
                column.Visible = False
            ElseIf fieldNames(i) = "ExpirationDate" Then
                column.Visible = False
            ElseIf fieldNames(i) = "Units" Then
                column.Visible = False
            End If
        Next


        Dim arow As DataRow

        _dtPO = CreateData()

        For Each item In results
            arow = _dtPO.NewRow
            arow(0) = item.GetType().GetProperty("DId")?.GetValue(item)
            arow(1) = item.GetType().GetProperty("PId")?.GetValue(item)
            arow(2) = item.GetType().GetProperty("OrderQty")?.GetValue(item)
            arow(3) = item.GetType().GetProperty("UnitPrice")?.GetValue(item)
            arow(4) = item.GetType().GetProperty("ItemCost")?.GetValue(item)
            arow(5) = item.GetType().GetProperty("ItemDesc")?.GetValue(item)
            arow(6) = item.GetType().GetProperty("ReceivedQty")?.GetValue(item)
            arow(7) = item.GetType().GetProperty("BackOrdered")?.GetValue(item)
            arow(8) = item.GetType().GetProperty("Received")?.GetValue(item)
            arow(9) = item.GetType().GetProperty("ItemId")?.GetValue(item)
            arow(10) = item.GetType().GetProperty("ExpirationDate")?.GetValue(item)
            arow(11) = item.GetType().GetProperty("Units")?.GetValue(item)

            _dtPO.Rows.Add(arow)
        Next

        _dtView = New DataView(_dtPO)
        grdPODetails.DataSource = _dtPO.DefaultView


    End Sub
    Private Function CreateData() As DataTable

        Dim _dtItems As DataTable = New DataTable
        _dtItems.Columns.Add("DId", GetType(Int16))
        _dtItems.Columns.Add("PId", GetType(Int16))
        _dtItems.Columns.Add("OrderQty", GetType(Int16))
        _dtItems.Columns.Add("UnitPrice", GetType(Double))
        _dtItems.Columns.Add("ItemCost", GetType(Double))
        _dtItems.Columns.Add("ItemDesc", GetType(String))
        _dtItems.Columns.Add("ReceivedQty", GetType(Int16))
        _dtItems.Columns.Add("BackOrdered", GetType(Boolean))
        _dtItems.Columns.Add("Received", GetType(Boolean))
        _dtItems.Columns.Add("ItemId", GetType(Int16))
        _dtItems.Columns.Add("ExpirationDate", GetType(Boolean))
        _dtItems.Columns.Add("Units", GetType(String))


        Return _dtItems

    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If ReconcileThePurchaeOrder() Then

            ' PrintBarCode()

            MessageBox.Show(Me, "All items were successfully saved and inventory updated!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
            LoadPoGrid()
            _dtPO.Clear()
            pnlNewItems.Visible = False

        End If

    End Sub
    Private Function PrintBarCode() As Boolean
        Dim barcode As String = String.Empty
        Try
            With _dtPO
                For i = 0 To _dtPO.Rows.Count - 1

                    barcode = .Rows(i).Item("ItemId") & "," &
                              .Rows(i).Item("OrderQty")

                    HowManyBarCodes(.Rows(i).Item("OrderQty"), barcode, .Rows(i).Item("ItemDesc").ToString(), .Rows(i).Item("Units").ToString())
                    barcode = String.Empty

                Next
            End With

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReconcilePo.PrintBarCode", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            Return False
        End Try

        Return True


    End Function
    Private Sub HowManyBarCodes(ByVal qty As Int16, ByVal barcode As String, ByVal desc As String, ByVal units As String)
        Dim xPrint As Int16 = 0

        Select Case units.ToUpper
            Case "EA"
                xPrint = qty
            Case "BOX"
                xPrint = qty
            Case "Case"
            Case "SHEETS"
            Case "10"
            Case "PACK"
        End Select

        For i = 0 To xPrint - 1
            Dim rpt As New rptPOBarCodes
            rpt._barcode = barcode
            rpt._description = desc
            rpt.Run()
            Dim sectionDocument = rpt.Document

            sectionDocument.Print(False, True, False)
        Next



    End Sub
    Private Function ReconcileThePurchaeOrder() As Boolean
        Dim ItemsRows As New ArrayList()
        Dim ItemsNeedDate As New ArrayList()
        Dim partialOrder As Int16 = 0
        Dim backOrder As Int16 = 0
        Dim updateInv As New ArrayList()

        If GridView1.SelectedRowsCount = 0 Then
            MessageBox.Show(Me, "No items have been selected! Select some items before saving", "No items selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Function
        End If
        Dim x As Int16 = GridView1.SelectedRowsCount()
        Dim x1 As Int16 = GridView1.DataRowCount()

        Try
            For I = 0 To GridView1.SelectedRowsCount() - 1
                If (GridView1.GetSelectedRows()(I) >= 0) Then
                    ItemsRows.Add(GridView1.GetDataRow(GridView1.GetSelectedRows()(I)))
                    If CType(GridView1.GetRowCellValue(I, "ReceivedQty"), Int16) >= 1 Then
                        partialOrder += 1
                    End If
                    If CType(GridView1.GetRowCellValue(I, "BackOrdered"), Boolean) = True Then
                        backOrder += 1
                    End If
                    If CType(GridView1.GetRowCellValue(I, "ExpirationDate"), Boolean) = True Then
                        ItemsNeedDate.Add(GridView1.GetDataRow(GridView1.GetSelectedRows()(I)))
                    End If
                End If
            Next

            If x1 <> x Then
                For j = 0 To GridView1.DataRowCount() - 1
                    If CType(GridView1.GetRowCellValue(j, "BackOrdered"), Boolean) = True Then
                        ItemsRows.Add(GridView1.GetDataRow(j))
                        backOrder += 1
                    End If
                Next
            End If

            'update purchase_order_details
            Dim pid As Int16
            _fileName = _fileDir & UpdatePurchaseOrderDetails
            Using po As New PurchaseOrders() With {._sqlText = ReadSqlFile(_fileName)}
                For n = 0 To ItemsRows.Count - 1
                    Dim row As DataRow = CType(ItemsRows(n), DataRow)
                    po._did = row(0)
                    po._pid = row(1)
                    pid = row(1)
                    po._orderQty = row(2)
                    po._itemId = row(9)
                    po._received = 1
                    If row(6) >= 1 Then
                        po._received = 0
                        po._receivedQty = row(6)
                    Else
                        po._receivedQty = 0
                    End If
                    If row(7) = True Then
                        po._received = 0
                        po._backOrdered = IIf(row(7), 1, 0)
                    Else
                        po._backOrdered = 0
                    End If
                    po.ReceiveInventoryFromPo()
                Next
            End Using

            'Update items table qty count
            _fileName = _fileDir & UpdateItemsFromPoSql
            Using po As New PurchaseOrders() With {._sqlText = ReadSqlFile(_fileName)}
                For p = 0 To ItemsRows.Count - 1
                    Dim row As DataRow = CType(ItemsRows(p), DataRow)
                    po._itemId = row(9)
                    If Not row(7) = True Then
                        po._qty = (row(2) - row(6))
                    End If
                    po.SetNewQuantityItems()
                Next
            End Using

            'update purchase_order table status
            _fileName = _fileDir & UpdatePurchaseOrderMainSql
            Using setStatus As New PurchaseOrders() With {._sqlText = ReadSqlFile(_fileName), ._pid = pid}
                If backOrder = 0 And partialOrder = 0 And x1 = x Then
                    setStatus._status = 1
                    setStatus.SetStatusPurchaseOrder()
                Else
                    setStatus._status = 3
                    setStatus.SetStatusPurchaseOrder()
                End If
            End Using


        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReconcilePo.ReconcileThePurchaeOrder", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            Return False
        End Try

        Return True

    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub

    Private Sub btnNewRow_Click(sender As Object, e As EventArgs) Handles btnNewRow.Click

        pnlNewItems.Visible = True

        Dim results = GetInventoryDb()


        grdGetItems.Properties.DataSource = results
        grdGetItems.Properties.DisplayMember = "ItemDesc"
        grdGetItems.Properties.ValueMember = "Id"


        'grdPODetails.RefreshDataSource()

    End Sub
    Private Function GetInventoryDb() As Object

        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & InventorySqlItems
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.GetInventory.Where(Function(x) x.VId = vendorId).ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderMain.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function

    Private Sub btnNewItemRow_Click(sender As Object, e As EventArgs) Handles btnNewItemRow.Click
        _fileName = _fileDir & InsertNewItemPodSql
        Using newitem As New PurchaseOrders() With {
            ._sqlText = ReadSqlFile(_fileName),
            ._pid = _PId,
            ._qty = txtNewitem.Text,
            ._itemId = _newItemId
        }
            newitem.InsertNewPurchaseOrderDetails()
            LoadGrid()
            pnlNewItems.Visible = False

        End Using
    End Sub

    Private Sub grdGetItems_EditValueChanged(sender As Object, e As EventArgs) Handles grdGetItems.EditValueChanged
        Dim arow = grdGetItems.GetSelectedDataRow()

        _newItemId = arow.GetType().GetProperty("Id")?.GetValue(arow)

    End Sub

    Private Sub chkNotInventory_CheckedChanged(sender As Object, e As EventArgs) Handles chkNotInventory.CheckedChanged
        If chkNotInventory.Checked Then
            _isNonInvnentory = 1
        Else
            _isNonInvnentory = 0
        End If

        LoadPoGrid()
    End Sub
End Class