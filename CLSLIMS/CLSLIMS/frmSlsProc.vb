﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmSlsProc
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub cbCode_TextChanged(sender As Object, e As EventArgs) Handles cbCode.TextChanged
        txtName.Text = GetName(cbCode.Text)
        txtPhone.Text = GetPhone(cbCode.Text)

    End Sub

    Private Function GetName(ByVal cd As String) As String
        Dim xSel As String = "Select SYSLISTOPT from SYSLIST where SYSLISTKEY in ('ASALESRG','PROC') AND SYSLISTVAL = '" & cd & "'"
        GetName = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetName = cRS(0)
            End If
            cRS.Close()
        End Using
    End Function

    Private Function GetPhone(ByVal cd As String) As String
        Dim xSel As String = "Select SYSLISTOPT from SYSLIST where SYSLISTKEY = 'SALESPH' AND SYSLISTVAL = '" & cd & "'"
        GetPhone = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetPhone = cRS(0)
            End If
            cRS.Close()
        End Using
    End Function

    Private Sub frmSlsProc_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "DEDRICK.RUSSELL" And CLSLIMS.UID <> "NED.SABBATINI" Then
            MsgBox("Unauthorized.")
            Me.Close()
            Me.Dispose()
        End If
        Dim xSel As String = "Select syslistval From syslist Where syslistkey in ('ASALESRG','PROC') order by syslistval"

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                cbCode.Items.Add(cRS(0))
            Loop
            cRS.Close()
            Cmd.Dispose()
        End Using

    End Sub

    Private Function NewCode(ByVal cd As String) As Boolean
        Dim xSel As String = "Select SYSLISTOPT from SYSLIST where SYSLISTKEY in ('ASALESRG','PROC') AND SYSLISTVAL = '" & cd & "'"
        NewCode = True
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                NewCode = False
            End If
            cRS.Close()
        End Using
    End Function

    Private Function NextSeq(ByVal ndx As String) As Integer
        Dim xSel As String = "Select max(SYSLISTSEQ) from SYSLIST where SYSLISTKEY = '" & ndx & "'"
        NextSeq = 0
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                NextSeq = cRS(0) + 1
            End If
            cRS.Close()
        End Using
    End Function
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Not rbProc.Checked And Not rbSales.Checked Then
            MsgBox("Salesman or Processor must be selected.")
            Exit Sub
        End If
        If cbCode.Text = "" Then
            MsgBox("Must select code or enter new code.")
            Exit Sub
        End If
        'Save data
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Dim cmdstr1 As String = "", cmdstr2 As String = ""
        If NewCode(cbCode.Text) Then
            If rbSales.Checked Then
                cmdstr1 = "Insert into SYSLIST Values ('ASALESRG'," & NextSeq("ASALESRG") & ",'" & cbCode.Text & "','" & txtName.Text & "')"
                cmdstr2 = "Insert into SYSLIST Values ('SALESPH'," & NextSeq("SALESPH") & ",'" & cbCode.Text & "','" & txtPhone.Text & "')"
            ElseIf rbProc.Checked Then
                cmdstr1 = "Insert into SYSLIST Values ('PROC'," & NextSeq("PROC") & ",'" & cbCode.Text & "','" & txtName.Text & "')"
                cmdstr2 = "Insert into SYSLIST Values ('SALESPH'," & NextSeq("SALESPH") & ",'" & cbCode.Text & "','" & txtPhone.Text & "')"
            End If
        Else
            If rbSales.Checked Then
                cmdstr1 = "Update SYSLIST Set SYSLISTOPT='" & txtName.Text & "' Where SYSLISTVAL='" & cbCode.Text & "' And SYSLISTKEY='ASALESRG'"
                cmdstr2 = "Update SYSLIST Set SYSLISTOPT='" & txtPhone.Text & "' Where SYSLISTVAL='" & cbCode.Text & "' And SYSLISTKEY='SALESPH'"
            ElseIf rbProc.Checked Then
                cmdstr1 = "Update SYSLIST Set SYSLISTOPT='" & txtName.Text & "' Where SYSLISTVAL='" & cbCode.Text & "' And SYSLISTKEY='PROC'"
                cmdstr2 = "Update SYSLIST Set SYSLISTOPT='" & txtPhone.Text & "' Where SYSLISTVAL='" & cbCode.Text & "' And SYSLISTKEY='SALESPH'"
            End If
        End If
        Cmd.CommandText = cmdstr1
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = cmdstr2
        Cmd.ExecuteNonQuery()

        txtName.Text = ""
        txtPhone.Text = ""
        rbProc.Checked = False
        rbSales.Checked = False
        cbCode.Text = ""
        cbCode.Items.Clear()
        Cmd.CommandText = "Select syslistval From syslist Where syslistkey in ('ASALESRG','PROC') order by syslistval"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cbCode.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
    End Sub
End Class