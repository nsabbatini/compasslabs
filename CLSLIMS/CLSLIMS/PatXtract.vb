﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class PatXtract
    Private Sub btnExt_Click(sender As Object, e As EventArgs) Handles btnExt.Click
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Dim Cmd As New SqlCommand
        Conn.Open()
        Cmd.Connection = Conn
        Dim Sel As String = "select a.puid,'',plname,pfname,substring(pmname,1,1),'','',PDOB,sex,'',pad1,pad2,pcity,pstate,pzip,pphone," &
                            "'','','','','','','',''," &  'guaranator
                            "piord as rank,b.bins,binsname,binsad1,binsad2,binscity,binsstate,binszip,binsphone,pichlnm+', '+pichfnm,pichrel,pigrp,pipol," &   'primary insurance
                            "'','','','',''," &    'Primary care physician
                            "'','',''," &          '-- Guarantor Info
                            "''," &                '-- Primary care physician info (NPI)
                            "''," &                '-- Pat Info
                            "'','','','','','','','','','',''," &         '-- Primary insurance provider info
                            "'','','','','','','','','','','' " &         '-- Secondary insurance provider info
                            "From PDEM a, PIPI b, BINS c, PORD d " &
                            "Where a.puid = d.puid And a.pefdt = d.pefdt And poact = 1 and pact=1 " &
                            "And a.puid = b.puid And a.pefdt = b.pefdt " &
                            "and b.bins=c.bins and b.binsefdt=c.binsefdt and pipix=0 " &
                            IIf(UCase(txtAcct.Text) = "ALL", "", "And acct='" & txtAcct.Text & "' ") &         '--     <-----------------  CHANGE ACCOUNT HERE
                        "union " &
                            "select a.puid,'',plname,pfname,substring(pmname,1,1),'','',PDOB,sex,'',pad1,pad2,pcity,pstate,pzip,pphone," &
                            "'','','','','','','',''," &   '--guaranator
                            "pimcord as rank,'MC','Medicare','','','','','','','','',pimchic,''," &   '--primary insurance
                            "'','','','',''," &    '-- Primary care physician
                            "'','',''," &          '-- Guarantor Info
                            "''," &                '-- Primary care physician info (NPI)
                            "''," &                '-- Pat Info
                            "'','','','','','','','','','',''," &         '-- Primary insurance provider info
                            "'','','','','','','','','','','' " &         '-- Secondary insurance provider info
                            "From PDEM a, PIMC b, PORD d " &
                            "Where a.puid = d.puid And a.pefdt = d.pefdt And poact = 1 and pact=1 " &
                            "and a.puid=b.puid and a.pefdt=b.pefdt and pimcx=0 " &
                            IIf(UCase(txtAcct.Text) = "ALL", "", "And acct='" & txtAcct.Text & "' ") &      '--     <-----------------  CHANGE ACCOUNT HERE
                        "union " &
                            "select a.puid,'',plname,pfname,substring(pmname,1,1),'','',PDOB,sex,'',pad1,pad2,pcity,pstate,pzip,pphone," &
                            "'','','','','','','',''," &   '--guaranator
                            "pimdord as rank,'MD','Medicaid','','','','','','','','',pimdrid,pimdstate," &   '--primary insurance
                            "'','','','',''," &    '-- Primary care physician
                            "'','',''," &          '-- Guarantor Info
                            "''," &                '-- Primary care physician info (NPI)
                            "'', " &               '-- Pat Info
                            "'','','','','','','','','','',''," &         '-- Primary insurance provider info
                            "'','','','','','','','','','','' " &         '-- Secondary insurance provider info
                            "From PDEM a, PIMD b, PORD d " &
                            "Where a.puid = d.puid And a.pefdt = d.pefdt And poact = 1 and pact=1 " &
                            "and a.puid=b.puid and a.pefdt=b.pefdt and pimdx=0 " &
                            IIf(UCase(txtAcct.Text) = "ALL", "", "And acct='" & txtAcct.Text & "' ") &      '--     <-----------------  CHANGE ACCOUNT HERE
                        "order by a.puid asc,rank desc"
        Dim SecIns As String = "", Ext As String = "", cnt As Integer = 0
        Dim Extfile As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\PatXtract.csv", False)
        Cmd.CommandText = Sel
        Dim rs As SqlDataReader
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cnt = cnt + 1
            If rs(24) = 2 Then
                SecIns = ""
                For i = 25 To 37
                    SecIns = SecIns & rs(i) & ","
                Next i
            ElseIf rs(24) = 1 Then
                For j = 0 To 37
                    If j <> 24 Then Ext = Ext & rs(j) & ","
                Next j
                If SecIns = "" Then SecIns = ",,,,,,,,,,,,,"
                Ext = Ext & SecIns & ","
                For j = 51 To 68
                    Ext = Ext & rs(j) & ","
                Next j
                Extfile.WriteLine(Ext)
                Ext = ""
                SecIns = ""
            Else
                SecIns = ""
                Ext = ""
            End If
            lstPatXtract.Items.Add(rs(0) & "  " & rs(24))
            If cnt Mod 1000 = 0 Then Debug.Print(cnt)
        Loop
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
        Extfile.Close()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class