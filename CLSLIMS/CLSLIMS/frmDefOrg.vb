﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.ComponentModel
Public Class frmDefOrg
    Private Sub frmDefOrg_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblNew.Text = ""
        ReloadCombos()
    End Sub
    Public Sub ReloadCombos()
        cmbOrg.Items.Clear()
        cmbAbbr.Items.Clear()
        cmbCPT.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select OGNAME from MOLECULAR Where Active=1 and TYPE='ORG' Order by OGNAME"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cmbOrg.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select Distinct OGABBR from MOLECULAR Where Active=1 and TYPE='ORG' Order by OGABBR"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbAbbr.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select Distinct CPT from MOLECULAR Where Active=1 and TYPE='ORG' Order by CPT"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbCPT.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()

    End Sub

    Private Sub cmbOrg_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbOrg.SelectedIndexChanged
        ValidateOrg()
    End Sub

    Private Sub cmbOrg_Validating(sender As Object, e As CancelEventArgs) Handles cmbOrg.Validating
        ValidateOrg()
    End Sub
    Public Sub ValidateOrg()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Dim rs As MySqlDataReader = Nothing
        Dim Src As String = "", Del As String = ""
        If Not cmbOrg.Items.Contains(cmbOrg.Text) Then
            lblNew.Text = "NEW"
        Else
            Cmd.CommandText = "Select OGABBR, CPT from MOLECULAR Where OGNAME='" & cmbOrg.Text & "' And active=1 and TYPE='ORG'"
            rs = Cmd.ExecuteReader
            If rs.Read Then
                cmbAbbr.Text = rs(0) : cmbCPT.Text = rs(1)
            End If
            rs.Close()
            ConnMySql.Close()
        End If
        cmbOrg.Enabled = False
    End Sub
    Public Sub ResetScrn()
        cmbOrg.Enabled = True
        cmbOrg.Text = ""
        cmbAbbr.Text = ""
        cmbCPT.Text = ""
        lblNew.Text = ""
        ReloadCombos()
        cmbOrg.Focus()
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        ResetScrn()
    End Sub

    Private Sub cmbOrg_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbOrg.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        Else
            ValidateOrg()
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        If lblNew.Text = "NEW" Then
            Cmd.CommandText = "Insert into MOLECULAR values ('" & cmbOrg.Text & "','" & cmbAbbr.Text & "','" & cmbCPT.Text & "','ORG',1)"
        Else
            Cmd.CommandText = "Update MOLECULAR Set OGABBR='" & cmbAbbr.Text & "',CPT='" & cmbCPT.Text & "' Where OGNAME='" & cmbOrg.Text & "' And Active=1 and TYPE='ORG'"
        End If
        Cmd.ExecuteNonQuery()
        ConnMySql.Close()
        ResetScrn()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class