﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOEGrid
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cbAntiPys = New System.Windows.Forms.CheckBox()
        Me.cbTram = New System.Windows.Forms.CheckBox()
        Me.cbTap = New System.Windows.Forms.CheckBox()
        Me.cbSkelConf = New System.Windows.Forms.CheckBox()
        Me.cbSedHyp = New System.Windows.Forms.CheckBox()
        Me.cbProConf = New System.Windows.Forms.CheckBox()
        Me.cbPreGab = New System.Windows.Forms.CheckBox()
        Me.cbPhenConf = New System.Windows.Forms.CheckBox()
        Me.cbOxyConf = New System.Windows.Forms.CheckBox()
        Me.cbOpiAnal = New System.Windows.Forms.CheckBox()
        Me.cbOpiConf = New System.Windows.Forms.CheckBox()
        Me.cbNicConf = New System.Windows.Forms.CheckBox()
        Me.cbMethy = New System.Windows.Forms.CheckBox()
        Me.cbMethAmpConf = New System.Windows.Forms.CheckBox()
        Me.cbMtdConf = New System.Windows.Forms.CheckBox()
        Me.cbHeroConf = New System.Windows.Forms.CheckBox()
        Me.cbGab = New System.Windows.Forms.CheckBox()
        Me.cbFent = New System.Windows.Forms.CheckBox()
        Me.cbCOCConf = New System.Windows.Forms.CheckBox()
        Me.cbTHCConf = New System.Windows.Forms.CheckBox()
        Me.cbBupConf = New System.Windows.Forms.CheckBox()
        Me.cbBenzConf = New System.Windows.Forms.CheckBox()
        Me.cbBarbConf = New System.Windows.Forms.CheckBox()
        Me.cbAntiEp = New System.Windows.Forms.CheckBox()
        Me.cbAD = New System.Windows.Forms.CheckBox()
        Me.cbADTri = New System.Windows.Forms.CheckBox()
        Me.cbADSer = New System.Windows.Forms.CheckBox()
        Me.cbAmpConf = New System.Windows.Forms.CheckBox()
        Me.cbAlcConf = New System.Windows.Forms.CheckBox()
        Me.btnOrd = New System.Windows.Forms.Button()
        Me.cbOpiScr = New System.Windows.Forms.CheckBox()
        Me.cbAlcScr = New System.Windows.Forms.CheckBox()
        Me.cbAmpScr = New System.Windows.Forms.CheckBox()
        Me.cbBarbScr = New System.Windows.Forms.CheckBox()
        Me.cbBenzScr = New System.Windows.Forms.CheckBox()
        Me.cbBupScr = New System.Windows.Forms.CheckBox()
        Me.cbTHCScr = New System.Windows.Forms.CheckBox()
        Me.cbCOCScr = New System.Windows.Forms.CheckBox()
        Me.cbHeroScr = New System.Windows.Forms.CheckBox()
        Me.cbMtdScr = New System.Windows.Forms.CheckBox()
        Me.cbMethAmpScr = New System.Windows.Forms.CheckBox()
        Me.cbNicScr = New System.Windows.Forms.CheckBox()
        Me.cbOxyScr = New System.Windows.Forms.CheckBox()
        Me.cbPhenScr = New System.Windows.Forms.CheckBox()
        Me.cbProScr = New System.Windows.Forms.CheckBox()
        Me.cbSkelScr = New System.Windows.Forms.CheckBox()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.cbXREF = New System.Windows.Forms.CheckBox()
        Me.cbSVT = New System.Windows.Forms.CheckBox()
        Me.cbChiral = New System.Windows.Forms.CheckBox()
        Me.cbAlk = New System.Windows.Forms.CheckBox()
        Me.cbLevo = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'cbAntiPys
        '
        Me.cbAntiPys.AutoSize = True
        Me.cbAntiPys.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntiPys.Location = New System.Drawing.Point(61, 294)
        Me.cbAntiPys.Name = "cbAntiPys"
        Me.cbAntiPys.Size = New System.Drawing.Size(169, 27)
        Me.cbAntiPys.TabIndex = 105
        Me.cbAntiPys.Text = "Antipsychotics"
        Me.cbAntiPys.UseVisualStyleBackColor = True
        '
        'cbTram
        '
        Me.cbTram.AutoSize = True
        Me.cbTram.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTram.Location = New System.Drawing.Point(794, 344)
        Me.cbTram.Name = "cbTram"
        Me.cbTram.Size = New System.Drawing.Size(119, 27)
        Me.cbTram.TabIndex = 102
        Me.cbTram.Text = "Tramadol"
        Me.cbTram.UseVisualStyleBackColor = True
        '
        'cbTap
        '
        Me.cbTap.AutoSize = True
        Me.cbTap.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTap.Location = New System.Drawing.Point(794, 313)
        Me.cbTap.Name = "cbTap"
        Me.cbTap.Size = New System.Drawing.Size(136, 27)
        Me.cbTap.TabIndex = 101
        Me.cbTap.Text = "Tapentadol"
        Me.cbTap.UseVisualStyleBackColor = True
        '
        'cbSkelConf
        '
        Me.cbSkelConf.AutoSize = True
        Me.cbSkelConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSkelConf.Location = New System.Drawing.Point(794, 282)
        Me.cbSkelConf.Name = "cbSkelConf"
        Me.cbSkelConf.Size = New System.Drawing.Size(277, 27)
        Me.cbSkelConf.TabIndex = 100
        Me.cbSkelConf.Text = "Skeletal Muscle Relaxants"
        Me.cbSkelConf.UseVisualStyleBackColor = True
        '
        'cbSedHyp
        '
        Me.cbSedHyp.AutoSize = True
        Me.cbSedHyp.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSedHyp.Location = New System.Drawing.Point(794, 251)
        Me.cbSedHyp.Name = "cbSedHyp"
        Me.cbSedHyp.Size = New System.Drawing.Size(213, 27)
        Me.cbSedHyp.TabIndex = 99
        Me.cbSedHyp.Text = "Sedative Hypnotics"
        Me.cbSedHyp.UseVisualStyleBackColor = True
        '
        'cbProConf
        '
        Me.cbProConf.AutoSize = True
        Me.cbProConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbProConf.Location = New System.Drawing.Point(794, 220)
        Me.cbProConf.Name = "cbProConf"
        Me.cbProConf.Size = New System.Drawing.Size(165, 27)
        Me.cbProConf.TabIndex = 98
        Me.cbProConf.Text = "Propoxyphene"
        Me.cbProConf.UseVisualStyleBackColor = True
        '
        'cbPreGab
        '
        Me.cbPreGab.AutoSize = True
        Me.cbPreGab.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPreGab.Location = New System.Drawing.Point(794, 189)
        Me.cbPreGab.Name = "cbPreGab"
        Me.cbPreGab.Size = New System.Drawing.Size(130, 27)
        Me.cbPreGab.TabIndex = 97
        Me.cbPreGab.Text = "Pregabalin"
        Me.cbPreGab.UseVisualStyleBackColor = True
        '
        'cbPhenConf
        '
        Me.cbPhenConf.AutoSize = True
        Me.cbPhenConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPhenConf.Location = New System.Drawing.Point(794, 158)
        Me.cbPhenConf.Name = "cbPhenConf"
        Me.cbPhenConf.Size = New System.Drawing.Size(160, 27)
        Me.cbPhenConf.TabIndex = 96
        Me.cbPhenConf.Text = "Phencyclidine"
        Me.cbPhenConf.UseVisualStyleBackColor = True
        '
        'cbOxyConf
        '
        Me.cbOxyConf.AutoSize = True
        Me.cbOxyConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOxyConf.Location = New System.Drawing.Point(794, 127)
        Me.cbOxyConf.Name = "cbOxyConf"
        Me.cbOxyConf.Size = New System.Drawing.Size(135, 27)
        Me.cbOxyConf.TabIndex = 95
        Me.cbOxyConf.Text = "Oxycodone"
        Me.cbOxyConf.UseVisualStyleBackColor = True
        '
        'cbOpiAnal
        '
        Me.cbOpiAnal.AutoSize = True
        Me.cbOpiAnal.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOpiAnal.Location = New System.Drawing.Point(794, 96)
        Me.cbOpiAnal.Name = "cbOpiAnal"
        Me.cbOpiAnal.Size = New System.Drawing.Size(317, 27)
        Me.cbOpiAnal.TabIndex = 94
        Me.cbOpiAnal.Text = "Opioids and Opiate Analogues"
        Me.cbOpiAnal.UseVisualStyleBackColor = True
        '
        'cbOpiConf
        '
        Me.cbOpiConf.AutoSize = True
        Me.cbOpiConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOpiConf.Location = New System.Drawing.Point(794, 65)
        Me.cbOpiConf.Name = "cbOpiConf"
        Me.cbOpiConf.Size = New System.Drawing.Size(102, 27)
        Me.cbOpiConf.TabIndex = 93
        Me.cbOpiConf.Text = "Opiates"
        Me.cbOpiConf.UseVisualStyleBackColor = True
        '
        'cbNicConf
        '
        Me.cbNicConf.AutoSize = True
        Me.cbNicConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbNicConf.Location = New System.Drawing.Point(794, 36)
        Me.cbNicConf.Name = "cbNicConf"
        Me.cbNicConf.Size = New System.Drawing.Size(109, 27)
        Me.cbNicConf.TabIndex = 92
        Me.cbNicConf.Text = "Nicotine"
        Me.cbNicConf.UseVisualStyleBackColor = True
        '
        'cbMethy
        '
        Me.cbMethy.AutoSize = True
        Me.cbMethy.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMethy.Location = New System.Drawing.Point(416, 354)
        Me.cbMethy.Name = "cbMethy"
        Me.cbMethy.Size = New System.Drawing.Size(189, 27)
        Me.cbMethy.TabIndex = 91
        Me.cbMethy.Text = "Methylphenidate"
        Me.cbMethy.UseVisualStyleBackColor = True
        '
        'cbMethAmpConf
        '
        Me.cbMethAmpConf.AutoSize = True
        Me.cbMethAmpConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMethAmpConf.Location = New System.Drawing.Point(416, 322)
        Me.cbMethAmpConf.Name = "cbMethAmpConf"
        Me.cbMethAmpConf.Size = New System.Drawing.Size(330, 27)
        Me.cbMethAmpConf.TabIndex = 90
        Me.cbMethAmpConf.Text = "Methylenedioxy-Amphetamines"
        Me.cbMethAmpConf.UseVisualStyleBackColor = True
        '
        'cbMtdConf
        '
        Me.cbMtdConf.AutoSize = True
        Me.cbMtdConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMtdConf.Location = New System.Drawing.Point(416, 290)
        Me.cbMtdConf.Name = "cbMtdConf"
        Me.cbMtdConf.Size = New System.Drawing.Size(135, 27)
        Me.cbMtdConf.TabIndex = 89
        Me.cbMtdConf.Text = "Methadone"
        Me.cbMtdConf.UseVisualStyleBackColor = True
        '
        'cbHeroConf
        '
        Me.cbHeroConf.AutoSize = True
        Me.cbHeroConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbHeroConf.Location = New System.Drawing.Point(416, 258)
        Me.cbHeroConf.Name = "cbHeroConf"
        Me.cbHeroConf.Size = New System.Drawing.Size(201, 27)
        Me.cbHeroConf.TabIndex = 88
        Me.cbHeroConf.Text = "Heroin Metabolite"
        Me.cbHeroConf.UseVisualStyleBackColor = True
        '
        'cbGab
        '
        Me.cbGab.AutoSize = True
        Me.cbGab.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbGab.Location = New System.Drawing.Point(416, 226)
        Me.cbGab.Name = "cbGab"
        Me.cbGab.Size = New System.Drawing.Size(252, 27)
        Me.cbGab.TabIndex = 87
        Me.cbGab.Text = "Gabapentin, Non-Blood"
        Me.cbGab.UseVisualStyleBackColor = True
        '
        'cbFent
        '
        Me.cbFent.AutoSize = True
        Me.cbFent.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbFent.Location = New System.Drawing.Point(416, 194)
        Me.cbFent.Name = "cbFent"
        Me.cbFent.Size = New System.Drawing.Size(121, 27)
        Me.cbFent.TabIndex = 86
        Me.cbFent.Text = "Fentanyls"
        Me.cbFent.UseVisualStyleBackColor = True
        '
        'cbCOCConf
        '
        Me.cbCOCConf.AutoSize = True
        Me.cbCOCConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCOCConf.Location = New System.Drawing.Point(416, 162)
        Me.cbCOCConf.Name = "cbCOCConf"
        Me.cbCOCConf.Size = New System.Drawing.Size(104, 27)
        Me.cbCOCConf.TabIndex = 85
        Me.cbCOCConf.Text = "Cocaine"
        Me.cbCOCConf.UseVisualStyleBackColor = True
        '
        'cbTHCConf
        '
        Me.cbTHCConf.AutoSize = True
        Me.cbTHCConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTHCConf.Location = New System.Drawing.Point(416, 130)
        Me.cbTHCConf.Name = "cbTHCConf"
        Me.cbTHCConf.Size = New System.Drawing.Size(241, 27)
        Me.cbTHCConf.TabIndex = 84
        Me.cbTHCConf.Text = "Cannabinoids, Natural"
        Me.cbTHCConf.UseVisualStyleBackColor = True
        '
        'cbBupConf
        '
        Me.cbBupConf.AutoSize = True
        Me.cbBupConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBupConf.Location = New System.Drawing.Point(416, 98)
        Me.cbBupConf.Name = "cbBupConf"
        Me.cbBupConf.Size = New System.Drawing.Size(170, 27)
        Me.cbBupConf.TabIndex = 83
        Me.cbBupConf.Text = "Buprenorphine"
        Me.cbBupConf.UseVisualStyleBackColor = True
        '
        'cbBenzConf
        '
        Me.cbBenzConf.AutoSize = True
        Me.cbBenzConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBenzConf.Location = New System.Drawing.Point(416, 66)
        Me.cbBenzConf.Name = "cbBenzConf"
        Me.cbBenzConf.Size = New System.Drawing.Size(188, 27)
        Me.cbBenzConf.TabIndex = 82
        Me.cbBenzConf.Text = "Benzodiazepines"
        Me.cbBenzConf.UseVisualStyleBackColor = True
        '
        'cbBarbConf
        '
        Me.cbBarbConf.AutoSize = True
        Me.cbBarbConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBarbConf.Location = New System.Drawing.Point(416, 36)
        Me.cbBarbConf.Name = "cbBarbConf"
        Me.cbBarbConf.Size = New System.Drawing.Size(147, 27)
        Me.cbBarbConf.TabIndex = 81
        Me.cbBarbConf.Text = "Barbiturates"
        Me.cbBarbConf.UseVisualStyleBackColor = True
        '
        'cbAntiEp
        '
        Me.cbAntiEp.AutoSize = True
        Me.cbAntiEp.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntiEp.Location = New System.Drawing.Point(61, 260)
        Me.cbAntiEp.Name = "cbAntiEp"
        Me.cbAntiEp.Size = New System.Drawing.Size(160, 27)
        Me.cbAntiEp.TabIndex = 80
        Me.cbAntiEp.Text = "Antiepileptics"
        Me.cbAntiEp.UseVisualStyleBackColor = True
        '
        'cbAD
        '
        Me.cbAD.AutoSize = True
        Me.cbAD.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAD.Location = New System.Drawing.Point(61, 226)
        Me.cbAD.Name = "cbAD"
        Me.cbAD.Size = New System.Drawing.Size(183, 27)
        Me.cbAD.TabIndex = 79
        Me.cbAD.Text = "Antidepressants"
        Me.cbAD.UseVisualStyleBackColor = True
        '
        'cbADTri
        '
        Me.cbADTri.AutoSize = True
        Me.cbADTri.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbADTri.Location = New System.Drawing.Point(61, 192)
        Me.cbADTri.Name = "cbADTri"
        Me.cbADTri.Size = New System.Drawing.Size(274, 27)
        Me.cbADTri.TabIndex = 78
        Me.cbADTri.Text = "Antidepressants, Tricyclic"
        Me.cbADTri.UseVisualStyleBackColor = True
        '
        'cbADSer
        '
        Me.cbADSer.AutoSize = True
        Me.cbADSer.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbADSer.Location = New System.Drawing.Point(61, 158)
        Me.cbADSer.Name = "cbADSer"
        Me.cbADSer.Size = New System.Drawing.Size(317, 27)
        Me.cbADSer.TabIndex = 77
        Me.cbADSer.Text = "Antidepressants, Serotonergic"
        Me.cbADSer.UseVisualStyleBackColor = True
        '
        'cbAmpConf
        '
        Me.cbAmpConf.AutoSize = True
        Me.cbAmpConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAmpConf.Location = New System.Drawing.Point(59, 98)
        Me.cbAmpConf.Name = "cbAmpConf"
        Me.cbAmpConf.Size = New System.Drawing.Size(171, 27)
        Me.cbAmpConf.TabIndex = 76
        Me.cbAmpConf.Text = "Amphetamines"
        Me.cbAmpConf.UseVisualStyleBackColor = True
        '
        'cbAlcConf
        '
        Me.cbAlcConf.AutoSize = True
        Me.cbAlcConf.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAlcConf.Location = New System.Drawing.Point(60, 36)
        Me.cbAlcConf.Name = "cbAlcConf"
        Me.cbAlcConf.Size = New System.Drawing.Size(214, 27)
        Me.cbAlcConf.TabIndex = 75
        Me.cbAlcConf.Text = "Alcohol Biomarkers"
        Me.cbAlcConf.UseVisualStyleBackColor = True
        '
        'btnOrd
        '
        Me.btnOrd.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOrd.Location = New System.Drawing.Point(1003, 433)
        Me.btnOrd.Name = "btnOrd"
        Me.btnOrd.Size = New System.Drawing.Size(97, 47)
        Me.btnOrd.TabIndex = 106
        Me.btnOrd.Text = "Order"
        Me.btnOrd.UseVisualStyleBackColor = True
        '
        'cbOpiScr
        '
        Me.cbOpiScr.AutoSize = True
        Me.cbOpiScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOpiScr.Location = New System.Drawing.Point(763, 71)
        Me.cbOpiScr.Name = "cbOpiScr"
        Me.cbOpiScr.Size = New System.Drawing.Size(15, 14)
        Me.cbOpiScr.TabIndex = 107
        Me.cbOpiScr.UseVisualStyleBackColor = True
        '
        'cbAlcScr
        '
        Me.cbAlcScr.AutoSize = True
        Me.cbAlcScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAlcScr.Location = New System.Drawing.Point(26, 42)
        Me.cbAlcScr.Name = "cbAlcScr"
        Me.cbAlcScr.Size = New System.Drawing.Size(15, 14)
        Me.cbAlcScr.TabIndex = 108
        Me.cbAlcScr.UseVisualStyleBackColor = True
        '
        'cbAmpScr
        '
        Me.cbAmpScr.AutoSize = True
        Me.cbAmpScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAmpScr.Location = New System.Drawing.Point(25, 104)
        Me.cbAmpScr.Name = "cbAmpScr"
        Me.cbAmpScr.Size = New System.Drawing.Size(15, 14)
        Me.cbAmpScr.TabIndex = 109
        Me.cbAmpScr.UseVisualStyleBackColor = True
        '
        'cbBarbScr
        '
        Me.cbBarbScr.AutoSize = True
        Me.cbBarbScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBarbScr.Location = New System.Drawing.Point(386, 43)
        Me.cbBarbScr.Name = "cbBarbScr"
        Me.cbBarbScr.Size = New System.Drawing.Size(15, 14)
        Me.cbBarbScr.TabIndex = 110
        Me.cbBarbScr.UseVisualStyleBackColor = True
        '
        'cbBenzScr
        '
        Me.cbBenzScr.AutoSize = True
        Me.cbBenzScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBenzScr.Location = New System.Drawing.Point(386, 72)
        Me.cbBenzScr.Name = "cbBenzScr"
        Me.cbBenzScr.Size = New System.Drawing.Size(15, 14)
        Me.cbBenzScr.TabIndex = 111
        Me.cbBenzScr.UseVisualStyleBackColor = True
        '
        'cbBupScr
        '
        Me.cbBupScr.AutoSize = True
        Me.cbBupScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBupScr.Location = New System.Drawing.Point(386, 101)
        Me.cbBupScr.Name = "cbBupScr"
        Me.cbBupScr.Size = New System.Drawing.Size(15, 14)
        Me.cbBupScr.TabIndex = 112
        Me.cbBupScr.UseVisualStyleBackColor = True
        '
        'cbTHCScr
        '
        Me.cbTHCScr.AutoSize = True
        Me.cbTHCScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTHCScr.Location = New System.Drawing.Point(386, 134)
        Me.cbTHCScr.Name = "cbTHCScr"
        Me.cbTHCScr.Size = New System.Drawing.Size(15, 14)
        Me.cbTHCScr.TabIndex = 113
        Me.cbTHCScr.UseVisualStyleBackColor = True
        '
        'cbCOCScr
        '
        Me.cbCOCScr.AutoSize = True
        Me.cbCOCScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCOCScr.Location = New System.Drawing.Point(386, 168)
        Me.cbCOCScr.Name = "cbCOCScr"
        Me.cbCOCScr.Size = New System.Drawing.Size(15, 14)
        Me.cbCOCScr.TabIndex = 114
        Me.cbCOCScr.UseVisualStyleBackColor = True
        '
        'cbHeroScr
        '
        Me.cbHeroScr.AutoSize = True
        Me.cbHeroScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbHeroScr.Location = New System.Drawing.Point(386, 264)
        Me.cbHeroScr.Name = "cbHeroScr"
        Me.cbHeroScr.Size = New System.Drawing.Size(15, 14)
        Me.cbHeroScr.TabIndex = 115
        Me.cbHeroScr.UseVisualStyleBackColor = True
        '
        'cbMtdScr
        '
        Me.cbMtdScr.AutoSize = True
        Me.cbMtdScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMtdScr.Location = New System.Drawing.Point(386, 296)
        Me.cbMtdScr.Name = "cbMtdScr"
        Me.cbMtdScr.Size = New System.Drawing.Size(15, 14)
        Me.cbMtdScr.TabIndex = 116
        Me.cbMtdScr.UseVisualStyleBackColor = True
        '
        'cbMethAmpScr
        '
        Me.cbMethAmpScr.AutoSize = True
        Me.cbMethAmpScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMethAmpScr.Location = New System.Drawing.Point(386, 328)
        Me.cbMethAmpScr.Name = "cbMethAmpScr"
        Me.cbMethAmpScr.Size = New System.Drawing.Size(15, 14)
        Me.cbMethAmpScr.TabIndex = 117
        Me.cbMethAmpScr.UseVisualStyleBackColor = True
        '
        'cbNicScr
        '
        Me.cbNicScr.AutoSize = True
        Me.cbNicScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbNicScr.Location = New System.Drawing.Point(763, 42)
        Me.cbNicScr.Name = "cbNicScr"
        Me.cbNicScr.Size = New System.Drawing.Size(15, 14)
        Me.cbNicScr.TabIndex = 118
        Me.cbNicScr.UseVisualStyleBackColor = True
        '
        'cbOxyScr
        '
        Me.cbOxyScr.AutoSize = True
        Me.cbOxyScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOxyScr.Location = New System.Drawing.Point(763, 133)
        Me.cbOxyScr.Name = "cbOxyScr"
        Me.cbOxyScr.Size = New System.Drawing.Size(15, 14)
        Me.cbOxyScr.TabIndex = 119
        Me.cbOxyScr.UseVisualStyleBackColor = True
        '
        'cbPhenScr
        '
        Me.cbPhenScr.AutoSize = True
        Me.cbPhenScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPhenScr.Location = New System.Drawing.Point(763, 164)
        Me.cbPhenScr.Name = "cbPhenScr"
        Me.cbPhenScr.Size = New System.Drawing.Size(15, 14)
        Me.cbPhenScr.TabIndex = 120
        Me.cbPhenScr.UseVisualStyleBackColor = True
        '
        'cbProScr
        '
        Me.cbProScr.AutoSize = True
        Me.cbProScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbProScr.Location = New System.Drawing.Point(763, 226)
        Me.cbProScr.Name = "cbProScr"
        Me.cbProScr.Size = New System.Drawing.Size(15, 14)
        Me.cbProScr.TabIndex = 121
        Me.cbProScr.UseVisualStyleBackColor = True
        '
        'cbSkelScr
        '
        Me.cbSkelScr.AutoSize = True
        Me.cbSkelScr.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSkelScr.Location = New System.Drawing.Point(763, 288)
        Me.cbSkelScr.Name = "cbSkelScr"
        Me.cbSkelScr.Size = New System.Drawing.Size(15, 14)
        Me.cbSkelScr.TabIndex = 122
        Me.cbSkelScr.UseVisualStyleBackColor = True
        '
        'lblHeader
        '
        Me.lblHeader.AutoSize = True
        Me.lblHeader.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeader.Location = New System.Drawing.Point(22, 11)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(19, 19)
        Me.lblHeader.TabIndex = 123
        Me.lblHeader.Text = "S"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(382, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 19)
        Me.Label1.TabIndex = 124
        Me.Label1.Text = "S"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(762, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 19)
        Me.Label2.TabIndex = 125
        Me.Label2.Text = "S"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(57, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 19)
        Me.Label3.TabIndex = 126
        Me.Label3.Text = "C"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(412, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 19)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "C"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(791, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 19)
        Me.Label5.TabIndex = 128
        Me.Label5.Text = "C"
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(874, 433)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(106, 47)
        Me.btnClear.TabIndex = 129
        Me.btnClear.Text = "Clear All Selections"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'cbXREF
        '
        Me.cbXREF.AutoSize = True
        Me.cbXREF.BackColor = System.Drawing.SystemColors.Highlight
        Me.cbXREF.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbXREF.Location = New System.Drawing.Point(49, 368)
        Me.cbXREF.Name = "cbXREF"
        Me.cbXREF.Size = New System.Drawing.Size(79, 27)
        Me.cbXREF.TabIndex = 130
        Me.cbXREF.Text = "XREF"
        Me.cbXREF.UseVisualStyleBackColor = False
        '
        'cbSVT
        '
        Me.cbSVT.AutoSize = True
        Me.cbSVT.BackColor = System.Drawing.SystemColors.Highlight
        Me.cbSVT.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSVT.Location = New System.Drawing.Point(152, 368)
        Me.cbSVT.Name = "cbSVT"
        Me.cbSVT.Size = New System.Drawing.Size(66, 27)
        Me.cbSVT.TabIndex = 131
        Me.cbSVT.Text = "SVT"
        Me.cbSVT.UseVisualStyleBackColor = False
        '
        'cbChiral
        '
        Me.cbChiral.AutoSize = True
        Me.cbChiral.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbChiral.Location = New System.Drawing.Point(60, 128)
        Me.cbChiral.Name = "cbChiral"
        Me.cbChiral.Size = New System.Drawing.Size(241, 27)
        Me.cbChiral.TabIndex = 132
        Me.cbChiral.Text = "Stereoisomer Analysis"
        Me.cbChiral.UseVisualStyleBackColor = True
        '
        'cbAlk
        '
        Me.cbAlk.AutoSize = True
        Me.cbAlk.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAlk.Location = New System.Drawing.Point(59, 69)
        Me.cbAlk.Name = "cbAlk"
        Me.cbAlk.Size = New System.Drawing.Size(239, 27)
        Me.cbAlk.TabIndex = 133
        Me.cbAlk.Text = "Alkaloids, Unspecified"
        Me.cbAlk.UseVisualStyleBackColor = True
        '
        'cbLevo
        '
        Me.cbLevo.AutoSize = True
        Me.cbLevo.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbLevo.Location = New System.Drawing.Point(794, 377)
        Me.cbLevo.Name = "cbLevo"
        Me.cbLevo.Size = New System.Drawing.Size(147, 27)
        Me.cbLevo.TabIndex = 134
        Me.cbLevo.Text = "Levorphanol"
        Me.cbLevo.UseVisualStyleBackColor = True
        '
        'frmOEGrid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1123, 492)
        Me.Controls.Add(Me.cbLevo)
        Me.Controls.Add(Me.cbAlk)
        Me.Controls.Add(Me.cbChiral)
        Me.Controls.Add(Me.cbSVT)
        Me.Controls.Add(Me.cbXREF)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblHeader)
        Me.Controls.Add(Me.cbSkelScr)
        Me.Controls.Add(Me.cbProScr)
        Me.Controls.Add(Me.cbPhenScr)
        Me.Controls.Add(Me.cbOxyScr)
        Me.Controls.Add(Me.cbNicScr)
        Me.Controls.Add(Me.cbMethAmpScr)
        Me.Controls.Add(Me.cbMtdScr)
        Me.Controls.Add(Me.cbHeroScr)
        Me.Controls.Add(Me.cbCOCScr)
        Me.Controls.Add(Me.cbTHCScr)
        Me.Controls.Add(Me.cbBupScr)
        Me.Controls.Add(Me.cbBenzScr)
        Me.Controls.Add(Me.cbBarbScr)
        Me.Controls.Add(Me.cbAmpScr)
        Me.Controls.Add(Me.cbAlcScr)
        Me.Controls.Add(Me.cbOpiScr)
        Me.Controls.Add(Me.btnOrd)
        Me.Controls.Add(Me.cbAntiPys)
        Me.Controls.Add(Me.cbTram)
        Me.Controls.Add(Me.cbTap)
        Me.Controls.Add(Me.cbSkelConf)
        Me.Controls.Add(Me.cbSedHyp)
        Me.Controls.Add(Me.cbProConf)
        Me.Controls.Add(Me.cbPreGab)
        Me.Controls.Add(Me.cbPhenConf)
        Me.Controls.Add(Me.cbOxyConf)
        Me.Controls.Add(Me.cbOpiAnal)
        Me.Controls.Add(Me.cbOpiConf)
        Me.Controls.Add(Me.cbNicConf)
        Me.Controls.Add(Me.cbMethy)
        Me.Controls.Add(Me.cbMethAmpConf)
        Me.Controls.Add(Me.cbMtdConf)
        Me.Controls.Add(Me.cbHeroConf)
        Me.Controls.Add(Me.cbGab)
        Me.Controls.Add(Me.cbFent)
        Me.Controls.Add(Me.cbCOCConf)
        Me.Controls.Add(Me.cbTHCConf)
        Me.Controls.Add(Me.cbBupConf)
        Me.Controls.Add(Me.cbBenzConf)
        Me.Controls.Add(Me.cbBarbConf)
        Me.Controls.Add(Me.cbAntiEp)
        Me.Controls.Add(Me.cbAD)
        Me.Controls.Add(Me.cbADTri)
        Me.Controls.Add(Me.cbADSer)
        Me.Controls.Add(Me.cbAmpConf)
        Me.Controls.Add(Me.cbAlcConf)
        Me.Name = "frmOEGrid"
        Me.Text = "Grid Order Entry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbAntiPys As CheckBox
    Friend WithEvents cbTram As CheckBox
    Friend WithEvents cbTap As CheckBox
    Friend WithEvents cbSkelConf As CheckBox
    Friend WithEvents cbSedHyp As CheckBox
    Friend WithEvents cbProConf As CheckBox
    Friend WithEvents cbPreGab As CheckBox
    Friend WithEvents cbPhenConf As CheckBox
    Friend WithEvents cbOxyConf As CheckBox
    Friend WithEvents cbOpiAnal As CheckBox
    Friend WithEvents cbOpiConf As CheckBox
    Friend WithEvents cbNicConf As CheckBox
    Friend WithEvents cbMethy As CheckBox
    Friend WithEvents cbMethAmpConf As CheckBox
    Friend WithEvents cbMtdConf As CheckBox
    Friend WithEvents cbHeroConf As CheckBox
    Friend WithEvents cbGab As CheckBox
    Friend WithEvents cbFent As CheckBox
    Friend WithEvents cbCOCConf As CheckBox
    Friend WithEvents cbTHCConf As CheckBox
    Friend WithEvents cbBupConf As CheckBox
    Friend WithEvents cbBenzConf As CheckBox
    Friend WithEvents cbBarbConf As CheckBox
    Friend WithEvents cbAntiEp As CheckBox
    Friend WithEvents cbAD As CheckBox
    Friend WithEvents cbADTri As CheckBox
    Friend WithEvents cbADSer As CheckBox
    Friend WithEvents cbAmpConf As CheckBox
    Friend WithEvents cbAlcConf As CheckBox
    Friend WithEvents btnOrd As Button
    Friend WithEvents cbOpiScr As CheckBox
    Friend WithEvents cbAlcScr As CheckBox
    Friend WithEvents cbAmpScr As CheckBox
    Friend WithEvents cbBarbScr As CheckBox
    Friend WithEvents cbBenzScr As CheckBox
    Friend WithEvents cbBupScr As CheckBox
    Friend WithEvents cbTHCScr As CheckBox
    Friend WithEvents cbCOCScr As CheckBox
    Friend WithEvents cbHeroScr As CheckBox
    Friend WithEvents cbMtdScr As CheckBox
    Friend WithEvents cbMethAmpScr As CheckBox
    Friend WithEvents cbNicScr As CheckBox
    Friend WithEvents cbOxyScr As CheckBox
    Friend WithEvents cbPhenScr As CheckBox
    Friend WithEvents cbProScr As CheckBox
    Friend WithEvents cbSkelScr As CheckBox
    Friend WithEvents lblHeader As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btnClear As Button
    Friend WithEvents cbXREF As CheckBox
    Friend WithEvents cbSVT As CheckBox
    Friend WithEvents cbChiral As CheckBox
    Friend WithEvents cbAlk As CheckBox
    Friend WithEvents cbLevo As CheckBox
End Class
