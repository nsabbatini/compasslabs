﻿Public Class frmOEGridInq
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmOEGridInq_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbAD.Checked = False
        cbADSer.Checked = False
        cbADTri.Checked = False
        cbAlcConf.Checked = False
        cbAlcScr.Checked = False
        cbAlk.Checked = False
        cbAmpConf.Checked = False
        cbAmpScr.Checked = False
        cbAntiEp.Checked = False
        cbAntiPys.Checked = False
        cbBarbConf.Checked = False
        cbBarbScr.Checked = False
        cbBenzConf.Checked = False
        cbBenzScr.Checked = False
        cbBupConf.Checked = False
        cbBupScr.Checked = False
        cbCOCConf.Checked = False
        cbCOCScr.Checked = False
        cbFent.Checked = False
        cbGab.Checked = False
        cbHeroConf.Checked = False
        cbHeroScr.Checked = False
        cbMethAmpConf.Checked = False
        cbMethAmpScr.Checked = False
        cbMethy.Checked = False
        cbMtdConf.Checked = False
        cbMtdScr.Checked = False
        cbNicConf.Checked = False
        cbNicScr.Checked = False
        cbOpiAnal.Checked = False
        cbOpiConf.Checked = False
        cbOpiScr.Checked = False
        cbOxyConf.Checked = False
        cbOxyScr.Checked = False
        cbPhenConf.Checked = False
        cbPhenScr.Checked = False
        cbPreGab.Checked = False
        cbProConf.Checked = False
        cbProScr.Checked = False
        cbSedHyp.Checked = False
        cbSkelConf.Checked = False
        cbSkelScr.Checked = False
        cbTap.Checked = False
        cbTHCConf.Checked = False
        cbTHCScr.Checked = False
        cbTram.Checked = False
        cbLevo.Checked = False
        cbXREF.Checked = False
        cbSVT.Checked = False
        cbChiral.Checked = False
        For i = 0 To SpcInq.lstOC.Items.Count - 1
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "350000" Then cbAlcConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "351000" Then cbAlcScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "395000" Then cbAlk.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "352000" Then cbAmpConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "353000" Then cbAmpScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "309010" Then cbChiral.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "301027" Then cbChiral.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "354000" Then cbADSer.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "355000" Then cbADTri.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "356000" Then cbAD.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "357000" Then cbAntiEp.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "358000" Then cbAntiPys.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "359000" Then cbBarbConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "360000" Then cbBarbScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "361000" Then cbBenzConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "362000" Then cbBenzScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "363000" Then cbBupConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "364000" Then cbBupScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "365000" Then cbTHCConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "366000" Then cbTHCScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "367000" Then cbCOCConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "368000" Then cbCOCScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "369000" Then cbFent.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "370000" Then cbGab.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "371000" Then cbHeroConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "372000" Then cbHeroScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "373000" Then cbMtdConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "374000" Then cbMtdScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "375000" Then cbMethAmpConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "376000" Then cbMethAmpScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "377000" Then cbMethy.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "378000" Then cbNicConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "379000" Then cbNicScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "380000" Then cbOpiConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "381000" Then cbOpiScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "382000" Then cbOpiAnal.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "383000" Then cbOxyConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "384000" Then cbOxyScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "385000" Then cbPhenConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "386000" Then cbPhenScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "387000" Then cbPreGab.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "388000" Then cbProConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "389000" Then cbProScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "390000" Then cbSedHyp.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "391000" Then cbSkelConf.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "392000" Then cbSkelScr.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "393000" Then cbTap.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "394000" Then cbTram.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "308130" Then cbLevo.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "800000" Then cbXREF.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "209000" Then cbSVT.Checked = True
        Next i

    End Sub

End Class