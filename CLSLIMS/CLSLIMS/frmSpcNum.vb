﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Public Class frmSpcNum

    Private Sub frmSpcNum_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" And CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "JONATHON.MORGAN" And CLSLIMS.UID <> "STEVE.ALEXANDER" And CLSLIMS.UID <> "KELLY.ELMORE" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpStart.Format = DateTimePickerFormat.Custom
        dtpStart.CustomFormat = "MM/dd/yyyy"
        dtpStart.Text = DateAdd(DateInterval.Month, -1, Now)
        dtpEnd.Format = DateTimePickerFormat.Custom
        dtpEnd.CustomFormat = "MM/dd/yyyy"
        dtpEnd.Text = Now
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\SpecNums.csv", False)
        Dim Title As String = "", r As String = "", p As Integer = 0, SQL As String = "", Acct As String = "", PrevAcct As String = "", cnt As Integer = 1

        Title = "Specimen Numbers by Received Date"
        If cbEx.Checked Then Title = Title & " (Direct Bill excluded)"
        file.WriteLine(Title)
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Sel As String = "Select pspecno,substring(PSRCVDT,5,2)+'/'+substring(PSRCVDT,7,2)+'/'+substring(PSRCVDT,1,4) From pspec " &
            "Where psact=1 and substring(psrcvdt,1,8) between '" & CLSLIMS.Piece(dtpStart.Text, "/", 3) & CLSLIMS.Piece(dtpStart.Text, "/", 1) & CLSLIMS.Piece(dtpStart.Text, "/", 2) & "' And '" & CLSLIMS.Piece(dtpEnd.Text, "/", 3) & CLSLIMS.Piece(dtpEnd.Text, "/", 1) & CLSLIMS.Piece(dtpEnd.Text, "/", 2) & "'"
        If cbEx.Checked = True Then
            Sel = "Select pspecno,substring(PSRCVDT,5,2)+'/'+substring(PSRCVDT,7,2)+'/'+substring(PSRCVDT,1,4) From pspec a, pord b " &
            "Where psact=1 and substring(psrcvdt,1,8) between '" & CLSLIMS.Piece(dtpStart.Text, "/", 3) & CLSLIMS.Piece(dtpStart.Text, "/", 1) & CLSLIMS.Piece(dtpStart.Text, "/", 2) & "' And '" & CLSLIMS.Piece(dtpEnd.Text, "/", 3) & CLSLIMS.Piece(dtpEnd.Text, "/", 1) & CLSLIMS.Piece(dtpEnd.Text, "/", 2) & "' " &
            "And a.pord=b.pord and a.poefdt=b.poefdt and acct not in (select acct from xdirectbill)"
        End If
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            file.WriteLine(rs(0) & "," & "," & rs(1))
            cnt = cnt + 1
        Loop
        rs.Close()
        Conn.Close()
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\SpecNums.csv")
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class