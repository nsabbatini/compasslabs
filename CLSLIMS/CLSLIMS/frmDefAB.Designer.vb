﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDefAB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.gbSource = New System.Windows.Forms.GroupBox()
        Me.cbEye = New System.Windows.Forms.CheckBox()
        Me.cbSH = New System.Windows.Forms.CheckBox()
        Me.cbUrn = New System.Windows.Forms.CheckBox()
        Me.cbSSTI = New System.Windows.Forms.CheckBox()
        Me.cbResp = New System.Windows.Forms.CheckBox()
        Me.gbDel = New System.Windows.Forms.GroupBox()
        Me.cbOpth = New System.Windows.Forms.CheckBox()
        Me.cbPO = New System.Windows.Forms.CheckBox()
        Me.cbIM = New System.Windows.Forms.CheckBox()
        Me.cbIV = New System.Windows.Forms.CheckBox()
        Me.cmbCat = New System.Windows.Forms.ComboBox()
        Me.cmbSubCat = New System.Windows.Forms.ComboBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.cmbAB = New System.Windows.Forms.ComboBox()
        Me.lblNew = New System.Windows.Forms.Label()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.gbSource.SuspendLayout()
        Me.gbDel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Antibiotic:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Category:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 102)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 19)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "SubCategory:"
        '
        'gbSource
        '
        Me.gbSource.Controls.Add(Me.cbEye)
        Me.gbSource.Controls.Add(Me.cbSH)
        Me.gbSource.Controls.Add(Me.cbUrn)
        Me.gbSource.Controls.Add(Me.cbSSTI)
        Me.gbSource.Controls.Add(Me.cbResp)
        Me.gbSource.Location = New System.Drawing.Point(53, 161)
        Me.gbSource.Name = "gbSource"
        Me.gbSource.Size = New System.Drawing.Size(135, 181)
        Me.gbSource.TabIndex = 5
        Me.gbSource.TabStop = False
        Me.gbSource.Text = "Source(s):"
        '
        'cbEye
        '
        Me.cbEye.AutoSize = True
        Me.cbEye.Location = New System.Drawing.Point(15, 143)
        Me.cbEye.Name = "cbEye"
        Me.cbEye.Size = New System.Drawing.Size(57, 23)
        Me.cbEye.TabIndex = 8
        Me.cbEye.Text = "Eye"
        Me.cbEye.UseVisualStyleBackColor = True
        '
        'cbSH
        '
        Me.cbSH.AutoSize = True
        Me.cbSH.Location = New System.Drawing.Point(15, 114)
        Me.cbSH.Name = "cbSH"
        Me.cbSH.Size = New System.Drawing.Size(50, 23)
        Me.cbSH.TabIndex = 7
        Me.cbSH.Text = "SH"
        Me.cbSH.UseVisualStyleBackColor = True
        '
        'cbUrn
        '
        Me.cbUrn.AutoSize = True
        Me.cbUrn.Location = New System.Drawing.Point(15, 85)
        Me.cbUrn.Name = "cbUrn"
        Me.cbUrn.Size = New System.Drawing.Size(72, 23)
        Me.cbUrn.TabIndex = 6
        Me.cbUrn.Text = "Urine"
        Me.cbUrn.UseVisualStyleBackColor = True
        '
        'cbSSTI
        '
        Me.cbSSTI.AutoSize = True
        Me.cbSSTI.Location = New System.Drawing.Point(15, 56)
        Me.cbSSTI.Name = "cbSSTI"
        Me.cbSSTI.Size = New System.Drawing.Size(66, 23)
        Me.cbSSTI.TabIndex = 5
        Me.cbSSTI.Text = "SSTI"
        Me.cbSSTI.UseVisualStyleBackColor = True
        '
        'cbResp
        '
        Me.cbResp.AutoSize = True
        Me.cbResp.Location = New System.Drawing.Point(15, 27)
        Me.cbResp.Name = "cbResp"
        Me.cbResp.Size = New System.Drawing.Size(68, 23)
        Me.cbResp.TabIndex = 4
        Me.cbResp.Text = "Resp"
        Me.cbResp.UseVisualStyleBackColor = True
        '
        'gbDel
        '
        Me.gbDel.Controls.Add(Me.cbOpth)
        Me.gbDel.Controls.Add(Me.cbPO)
        Me.gbDel.Controls.Add(Me.cbIM)
        Me.gbDel.Controls.Add(Me.cbIV)
        Me.gbDel.Location = New System.Drawing.Point(238, 161)
        Me.gbDel.Name = "gbDel"
        Me.gbDel.Size = New System.Drawing.Size(202, 154)
        Me.gbDel.TabIndex = 6
        Me.gbDel.TabStop = False
        Me.gbDel.Text = "Delivery Method(s):"
        '
        'cbOpth
        '
        Me.cbOpth.AutoSize = True
        Me.cbOpth.Location = New System.Drawing.Point(16, 113)
        Me.cbOpth.Name = "cbOpth"
        Me.cbOpth.Size = New System.Drawing.Size(110, 23)
        Me.cbOpth.TabIndex = 12
        Me.cbOpth.Text = "Opthalmic"
        Me.cbOpth.UseVisualStyleBackColor = True
        '
        'cbPO
        '
        Me.cbPO.AutoSize = True
        Me.cbPO.Location = New System.Drawing.Point(16, 84)
        Me.cbPO.Name = "cbPO"
        Me.cbPO.Size = New System.Drawing.Size(51, 23)
        Me.cbPO.TabIndex = 11
        Me.cbPO.Text = "PO"
        Me.cbPO.UseVisualStyleBackColor = True
        '
        'cbIM
        '
        Me.cbIM.AutoSize = True
        Me.cbIM.Location = New System.Drawing.Point(16, 55)
        Me.cbIM.Name = "cbIM"
        Me.cbIM.Size = New System.Drawing.Size(49, 23)
        Me.cbIM.TabIndex = 10
        Me.cbIM.Text = "IM"
        Me.cbIM.UseVisualStyleBackColor = True
        '
        'cbIV
        '
        Me.cbIV.AutoSize = True
        Me.cbIV.Location = New System.Drawing.Point(16, 26)
        Me.cbIV.Name = "cbIV"
        Me.cbIV.Size = New System.Drawing.Size(46, 23)
        Me.cbIV.TabIndex = 9
        Me.cbIV.Text = "IV"
        Me.cbIV.UseVisualStyleBackColor = True
        '
        'cmbCat
        '
        Me.cmbCat.FormattingEnabled = True
        Me.cmbCat.Location = New System.Drawing.Point(131, 61)
        Me.cmbCat.Name = "cmbCat"
        Me.cmbCat.Size = New System.Drawing.Size(302, 27)
        Me.cmbCat.TabIndex = 2
        '
        'cmbSubCat
        '
        Me.cmbSubCat.FormattingEnabled = True
        Me.cmbSubCat.Location = New System.Drawing.Point(130, 99)
        Me.cmbSubCat.Name = "cmbSubCat"
        Me.cmbSubCat.Size = New System.Drawing.Size(303, 27)
        Me.cmbSubCat.TabIndex = 3
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(316, 376)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 33)
        Me.btnExit.TabIndex = 14
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(195, 376)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(76, 33)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cmbAB
        '
        Me.cmbAB.FormattingEnabled = True
        Me.cmbAB.Location = New System.Drawing.Point(130, 26)
        Me.cmbAB.Name = "cmbAB"
        Me.cmbAB.Size = New System.Drawing.Size(302, 27)
        Me.cmbAB.TabIndex = 1
        '
        'lblNew
        '
        Me.lblNew.AutoSize = True
        Me.lblNew.ForeColor = System.Drawing.Color.Red
        Me.lblNew.Location = New System.Drawing.Point(439, 31)
        Me.lblNew.Name = "lblNew"
        Me.lblNew.Size = New System.Drawing.Size(47, 19)
        Me.lblNew.TabIndex = 15
        Me.lblNew.Text = "NEW"
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(79, 376)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(76, 33)
        Me.btnNew.TabIndex = 16
        Me.btnNew.Text = "New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'frmDefAB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 431)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.lblNew)
        Me.Controls.Add(Me.cmbAB)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.cmbSubCat)
        Me.Controls.Add(Me.cmbCat)
        Me.Controls.Add(Me.gbDel)
        Me.Controls.Add(Me.gbSource)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmDefAB"
        Me.Text = "Define Antibiotics"
        Me.gbSource.ResumeLayout(False)
        Me.gbSource.PerformLayout()
        Me.gbDel.ResumeLayout(False)
        Me.gbDel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents gbSource As GroupBox
    Friend WithEvents cbEye As CheckBox
    Friend WithEvents cbSH As CheckBox
    Friend WithEvents cbUrn As CheckBox
    Friend WithEvents cbSSTI As CheckBox
    Friend WithEvents cbResp As CheckBox
    Friend WithEvents gbDel As GroupBox
    Friend WithEvents cbOpth As CheckBox
    Friend WithEvents cbPO As CheckBox
    Friend WithEvents cbIM As CheckBox
    Friend WithEvents cbIV As CheckBox
    Friend WithEvents cmbCat As ComboBox
    Friend WithEvents cmbSubCat As ComboBox
    Friend WithEvents btnExit As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents cmbAB As ComboBox
    Friend WithEvents lblNew As Label
    Friend WithEvents btnNew As Button
End Class
