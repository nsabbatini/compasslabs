﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmTestAbbrv
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub txtOC_KeyUp(sender As Object, e As KeyEventArgs) Handles txtOC.KeyUp
        If e.KeyCode = Keys.Enter Then
            Call GetOCDetails()
            txtOC.Text = ""
            txtOC.Focus()
        End If
    End Sub
    Public Sub GetOCDetails()
        Dim Sel As String, OCName As String = "", OC As String = ""
        lblAbbrs.Text = ""
        OC = txtOC.Text
        Sel = "select oc.OC,OC.ONAME,octc.TC,TC.TABRV,OCTC.TEFDT from OCTC, OC, TC " &
              "where OCTC.OC = OC.OC And OCTC.OEFDT = OC.OEFDT " &
              "and OCTC.TC=TC.TC and OCTC.TEFDT = TC.TEFDT " &
              "and OC.OACT=1 " &
              "and OC.OC = '" & txtOC.Text & "' " &
              "Order by OCTC.OCTCx"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                If OCName = "" Then OCName = rs(1)
                lblAbbrs.Text = lblAbbrs.Text & rs(3) & ", "
            Loop
            rs.Close()
            Conn.Close()
        End Using
        'Remove trailing comma
        lblAbbrs.Text = Mid(lblAbbrs.Text, 1, Len(lblAbbrs.Text) - 2)
        lblAbbrs.Text = "[] " & OCName & "-" & OC & " (" & lblAbbrs.Text & ")"
        lblOCName.Text = OC & " - " & OCName
    End Sub

    Private Sub frmTestAbbrv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtOC.Focus()
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        My.Computer.Clipboard.SetText(lblAbbrs.Text)
    End Sub
End Class