﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class OCInq

    Private Sub txtOC_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtOC.KeyUp
        If e.KeyCode = Keys.Enter Then
            lblOC.Text = ""
            Call GetOCDetails()
            txtOC.Text = ""
            txtOC.Focus()
        End If
    End Sub
    Public Sub GetOCDetails()
        Dim sType As String, Sel As String, Sel2 As String
        ListBox1.Items.Clear()
        Sel = "select oc.OC,OC.ONAME,octc.TC,TC.TNAME,OCTC.TEFDT,OC.OABRV from OCTC, OC, TC " &
              "where OCTC.OC = OC.OC And OCTC.OEFDT = OC.OEFDT " &
              "and OCTC.TC=TC.TC and OCTC.TEFDT = TC.TEFDT " &
              "and OC.OACT=1 " &
              "and (OC.OC = '" & txtOC.Text & "' or OC.OABRV='" & txtOC.Text & "') " &
              "Order by OCTC.OCTCx"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                sType = "LCMS"
                If Mid(rs(2), 1, 1) = "2" Then sType = "SCRN"
                If Mid(rs(2), 1, 1) = "5" Then sType = "DNA"
                If Mid(rs(2), 1, 1) = "M" Then sType = "MOLECULAR"
                ListBox1.Items.Add(LJ(rs(2), 10) & LJ(rs(3), 28) & LJ(sType, 9))
                lblOC.Text = rs(0) & "-" & rs(1) & "-" & rs(5)
                If cbDetail.Checked Then
                    Sel2 = "Select TCRC.RC,RC.RNAME From TCRC, RC Where TC='" & rs(2) & "' And TEFDT='" & rs(4) & "' And TCRC.RC=RC.RC And TCRC.REFDT=RC.REFDT"
                    Dim cmd2 As New SqlCommand(Sel2, Conn)
                    Dim rs2 As SqlDataReader = cmd2.ExecuteReader
                    Do While rs2.Read
                        ListBox1.Items.Add(Space(4) & LJ(rs2(0), 10) & LJ(rs2(1), 40))
                    Loop
                    rs2.Close()
                    cmd2.Dispose()
                End If
            Loop
            rs.Close()
            Conn.Close()
        End Using

    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Public Function LJ(ByVal strLJ As String, ByVal iLen As Integer) As String
        LJ = ""
        If Len(strLJ) > 30 Then strLJ = Mid(strLJ, 1, 30)
        LJ = strLJ & Space(iLen - Len(strLJ))
    End Function

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Dim printer As New PCPrint()
        'Set the font we want to use
        printer.PrinterFont = New Font("Courier New", 10)
        'Set Margins
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        printer.DefaultPageSettings.Margins = mar

        Dim txt As String = "Ordering Code: " & lblOC.Text & vbCrLf
        For i = 0 To ListBox1.Items.Count - 1
            txt = txt & vbCrLf & ListBox1.Items(i)
        Next i
        printer.TextToPrint = " " & txt
        'Issue print command
        printer.Print()

    End Sub

    Private Sub cbDetail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDetail.CheckedChanged
        If CLSLIMS.Piece(lblOC.Text, "-", 1) <> "" Then
            txtOC.Text = CLSLIMS.Piece(lblOC.Text, "-", 1)
            Call GetOCDetails()
            txtOC.Text = ""
            txtOC.Focus()
        End If
    End Sub

    Private Sub btnLstAll_Click(sender As System.Object, e As System.EventArgs) Handles btnLstAll.Click
        Dim Sel As String
        ListBox1.Items.Clear()
        Sel = "Select OC,OC.ONAME From OC Where OACT=1 and (OC Like '" & txtOC.Text & "%' Or ONAME Like '%" & txtOC.Text & "%') Order by OC"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                ListBox1.Items.Add(LJ(rs(0), 10) & LJ(rs(1), 50))
            Loop
            rs.Close()
            Conn.Close()
        End Using

    End Sub

    Private Sub txtOC_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOC.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
    End Sub
End Class


