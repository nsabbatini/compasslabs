﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PrtWrklstO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PrtWrklstO))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMBN = New System.Windows.Forms.TextBox()
        Me.lblWrkLst2 = New System.Windows.Forms.Label()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.lblWrklst = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.PrintPrev = New System.Windows.Forms.PrintPreviewDialog()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 45)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "MBN:"
        '
        'txtMBN
        '
        Me.txtMBN.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMBN.Location = New System.Drawing.Point(133, 45)
        Me.txtMBN.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMBN.Name = "txtMBN"
        Me.txtMBN.Size = New System.Drawing.Size(169, 32)
        Me.txtMBN.TabIndex = 3
        '
        'lblWrkLst2
        '
        Me.lblWrkLst2.AutoSize = True
        Me.lblWrkLst2.Location = New System.Drawing.Point(185, 185)
        Me.lblWrkLst2.Name = "lblWrkLst2"
        Me.lblWrkLst2.Size = New System.Drawing.Size(0, 17)
        Me.lblWrkLst2.TabIndex = 13
        Me.lblWrkLst2.Visible = False
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Location = New System.Drawing.Point(99, 127)
        Me.btnPreview.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(86, 40)
        Me.btnPreview.TabIndex = 12
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'lblWrklst
        '
        Me.lblWrklst.AutoSize = True
        Me.lblWrklst.Location = New System.Drawing.Point(237, 185)
        Me.lblWrklst.Name = "lblWrklst"
        Me.lblWrklst.Size = New System.Drawing.Size(0, 17)
        Me.lblWrklst.TabIndex = 11
        Me.lblWrklst.Visible = False
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(278, 185)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(70, 40)
        Me.btnExit.TabIndex = 10
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(216, 127)
        Me.btnPrint.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(86, 40)
        Me.btnPrint.TabIndex = 9
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'lstData
        '
        Me.lstData.Font = New System.Drawing.Font("Courier New", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 20
        Me.lstData.Location = New System.Drawing.Point(21, 108)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(35, 24)
        Me.lstData.TabIndex = 14
        Me.lstData.Visible = False
        '
        'PrintDoc
        '
        '
        'PrintPrev
        '
        Me.PrintPrev.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPrev.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPrev.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPrev.Document = Me.PrintDoc
        Me.PrintPrev.Enabled = True
        Me.PrintPrev.Icon = CType(resources.GetObject("PrintPrev.Icon"), System.Drawing.Icon)
        Me.PrintPrev.Name = "PrintPreviewDialog1"
        Me.PrintPrev.Visible = False
        '
        'PrtWrklstO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 244)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.lblWrkLst2)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.lblWrklst)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.txtMBN)
        Me.Controls.Add(Me.Label1)
        Me.Name = "PrtWrklstO"
        Me.Text = "Print Oral Worklist"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMBN As System.Windows.Forms.TextBox
    Friend WithEvents lblWrkLst2 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblWrklst As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents lstData As System.Windows.Forms.ListBox
    Friend WithEvents PrintDoc As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPrev As System.Windows.Forms.PrintPreviewDialog
End Class
