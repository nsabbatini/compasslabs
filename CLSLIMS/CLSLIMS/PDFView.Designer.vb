﻿Imports CLSLIMS.PCPrint
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PDFView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PDFView))
        Me.PcPrint1 = New PCPrint()
        Me.PDF1 = New AxAcroPDFLib.AxAcroPDF()
        CType(Me.PDF1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PcPrint1
        '
        Me.PcPrint1.PrinterFont = Nothing
        Me.PcPrint1.TextToPrint = ""
        '
        'PDF1
        '
        Me.PDF1.Enabled = True
        Me.PDF1.Location = New System.Drawing.Point(2, 1)
        Me.PDF1.Name = "PDF1"
        Me.PDF1.OcxState = CType(resources.GetObject("PDF1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.PDF1.Size = New System.Drawing.Size(963, 781)
        Me.PDF1.TabIndex = 0
        '
        'PDFView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(964, 782)
        Me.Controls.Add(Me.PDF1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "PDFView"
        Me.Text = "PDFView"
        CType(Me.PDF1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PcPrint1 As PCPrint
    Friend WithEvents PDF1 As AxAcroPDF
End Class
