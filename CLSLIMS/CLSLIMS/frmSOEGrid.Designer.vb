﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSOEGrid
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbOralTram = New System.Windows.Forms.CheckBox()
        Me.cbOralMtd = New System.Windows.Forms.CheckBox()
        Me.cbOralTap = New System.Windows.Forms.CheckBox()
        Me.cbOralSkel = New System.Windows.Forms.CheckBox()
        Me.cbOralPro = New System.Windows.Forms.CheckBox()
        Me.cbOralPhen = New System.Windows.Forms.CheckBox()
        Me.cbOralOxy = New System.Windows.Forms.CheckBox()
        Me.cbOralOpiAnal = New System.Windows.Forms.CheckBox()
        Me.cbOralOpi = New System.Windows.Forms.CheckBox()
        Me.cbOralMethy = New System.Windows.Forms.CheckBox()
        Me.cbOralMethAmp = New System.Windows.Forms.CheckBox()
        Me.cbOralHero = New System.Windows.Forms.CheckBox()
        Me.cbOralFent = New System.Windows.Forms.CheckBox()
        Me.cbOralCOC = New System.Windows.Forms.CheckBox()
        Me.cbOralTHC = New System.Windows.Forms.CheckBox()
        Me.cbOralBup = New System.Windows.Forms.CheckBox()
        Me.cbOralBenz = New System.Windows.Forms.CheckBox()
        Me.cbOralAmp = New System.Windows.Forms.CheckBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnOrd = New System.Windows.Forms.Button()
        Me.cbOralAlk = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiPsych = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiEpi = New System.Windows.Forms.CheckBox()
        Me.cbOralAnti = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiTri = New System.Windows.Forms.CheckBox()
        Me.cbOralADSer = New System.Windows.Forms.CheckBox()
        Me.cbOralGaba = New System.Windows.Forms.CheckBox()
        Me.cbOralPregaba = New System.Windows.Forms.CheckBox()
        Me.cbOralSedHyp = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'cbOralTram
        '
        Me.cbOralTram.AutoSize = True
        Me.cbOralTram.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTram.Location = New System.Drawing.Point(705, 230)
        Me.cbOralTram.Name = "cbOralTram"
        Me.cbOralTram.Size = New System.Drawing.Size(145, 23)
        Me.cbOralTram.TabIndex = 109
        Me.cbOralTram.Text = "Oral Tramadol"
        Me.cbOralTram.UseVisualStyleBackColor = True
        '
        'cbOralMtd
        '
        Me.cbOralMtd.AutoSize = True
        Me.cbOralMtd.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMtd.Location = New System.Drawing.Point(338, 182)
        Me.cbOralMtd.Name = "cbOralMtd"
        Me.cbOralMtd.Size = New System.Drawing.Size(158, 23)
        Me.cbOralMtd.TabIndex = 108
        Me.cbOralMtd.Text = "Oral Methadone"
        Me.cbOralMtd.UseVisualStyleBackColor = True
        '
        'cbOralTap
        '
        Me.cbOralTap.AutoSize = True
        Me.cbOralTap.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTap.Location = New System.Drawing.Point(705, 206)
        Me.cbOralTap.Name = "cbOralTap"
        Me.cbOralTap.Size = New System.Drawing.Size(160, 23)
        Me.cbOralTap.TabIndex = 107
        Me.cbOralTap.Text = "Oral Tapentadol"
        Me.cbOralTap.UseVisualStyleBackColor = True
        '
        'cbOralSkel
        '
        Me.cbOralSkel.AutoSize = True
        Me.cbOralSkel.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralSkel.Location = New System.Drawing.Point(705, 182)
        Me.cbOralSkel.Name = "cbOralSkel"
        Me.cbOralSkel.Size = New System.Drawing.Size(281, 23)
        Me.cbOralSkel.TabIndex = 106
        Me.cbOralSkel.Text = "Oral Skeletal Muscle Relaxants"
        Me.cbOralSkel.UseVisualStyleBackColor = True
        '
        'cbOralPro
        '
        Me.cbOralPro.AutoSize = True
        Me.cbOralPro.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPro.Location = New System.Drawing.Point(705, 134)
        Me.cbOralPro.Name = "cbOralPro"
        Me.cbOralPro.Size = New System.Drawing.Size(184, 23)
        Me.cbOralPro.TabIndex = 105
        Me.cbOralPro.Text = "Oral Propoxyphene"
        Me.cbOralPro.UseVisualStyleBackColor = True
        '
        'cbOralPhen
        '
        Me.cbOralPhen.AutoSize = True
        Me.cbOralPhen.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPhen.Location = New System.Drawing.Point(705, 86)
        Me.cbOralPhen.Name = "cbOralPhen"
        Me.cbOralPhen.Size = New System.Drawing.Size(178, 23)
        Me.cbOralPhen.TabIndex = 104
        Me.cbOralPhen.Text = "Oral Phencyclidine"
        Me.cbOralPhen.UseVisualStyleBackColor = True
        '
        'cbOralOxy
        '
        Me.cbOralOxy.AutoSize = True
        Me.cbOralOxy.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOxy.Location = New System.Drawing.Point(705, 62)
        Me.cbOralOxy.Name = "cbOralOxy"
        Me.cbOralOxy.Size = New System.Drawing.Size(156, 23)
        Me.cbOralOxy.TabIndex = 103
        Me.cbOralOxy.Text = "Oral Oxycodone"
        Me.cbOralOxy.UseVisualStyleBackColor = True
        '
        'cbOralOpiAnal
        '
        Me.cbOralOpiAnal.AutoSize = True
        Me.cbOralOpiAnal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOpiAnal.Location = New System.Drawing.Point(705, 38)
        Me.cbOralOpiAnal.Name = "cbOralOpiAnal"
        Me.cbOralOpiAnal.Size = New System.Drawing.Size(311, 23)
        Me.cbOralOpiAnal.TabIndex = 102
        Me.cbOralOpiAnal.Text = "Oral Opioids and Opiate Analogues"
        Me.cbOralOpiAnal.UseVisualStyleBackColor = True
        '
        'cbOralOpi
        '
        Me.cbOralOpi.AutoSize = True
        Me.cbOralOpi.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOpi.Location = New System.Drawing.Point(705, 14)
        Me.cbOralOpi.Name = "cbOralOpi"
        Me.cbOralOpi.Size = New System.Drawing.Size(129, 23)
        Me.cbOralOpi.TabIndex = 101
        Me.cbOralOpi.Text = "Oral Opiates"
        Me.cbOralOpi.UseVisualStyleBackColor = True
        '
        'cbOralMethy
        '
        Me.cbOralMethy.AutoSize = True
        Me.cbOralMethy.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMethy.Location = New System.Drawing.Point(338, 230)
        Me.cbOralMethy.Name = "cbOralMethy"
        Me.cbOralMethy.Size = New System.Drawing.Size(204, 23)
        Me.cbOralMethy.TabIndex = 100
        Me.cbOralMethy.Text = "Oral Methylphenidate"
        Me.cbOralMethy.UseVisualStyleBackColor = True
        '
        'cbOralMethAmp
        '
        Me.cbOralMethAmp.AutoSize = True
        Me.cbOralMethAmp.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMethAmp.Location = New System.Drawing.Point(338, 206)
        Me.cbOralMethAmp.Name = "cbOralMethAmp"
        Me.cbOralMethAmp.Size = New System.Drawing.Size(325, 23)
        Me.cbOralMethAmp.TabIndex = 99
        Me.cbOralMethAmp.Text = "Oral Methylenedioxy-Amphetamines"
        Me.cbOralMethAmp.UseVisualStyleBackColor = True
        '
        'cbOralHero
        '
        Me.cbOralHero.AutoSize = True
        Me.cbOralHero.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralHero.Location = New System.Drawing.Point(338, 158)
        Me.cbOralHero.Name = "cbOralHero"
        Me.cbOralHero.Size = New System.Drawing.Size(214, 23)
        Me.cbOralHero.TabIndex = 98
        Me.cbOralHero.Text = "Oral Heroin Metabolite"
        Me.cbOralHero.UseVisualStyleBackColor = True
        '
        'cbOralFent
        '
        Me.cbOralFent.AutoSize = True
        Me.cbOralFent.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralFent.Location = New System.Drawing.Point(338, 134)
        Me.cbOralFent.Name = "cbOralFent"
        Me.cbOralFent.Size = New System.Drawing.Size(145, 23)
        Me.cbOralFent.TabIndex = 97
        Me.cbOralFent.Text = "Oral Fentanyls"
        Me.cbOralFent.UseVisualStyleBackColor = True
        '
        'cbOralCOC
        '
        Me.cbOralCOC.AutoSize = True
        Me.cbOralCOC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralCOC.Location = New System.Drawing.Point(338, 86)
        Me.cbOralCOC.Name = "cbOralCOC"
        Me.cbOralCOC.Size = New System.Drawing.Size(131, 23)
        Me.cbOralCOC.TabIndex = 96
        Me.cbOralCOC.Text = "Oral Cocaine"
        Me.cbOralCOC.UseVisualStyleBackColor = True
        '
        'cbOralTHC
        '
        Me.cbOralTHC.AutoSize = True
        Me.cbOralTHC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTHC.Location = New System.Drawing.Point(338, 62)
        Me.cbOralTHC.Name = "cbOralTHC"
        Me.cbOralTHC.Size = New System.Drawing.Size(247, 23)
        Me.cbOralTHC.TabIndex = 95
        Me.cbOralTHC.Text = "Oral Cannabinoids, Natural"
        Me.cbOralTHC.UseVisualStyleBackColor = True
        '
        'cbOralBup
        '
        Me.cbOralBup.AutoSize = True
        Me.cbOralBup.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralBup.Location = New System.Drawing.Point(338, 38)
        Me.cbOralBup.Name = "cbOralBup"
        Me.cbOralBup.Size = New System.Drawing.Size(187, 23)
        Me.cbOralBup.TabIndex = 94
        Me.cbOralBup.Text = "Oral Buprenorphine"
        Me.cbOralBup.UseVisualStyleBackColor = True
        '
        'cbOralBenz
        '
        Me.cbOralBenz.AutoSize = True
        Me.cbOralBenz.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralBenz.Location = New System.Drawing.Point(338, 14)
        Me.cbOralBenz.Name = "cbOralBenz"
        Me.cbOralBenz.Size = New System.Drawing.Size(204, 23)
        Me.cbOralBenz.TabIndex = 93
        Me.cbOralBenz.Text = "Oral Benzodiazepines"
        Me.cbOralBenz.UseVisualStyleBackColor = True
        '
        'cbOralAmp
        '
        Me.cbOralAmp.AutoSize = True
        Me.cbOralAmp.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAmp.Location = New System.Drawing.Point(19, 42)
        Me.cbOralAmp.Name = "cbOralAmp"
        Me.cbOralAmp.Size = New System.Drawing.Size(189, 23)
        Me.cbOralAmp.TabIndex = 92
        Me.cbOralAmp.Text = "Oral Amphetamines"
        Me.cbOralAmp.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(705, 273)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(106, 47)
        Me.btnClear.TabIndex = 131
        Me.btnClear.Text = "Clear All Selections"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnOrd
        '
        Me.btnOrd.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOrd.Location = New System.Drawing.Point(845, 273)
        Me.btnOrd.Name = "btnOrd"
        Me.btnOrd.Size = New System.Drawing.Size(97, 47)
        Me.btnOrd.TabIndex = 130
        Me.btnOrd.Text = "Order"
        Me.btnOrd.UseVisualStyleBackColor = True
        '
        'cbOralAlk
        '
        Me.cbOralAlk.AutoSize = True
        Me.cbOralAlk.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAlk.Location = New System.Drawing.Point(19, 18)
        Me.cbOralAlk.Name = "cbOralAlk"
        Me.cbOralAlk.Size = New System.Drawing.Size(241, 23)
        Me.cbOralAlk.TabIndex = 132
        Me.cbOralAlk.Text = "Oral Alkaloids Unspecified"
        Me.cbOralAlk.UseVisualStyleBackColor = True
        '
        'cbOralAntiPsych
        '
        Me.cbOralAntiPsych.AutoSize = True
        Me.cbOralAntiPsych.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiPsych.Location = New System.Drawing.Point(19, 162)
        Me.cbOralAntiPsych.Name = "cbOralAntiPsych"
        Me.cbOralAntiPsych.Size = New System.Drawing.Size(184, 23)
        Me.cbOralAntiPsych.TabIndex = 137
        Me.cbOralAntiPsych.Text = "Oral Antipsychotics"
        Me.cbOralAntiPsych.UseVisualStyleBackColor = True
        '
        'cbOralAntiEpi
        '
        Me.cbOralAntiEpi.AutoSize = True
        Me.cbOralAntiEpi.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiEpi.Location = New System.Drawing.Point(19, 138)
        Me.cbOralAntiEpi.Name = "cbOralAntiEpi"
        Me.cbOralAntiEpi.Size = New System.Drawing.Size(179, 23)
        Me.cbOralAntiEpi.TabIndex = 136
        Me.cbOralAntiEpi.Text = "Oral Antiepileptics"
        Me.cbOralAntiEpi.UseVisualStyleBackColor = True
        '
        'cbOralAnti
        '
        Me.cbOralAnti.AutoSize = True
        Me.cbOralAnti.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAnti.Location = New System.Drawing.Point(19, 114)
        Me.cbOralAnti.Name = "cbOralAnti"
        Me.cbOralAnti.Size = New System.Drawing.Size(199, 23)
        Me.cbOralAnti.TabIndex = 135
        Me.cbOralAnti.Text = "Oral Antidepressants"
        Me.cbOralAnti.UseVisualStyleBackColor = True
        '
        'cbOralAntiTri
        '
        Me.cbOralAntiTri.AutoSize = True
        Me.cbOralAntiTri.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiTri.Location = New System.Drawing.Point(19, 90)
        Me.cbOralAntiTri.Name = "cbOralAntiTri"
        Me.cbOralAntiTri.Size = New System.Drawing.Size(275, 23)
        Me.cbOralAntiTri.TabIndex = 134
        Me.cbOralAntiTri.Text = "Oral Antidepressants, Tricyclic"
        Me.cbOralAntiTri.UseVisualStyleBackColor = True
        '
        'cbOralADSer
        '
        Me.cbOralADSer.AutoSize = True
        Me.cbOralADSer.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralADSer.Location = New System.Drawing.Point(19, 66)
        Me.cbOralADSer.Name = "cbOralADSer"
        Me.cbOralADSer.Size = New System.Drawing.Size(313, 23)
        Me.cbOralADSer.TabIndex = 133
        Me.cbOralADSer.Text = "Oral Antidepressants, Serotonergic"
        Me.cbOralADSer.UseVisualStyleBackColor = True
        '
        'cbOralGaba
        '
        Me.cbOralGaba.AutoSize = True
        Me.cbOralGaba.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralGaba.Location = New System.Drawing.Point(338, 110)
        Me.cbOralGaba.Name = "cbOralGaba"
        Me.cbOralGaba.Size = New System.Drawing.Size(256, 23)
        Me.cbOralGaba.TabIndex = 138
        Me.cbOralGaba.Text = "Oral Gabapentin, Non-Blood"
        Me.cbOralGaba.UseVisualStyleBackColor = True
        '
        'cbOralPregaba
        '
        Me.cbOralPregaba.AutoSize = True
        Me.cbOralPregaba.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPregaba.Location = New System.Drawing.Point(705, 110)
        Me.cbOralPregaba.Name = "cbOralPregaba"
        Me.cbOralPregaba.Size = New System.Drawing.Size(155, 23)
        Me.cbOralPregaba.TabIndex = 139
        Me.cbOralPregaba.Text = "Oral Pregabalin"
        Me.cbOralPregaba.UseVisualStyleBackColor = True
        '
        'cbOralSedHyp
        '
        Me.cbOralSedHyp.AutoSize = True
        Me.cbOralSedHyp.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralSedHyp.Location = New System.Drawing.Point(705, 158)
        Me.cbOralSedHyp.Name = "cbOralSedHyp"
        Me.cbOralSedHyp.Size = New System.Drawing.Size(222, 23)
        Me.cbOralSedHyp.TabIndex = 140
        Me.cbOralSedHyp.Text = "Oral Sedative Hypnotics"
        Me.cbOralSedHyp.UseVisualStyleBackColor = True
        '
        'frmSOEGrid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1023, 341)
        Me.Controls.Add(Me.cbOralSedHyp)
        Me.Controls.Add(Me.cbOralPregaba)
        Me.Controls.Add(Me.cbOralGaba)
        Me.Controls.Add(Me.cbOralAntiPsych)
        Me.Controls.Add(Me.cbOralAntiEpi)
        Me.Controls.Add(Me.cbOralAnti)
        Me.Controls.Add(Me.cbOralAntiTri)
        Me.Controls.Add(Me.cbOralADSer)
        Me.Controls.Add(Me.cbOralAlk)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnOrd)
        Me.Controls.Add(Me.cbOralTram)
        Me.Controls.Add(Me.cbOralMtd)
        Me.Controls.Add(Me.cbOralTap)
        Me.Controls.Add(Me.cbOralSkel)
        Me.Controls.Add(Me.cbOralPro)
        Me.Controls.Add(Me.cbOralPhen)
        Me.Controls.Add(Me.cbOralOxy)
        Me.Controls.Add(Me.cbOralOpiAnal)
        Me.Controls.Add(Me.cbOralOpi)
        Me.Controls.Add(Me.cbOralMethy)
        Me.Controls.Add(Me.cbOralMethAmp)
        Me.Controls.Add(Me.cbOralHero)
        Me.Controls.Add(Me.cbOralFent)
        Me.Controls.Add(Me.cbOralCOC)
        Me.Controls.Add(Me.cbOralTHC)
        Me.Controls.Add(Me.cbOralBup)
        Me.Controls.Add(Me.cbOralBenz)
        Me.Controls.Add(Me.cbOralAmp)
        Me.Name = "frmSOEGrid"
        Me.Text = "Oral Fluid Order Entry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbOralTram As CheckBox
    Friend WithEvents cbOralMtd As CheckBox
    Friend WithEvents cbOralTap As CheckBox
    Friend WithEvents cbOralSkel As CheckBox
    Friend WithEvents cbOralPro As CheckBox
    Friend WithEvents cbOralPhen As CheckBox
    Friend WithEvents cbOralOxy As CheckBox
    Friend WithEvents cbOralOpiAnal As CheckBox
    Friend WithEvents cbOralOpi As CheckBox
    Friend WithEvents cbOralMethy As CheckBox
    Friend WithEvents cbOralMethAmp As CheckBox
    Friend WithEvents cbOralHero As CheckBox
    Friend WithEvents cbOralFent As CheckBox
    Friend WithEvents cbOralCOC As CheckBox
    Friend WithEvents cbOralTHC As CheckBox
    Friend WithEvents cbOralBup As CheckBox
    Friend WithEvents cbOralBenz As CheckBox
    Friend WithEvents cbOralAmp As CheckBox
    Friend WithEvents btnClear As Button
    Friend WithEvents btnOrd As Button
    Friend WithEvents cbOralAlk As CheckBox
    Friend WithEvents cbOralAntiPsych As CheckBox
    Friend WithEvents cbOralAntiEpi As CheckBox
    Friend WithEvents cbOralAnti As CheckBox
    Friend WithEvents cbOralAntiTri As CheckBox
    Friend WithEvents cbOralADSer As CheckBox
    Friend WithEvents cbOralGaba As CheckBox
    Friend WithEvents cbOralPregaba As CheckBox
    Friend WithEvents cbOralSedHyp As CheckBox
End Class
