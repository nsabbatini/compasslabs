﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOrders
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtOrdNo = New System.Windows.Forms.TextBox()
        Me.txtAcctNo = New System.Windows.Forms.TextBox()
        Me.txtAcctName = New System.Windows.Forms.TextBox()
        Me.txtContact = New System.Windows.Forms.TextBox()
        Me.txtAddr = New System.Windows.Forms.TextBox()
        Me.txtAddr2 = New System.Windows.Forms.TextBox()
        Me.txtCity = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtState = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtZIP = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cmbItem = New System.Windows.Forms.ComboBox()
        Me.lblItemDesc = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.btnOrder = New System.Windows.Forms.Button()
        Me.lstItems = New System.Windows.Forms.ListBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbCourier = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTrackNum = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtWt = New System.Windows.Forms.TextBox()
        Me.txtPkgs = New System.Windows.Forms.TextBox()
        Me.txtNotes = New System.Windows.Forms.RichTextBox()
        Me.txtPrevOrd = New System.Windows.Forms.RichTextBox()
        Me.txtInfo = New System.Windows.Forms.RichTextBox()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.lblUnit = New System.Windows.Forms.Label()
        Me.pnlOrders = New System.Windows.Forms.Panel()
        Me.lstItemsOrdered = New System.Windows.Forms.ListBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblOrdDate = New System.Windows.Forms.Label()
        Me.lblPickDate = New System.Windows.Forms.Label()
        Me.lblShipDate = New System.Windows.Forms.Label()
        Me.lblRecvDate = New System.Windows.Forms.Label()
        Me.wbAcctVol = New System.Windows.Forms.WebBrowser()
        Me.btnSaveApp = New System.Windows.Forms.Button()
        Me.txtFax = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.PrintPck = New System.Drawing.Printing.PrintDocument()
        Me.lstPrtPick = New System.Windows.Forms.ListBox()
        Me.btnVol = New System.Windows.Forms.Button()
        Me.OrderBlanks = New System.Windows.Forms.PrintDialog()
        Me.btnPrtPck = New System.Windows.Forms.Button()
        Me.btnNotes = New System.Windows.Forms.Button()
        Me.pnlOrders.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(3, 491)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(92, 31)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(3, 398)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(92, 31)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(130, 19)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Order Number:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(74, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 19)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Account:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 19)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Account Name:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(76, 131)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 19)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Contact:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(74, 165)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 19)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Address:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(107, 233)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 19)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "City:"
        '
        'txtOrdNo
        '
        Me.txtOrdNo.Location = New System.Drawing.Point(161, 20)
        Me.txtOrdNo.Name = "txtOrdNo"
        Me.txtOrdNo.Size = New System.Drawing.Size(100, 27)
        Me.txtOrdNo.TabIndex = 0
        '
        'txtAcctNo
        '
        Me.txtAcctNo.Location = New System.Drawing.Point(163, 60)
        Me.txtAcctNo.Name = "txtAcctNo"
        Me.txtAcctNo.Size = New System.Drawing.Size(127, 27)
        Me.txtAcctNo.TabIndex = 1
        '
        'txtAcctName
        '
        Me.txtAcctName.Location = New System.Drawing.Point(163, 94)
        Me.txtAcctName.Name = "txtAcctName"
        Me.txtAcctName.Size = New System.Drawing.Size(328, 27)
        Me.txtAcctName.TabIndex = 2
        '
        'txtContact
        '
        Me.txtContact.Location = New System.Drawing.Point(163, 128)
        Me.txtContact.Name = "txtContact"
        Me.txtContact.Size = New System.Drawing.Size(411, 27)
        Me.txtContact.TabIndex = 3
        '
        'txtAddr
        '
        Me.txtAddr.Location = New System.Drawing.Point(163, 162)
        Me.txtAddr.Name = "txtAddr"
        Me.txtAddr.Size = New System.Drawing.Size(411, 27)
        Me.txtAddr.TabIndex = 4
        '
        'txtAddr2
        '
        Me.txtAddr2.Location = New System.Drawing.Point(163, 196)
        Me.txtAddr2.Name = "txtAddr2"
        Me.txtAddr2.Size = New System.Drawing.Size(411, 27)
        Me.txtAddr2.TabIndex = 5
        '
        'txtCity
        '
        Me.txtCity.Location = New System.Drawing.Point(163, 230)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(142, 27)
        Me.txtCity.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(309, 233)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 19)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "State:"
        '
        'txtState
        '
        Me.txtState.Location = New System.Drawing.Point(375, 231)
        Me.txtState.Name = "txtState"
        Me.txtState.Size = New System.Drawing.Size(43, 27)
        Me.txtState.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(425, 234)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 19)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "ZIP:"
        '
        'txtZIP
        '
        Me.txtZIP.Location = New System.Drawing.Point(474, 233)
        Me.txtZIP.Name = "txtZIP"
        Me.txtZIP.Size = New System.Drawing.Size(100, 27)
        Me.txtZIP.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 19)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "Item:"
        '
        'cmbItem
        '
        Me.cmbItem.FormattingEnabled = True
        Me.cmbItem.Location = New System.Drawing.Point(59, 14)
        Me.cmbItem.Name = "cmbItem"
        Me.cmbItem.Size = New System.Drawing.Size(142, 27)
        Me.cmbItem.TabIndex = 10
        '
        'lblItemDesc
        '
        Me.lblItemDesc.AutoSize = True
        Me.lblItemDesc.Location = New System.Drawing.Point(207, 17)
        Me.lblItemDesc.Name = "lblItemDesc"
        Me.lblItemDesc.Size = New System.Drawing.Size(145, 19)
        Me.lblItemDesc.TabIndex = 29
        Me.lblItemDesc.Text = "Item Description"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(10, 55)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 19)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Qty:"
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(59, 49)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(64, 27)
        Me.txtQty.TabIndex = 11
        '
        'btnOrder
        '
        Me.btnOrder.Location = New System.Drawing.Point(239, 49)
        Me.btnOrder.Name = "btnOrder"
        Me.btnOrder.Size = New System.Drawing.Size(75, 30)
        Me.btnOrder.TabIndex = 12
        Me.btnOrder.Text = "Order"
        Me.btnOrder.UseVisualStyleBackColor = True
        '
        'lstItems
        '
        Me.lstItems.Font = New System.Drawing.Font("Courier New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstItems.FormattingEnabled = True
        Me.lstItems.ItemHeight = 22
        Me.lstItems.Location = New System.Drawing.Point(18, 95)
        Me.lstItems.Name = "lstItems"
        Me.lstItems.Size = New System.Drawing.Size(591, 114)
        Me.lstItems.TabIndex = 33
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(627, 97)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(75, 19)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Courier:"
        '
        'cmbCourier
        '
        Me.cmbCourier.FormattingEnabled = True
        Me.cmbCourier.Items.AddRange(New Object() {"UPS Ground", "Fedex Ground", "Fedex Overnight"})
        Me.cmbCourier.Location = New System.Drawing.Point(708, 94)
        Me.cmbCourier.Name = "cmbCourier"
        Me.cmbCourier.Size = New System.Drawing.Size(121, 27)
        Me.cmbCourier.TabIndex = 13
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(603, 136)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(99, 19)
        Me.Label12.TabIndex = 36
        Me.Label12.Text = "Tracking#:"
        '
        'txtTrackNum
        '
        Me.txtTrackNum.Location = New System.Drawing.Point(708, 134)
        Me.txtTrackNum.Name = "txtTrackNum"
        Me.txtTrackNum.Size = New System.Drawing.Size(213, 27)
        Me.txtTrackNum.TabIndex = 14
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(629, 170)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(73, 19)
        Me.Label13.TabIndex = 38
        Me.Label13.Text = "Weight:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(609, 204)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 19)
        Me.Label14.TabIndex = 39
        Me.Label14.Text = "# of Pkgs:"
        '
        'txtWt
        '
        Me.txtWt.Location = New System.Drawing.Point(708, 168)
        Me.txtWt.Name = "txtWt"
        Me.txtWt.Size = New System.Drawing.Size(100, 27)
        Me.txtWt.TabIndex = 15
        '
        'txtPkgs
        '
        Me.txtPkgs.Location = New System.Drawing.Point(708, 202)
        Me.txtPkgs.Name = "txtPkgs"
        Me.txtPkgs.Size = New System.Drawing.Size(46, 27)
        Me.txtPkgs.TabIndex = 16
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(927, 19)
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ReadOnly = True
        Me.txtNotes.Size = New System.Drawing.Size(267, 131)
        Me.txtNotes.TabIndex = 42
        Me.txtNotes.Text = ""
        '
        'txtPrevOrd
        '
        Me.txtPrevOrd.Location = New System.Drawing.Point(927, 156)
        Me.txtPrevOrd.Name = "txtPrevOrd"
        Me.txtPrevOrd.Size = New System.Drawing.Size(267, 135)
        Me.txtPrevOrd.TabIndex = 43
        Me.txtPrevOrd.Text = ""
        '
        'txtInfo
        '
        Me.txtInfo.Enabled = False
        Me.txtInfo.Location = New System.Drawing.Point(927, 398)
        Me.txtInfo.Name = "txtInfo"
        Me.txtInfo.Size = New System.Drawing.Size(267, 134)
        Me.txtInfo.TabIndex = 45
        Me.txtInfo.Text = ""
        '
        'lstData
        '
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 19
        Me.lstData.Location = New System.Drawing.Point(741, 378)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(61, 23)
        Me.lstData.TabIndex = 47
        Me.lstData.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(88, 272)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(66, 19)
        Me.Label15.TabIndex = 48
        Me.Label15.Text = "Phone:"
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(163, 272)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(166, 27)
        Me.txtPhone.TabIndex = 9
        '
        'lblUnit
        '
        Me.lblUnit.AutoSize = True
        Me.lblUnit.Location = New System.Drawing.Point(129, 55)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(43, 19)
        Me.lblUnit.TabIndex = 49
        Me.lblUnit.Text = "Unit"
        '
        'pnlOrders
        '
        Me.pnlOrders.BackColor = System.Drawing.Color.LightGray
        Me.pnlOrders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOrders.Controls.Add(Me.lblItemDesc)
        Me.pnlOrders.Controls.Add(Me.lstItems)
        Me.pnlOrders.Controls.Add(Me.cmbItem)
        Me.pnlOrders.Controls.Add(Me.lblUnit)
        Me.pnlOrders.Controls.Add(Me.txtQty)
        Me.pnlOrders.Controls.Add(Me.btnOrder)
        Me.pnlOrders.Controls.Add(Me.Label9)
        Me.pnlOrders.Controls.Add(Me.Label10)
        Me.pnlOrders.Location = New System.Drawing.Point(111, 312)
        Me.pnlOrders.Name = "pnlOrders"
        Me.pnlOrders.Size = New System.Drawing.Size(624, 220)
        Me.pnlOrders.TabIndex = 50
        '
        'lstItemsOrdered
        '
        Me.lstItemsOrdered.FormattingEnabled = True
        Me.lstItemsOrdered.ItemHeight = 19
        Me.lstItemsOrdered.Location = New System.Drawing.Point(741, 406)
        Me.lstItemsOrdered.Name = "lstItemsOrdered"
        Me.lstItemsOrdered.Size = New System.Drawing.Size(61, 23)
        Me.lstItemsOrdered.TabIndex = 51
        Me.lstItemsOrdered.Visible = False
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(277, 23)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(61, 19)
        Me.lblStatus.TabIndex = 52
        Me.lblStatus.Text = "Status"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(418, 18)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(81, 19)
        Me.Label16.TabIndex = 53
        Me.Label16.Text = "Ordered:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(430, 41)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(69, 19)
        Me.Label17.TabIndex = 54
        Me.Label17.Text = "Picked:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(627, 19)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(80, 19)
        Me.Label18.TabIndex = 55
        Me.Label18.Text = "Shipped:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(618, 41)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(89, 19)
        Me.Label19.TabIndex = 56
        Me.Label19.Text = "Received:"
        '
        'lblOrdDate
        '
        Me.lblOrdDate.AutoSize = True
        Me.lblOrdDate.Location = New System.Drawing.Point(496, 18)
        Me.lblOrdDate.Name = "lblOrdDate"
        Me.lblOrdDate.Size = New System.Drawing.Size(107, 19)
        Me.lblOrdDate.TabIndex = 57
        Me.lblOrdDate.Text = "01/01/2018"
        '
        'lblPickDate
        '
        Me.lblPickDate.AutoSize = True
        Me.lblPickDate.Location = New System.Drawing.Point(496, 41)
        Me.lblPickDate.Name = "lblPickDate"
        Me.lblPickDate.Size = New System.Drawing.Size(107, 19)
        Me.lblPickDate.TabIndex = 58
        Me.lblPickDate.Text = "01/01/2018"
        '
        'lblShipDate
        '
        Me.lblShipDate.AutoSize = True
        Me.lblShipDate.Location = New System.Drawing.Point(704, 19)
        Me.lblShipDate.Name = "lblShipDate"
        Me.lblShipDate.Size = New System.Drawing.Size(107, 19)
        Me.lblShipDate.TabIndex = 59
        Me.lblShipDate.Text = "01/01/2018"
        '
        'lblRecvDate
        '
        Me.lblRecvDate.AutoSize = True
        Me.lblRecvDate.Location = New System.Drawing.Point(704, 41)
        Me.lblRecvDate.Name = "lblRecvDate"
        Me.lblRecvDate.Size = New System.Drawing.Size(107, 19)
        Me.lblRecvDate.TabIndex = 60
        Me.lblRecvDate.Text = "01/01/2018"
        '
        'wbAcctVol
        '
        Me.wbAcctVol.AllowNavigation = False
        Me.wbAcctVol.Location = New System.Drawing.Point(927, 298)
        Me.wbAcctVol.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbAcctVol.Name = "wbAcctVol"
        Me.wbAcctVol.Size = New System.Drawing.Size(264, 94)
        Me.wbAcctVol.TabIndex = 61
        Me.wbAcctVol.WebBrowserShortcutsEnabled = False
        '
        'btnSaveApp
        '
        Me.btnSaveApp.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveApp.Location = New System.Drawing.Point(3, 335)
        Me.btnSaveApp.Name = "btnSaveApp"
        Me.btnSaveApp.Size = New System.Drawing.Size(92, 54)
        Me.btnSaveApp.TabIndex = 62
        Me.btnSaveApp.Text = "Save Approve"
        Me.btnSaveApp.UseVisualStyleBackColor = True
        '
        'txtFax
        '
        Me.txtFax.Location = New System.Drawing.Point(408, 272)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(166, 27)
        Me.txtFax.TabIndex = 63
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(354, 272)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(44, 19)
        Me.Label20.TabIndex = 64
        Me.Label20.Text = "Fax:"
        '
        'PrintPck
        '
        '
        'lstPrtPick
        '
        Me.lstPrtPick.FormattingEnabled = True
        Me.lstPrtPick.ItemHeight = 19
        Me.lstPrtPick.Location = New System.Drawing.Point(741, 435)
        Me.lstPrtPick.Name = "lstPrtPick"
        Me.lstPrtPick.Size = New System.Drawing.Size(61, 23)
        Me.lstPrtPick.TabIndex = 65
        Me.lstPrtPick.Visible = False
        '
        'btnVol
        '
        Me.btnVol.Location = New System.Drawing.Point(839, 326)
        Me.btnVol.Name = "btnVol"
        Me.btnVol.Size = New System.Drawing.Size(82, 28)
        Me.btnVol.TabIndex = 66
        Me.btnVol.Text = "Volume"
        Me.btnVol.UseVisualStyleBackColor = True
        '
        'OrderBlanks
        '
        Me.OrderBlanks.UseEXDialog = True
        '
        'btnPrtPck
        '
        Me.btnPrtPck.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrtPck.Location = New System.Drawing.Point(741, 482)
        Me.btnPrtPck.Name = "btnPrtPck"
        Me.btnPrtPck.Size = New System.Drawing.Size(92, 50)
        Me.btnPrtPck.TabIndex = 67
        Me.btnPrtPck.Text = "Print Pick"
        Me.btnPrtPck.UseVisualStyleBackColor = True
        '
        'btnNotes
        '
        Me.btnNotes.Location = New System.Drawing.Point(856, 41)
        Me.btnNotes.Name = "btnNotes"
        Me.btnNotes.Size = New System.Drawing.Size(65, 28)
        Me.btnNotes.TabIndex = 68
        Me.btnNotes.Text = "Notes"
        Me.btnNotes.UseVisualStyleBackColor = True
        '
        'frmOrders
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1206, 544)
        Me.Controls.Add(Me.btnNotes)
        Me.Controls.Add(Me.btnPrtPck)
        Me.Controls.Add(Me.btnVol)
        Me.Controls.Add(Me.lstPrtPick)
        Me.Controls.Add(Me.txtFax)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btnSaveApp)
        Me.Controls.Add(Me.wbAcctVol)
        Me.Controls.Add(Me.lblRecvDate)
        Me.Controls.Add(Me.lblShipDate)
        Me.Controls.Add(Me.lblPickDate)
        Me.Controls.Add(Me.lblOrdDate)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.lstItemsOrdered)
        Me.Controls.Add(Me.pnlOrders)
        Me.Controls.Add(Me.txtPhone)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.txtInfo)
        Me.Controls.Add(Me.txtPrevOrd)
        Me.Controls.Add(Me.txtNotes)
        Me.Controls.Add(Me.txtPkgs)
        Me.Controls.Add(Me.txtWt)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtTrackNum)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.cmbCourier)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtZIP)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtState)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.txtAddr2)
        Me.Controls.Add(Me.txtAddr)
        Me.Controls.Add(Me.txtContact)
        Me.Controls.Add(Me.txtAcctName)
        Me.Controls.Add(Me.txtAcctNo)
        Me.Controls.Add(Me.txtOrdNo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmOrders"
        Me.Text = "Order Supplies"
        Me.pnlOrders.ResumeLayout(False)
        Me.pnlOrders.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExit As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtOrdNo As TextBox
    Friend WithEvents txtAcctNo As TextBox
    Friend WithEvents txtAcctName As TextBox
    Friend WithEvents txtContact As TextBox
    Friend WithEvents txtAddr As TextBox
    Friend WithEvents txtAddr2 As TextBox
    Friend WithEvents txtCity As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtState As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtZIP As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cmbItem As ComboBox
    Friend WithEvents lblItemDesc As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtQty As TextBox
    Friend WithEvents btnOrder As Button
    Friend WithEvents lstItems As ListBox
    Friend WithEvents Label11 As Label
    Friend WithEvents cmbCourier As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtTrackNum As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtWt As TextBox
    Friend WithEvents txtPkgs As TextBox
    Friend WithEvents txtNotes As RichTextBox
    Friend WithEvents txtPrevOrd As RichTextBox
    Friend WithEvents txtInfo As RichTextBox
    Friend WithEvents lstData As ListBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents lblUnit As Label
    Friend WithEvents pnlOrders As Panel
    Friend WithEvents lstItemsOrdered As ListBox
    Friend WithEvents lblStatus As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents lblOrdDate As Label
    Friend WithEvents lblPickDate As Label
    Friend WithEvents lblShipDate As Label
    Friend WithEvents lblRecvDate As Label
    Friend WithEvents wbAcctVol As WebBrowser
    Friend WithEvents btnSaveApp As Button
    Friend WithEvents txtFax As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents PrintPck As Printing.PrintDocument
    Friend WithEvents lstPrtPick As ListBox
    Friend WithEvents btnVol As Button
    Friend WithEvents OrderBlanks As PrintDialog
    Friend WithEvents btnPrtPck As Button
    Friend WithEvents btnNotes As Button
End Class
