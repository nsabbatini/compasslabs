﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class SendOuts

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click

        If rbManifest.Checked = True Then

            Call BuildManifest()

            PrintDoc.DefaultPageSettings.Margins.Left = 10
            PrintDoc.DefaultPageSettings.Margins.Right = 10
            PrintDoc.DefaultPageSettings.Margins.Top = 10
            PrintDoc.DefaultPageSettings.Margins.Bottom = 10
            If cmbSO.Text = "Lab Corp" Then
                PrintDoc.DefaultPageSettings.Landscape = True
            Else
                PrintDoc.DefaultPageSettings.Landscape = False
            End If
            PrintDoc.Print()

        ElseIf rbReq.Checked = True Then

            Call BuildReq()

            PrintReq.DefaultPageSettings.Margins.Left = 10
            PrintReq.DefaultPageSettings.Margins.Right = 10
            PrintReq.DefaultPageSettings.Margins.Top = 10
            PrintReq.DefaultPageSettings.Margins.Bottom = 10
            PrintReq.DefaultPageSettings.Landscape = False
            PrintReq.Print()

        End If

        Me.Close()
        Me.Dispose()
    End Sub
    Private Sub btnPreview_Click(sender As System.Object, e As System.EventArgs) Handles btnPreview.Click

        If rbManifest.Checked = True Then

            Call BuildManifest()

            PrintDoc.DefaultPageSettings.Margins.Left = 10
            PrintDoc.DefaultPageSettings.Margins.Right = 10
            PrintDoc.DefaultPageSettings.Margins.Top = 10
            PrintDoc.DefaultPageSettings.Margins.Bottom = 10
            If cmbSO.Text = "Lab Corp" Then
                PrintDoc.DefaultPageSettings.Landscape = True
            Else
                PrintDoc.DefaultPageSettings.Landscape = False
            End If
            PrintPreview.ShowDialog()

        ElseIf rbReq.Checked = True Then

            Call BuildReq()

            PrintReq.DefaultPageSettings.Margins.Left = 10
            PrintReq.DefaultPageSettings.Margins.Right = 10
            PrintReq.DefaultPageSettings.Margins.Top = 10
            PrintReq.DefaultPageSettings.Margins.Bottom = 10
            PrintReq.DefaultPageSettings.Landscape = False
            PrintPreviewReq.ShowDialog()

        End If

        Me.Close()
        Me.Dispose()

    End Sub

    Public Sub BuildReq()
        Dim Sel As String, p As String, c As Integer = 1, SO As String = "", TC As String = ""
        Select Case cmbSO.Text
            Case "Lab Corp" : SO = "LC"
            Case "Cleveland Heart Lab" : SO = "CHL"
            Case "AEL" : SO = "AEL"
            Case "Delta Medical Center" : SO = "DMC"
        End Select
        Sel = "Select A.PSPECNO,PLNAME+', '+PFNAME+' '+PMNAME,Substring(PDOB,5,2)+'/'+Substring(PDOB,7,2)+'/'+Substring(PDOB,1,4),SEX,Substring(PSCDTA,5,2)+'/'+Substring(PSCDTA,7,2)+'/'+Substring(PSCDTA,1,4),SOLAB,A.TC,TNAME " & _
            "From POCTC A, PSPEC B, XSO C, PDEM D, TC E "

        If txtTray.Text <> "ALL" Then Sel = Sel & ", XTRAYS F "

        Sel = Sel & "Where A.PSPECNO = B.PSPECNO And PSACT = 1 " & _
            "And Substring(PSRCVDT,5,2)+'/'+Substring(PSRCVDT,7,2)+'/'+Substring(PSRCVDT,1,4)='" & DateTimePicker1.Text & "' And A.TC=C.TC And A.TC=E.TC " & _
            "And TACT=1 And B.PUID=D.PUID And B.PEFDT=D.PEFDT And A.ADSQ1=B.ADSQ1 And PSRCVDT Between STARTDT And ENDDT And SOLAB='" & SO & "'"

        If txtTray.Text <> "ALL" Then Sel = Sel & " And TRAY='" & txtTray.Text & "' And A.PSPECNO=F.PSPECNO"

        Sel = Sel & " Order By SOLAB, PSRCVDT"
        '"And TACT=1 And B.PUID=D.PUID And B.PEFDT=D.PEFDT And A.ADSQ1=B.ADSQ1 And PSRCVDT Between STARTDT And ENDDT And TRAY='" & txtTray.Text & "' Order By SOLAB, PSRCVDT"
        lstData.Items.Clear()
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            TC = GetXTC(rs(6), SO) ': If rs(6) = "4014" Then TC = "884318"
            p = Space(2) & CLSLIMS.RJ(rs(0), 12) & Space(3) & CLSLIMS.LJ(rs(1), 27) & Space(3) & CLSLIMS.RJ(rs(2), 3) & Space(3) & rs(3) & Space(3) & CLSLIMS.RJ(rs(4), 10) & Space(5) & rs(5) & Space(5) & CLSLIMS.LJ(TC, 7) & Space(1) & CLSLIMS.LJ(rs(7), 25)
            lstData.Items.Add(p)
            c = c + 1
        Loop
        rs.Close()
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub BuildManifest()
        Dim Sel As String, p As String, c As Integer = 1, SO As String = "", TC As String = ""
        Select Case cmbSO.Text
            Case "Lab Corp" : SO = "LC"
            Case "Cleveland Heart Lab" : SO = "CHL"
            Case "AEL" : SO = "AEL"
            Case "Delta Medical Center" : SO = "DMC"
        End Select
        Sel = "Select A.PSPECNO,PLNAME+', '+PFNAME+' '+PMNAME,Substring(PDOB,5,2)+'/'+Substring(PDOB,7,2)+'/'+Substring(PDOB,1,4),SEX,Substring(PSCDTA,5,2)+'/'+Substring(PSCDTA,7,2)+'/'+Substring(PSCDTA,1,4),SOLAB,A.TC,TNAME " & _
            "From POCTC A, PSPEC B, XSO C, PDEM D, TC E "

        If txtTray.Text <> "ALL" Then Sel = Sel & ", XTRAYS F "

        Sel = Sel & "Where A.PSPECNO = B.PSPECNO And PSACT = 1 " & _
            "And Substring(PSRCVDT,5,2)+'/'+Substring(PSRCVDT,7,2)+'/'+Substring(PSRCVDT,1,4)='" & DateTimePicker1.Text & "' And A.TC=C.TC And A.TC=E.TC " & _
            "And TACT=1 And B.PUID=D.PUID And B.PEFDT=D.PEFDT And A.ADSQ1=B.ADSQ1 And PSRCVDT Between STARTDT And ENDDT And SOLAB='" & SO & "'"

        If txtTray.Text <> "ALL" Then Sel = Sel & " And TRAY='" & txtTray.Text & "' And A.PSPECNO=F.PSPECNO"
        'Sel = Sel & " and a.pspecno in ('10000162','10000163')"
        Sel = Sel & " Order By SOLAB, PSRCVDT"
        '"And TACT=1 And B.PUID=D.PUID And B.PEFDT=D.PEFDT And A.ADSQ1=B.ADSQ1 And PSRCVDT Between STARTDT And ENDDT And TRAY='" & txtTray.Text & "' Order By SOLAB, PSRCVDT"
        lstData.Items.Clear()
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            TC = GetXTC(rs(6), SO) ': If rs(6) = "4014" Then TC = "884318"
            p = Space(2) & CLSLIMS.RJ(rs(0), 12) & Space(3) & CLSLIMS.LJ(rs(1), 27) & Space(3) & CLSLIMS.RJ(rs(2), 3) & Space(3) & rs(3) & Space(3) & CLSLIMS.RJ(rs(4), 10) & Space(5) & rs(5) & Space(5) & CLSLIMS.LJ(TC, 7) & Space(1) & CLSLIMS.LJ(rs(7), 25)
            lstData.Items.Add(p)
            c = c + 1
        Loop
        rs.Close()
        Conn.Close()
        Conn.Dispose()
    End Sub


    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 28
        If cmbSO.Text = "Lab Corp" Then
            PageLngth = 6
        End If

        Static PageNo As Integer = 1
        Dim y As Integer, PrevSOLAB As String = "", SOLAB As String = ""
        e.Graphics.DrawString(cmbSO.Text, Font2, Brushes.Black, 330, 50)
        'e.Graphics.DrawString("AEL", Font2, Brushes.Black, 330, 50)
        e.Graphics.DrawString("Send Out Specimens" & IIf(cmbSO.Text = "Lab Corp", " - Lab Corp Account #: 41005270", "") & IIf(cmbSO.Text = "AEL", " - AEL Account #: CLSTN", ""), Font3, Brushes.Black, 150, 100)
        'e.Graphics.DrawString("Send Out Specimens", Font2, Brushes.Black, 250, 100)
        e.Graphics.DrawString("For " & DateTimePicker1.Text, Font3, Brushes.Black, 25, 158)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 690, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        If cmbSO.Text = "Lab Corp" Then e.Graphics.DrawString("CLIENT BILL", Font3, Brushes.Black, 850, 48)
        e.Graphics.DrawString("                                             Date of        Collection   SendOut", Font, Brushes.Black, 25, 185)
        e.Graphics.DrawString("Specimen #     Patient Name                   Birth     Sex     Date       Lab         Tests", Font, Brushes.Black, 25, 200)

        e.Graphics.DrawLine(Pens.Black, 25, 180, 1075, 180)   'Horizonal line above headings
        e.Graphics.DrawLine(Pens.Black, 25, 220, 1075, 220)   'Horizonal line under headings

        e.Graphics.DrawLine(Pens.Black, 108, 180, 108, 1080)   'Veritcal line 2
        e.Graphics.DrawLine(Pens.Black, 315, 180, 315, 1080)   'Veritcal line 3
        e.Graphics.DrawLine(Pens.Black, 405, 180, 405, 1080)   'Veritcal line 4
        e.Graphics.DrawLine(Pens.Black, 435, 180, 435, 1080)      'Vertical line 1
        e.Graphics.DrawLine(Pens.Black, 520, 180, 520, 1080)   'Veritcal line 5
        e.Graphics.DrawLine(Pens.Black, 590, 180, 590, 1080)   'Veritcal line 6


        Dim c As Integer
        Static intStart As Integer

        c = 1
        If cmbSO.Text = "Lab Corp" Then
            y = 295
        Else
            y = 225
        End If
        For i = intStart To lstData.Items.Count - 1
            'data line
            e.Graphics.DrawString(lstData.Items(i).ToString, Font, Brushes.Black, 1, y)
            e.Graphics.DrawLine(Pens.Black, 25, y + 20, 1075, y + 20) 'Horizonal line after sample
            If cmbSO.Text = "Lab Corp" Then
                y = y + 100
            Else
                y = y + 30
            End If
            c = c + 1
            If c > PageLngth Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i
    End Sub

    Private Sub SendOuts_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DateTimePicker1.Format = DateTimePickerFormat.Custom
        DateTimePicker1.CustomFormat = "MM/dd/yyyy"
        DateTimePicker1.Text = Now
    End Sub
    Public Function GetXTC(ByVal tc As String, solab As String)
        GetXTC = tc
        Dim Sel As String = "Select xreftc from xso where solab='" & solab & "' and tc='" & tc & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetXTC = rs(0)
        End If
        rs.Close()
        Cmd.Dispose()

    End Function

    Private Sub PrintReq_PrintPage(sender As System.Object, e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintReq.PrintPage
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 28
        If cmbSO.Text = "Lab Corp" Then
            PageLngth = 6
        End If

        Static PageNo As Integer = 1
        Dim y As Integer, PrevSOLAB As String = "", SOLAB As String = ""
        e.Graphics.DrawString(cmbSO.Text, Font2, Brushes.Black, 330, 50)
        e.Graphics.DrawString("Send Out Specimens" & IIf(cmbSO.Text = "Lab Corp", " - Lab Corp Account #: 41005270", ""), Font3, Brushes.Black, 150, 100)
        'e.Graphics.DrawString("Send Out Specimens", Font2, Brushes.Black, 250, 100)
        e.Graphics.DrawString("For " & DateTimePicker1.Text, Font3, Brushes.Black, 25, 158)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 690, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        If cmbSO.Text = "Lab Corp" Then e.Graphics.DrawString("CLIENT BILL", Font3, Brushes.Black, 850, 48)
        e.Graphics.DrawString("                                             Date of        Collection   SendOut", Font, Brushes.Black, 25, 185)
        e.Graphics.DrawString("Specimen #     Patient Name                   Birth     Sex     Date       Lab         Tests", Font, Brushes.Black, 25, 200)

        e.Graphics.DrawLine(Pens.Black, 25, 180, 1075, 180)   'Horizonal line above headings
        e.Graphics.DrawLine(Pens.Black, 25, 220, 1075, 220)   'Horizonal line under headings

        e.Graphics.DrawLine(Pens.Black, 108, 180, 108, 1080)   'Veritcal line 2
        e.Graphics.DrawLine(Pens.Black, 315, 180, 315, 1080)   'Veritcal line 3
        e.Graphics.DrawLine(Pens.Black, 405, 180, 405, 1080)   'Veritcal line 4
        e.Graphics.DrawLine(Pens.Black, 435, 180, 435, 1080)      'Vertical line 1
        e.Graphics.DrawLine(Pens.Black, 520, 180, 520, 1080)   'Veritcal line 5
        e.Graphics.DrawLine(Pens.Black, 590, 180, 590, 1080)   'Veritcal line 6


        Dim c As Integer
        Static intStart As Integer

        c = 1
        If cmbSO.Text = "Lab Corp" Then
            y = 295
        Else
            y = 225
        End If
        For i = intStart To lstData.Items.Count - 1
            'data line
            e.Graphics.DrawString(lstData.Items(i).ToString, Font, Brushes.Black, 1, y)
            e.Graphics.DrawLine(Pens.Black, 25, y + 20, 1075, y + 20) 'Horizonal line after sample
            If cmbSO.Text = "Lab Corp" Then
                y = y + 100
            Else
                y = y + 30
            End If
            c = c + 1
            If c > PageLngth Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i

    End Sub
End Class