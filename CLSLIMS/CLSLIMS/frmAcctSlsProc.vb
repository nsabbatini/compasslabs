﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmAcctSlsProc
    Public lvDataSortOrder As String = "Ascending"


    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmAcctSlsProc_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lvData.Sorting = SortOrder.Ascending
        Dim item As ListViewItem = Nothing
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Sel As String = "select acct,aname,asalesrg,c.syslistopt,amisc1,b.syslistopt,astate,aact,amisc2 from acct a " &
                            "left join syslist b on b.syslistval=a.amisc1 and b.syslistkey='PROC' " &
                            "join syslist c on c.syslistval=a.asalesrg and c.syslistkey='ASALESRG' " &
                            "where acct not in ('999','PTSAMPLES','220','S220') and aefdt=(Select max(aefdt) from Acct b where b.acct=a.acct)"
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            item = lvData.Items.Add(rs(0))
            item.SubItems.Add(Replace(Trim(rs(1)), ",", " "))
            item.SubItems.Add(rs(2))
            item.SubItems.Add(rs(3))
            If Not IsDBNull(rs(4)) Then
                item.SubItems.Add(rs(4))
            Else
                item.SubItems.Add("")
            End If
            If Not IsDBNull(rs(5)) Then
                item.SubItems.Add(rs(5))
            Else
                item.SubItems.Add("")
            End If
            If Not IsDBNull(rs(6)) Then
                item.SubItems.Add(rs(6))
            Else
                item.SubItems.Add("")
            End If
            If Not IsDBNull(rs(7)) Then
                item.SubItems.Add(rs(7))
            Else
                item.SubItems.Add("")
            End If
            If Not IsDBNull(rs(8)) Then
                item.SubItems.Add(rs(8))
            Else
                item.SubItems.Add("")
            End If
        Loop
        rs.Close()
        Conn.Close()
        lvData.Sort()
    End Sub

    Private Sub lvData_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles lvData.ColumnClick
        If lvDataSortOrder = "Ascending" Then
            lvData.ListViewItemSorter = New ListViewItemComparer(e.Column, SortOrder.Ascending)
            lvDataSortOrder = "Descending"
        Else
            lvData.ListViewItemSorter = New ListViewItemComparer(e.Column, SortOrder.Descending)
            lvDataSortOrder = "Ascending"
        End If
    End Sub

    Private Sub btnBuildXLS_Click(sender As Object, e As EventArgs) Handles btnBuildXLS.Click
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\AcctSlsProc.csv", False)
        file.WriteLine("Account,Account Name,Salesman,Salesman Name,Processor,Processor Name,State,Active,Renewal Date")
        For i = 0 To lvData.Items.Count - 1
            For j = 0 To 8
                If Not (lvData.Items(i).SubItems(j) Is Nothing) Then file.Write(lvData.Items(i).SubItems(j).Text & ",")
            Next j
            file.WriteLine()
        Next i
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\AcctSlsProc.csv")
        Me.Close()
        Me.Dispose()
    End Sub
End Class
Public Class ListViewItemComparer
        Implements IComparer
        Private col As Integer, order As SortOrder
        Public Sub New()
            col = 0
            order = SortOrder.Ascending
        End Sub
        Public Sub New(ByVal column As Integer, ByVal order As SortOrder)
            col = column
            Me.order = order
        End Sub
        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Compare = Nothing
            Try
                Dim returnVal As Integer = -1
                returnVal = [String].Compare(CType(x, ListViewItem).SubItems(col).Text, CType(y, ListViewItem).SubItems(col).Text)
                If order = SortOrder.Descending Then
                    returnVal *= -1
                End If
                Return returnVal
            Catch ex As Exception
                Debug.Print(Date.Now & " Compare error " & ex.Message)
            End Try
        End Function
    End Class

