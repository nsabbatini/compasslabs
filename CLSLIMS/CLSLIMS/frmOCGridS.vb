﻿Imports CLSLIMS.CLSLIMS
Imports System.Data.SqlClient
Public Class frmOCGridS
    Public Sub GetDefOC()
        ClearCLick()
        Dim OC As String = "", TstMethod As String = ""
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = conn
        Dim rs As SqlDataReader
        Cmd.CommandText = "Select OC,ACCTOCx From ACCT A, ACCTOC B Where A.ACCT=B.ACCT And A.AEFDT=B.AEFDT And AACT=1 And A.ACCT='" & CLSLIMS.AcctNum & "' Order By ACCTOCx"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            OC = rs(0)
            Select Case OC
                Case "S349000" : cbOralAlk.Checked = True
                Case "S352000" : cbOralAmp.Checked = True
                Case "S354000" : cbOralADSer.Checked = True
                Case "S355000" : cbOralAntiTri.Checked = True
                Case "S356000" : cbOralAnti.Checked = True
                Case "S357000" : cbOralAntiEpi.Checked = True
                Case "S358000" : cbOralAntiPsych.Checked = True
                Case "S361000" : cbOralBenz.Checked = True
                Case "S363000" : cbOralBup.Checked = True
                Case "S365000" : cbOralTHC.Checked = True
                Case "S306020" : cbOralCOC.Checked = True
                Case "S369000" : cbOralFent.Checked = True
                Case "S370000" : cbOralGaba.Checked = True
                Case "S371000" : cbOralHero.Checked = True
                Case "S305040" : cbOralMtd.Checked = True
                Case "S375000" : cbOralMethAmp.Checked = True
                Case "S377000" : cbOralMethy.Checked = True
                Case "S380000" : cbOralOpi.Checked = True
                Case "S382000" : cbOralOpiAnal.Checked = True
                Case "S383000" : cbOralOxy.Checked = True
                Case "S385000" : cbOralPhen.Checked = True
                Case "S387000" : cbOralPregaba.Checked = True
                Case "S388000" : cbOralPro.Checked = True
                Case "S390000" : cbOralSedHyp.Checked = True
                Case "S391000" : cbOralSkel.Checked = True
                Case "S393000" : cbOralTap.Checked = True
                Case "S305120" : cbOralTram.Checked = True
                    'Case "800000" : cbXREF.Checked = True
            End Select
        Loop
        rs.Close()
        Cmd.CommandText = "Select AAVRSPIN,AMISC2,ADFLTRFMT,ABCONT From ACCT Where AACT=1 And ACCT='" & CLSLIMS.AcctNum & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            TstMethod = rs(0)
            lblProfileDate.Text = rs(1)
        End If
        rs.Close()
        conn.Close()
    End Sub
    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        PrintForm1.Print()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Public Sub ClearCLick()
        cbOralADSer.Checked = False
        cbOralAlk.Checked = False
        cbOralAmp.Checked = False
        cbOralAntiEpi.Checked = False
        cbOralAntiPsych.Checked = False
        cbOralBenz.Checked = False
        cbOralBup.Checked = False
        cbOralCOC.Checked = False
        cbOralFent.Checked = False
        cbOralGaba.Checked = False
        cbOralHero.Checked = False
        cbOralMethAmp.Checked = False
        cbOralMethy.Checked = False
        cbOralMtd.Checked = False
        cbOralOpi.Checked = False
        cbOralOpiAnal.Checked = False
        cbOralOxy.Checked = False
        cbOralPhen.Checked = False
        cbOralPregaba.Checked = False
        cbOralPro.Checked = False
        cbOralSedHyp.Checked = False
        cbOralSkel.Checked = False
        cbOralTap.Checked = False
        cbOralTHC.Checked = False
        cbOralTram.Checked = False
        'cbXREF.Checked = False
        'dtpProfDt.Text = Now
    End Sub
End Class