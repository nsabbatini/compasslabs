﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Sql.Database
Public Class frmViewPO
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty

    Private Sub frmViewPO_Load(sender As Object, e As EventArgs) Handles Me.Load
        LoadLookup()
    End Sub
    Private Sub LoadLookup()

        Dim results = GetPoNumbers()

        lookupPo.Properties.DataSource = results
        lookupPo.Properties.DisplayMember = "PoNumber"
        lookupPo.Properties.ValueMember = "PId"



    End Sub

    Private Sub lookupPo_EditValueChanged(sender As Object, e As EventArgs) Handles lookupPo.EditValueChanged

    End Sub

    Private Function GetPoNumbers() As Object

        Try

            Using purchaseOrders As New PurchaseOrders()
                _fileName = _fileDir & GetPurchaseOrdersSql
                purchaseOrders._sqlText = ReadSqlFile(_fileName)
                Return purchaseOrders.GetPurchaseOrder.Where(Function(x) x.Status = POStatus.Ordered).OrderByDescending(Function(o) o.PId).ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderEdit.GetPoNumbers", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try

    End Function
End Class