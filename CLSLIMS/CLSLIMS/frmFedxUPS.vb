﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmFedxUPS
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        If txtFileName.Text = "" Then
            MsgBox("Select file to import.")
            Exit Sub
        End If
        If Not System.IO.File.Exists(txtFileName.Text) Then
            MsgBox("File not found.")
            Exit Sub
        End If
        If Not rbFedx.Checked And Not rbUPS.Checked Then
            MsgBox("Must select Fedex or UPS")
            Exit Sub
        End If
        Dim myFile As New Microsoft.VisualBasic.FileIO.TextFieldParser(txtFileName.Text)
        myFile.TextFieldType = FileIO.FieldType.Delimited
        myFile.SetDelimiters(",")
        Dim rec As String()
        Dim f As String = "", cnt As Integer = 0, cnt2 As Integer = 0
        lstInfo.Items.Add("Running. . .") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        Do While Not myFile.EndOfData
            rec = myFile.ReadFields()
            cnt = cnt + 1
            If rbFedx.Checked Then
                Debug.Print(rec(8))
                If Not CheckFedex(CLSLIMS.Piece(rec(8), ".", 1)) Then
                    cnt2 = cnt2 + 1
                    lstInfo.Items.Add(CLSLIMS.Piece(rec(8), ".", 1) & "," & rec(10) & "," & rec(13) & "," & rec(40) & "," & rec(44) & "," & rec(45)) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
                End If
            ElseIf rbUPS.Checked Then
                Debug.Print(rec(13))
                If Not CheckFedex(CLSLIMS.Piece(rec(13), ".", 1)) Then
                    cnt2 = cnt2 + 1
                    lstInfo.Items.Add(CLSLIMS.Piece(rec(13), ".", 1) & "," & rec(11) & "," & rec(13) & "," & rec(74) & "," & rec(75) & "," & rec(78) & "," & rec(79)) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
                End If
            End If
        Loop
        myFile.Close()
        lstInfo.Items.Add("Done.") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        lstInfo.Items.Add("Reconciled " & cnt & " records. " & cnt2 & " unidentified.") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1

    End Sub
    Public Function CheckFedex(trk As String) As Boolean
        CheckFedex = False
        Dim ConnMySqlFin As New MySqlConnection("Server=192.168.65.100;Port=3306;Database=compass;Uid=nsabbatini;Pwd=Compass1910;")
        ConnMySqlFin.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySqlFin
        Cmd.CommandText = "Select acct from inbound where inbtrknum='" & trk & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            CheckFedex = True
        End If
        rs.Close()
        If Not CheckFedex Then
            Cmd.CommandText = "Select acct from tracking where tracknum='" & trk & "'"
            rs = Cmd.ExecuteReader
            If rs.Read() Then
                CheckFedex = True
            End If
            rs.Close()
        End If
        If Not CheckFedex Then
            Cmd.CommandText = "Select acct from orders where trackingnum='" & trk & "'"
            rs = Cmd.ExecuteReader
            If rs.Read() Then
                CheckFedex = True
            End If
            rs.Close()
        End If
        ConnMySqlFin.Close()
    End Function

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click

        OpenFileDialog1.Title = "Open File Dialog"
        OpenFileDialog1.InitialDirectory = "C:\"
        OpenFileDialog1.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        OpenFileDialog1.FilterIndex = 2
        OpenFileDialog1.RestoreDirectory = True

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            txtFileName.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub frmFedxUPS_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Debug.Print(CLSLIMS.UID)
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
        End If
        If CLSLIMS.UID = "" Or (CLSLIMS.UID <> "MARCIN.BARTCZAK" And CLSLIMS.UID <> "KBMERRITT" And CLSLIMS.UID <> "NED.SABBATINI") Then
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Dim printer As New PCPrint()
        'Set the font we want to use
        printer.PrinterFont = New Font("Courier New", 10)
        'Set Margins
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        printer.DefaultPageSettings.Margins = mar

        Dim txt As String = ""
        For i = 0 To lstInfo.Items.Count - 1
            txt = txt & vbCrLf & lstInfo.Items(i)
        Next i
        printer.TextToPrint = " " & txt
        'Issue print command
        printer.Print()
    End Sub
End Class