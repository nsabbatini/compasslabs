﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMainOrderForm
    Inherits DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMainOrderForm))
        Me.FluentDesignFormContainer1 = New DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer()
        Me.pnlMainOrder = New DevExpress.XtraEditors.PanelControl()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.FluentFormDefaultManager1 = New DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager(Me.components)
        Me.AccordionControl1 = New DevExpress.XtraBars.Navigation.AccordionControl()
        Me.AccordionControlElement1 = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnNewOrder = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnHistory = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnAddNote = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnVolume = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnPrint = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.AccordionControlSeparator1 = New DevExpress.XtraBars.Navigation.AccordionControlSeparator()
        Me.ctlActions = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnApprove = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnExit = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.FluentDesignFormControl1 = New DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl()
        Me.tmrClient = New System.Windows.Forms.Timer(Me.components)
        Me.FluentDesignFormContainer1.SuspendLayout()
        CType(Me.pnlMainOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FluentFormDefaultManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccordionControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FluentDesignFormControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FluentDesignFormContainer1
        '
        Me.FluentDesignFormContainer1.Controls.Add(Me.pnlMainOrder)
        Me.FluentDesignFormContainer1.Controls.Add(Me.PictureBox1)
        Me.FluentDesignFormContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FluentDesignFormContainer1.Location = New System.Drawing.Point(215, 31)
        Me.FluentDesignFormContainer1.Margin = New System.Windows.Forms.Padding(2)
        Me.FluentDesignFormContainer1.Name = "FluentDesignFormContainer1"
        Me.FluentDesignFormContainer1.Size = New System.Drawing.Size(848, 507)
        Me.FluentDesignFormContainer1.TabIndex = 0
        '
        'pnlMainOrder
        '
        Me.pnlMainOrder.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainOrder.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainOrder.Margin = New System.Windows.Forms.Padding(2)
        Me.pnlMainOrder.Name = "pnlMainOrder"
        Me.pnlMainOrder.Size = New System.Drawing.Size(848, 507)
        Me.pnlMainOrder.TabIndex = 2
        Me.pnlMainOrder.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Olive
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(848, 507)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'FluentFormDefaultManager1
        '
        Me.FluentFormDefaultManager1.DockingEnabled = False
        Me.FluentFormDefaultManager1.Form = Me
        '
        'AccordionControl1
        '
        Me.AccordionControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.AccordionControl1.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {Me.AccordionControlElement1})
        Me.AccordionControl1.Location = New System.Drawing.Point(0, 31)
        Me.AccordionControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.AccordionControl1.Name = "AccordionControl1"
        Me.AccordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Touch
        Me.AccordionControl1.Size = New System.Drawing.Size(215, 507)
        Me.AccordionControl1.TabIndex = 1
        Me.AccordionControl1.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu
        '
        'AccordionControlElement1
        '
        Me.AccordionControlElement1.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {Me.btnNewOrder, Me.btnHistory, Me.btnAddNote, Me.btnVolume, Me.btnPrint, Me.AccordionControlSeparator1, Me.ctlActions})
        Me.AccordionControlElement1.Expanded = True
        Me.AccordionControlElement1.Name = "AccordionControlElement1"
        Me.AccordionControlElement1.Text = "Order Actions"
        '
        'btnNewOrder
        '
        Me.btnNewOrder.ImageOptions.Image = CType(resources.GetObject("btnNewOrder.ImageOptions.Image"), System.Drawing.Image)
        Me.btnNewOrder.Name = "btnNewOrder"
        Me.btnNewOrder.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnNewOrder.Text = "New Order"
        '
        'btnHistory
        '
        Me.btnHistory.ImageOptions.Image = CType(resources.GetObject("btnHistory.ImageOptions.Image"), System.Drawing.Image)
        Me.btnHistory.Name = "btnHistory"
        Me.btnHistory.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnHistory.Text = "Order History"
        '
        'btnAddNote
        '
        Me.btnAddNote.Enabled = False
        Me.btnAddNote.ImageOptions.Image = CType(resources.GetObject("btnAddNote.ImageOptions.Image"), System.Drawing.Image)
        Me.btnAddNote.Name = "btnAddNote"
        Me.btnAddNote.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnAddNote.Text = "Add Note"
        '
        'btnVolume
        '
        Me.btnVolume.Enabled = False
        Me.btnVolume.ImageOptions.Image = CType(resources.GetObject("btnVolume.ImageOptions.Image"), System.Drawing.Image)
        Me.btnVolume.Name = "btnVolume"
        Me.btnVolume.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnVolume.Text = "Volume"
        '
        'btnPrint
        '
        Me.btnPrint.Enabled = False
        Me.btnPrint.ImageOptions.Image = CType(resources.GetObject("btnPrint.ImageOptions.Image"), System.Drawing.Image)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnPrint.Text = "Print Pick List"
        '
        'AccordionControlSeparator1
        '
        Me.AccordionControlSeparator1.Name = "AccordionControlSeparator1"
        '
        'ctlActions
        '
        Me.ctlActions.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {Me.btnApprove, Me.btnExit})
        Me.ctlActions.Expanded = True
        Me.ctlActions.Name = "ctlActions"
        Me.ctlActions.Text = "Actions"
        '
        'btnApprove
        '
        Me.btnApprove.ImageOptions.Image = CType(resources.GetObject("btnApprove.ImageOptions.Image"), System.Drawing.Image)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnApprove.Text = "Approve Order"
        '
        'btnExit
        '
        Me.btnExit.ImageOptions.Image = CType(resources.GetObject("btnExit.ImageOptions.Image"), System.Drawing.Image)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnExit.Text = "Exit"
        '
        'FluentDesignFormControl1
        '
        Me.FluentDesignFormControl1.FluentDesignForm = Me
        Me.FluentDesignFormControl1.Location = New System.Drawing.Point(0, 0)
        Me.FluentDesignFormControl1.Manager = Me.FluentFormDefaultManager1
        Me.FluentDesignFormControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.FluentDesignFormControl1.Name = "FluentDesignFormControl1"
        Me.FluentDesignFormControl1.Size = New System.Drawing.Size(1063, 31)
        Me.FluentDesignFormControl1.TabIndex = 2
        Me.FluentDesignFormControl1.TabStop = False
        '
        'frmMainOrderForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1063, 538)
        Me.ControlContainer = Me.FluentDesignFormContainer1
        Me.Controls.Add(Me.FluentDesignFormContainer1)
        Me.Controls.Add(Me.AccordionControl1)
        Me.Controls.Add(Me.FluentDesignFormControl1)
        Me.FluentDesignFormControl = Me.FluentDesignFormControl1
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmMainOrderForm"
        Me.NavigationControl = Me.AccordionControl1
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Order Client Supplies"
        Me.FluentDesignFormContainer1.ResumeLayout(False)
        CType(Me.pnlMainOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FluentFormDefaultManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccordionControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FluentDesignFormControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FluentDesignFormContainer1 As DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer
    Friend WithEvents AccordionControl1 As DevExpress.XtraBars.Navigation.AccordionControl
    Friend WithEvents AccordionControlElement1 As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents FluentDesignFormControl1 As DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl
    Friend WithEvents FluentFormDefaultManager1 As DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager
    Friend WithEvents btnNewOrder As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnHistory As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents AccordionControlSeparator1 As DevExpress.XtraBars.Navigation.AccordionControlSeparator
    Friend WithEvents ctlActions As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnApprove As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnPrint As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnExit As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents pnlMainOrder As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnAddNote As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnVolume As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents tmrClient As Timer
End Class
