﻿Imports Compass.MySql.Database
Imports Compass.Global
Public Class frmNewVendor
    Private _dirPathToFIle As String = System.AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private Sub btnSave_Click_1(sender As Object, e As EventArgs) Handles btnSave.Click
        _fileName = _fileDir & SaveVendorsSql

        Using save As New SetNewVendors() With {
            .VendorName = txtVendor.Text.Trim,
            .Address = txtAddress.Text,
            .Address2 = txtAddress2.Text,
            .City = txtCity.Text,
            .State = txtState.Text,
            .Zip = txtZipCode.Text,
            .Contact = txtContactInfo.Text,
            .Phone = txtPhone.Text,
            .Fax = txtFax.Text,
            .Notes = txtNotes.Text,
            ._sqlText = ReadSqlFile(_fileName),
            .VendorId = txtVendor.Text.Substring(0, 4)
        }

            If save.SaveNewVendor() Then
                Dim x = MessageBox.Show(Me, "Would you like to add inventory items to this vendor?.", "Add new inventory", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If x = DialogResult.Yes Then
                    ''load items form
                Else
                    Me.Close()
                End If

            Else
                MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            End If
        End Using
    End Sub

    Private Sub btnCancel_Click_1(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub
End Class