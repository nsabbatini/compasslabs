﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmCorrectFlag

    Private Sub frmCorrectFlag_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "SUSAN.SHUMAKER" And CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "EMILY.ALEX" And CLSLIMS.UID <> "ROCKY.HAMMONS" And CLSLIMS.UID <> "MARCIN.BARTCZAK" And CLSLIMS.UID <> "LEAH.RAST" And CLSLIMS.UID <> "JAY.TOLAR" Then
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub txtSpecID_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtSpecID.KeyPress
        If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 10 Then
            Exit Sub
        End If
        'Check to see if Specimen Number in use
        If Not CheckSpecNo(txtSpecID.Text) Then
            MsgBox(txtSpecID.Text & " Specimen ID not found.")
            txtSpecID.Text = ""
            txtSpecID.Focus()
            Exit Sub
        End If
        Dim Sel As String = "select pfname,plname,pdob,sex,psrcvdt from pdem a, pspec b where pspecno='" & txtSpecID.Text & "' and a.puid=b.puid and a.pefdt=b.pefdt and psact=1 and pact=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            lstInfo.Items.Add(rs(1) & ", " & rs(0))
            lstInfo.Items.Add("DOB: " & rs(2) & "   Gender: " & rs(3))
            lstInfo.Items.Add("Received: " & rs(4))
            txtRC.Focus()
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()

    End Sub

    Public Function CheckSpecNo(ByVal specno As String) As Boolean
        Dim xSel As String = "Select PSPECNO From PSPEC Where PSACT=1 And PSPECNO='" & specno & "'"
        CheckSpecNo = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                CheckSpecNo = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Sub txtRC_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtRC.KeyPress
        If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 10 Then
            Exit Sub
        End If
        Dim Sel As String = "select count(*) from pres where pspecno='" & txtSpecID.Text & "' and pract=1 and rc='" & txtRC.Text & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If rs(0) = 0 Then
                MsgBox("Result code not found on specimen")
                txtRC.Text = ""
                txtRC.Focus()
            Else
                lstInfo.Items.Add("Result: " & GetRC(txtRC.Text))
                txtFlag.Focus()
            End If
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()

    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim Upd As String = "Update pres set prflag='" & txtFlag.Text & "' where pspecno='" & txtSpecID.Text & "' and rc='" & txtRC.Text & "' and pract=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = Upd
        Cmd.ExecuteNonQuery()
        Cmd.Dispose()
        Conn.Close()
        txtSpecID.Text = ""
        txtRC.Text = ""
        txtFlag.Text = ""
        txtSpecID.Focus()
        lstInfo.Items.Clear()

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub
    Public Function GetRC(ByVal rc As String) As String
        Dim xSel As String = "Select Rname From RC Where RACT=1 And Rc='" & rc & "'"
        GetRC = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetRC = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
End Class