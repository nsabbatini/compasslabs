﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports DevExpress.XtraEditors
Imports System.Deployment.Application
Public Class frmGetQuedOrdersb
    Private ELEMENT_WIDTH As Integer = 250 '123    'width of a button
    Private ELEMENT_HEIGHT As Integer = 110    'height of a button
    Private NUM_COLUMNS As Integer = 7
    Private NUM_ROWS As Integer = 8
    Private dynButton As SimpleButton
    Private pt As Point
    Dim intDynI As Integer = 0
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private mfrm As New frmShowOrders
    Private _orderId As Int16
    Private Sub frmGetQuedOrdersb_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Using frm As New Login
                frm.ShowDialog()
            End Using
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        'Timer1.Enabled = True
        InitNeedApproved()
        InitOrdersToPrint()
        'InitOrdersReqs()
        'InitNeedsPicked()
        'InitOrdersToBeShipped()

    End Sub
    Private Sub InitNeedApproved()
        GetScreenResolution()
        ELEMENT_WIDTH = 230
        ELEMENT_HEIGHT = 110
        intDynI = 0

        Dim orderitems = GetOrdersInQue()

        For Each items In orderitems
            dynButton = New SimpleButton
            dynButton.LookAndFeel.SetSkinStyle("Liquid Sky")
            pt = New Point(ELEMENT_WIDTH * (intDynI Mod NUM_COLUMNS) + 3, ELEMENT_HEIGHT * (intDynI \ NUM_COLUMNS) + 31)
            dynButton.Location = pt
            dynButton.Width = ELEMENT_WIDTH
            dynButton.Height = ELEMENT_HEIGHT
            dynButton.ImageOptions.ImageList = imgList1
            dynButton.ImageOptions.ImageToTextAlignment = ImageAlignToText.TopCenter


            dynButton.ImageOptions.ImageIndex = 0
            dynButton.Text = "Order #:" & items.GetType().GetProperty("OrderNumber")?.GetValue(items) & vbCrLf & items.GetType().GetProperty("Aname")?.GetValue(items)
            dynButton.Name = items.GetType().GetProperty("OrderNumber")?.GetValue(items)
            dynButton.Tag = items.GetType().GetProperty("OrderId")?.GetValue(items)
            dynButton.TabStop = False

            AddHandler dynButton.Click, AddressOf btnItem_Click
            AddHandler dynButton.MouseEnter, AddressOf btnItem_MouseEnter
            AddHandler dynButton.MouseLeave, AddressOf btnItem_MouseLeave
            AddHandler dynButton.MouseDown, AddressOf btnItem_MouseClick


            grp1.Controls.Add(dynButton)
            intDynI += 1
        Next


    End Sub



    Private Sub InitOrdersToPrint()
        GetScreenResolution()
        ELEMENT_WIDTH = 230
        ELEMENT_HEIGHT = 110
        intDynI = 0

        Dim orderitems = GetOrdersForPrint()

        For Each items In orderitems
            dynButton = New SimpleButton
            dynButton.LookAndFeel.SetSkinStyle("Liquid Sky")
            pt = New Point(ELEMENT_WIDTH * (intDynI Mod NUM_COLUMNS) + 3, ELEMENT_HEIGHT * (intDynI \ NUM_COLUMNS) + 31)
            dynButton.Location = pt
            dynButton.Width = ELEMENT_WIDTH
            dynButton.Height = ELEMENT_HEIGHT
            dynButton.ImageOptions.ImageList = imgList1
            dynButton.ImageOptions.ImageToTextAlignment = ImageAlignToText.TopCenter


            dynButton.ImageOptions.ImageIndex = 1
            dynButton.Text = "Order #:" & items.GetType().GetProperty("OrderNumber")?.GetValue(items) & vbCrLf & items.GetType().GetProperty("ClientName")?.GetValue(items)
            dynButton.Name = items.GetType().GetProperty("OrderNumber")?.GetValue(items)
            dynButton.Tag = items.GetType().GetProperty("OrderId")?.GetValue(items)
            dynButton.TabStop = False

            AddHandler dynButton.Click, AddressOf btnPrint_Click
            AddHandler dynButton.MouseEnter, AddressOf btnPrint_MouseEnter
            AddHandler dynButton.MouseLeave, AddressOf btnPrint_MouseLeave

            grp2.Controls.Add(dynButton)
            intDynI += 1
        Next


    End Sub



    Private Sub InitOrdersReqs()
        GetScreenResolution()
        ELEMENT_WIDTH = 230
        ELEMENT_HEIGHT = 110
        intDynI = 0

        Dim orderitems = GetOrdersForReqs()

        For Each items In orderitems
            dynButton = New SimpleButton
            dynButton.LookAndFeel.SetSkinStyle("Liquid Sky")
            pt = New Point(ELEMENT_WIDTH * (intDynI Mod NUM_COLUMNS) + 3, ELEMENT_HEIGHT * (intDynI \ NUM_COLUMNS) + 31)
            dynButton.Location = pt
            dynButton.Width = ELEMENT_WIDTH
            dynButton.Height = ELEMENT_HEIGHT
            dynButton.ImageOptions.ImageList = imgList1
            dynButton.ImageOptions.ImageToTextAlignment = ImageAlignToText.TopCenter


            dynButton.ImageOptions.ImageIndex = 2
            dynButton.Text = "Order #:" & items.GetType().GetProperty("OrderNum")?.GetValue(items) & vbCrLf & items.GetType().GetProperty("ClientName")?.GetValue(items)
            dynButton.Name = items.GetType().GetProperty("OrderNum")?.GetValue(items)
            dynButton.Tag = items.GetType().GetProperty("OrderNum")?.GetValue(items)
            dynButton.TabStop = False

            AddHandler dynButton.Click, AddressOf btnReqs_Click
            AddHandler dynButton.MouseEnter, AddressOf btnReqs_MouseEnter
            AddHandler dynButton.MouseLeave, AddressOf btnReqs_MouseLeave

            grp3.Controls.Add(dynButton)
            intDynI += 1
        Next


    End Sub



    Private Sub InitNeedsPicked()

        GetScreenResolution()
        ELEMENT_WIDTH = 230
        ELEMENT_HEIGHT = 110
        intDynI = 0

        Dim orderitems = GetOrdersForPicking()

        For Each items In orderitems
            dynButton = New SimpleButton
            dynButton.LookAndFeel.SetSkinStyle("Liquid Sky")
            pt = New Point(ELEMENT_WIDTH * (intDynI Mod NUM_COLUMNS) + 3, ELEMENT_HEIGHT * (intDynI \ NUM_COLUMNS) + 31)
            dynButton.Location = pt
            dynButton.Width = ELEMENT_WIDTH
            dynButton.Height = ELEMENT_HEIGHT
            dynButton.ImageOptions.ImageList = imgList1
            dynButton.ImageOptions.ImageToTextAlignment = ImageAlignToText.TopCenter


            dynButton.ImageOptions.ImageIndex = 3
            dynButton.Text = "Order #:" & items.GetType().GetProperty("OrderNum")?.GetValue(items) & vbCrLf & items.GetType().GetProperty("ClientName")?.GetValue(items)
            dynButton.Name = items.GetType().GetProperty("OrderNum")?.GetValue(items)
            dynButton.Tag = items.GetType().GetProperty("OrderNum")?.GetValue(items)
            dynButton.TabStop = False

            AddHandler dynButton.Click, AddressOf btnPick_Click
            AddHandler dynButton.MouseEnter, AddressOf btnPick_MouseEnter
            AddHandler dynButton.MouseLeave, AddressOf btnPick_MouseLeave

            grp4.Controls.Add(dynButton)
            intDynI += 1
        Next

    End Sub



    Private Sub InitOrdersToBeShipped()

        GetScreenResolution()
        ELEMENT_WIDTH = 230
        ELEMENT_HEIGHT = 110
        intDynI = 0

        Dim orderitems = GetOrdersForShipping()

        For Each items In orderitems
            dynButton = New SimpleButton
            dynButton.LookAndFeel.SetSkinStyle("Liquid Sky")
            pt = New Point(ELEMENT_WIDTH * (intDynI Mod NUM_COLUMNS) + 3, ELEMENT_HEIGHT * (intDynI \ NUM_COLUMNS) + 31)
            dynButton.Location = pt
            dynButton.Width = ELEMENT_WIDTH
            dynButton.Height = ELEMENT_HEIGHT
            dynButton.ImageOptions.ImageToTextAlignment = ImageAlignToText.TopCenter

            dynButton.ImageOptions.ImageList = imgList1


            dynButton.ImageOptions.ImageIndex = 4
            dynButton.Text = "Order #:" & items.GetType().GetProperty("OrderNum")?.GetValue(items) & vbCrLf & items.GetType().GetProperty("Aname")?.GetValue(items)
            dynButton.Name = items.GetType().GetProperty("OrderNum")?.GetValue(items)
            dynButton.Tag = items.GetType().GetProperty("OrderNum")?.GetValue(items)
            dynButton.TabStop = False

            AddHandler dynButton.Click, AddressOf btnShip_Click
            AddHandler dynButton.MouseEnter, AddressOf btnShip_MouseEnter
            AddHandler dynButton.MouseLeave, AddressOf btnShip_MouseLeave

            grp5.Controls.Add(dynButton)
            intDynI += 1
        Next
    End Sub



    Private Function GetOrdersInQue() As Object

        Try


            Using getVendors As New OrdersDb()
                _fileName = _fileDir & OrdersSql
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetOrders.Where(Function(x) x.OrderStatus = 1).ToList
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmGetQuedOrdersb.GetOrdersInQue", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function
    Private Function GetOrdersForPrint() As Object
        _fileName = String.Empty

        Try

            Using getVendors As New OrdersDb()
                _fileName = _fileDir & GetOrdersForPrintSql
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetPrintOrdersItems.ToList()
            End Using



        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmGetQuedOrdersb.GetOrdersForPrint", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Function
    Private Function GetOrdersForReqs() As Object
        _fileName = String.Empty

        Try

            Using getVendors As New OrdersDb()
                _fileName = _fileDir & GetOrderForReqsSql
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetPrintOrdersItems.ToList()
            End Using



        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmGetQuedOrdersb.GetOrdersForReqs", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Function
    Private Function GetOrdersForPicking() As Object
        _fileName = String.Empty

        Try

            Using getVendors As New OrdersDb()
                _fileName = _fileDir & GetOrderForPickingSql
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetPrintOrdersItems.ToList()
            End Using



        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmGetQuedOrdersb.GetOrdersForPicking", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Function
    Private Function GetOrdersForShipping() As Object
        _fileName = String.Empty

        Try

            Using getVendors As New OrdersDb()
                _fileName = _fileDir & GetOrderForShippingSql
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetOrders.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmGetQuedOrdersb.GetOrdersForShipping", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Function

    Private Sub GetScreenResolution()

        Dim height = My.Computer.Screen.Bounds.Height
        Dim width = My.Computer.Screen.Bounds.Width

        Select Case width
            Case 1680
                NUM_COLUMNS = 3
                NUM_ROWS = 8
            Case 1280
                NUM_COLUMNS = 3
                NUM_ROWS = 8
            Case 1920
                NUM_COLUMNS = 4
                NUM_ROWS = 8
        End Select

    End Sub
#Region "ButtonEvents"
    Private Sub btnReqs_Click(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)
    End Sub
    Private Sub btnReqs_MouseLeave(sender As Object, e As EventArgs)
        mfrm.Hide()
        mfrm = Nothing
    End Sub

    Private Sub btnReqs_MouseEnter(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)
        If mfrm Is Nothing Then
            mfrm = New frmShowOrders
        End If
        _orderId = btnItem.Tag
        mfrm.OrderId = btnItem.Tag
        mfrm.Show()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)


    End Sub
    Private Sub btnPrint_MouseLeave(sender As Object, e As EventArgs)
        mfrm.Hide()
        mfrm = Nothing
    End Sub

    Private Sub btnPrint_MouseEnter(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)
        If mfrm Is Nothing Then
            mfrm = New frmShowOrders
        End If
        _orderId = btnItem.Tag
        mfrm.OrderId = btnItem.Tag
        mfrm.Show()
    End Sub

    Private Sub btnItem_Click(sender As Object, e As EventArgs)

        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)


    End Sub
    Private Sub btnItem_MouseLeave(sender As Object, e As EventArgs)
        mfrm.Hide()
        mfrm = Nothing

    End Sub

    Private Sub btnItem_MouseEnter(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)
        If mfrm Is Nothing Then
            mfrm = New frmShowOrders
        End If
        _orderId = btnItem.Tag
        mfrm.OrderId = btnItem.Tag
        mfrm.Show()


    End Sub
    Private Sub btnItem_MouseClick(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Right Then
            Dim x = MessageBox.Show(Me, "Do you want to approve Order?", "Order", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = DialogResult.Yes Then
                _fileName = _fileDir & ApproveOrderedItemsSql
                Using approve As New OrdersDb() With {._sqlText = ReadSqlFile(_fileName)}
                    If approve.ApproveOrder(_orderId) Then
                        InitQue()
                    End If
                End Using
            Else
                'go to order form
            End If
        End If
    End Sub
    Private Sub btnPick_Click(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)

    End Sub
    Private Sub btnPick_MouseLeave(sender As Object, e As EventArgs)

        mfrm.Hide()
        mfrm = Nothing

    End Sub

    Private Sub btnPick_MouseEnter(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)
        If mfrm Is Nothing Then
            mfrm = New frmShowOrders
        End If
        _orderId = btnItem.Tag
        mfrm.OrderId = btnItem.Tag
        mfrm.Show()


    End Sub
    Private Sub btnShip_Click(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)

    End Sub
    Private Sub btnShip_MouseLeave(sender As Object, e As EventArgs)
        mfrm.Hide()
        mfrm = Nothing
    End Sub

    Private Sub btnShip_MouseEnter(sender As Object, e As EventArgs)
        Dim btnItem As SimpleButton

        btnItem = DirectCast(sender, SimpleButton)
        If mfrm Is Nothing Then
            mfrm = New frmShowOrders
        End If
        _orderId = btnItem.Tag
        mfrm.OrderId = btnItem.Tag
        mfrm.Show()
    End Sub
    Private Sub InitQue()
        grp1.Controls.Clear()
        grp2.Controls.Clear()
        grp3.Controls.Clear()
        grp4.Controls.Clear()
        grp5.Controls.Clear()

        InitNeedApproved()
        InitOrdersToPrint()
        InitOrdersReqs()
        InitNeedsPicked()
        InitOrdersToBeShipped()
    End Sub



#End Region
#Region "Timer"
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

    End Sub
#End Region

End Class