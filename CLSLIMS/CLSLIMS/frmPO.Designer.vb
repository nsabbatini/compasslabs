﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRecName = New System.Windows.Forms.TextBox()
        Me.txtCoName = New System.Windows.Forms.TextBox()
        Me.txtAddr1 = New System.Windows.Forms.TextBox()
        Me.txtAddr2 = New System.Windows.Forms.TextBox()
        Me.txtCity = New System.Windows.Forms.TextBox()
        Me.txtState = New System.Windows.Forms.TextBox()
        Me.txtZIP = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtSTPhone = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtSTZIP = New System.Windows.Forms.TextBox()
        Me.txtSTState = New System.Windows.Forms.TextBox()
        Me.txtSTCity = New System.Windows.Forms.TextBox()
        Me.txtSTAddr2 = New System.Windows.Forms.TextBox()
        Me.txtSTAddr1 = New System.Windows.Forms.TextBox()
        Me.txtSTCoName = New System.Windows.Forms.TextBox()
        Me.txtSTRecName = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtComments = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(638, 662)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 31)
        Me.btnExit.TabIndex = 0
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(134, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "TO:"
        '
        'txtRecName
        '
        Me.txtRecName.Location = New System.Drawing.Point(138, 122)
        Me.txtRecName.Name = "txtRecName"
        Me.txtRecName.Size = New System.Drawing.Size(219, 27)
        Me.txtRecName.TabIndex = 2
        '
        'txtCoName
        '
        Me.txtCoName.Location = New System.Drawing.Point(138, 155)
        Me.txtCoName.Name = "txtCoName"
        Me.txtCoName.Size = New System.Drawing.Size(219, 27)
        Me.txtCoName.TabIndex = 3
        '
        'txtAddr1
        '
        Me.txtAddr1.Location = New System.Drawing.Point(138, 188)
        Me.txtAddr1.Name = "txtAddr1"
        Me.txtAddr1.Size = New System.Drawing.Size(219, 27)
        Me.txtAddr1.TabIndex = 4
        '
        'txtAddr2
        '
        Me.txtAddr2.Location = New System.Drawing.Point(138, 221)
        Me.txtAddr2.Name = "txtAddr2"
        Me.txtAddr2.Size = New System.Drawing.Size(219, 27)
        Me.txtAddr2.TabIndex = 5
        '
        'txtCity
        '
        Me.txtCity.Location = New System.Drawing.Point(138, 254)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(102, 27)
        Me.txtCity.TabIndex = 6
        '
        'txtState
        '
        Me.txtState.Location = New System.Drawing.Point(246, 254)
        Me.txtState.Name = "txtState"
        Me.txtState.Size = New System.Drawing.Size(34, 27)
        Me.txtState.TabIndex = 7
        '
        'txtZIP
        '
        Me.txtZIP.Location = New System.Drawing.Point(286, 254)
        Me.txtZIP.Name = "txtZIP"
        Me.txtZIP.Size = New System.Drawing.Size(71, 27)
        Me.txtZIP.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 125)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 19)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Recipient:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(33, 158)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 19)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Company:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(43, 188)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 19)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Address:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 254)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(116, 19)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "City, ST  ZIP:"
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(138, 287)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(219, 27)
        Me.txtPhone.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(57, 290)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 19)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Phone:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(490, 89)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 19)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "SHIP TO:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(413, 290)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 19)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Phone:"
        '
        'txtSTPhone
        '
        Me.txtSTPhone.Location = New System.Drawing.Point(494, 287)
        Me.txtSTPhone.Name = "txtSTPhone"
        Me.txtSTPhone.Size = New System.Drawing.Size(219, 27)
        Me.txtSTPhone.TabIndex = 28
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(363, 254)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(116, 19)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "City, ST  ZIP:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(399, 188)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 19)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Address:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(389, 158)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(90, 19)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Company:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(387, 125)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 19)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Recipient:"
        '
        'txtSTZIP
        '
        Me.txtSTZIP.Location = New System.Drawing.Point(642, 254)
        Me.txtSTZIP.Name = "txtSTZIP"
        Me.txtSTZIP.Size = New System.Drawing.Size(71, 27)
        Me.txtSTZIP.TabIndex = 23
        '
        'txtSTState
        '
        Me.txtSTState.Location = New System.Drawing.Point(602, 254)
        Me.txtSTState.Name = "txtSTState"
        Me.txtSTState.Size = New System.Drawing.Size(34, 27)
        Me.txtSTState.TabIndex = 22
        '
        'txtSTCity
        '
        Me.txtSTCity.Location = New System.Drawing.Point(494, 254)
        Me.txtSTCity.Name = "txtSTCity"
        Me.txtSTCity.Size = New System.Drawing.Size(102, 27)
        Me.txtSTCity.TabIndex = 21
        '
        'txtSTAddr2
        '
        Me.txtSTAddr2.Location = New System.Drawing.Point(494, 221)
        Me.txtSTAddr2.Name = "txtSTAddr2"
        Me.txtSTAddr2.Size = New System.Drawing.Size(219, 27)
        Me.txtSTAddr2.TabIndex = 20
        '
        'txtSTAddr1
        '
        Me.txtSTAddr1.Location = New System.Drawing.Point(494, 188)
        Me.txtSTAddr1.Name = "txtSTAddr1"
        Me.txtSTAddr1.Size = New System.Drawing.Size(219, 27)
        Me.txtSTAddr1.TabIndex = 19
        '
        'txtSTCoName
        '
        Me.txtSTCoName.Location = New System.Drawing.Point(494, 155)
        Me.txtSTCoName.Name = "txtSTCoName"
        Me.txtSTCoName.Size = New System.Drawing.Size(219, 27)
        Me.txtSTCoName.TabIndex = 18
        '
        'txtSTRecName
        '
        Me.txtSTRecName.Location = New System.Drawing.Point(494, 122)
        Me.txtSTRecName.Name = "txtSTRecName"
        Me.txtSTRecName.Size = New System.Drawing.Size(219, 27)
        Me.txtSTRecName.TabIndex = 17
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(72, 22)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 19)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "PO#:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(138, 19)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(102, 27)
        Me.TextBox1.TabIndex = 31
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(468, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(54, 19)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "Date:"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(528, 19)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 27)
        Me.DateTimePicker1.TabIndex = 33
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(12, 336)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(288, 19)
        Me.Label15.TabIndex = 34
        Me.Label15.Text = "Comments or Special Instructions:"
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(61, 359)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(618, 72)
        Me.txtComments.TabIndex = 35
        Me.txtComments.Text = ""
        '
        'frmPO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(740, 705)
        Me.Controls.Add(Me.txtComments)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtSTPhone)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtSTZIP)
        Me.Controls.Add(Me.txtSTState)
        Me.Controls.Add(Me.txtSTCity)
        Me.Controls.Add(Me.txtSTAddr2)
        Me.Controls.Add(Me.txtSTAddr1)
        Me.Controls.Add(Me.txtSTCoName)
        Me.Controls.Add(Me.txtSTRecName)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtPhone)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtZIP)
        Me.Controls.Add(Me.txtState)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.txtAddr2)
        Me.Controls.Add(Me.txtAddr1)
        Me.Controls.Add(Me.txtCoName)
        Me.Controls.Add(Me.txtRecName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmPO"
        Me.Text = "Purchase Orders"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExit As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRecName As TextBox
    Friend WithEvents txtCoName As TextBox
    Friend WithEvents txtAddr1 As TextBox
    Friend WithEvents txtAddr2 As TextBox
    Friend WithEvents txtCity As TextBox
    Friend WithEvents txtState As TextBox
    Friend WithEvents txtZIP As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtSTPhone As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtSTZIP As TextBox
    Friend WithEvents txtSTState As TextBox
    Friend WithEvents txtSTCity As TextBox
    Friend WithEvents txtSTAddr2 As TextBox
    Friend WithEvents txtSTAddr1 As TextBox
    Friend WithEvents txtSTCoName As TextBox
    Friend WithEvents txtSTRecName As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents Label15 As Label
    Friend WithEvents txtComments As RichTextBox
End Class
