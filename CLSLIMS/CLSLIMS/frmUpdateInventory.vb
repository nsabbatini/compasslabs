﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports System.ComponentModel

Public Class frmUpdateInventory
    Private _dirPathToFIle As String = System.AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dsItems As New DataSet
    Private Sub frmUpdateInventory_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GetItemsForUpdate()
    End Sub
    Private Sub GetItemsForUpdate()

        grdItemsUpdate.DataSource = GetInventoryDb()
        grdItemsUpdate.DataMember = "Items"


    End Sub
    Private Function GetInventoryDb() As DataSet
        _dsItems = New DataSet


        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & GetItemsForInvUpdateSql
                getInventory._sqlText = ReadSqlFile(_fileName)
                _dsItems = getInventory.GetInventoryForGrid

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try

        Return _dsItems

    End Function
    Private Sub btnUpdateInventory_Click(sender As Object, e As EventArgs) Handles btnUpdateInventory.Click
        Dim x As Int16
        Dim arr As New ArrayList

        _fileName = _fileDir & SaveCurrentQtySql

        Using qty As New ReceiveInventory() With {._sqlText = ReadSqlFile(_fileName)}
            For i = 0 To GridView1.DataRowCount - 1
                arr.Add(CType(GridView1.GetRowCellValue(i, "id"), Int16))
                arr.Add(CType(GridView1.GetRowCellValue(i, "currentqty"), Int64))
                arr.Add(CType(GridView1.GetRowCellValue(i, "minqty"), Int16))
                arr.Add(CType(GridView1.GetRowCellValue(i, "itemvendorcost"), Double))
                arr.Add(CType(GridView1.GetRowCellValue(i, "itemunit"), String).Trim())
                x = qty.SaveCurrentQtyDb(arr)
                arr.Clear()
            Next
            If x = GridView1.DataRowCount Then
                MsgBox("All items updated successfully!", MsgBoxStyle.Information)
                Me.Close()
            End If
        End Using

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub

    Private Sub grdItemsUpdate_ProcessGridKey(sender As Object, e As KeyEventArgs) Handles grdItemsUpdate.ProcessGridKey

        If e.KeyCode = Keys.Enter Then
            TryCast(grdItemsUpdate.FocusedView, ColumnView).FocusedRowHandle += 1
            e.Handled = True
        End If

    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Using frm As New frmRptViewer
            frm._reportType = "CheckList"
            frm.ShowDialog()

        End Using
    End Sub

    Private Sub RepositoryItemButtonEdit1_Click(sender As Object, e As EventArgs) Handles RepositoryItemButtonEdit1.Click
        Dim arow As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim x = MessageBox.Show(Me, "Are you sure you want to delete this item?", "Delete Item", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If x = DialogResult.Yes Then
            DeleteInvItems(arow(0))
            GetItemsForUpdate()
        End If
    End Sub
    Private Function DeleteInvItems(ByVal itemid As Int16) As Boolean
        _fileName = _fileDir & DeleteInventoryItemsSql

        Using vendors As New InventoryControl() With {._itemId = itemid, ._sqlText = ReadSqlFile(_fileName)}

            If vendors.DeleteInventoryItems Then
                Return True
            Else
                Return False
            End If

        End Using
    End Function
End Class