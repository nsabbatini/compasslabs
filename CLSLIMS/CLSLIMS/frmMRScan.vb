﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Public Class frmMRScan
    Public scanner As String = "", SpecID As String = ""
    Private Sub frmMRScan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            Debug.Print(CLSLIMS.UID)
        End If
        If CLSLIMS.UID = "" Or CLSLIMS.GetDept(CLSLIMS.UID) <> "SALESUPPORT" Then
            If CLSLIMS.UID <> "NED.SABBATINI" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        txtSpecId.Focus()

        Try
            Dim inifile As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader("C:\CLSLIMS\Scanner.ini")
            scanner = inifile.ReadLine
            inifile.Close()
            CLSLIMS.Log("Scanner:" & scanner)
        Catch
        End Try
    End Sub
    Private Sub txtAcct_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSpecId.KeyUp
        If e.KeyCode = Keys.Enter Then
            ValidateSpecId()
        End If
    End Sub
    Public Sub ValidateSpecId()
        SpecID = ""
        If txtSpecId.Text = "" Then Exit Sub
        Dim Sel As String = "Select pfNAME,plname from PDEM a, PSPEC b Where a.puid=b.puid and a.pefdt=b.pefdt And PSPECNO='" & txtSpecId.Text & "' And PSACT=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            lblLastFirst.Text = rs(1) & ", " & rs(0)
            SpecID = txtSpecId.Text
            txtReqNum.Text = txtSpecId.Text
        Else
            MsgBox("Specimen Number not found.")
            txtSpecId.Text = ""
            txtSpecId.Focus()
            SpecID = ""
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
        txtSpecId.Text = ""
        txtSpecId.Focus()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnScan_Click(sender As Object, e As EventArgs) Handles btnScan.Click

        If SpecID = "" Then
            MsgBox("Enter specimen number.")
            Exit Sub
        End If
        lblInfo.Text = "Scanning . . ."
        Dim Success As Boolean = False, AppendNotNec As Boolean = True
        'Check WorkingFile Folder - C:\CLSLIMS\Affidavit
        If Not Directory.Exists("C:\CLSLIMS\MedRec") Then
            Directory.CreateDirectory("C:\CLSLIMS\MedRec")
        End If
        Dim WorkingFile As String = "C:\CLSLIMS\MedRec\" & Trim(SpecID) & "MR.pdf"
        Dim CopyFile As String = "\\CMPFS1\MedRec\" & Trim(SpecID) & "MR.pdf"
        Dim CopyFile2 As String = "\\CL-AGILENT\MedRec\" & Trim(SpecID) & "MR.pdf"
        Dim CopyFile3 As String = "\\10.65.1.24\E\FTP\LBS\MEDREC\" & Trim(SpecID) & "MR.pdf"

        ScanImage.RemoveAllImages()

        If File.Exists(WorkingFile) Then
            File.Delete(WorkingFile)
        End If

        'ScanImage.LicenseKeys = "E378C7266C834EDE2B70E88E236EEE1C"
        'ScanImage.LicenseKeys = "50663FBE5F990BEFC170A6AC986CE8BA"
        ScanImage.LicenseKeys = "E9C5E61C6C3BEE4361A6AE53AB47C116"
        ScanImage.IfThrowException = False
        ScanImage.IfDisableSourceAfterAcquire = True
        ScanImage.IfShowUI = False
        Dim int As Integer = ScanImage.SourceCount
        For i = 0 To int
            If ScanImage.SourceNameItems(i) = scanner Then
                ScanImage.SelectSourceByIndex(i)
                Exit For
            End If
        Next i

        If ScanImage.CurrentSourceName = "" Then
            ScanImage.SelectSource()
        End If
        Debug.Print(ScanImage.CurrentSourceName)
        Success = ScanImage.CloseSource()
        Success = ScanImage.OpenSource()
        CLSLIMS.Log("Close-Open Source")
        If Success Then
            'ScanImage.Resolution = 600
            Success = ScanImage.AcquireImage()
            CLSLIMS.Log(Success)
            If Success Then
                CLSLIMS.Log("Acqired image")
                Success = ScanImage.SaveAllAsPDF(WorkingFile)
                CLSLIMS.Log("Working File:" & WorkingFile)
            End If
            If Success Then
                FileCopy(WorkingFile, CopyFile)
                CLSLIMS.Log("CopyFile:" & CopyFile)
                FileCopy(WorkingFile, CopyFile2)
                CLSLIMS.Log("CopyFile2:" & CopyFile2)
                If cbLBS.Checked Then
                    FileCopy(WorkingFile, CopyFile3)
                    CLSLIMS.Log("CopyFile3:" & CopyFile3)
                End If
            End If
            End If
        lblInfo.Text = "Scan complete."

    End Sub
End Class