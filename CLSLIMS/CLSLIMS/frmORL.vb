﻿Imports System.Drawing.Printing

Public Class frmORL
    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If Val(txtStart.Text) < 1500000 Or Val(txtStart.Text) > 1599999 Then
            MsgBox("Number must start with 15 and be seven digits.")
            Exit Sub
        End If
        If Val(txtNoSheets.Text) < 1 Or Val(txtNoSheets.Text) > 10 Then
            MsgBox("Only can pirnt between 1-10 sheets.")
            Exit Sub
        End If
        PrintLbl.DefaultPageSettings.Margins.Left = 100
        PrintLbl.DefaultPageSettings.Margins.Right = 10
        PrintLbl.DefaultPageSettings.Margins.Top = 150
        PrintLbl.DefaultPageSettings.Margins.Bottom = 10
        PrintLbl.DefaultPageSettings.Landscape = False
        PrintDialog.Document = PrintLbl
        PrintDialog.PrinterSettings = PrintLbl.PrinterSettings
        If PrintDialog.ShowDialog = DialogResult.OK Then
            PrintLbl.PrinterSettings = PrintDialog.PrinterSettings
            PrintLbl.Print()
        End If

    End Sub

    Private Sub PrintLbl_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintLbl.PrintPage
        Dim ReqNum As Integer = Val(txtStart.Text), x As Integer = 120, y As Integer = 30, x2 As Integer = 330
        Dim NumFont As Font = New Drawing.Font("Tahoma", 8)
        Dim BCFont As Font = New Drawing.Font("Free 3 of 9", 14)
        Dim LogoImage As Image = Image.FromFile("Compass Logo.jpg")
        Dim LeftParen As Image = Image.FromFile("LeftParen.png")
        Dim RightParen As Image = Image.FromFile("RightParen.png")
        Static Jay As Integer = 1
        'Static ReqNum As Integer
        CLSLIMS.Log(txtStart.Text & " - " & txtNoSheets.Text)
        For i = 1 To 15
            e.Graphics.DrawImage(LogoImage, x, y + 7, 150, 49)
            e.Graphics.DrawImage(LeftParen, x2 - 15, y + 15, 25, 55)
            e.Graphics.DrawString("Center Over Cap", NumFont, Brushes.Black, x2 + 20, y + 20)
            e.Graphics.DrawString(ReqNum & "-" & Rando(), NumFont, Brushes.Black, x2 + 30, y + 35)
            e.Graphics.DrawString("*" & ReqNum & "*", BCFont, Brushes.Black, x2 + 25, y + 50)
            e.Graphics.DrawImage(RightParen, x2 + 120, y + 15, 25, 55)
            e.Graphics.DrawString("Date:__/__/__", NumFont, Brushes.Black, x2 + 255, y + 20)
            e.Graphics.DrawString("(Mo/Day/Yr)", NumFont, Brushes.Black, x2 + 260, y + 38)
            e.Graphics.DrawString("Patient's Init:________", NumFont, Brushes.Black, x2 + 235, y + 56)
            y = y + 66 : ReqNum = ReqNum + 1
        Next i
        'Jay = Jay + 1
        'If Val(txtNoSheets.Text) > 1 Then
        '    If Jay < Val(txtNoSheets.Text) Then
        '        e.HasMorePages = True
        '    End If
        'End If
    End Sub

    Public Function Rando() As Integer
        Rando = "123"
        Do
            Rando = Int(Rnd(Format(Now, "mmss")) * 1000)
        Loop Until Rando > 99 And Rando < 1000
    End Function

    Private Sub frmORL_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "DEDRICK.RUSSELL" Then
            MsgBox("See Dedrick for printing lables.")
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class