﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Report.Library
Imports Compass.Sql.Database
Imports System.IO
Imports GrapeCity.ActiveReports.Export.Pdf.Section

Public Class frmRptViewer
    Private _dirPathToFIle As String = System.AppDomain.CurrentDomain.BaseDirectory()
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Friend WithEvents PdfExport1 As PdfExport
    Public _reportType As String
    Public _ponumber As String
    Public _clientAcct As String
    Public _subTotal As Double
    Public _salesTax As Double
    Public _totalSum As Double
    Public _IsClient As Boolean
    Private _clientInfo As Object
    Private CityStateZip As String
    Private ClientAddress As String = ""
    Public _needsApproval As Boolean = False
    Public _notes As String = ""
    Public _nonInventory As Boolean = False

    Private Sub frmRptViewer_Load(sender As Object, e As EventArgs) Handles Me.Load
        If _reportType = "CheckList" Then
            PrintInventoryCheckList()
        ElseIf _reportType = "PO" Then
            PrintPurchaseOrder()
        ElseIf _reportType = "OP" Then
            OpenPurchaseOrders()
        End If
    End Sub
    Private Sub OpenPurchaseOrders()

        Dim rpt As New rptOpenPurchaseOrders
        Dim results = GetOpenPurchaseOrders()
        Dim dtOpen As New DataTable

        dtOpen.TableName = "PO"
        dtOpen = CreateData()

        Dim arow As DataRow

        For Each item In results
            arow = dtOpen.NewRow
            arow(0) = item.GetType().GetProperty("PONumber")?.GetValue(item)
            arow(1) = item.GetType().GetProperty("ItemDesc")?.GetValue(item)
            arow(2) = item.GetType().GetProperty("OrderQty")?.GetValue(item)
            arow(3) = item.GetType().GetProperty("BackOrdered")?.GetValue(item)

            dtOpen.Rows.Add(arow)
        Next

        If dtOpen.Rows.Count = 0 Then
            MessageBox.Show(Me, "There are no open purchase orders at this time!", "Open Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        End If
        rptView.ViewType = GrapeCity.Viewer.Common.Model.ViewType.Continuous
        rpt.DataSource = dtOpen
        rpt.DataMember = "PO"
        rpt.Run()
        rptView.LoadDocument(rpt)


    End Sub
    Private Function CreateData() As DataTable

        Dim _dtItems As DataTable = New DataTable
        _dtItems.Columns.Add("PONumber", GetType(Int16))
        _dtItems.Columns.Add("ItemDesc", GetType(String))
        _dtItems.Columns.Add("OrderQty", GetType(Int16))
        _dtItems.Columns.Add("BackOrdered", GetType(Boolean))


        Return _dtItems

    End Function
    Private Sub PrintPurchaseOrder()
        Dim rpt As New rptPurchaseOrder
        Dim vCityStateZip As String = ""
        Dim vname As String = ""
        Dim vaddress As String = ""
        Dim vcontact As String = ""

        If _IsClient Then
            _clientInfo = GetClientInformation()

            CityStateZip = _clientInfo(0).GetType().GetProperty("Acity")?.GetValue(_clientInfo(0)) & ", " &
                _clientInfo(0).GetType().GetProperty("Astate")?.GetValue(_clientInfo(0)) & " " &
                _clientInfo(0).GetType().GetProperty("Azip")?.GetValue(_clientInfo(0))

            ClientAddress = _clientInfo(0).GetType().GetProperty("Aad1")?.GetValue(_clientInfo(0))
        Else
            CustomerBusinessName = "Compass Lab Services"
            ClientAddress = "1910 Nonconnah Blvd, Suite 108"
            CityStateZip = "Memphis, TN 38132"
        End If

        If Not _nonInventory Then
            Dim vendor = GetVendorDb()
            vCityStateZip = vendor(0).GetType().GetProperty("City")?.GetValue(vendor(0)) & ", " &
            vendor(0).GetType().GetProperty("State")?.GetValue(vendor(0)) & " " &
            vendor(0).GetType().GetProperty("Zip")?.GetValue(vendor(0))

            vname = vendor(0).GetType().GetProperty("VendorName")?.GetValue(vendor(0))
            vaddress = vendor(0).GetType().GetProperty("Address")?.GetValue(vendor(0))
            vcontact = vendor(0).GetType().GetProperty("Contact")?.GetValue(vendor(0))

        End If
        Try
            rptView.ViewType = GrapeCity.Viewer.Common.Model.ViewType.Continuous
            rpt._ClientName = StrConv(CustomerBusinessName, vbProperCase)
            rpt._Address = ClientAddress
            rpt._CityStateZip = CityStateZip.Trim()
            If Not _nonInventory Then
                rpt._VendorName = StrConv(vname, vbProperCase)
                rpt._VAddress = vaddress
                rpt._VCityStateZip = vCityStateZip
                rpt._VContact = StrConv(vcontact, vbProperCase)
            Else
                rpt._VendorName = CustomerBusinessName
                rpt._VAddress = ClientAddress
                rpt._VCityStateZip = CityStateZip
                rpt._VContact = String.Empty

            End If
            rpt._PONumber = _ponumber
            rpt._SubTotal = _subTotal
            rpt._SalesTax = _salesTax
            rpt._TotalSum = _totalSum
            rpt._Notes = _notes
            rpt._SalesPerson = StrConv(SalesPerson, vbProperCase)
            rpt._QuoteNumber = QuoteNumber
            rpt._Department = Department
            rpt._PaymentTerms = PaymentTerms
            rpt.DataSource = GetTheOrderItems()
            rpt.DataMember = "Items"
            rpt.Run()
            If _needsApproval Then
                If PdfExport1 Is Nothing Then
                    PdfExport1 = New PdfExport
                End If
                If Not Directory.Exists(_dirPathToFIle & "\pos") Then
                    Directory.CreateDirectory(_dirPathToFIle & "\pos")
                End If
                PdfExport1.Export(rpt.Document, _dirPathToFIle & "\pos\po" & _ponumber & ".pdf")
                ''send email
                'SendPdfForApproval(_dirPathToFIle & "\pos\po" & _ponumber & ".pdf")
            End If
            rptView.LoadDocument(rpt)

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmRptViewer.PrintPurchaseOrder", ex.StackTrace.ToString)
                MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            End Using
        End Try
    End Sub
    Private Sub PrintInventoryCheckList()
        Dim rpt As New rptCurrentInventory

        Try
            rptView.ViewType = GrapeCity.Viewer.Common.Model.ViewType.Continuous
            rpt.DataSource = GetInventoryDb()
            rpt.DataMember = "items"
            rpt.Run()
            rptView.LoadDocument(rpt)

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmRptViewer.PrintInventoryCheckList", ex.StackTrace.ToString)
                MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            End Using
        End Try
    End Sub

    Private Function GetInventoryDb() As DataSet

        Try

            Using getInventory As New ReceiveInventory()

                _fileName = _fileDir & GetItemsForInvUpdateSql
                getInventory._sqlText = ReadSqlFile(_fileName)

                Dim dsItems As New DataSet

                dsItems = getInventory.GetItemsDb

                Return dsItems

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmRptViewer.GetInventoryDb", ex.StackTrace.ToString)
                MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            End Using
        End Try



    End Function
    Private Function GetClientInformation() As Object

        Try

            Using getInventory As New GetClientInfoDb()

                _fileName = _fileDir & GetClientInformationSql
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.SetClientInfo.Where(Function(x) x.Acct = CType(_clientAcct, String)).ToList

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmRptViewer.GetClientInformation", ex.StackTrace.ToString)
            End Using
        End Try


    End Function
    Private Function GetVendorDb() As Object

        Try

            Using getVendors As New GetInventoryDb()
                _fileName = _fileDir & VendorSqlFile
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetVendorInfo.Where(Function(x) x.Id = VendorId).ToList
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmRptViewer.GetVendorDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function
    Private Function GetTheOrderItems() As DataSet
        Dim dsItems As DataSet

        _fileName = _fileDir & GetPurchaseOrderItemsSql
        Using items As New PurchaseOrders With {._sqlText = ReadSqlFile(_fileName),
            ._pnumber = CType(_ponumber, Int16)
            }
            dsItems = items.GetPurchaseOrderItems

        End Using

        Return dsItems

    End Function
    Private Function GetOpenPurchaseOrders() As Object

        Try

            Using getOpenPo As New PurchaseOrders()
                _fileName = _fileDir & GetReconcilePoSql
                getOpenPo._sqlText = ReadSqlFile(_fileName)
                Return getOpenPo.ReconcilePo.Where(Function(x) x.Received = False).ToList
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmRptViewer.GetOpenPurchaseOrders", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try

    End Function
End Class