﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QuickOE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbOC = New System.Windows.Forms.ListBox()
        Me.lbSamples = New System.Windows.Forms.ListBox()
        Me.txtTray = New System.Windows.Forms.TextBox()
        Me.txtReqNum = New System.Windows.Forms.TextBox()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.txtOC = New System.Windows.Forms.TextBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnLblSave = New System.Windows.Forms.Button()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.lstAcctNameSearch = New System.Windows.Forms.ListBox()
        Me.lstOCNameSearch = New System.Windows.Forms.ListBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblSelItem = New System.Windows.Forms.Label()
        Me.lblF4 = New System.Windows.Forms.Label()
        Me.lblF5 = New System.Windows.Forms.Label()
        Me.lblF9 = New System.Windows.Forms.Label()
        Me.lblF10 = New System.Windows.Forms.Label()
        Me.lblF11 = New System.Windows.Forms.Label()
        Me.lblF6 = New System.Windows.Forms.Label()
        Me.btnPrtLbl = New System.Windows.Forms.Button()
        Me.lblF2 = New System.Windows.Forms.Label()
        Me.lblF1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lstStatusCode = New System.Windows.Forms.ListBox()
        Me.txtStatusCode = New System.Windows.Forms.TextBox()
        Me.lstStatusCodeSearch = New System.Windows.Forms.ListBox()
        Me.lblIntCom = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnGrid = New System.Windows.Forms.Button()
        Me.lblUID = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbSpecType = New System.Windows.Forms.ComboBox()
        Me.lblPatInfo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tray #:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 95)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Requisition #:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(49, 129)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 19)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Account #:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 203)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(140, 19)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Ordering Codes:"
        '
        'lbOC
        '
        Me.lbOC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbOC.FormattingEnabled = True
        Me.lbOC.ItemHeight = 19
        Me.lbOC.Location = New System.Drawing.Point(159, 239)
        Me.lbOC.Name = "lbOC"
        Me.lbOC.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbOC.Size = New System.Drawing.Size(203, 137)
        Me.lbOC.TabIndex = 4
        '
        'lbSamples
        '
        Me.lbSamples.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbSamples.FormattingEnabled = True
        Me.lbSamples.ItemHeight = 16
        Me.lbSamples.Location = New System.Drawing.Point(384, 4)
        Me.lbSamples.Name = "lbSamples"
        Me.lbSamples.Size = New System.Drawing.Size(519, 484)
        Me.lbSamples.TabIndex = 5
        '
        'txtTray
        '
        Me.txtTray.AcceptsTab = True
        Me.txtTray.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTray.Location = New System.Drawing.Point(92, 39)
        Me.txtTray.Name = "txtTray"
        Me.txtTray.Size = New System.Drawing.Size(118, 27)
        Me.txtTray.TabIndex = 0
        '
        'txtReqNum
        '
        Me.txtReqNum.AcceptsTab = True
        Me.txtReqNum.Enabled = False
        Me.txtReqNum.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReqNum.Location = New System.Drawing.Point(159, 92)
        Me.txtReqNum.Name = "txtReqNum"
        Me.txtReqNum.Size = New System.Drawing.Size(118, 27)
        Me.txtReqNum.TabIndex = 1
        '
        'txtAcct
        '
        Me.txtAcct.AcceptsTab = True
        Me.txtAcct.Enabled = False
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(159, 125)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(118, 27)
        Me.txtAcct.TabIndex = 2
        '
        'txtOC
        '
        Me.txtOC.AcceptsTab = True
        Me.txtOC.Enabled = False
        Me.txtOC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOC.Location = New System.Drawing.Point(159, 195)
        Me.txtOC.Name = "txtOC"
        Me.txtOC.Size = New System.Drawing.Size(118, 27)
        Me.txtOC.TabIndex = 3
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(18, 459)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(56, 29)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(79, 459)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(56, 29)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnLblSave
        '
        Me.btnLblSave.Enabled = False
        Me.btnLblSave.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLblSave.Location = New System.Drawing.Point(141, 459)
        Me.btnLblSave.Name = "btnLblSave"
        Me.btnLblSave.Size = New System.Drawing.Size(118, 29)
        Me.btnLblSave.TabIndex = 5
        Me.btnLblSave.Text = "Label Save"
        Me.btnLblSave.UseVisualStyleBackColor = True
        '
        'lblMsg
        '
        Me.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMsg.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMsg.Location = New System.Drawing.Point(13, 239)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(134, 134)
        Me.lblMsg.TabIndex = 7
        '
        'lstAcctNameSearch
        '
        Me.lstAcctNameSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAcctNameSearch.FormattingEnabled = True
        Me.lstAcctNameSearch.ItemHeight = 14
        Me.lstAcctNameSearch.Location = New System.Drawing.Point(308, 92)
        Me.lstAcctNameSearch.Name = "lstAcctNameSearch"
        Me.lstAcctNameSearch.Size = New System.Drawing.Size(54, 4)
        Me.lstAcctNameSearch.TabIndex = 8
        Me.lstAcctNameSearch.Visible = False
        '
        'lstOCNameSearch
        '
        Me.lstOCNameSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOCNameSearch.FormattingEnabled = True
        Me.lstOCNameSearch.ItemHeight = 14
        Me.lstOCNameSearch.Location = New System.Drawing.Point(308, 110)
        Me.lstOCNameSearch.Name = "lstOCNameSearch"
        Me.lstOCNameSearch.Size = New System.Drawing.Size(54, 4)
        Me.lstOCNameSearch.TabIndex = 9
        Me.lstOCNameSearch.Visible = False
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Image = Global.CLSLIMS.My.Resources.Resources.ZebraPtr1
        Me.Button1.Location = New System.Drawing.Point(217, 39)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(42, 34)
        Me.Button1.TabIndex = 10
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblSelItem
        '
        Me.lblSelItem.AutoSize = True
        Me.lblSelItem.Location = New System.Drawing.Point(318, 12)
        Me.lblSelItem.Name = "lblSelItem"
        Me.lblSelItem.Size = New System.Drawing.Size(0, 13)
        Me.lblSelItem.TabIndex = 11
        Me.lblSelItem.Visible = False
        '
        'lblF4
        '
        Me.lblF4.AutoSize = True
        Me.lblF4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF4.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF4.Location = New System.Drawing.Point(138, 502)
        Me.lblF4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF4.Name = "lblF4"
        Me.lblF4.Size = New System.Drawing.Size(22, 15)
        Me.lblF4.TabIndex = 13
        Me.lblF4.Text = "F4"
        '
        'lblF5
        '
        Me.lblF5.AutoSize = True
        Me.lblF5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF5.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF5.Location = New System.Drawing.Point(169, 502)
        Me.lblF5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF5.Name = "lblF5"
        Me.lblF5.Size = New System.Drawing.Size(22, 15)
        Me.lblF5.TabIndex = 14
        Me.lblF5.Text = "F5"
        '
        'lblF9
        '
        Me.lblF9.AutoSize = True
        Me.lblF9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF9.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF9.Location = New System.Drawing.Point(258, 502)
        Me.lblF9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF9.Name = "lblF9"
        Me.lblF9.Size = New System.Drawing.Size(22, 15)
        Me.lblF9.TabIndex = 15
        Me.lblF9.Text = "F9"
        '
        'lblF10
        '
        Me.lblF10.AutoSize = True
        Me.lblF10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF10.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF10.Location = New System.Drawing.Point(289, 502)
        Me.lblF10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF10.Name = "lblF10"
        Me.lblF10.Size = New System.Drawing.Size(29, 15)
        Me.lblF10.TabIndex = 16
        Me.lblF10.Text = "F10"
        '
        'lblF11
        '
        Me.lblF11.AutoSize = True
        Me.lblF11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF11.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF11.Location = New System.Drawing.Point(326, 502)
        Me.lblF11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF11.Name = "lblF11"
        Me.lblF11.Size = New System.Drawing.Size(29, 15)
        Me.lblF11.TabIndex = 17
        Me.lblF11.Text = "F11"
        '
        'lblF6
        '
        Me.lblF6.AutoSize = True
        Me.lblF6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF6.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF6.Location = New System.Drawing.Point(210, 502)
        Me.lblF6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF6.Name = "lblF6"
        Me.lblF6.Size = New System.Drawing.Size(22, 15)
        Me.lblF6.TabIndex = 18
        Me.lblF6.Text = "F6"
        '
        'btnPrtLbl
        '
        Me.btnPrtLbl.Enabled = False
        Me.btnPrtLbl.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrtLbl.Location = New System.Drawing.Point(264, 459)
        Me.btnPrtLbl.Name = "btnPrtLbl"
        Me.btnPrtLbl.Size = New System.Drawing.Size(98, 29)
        Me.btnPrtLbl.TabIndex = 19
        Me.btnPrtLbl.Text = "Print Label"
        Me.btnPrtLbl.UseVisualStyleBackColor = True
        '
        'lblF2
        '
        Me.lblF2.AutoSize = True
        Me.lblF2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF2.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF2.Location = New System.Drawing.Point(80, 502)
        Me.lblF2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF2.Name = "lblF2"
        Me.lblF2.Size = New System.Drawing.Size(22, 15)
        Me.lblF2.TabIndex = 20
        Me.lblF2.Text = "F2"
        Me.lblF2.Visible = False
        '
        'lblF1
        '
        Me.lblF1.AutoSize = True
        Me.lblF1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblF1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblF1.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblF1.Location = New System.Drawing.Point(31, 502)
        Me.lblF1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblF1.Name = "lblF1"
        Me.lblF1.Size = New System.Drawing.Size(22, 15)
        Me.lblF1.TabIndex = 21
        Me.lblF1.Text = "F1"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(7, 393)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 19)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Status Code:"
        '
        'lstStatusCode
        '
        Me.lstStatusCode.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstStatusCode.FormattingEnabled = True
        Me.lstStatusCode.ItemHeight = 19
        Me.lstStatusCode.Location = New System.Drawing.Point(159, 381)
        Me.lstStatusCode.Name = "lstStatusCode"
        Me.lstStatusCode.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstStatusCode.Size = New System.Drawing.Size(203, 61)
        Me.lstStatusCode.TabIndex = 23
        '
        'txtStatusCode
        '
        Me.txtStatusCode.AcceptsTab = True
        Me.txtStatusCode.Enabled = False
        Me.txtStatusCode.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatusCode.Location = New System.Drawing.Point(11, 415)
        Me.txtStatusCode.Name = "txtStatusCode"
        Me.txtStatusCode.Size = New System.Drawing.Size(136, 27)
        Me.txtStatusCode.TabIndex = 24
        '
        'lstStatusCodeSearch
        '
        Me.lstStatusCodeSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstStatusCodeSearch.FormattingEnabled = True
        Me.lstStatusCodeSearch.ItemHeight = 14
        Me.lstStatusCodeSearch.Location = New System.Drawing.Point(308, 129)
        Me.lstStatusCodeSearch.Name = "lstStatusCodeSearch"
        Me.lstStatusCodeSearch.Size = New System.Drawing.Size(54, 4)
        Me.lstStatusCodeSearch.TabIndex = 26
        Me.lstStatusCodeSearch.Visible = False
        '
        'lblIntCom
        '
        Me.lblIntCom.AutoSize = True
        Me.lblIntCom.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntCom.Location = New System.Drawing.Point(126, 393)
        Me.lblIntCom.Name = "lblIntCom"
        Me.lblIntCom.Size = New System.Drawing.Size(0, 19)
        Me.lblIntCom.TabIndex = 27
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(159, 73)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(0, 13)
        Me.Label6.TabIndex = 28
        '
        'btnGrid
        '
        Me.btnGrid.Enabled = False
        Me.btnGrid.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrid.Location = New System.Drawing.Point(289, 193)
        Me.btnGrid.Name = "btnGrid"
        Me.btnGrid.Size = New System.Drawing.Size(73, 29)
        Me.btnGrid.TabIndex = 29
        Me.btnGrid.Text = "Grid"
        Me.btnGrid.UseVisualStyleBackColor = True
        '
        'lblUID
        '
        Me.lblUID.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUID.Location = New System.Drawing.Point(217, 12)
        Me.lblUID.Name = "lblUID"
        Me.lblUID.Size = New System.Drawing.Size(161, 13)
        Me.lblUID.TabIndex = 30
        Me.lblUID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(9, 166)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(138, 19)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Specimen Type:"
        '
        'cmbSpecType
        '
        Me.cmbSpecType.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSpecType.FormattingEnabled = True
        Me.cmbSpecType.Location = New System.Drawing.Point(159, 158)
        Me.cmbSpecType.Name = "cmbSpecType"
        Me.cmbSpecType.Size = New System.Drawing.Size(178, 27)
        Me.cmbSpecType.TabIndex = 32
        '
        'lblPatInfo
        '
        Me.lblPatInfo.AutoSize = True
        Me.lblPatInfo.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPatInfo.Location = New System.Drawing.Point(397, 502)
        Me.lblPatInfo.Name = "lblPatInfo"
        Me.lblPatInfo.Size = New System.Drawing.Size(0, 25)
        Me.lblPatInfo.TabIndex = 33
        '
        'QuickOE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(906, 542)
        Me.Controls.Add(Me.lblPatInfo)
        Me.Controls.Add(Me.cmbSpecType)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblUID)
        Me.Controls.Add(Me.btnGrid)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblIntCom)
        Me.Controls.Add(Me.lstStatusCodeSearch)
        Me.Controls.Add(Me.txtStatusCode)
        Me.Controls.Add(Me.lstStatusCode)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblF1)
        Me.Controls.Add(Me.lblF2)
        Me.Controls.Add(Me.btnPrtLbl)
        Me.Controls.Add(Me.lblF6)
        Me.Controls.Add(Me.lblF11)
        Me.Controls.Add(Me.lblF10)
        Me.Controls.Add(Me.lblF9)
        Me.Controls.Add(Me.lblF5)
        Me.Controls.Add(Me.lblF4)
        Me.Controls.Add(Me.lblSelItem)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lstOCNameSearch)
        Me.Controls.Add(Me.lstAcctNameSearch)
        Me.Controls.Add(Me.lblMsg)
        Me.Controls.Add(Me.btnLblSave)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.txtOC)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.txtReqNum)
        Me.Controls.Add(Me.txtTray)
        Me.Controls.Add(Me.lbSamples)
        Me.Controls.Add(Me.lbOC)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.KeyPreview = True
        Me.Name = "QuickOE"
        Me.Text = "Order Entry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbOC As System.Windows.Forms.ListBox
    Friend WithEvents lbSamples As System.Windows.Forms.ListBox
    Friend WithEvents txtTray As System.Windows.Forms.TextBox
    Friend WithEvents txtReqNum As System.Windows.Forms.TextBox
    Friend WithEvents txtAcct As System.Windows.Forms.TextBox
    Friend WithEvents txtOC As System.Windows.Forms.TextBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnLblSave As System.Windows.Forms.Button
    Friend WithEvents lblMsg As System.Windows.Forms.Label
    Friend WithEvents lstAcctNameSearch As System.Windows.Forms.ListBox
    Friend WithEvents lstOCNameSearch As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblSelItem As System.Windows.Forms.Label
    Friend WithEvents lblF4 As System.Windows.Forms.Label
    Friend WithEvents lblF5 As System.Windows.Forms.Label
    Friend WithEvents lblF9 As System.Windows.Forms.Label
    Friend WithEvents lblF10 As System.Windows.Forms.Label
    Friend WithEvents lblF11 As System.Windows.Forms.Label
    Friend WithEvents lblF6 As System.Windows.Forms.Label
    Friend WithEvents btnPrtLbl As System.Windows.Forms.Button
    Friend WithEvents lblF2 As System.Windows.Forms.Label
    Friend WithEvents lblF1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lstStatusCode As System.Windows.Forms.ListBox
    Friend WithEvents txtStatusCode As System.Windows.Forms.TextBox
    Friend WithEvents lstStatusCodeSearch As System.Windows.Forms.ListBox
    Friend WithEvents lblIntCom As System.Windows.Forms.Label
    Friend WithEvents Label6 As Label
    Friend WithEvents btnGrid As Button
    Friend WithEvents lblUID As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents cmbSpecType As ComboBox
    Friend WithEvents lblPatInfo As Label
End Class
