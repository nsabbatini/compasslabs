﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Collections.Specialized
Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class epMotion
    Public Grid(192) As String
    Public DataGrid As NameValueCollection
    Public k As Integer
    Public blnPrint As Boolean
    Public CurrPos As Integer = 0

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        If txtFldr.Text <> "" Then
            Dim response As MsgBoxResult = MsgBox("Have you saved your changes?", MsgBoxStyle.YesNo)
            If response = MsgBoxResult.No Then
                response = MsgBox("Would you like to save now?", MsgBoxStyle.YesNo)
                If response = MsgBoxResult.Yes Then
                    Dim dummy As System.Object = DBNull.Value, dummye As System.EventArgs = System.EventArgs.Empty
                    Call btnCloseNow_Click(dummy, dummye)
                End If
            End If
        End If
        Me.Close()
        Me.Dispose()
    End Sub

    Private Function GetBatchPos(ByRef SpecNo As String) As String
        Dim Sel As String = "select mbatch,pbix from pbi a, pb b where a.wl='SEIA' and a.wl=b.wl and a.batch=b.batch and pspecno='" & SpecNo & "'"
        GetBatchPos = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                GetBatchPos = "  " & rs(0) & " - " & rs(1)
            Loop
            rs.Close()
            Conn.Close()
        End Using

    End Function


    Private Sub txtSpecNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSpecNum.KeyPress
        Dim dup As String, x As Integer, y As Integer  ', BatchPos As String
        Dim Blnk As Boolean, strTray As String = "", blnTHC As Boolean = False, blnETG As Boolean = False, blnOnly As Boolean = False, blnRudolph As Boolean = False

        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtSpecNum.Text = "" Then
            Exit Sub
        End If
        If Mid(txtSpecNum.Text, 1, 1) = "0" Then txtSpecNum.Text = Mid(txtSpecNum.Text, 2)
        If Not CLSLIMS.THC And Not CLSLIMS.ETG Then
            'Check for valid specimen number in trays
            'If Specimen number preceeded with an "R" then skip check because specimen is repeat
            If UCase(Mid(txtSpecNum.Text, 1, 1)) <> "R" Then
                strTray = CheckforTray(txtSpecNum.Text)
                If strTray = "" Then
                    txtSpecNum.Text = ""
                    txtSpecNum.Focus()
                    MsgBox("Specimen not on trays listed.")
                    Exit Sub
                End If
            Else
                txtSpecNum.Text = Mid(txtSpecNum.Text, 2)
            End If
        Else
            If txtSpecNum.Text <> "HIGH" Then
                If Not IfSample(txtSpecNum.Text) Then
                    txtSpecNum.Text = ""
                    txtSpecNum.Focus()
                    MsgBox("Specimen not Valid.")
                    Exit Sub
                    'Added else 9/8/2017 to added tray position for THC/ETG folders === Removed 10/18/2017 specimen prep not entering trays
                Else
                    'strTray = CheckforTray(txtSpecNum.Text)
                End If
            End If
        End If
        'Check for Duplicates in grid
        dup = CheckforDuplicates(txtSpecNum.Text)
        If dup <> "" Then
            MsgBox("Duplicate sample number. Position: " & dup)
            txtSpecNum.Text = ""
            txtSpecNum.Focus()
            Exit Sub
        End If
        lblSpecNum.Text = txtSpecNum.Text
        'Increment & Display position
        lblNxtPos.Text = Grid(CurrPos)
        'Check for THC Conf test ordered
        blnTHC = CheckforTHCConf(txtSpecNum.Text)
        'Check for ETG
        blnETG = CheckforETGConf(txtSpecNum.Text)
        'Check for Dr. Rudolph (371 or 372)
        'blnRudolph = CheckforRudolph(txtSpecNum.Text)
        blnOnly = CheckforSingle(txtSpecNum.Text)
        'Load Collection and grdPlate
        DataGrid.Item(Grid(CurrPos)) = txtSpecNum.Text & " " & strTray & "^" & IIf(blnTHC, "1", "0") & "^" & IIf(blnETG, "1", "0") & "^" & IIf(blnRudolph, "1", "0")
        Select Case Mid(Grid(CurrPos), 3, 1)
            Case "A" : x = 0
            Case "B" : x = 1
            Case "C" : x = 2
            Case "D" : x = 3
            Case "E" : x = 4
            Case "F" : x = 5
            Case "G" : x = 6
            Case "H" : x = 7
        End Select
        y = Val(Mid(Grid(CurrPos), 4)) - 1
        If Mid(Grid(CurrPos), 1, 2) = "P1" Then
            grdPlate1.Rows(x).Cells(y).Value = txtSpecNum.Text & " " & strTray
            If blnTHC And Not blnOnly Then
                grdPlate1.Rows(x).Cells(y).Style.BackColor = Color.LightBlue
                grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Regular)
            ElseIf blnTHC And blnOnly Then
                grdPlate1.Rows(x).Cells(y).Style.BackColor = Color.LightBlue
                grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Italic)
            Else
                grdPlate1.Rows(x).Cells(y).Style.BackColor = Color.White
                grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Regular)
            End If

            If blnETG And Not blnOnly Then
                grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Underline)
            ElseIf blnETG And blnOnly Then
                grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Underline And FontStyle.Italic)
            ElseIf blnOnly Then
                grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Italic)
            Else
                grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Regular)
            End If
            'If blnRudolph Then grdPlate1.Rows(x).Cells(y).Value = "(" & txtSpecNum.Text & ")" & " " & strTray
        ElseIf Mid(Grid(CurrPos), 1, 2) = "P2" Then
            grdPlate2.Rows(x).Cells(y).Value = txtSpecNum.Text & " " & strTray
            If blnTHC And Not blnOnly Then
                grdPlate2.Rows(x).Cells(y).Style.BackColor = Color.LightBlue
                grdPlate2.Rows(x).Cells(y).Style.Font = New Font(grdPlate2.Font, FontStyle.Regular)
            ElseIf blnTHC And blnOnly Then
                grdPlate2.Rows(x).Cells(y).Style.BackColor = Color.LightBlue
                grdPlate2.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Italic)
            Else
                grdPlate2.Rows(x).Cells(y).Style.BackColor = Color.White
                grdPlate2.Rows(x).Cells(y).Style.Font = New Font(grdPlate2.Font, FontStyle.Regular)
            End If
            If blnETG And Not blnOnly Then
                grdPlate2.Rows(x).Cells(y).Style.Font = New Font(grdPlate2.Font, FontStyle.Underline)
            ElseIf blnETG And blnOnly Then
                grdPlate2.Rows(x).Cells(y).Style.Font = New Font(grdPlate2.Font, FontStyle.Underline And FontStyle.Italic)
            Else
                grdPlate2.Rows(x).Cells(y).Style.Font = New Font(grdPlate2.Font, FontStyle.Regular)
            End If
            'If blnRudolph Then grdPlate2.Rows(x).Cells(y).Value = "(" & txtSpecNum.Text & ")" & " " & strTray
        End If
        blnTHC = False : blnETG = False : blnRudolph = False
        CurrPos = CurrPos + 1
        'Check to make sure CurrPos is blank
        Blnk = False
        Do While (Not Blnk) And (CurrPos < 193)
            Select Case Mid(Grid(CurrPos), 3, 1)
                Case "A" : x = 0
                Case "B" : x = 1
                Case "C" : x = 2
                Case "D" : x = 3
                Case "E" : x = 4
                Case "F" : x = 5
                Case "G" : x = 6
                Case "H" : x = 7
            End Select
            y = Val(Mid(Grid(CurrPos), 4)) - 1
            If Mid(Grid(CurrPos), 1, 2) = "P1" Then
                If Trim(grdPlate1.Rows(x).Cells(y).Value) <> "" Then
                    CurrPos = CurrPos + 1
                Else
                    Blnk = True
                End If
            ElseIf Mid(Grid(CurrPos), 1, 2) = "P2" Then
                If Trim(grdPlate2.Rows(x).Cells(y).Value) <> "" Then
                    CurrPos = CurrPos + 1
                Else
                    Blnk = True
                End If
            End If
        Loop
        'Reset field
        txtSpecNum.Text = ""
        txtSpecNum.Focus()

    End Sub
    Private Function CheckforDuplicates(specno As String) As String
        CheckforDuplicates = ""
        For Each key In DataGrid.Keys
            If Mid(Replace(Replace(DataGrid(key), "(", ""), ")", ""), 1, Len(specno)) = specno Then
                CheckforDuplicates = key
            End If
        Next key
    End Function
    Public Function CheckforTHCConf(ByVal specno As String) As Boolean
        CheckforTHCConf = False
        Dim Sel As String = "Select count(*) From POCTC Where PSPECNO='" & specno & "' And TC In ('302080','302050','302060','302070')"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforTHCConf = True
                End If
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
    Public Function CheckforSingle(ByVal specno As String) As Boolean
        CheckforSingle = True
        Dim Sel As String = "Select count(*) From QWL Where PSPECNO='" & specno & "' And WL like '%PAIN%'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforSingle = False
                End If
            End If
            rs.Close()
            Cmd.CommandText = "Select count(*) From PBI a, PB b Where PSPECNO='" & specno & "' And a.WL like '%PAIN%' And a.WL=b.WL And a.BEFDT=b.BEFDT and BACT=1 And a.BATCH=b.BATCH"
            rs = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforSingle = False
                End If
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
    Public Function CheckforETGConf(ByVal specno As String) As Boolean
        CheckforETGConf = False
        Dim Sel As String = "Select count(*) From POCTC Where PSPECNO='" & specno & "' And TC In ('308010','308020')"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforETGConf = True
                End If
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function

    Public Function CheckforRudolph(ByVal specno As String) As Boolean
        CheckforRudolph = False
        Dim Sel As String = "Select count(*) from PORD A, PSPEC B Where A.PORD=B.PORD And A.POEFDT=B.POEFDT And PSACT=1 And POACT=1 And ACCT In ('371','372') And PSPECNO='" & specno & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforRudolph = True
                End If
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function

    Private Function CheckforTray(ByVal specno As String) As String
        Dim TL As String, sq As Integer
        CheckforTray = ""
        Dim strTrays As String = ""
        If txtTray1.Text <> "" Then
            strTrays = strTrays & "'" & txtTray1.Text & "',"
        End If
        If txtTray2.Text <> "" Then
            strTrays = strTrays & "'" & txtTray2.Text & "',"
        End If
        If txtTray3.Text <> "" Then
            strTrays = strTrays & "'" & txtTray3.Text & "',"
        End If
        If txtTray4.Text <> "" Then
            strTrays = strTrays & "'" & txtTray4.Text & "',"
        End If
        'Remove trailing comma
        If strTrays <> "" Then strTrays = Mid(strTrays, 1, Len(strTrays) - 1)
        specno = Replace(Replace(specno, ")", ""), "(", "")
        Dim Sel As String = "Select TRAY, TRAYx From XTRAYS Where PSPECNO='" & specno & "' And TRAY In (" & strTrays & ")"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                sq = Val(Mid(rs(0), 3, 2))
                TL = Mid("ABCDEFGHJKLMNPQRSTUVWXYZ", sq, 1)
                CheckforTray = TL & IIf(rs(1) < 10, "0", "") & rs(1)
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
    Private Function GetTrayPos(specno As String) As String
        GetTrayPos = ""
        Dim Sel As String = "Select POS From XTRAYS Where PSPECNO='" & specno & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                GetTrayPos = rs(0)
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function

    Private Sub LoadGrid()
        Dim i As Integer, j As Integer
        Dim Pre As String = "A"
        Dim Suf As String = "1"

        grdPlate1.Rows.Clear()
        grdPlate1.Rows.Add(8)
        grdPlate1.RowHeadersWidth = 45
        For i = 0 To 7
            grdPlate1.Rows(i).Height = 35

            grdPlate1.Rows(i).HeaderCell.Value = Mid("ABCDEFGH", i + 1, 1)
        Next i
        For i = 0 To 6 Step 2
            For j = 0 To 5
                grdPlate1.Rows(i).Cells(j).Style.BackColor = Color.LemonChiffon
            Next
        Next
        For i = 1 To 7 Step 2
            For j = 6 To 11
                grdPlate1.Rows(i).Cells(j).Style.BackColor = Color.LemonChiffon
            Next
        Next

        grdPlate2.Rows.Clear()
        grdPlate2.Rows.Add(8)
        grdPlate2.RowHeadersWidth = 45
        For i = 0 To 7
            grdPlate2.Rows(i).Height = 35
            grdPlate2.Rows(i).HeaderCell.Value = Mid("ABCDEFGH", i + 1, 1)
        Next i

        For i = 0 To 6 Step 2
            For j = 0 To 5
                grdPlate2.Rows(i).Cells(j).Style.BackColor = Color.LemonChiffon
            Next
        Next
        For i = 1 To 7 Step 2
            For j = 6 To 11
                grdPlate2.Rows(i).Cells(j).Style.BackColor = Color.LemonChiffon
            Next
        Next

        DataGrid.Clear()
        For i = 0 To 192 : Grid(i) = "" : Next i
        j = 0
        For i = 1 To 96
            Select Case i
                Case 1 : Pre = "A"
                Case 13 : Pre = "B"
                Case 25 : Pre = "C"
                Case 37 : Pre = "D"
                Case 49 : Pre = "E"
                Case 61 : Pre = "F"
                Case 73 : Pre = "G"
                Case 85 : Pre = "H"
            End Select
            j = j + 1
            Suf = Convert.ToString(j)
            If j = 12 Then j = 0
            Grid(i) = "P1" & Pre & Suf
            DataGrid.Add(Grid(i), "")
        Next i
        j = 0
        For i = 1 To 96
            Select Case i
                Case 1 : Pre = "A"
                Case 13 : Pre = "B"
                Case 25 : Pre = "C"
                Case 37 : Pre = "D"
                Case 49 : Pre = "E"
                Case 61 : Pre = "F"
                Case 73 : Pre = "G"
                Case 85 : Pre = "H"
            End Select
            j = j + 1
            Suf = Convert.ToString(j)
            If j = 12 Then j = 0
            Grid(i + 96) = "P2" & Pre & Suf
            DataGrid.Add(Grid(i + 96), "")
        Next i

        Grid(0) = "EP"
        'Grid(1) = "Control 1"
        'Grid(2) = "Control 2"
        'Grid(3) = "Low"
        'Grid(4) = "High"
        'Grid(5) = "Hydro"
        'Grid(6) = "Negative"
        CurrPos = 1
        grdPlate1.ClearSelection()
        grdPlate2.ClearSelection()
        txtFldr.Text = ""
        txtFldr.Focus()
    End Sub

    Private Sub epMotion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width = 1920 And System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height = 1080 Then
            Dim xSize As Size
            xSize.Height = 306
            xSize.Width = 828
            grdPlate1.Size = xSize
            grdPlate2.Size = xSize
        End If
        DataGrid = New NameValueCollection
        LoadGrid()
        If CLSLIMS.THC Or CLSLIMS.ETG Then
            txtTray1.Visible = False
            txtTray2.Visible = False
            txtTray3.Visible = False
            txtTray4.Visible = False
        Else
            txtTray1.Visible = True
            txtTray2.Visible = True
            txtTray3.Visible = True
            txtTray4.Visible = True

        End If
    End Sub
    Private Const CP_NOCLOSE_BUTTON As Integer = &H200
    Protected Overloads Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim myCp As CreateParams = MyBase.CreateParams
            myCp.ClassStyle = myCp.ClassStyle Or CP_NOCLOSE_BUTTON
            Return myCp
        End Get
    End Property

    Private Sub XLS(ByRef blnPrint As Boolean, ByRef blnReset As Boolean)
        Dim XLApp As Excel.Application
        Dim XLWBS As Excel.Workbooks
        Dim XLWB As Excel.Workbook
        Dim XLWS As Excel.Worksheet
        Dim x As String = "x"
        Dim i As Integer, j As Integer, c As Integer
        Dim blnTHC As Boolean, blnETG As Boolean
        Dim FilePreFix As String = ""

        XLApp = New Excel.Application
        XLApp.Visible = False
        XLWBS = XLApp.Workbooks
        XLWB = XLWBS.Open("\\cl-agilent\shared\EP Motion\GridsBlank.xlsx")
        'XLWB = XLWBS.Open("C:\EP Motion\GridsBlank.xlsx")
        XLWS = XLWB.Sheets(1)
        c = 1
        For i = 4 To 11
            For j = 2 To 13
                If i = 11 And j = 10 Then
                    i = i
                End If
                XLWS.Cells(i, j) = CLSLIMS.Piece(DataGrid(Grid(c)), "^", 1) & IIf(Len(CLSLIMS.Piece(DataGrid(Grid(c)), "^", 1)) < 9, " ", "")
                blnTHC = CLSLIMS.Piece(DataGrid(Grid(c)), "^", 2) = "1"
                blnETG = CLSLIMS.Piece(DataGrid(Grid(c)), "^", 3) = "1"
                If blnTHC Then
                    XLWS.Cells(i, j).interior.color = Color.Gray
                Else
                    XLWS.Cells(i, j).interior.color = Color.White
                End If
                If blnETG Then
                    XLWS.Cells(i, j).font.underline = True
                Else
                    XLWS.Cells(i, j).font.underline = False
                End If
                If IfSample(DataGrid(Grid(c))) Then UpdXTRAYS(DataGrid(Grid(c)), Grid(c))
                c = c + 1
                If c > DataGrid.Count Then
                    Exit For
                End If
            Next j
            If c > DataGrid.Count Then
                Exit For
            End If
        Next i
        For i = 14 To 21
            For j = 2 To 13
                XLWS.Cells(i, j) = CLSLIMS.Piece(DataGrid(Grid(c)), "^", 1) & IIf(Len(CLSLIMS.Piece(DataGrid(Grid(c)), "^", 1)) < 9, " ", "")
                blnTHC = CLSLIMS.Piece(DataGrid(Grid(c)), "^", 2) = "1"
                blnETG = CLSLIMS.Piece(DataGrid(Grid(c)), "^", 3) = "1"
                If blnTHC Then
                    XLWS.Cells(i, j).interior.color = Color.Gray
                Else
                    XLWS.Cells(i, j).interior.color = Color.White
                End If
                If blnETG Then
                    XLWS.Cells(i, j).font.underline = True
                Else
                    XLWS.Cells(i, j).font.underline = False
                End If
                If IfSample(DataGrid(Grid(c))) Then UpdXTRAYS(DataGrid(Grid(c)), Grid(c))
                c = c + 1
                If c > DataGrid.Count Then
                    Exit For
                End If
            Next j
            If c > DataGrid.Count Then
                Exit For
            End If
        Next i
        If CLSLIMS.THC Then
            XLWS.Cells(1, 8) = "T" & txtFldr.Text
        ElseIf CLSLIMS.ETG Then
            XLWS.Cells(1, 8) = "E" & txtFldr.Text
        Else
            XLWS.Cells(1, 8) = "'" & txtFldr.Text
        End If
        Dim strTrays As String = ""
        If txtTray1.Text <> "" Then
            strTrays = txtTray1.Text & ", "
        End If
        If txtTray2.Text <> "" Then
            strTrays = strTrays & txtTray2.Text & ", "
        End If
        If txtTray3.Text <> "" Then
            strTrays = strTrays & txtTray3.Text & ", "
        End If
        If txtTray4.Text <> "" Then
            strTrays = strTrays & txtTray4.Text
        Else
            'Remove trailing comma
            If strTrays <> "" Then strTrays = Mid(strTrays, 1, Len(strTrays) - 1)
        End If

        XLWS.Cells(33, 3) = strTrays
        FilePreFix = "Grid"
        If CLSLIMS.THC Then FilePreFix = "THC"
        If CLSLIMS.ETG Then FilePreFix = "ETG"
        If System.IO.File.Exists("\\cl-agilent\shared\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx") Then
            System.IO.File.Delete("\\cl-agilent\shared\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx")
        End If
        'If System.IO.File.Exists("C:\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx") Then
        '    System.IO.File.Delete("C:\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx")
        'End If

        XLWB.SaveAs("\\cl-agilent\shared\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx")
        'XLWB.SaveAs("C:\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx")
        If blnPrint Then
            'Print Spreadsheet
            XLWS.PrintOutEx()
        End If
        XLWB.Close()
        XLWBS.Close()
        XLApp.Quit()
        'Save Folder in XTRAYS table

        If blnReset Then
            'Reset for next Block
            LoadGrid()
            txtSpecNum.Text = ""
            txtSpecNum.Focus()
            k = 1
        End If
    End Sub

    Private Function IfSample(ByRef samp As String) As Boolean
        IfSample = False
        If samp = "" Then
            Exit Function
        End If
        samp = Replace(Replace(samp, ")", ""), "(", "")
        samp = Piece(samp, " ", 1)
        Dim Sel As String = "Select TRAY From XTRAYS Where PSPECNO = '" & samp & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                IfSample = True
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
    Private Sub UpdXTRAYS(ByRef samp As String, ByRef POS As String)
        If CLSLIMS.THC Or CLSLIMS.ETG Then Exit Sub
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        samp = Replace(Replace(samp, ")", ""), "(", "")
        samp = Piece(samp, " ", 1)
        If Len(POS) = 4 Then POS = Mid(POS, 1, 3) & "0" & Mid(POS, 4, 1)
        Cmd.CommandText = "Update XTRAYS Set FOLDER='" & txtFldr.Text & "',POS='" & POS & "' Where PSPECNO='" & samp & "'"
        Cmd.ExecuteNonQuery()

    End Sub
    Private Function Piece(ByVal str As String, Optional ByVal charz As String = "^", Optional ByVal pos As Integer = 1) As String
        Dim pc As Array
        If pos < 1 Then pos = 1
        If InStr(str, charz) = 0 Then
            Piece = str
            Exit Function
        End If
        pc = Split(str, charz)
        If UBound(pc) < pos - 1 Then
            Piece = ""
        Else
            Piece = pc(pos - 1)
        End If
    End Function

    Private Sub btnCloseNow_Click(sender As System.Object, e As System.EventArgs) Handles btnCloseNow.Click
        Dim ans As MsgBoxResult = MsgBox("Would you like to print worksheets?", MsgBoxStyle.YesNo)
        blnPrint = False
        If ans = MsgBoxResult.Yes Then
            blnPrint = True
        End If
        Call XLS(blnPrint, True)
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub grdPlate1_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdPlate1.CellClick
        grdPlate2.ClearSelection()
        Select Case e.RowIndex
            Case 0 : CurrPos = e.ColumnIndex + 1
            Case 1 : CurrPos = e.ColumnIndex + 13
            Case 2 : CurrPos = e.ColumnIndex + 25
            Case 3 : CurrPos = e.ColumnIndex + 37
            Case 4 : CurrPos = e.ColumnIndex + 49
            Case 5 : CurrPos = e.ColumnIndex + 61
            Case 6 : CurrPos = e.ColumnIndex + 73
            Case 7 : CurrPos = e.ColumnIndex + 85
        End Select
        txtSpecNum.Focus()
    End Sub
    Private Sub grdPlate2_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdPlate2.CellClick
        grdPlate1.ClearSelection()
        Select Case e.RowIndex
            Case 0 : CurrPos = e.ColumnIndex + 97
            Case 1 : CurrPos = e.ColumnIndex + 109
            Case 2 : CurrPos = e.ColumnIndex + 121
            Case 3 : CurrPos = e.ColumnIndex + 133
            Case 4 : CurrPos = e.ColumnIndex + 145
            Case 5 : CurrPos = e.ColumnIndex + 157
            Case 6 : CurrPos = e.ColumnIndex + 169
            Case 7 : CurrPos = e.ColumnIndex + 181
        End Select
        txtSpecNum.Focus()
    End Sub


    Private Sub txtBatch_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtFldr.KeyPress

        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtFldr.Text = "" Then
            Exit Sub
        End If
        Dim XLApp As Excel.Application
        Dim XLWBS As Excel.Workbooks
        Dim XLWB As Excel.Workbook
        Dim XLWS As Excel.Worksheet
        Dim c As Integer, x As Integer, y As Integer, i As Integer
        Dim FilePreFix As String = ""
        FilePreFix = "Grid"
        If CLSLIMS.THC Then FilePreFix = "THC"
        If CLSLIMS.ETG Then FilePreFix = "ETG"
        If System.IO.File.Exists("\\cl-agilent\shared\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx") Then
            'If System.IO.File.Exists("C:\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx") Then
            'Load grids from spreadsheet
            XLApp = New Excel.Application
            XLApp.Visible = False
            XLWBS = XLApp.Workbooks
            XLWB = XLWBS.Open("\\cl-agilent\shared\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx")
            'XLWB = XLWBS.Open("C:\EP Motion\Archive\" & FilePreFix & txtFldr.Text & ".xlsx")
            XLWS = XLWB.Sheets(1)

            c = 1
            For i = 4 To 11
                For j = 2 To 13
                    Select Case Mid(Grid(c), 3, 1)
                        Case "A" : x = 0
                        Case "B" : x = 1
                        Case "C" : x = 2
                        Case "D" : x = 3
                        Case "E" : x = 4
                        Case "F" : x = 5
                        Case "G" : x = 6
                        Case "H" : x = 7
                    End Select
                    y = Val(Mid(Grid(c), 4)) - 1
                    DataGrid.Item(Grid(c)) = XLWS.Cells(i, j).Value
                    grdPlate1.Rows(x).Cells(y).Value = XLWS.Cells(i, j).Value

                    If System.Drawing.ColorTranslator.FromOle(CInt(CDbl(XLWS.Cells(i, j).interior.color))) = Color.Gray Then
                        DataGrid.Item(Grid(c)) = CLSLIMS.ReplacePiece(DataGrid.Item(Grid(c)), "^", 2, "1")
                        grdPlate1.Rows(x).Cells(y).Style.BackColor = Color.LightBlue
                    End If
                    If XLWS.Cells(i, j).font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle Then
                        DataGrid.Item(Grid(c)) = CLSLIMS.ReplacePiece(DataGrid.Item(Grid(c)), "^", 3, "1")
                        grdPlate1.Rows(x).Cells(y).Style.Font = New Font(grdPlate1.Font, FontStyle.Underline)
                    End If
                    c = c + 1
                Next j
            Next i
            For i = 14 To 21
                For j = 2 To 13
                    Select Case Mid(Grid(c), 3, 1)
                        Case "A" : x = 0
                        Case "B" : x = 1
                        Case "C" : x = 2
                        Case "D" : x = 3
                        Case "E" : x = 4
                        Case "F" : x = 5
                        Case "G" : x = 6
                        Case "H" : x = 7
                    End Select
                    y = Val(Mid(Grid(c), 4)) - 1
                    DataGrid.Item(Grid(c)) = XLWS.Cells(i, j).Value
                    grdPlate2.Rows(x).Cells(y).Value = XLWS.Cells(i, j).Value

                    If System.Drawing.ColorTranslator.FromOle(CInt(CDbl(XLWS.Cells(i, j).interior.color))) = Color.Gray Then
                        DataGrid.Item(Grid(c)) = CLSLIMS.ReplacePiece(DataGrid.Item(Grid(c)), "^", 2, "1")
                        grdPlate2.Rows(x).Cells(y).Style.BackColor = Color.LightBlue
                    End If
                    If XLWS.Cells(i, j).font.Underline = Excel.XlUnderlineStyle.xlUnderlineStyleSingle Then
                        DataGrid.Item(Grid(c)) = CLSLIMS.ReplacePiece(DataGrid.Item(Grid(c)), "^", 3, "1")
                        grdPlate2.Rows(x).Cells(y).Style.Font = New Font(grdPlate2.Font, FontStyle.Underline)
                    End If
                    c = c + 1
                Next j
            Next i
            'Trays
            Dim strTrays As String = XLWS.Cells(33, 3).Value & ",,,,,,"
            txtTray1.Text = Trim(Piece(strTrays, ",", 1))
            txtTray2.Text = Trim(Piece(strTrays, ",", 2))
            txtTray3.Text = Trim(Piece(strTrays, ",", 3))
            txtTray4.Text = Trim(Piece(strTrays, ",", 4))

            XLWB.Close()
            XLWBS.Close()
            XLApp.Quit()
            txtSpecNum.Focus()

        ElseIf UCase(Mid(txtFldr.Text, 1, 1)) = "N" Then
            Dim DT As String = Format(Now, "yyMMdd")
            Dim FN As String = ""
            For i = 1 To 16
                If i < 10 Then
                    FN = DT & "0" & i
                Else
                    FN = DT & i
                End If
                If Not System.IO.File.Exists("\\cl-agilent\shared\EP Motion\Archive\" & FilePreFix & FN & ".xlsx") Then
                    'If Not System.IO.File.Exists("C:\EP Motion\Archive\" & FilePreFix & FN & ".xlsx") Then
                    txtFldr.Text = FN
                    Call XLS(False, False)
                    Exit For
                End If
            Next i
            CurrPos = ((i - 1) * 12) + 1
            If CLSLIMS.THC Then     'THC
                If i < 9 Then
                    grdPlate1.Rows(i - 1).Cells(0).Value = "BLANK" : DataGrid.Item(Grid(CurrPos)) = "BLANK" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(1).Value = "NEG" : DataGrid.Item(Grid(CurrPos)) = "NEG" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(2).Value = "P1" : DataGrid.Item(Grid(CurrPos)) = "P1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(3).Value = "P2" : DataGrid.Item(Grid(CurrPos)) = "P2" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(4).Value = "H1" : DataGrid.Item(Grid(CurrPos)) = "H1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(5).Value = "H2" : DataGrid.Item(Grid(CurrPos)) = "H2"
                Else
                    grdPlate1.Rows(i - 9).Cells(6).Value = "BLANK" : DataGrid.Item(Grid(CurrPos)) = "BLANK" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(7).Value = "NEG" : DataGrid.Item(Grid(CurrPos)) = "NEG" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(8).Value = "P1" : DataGrid.Item(Grid(CurrPos)) = "P1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(9).Value = "P2" : DataGrid.Item(Grid(CurrPos)) = "P2" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(10).Value = "H1" : DataGrid.Item(Grid(CurrPos)) = "H1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(11).Value = "H2" : DataGrid.Item(Grid(CurrPos)) = "H2"
                End If
                If i < 13 Then
                    grdPlate2.Rows(5).Cells(i - 1).Value = "Plate_QC" : DataGrid.Item(Grid(156 + i)) = "Plate_QC"
                Else
                    grdPlate2.Rows(6).Cells(i - 13).Value = "Plate_QC" : DataGrid.Item(Grid(156 + i)) = "Plate_QC"
                End If
            ElseIf CLSLIMS.ETG Then     'ETG
                CurrPos = 94
                'grdPlate1.Rows(7).Cells(8).Value = "CAL1" : DataGrid.Item(Grid(CurrPos)) = "CAL1" : CurrPos = CurrPos + 1
                grdPlate1.Rows(7).Cells(9).Value = "BLK" : DataGrid.Item(Grid(CurrPos)) = "BLK" : CurrPos = CurrPos + 1
                grdPlate1.Rows(7).Cells(10).Value = "NEG" : DataGrid.Item(Grid(CurrPos)) = "NEG" : CurrPos = CurrPos + 1
                grdPlate1.Rows(7).Cells(11).Value = "POS" : DataGrid.Item(Grid(CurrPos)) = "POS"
                i = 2 ' So that CurrPos will get reset to 1 on first ETG folder
            Else   'Regular
                If i < 9 Then
                    grdPlate1.Rows(i - 1).Cells(0).Value = "BLANK" : DataGrid.Item(Grid(CurrPos)) = "BLANK" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(1).Value = "NEG" : DataGrid.Item(Grid(CurrPos)) = "NEG" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(2).Value = "P1" : DataGrid.Item(Grid(CurrPos)) = "P1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(3).Value = "P2" : DataGrid.Item(Grid(CurrPos)) = "P2" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(4).Value = "H1" : DataGrid.Item(Grid(CurrPos)) = "H1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 1).Cells(5).Value = "H2" : DataGrid.Item(Grid(CurrPos)) = "H2"
                Else
                    grdPlate1.Rows(i - 9).Cells(6).Value = "BLANK" : DataGrid.Item(Grid(CurrPos)) = "BLANK" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(7).Value = "NEG" : DataGrid.Item(Grid(CurrPos)) = "NEG" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(8).Value = "P1" : DataGrid.Item(Grid(CurrPos)) = "P1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(9).Value = "P2" : DataGrid.Item(Grid(CurrPos)) = "P2" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(10).Value = "H1" : DataGrid.Item(Grid(CurrPos)) = "H1" : CurrPos = CurrPos + 1
                    grdPlate1.Rows(i - 9).Cells(11).Value = "H2" : DataGrid.Item(Grid(CurrPos)) = "H2"
                End If
                If i < 13 Then
                    grdPlate2.Rows(5).Cells(i - 1).Value = "Plate_QC" : DataGrid.Item(Grid(156 + i)) = "Plate_QC"
                Else
                    grdPlate2.Rows(6).Cells(i - 13).Value = "Plate_QC" : DataGrid.Item(Grid(156 + i)) = "Plate_QC"
                End If
                'If i < 9 Then
                '    grdPlate1.Rows(i - 1).Cells(0).Value = "LOW1" : DataGrid.Item(Grid(CurrPos)) = "LOW1" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 1).Cells(1).Value = "LOW2" : DataGrid.Item(Grid(CurrPos)) = "LOW2" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 1).Cells(2).Value = "HIGH1" : DataGrid.Item(Grid(CurrPos)) = "HIGH1" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 1).Cells(3).Value = "HIGH2" : DataGrid.Item(Grid(CurrPos)) = "HIGH2" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 1).Cells(4).Value = "HYDRO" : DataGrid.Item(Grid(CurrPos)) = "HYDRO" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 1).Cells(5).Value = "NEG" : DataGrid.Item(Grid(CurrPos)) = "NEG"
                'Else
                '    grdPlate1.Rows(i - 9).Cells(6).Value = "LOW1" : DataGrid.Item(Grid(CurrPos)) = "LOW1" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 9).Cells(7).Value = "LOW2" : DataGrid.Item(Grid(CurrPos)) = "LOW2" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 9).Cells(8).Value = "HIGH1" : DataGrid.Item(Grid(CurrPos)) = "HIGH1" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 9).Cells(9).Value = "HIGH2" : DataGrid.Item(Grid(CurrPos)) = "HIGH2" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 9).Cells(10).Value = "HYDRO" : DataGrid.Item(Grid(CurrPos)) = "HYDRO" : CurrPos = CurrPos + 1
                '    grdPlate1.Rows(i - 9).Cells(11).Value = "NEG" : DataGrid.Item(Grid(CurrPos)) = "NEG"
                'End If
                'If i < 13 Then
                '    grdPlate2.Rows(5).Cells(i - 1).Value = "Plate_QC" : DataGrid.Item(Grid(156 + i)) = "Plate_QC"
                'Else
                '    grdPlate2.Rows(6).Cells(i - 13).Value = "Plate_QC" : DataGrid.Item(Grid(156 + i)) = "Plate_QC"
                'End If
            End If
            If i = 1 Then CurrPos = 7 Else CurrPos = 1
            txtTray1.Focus()
        Else
            MsgBox("Folder " & txtFldr.Text & " not found. Enter N for new folder.")
            txtFldr.Text = ""
            txtFldr.Focus()
        End If
    End Sub

    Private Sub txtTray1_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtTray1.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtTray1.Text = "" Then
            Exit Sub
        End If
        If Not ValidateTray(txtTray1.Text) Then
            MsgBox("Invalid tray number " & txtTray1.Text)
            txtTray1.Text = ""
            txtTray1.Focus()
        Else
            txtTray2.Focus()
        End If
    End Sub
    Private Sub txtTray2_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtTray2.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtTray2.Text = "" Then
            Exit Sub
        End If
        If Not ValidateTray(txtTray2.Text) Then
            MsgBox("Invalid tray number " & txtTray2.Text)
            txtTray2.Text = ""
            txtTray2.Focus()
        Else
            txtTray3.Focus()
        End If
    End Sub
    Private Sub txtTray3_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtTray3.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtTray3.Text = "" Then
            txtTray4.Focus()
            Exit Sub
        End If
        If Not ValidateTray(txtTray3.Text) Then
            MsgBox("Invalid tray number " & txtTray3.Text)
            txtTray3.Text = ""
            txtTray3.Focus()
        Else
            txtTray4.Focus()
        End If
    End Sub
    Private Sub txtTray4_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtTray4.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtTray4.Text = "" Then
            txtSpecNum.Focus()
            Exit Sub
        End If
        If Not ValidateTray(txtTray4.Text) Then
            MsgBox("Invalid tray number " & txtTray4.Text)
            txtTray4.Text = ""
            txtTray4.Focus()
        Else
            txtSpecNum.Focus()
        End If
    End Sub

    Private Function ValidateTray(ByRef tray As String) As Boolean
        ValidateTray = False
        Dim Sel As String = "Select TRAY From XTRAYS Where TRAY = '" & tray & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                ValidateTray = True
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
End Class
