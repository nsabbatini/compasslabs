﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmReceiveHistory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.grdHistory = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInvoice = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReceivedby = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateReceived = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVendor = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.grdHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdHistory
        '
        Me.grdHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdHistory.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grdHistory.Location = New System.Drawing.Point(0, 0)
        Me.grdHistory.MainView = Me.GridView1
        Me.grdHistory.Name = "grdHistory"
        Me.grdHistory.Size = New System.Drawing.Size(800, 623)
        Me.grdHistory.TabIndex = 1
        Me.grdHistory.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.colInvoice, Me.colReceivedby, Me.colDateReceived, Me.colVendor})
        Me.GridView1.GridControl = Me.grdHistory
        Me.GridView1.Name = "GridView1"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.FieldName = "id"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'colInvoice
        '
        Me.colInvoice.Caption = "Invoice Number"
        Me.colInvoice.FieldName = "invoice_number"
        Me.colInvoice.MinWidth = 75
        Me.colInvoice.Name = "colInvoice"
        Me.colInvoice.Visible = True
        Me.colInvoice.VisibleIndex = 0
        '
        'colReceivedby
        '
        Me.colReceivedby.Caption = "Received By"
        Me.colReceivedby.FieldName = "received_by"
        Me.colReceivedby.Name = "colReceivedby"
        Me.colReceivedby.Visible = True
        Me.colReceivedby.VisibleIndex = 1
        '
        'colDateReceived
        '
        Me.colDateReceived.Caption = "Date Received"
        Me.colDateReceived.FieldName = "date_received"
        Me.colDateReceived.Name = "colDateReceived"
        Me.colDateReceived.Visible = True
        Me.colDateReceived.VisibleIndex = 2
        '
        'colVendor
        '
        Me.colVendor.Caption = "Vendor"
        Me.colVendor.FieldName = "vendor"
        Me.colVendor.Name = "colVendor"
        Me.colVendor.Visible = True
        Me.colVendor.VisibleIndex = 3
        '
        'frmReceiveHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 623)
        Me.Controls.Add(Me.grdHistory)
        Me.Name = "frmReceiveHistory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receive history"
        CType(Me.grdHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grdHistory As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colInvoice As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReceivedby As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateReceived As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVendor As DevExpress.XtraGrid.Columns.GridColumn
End Class
