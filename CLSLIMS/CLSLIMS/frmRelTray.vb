﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmRelTray
    Public aReady(200) As String
    Private Sub txtTray_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTray.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        lvInfo.Items.Clear()
        lblName.Text = ""
        Dim Name As String = ValidateTray(txtTray.Text)
        If Name <> "" Then
            lblName.Text = Name
            txtTray.Focus()
            Exit Sub
        End If
        LoadTray(txtTray.Text)
    End Sub

    Public Sub LoadTray(ByVal tray As String)
        Dim SomRel As Boolean = False
        lvInfo.Items.Clear()
        Dim item As ListViewItem = Nothing, i As Integer = 0
        Dim sel = "Select pspecno From XTRAYS Where pspecno != 'HOLDER' And TRAY = '" & tray & "' Order By TRAYx"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            item = lvInfo.Items.Add(rs(0))
            If Ready(rs(0)) Then
                item.SubItems.Add("Ready.")
                aReady(i) = rs(0) : i = i + 1
                SomRel = True
            Else
                item.SubItems.Add(CLSLIMS.IntText)
                CLSLIMS.IntText = ""
            End If
            item.SubItems.Add(frmSpecRel.ValidateSpec(rs(0)))
            lvInfo.Refresh()
        Loop
        rs.Close()
        Conn.Close()
        If Not SomRel Then
            lstInfo.Items.Add("Nothing on tray to be released.") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        End If
    End Sub
    Public Function ValidateTray(ByVal tray As String) As String
        ValidateTray = ""
        Dim sel = "Select count(*) From XTRAYS Where tray = '" & txtTray.Text & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            If rs(0) = 0 Then
                ValidateTray = "Tray " & tray & " is Invalid."
            End If
        End If
        rs.Close()
        Conn.Close()
    End Function

    Private Sub frmRelTray_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-NEDPC" Then CLSLIMS.UID = "NED.SABBATINI"
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "MARCIN.BARTCZAK" And CLSLIMS.UID <> "JOHN.SIDWELL" And CLSLIMS.UID <> "CHRIS.COX" And CLSLIMS.UID <> "L.HIGGENBOTTOM" Then
            Me.Close()
            Me.Dispose()
        End If
        txtTray.Focus()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnRel_Click(sender As Object, e As EventArgs) Handles btnRel.Click
        Dim SomRel As Boolean = False
        For j = 0 To 199
            If aReady(j) <> "" Then
                VerifyPain(aReady(j))
                lstInfo.Items.Add("Releasing " & aReady(j) & ". . .") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1 : lstInfo.Refresh()
                aReady(j) = ""
                SomRel = True
            End If
        Next
        If SomRel Then
            lstInfo.Items.Add("All specimens with unverified PAIN results ")
            lstInfo.Items.Add("have been released for Tray " & txtTray.Text & ".")
            lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        End If
        txtTray.Text = ""
        txtTray.Focus()
        ReDim aReady(200)
    End Sub
    Public Function Ready(ByVal spec As String) As Boolean
        Ready = True

        Dim StatCd As String = CheckStatusCode(spec)
        If StatCd <> "" Then
            lblName.Text = StatCd : CLSLIMS.IntText = StatCd
            Ready = False
            Exit Function
        End If
        Dim Res As String = ResultsNotEntered(spec)
        If Res <> "" Then
            lblName.Text = Res : CLSLIMS.IntText = Res
            Ready = False
            Exit Function
        End If
        Dim Final As String = AlreadyVerified(spec)
        If Final <> "" Then
            lblName.Text = Final : CLSLIMS.IntText = Final
            Ready = False
            Exit Function
        End If

    End Function
    Public Function CheckStatusCode(ByVal specno As String) As String
        CheckStatusCode = ""
        Dim sel = "select a.statcd from statcd a, psstat b, pspec c where c.pspecno='" & specno & "' and psact=1 and b.pspecno=c.pspecno and a.statcd=b.statcd and b.adsq1=c.adsq1 and stcdnre=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            CheckStatusCode = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function

    Public Function ResultsNotEntered(ByVal specno As String) As String
        ResultsNotEntered = ""
        Dim sel = "select RABRV from pres a, rc b, wlrc c where pspecno='" & specno & "' and pract=1 and a.rc=b.rc and a.refdt=b.refdt and prstat < 2 and a.rc=c.rc and a.refdt=c.refdt and c.wl like '%PAIN%'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            ResultsNotEntered = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function AlreadyVerified(ByVal specno As String) As String
        AlreadyVerified = ""
        Dim sel = "select rname from pres a, rc b,wlrc c where pspecno='" & specno & "' and pract=1 and a.rc=b.rc and a.refdt=b.refdt and prstat < 3 and a.rc=c.rc and a.refdt=c.refdt and c.wl like '%PAIN%'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            AlreadyVerified = ""
        Else
            AlreadyVerified = "All PAIN results verified."
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Sub VerifyPain(ByVal specno As String)
        If specno = "" Then Exit Sub
        Dim upd As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        'lstInfo.Items.Add("Verifying . . .")
        upd = "Update PRES Set PRSTAT=3,PRVID='" & CLSLIMS.UID & "',PRVDT='" & CLSLIMS.CDT & "' Where PSPECNO='" & specno & "' And PRSTAT < 3 And PRACT=1 And rc+refdt in (Select rc+refdt From WLRC Where WL Like '%PAIN%')"
        Cmd.CommandText = upd
        Cmd.ExecuteNonQuery()
        'lstInfo.Items.Add("Removing from queues . . .") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        upd = "Delete From QPRES Where PSPECNO='" & specno & "' And TC in (Select TC From TCRC a, PRES b, WLRC c Where PSPECNO='" & specno & "' And A.rc=b.rc and a.refdt=b.refdt and a.rc=c.rc and a.refdt=c.refdt and pract=1 and c.WL Like '%PAIN%')"
        Cmd.CommandText = upd
        Cmd.ExecuteNonQuery()
        upd = "Delete From QWL Where PSPECNO='" & specno & "' And WL Like '%PAIN%'"
        Cmd.CommandText = upd
        Cmd.ExecuteNonQuery()
        If frmSpecRel.CheckQRPMON(specno) Then
            'lstInfo.Items.Add("Adding to report monitor . . .") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
            upd = "Insert Into QRPMON Values ('" & CLSLIMS.CDT & "','" & specno & "')"
            Cmd.CommandText = upd
            Cmd.ExecuteNonQuery()
        End If
    End Sub
End Class