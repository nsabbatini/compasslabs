﻿Imports CLSLIMS.CLSLIMS
Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmPtr
    Private Sub frmPtr_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select lkup From Stuff Where What='PRINTERS' and value='Y'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cbPrinter.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.Dispose()
        ConnMySql.Close()
        ConnMySql.Dispose()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand, Valid As Boolean = True
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select count(*) From Stuff Where What='PRINTERS' and value='Y' and lkup='" & cbPrinter.Text & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If rs(0) = 0 Then
                MsgBox("Invalid Printer.")
                Valid = False
            End If
        End If
        rs.Close()
        Cmd.Dispose()
        ConnMySql.Close()
        ConnMySql.Dispose()
        If Valid Then
            frmQues.CurrPrinter = cbPrinter.Text
            Me.Close()
            Me.Dispose()
        End If
    End Sub
End Class