﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmBuildHam
    Public FN As String = ""

    Private Sub btnOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnOpen.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "\\cl-agilent\shared\Hamilton\"
        openFileDialog1.Filter = "CSV files (*.csv)|*A.csv"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                FN = openFileDialog1.FileName
            Catch Ex As Exception
                MessageBox.Show("Cannot read file from disk. Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open. 
            End Try
        End If
        txtFN.Text = CLSLIMS.Piece(FN, "\", CLSLIMS.NumberofPieces(FN, "\"))

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnBuild_Click(sender As System.Object, e As System.EventArgs) Handles btnBuild.Click
        Dim r As String = "", specno As String = "", Coor As String = "", Row As String = "", Col As String = "", Tray As String = "", Seq As String = "", sq As Integer = 0, TL As String = "", Pos As String = ""
        Dim XLApp As Excel.Application
        Dim XLWBS As Excel.Workbooks
        Dim XLWB As Excel.Workbook
        Dim XLWS As Excel.Worksheet

        Dim i As Integer, j As Integer

        If System.IO.File.Exists("\\cl-agilent\shared\EP Motion\Archive\H" & Replace(CLSLIMS.Piece(txtFN.Text, ".", 1), "A", "") & ".xlsx") Then
            System.IO.File.Delete("\\cl-agilent\shared\EP Motion\Archive\H" & Replace(CLSLIMS.Piece(txtFN.Text, ".", 1), "A", "") & ".xlsx")
        End If

        XLApp = New Excel.Application
        XLApp.Visible = False
        XLWBS = XLApp.Workbooks
        XLWB = XLWBS.Open("\\cl-agilent\shared\EP Motion\GridsBlank.xlsx")
        XLWS = XLWB.Sheets(1)

        Dim file As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader(FN)
        r = file.ReadLine
        Do While Not file.EndOfStream
            r = file.ReadLine
            r = Replace(r, Chr(34), "")
            Coor = CLSLIMS.Piece(r, ",", 1)
            specno = CLSLIMS.Piece(r, ",", 2)
            Tray = GetTray(specno) : Seq = GetTraySeq(specno)
            If Tray <> "" And Seq <> "" Then
                sq = Val(Mid(Tray, 3, 2)) : TL = Mid("ABCDEFGHJKLMNPQRSTUVWXYZ", sq, 1)
                Pos = TL & IIf(Seq < 10, "0", "") & Seq
            Else
                Pos = "Z99"
            End If
            Row = Mid(Coor, 1, 1) : Col = Mid(Coor, 2)
            Select Case Row
                Case "A" : i = 4
                Case "B" : i = 5
                Case "C" : i = 6
                Case "D" : i = 7
                Case "E" : i = 8
                Case "F" : i = 9
                Case "G" : i = 10
                Case "H" : i = 11
            End Select
            Select Case Col
                Case "1" : j = 2
                Case "2" : j = 3
                Case "3" : j = 4
                Case "4" : j = 5
                Case "5" : j = 6
                Case "6" : j = 7
                Case "7" : j = 8
                Case "8" : j = 9
                Case "9" : j = 10
                Case "10" : j = 11
                Case "11" : j = 12
                Case "12" : j = 13
            End Select
            XLWS.Cells(i, j) = specno & " " & Pos
            If CheckforTHCConf(specno) Then
                XLWS.Cells(i, j).interior.color = Color.Gray
            Else
                XLWS.Cells(i, j).interior.color = Color.White
            End If
            If CheckforETGConf(specno) Then
                XLWS.Cells(i, j).font.underline = True
            Else
                XLWS.Cells(i, j).font.underline = False
            End If
        Loop
        file.Close()
        FN = "\\cl-agilent\shared\Hamilton\" & Replace(txtFN.Text, "A", "B")
        If System.IO.File.Exists(FN) Then
            file = My.Computer.FileSystem.OpenTextFileReader(FN)
            r = file.ReadLine
            Do While Not file.EndOfStream
                r = file.ReadLine
                r = Replace(r, Chr(34), "")
                Coor = CLSLIMS.Piece(r, ",", 1)
                specno = CLSLIMS.Piece(r, ",", 2)
                Tray = GetTray(specno) : Seq = GetTraySeq(specno)
                If Tray <> "" And Seq <> "" Then
                    sq = Val(Mid(Tray, 3, 2)) : TL = Mid("ABCDEFGHJKLMNPQRSTUVWXYZ", sq, 1)
                    Pos = TL & IIf(Seq < 10, "0", "") & Seq
                Else
                    Pos = "Z99"
                End If
                Row = Mid(Coor, 1, 1) : Col = Mid(Coor, 2)
                Select Case Row
                    Case "A" : i = 14
                    Case "B" : i = 15
                    Case "C" : i = 16
                    Case "D" : i = 17
                    Case "E" : i = 18
                    Case "F" : i = 19
                    Case "G" : i = 20
                    Case "H" : i = 21
                End Select
                Select Case Col
                    Case "1" : j = 2
                    Case "2" : j = 3
                    Case "3" : j = 4
                    Case "4" : j = 5
                    Case "5" : j = 6
                    Case "6" : j = 7
                    Case "7" : j = 8
                    Case "8" : j = 9
                    Case "9" : j = 10
                    Case "10" : j = 11
                    Case "11" : j = 12
                    Case "12" : j = 13
                End Select
                XLWS.Cells(i, j) = specno & " " & Pos
                If CheckforTHCConf(specno) Then
                    XLWS.Cells(i, j).interior.color = Color.Gray
                Else
                    XLWS.Cells(i, j).interior.color = Color.White
                End If
                If CheckforETGConf(specno) Then
                    XLWS.Cells(i, j).font.underline = True
                Else
                    XLWS.Cells(i, j).font.underline = False
                End If
            Loop
            file.Close()
        End If
        XLWS.Cells(1, 8) = Replace(CLSLIMS.Piece(txtFN.Text, ".", 1), "A", "")
        XLWB.SaveAs("\\cl-agilent\shared\EP Motion\Archive\H" & Replace(CLSLIMS.Piece(txtFN.Text, ".", 1), "A", "") & ".xlsx")
        Me.Close()
    End Sub

    Private Sub frmBuildHam_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtFN.Text = ""
    End Sub
    Public Function CheckforTHCConf(ByVal specno As String) As Boolean
        CheckforTHCConf = False
        Dim Sel As String = "Select count(*) From POCTC Where PSPECNO='" & specno & "' And TC In ('302080','302050','302060','302070')"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforTHCConf = True
                End If
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using
    End Function
    Public Function CheckforETGConf(ByVal specno As String) As Boolean
        CheckforETGConf = False
        Dim Sel As String = "Select count(*) From POCTC Where PSPECNO='" & specno & "' And TC In ('308010','308020')"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforETGConf = True
                End If
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using
    End Function
    Private Function GetTray(specno As String) As String
        GetTray = ""
        Dim Sel As String = "Select TRAY From XTRAYS Where PSPECNO='" & specno & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                GetTray = rs(0)
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using
    End Function
    Public Function GetTraySeq(ByVal spec As String) As String
        Dim xSel As String = "Select TRAYx From XTRAYS Where PSPECNO ='" & spec & "'"
        GetTraySeq = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetTraySeq = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
End Class