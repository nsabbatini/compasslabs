﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PhyInq
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPhy = New System.Windows.Forms.TextBox()
        Me.lstAcctPhy = New System.Windows.Forms.ListBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cbRes = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(286, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Physician's Last Name Begin with:"
        '
        'txtPhy
        '
        Me.txtPhy.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhy.Location = New System.Drawing.Point(295, 16)
        Me.txtPhy.Name = "txtPhy"
        Me.txtPhy.Size = New System.Drawing.Size(203, 27)
        Me.txtPhy.TabIndex = 0
        '
        'lstAcctPhy
        '
        Me.lstAcctPhy.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAcctPhy.FormattingEnabled = True
        Me.lstAcctPhy.ItemHeight = 16
        Me.lstAcctPhy.Location = New System.Drawing.Point(7, 70)
        Me.lstAcctPhy.Name = "lstAcctPhy"
        Me.lstAcctPhy.Size = New System.Drawing.Size(728, 132)
        Me.lstAcctPhy.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(660, 212)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 27)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "EXIT"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cbRes
        '
        Me.cbRes.AutoSize = True
        Me.cbRes.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbRes.Location = New System.Drawing.Point(574, 23)
        Me.cbRes.Name = "cbRes"
        Me.cbRes.Size = New System.Drawing.Size(149, 23)
        Me.cbRes.TabIndex = 4
        Me.cbRes.Text = "Residents Only"
        Me.cbRes.UseVisualStyleBackColor = True
        '
        'PhyInq
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(747, 251)
        Me.Controls.Add(Me.cbRes)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lstAcctPhy)
        Me.Controls.Add(Me.txtPhy)
        Me.Controls.Add(Me.Label1)
        Me.Name = "PhyInq"
        Me.Text = "Physician Inquiry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPhy As System.Windows.Forms.TextBox
    Friend WithEvents lstAcctPhy As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cbRes As CheckBox
End Class
