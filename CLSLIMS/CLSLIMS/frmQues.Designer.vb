﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmQues
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstOrdApp = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lstPrt = New System.Windows.Forms.ListBox()
        Me.lstPick = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lstShip = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PrintReq = New System.Drawing.Printing.PrintDocument()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lstReqNum = New System.Windows.Forms.ListBox()
        Me.CovidReq = New System.Drawing.Printing.PrintDocument()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(83, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(263, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Order Needing to be Approved:"
        '
        'lstOrdApp
        '
        Me.lstOrdApp.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOrdApp.FormattingEnabled = True
        Me.lstOrdApp.ItemHeight = 18
        Me.lstOrdApp.Location = New System.Drawing.Point(12, 30)
        Me.lstOrdApp.Name = "lstOrdApp"
        Me.lstOrdApp.Size = New System.Drawing.Size(552, 112)
        Me.lstOrdApp.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(83, 151)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(245, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Order Needing to be Printed:"
        '
        'lstPrt
        '
        Me.lstPrt.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPrt.FormattingEnabled = True
        Me.lstPrt.ItemHeight = 18
        Me.lstPrt.Location = New System.Drawing.Point(12, 173)
        Me.lstPrt.Name = "lstPrt"
        Me.lstPrt.Size = New System.Drawing.Size(552, 112)
        Me.lstPrt.TabIndex = 3
        '
        'lstPick
        '
        Me.lstPick.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPick.FormattingEnabled = True
        Me.lstPick.ItemHeight = 18
        Me.lstPick.Location = New System.Drawing.Point(12, 498)
        Me.lstPick.Name = "lstPick"
        Me.lstPick.Size = New System.Drawing.Size(552, 112)
        Me.lstPick.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(102, 465)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(239, 19)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Order Needing to be Picked:"
        '
        'lstShip
        '
        Me.lstShip.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstShip.FormattingEnabled = True
        Me.lstShip.ItemHeight = 18
        Me.lstShip.Location = New System.Drawing.Point(12, 659)
        Me.lstShip.Name = "lstShip"
        Me.lstShip.Size = New System.Drawing.Size(552, 112)
        Me.lstShip.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(83, 626)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(258, 19)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Orders Needing to be Shipped:"
        '
        'PrintReq
        '
        Me.PrintReq.DocumentName = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(89, 299)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(196, 19)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Order Needing Req #s:"
        '
        'lstReqNum
        '
        Me.lstReqNum.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstReqNum.FormattingEnabled = True
        Me.lstReqNum.ItemHeight = 18
        Me.lstReqNum.Location = New System.Drawing.Point(10, 339)
        Me.lstReqNum.Name = "lstReqNum"
        Me.lstReqNum.Size = New System.Drawing.Size(552, 112)
        Me.lstReqNum.TabIndex = 9
        '
        'CovidReq
        '
        Me.CovidReq.DocumentName = ""
        '
        'frmQues
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(574, 796)
        Me.Controls.Add(Me.lstReqNum)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lstShip)
        Me.Controls.Add(Me.lstPick)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lstPrt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lstOrdApp)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmQues"
        Me.Text = "Queues"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents lstOrdApp As ListBox
    Friend WithEvents Label2 As Label
    Friend WithEvents lstPrt As ListBox
    Friend WithEvents lstPick As ListBox
    Friend WithEvents Label3 As Label
    Friend WithEvents lstShip As ListBox
    Friend WithEvents Label4 As Label
    Friend WithEvents PrintReq As Printing.PrintDocument
    Friend WithEvents Label5 As Label
    Friend WithEvents lstReqNum As ListBox
    Friend WithEvents CovidReq As Printing.PrintDocument
End Class
