﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecvInv
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lstInvRecv = New System.Windows.Forms.ListBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbVendorId = New System.Windows.Forms.ComboBox()
        Me.txtInvNum = New System.Windows.Forms.TextBox()
        Me.dtpRecvDt = New System.Windows.Forms.DateTimePicker()
        Me.cbItem = New System.Windows.Forms.ComboBox()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.cbVendorItem = New System.Windows.Forms.ComboBox()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.lblVndName = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(73, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Vendor Id:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(25, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Invoice Number:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(133, 19)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Date Received:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(68, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 19)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Item Id:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(59, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 19)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Quantity:"
        '
        'lstInvRecv
        '
        Me.lstInvRecv.FormattingEnabled = True
        Me.lstInvRecv.ItemHeight = 19
        Me.lstInvRecv.Location = New System.Drawing.Point(9, 112)
        Me.lstInvRecv.Name = "lstInvRecv"
        Me.lstInvRecv.Size = New System.Drawing.Size(714, 137)
        Me.lstInvRecv.TabIndex = 5
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(600, 71)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(65, 35)
        Me.btnAdd.TabIndex = 6
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(548, 443)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 35)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(654, 443)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 35)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(5, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(139, 19)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Vendor Item Id:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Panel1.Controls.Add(Me.txtQty)
        Me.Panel1.Controls.Add(Me.cbVendorItem)
        Me.Panel1.Controls.Add(Me.cbItem)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lstInvRecv)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Location = New System.Drawing.Point(29, 158)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(737, 265)
        Me.Panel1.TabIndex = 10
        '
        'cbVendorId
        '
        Me.cbVendorId.FormattingEnabled = True
        Me.cbVendorId.Location = New System.Drawing.Point(185, 40)
        Me.cbVendorId.Name = "cbVendorId"
        Me.cbVendorId.Size = New System.Drawing.Size(183, 27)
        Me.cbVendorId.TabIndex = 11
        '
        'txtInvNum
        '
        Me.txtInvNum.Location = New System.Drawing.Point(185, 71)
        Me.txtInvNum.Name = "txtInvNum"
        Me.txtInvNum.Size = New System.Drawing.Size(183, 27)
        Me.txtInvNum.TabIndex = 12
        '
        'dtpRecvDt
        '
        Me.dtpRecvDt.Location = New System.Drawing.Point(185, 104)
        Me.dtpRecvDt.Name = "dtpRecvDt"
        Me.dtpRecvDt.Size = New System.Drawing.Size(183, 27)
        Me.dtpRecvDt.TabIndex = 13
        '
        'cbItem
        '
        Me.cbItem.FormattingEnabled = True
        Me.cbItem.Location = New System.Drawing.Point(156, 36)
        Me.cbItem.Name = "cbItem"
        Me.cbItem.Size = New System.Drawing.Size(121, 27)
        Me.cbItem.TabIndex = 10
        '
        'lstData
        '
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 19
        Me.lstData.Location = New System.Drawing.Point(39, 455)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(70, 23)
        Me.lstData.TabIndex = 14
        Me.lstData.UseTabStops = False
        Me.lstData.Visible = False
        '
        'cbVendorItem
        '
        Me.cbVendorItem.FormattingEnabled = True
        Me.cbVendorItem.Location = New System.Drawing.Point(156, 9)
        Me.cbVendorItem.Name = "cbVendorItem"
        Me.cbVendorItem.Size = New System.Drawing.Size(121, 27)
        Me.cbVendorItem.TabIndex = 11
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(156, 63)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(73, 27)
        Me.txtQty.TabIndex = 12
        '
        'lblVndName
        '
        Me.lblVndName.AutoSize = True
        Me.lblVndName.Location = New System.Drawing.Point(389, 43)
        Me.lblVndName.Name = "lblVndName"
        Me.lblVndName.Size = New System.Drawing.Size(119, 19)
        Me.lblVndName.TabIndex = 15
        Me.lblVndName.Text = "Vendor Name"
        '
        'frmRecvInv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 490)
        Me.Controls.Add(Me.lblVndName)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.dtpRecvDt)
        Me.Controls.Add(Me.txtInvNum)
        Me.Controls.Add(Me.cbVendorId)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmRecvInv"
        Me.Text = "Receive Inventory"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lstInvRecv As ListBox
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents cbVendorId As ComboBox
    Friend WithEvents txtInvNum As TextBox
    Friend WithEvents dtpRecvDt As DateTimePicker
    Friend WithEvents cbItem As ComboBox
    Friend WithEvents lstData As ListBox
    Friend WithEvents cbVendorItem As ComboBox
    Friend WithEvents txtQty As TextBox
    Friend WithEvents lblVndName As Label
End Class
