﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AcctInq
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.lblAcct = New System.Windows.Forms.Label()
        Me.lblCityST = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.lblFax = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblSls = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblColl = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblOC = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lstPhy = New System.Windows.Forms.ListBox()
        Me.lstReqs = New System.Windows.Forms.ListBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblRpt = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblCreate = New System.Windows.Forms.Label()
        Me.lblUpdate = New System.Windows.Forms.Label()
        Me.lstAcctNameSearch = New System.Windows.Forms.ListBox()
        Me.lblSlsPhone = New System.Windows.Forms.Label()
        Me.lblChiral = New System.Windows.Forms.Label()
        Me.btnImgD = New System.Windows.Forms.Button()
        Me.btnImgC = New System.Windows.Forms.Button()
        Me.btnImgB = New System.Windows.Forms.Button()
        Me.btnImgA = New System.Windows.Forms.Button()
        Me.btnImgE = New System.Windows.Forms.Button()
        Me.btnImgF = New System.Windows.Forms.Button()
        Me.btnImgG = New System.Windows.Forms.Button()
        Me.btnGrid = New System.Windows.Forms.Button()
        Me.btnNotes = New System.Windows.Forms.Button()
        Me.lblAddr2 = New System.Windows.Forms.Label()
        Me.lblAddr1 = New System.Windows.Forms.Label()
        Me.lblColl2 = New System.Windows.Forms.Label()
        Me.btnVol = New System.Windows.Forms.Button()
        Me.lblVolMo = New System.Windows.Forms.Label()
        Me.lblVol = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(358, 725)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 26)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "EXIT"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Account:"
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(97, 12)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(109, 27)
        Me.txtAcct.TabIndex = 0
        '
        'lblAcct
        '
        Me.lblAcct.AutoSize = True
        Me.lblAcct.BackColor = System.Drawing.SystemColors.Window
        Me.lblAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcct.Location = New System.Drawing.Point(15, 50)
        Me.lblAcct.Name = "lblAcct"
        Me.lblAcct.Size = New System.Drawing.Size(0, 19)
        Me.lblAcct.TabIndex = 3
        '
        'lblCityST
        '
        Me.lblCityST.BackColor = System.Drawing.SystemColors.Window
        Me.lblCityST.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCityST.Location = New System.Drawing.Point(67, 118)
        Me.lblCityST.Name = "lblCityST"
        Me.lblCityST.Size = New System.Drawing.Size(200, 20)
        Me.lblCityST.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 19)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Phone:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(47, 172)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Fax:"
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.BackColor = System.Drawing.SystemColors.Window
        Me.lblPhone.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(93, 144)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(123, 19)
        Me.lblPhone.TabIndex = 7
        Me.lblPhone.Text = "901-378-1979"
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.BackColor = System.Drawing.SystemColors.Window
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(93, 172)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(0, 19)
        Me.lblFax.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(19, 278)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 19)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Salesman:"
        '
        'lblSls
        '
        Me.lblSls.AutoSize = True
        Me.lblSls.BackColor = System.Drawing.SystemColors.Window
        Me.lblSls.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSls.Location = New System.Drawing.Point(112, 278)
        Me.lblSls.Name = "lblSls"
        Me.lblSls.Size = New System.Drawing.Size(0, 19)
        Me.lblSls.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(2, 320)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 19)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Collector(s):"
        '
        'lblColl
        '
        Me.lblColl.AutoSize = True
        Me.lblColl.BackColor = System.Drawing.SystemColors.Window
        Me.lblColl.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColl.Location = New System.Drawing.Point(112, 320)
        Me.lblColl.Name = "lblColl"
        Me.lblColl.Size = New System.Drawing.Size(0, 19)
        Me.lblColl.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 231)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(219, 19)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Default Ordering Code(s):"
        '
        'lblOC
        '
        Me.lblOC.AutoSize = True
        Me.lblOC.BackColor = System.Drawing.SystemColors.Window
        Me.lblOC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOC.Location = New System.Drawing.Point(27, 204)
        Me.lblOC.Name = "lblOC"
        Me.lblOC.Size = New System.Drawing.Size(0, 19)
        Me.lblOC.TabIndex = 14
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 370)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(113, 19)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Physician(s):"
        '
        'lstPhy
        '
        Me.lstPhy.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPhy.FormattingEnabled = True
        Me.lstPhy.ItemHeight = 16
        Me.lstPhy.Location = New System.Drawing.Point(133, 370)
        Me.lstPhy.Name = "lstPhy"
        Me.lstPhy.Size = New System.Drawing.Size(145, 68)
        Me.lstPhy.TabIndex = 16
        '
        'lstReqs
        '
        Me.lstReqs.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstReqs.FormattingEnabled = True
        Me.lstReqs.ItemHeight = 16
        Me.lstReqs.Location = New System.Drawing.Point(19, 504)
        Me.lstReqs.Name = "lstReqs"
        Me.lstReqs.Size = New System.Drawing.Size(447, 84)
        Me.lstReqs.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(19, 472)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 19)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Printed Reqs:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(5, 201)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(184, 19)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Reporting Method(s):"
        '
        'lblRpt
        '
        Me.lblRpt.AutoSize = True
        Me.lblRpt.BackColor = System.Drawing.SystemColors.Window
        Me.lblRpt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRpt.Location = New System.Drawing.Point(195, 201)
        Me.lblRpt.Name = "lblRpt"
        Me.lblRpt.Size = New System.Drawing.Size(0, 19)
        Me.lblRpt.TabIndex = 20
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(242, 144)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 19)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Created:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(238, 173)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(84, 19)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Updated:"
        '
        'lblCreate
        '
        Me.lblCreate.AutoSize = True
        Me.lblCreate.BackColor = System.Drawing.SystemColors.Window
        Me.lblCreate.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreate.Location = New System.Drawing.Point(328, 144)
        Me.lblCreate.Name = "lblCreate"
        Me.lblCreate.Size = New System.Drawing.Size(107, 19)
        Me.lblCreate.TabIndex = 23
        Me.lblCreate.Text = "03/24/2014"
        '
        'lblUpdate
        '
        Me.lblUpdate.AutoSize = True
        Me.lblUpdate.BackColor = System.Drawing.SystemColors.Window
        Me.lblUpdate.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpdate.Location = New System.Drawing.Point(328, 173)
        Me.lblUpdate.Name = "lblUpdate"
        Me.lblUpdate.Size = New System.Drawing.Size(107, 19)
        Me.lblUpdate.TabIndex = 24
        Me.lblUpdate.Text = "03/24/2014"
        '
        'lstAcctNameSearch
        '
        Me.lstAcctNameSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAcctNameSearch.FormattingEnabled = True
        Me.lstAcctNameSearch.ItemHeight = 14
        Me.lstAcctNameSearch.Location = New System.Drawing.Point(95, 47)
        Me.lstAcctNameSearch.Name = "lstAcctNameSearch"
        Me.lstAcctNameSearch.Size = New System.Drawing.Size(338, 200)
        Me.lstAcctNameSearch.TabIndex = 25
        Me.lstAcctNameSearch.Visible = False
        '
        'lblSlsPhone
        '
        Me.lblSlsPhone.AutoSize = True
        Me.lblSlsPhone.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSlsPhone.Location = New System.Drawing.Point(129, 298)
        Me.lblSlsPhone.Name = "lblSlsPhone"
        Me.lblSlsPhone.Size = New System.Drawing.Size(0, 19)
        Me.lblSlsPhone.TabIndex = 26
        '
        'lblChiral
        '
        Me.lblChiral.AutoSize = True
        Me.lblChiral.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChiral.Location = New System.Drawing.Point(284, 373)
        Me.lblChiral.Name = "lblChiral"
        Me.lblChiral.Size = New System.Drawing.Size(0, 19)
        Me.lblChiral.TabIndex = 27
        '
        'btnImgD
        '
        Me.btnImgD.Enabled = False
        Me.btnImgD.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImgD.Location = New System.Drawing.Point(173, 657)
        Me.btnImgD.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImgD.Name = "btnImgD"
        Me.btnImgD.Size = New System.Drawing.Size(132, 45)
        Me.btnImgD.TabIndex = 32
        Me.btnImgD.Text = "D"
        Me.btnImgD.UseVisualStyleBackColor = True
        '
        'btnImgC
        '
        Me.btnImgC.Enabled = False
        Me.btnImgC.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImgC.Location = New System.Drawing.Point(28, 657)
        Me.btnImgC.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImgC.Name = "btnImgC"
        Me.btnImgC.Size = New System.Drawing.Size(132, 45)
        Me.btnImgC.TabIndex = 33
        Me.btnImgC.Text = "C"
        Me.btnImgC.UseVisualStyleBackColor = True
        '
        'btnImgB
        '
        Me.btnImgB.Enabled = False
        Me.btnImgB.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImgB.Location = New System.Drawing.Point(173, 608)
        Me.btnImgB.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImgB.Name = "btnImgB"
        Me.btnImgB.Size = New System.Drawing.Size(132, 45)
        Me.btnImgB.TabIndex = 34
        Me.btnImgB.Text = "B"
        Me.btnImgB.UseVisualStyleBackColor = True
        '
        'btnImgA
        '
        Me.btnImgA.Enabled = False
        Me.btnImgA.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImgA.Location = New System.Drawing.Point(28, 608)
        Me.btnImgA.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImgA.Name = "btnImgA"
        Me.btnImgA.Size = New System.Drawing.Size(132, 45)
        Me.btnImgA.TabIndex = 35
        Me.btnImgA.Text = "A"
        Me.btnImgA.UseVisualStyleBackColor = True
        '
        'btnImgE
        '
        Me.btnImgE.Enabled = False
        Me.btnImgE.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImgE.Location = New System.Drawing.Point(28, 706)
        Me.btnImgE.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImgE.Name = "btnImgE"
        Me.btnImgE.Size = New System.Drawing.Size(132, 45)
        Me.btnImgE.TabIndex = 36
        Me.btnImgE.Text = "E"
        Me.btnImgE.UseVisualStyleBackColor = True
        '
        'btnImgF
        '
        Me.btnImgF.Enabled = False
        Me.btnImgF.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImgF.Location = New System.Drawing.Point(173, 706)
        Me.btnImgF.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImgF.Name = "btnImgF"
        Me.btnImgF.Size = New System.Drawing.Size(132, 45)
        Me.btnImgF.TabIndex = 37
        Me.btnImgF.Text = "F"
        Me.btnImgF.UseVisualStyleBackColor = True
        '
        'btnImgG
        '
        Me.btnImgG.Enabled = False
        Me.btnImgG.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImgG.Location = New System.Drawing.Point(314, 608)
        Me.btnImgG.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImgG.Name = "btnImgG"
        Me.btnImgG.Size = New System.Drawing.Size(132, 45)
        Me.btnImgG.TabIndex = 38
        Me.btnImgG.Text = "G"
        Me.btnImgG.UseVisualStyleBackColor = True
        '
        'btnGrid
        '
        Me.btnGrid.Enabled = False
        Me.btnGrid.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrid.Location = New System.Drawing.Point(242, 226)
        Me.btnGrid.Name = "btnGrid"
        Me.btnGrid.Size = New System.Drawing.Size(51, 33)
        Me.btnGrid.TabIndex = 39
        Me.btnGrid.Text = "Grid"
        Me.btnGrid.UseVisualStyleBackColor = True
        '
        'btnNotes
        '
        Me.btnNotes.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNotes.Location = New System.Drawing.Point(314, 657)
        Me.btnNotes.Margin = New System.Windows.Forms.Padding(2)
        Me.btnNotes.Name = "btnNotes"
        Me.btnNotes.Size = New System.Drawing.Size(132, 45)
        Me.btnNotes.TabIndex = 40
        Me.btnNotes.Text = "NOTES"
        Me.btnNotes.UseVisualStyleBackColor = True
        '
        'lblAddr2
        '
        Me.lblAddr2.BackColor = System.Drawing.SystemColors.Window
        Me.lblAddr2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddr2.Location = New System.Drawing.Point(67, 95)
        Me.lblAddr2.Name = "lblAddr2"
        Me.lblAddr2.Size = New System.Drawing.Size(200, 20)
        Me.lblAddr2.TabIndex = 41
        '
        'lblAddr1
        '
        Me.lblAddr1.BackColor = System.Drawing.SystemColors.Window
        Me.lblAddr1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddr1.Location = New System.Drawing.Point(67, 72)
        Me.lblAddr1.Name = "lblAddr1"
        Me.lblAddr1.Size = New System.Drawing.Size(200, 20)
        Me.lblAddr1.TabIndex = 42
        '
        'lblColl2
        '
        Me.lblColl2.AutoSize = True
        Me.lblColl2.BackColor = System.Drawing.SystemColors.Window
        Me.lblColl2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColl2.Location = New System.Drawing.Point(112, 342)
        Me.lblColl2.Name = "lblColl2"
        Me.lblColl2.Size = New System.Drawing.Size(0, 19)
        Me.lblColl2.TabIndex = 43
        '
        'btnVol
        '
        Me.btnVol.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVol.Location = New System.Drawing.Point(314, 410)
        Me.btnVol.Name = "btnVol"
        Me.btnVol.Size = New System.Drawing.Size(82, 28)
        Me.btnVol.TabIndex = 67
        Me.btnVol.Text = "Volume"
        Me.btnVol.UseVisualStyleBackColor = True
        '
        'lblVolMo
        '
        Me.lblVolMo.AutoSize = True
        Me.lblVolMo.Font = New System.Drawing.Font("Courier New", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVolMo.Location = New System.Drawing.Point(250, 458)
        Me.lblVolMo.Name = "lblVolMo"
        Me.lblVolMo.Size = New System.Drawing.Size(224, 14)
        Me.lblVolMo.TabIndex = 68
        Me.lblVolMo.Text = "2019-10 2019-09 2019-08 2019-07"
        '
        'lblVol
        '
        Me.lblVol.AutoSize = True
        Me.lblVol.Font = New System.Drawing.Font("Courier New", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVol.Location = New System.Drawing.Point(250, 477)
        Me.lblVol.Name = "lblVol"
        Me.lblVol.Size = New System.Drawing.Size(196, 14)
        Me.lblVol.TabIndex = 69
        Me.lblVol.Text = "999     999     999     999"
        '
        'AcctInq
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 768)
        Me.Controls.Add(Me.lblVol)
        Me.Controls.Add(Me.lblVolMo)
        Me.Controls.Add(Me.btnVol)
        Me.Controls.Add(Me.lblColl2)
        Me.Controls.Add(Me.lstAcctNameSearch)
        Me.Controls.Add(Me.btnNotes)
        Me.Controls.Add(Me.btnGrid)
        Me.Controls.Add(Me.btnImgG)
        Me.Controls.Add(Me.btnImgF)
        Me.Controls.Add(Me.btnImgE)
        Me.Controls.Add(Me.btnImgA)
        Me.Controls.Add(Me.btnImgB)
        Me.Controls.Add(Me.btnImgC)
        Me.Controls.Add(Me.btnImgD)
        Me.Controls.Add(Me.lblChiral)
        Me.Controls.Add(Me.lblSlsPhone)
        Me.Controls.Add(Me.lblUpdate)
        Me.Controls.Add(Me.lblCreate)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblRpt)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lstReqs)
        Me.Controls.Add(Me.lstPhy)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblOC)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblColl)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblSls)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblFax)
        Me.Controls.Add(Me.lblPhone)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCityST)
        Me.Controls.Add(Me.lblAcct)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblAddr2)
        Me.Controls.Add(Me.lblAddr1)
        Me.Name = "AcctInq"
        Me.Text = "Account Inquiry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtAcct As System.Windows.Forms.TextBox
    Friend WithEvents lblAcct As System.Windows.Forms.Label
    Friend WithEvents lblCityST As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblSls As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblColl As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblOC As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lstPhy As System.Windows.Forms.ListBox
    Friend WithEvents lstReqs As System.Windows.Forms.ListBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblRpt As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblCreate As System.Windows.Forms.Label
    Friend WithEvents lblUpdate As System.Windows.Forms.Label
    Friend WithEvents lstAcctNameSearch As System.Windows.Forms.ListBox
    Friend WithEvents lblSlsPhone As System.Windows.Forms.Label
    Friend WithEvents lblChiral As System.Windows.Forms.Label
    Friend WithEvents btnImgD As System.Windows.Forms.Button
    Friend WithEvents btnImgC As Button
    Friend WithEvents btnImgB As Button
    Friend WithEvents btnImgA As Button
    Friend WithEvents btnImgE As Button
    Friend WithEvents btnImgF As Button
    Friend WithEvents btnImgG As Button
    Friend WithEvents btnGrid As Button
    Friend WithEvents btnNotes As Button
    Friend WithEvents lblAddr2 As Label
    Friend WithEvents lblAddr1 As Label
    Friend WithEvents lblColl2 As Label
    Friend WithEvents btnVol As Button
    Friend WithEvents lblVolMo As Label
    Friend WithEvents lblVol As Label
End Class
