﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHist
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFldr = New System.Windows.Forms.TextBox()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lvSamp = New System.Windows.Forms.ListView()
        Me.Drugs = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Samp1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Samp2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Samp3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Samp4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Samp5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(56, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Folder:"
        '
        'txtFldr
        '
        Me.txtFldr.Location = New System.Drawing.Point(138, 42)
        Me.txtFldr.Name = "txtFldr"
        Me.txtFldr.Size = New System.Drawing.Size(100, 27)
        Me.txtFldr.TabIndex = 1
        Me.txtFldr.Text = "20021801"
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(290, 36)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 35)
        Me.btnRun.TabIndex = 2
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(781, 396)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 35)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lvSamp
        '
        Me.lvSamp.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Drugs, Me.Samp1, Me.Samp2, Me.Samp3, Me.Samp4, Me.Samp5})
        Me.lvSamp.Location = New System.Drawing.Point(34, 93)
        Me.lvSamp.Name = "lvSamp"
        Me.lvSamp.Size = New System.Drawing.Size(927, 284)
        Me.lvSamp.TabIndex = 4
        Me.lvSamp.UseCompatibleStateImageBehavior = False
        '
        'Drugs
        '
        Me.Drugs.Text = "Drugs"
        Me.Drugs.Width = 100
        '
        'Samp1
        '
        Me.Samp1.Text = "Sample"
        '
        'Samp2
        '
        Me.Samp2.Text = "Sample"
        '
        'Samp3
        '
        Me.Samp3.Text = "Sample"
        '
        'Samp4
        '
        Me.Samp4.Text = "Sample"
        '
        'Samp5
        '
        Me.Samp5.Text = "Sample"
        '
        'frmHist
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1000, 465)
        Me.Controls.Add(Me.lvSamp)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.txtFldr)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmHist"
        Me.Text = "History"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtFldr As TextBox
    Friend WithEvents btnRun As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents lvSamp As ListView
    Friend WithEvents Drugs As ColumnHeader
    Friend WithEvents Samp1 As ColumnHeader
    Friend WithEvents Samp2 As ColumnHeader
    Friend WithEvents Samp3 As ColumnHeader
    Friend WithEvents Samp4 As ColumnHeader
    Friend WithEvents Samp5 As ColumnHeader
End Class
