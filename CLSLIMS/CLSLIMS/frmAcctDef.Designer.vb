﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAcctDef
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.lblAcctName = New System.Windows.Forms.Label()
        Me.cbSales = New System.Windows.Forms.ComboBox()
        Me.cbProc = New System.Windows.Forms.ComboBox()
        Me.dtpRenewal = New System.Windows.Forms.DateTimePicker()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.cbProc2 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(131, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Account:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(118, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Salesman:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(102, 126)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 19)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Processor 1:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(81, 191)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 19)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Renewal Date:"
        '
        'txtAcct
        '
        Me.txtAcct.Location = New System.Drawing.Point(229, 48)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(100, 27)
        Me.txtAcct.TabIndex = 4
        '
        'lblAcctName
        '
        Me.lblAcctName.AutoSize = True
        Me.lblAcctName.Location = New System.Drawing.Point(329, 50)
        Me.lblAcctName.Name = "lblAcctName"
        Me.lblAcctName.Size = New System.Drawing.Size(0, 19)
        Me.lblAcctName.TabIndex = 5
        '
        'cbSales
        '
        Me.cbSales.FormattingEnabled = True
        Me.cbSales.Location = New System.Drawing.Point(229, 90)
        Me.cbSales.Name = "cbSales"
        Me.cbSales.Size = New System.Drawing.Size(309, 27)
        Me.cbSales.TabIndex = 6
        '
        'cbProc
        '
        Me.cbProc.FormattingEnabled = True
        Me.cbProc.Location = New System.Drawing.Point(229, 123)
        Me.cbProc.Name = "cbProc"
        Me.cbProc.Size = New System.Drawing.Size(309, 27)
        Me.cbProc.TabIndex = 7
        '
        'dtpRenewal
        '
        Me.dtpRenewal.Location = New System.Drawing.Point(229, 189)
        Me.dtpRenewal.Name = "dtpRenewal"
        Me.dtpRenewal.Size = New System.Drawing.Size(157, 27)
        Me.dtpRenewal.TabIndex = 8
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(180, 241)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(83, 39)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(368, 241)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(80, 39)
        Me.btnExit.TabIndex = 10
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'cbProc2
        '
        Me.cbProc2.FormattingEnabled = True
        Me.cbProc2.Location = New System.Drawing.Point(229, 156)
        Me.cbProc2.Name = "cbProc2"
        Me.cbProc2.Size = New System.Drawing.Size(309, 27)
        Me.cbProc2.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(102, 159)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 19)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Processor 2:"
        '
        'frmAcctDef
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(664, 302)
        Me.Controls.Add(Me.cbProc2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.dtpRenewal)
        Me.Controls.Add(Me.cbProc)
        Me.Controls.Add(Me.cbSales)
        Me.Controls.Add(Me.lblAcctName)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmAcctDef"
        Me.Text = "Partial Account Definition"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtAcct As TextBox
    Friend WithEvents lblAcctName As Label
    Friend WithEvents cbSales As ComboBox
    Friend WithEvents cbProc As ComboBox
    Friend WithEvents dtpRenewal As DateTimePicker
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents cbProc2 As ComboBox
    Friend WithEvents Label5 As Label
End Class
