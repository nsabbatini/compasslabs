﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestAbbrv
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtOC = New System.Windows.Forms.TextBox()
        Me.lblOCName = New System.Windows.Forms.Label()
        Me.lblAbbrs = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnCopy
        '
        Me.btnCopy.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopy.Location = New System.Drawing.Point(81, 142)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(75, 31)
        Me.btnCopy.TabIndex = 0
        Me.btnCopy.Text = "COPY"
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(171, 142)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 31)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 19)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Profile Number:"
        '
        'txtOC
        '
        Me.txtOC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOC.Location = New System.Drawing.Point(158, 26)
        Me.txtOC.Name = "txtOC"
        Me.txtOC.Size = New System.Drawing.Size(99, 27)
        Me.txtOC.TabIndex = 0
        '
        'lblOCName
        '
        Me.lblOCName.AutoSize = True
        Me.lblOCName.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOCName.Location = New System.Drawing.Point(278, 28)
        Me.lblOCName.Name = "lblOCName"
        Me.lblOCName.Size = New System.Drawing.Size(0, 19)
        Me.lblOCName.TabIndex = 4
        '
        'lblAbbrs
        '
        Me.lblAbbrs.AutoSize = True
        Me.lblAbbrs.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbbrs.Location = New System.Drawing.Point(154, 73)
        Me.lblAbbrs.Name = "lblAbbrs"
        Me.lblAbbrs.Size = New System.Drawing.Size(138, 19)
        Me.lblAbbrs.TabIndex = 5
        Me.lblAbbrs.Text = "Profile Number:"
        '
        'frmTestAbbrv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(926, 185)
        Me.Controls.Add(Me.lblAbbrs)
        Me.Controls.Add(Me.lblOCName)
        Me.Controls.Add(Me.txtOC)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCopy)
        Me.Name = "frmTestAbbrv"
        Me.Text = "Test Abbreviations"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCopy As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtOC As TextBox
    Friend WithEvents lblOCName As Label
    Friend WithEvents lblAbbrs As Label
End Class
