﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAffScan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtSpecId = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblLastFirst = New System.Windows.Forms.Label()
        Me.btnScan = New System.Windows.Forms.Button()
        Me.ScanImage = New Dynamsoft.DotNet.TWAIN.DynamicDotNetTwain()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtSpecId
        '
        Me.txtSpecId.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpecId.Location = New System.Drawing.Point(119, 10)
        Me.txtSpecId.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSpecId.Name = "txtSpecId"
        Me.txtSpecId.Size = New System.Drawing.Size(76, 24)
        Me.txtSpecId.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 13)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Specimen ID:"
        '
        'lblLastFirst
        '
        Me.lblLastFirst.AutoSize = True
        Me.lblLastFirst.Location = New System.Drawing.Point(227, 17)
        Me.lblLastFirst.Name = "lblLastFirst"
        Me.lblLastFirst.Size = New System.Drawing.Size(0, 13)
        Me.lblLastFirst.TabIndex = 10
        '
        'btnScan
        '
        Me.btnScan.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScan.Location = New System.Drawing.Point(41, 96)
        Me.btnScan.Margin = New System.Windows.Forms.Padding(2)
        Me.btnScan.Name = "btnScan"
        Me.btnScan.Size = New System.Drawing.Size(119, 49)
        Me.btnScan.TabIndex = 13
        Me.btnScan.Text = "Scan"
        Me.btnScan.UseVisualStyleBackColor = True
        '
        'ScanImage
        '
        Me.ScanImage.AnnotationFillColor = System.Drawing.Color.White
        Me.ScanImage.AnnotationPen = Nothing
        Me.ScanImage.AnnotationTextColor = System.Drawing.Color.Black
        Me.ScanImage.AnnotationTextFont = Nothing
        Me.ScanImage.IfShowCancelDialogWhenImageTransfer = False
        Me.ScanImage.IfThrowException = False
        Me.ScanImage.Location = New System.Drawing.Point(218, 57)
        Me.ScanImage.LogLevel = CType(0, Short)
        Me.ScanImage.Margin = New System.Windows.Forms.Padding(2)
        Me.ScanImage.Name = "ScanImage"
        Me.ScanImage.PDFMarginBottom = CType(0UI, UInteger)
        Me.ScanImage.PDFMarginLeft = CType(0UI, UInteger)
        Me.ScanImage.PDFMarginRight = CType(0UI, UInteger)
        Me.ScanImage.PDFMarginTop = CType(0UI, UInteger)
        Me.ScanImage.PDFXConformance = CType(0UI, UInteger)
        Me.ScanImage.ProductFamily = "Dynamic .NET TWAIN (.NET Framewor"
        Me.ScanImage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ScanImage.Size = New System.Drawing.Size(569, 615)
        Me.ScanImage.TabIndex = 14
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.BackColor = System.Drawing.Color.RoyalBlue
        Me.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblInfo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.ForeColor = System.Drawing.Color.White
        Me.lblInfo.Location = New System.Drawing.Point(41, 247)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(111, 21)
        Me.lblInfo.TabIndex = 18
        Me.lblInfo.Text = "                    "
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(69, 204)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(2)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(56, 26)
        Me.btnExit.TabIndex = 17
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(368, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(201, 33)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Affidavit Scan"
        '
        'frmAffScan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 687)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.ScanImage)
        Me.Controls.Add(Me.btnScan)
        Me.Controls.Add(Me.lblLastFirst)
        Me.Controls.Add(Me.txtSpecId)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmAffScan"
        Me.Text = "Affidavit Scan"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtSpecId As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblLastFirst As Label
    Friend WithEvents btnScan As Button
    Friend WithEvents ScanImage As Dynamsoft.DotNet.TWAIN.DynamicDotNetTwain
    Friend WithEvents lblInfo As Label
    Friend WithEvents btnExit As Button
    Friend WithEvents Label2 As Label
End Class
