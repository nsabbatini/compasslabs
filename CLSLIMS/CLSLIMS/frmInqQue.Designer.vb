﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInqQue
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstInsQue = New System.Windows.Forms.ListView()
        Me.colIns = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colSpec = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colRC = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblTime = New System.Windows.Forms.Label()
        Me.btnAgain = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.UpdGrid = New System.ComponentModel.BackgroundWorker()
        Me.lblCnt = New System.Windows.Forms.Label()
        Me.lblStats = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lstInsQue
        '
        Me.lstInsQue.BackColor = System.Drawing.Color.YellowGreen
        Me.lstInsQue.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colIns, Me.colSpec, Me.colRC})
        Me.lstInsQue.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInsQue.GridLines = True
        Me.lstInsQue.Location = New System.Drawing.Point(12, 50)
        Me.lstInsQue.Name = "lstInsQue"
        Me.lstInsQue.Size = New System.Drawing.Size(608, 178)
        Me.lstInsQue.TabIndex = 0
        Me.lstInsQue.UseCompatibleStateImageBehavior = False
        Me.lstInsQue.View = System.Windows.Forms.View.Details
        '
        'colIns
        '
        Me.colIns.Text = "Instrument"
        Me.colIns.Width = 200
        '
        'colSpec
        '
        Me.colSpec.Text = "Spec#"
        Me.colSpec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colSpec.Width = 200
        '
        'colRC
        '
        Me.colRC.Text = "Result#"
        Me.colRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colRC.Width = 200
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(12, 23)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(198, 18)
        Me.lblTime.TabIndex = 1
        Me.lblTime.Text = "01/01/2019 14:13:45"
        '
        'btnAgain
        '
        Me.btnAgain.Enabled = False
        Me.btnAgain.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgain.Location = New System.Drawing.Point(410, 234)
        Me.btnAgain.Name = "btnAgain"
        Me.btnAgain.Size = New System.Drawing.Size(75, 31)
        Me.btnAgain.TabIndex = 2
        Me.btnAgain.Text = "Again"
        Me.btnAgain.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(507, 234)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 31)
        Me.btnExit.TabIndex = 3
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'UpdGrid
        '
        '
        'lblCnt
        '
        Me.lblCnt.AutoSize = True
        Me.lblCnt.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCnt.Location = New System.Drawing.Point(595, 29)
        Me.lblCnt.Name = "lblCnt"
        Me.lblCnt.Size = New System.Drawing.Size(18, 18)
        Me.lblCnt.TabIndex = 4
        Me.lblCnt.Text = "9"
        '
        'lblStats
        '
        Me.lblStats.AutoSize = True
        Me.lblStats.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStats.Location = New System.Drawing.Point(12, 240)
        Me.lblStats.Name = "lblStats"
        Me.lblStats.Size = New System.Drawing.Size(80, 16)
        Me.lblStats.TabIndex = 5
        Me.lblStats.Text = "Elaspsed:"
        '
        'frmInqQue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 271)
        Me.Controls.Add(Me.lblStats)
        Me.Controls.Add(Me.lblCnt)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnAgain)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.lstInsQue)
        Me.Name = "frmInqQue"
        Me.Text = "frmInqQue"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstInsQue As ListView
    Friend WithEvents colIns As ColumnHeader
    Friend WithEvents colSpec As ColumnHeader
    Friend WithEvents colRC As ColumnHeader
    Friend WithEvents lblTime As Label
    Friend WithEvents btnAgain As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents UpdGrid As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblCnt As Label
    Friend WithEvents lblStats As Label
End Class
