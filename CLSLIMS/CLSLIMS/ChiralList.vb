﻿Imports CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class ChiralList

    Private Sub ChiralList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sel As String, Acct As String = "", NOP As Integer, x As String, AcctName As String = ""

        sel = "Select FLEXP From FL Where FL Like '%CHIRAL%' And FLACT=1"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                x = cRS(0)
                NOP = CLSLIMS.NumberofPieces(x, "=")
                For i = 2 To NOP
                    Acct = Replace(Mid(CLSLIMS.Piece(x, "=", i), 1, InStr(CLSLIMS.Piece(x, "=", i), ")") - 1), Chr(34), "")
                    If Acct <> "Positive" Then
                        AcctName = GetAcctName(Acct)
                        If AcctName <> "" Then
                            lbChirals.Items.Add(CLSLIMS.RJ(Acct, 6) & " - " & AcctName)
                        End If
                    End If
                Next i
            Loop
            cRS.Close()
            sel = "Select a.Acct From Acct A, acctoc b where a.acct=b.acct and a.aefdt=b.aefdt and aact=1 and oc='301027'"
            Cmd.CommandText = sel
            cRS = Cmd.ExecuteReader
            Do While cRS.Read
                lbChirals.Items.Add(CLSLIMS.RJ(cRS(0), 6) & " - " & GetAcctName(cRS(0)) & "*")
            Loop
            cRS.Close()
            Cmd.Dispose()
            Conn.Close()
        End Using

    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim printer As New PCPrint()
        'Set the font we want to use
        printer.PrinterFont = New Font("Courier New", 10)
        'Set Margins
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        printer.DefaultPageSettings.Margins = mar

        Dim txt As String = "Status Codes " & Format(Now, "MM/dd/yyyy hh:mm") & vbCrLf
        For i = 0 To lbChirals.Items.Count - 1
            txt = txt & vbCrLf & lbChirals.Items(i)
        Next i
        printer.TextToPrint = " " & txt
        'Issue print command
        printer.Print()

    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Private Function GetAcctName(ByVal acct As String) As String
        Dim xSel As String = "Select ANAME from ACCT where aact=1 and acct= '" & acct & "'"
        GetAcctName = ""
        If acct = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(0)
            End If
        End Using

    End Function
End Class