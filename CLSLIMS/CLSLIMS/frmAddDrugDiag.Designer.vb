﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddDrugDiag
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddDrugDiag))
        Me.btnRpt = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.Preview = New System.Windows.Forms.PrintPreviewDialog()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'btnRpt
        '
        Me.btnRpt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRpt.Location = New System.Drawing.Point(657, 224)
        Me.btnRpt.Name = "btnRpt"
        Me.btnRpt.Size = New System.Drawing.Size(91, 36)
        Me.btnRpt.TabIndex = 0
        Me.btnRpt.Text = "Report"
        Me.btnRpt.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(754, 224)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(91, 36)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'PrintDoc
        '
        '
        'Preview
        '
        Me.Preview.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.Preview.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.Preview.ClientSize = New System.Drawing.Size(400, 300)
        Me.Preview.Document = Me.PrintDoc
        Me.Preview.Enabled = True
        Me.Preview.Icon = CType(resources.GetObject("Preview.Icon"), System.Drawing.Icon)
        Me.Preview.Name = "Preview"
        Me.Preview.Visible = False
        '
        'lstData
        '
        Me.lstData.FormattingEnabled = True
        Me.lstData.Location = New System.Drawing.Point(23, 12)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(832, 199)
        Me.lstData.TabIndex = 2
        '
        'frmAddDrugDiag
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(867, 270)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRpt)
        Me.Name = "frmAddDrugDiag"
        Me.Text = "Addiction Drugs / Diagnosis Codes"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnRpt As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents PrintDoc As Printing.PrintDocument
    Friend WithEvents Preview As PrintPreviewDialog
    Friend WithEvents lstData As ListBox
End Class
