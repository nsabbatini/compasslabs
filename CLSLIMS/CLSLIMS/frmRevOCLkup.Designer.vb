﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRevOCLkup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbAlc = New System.Windows.Forms.CheckBox()
        Me.cbAmp = New System.Windows.Forms.CheckBox()
        Me.cbADSer = New System.Windows.Forms.CheckBox()
        Me.cbADTri = New System.Windows.Forms.CheckBox()
        Me.cbAD = New System.Windows.Forms.CheckBox()
        Me.cbAntiEp = New System.Windows.Forms.CheckBox()
        Me.cbBarb = New System.Windows.Forms.CheckBox()
        Me.cbBenz = New System.Windows.Forms.CheckBox()
        Me.cbBup = New System.Windows.Forms.CheckBox()
        Me.cbTHC = New System.Windows.Forms.CheckBox()
        Me.cbCOC = New System.Windows.Forms.CheckBox()
        Me.cbFent = New System.Windows.Forms.CheckBox()
        Me.cbGab = New System.Windows.Forms.CheckBox()
        Me.cbHero = New System.Windows.Forms.CheckBox()
        Me.cbMtd = New System.Windows.Forms.CheckBox()
        Me.cbMethAmp = New System.Windows.Forms.CheckBox()
        Me.cbMethy = New System.Windows.Forms.CheckBox()
        Me.cbNic = New System.Windows.Forms.CheckBox()
        Me.cbOpi = New System.Windows.Forms.CheckBox()
        Me.cbOpiAnal = New System.Windows.Forms.CheckBox()
        Me.cbOxy = New System.Windows.Forms.CheckBox()
        Me.cbPhen = New System.Windows.Forms.CheckBox()
        Me.cbPreGab = New System.Windows.Forms.CheckBox()
        Me.cbPro = New System.Windows.Forms.CheckBox()
        Me.cbSedHyp = New System.Windows.Forms.CheckBox()
        Me.cbSkel = New System.Windows.Forms.CheckBox()
        Me.cbTap = New System.Windows.Forms.CheckBox()
        Me.cbTram = New System.Windows.Forms.CheckBox()
        Me.btnLkUp = New System.Windows.Forms.Button()
        Me.rbScreening = New System.Windows.Forms.RadioButton()
        Me.rbConf = New System.Windows.Forms.RadioButton()
        Me.lbDisplayResults = New System.Windows.Forms.ListBox()
        Me.cbAntiPys = New System.Windows.Forms.CheckBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbAlc
        '
        Me.cbAlc.AutoSize = True
        Me.cbAlc.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAlc.Location = New System.Drawing.Point(19, 23)
        Me.cbAlc.Name = "cbAlc"
        Me.cbAlc.Size = New System.Drawing.Size(150, 20)
        Me.cbAlc.TabIndex = 0
        Me.cbAlc.Text = "Alcohol Biomarkers"
        Me.cbAlc.UseVisualStyleBackColor = True
        '
        'cbAmp
        '
        Me.cbAmp.AutoSize = True
        Me.cbAmp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAmp.Location = New System.Drawing.Point(19, 49)
        Me.cbAmp.Name = "cbAmp"
        Me.cbAmp.Size = New System.Drawing.Size(123, 20)
        Me.cbAmp.TabIndex = 1
        Me.cbAmp.Text = "Amphetamines"
        Me.cbAmp.UseVisualStyleBackColor = True
        '
        'cbADSer
        '
        Me.cbADSer.AutoSize = True
        Me.cbADSer.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbADSer.Location = New System.Drawing.Point(19, 75)
        Me.cbADSer.Name = "cbADSer"
        Me.cbADSer.Size = New System.Drawing.Size(227, 20)
        Me.cbADSer.TabIndex = 2
        Me.cbADSer.Text = "Antidepressants, Serotonergic"
        Me.cbADSer.UseVisualStyleBackColor = True
        '
        'cbADTri
        '
        Me.cbADTri.AutoSize = True
        Me.cbADTri.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbADTri.Location = New System.Drawing.Point(19, 101)
        Me.cbADTri.Name = "cbADTri"
        Me.cbADTri.Size = New System.Drawing.Size(194, 20)
        Me.cbADTri.TabIndex = 3
        Me.cbADTri.Text = "Antidepressants, Tricyclic"
        Me.cbADTri.UseVisualStyleBackColor = True
        '
        'cbAD
        '
        Me.cbAD.AutoSize = True
        Me.cbAD.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAD.Location = New System.Drawing.Point(19, 127)
        Me.cbAD.Name = "cbAD"
        Me.cbAD.Size = New System.Drawing.Size(135, 20)
        Me.cbAD.TabIndex = 4
        Me.cbAD.Text = "Antidepressants"
        Me.cbAD.UseVisualStyleBackColor = True
        '
        'cbAntiEp
        '
        Me.cbAntiEp.AutoSize = True
        Me.cbAntiEp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntiEp.Location = New System.Drawing.Point(19, 153)
        Me.cbAntiEp.Name = "cbAntiEp"
        Me.cbAntiEp.Size = New System.Drawing.Size(115, 20)
        Me.cbAntiEp.TabIndex = 5
        Me.cbAntiEp.Text = "Antiepileptics"
        Me.cbAntiEp.UseVisualStyleBackColor = True
        '
        'cbBarb
        '
        Me.cbBarb.AutoSize = True
        Me.cbBarb.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBarb.Location = New System.Drawing.Point(19, 205)
        Me.cbBarb.Name = "cbBarb"
        Me.cbBarb.Size = New System.Drawing.Size(109, 20)
        Me.cbBarb.TabIndex = 6
        Me.cbBarb.Text = "Barbiturates"
        Me.cbBarb.UseVisualStyleBackColor = True
        '
        'cbBenz
        '
        Me.cbBenz.AutoSize = True
        Me.cbBenz.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBenz.Location = New System.Drawing.Point(263, 23)
        Me.cbBenz.Name = "cbBenz"
        Me.cbBenz.Size = New System.Drawing.Size(134, 20)
        Me.cbBenz.TabIndex = 7
        Me.cbBenz.Text = "Benzodiazepines"
        Me.cbBenz.UseVisualStyleBackColor = True
        '
        'cbBup
        '
        Me.cbBup.AutoSize = True
        Me.cbBup.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBup.Location = New System.Drawing.Point(263, 49)
        Me.cbBup.Name = "cbBup"
        Me.cbBup.Size = New System.Drawing.Size(122, 20)
        Me.cbBup.TabIndex = 8
        Me.cbBup.Text = "Buprenorphine"
        Me.cbBup.UseVisualStyleBackColor = True
        '
        'cbTHC
        '
        Me.cbTHC.AutoSize = True
        Me.cbTHC.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTHC.Location = New System.Drawing.Point(263, 75)
        Me.cbTHC.Name = "cbTHC"
        Me.cbTHC.Size = New System.Drawing.Size(167, 20)
        Me.cbTHC.TabIndex = 9
        Me.cbTHC.Text = "Cannabinoids, Natural"
        Me.cbTHC.UseVisualStyleBackColor = True
        '
        'cbCOC
        '
        Me.cbCOC.AutoSize = True
        Me.cbCOC.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCOC.Location = New System.Drawing.Point(263, 101)
        Me.cbCOC.Name = "cbCOC"
        Me.cbCOC.Size = New System.Drawing.Size(77, 20)
        Me.cbCOC.TabIndex = 10
        Me.cbCOC.Text = "Cocaine"
        Me.cbCOC.UseVisualStyleBackColor = True
        '
        'cbFent
        '
        Me.cbFent.AutoSize = True
        Me.cbFent.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbFent.Location = New System.Drawing.Point(263, 127)
        Me.cbFent.Name = "cbFent"
        Me.cbFent.Size = New System.Drawing.Size(89, 20)
        Me.cbFent.TabIndex = 11
        Me.cbFent.Text = "Fentanyls"
        Me.cbFent.UseVisualStyleBackColor = True
        '
        'cbGab
        '
        Me.cbGab.AutoSize = True
        Me.cbGab.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbGab.Location = New System.Drawing.Point(263, 153)
        Me.cbGab.Name = "cbGab"
        Me.cbGab.Size = New System.Drawing.Size(173, 20)
        Me.cbGab.TabIndex = 12
        Me.cbGab.Text = "Gabapentin, Non-Blood"
        Me.cbGab.UseVisualStyleBackColor = True
        '
        'cbHero
        '
        Me.cbHero.AutoSize = True
        Me.cbHero.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbHero.Location = New System.Drawing.Point(263, 179)
        Me.cbHero.Name = "cbHero"
        Me.cbHero.Size = New System.Drawing.Size(142, 20)
        Me.cbHero.TabIndex = 13
        Me.cbHero.Text = "Heroin Metabolite"
        Me.cbHero.UseVisualStyleBackColor = True
        '
        'cbMtd
        '
        Me.cbMtd.AutoSize = True
        Me.cbMtd.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMtd.Location = New System.Drawing.Point(263, 205)
        Me.cbMtd.Name = "cbMtd"
        Me.cbMtd.Size = New System.Drawing.Size(100, 20)
        Me.cbMtd.TabIndex = 14
        Me.cbMtd.Text = "Methadone"
        Me.cbMtd.UseVisualStyleBackColor = True
        '
        'cbMethAmp
        '
        Me.cbMethAmp.AutoSize = True
        Me.cbMethAmp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMethAmp.Location = New System.Drawing.Point(263, 231)
        Me.cbMethAmp.Name = "cbMethAmp"
        Me.cbMethAmp.Size = New System.Drawing.Size(231, 20)
        Me.cbMethAmp.TabIndex = 15
        Me.cbMethAmp.Text = "Methylenedioxy-Amphetamines"
        Me.cbMethAmp.UseVisualStyleBackColor = True
        '
        'cbMethy
        '
        Me.cbMethy.AutoSize = True
        Me.cbMethy.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMethy.Location = New System.Drawing.Point(263, 257)
        Me.cbMethy.Name = "cbMethy"
        Me.cbMethy.Size = New System.Drawing.Size(133, 20)
        Me.cbMethy.TabIndex = 16
        Me.cbMethy.Text = "Methyphenidate"
        Me.cbMethy.UseVisualStyleBackColor = True
        '
        'cbNic
        '
        Me.cbNic.AutoSize = True
        Me.cbNic.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbNic.Location = New System.Drawing.Point(262, 283)
        Me.cbNic.Name = "cbNic"
        Me.cbNic.Size = New System.Drawing.Size(78, 20)
        Me.cbNic.TabIndex = 17
        Me.cbNic.Text = "Nicotine"
        Me.cbNic.UseVisualStyleBackColor = True
        '
        'cbOpi
        '
        Me.cbOpi.AutoSize = True
        Me.cbOpi.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOpi.Location = New System.Drawing.Point(515, 23)
        Me.cbOpi.Name = "cbOpi"
        Me.cbOpi.Size = New System.Drawing.Size(76, 20)
        Me.cbOpi.TabIndex = 18
        Me.cbOpi.Text = "Opiates"
        Me.cbOpi.UseVisualStyleBackColor = True
        '
        'cbOpiAnal
        '
        Me.cbOpiAnal.AutoSize = True
        Me.cbOpiAnal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOpiAnal.Location = New System.Drawing.Point(515, 49)
        Me.cbOpiAnal.Name = "cbOpiAnal"
        Me.cbOpiAnal.Size = New System.Drawing.Size(219, 20)
        Me.cbOpiAnal.TabIndex = 19
        Me.cbOpiAnal.Text = "Opioids and Opiate Analogues"
        Me.cbOpiAnal.UseVisualStyleBackColor = True
        '
        'cbOxy
        '
        Me.cbOxy.AutoSize = True
        Me.cbOxy.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOxy.Location = New System.Drawing.Point(515, 75)
        Me.cbOxy.Name = "cbOxy"
        Me.cbOxy.Size = New System.Drawing.Size(98, 20)
        Me.cbOxy.TabIndex = 20
        Me.cbOxy.Text = "Oxycodone"
        Me.cbOxy.UseVisualStyleBackColor = True
        '
        'cbPhen
        '
        Me.cbPhen.AutoSize = True
        Me.cbPhen.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPhen.Location = New System.Drawing.Point(515, 101)
        Me.cbPhen.Name = "cbPhen"
        Me.cbPhen.Size = New System.Drawing.Size(114, 20)
        Me.cbPhen.TabIndex = 21
        Me.cbPhen.Text = "Phencyclidine"
        Me.cbPhen.UseVisualStyleBackColor = True
        '
        'cbPreGab
        '
        Me.cbPreGab.AutoSize = True
        Me.cbPreGab.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPreGab.Location = New System.Drawing.Point(515, 127)
        Me.cbPreGab.Name = "cbPreGab"
        Me.cbPreGab.Size = New System.Drawing.Size(95, 20)
        Me.cbPreGab.TabIndex = 22
        Me.cbPreGab.Text = "Pregabalin"
        Me.cbPreGab.UseVisualStyleBackColor = True
        '
        'cbPro
        '
        Me.cbPro.AutoSize = True
        Me.cbPro.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPro.Location = New System.Drawing.Point(515, 153)
        Me.cbPro.Name = "cbPro"
        Me.cbPro.Size = New System.Drawing.Size(120, 20)
        Me.cbPro.TabIndex = 23
        Me.cbPro.Text = "Propoxyphene"
        Me.cbPro.UseVisualStyleBackColor = True
        '
        'cbSedHyp
        '
        Me.cbSedHyp.AutoSize = True
        Me.cbSedHyp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSedHyp.Location = New System.Drawing.Point(515, 179)
        Me.cbSedHyp.Name = "cbSedHyp"
        Me.cbSedHyp.Size = New System.Drawing.Size(152, 20)
        Me.cbSedHyp.TabIndex = 24
        Me.cbSedHyp.Text = "Sedative Hypnotics"
        Me.cbSedHyp.UseVisualStyleBackColor = True
        '
        'cbSkel
        '
        Me.cbSkel.AutoSize = True
        Me.cbSkel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSkel.Location = New System.Drawing.Point(515, 205)
        Me.cbSkel.Name = "cbSkel"
        Me.cbSkel.Size = New System.Drawing.Size(194, 20)
        Me.cbSkel.TabIndex = 25
        Me.cbSkel.Text = "Skeletal Muscle Relaxants"
        Me.cbSkel.UseVisualStyleBackColor = True
        '
        'cbTap
        '
        Me.cbTap.AutoSize = True
        Me.cbTap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTap.Location = New System.Drawing.Point(515, 231)
        Me.cbTap.Name = "cbTap"
        Me.cbTap.Size = New System.Drawing.Size(99, 20)
        Me.cbTap.TabIndex = 26
        Me.cbTap.Text = "Tapentadol"
        Me.cbTap.UseVisualStyleBackColor = True
        '
        'cbTram
        '
        Me.cbTram.AutoSize = True
        Me.cbTram.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTram.Location = New System.Drawing.Point(515, 257)
        Me.cbTram.Name = "cbTram"
        Me.cbTram.Size = New System.Drawing.Size(86, 20)
        Me.cbTram.TabIndex = 27
        Me.cbTram.Text = "Tramadol"
        Me.cbTram.UseVisualStyleBackColor = True
        '
        'btnLkUp
        '
        Me.btnLkUp.BackColor = System.Drawing.Color.CornflowerBlue
        Me.btnLkUp.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLkUp.Location = New System.Drawing.Point(19, 257)
        Me.btnLkUp.Name = "btnLkUp"
        Me.btnLkUp.Size = New System.Drawing.Size(117, 75)
        Me.btnLkUp.TabIndex = 28
        Me.btnLkUp.Text = "Look Up Ordering Code"
        Me.btnLkUp.UseVisualStyleBackColor = False
        '
        'rbScreening
        '
        Me.rbScreening.AutoSize = True
        Me.rbScreening.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbScreening.Location = New System.Drawing.Point(515, 309)
        Me.rbScreening.Name = "rbScreening"
        Me.rbScreening.Size = New System.Drawing.Size(107, 23)
        Me.rbScreening.TabIndex = 29
        Me.rbScreening.TabStop = True
        Me.rbScreening.Text = "Screening"
        Me.rbScreening.UseVisualStyleBackColor = True
        '
        'rbConf
        '
        Me.rbConf.AutoSize = True
        Me.rbConf.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbConf.Location = New System.Drawing.Point(515, 329)
        Me.rbConf.Name = "rbConf"
        Me.rbConf.Size = New System.Drawing.Size(141, 23)
        Me.rbConf.TabIndex = 30
        Me.rbConf.TabStop = True
        Me.rbConf.Text = "Confirmations"
        Me.rbConf.UseVisualStyleBackColor = True
        '
        'lbDisplayResults
        '
        Me.lbDisplayResults.BackColor = System.Drawing.Color.LightGray
        Me.lbDisplayResults.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbDisplayResults.ForeColor = System.Drawing.Color.White
        Me.lbDisplayResults.FormattingEnabled = True
        Me.lbDisplayResults.ItemHeight = 19
        Me.lbDisplayResults.Location = New System.Drawing.Point(12, 358)
        Me.lbDisplayResults.Name = "lbDisplayResults"
        Me.lbDisplayResults.Size = New System.Drawing.Size(721, 99)
        Me.lbDisplayResults.TabIndex = 31
        '
        'cbAntiPys
        '
        Me.cbAntiPys.AutoSize = True
        Me.cbAntiPys.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntiPys.Location = New System.Drawing.Point(19, 179)
        Me.cbAntiPys.Name = "cbAntiPys"
        Me.cbAntiPys.Size = New System.Drawing.Size(123, 20)
        Me.cbAntiPys.TabIndex = 32
        Me.cbAntiPys.Text = "Antipsychotics"
        Me.cbAntiPys.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(631, 479)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(78, 28)
        Me.btnExit.TabIndex = 33
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(12, 479)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(157, 28)
        Me.btnClear.TabIndex = 34
        Me.btnClear.Text = "Clear Selections"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'frmRevOCLkup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(745, 519)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.cbAntiPys)
        Me.Controls.Add(Me.lbDisplayResults)
        Me.Controls.Add(Me.rbConf)
        Me.Controls.Add(Me.rbScreening)
        Me.Controls.Add(Me.btnLkUp)
        Me.Controls.Add(Me.cbTram)
        Me.Controls.Add(Me.cbTap)
        Me.Controls.Add(Me.cbSkel)
        Me.Controls.Add(Me.cbSedHyp)
        Me.Controls.Add(Me.cbPro)
        Me.Controls.Add(Me.cbPreGab)
        Me.Controls.Add(Me.cbPhen)
        Me.Controls.Add(Me.cbOxy)
        Me.Controls.Add(Me.cbOpiAnal)
        Me.Controls.Add(Me.cbOpi)
        Me.Controls.Add(Me.cbNic)
        Me.Controls.Add(Me.cbMethy)
        Me.Controls.Add(Me.cbMethAmp)
        Me.Controls.Add(Me.cbMtd)
        Me.Controls.Add(Me.cbHero)
        Me.Controls.Add(Me.cbGab)
        Me.Controls.Add(Me.cbFent)
        Me.Controls.Add(Me.cbCOC)
        Me.Controls.Add(Me.cbTHC)
        Me.Controls.Add(Me.cbBup)
        Me.Controls.Add(Me.cbBenz)
        Me.Controls.Add(Me.cbBarb)
        Me.Controls.Add(Me.cbAntiEp)
        Me.Controls.Add(Me.cbAD)
        Me.Controls.Add(Me.cbADTri)
        Me.Controls.Add(Me.cbADSer)
        Me.Controls.Add(Me.cbAmp)
        Me.Controls.Add(Me.cbAlc)
        Me.Name = "frmRevOCLkup"
        Me.Text = "Reverse Ordering Code Lookup"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbAlc As CheckBox
    Friend WithEvents cbAmp As CheckBox
    Friend WithEvents cbADSer As CheckBox
    Friend WithEvents cbADTri As CheckBox
    Friend WithEvents cbAD As CheckBox
    Friend WithEvents cbAntiEp As CheckBox
    Friend WithEvents cbBarb As CheckBox
    Friend WithEvents cbBenz As CheckBox
    Friend WithEvents cbBup As CheckBox
    Friend WithEvents cbTHC As CheckBox
    Friend WithEvents cbCOC As CheckBox
    Friend WithEvents cbFent As CheckBox
    Friend WithEvents cbGab As CheckBox
    Friend WithEvents cbHero As CheckBox
    Friend WithEvents cbMtd As CheckBox
    Friend WithEvents cbMethAmp As CheckBox
    Friend WithEvents cbMethy As CheckBox
    Friend WithEvents cbNic As CheckBox
    Friend WithEvents cbOpi As CheckBox
    Friend WithEvents cbOpiAnal As CheckBox
    Friend WithEvents cbOxy As CheckBox
    Friend WithEvents cbPhen As CheckBox
    Friend WithEvents cbPreGab As CheckBox
    Friend WithEvents cbPro As CheckBox
    Friend WithEvents cbSedHyp As CheckBox
    Friend WithEvents cbSkel As CheckBox
    Friend WithEvents cbTap As CheckBox
    Friend WithEvents cbTram As CheckBox
    Friend WithEvents btnLkUp As Button
    Friend WithEvents rbScreening As RadioButton
    Friend WithEvents rbConf As RadioButton
    Friend WithEvents lbDisplayResults As ListBox
    Friend WithEvents cbAntiPys As CheckBox
    Friend WithEvents btnExit As Button
    Friend WithEvents btnClear As Button
End Class
