﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SendOuts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SendOuts))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreview = New System.Windows.Forms.PrintPreviewDialog()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbSO = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTray = New System.Windows.Forms.TextBox()
        Me.rbManifest = New System.Windows.Forms.RadioButton()
        Me.rbReq = New System.Windows.Forms.RadioButton()
        Me.PrintReq = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewReq = New System.Windows.Forms.PrintPreviewDialog()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(71, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 19)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Date:"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker1.Location = New System.Drawing.Point(131, 28)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(140, 27)
        Me.DateTimePicker1.TabIndex = 4
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(218, 176)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(52, 32)
        Me.btnExit.TabIndex = 12
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(141, 176)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(64, 32)
        Me.btnPrint.TabIndex = 13
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Location = New System.Drawing.Point(60, 176)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 32)
        Me.btnPreview.TabIndex = 14
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'PrintDoc
        '
        '
        'PrintPreview
        '
        Me.PrintPreview.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreview.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreview.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreview.Document = Me.PrintDoc
        Me.PrintPreview.Enabled = True
        Me.PrintPreview.Icon = CType(resources.GetObject("PrintPreview.Icon"), System.Drawing.Icon)
        Me.PrintPreview.Name = "PrintPreview"
        Me.PrintPreview.Visible = False
        '
        'lstData
        '
        Me.lstData.Font = New System.Drawing.Font("Courier New", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 17
        Me.lstData.Location = New System.Drawing.Point(11, 120)
        Me.lstData.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(40, 4)
        Me.lstData.TabIndex = 15
        Me.lstData.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(118, 19)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "SendOut Lab:"
        '
        'cmbSO
        '
        Me.cmbSO.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSO.FormattingEnabled = True
        Me.cmbSO.Items.AddRange(New Object() {"Cleveland Heart Lab", "Lab Corp", "AEL", "Delta Medical Center"})
        Me.cmbSO.Location = New System.Drawing.Point(131, 59)
        Me.cmbSO.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.cmbSO.Name = "cmbSO"
        Me.cmbSO.Size = New System.Drawing.Size(140, 27)
        Me.cmbSO.TabIndex = 17
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(73, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 19)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Tray:"
        '
        'txtTray
        '
        Me.txtTray.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTray.Location = New System.Drawing.Point(131, 91)
        Me.txtTray.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtTray.Name = "txtTray"
        Me.txtTray.Size = New System.Drawing.Size(140, 27)
        Me.txtTray.TabIndex = 19
        '
        'rbManifest
        '
        Me.rbManifest.AutoSize = True
        Me.rbManifest.Checked = True
        Me.rbManifest.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbManifest.Location = New System.Drawing.Point(74, 137)
        Me.rbManifest.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.rbManifest.Name = "rbManifest"
        Me.rbManifest.Size = New System.Drawing.Size(84, 21)
        Me.rbManifest.TabIndex = 20
        Me.rbManifest.TabStop = True
        Me.rbManifest.Text = "Manifest"
        Me.rbManifest.UseVisualStyleBackColor = True
        '
        'rbReq
        '
        Me.rbReq.AutoSize = True
        Me.rbReq.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbReq.Location = New System.Drawing.Point(174, 137)
        Me.rbReq.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.rbReq.Name = "rbReq"
        Me.rbReq.Size = New System.Drawing.Size(102, 21)
        Me.rbReq.TabIndex = 21
        Me.rbReq.TabStop = True
        Me.rbReq.Text = "Requisition"
        Me.rbReq.UseVisualStyleBackColor = True
        '
        'PrintReq
        '
        '
        'PrintPreviewReq
        '
        Me.PrintPreviewReq.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewReq.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewReq.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewReq.Document = Me.PrintReq
        Me.PrintPreviewReq.Enabled = True
        Me.PrintPreviewReq.Icon = CType(resources.GetObject("PrintPreviewReq.Icon"), System.Drawing.Icon)
        Me.PrintPreviewReq.Name = "PrintPreviewReq"
        Me.PrintPreviewReq.Visible = False
        '
        'SendOuts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(298, 231)
        Me.Controls.Add(Me.rbReq)
        Me.Controls.Add(Me.rbManifest)
        Me.Controls.Add(Me.txtTray)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbSO)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "SendOuts"
        Me.Text = "SendOuts"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents PrintDoc As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPreview As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents lstData As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbSO As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTray As System.Windows.Forms.TextBox
    Friend WithEvents rbManifest As System.Windows.Forms.RadioButton
    Friend WithEvents rbReq As System.Windows.Forms.RadioButton
    Friend WithEvents PrintReq As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPreviewReq As System.Windows.Forms.PrintPreviewDialog
End Class
