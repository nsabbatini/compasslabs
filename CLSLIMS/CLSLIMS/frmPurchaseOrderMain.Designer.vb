﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPurchaseOrderMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPurchaseOrderMain))
        Dim EditorButtonImageOptions1 As DevExpress.XtraEditors.Controls.EditorButtonImageOptions = New DevExpress.XtraEditors.Controls.EditorButtonImageOptions()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject4 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.btnRecalculate = New DevExpress.XtraEditors.SimpleButton()
        Me.lblTotalPo = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.lblSubTotal = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtTax = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.txtTerms = New DevExpress.XtraEditors.TextEdit()
        Me.txtCost = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdOrders = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemButtonEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtQty = New DevExpress.XtraEditors.TextEdit()
        Me.txtPO = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.grdVendors = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.grdGetItems = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.chkNeedsApproval = New DevExpress.XtraEditors.CheckEdit()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.chkShipping = New DevExpress.XtraEditors.CheckEdit()
        Me.grdLookup = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.lblClient = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPaymentTerms = New DevExpress.XtraEditors.TextEdit()
        Me.txtQuoteNumber = New DevExpress.XtraEditors.TextEdit()
        Me.txtSalesPerson = New DevExpress.XtraEditors.TextEdit()
        Me.DxValidationProvider1 = New DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(Me.components)
        Me.HyperLinkEdit1 = New DevExpress.XtraEditors.HyperLinkEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.txtVendor = New DevExpress.XtraEditors.TextEdit()
        Me.txtItem = New DevExpress.XtraEditors.TextEdit()
        Me.lookupDepartments = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTerms.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdVendors.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdGetItems.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.chkNeedsApproval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkShipping.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdLookup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPaymentTerms.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQuoteNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSalesPerson.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DxValidationProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HyperLinkEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVendor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItem.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lookupDepartments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.txtNotes)
        Me.GroupControl1.Controls.Add(Me.btnRecalculate)
        Me.GroupControl1.Controls.Add(Me.lblTotalPo)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.lblSubTotal)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.txtTax)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 459)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(764, 216)
        Me.GroupControl1.TabIndex = 36
        Me.GroupControl1.Text = "Order Total"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(6, 32)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(109, 13)
        Me.LabelControl10.TabIndex = 27
        Me.LabelControl10.Text = "Notes and Instructions"
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(5, 52)
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.Size = New System.Drawing.Size(501, 159)
        Me.txtNotes.TabIndex = 26
        '
        'btnRecalculate
        '
        Me.btnRecalculate.ImageOptions.SvgImage = CType(resources.GetObject("btnRecalculate.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnRecalculate.Location = New System.Drawing.Point(575, 168)
        Me.btnRecalculate.Name = "btnRecalculate"
        Me.btnRecalculate.Size = New System.Drawing.Size(172, 43)
        Me.btnRecalculate.TabIndex = 25
        Me.btnRecalculate.Text = "Re - Calculate PO"
        '
        'lblTotalPo
        '
        Me.lblTotalPo.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblTotalPo.Appearance.Options.UseFont = True
        Me.lblTotalPo.Location = New System.Drawing.Point(623, 131)
        Me.lblTotalPo.Name = "lblTotalPo"
        Me.lblTotalPo.Size = New System.Drawing.Size(41, 19)
        Me.lblTotalPo.TabIndex = 24
        Me.lblTotalPo.Text = "$0.00"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(575, 131)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(42, 19)
        Me.LabelControl8.TabIndex = 23
        Me.LabelControl8.Text = "Total:"
        '
        'lblSubTotal
        '
        Me.lblSubTotal.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblSubTotal.Appearance.Options.UseFont = True
        Me.lblSubTotal.Location = New System.Drawing.Point(623, 38)
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Size = New System.Drawing.Size(41, 19)
        Me.lblSubTotal.TabIndex = 22
        Me.lblSubTotal.Text = "$0.00"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(543, 38)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(74, 19)
        Me.LabelControl7.TabIndex = 21
        Me.LabelControl7.Text = "Sub Total:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(528, 84)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(89, 19)
        Me.LabelControl6.TabIndex = 20
        Me.LabelControl6.Text = "Tax Rate %:"
        '
        'txtTax
        '
        Me.txtTax.Location = New System.Drawing.Point(623, 77)
        Me.txtTax.Name = "txtTax"
        Me.txtTax.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtTax.Properties.Appearance.Options.UseFont = True
        Me.txtTax.Size = New System.Drawing.Size(124, 26)
        Me.txtTax.TabIndex = 10
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(394, 5)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(171, 19)
        Me.LabelControl5.TabIndex = 33
        Me.LabelControl5.Text = "Purchase Order Number"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(614, 5)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(71, 19)
        Me.LabelControl9.TabIndex = 38
        Me.LabelControl9.Text = "PO Terms"
        '
        'txtTerms
        '
        Me.txtTerms.EditValue = "30 Days"
        Me.txtTerms.Location = New System.Drawing.Point(614, 30)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtTerms.Properties.Appearance.Options.UseFont = True
        Me.txtTerms.Size = New System.Drawing.Size(127, 26)
        Me.txtTerms.TabIndex = 37
        Me.txtTerms.TabStop = False
        '
        'txtCost
        '
        Me.txtCost.Location = New System.Drawing.Point(529, 234)
        Me.txtCost.Name = "txtCost"
        Me.txtCost.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtCost.Properties.Appearance.Options.UseFont = True
        Me.txtCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtCost.Properties.Mask.EditMask = "c"
        Me.txtCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCost.Size = New System.Drawing.Size(100, 26)
        Me.txtCost.TabIndex = 8
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Vendor"
        Me.GridColumn1.FieldName = "VendorName"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Id"
        Me.GridColumn2.FieldName = "Id"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'grdOrders
        '
        Me.grdOrders.Location = New System.Drawing.Point(12, 266)
        Me.grdOrders.MainView = Me.GridView1
        Me.grdOrders.Name = "grdOrders"
        Me.grdOrders.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemButtonEdit1})
        Me.grdOrders.Size = New System.Drawing.Size(764, 187)
        Me.grdOrders.TabIndex = 32
        Me.grdOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn13, Me.GridColumn5, Me.GridColumn6, Me.GridColumn9, Me.GridColumn7, Me.GridColumn8})
        Me.GridView1.GridControl = Me.grdOrders
        Me.GridView1.Name = "GridView1"
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Delete"
        Me.GridColumn13.ColumnEdit = Me.RepositoryItemButtonEdit1
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 0
        Me.GridColumn13.Width = 46
        '
        'RepositoryItemButtonEdit1
        '
        Me.RepositoryItemButtonEdit1.AutoHeight = False
        EditorButtonImageOptions1.Image = CType(resources.GetObject("EditorButtonImageOptions1.Image"), System.Drawing.Image)
        Me.RepositoryItemButtonEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, EditorButtonImageOptions1, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, SerializableAppearanceObject2, SerializableAppearanceObject3, SerializableAppearanceObject4, "", Nothing, Nothing, DevExpress.Utils.ToolTipAnchor.[Default])})
        Me.RepositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1"
        Me.RepositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Qty"
        Me.GridColumn5.FieldName = "Qty"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 50
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Items"
        Me.GridColumn6.FieldName = "ItemDesc"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 514
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Unit Cost"
        Me.GridColumn9.FieldName = "UnitCost"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 3
        Me.GridColumn9.Width = 61
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Price"
        Me.GridColumn7.DisplayFormat.FormatString = "0:c"
        Me.GridColumn7.FieldName = "Price"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 4
        Me.GridColumn7.Width = 68
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "GridColumn8"
        Me.GridColumn8.FieldName = "ItemId"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.ImageOptions.SvgImage = CType(resources.GetObject("btnAdd.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnAdd.Location = New System.Drawing.Point(635, 225)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(106, 35)
        Me.btnAdd.TabIndex = 9
        Me.btnAdd.Text = "Add"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(529, 209)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(34, 19)
        Me.LabelControl4.TabIndex = 30
        Me.LabelControl4.Text = "Price"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 209)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(33, 19)
        Me.LabelControl3.TabIndex = 29
        Me.LabelControl3.Text = "Item"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(456, 209)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(25, 19)
        Me.LabelControl2.TabIndex = 28
        Me.LabelControl2.Text = "Qty"
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(456, 234)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtQty.Properties.Appearance.Options.UseFont = True
        Me.txtQty.Properties.Appearance.Options.UseTextOptions = True
        Me.txtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtQty.Size = New System.Drawing.Size(63, 26)
        Me.txtQty.TabIndex = 7
        '
        'txtPO
        '
        Me.txtPO.Location = New System.Drawing.Point(394, 30)
        Me.txtPO.Name = "txtPO"
        Me.txtPO.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtPO.Properties.Appearance.Options.UseFont = True
        Me.txtPO.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPO.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtPO.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPO.Properties.ReadOnly = True
        Me.txtPO.Size = New System.Drawing.Size(171, 26)
        Me.txtPO.TabIndex = 25
        Me.txtPO.TabStop = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(12, 5)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(51, 19)
        Me.LabelControl1.TabIndex = 24
        Me.LabelControl1.Text = "Vendor"
        '
        'grdVendors
        '
        Me.grdVendors.Location = New System.Drawing.Point(12, 30)
        Me.grdVendors.Name = "grdVendors"
        Me.grdVendors.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.grdVendors.Properties.Appearance.Options.UseFont = True
        Me.grdVendors.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdVendors.Properties.PopupView = Me.GridLookUpEdit1View
        Me.grdVendors.Size = New System.Drawing.Size(342, 26)
        Me.grdVendors.TabIndex = 1
        '
        'grdGetItems
        '
        Me.grdGetItems.Location = New System.Drawing.Point(12, 234)
        Me.grdGetItems.Name = "grdGetItems"
        Me.grdGetItems.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.grdGetItems.Properties.Appearance.Options.UseFont = True
        Me.grdGetItems.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdGetItems.Properties.PopupView = Me.GridView2
        Me.grdGetItems.Size = New System.Drawing.Size(438, 26)
        Me.grdGetItems.TabIndex = 6
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.GridColumn4, Me.GridColumn12})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsFilter.ShowAllTableValuesInFilterPopup = True
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Item"
        Me.GridColumn3.FieldName = "ItemDesc"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        Me.GridColumn3.Width = 300
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Id"
        Me.GridColumn4.FieldName = "ItemId"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Item #"
        Me.GridColumn12.FieldName = "Vendoritemid"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.OptionsColumn.AllowEdit = False
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 1
        Me.GridColumn12.Width = 77
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.chkNeedsApproval)
        Me.GroupControl2.Controls.Add(Me.btnCancel)
        Me.GroupControl2.Controls.Add(Me.btnSave)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl2.Location = New System.Drawing.Point(0, 681)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(790, 132)
        Me.GroupControl2.TabIndex = 40
        Me.GroupControl2.Text = "Actions"
        '
        'chkNeedsApproval
        '
        Me.chkNeedsApproval.Location = New System.Drawing.Point(469, 35)
        Me.chkNeedsApproval.Name = "chkNeedsApproval"
        Me.chkNeedsApproval.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.chkNeedsApproval.Properties.Appearance.Options.UseFont = True
        Me.chkNeedsApproval.Properties.Caption = "Needs Purchasing Agents approval"
        Me.chkNeedsApproval.Size = New System.Drawing.Size(290, 23)
        Me.chkNeedsApproval.TabIndex = 13
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.ImageOptions.SvgImage = CType(resources.GetObject("btnCancel.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnCancel.Location = New System.Drawing.Point(469, 75)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(141, 35)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "Exit"
        '
        'btnSave
        '
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.ImageOptions.SvgImage = CType(resources.GetObject("btnSave.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnSave.Location = New System.Drawing.Point(635, 75)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(141, 35)
        Me.btnSave.TabIndex = 11
        Me.btnSave.Text = "Save"
        '
        'chkShipping
        '
        Me.chkShipping.Location = New System.Drawing.Point(17, 89)
        Me.chkShipping.Name = "chkShipping"
        Me.chkShipping.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.chkShipping.Properties.Appearance.Options.UseFont = True
        Me.chkShipping.Properties.Caption = "Check if items or shipped directly to client"
        Me.chkShipping.Size = New System.Drawing.Size(336, 23)
        Me.chkShipping.TabIndex = 41
        Me.chkShipping.TabStop = False
        '
        'grdLookup
        '
        Me.grdLookup.EditValue = "Select a Client"
        Me.grdLookup.Location = New System.Drawing.Point(394, 82)
        Me.grdLookup.Margin = New System.Windows.Forms.Padding(2)
        Me.grdLookup.Name = "grdLookup"
        Me.grdLookup.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.grdLookup.Properties.Appearance.Options.UseFont = True
        Me.grdLookup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdLookup.Properties.DisplayMember = "Aname"
        Me.grdLookup.Properties.PopupView = Me.GridView3
        Me.grdLookup.Properties.ValueMember = "Acct"
        Me.grdLookup.Size = New System.Drawing.Size(347, 30)
        Me.grdLookup.TabIndex = 42
        Me.grdLookup.TabStop = False
        Me.grdLookup.Visible = False
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn10, Me.GridColumn11})
        Me.GridView3.DetailHeight = 239
        Me.GridView3.FixedLineWidth = 1
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Client"
        Me.GridColumn10.FieldName = "Aname"
        Me.GridColumn10.MinWidth = 13
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 0
        Me.GridColumn10.Width = 50
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Account #"
        Me.GridColumn11.FieldName = "Acct"
        Me.GridColumn11.MinWidth = 13
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 1
        Me.GridColumn11.Width = 50
        '
        'lblClient
        '
        Me.lblClient.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblClient.Appearance.Options.UseFont = True
        Me.lblClient.Location = New System.Drawing.Point(394, 62)
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Size = New System.Drawing.Size(86, 19)
        Me.lblClient.TabIndex = 43
        Me.lblClient.Text = "Select Client"
        Me.lblClient.Visible = False
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(549, 138)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(45, 19)
        Me.LabelControl11.TabIndex = 51
        Me.LabelControl11.Text = "Terms"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(394, 138)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(83, 19)
        Me.LabelControl12.TabIndex = 50
        Me.LabelControl12.Text = "Department"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(206, 134)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(105, 19)
        Me.LabelControl13.TabIndex = 49
        Me.LabelControl13.Text = "Quote Number"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(18, 134)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(89, 19)
        Me.LabelControl14.TabIndex = 48
        Me.LabelControl14.Text = "Sales Person"
        '
        'txtPaymentTerms
        '
        Me.txtPaymentTerms.Location = New System.Drawing.Point(549, 159)
        Me.txtPaymentTerms.Name = "txtPaymentTerms"
        Me.txtPaymentTerms.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtPaymentTerms.Properties.Appearance.Options.UseFont = True
        Me.txtPaymentTerms.Size = New System.Drawing.Size(192, 26)
        Me.txtPaymentTerms.TabIndex = 5
        '
        'txtQuoteNumber
        '
        Me.txtQuoteNumber.Location = New System.Drawing.Point(206, 159)
        Me.txtQuoteNumber.Name = "txtQuoteNumber"
        Me.txtQuoteNumber.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtQuoteNumber.Properties.Appearance.Options.UseFont = True
        Me.txtQuoteNumber.Size = New System.Drawing.Size(177, 26)
        Me.txtQuoteNumber.TabIndex = 3
        '
        'txtSalesPerson
        '
        Me.txtSalesPerson.Location = New System.Drawing.Point(18, 159)
        Me.txtSalesPerson.Name = "txtSalesPerson"
        Me.txtSalesPerson.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtSalesPerson.Properties.Appearance.Options.UseFont = True
        Me.txtSalesPerson.Size = New System.Drawing.Size(182, 26)
        Me.txtSalesPerson.TabIndex = 2
        Me.txtSalesPerson.TabStop = False
        '
        'DxValidationProvider1
        '
        Me.DxValidationProvider1.ValidateHiddenControls = False
        Me.DxValidationProvider1.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual
        '
        'HyperLinkEdit1
        '
        Me.HyperLinkEdit1.EditValue = "Add vendor"
        Me.HyperLinkEdit1.Location = New System.Drawing.Point(12, 63)
        Me.HyperLinkEdit1.Name = "HyperLinkEdit1"
        Me.HyperLinkEdit1.Size = New System.Drawing.Size(67, 20)
        Me.HyperLinkEdit1.TabIndex = 52
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(98, 7)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Check if non - inventory item"
        Me.CheckEdit1.Size = New System.Drawing.Size(229, 20)
        Me.CheckEdit1.TabIndex = 53
        '
        'txtVendor
        '
        Me.txtVendor.Location = New System.Drawing.Point(12, 30)
        Me.txtVendor.Name = "txtVendor"
        Me.txtVendor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtVendor.Properties.Appearance.Options.UseFont = True
        Me.txtVendor.Size = New System.Drawing.Size(342, 26)
        Me.txtVendor.TabIndex = 54
        Me.txtVendor.Visible = False
        '
        'txtItem
        '
        Me.txtItem.Location = New System.Drawing.Point(12, 234)
        Me.txtItem.Name = "txtItem"
        Me.txtItem.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtItem.Properties.Appearance.Options.UseFont = True
        Me.txtItem.Size = New System.Drawing.Size(438, 26)
        Me.txtItem.TabIndex = 55
        Me.txtItem.Visible = False
        '
        'lookupDepartments
        '
        Me.lookupDepartments.Location = New System.Drawing.Point(389, 159)
        Me.lookupDepartments.Name = "lookupDepartments"
        Me.lookupDepartments.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lookupDepartments.Properties.Appearance.Options.UseFont = True
        Me.lookupDepartments.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lookupDepartments.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentId", "Name1", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentName", "Department")})
        Me.lookupDepartments.Size = New System.Drawing.Size(154, 26)
        Me.lookupDepartments.TabIndex = 56
        '
        'frmPurchaseOrderMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(790, 813)
        Me.ControlBox = False
        Me.Controls.Add(Me.lookupDepartments)
        Me.Controls.Add(Me.txtItem)
        Me.Controls.Add(Me.txtVendor)
        Me.Controls.Add(Me.CheckEdit1)
        Me.Controls.Add(Me.HyperLinkEdit1)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.txtPaymentTerms)
        Me.Controls.Add(Me.txtQuoteNumber)
        Me.Controls.Add(Me.txtSalesPerson)
        Me.Controls.Add(Me.lblClient)
        Me.Controls.Add(Me.grdLookup)
        Me.Controls.Add(Me.chkShipping)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.grdGetItems)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.txtTerms)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.grdOrders)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(Me.txtPO)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.grdVendors)
        Me.Name = "frmPurchaseOrderMain"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "New Purchase Order"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTerms.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdVendors.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdGetItems.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.chkNeedsApproval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkShipping.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdLookup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPaymentTerms.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQuoteNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSalesPerson.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DxValidationProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HyperLinkEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVendor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItem.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lookupDepartments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRecalculate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblTotalPo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSubTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTerms As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCost As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grdOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtQty As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdVendors As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents grdGetItems As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkShipping As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents grdLookup As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblClient As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPaymentTerms As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtQuoteNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSalesPerson As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkNeedsApproval As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DxValidationProvider1 As DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider
    Friend WithEvents HyperLinkEdit1 As DevExpress.XtraEditors.HyperLinkEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtVendor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtItem As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lookupDepartments As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemButtonEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
End Class
