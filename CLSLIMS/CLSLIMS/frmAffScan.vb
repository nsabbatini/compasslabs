﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Public Class frmAffScan
    Public scanner As String = "", SpecID As String = ""
    Private Sub frmAffScan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            Debug.Print(CLSLIMS.UID)
        End If
        If CLSLIMS.UID = "" Or CLSLIMS.GetDept(CLSLIMS.UID) <> "SALESUPPORT" Then
            If CLSLIMS.UID <> "NED.SABBATINI" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        txtSpecId.Focus()

        Try
            Dim inifile As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader("C:\CLSLIMS\Scanner.ini")
            scanner = inifile.ReadLine
            inifile.Close()
        Catch
        End Try
    End Sub

    Private Sub txtAcct_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSpecId.KeyUp
        If e.KeyCode = Keys.Enter Then
            ValidateSpecId()
        End If
    End Sub
    Public Sub ValidateSpecId()
        SpecID = ""
        If txtSpecId.Text = "" Then Exit Sub
        Dim Sel As String = "Select pfNAME,plname from PDEM a, PSPEC b Where a.puid=b.puid and a.pefdt=b.pefdt And PSPECNO='" & txtSpecId.Text & "' And PSACT=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            lblLastFirst.Text = rs(1) & ", " & rs(0)
            SpecID = txtSpecId.Text
        Else
            MsgBox("Specimen Number not found.")
            txtSpecId.Text = ""
            txtSpecId.Focus()
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
        txtSpecId.Text = ""
        txtSpecId.Focus()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnScan_Click(sender As Object, e As EventArgs) Handles btnScan.Click

        lblInfo.Text = "Scanning . . ."
        Dim Success As Boolean = False, AppendNotNec As Boolean = True
        'Check WorkingFile Folder - C:\CLSLIMS\Affidavit
        If Not Directory.Exists("C:\CLSLIMS\Affidavit") Then
            Directory.CreateDirectory("C:\CLSLIMS\Affidavit")
        End If
        Dim WorkingFile As String = "C:\CLSLIMS\Affidavit\" & Trim(SpecID) & "AFF.pdf"
        Dim CopyFile As String = "\\CMPFS1\Affidavit\" & Trim(SpecID) & "AFF.pdf"
        Dim CopyFile2 As String = "\\CL-AGILENT\Affidavit\" & Trim(SpecID) & "AFF.pdf"
        'Dim tempscan As String = "C:\CLSLIMS\Affidavit\tempscan.pdf", PDF12(2) As String

        ScanImage.RemoveAllImages()

        If File.Exists(WorkingFile) Then
            File.Delete(WorkingFile)
        End If

        'ScanImage.LicenseKeys = "E378C7266C834EDE2B70E88E236EEE1C"
        'ScanImage.LicenseKeys = "50663FBE5F990BEFC170A6AC986CE8BA"
        ScanImage.LicenseKeys = "E9C5E61C6C3BEE4361A6AE53AB47C116"
        ScanImage.IfThrowException = False
        ScanImage.IfDisableSourceAfterAcquire = True
        ScanImage.IfShowUI = False
        Dim int As Integer = ScanImage.SourceCount
        For i = 0 To int
            If ScanImage.SourceNameItems(i) = scanner Then
                ScanImage.SelectSourceByIndex(i)
                Exit For
            End If
        Next i

        If ScanImage.CurrentSourceName = "" Then
            ScanImage.SelectSource()
        End If
        Debug.Print(ScanImage.CurrentSourceName)
        Success = ScanImage.CloseSource()
        Success = ScanImage.OpenSource()

        If Success Then
            'ScanImage.Resolution = 600
            Success = ScanImage.AcquireImage()
            If Success Then
                Success = ScanImage.SaveAllAsPDF(WorkingFile)
            End If
            If Success Then
                FileCopy(WorkingFile, CopyFile)
                FileCopy(WorkingFile, CopyFile2)
            End If
        End If
        lblInfo.Text = "Scan complete."

    End Sub
    Public Function CombinePDFs(ByVal FirstPDF As String, ByVal SecondPDF As String, Optional ByVal ComboFile As String = "OutputCmb.pdf") As String

        CombinePDFs = FirstPDF
        Dim reader As iTextSharp.text.pdf.PdfReader = Nothing
        Dim reader2 As iTextSharp.text.pdf.PdfReader = Nothing
        Dim writer As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim cb As iTextSharp.text.pdf.PdfContentByte = Nothing
        Dim page As iTextSharp.text.pdf.PdfImportedPage = Nothing
        Dim rotation As Integer, PageCount As Integer, i As Integer = 1
        Dim WorkingFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim CombineFile = Path.Combine(WorkingFolder, ComboFile)
        Dim fs As New FileStream(CombineFile, FileMode.Create)
        Dim pdfDoc As New Document
        Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, fs)

        pdfDoc.Open()

        reader = New iTextSharp.text.pdf.PdfReader(FirstPDF)

        PageCount = reader.NumberOfPages
        cb = pdfWrite.DirectContent
        pdfDoc.NewPage()
        Do While i <= PageCount
            page = pdfWrite.GetImportedPage(reader, i)
            rotation = reader.GetPageRotation(i)
            cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
            pdfDoc.NewPage()
            i = i + 1
        Loop
        'reader.Close()
        reader2 = New iTextSharp.text.pdf.PdfReader(SecondPDF)

        PageCount = reader2.NumberOfPages
        i = 1
        Do While i <= PageCount
            page = pdfWrite.GetImportedPage(reader2, i)
            rotation = reader2.GetPageRotation(i)
            cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
            pdfDoc.NewPage()
            i = i + 1
        Loop
        pdfDoc.Close()
        reader.Close()
        reader2.Close()
        fs.Close()

        CombinePDFs = CombineFile
    End Function


End Class