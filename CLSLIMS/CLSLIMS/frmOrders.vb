﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class frmOrders
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub cmbItem_TextChanged(sender As Object, e As EventArgs) Handles cmbItem.TextChanged
        FormatItemDesc(cmbItem.Text)
    End Sub

    Private Sub frmOrders_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        ClearFields()
        Dim Sel As String = ""
        Sel = "Select itemabbr,itemunit,itemperunit,itemdesc from ITEMS"
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            cmbItem.Items.Add(rs(0))
            lstData.Items.Add(rs(0) & "^" & rs(1) & "^" & rs(2) & "^" & rs(3))
        Loop
        rs.Close()
        ConnMySql.Close()
        lblItemDesc.Text = ""
        lblUnit.Text = ""
        lblStatus.Text = ""
        If CLSLIMS.OrderNum <> "" Then
            txtOrdNo.Text = CLSLIMS.OrderNum
            Me.Text = "ORDER APPROVAL"
            LoadOrder(CLSLIMS.OrderNum)
        End If

    End Sub

    Private Sub txtAcctNo_KeyUp(sender As Object, e As KeyEventArgs) Handles txtAcctNo.KeyUp
        Dim r As String = "", a As String = "", b As String = "", tmp As String = "", tmp2 As String = "", tmp3 As String = ""
        If e.KeyCode = Keys.Enter Then
            r = GetAcctInfo(txtAcctNo.Text)
            txtAcctName.Text = CLSLIMS.Piece(r, "^", 1)
            txtContact.Text = CLSLIMS.Piece(r, "^", 2)
            txtAddr.Text = CLSLIMS.Piece(r, "^", 3)
            txtAddr2.Text = CLSLIMS.Piece(r, "^", 4)
            txtCity.Text = CLSLIMS.Piece(r, "^", 5)
            txtState.Text = CLSLIMS.Piece(r, "^", 6)
            txtZIP.Text = CLSLIMS.Piece(r, "^", 7)
            txtPhone.Text = CLSLIMS.Piece(r, "^", 8)
            txtFax.Text = CLSLIMS.Piece(r, "^", 9)
            txtNotes.Text = CHECKSTUFF("ACCTNOTE", txtAcctNo.Text) & vbCrLf & Chr(3) & CHECKSTUFF("ORDERNOTE", txtOrdNo.Text)
            '----------------------------------------------------------------------------------------------------------------------------
            r = GetPrevOrd(txtAcctNo.Text)
                a = CLSLIMS.Piece(r, "|", 1) : b = CLSLIMS.Piece(r, "|", 2)
                For i = 1 To 20
                    tmp = CLSLIMS.Piece(a, "^", i) : tmp2 = CLSLIMS.Piece(b, "^", i)
                    If tmp = "" Then Exit For
                    txtPrevOrd.Text = txtPrevOrd.Text & "Order#: " & CLSLIMS.Piece(tmp, ";", 1) & vbCrLf & "Shipped: " & CLSLIMS.CDT(CLSLIMS.Piece(tmp, ";", 3), "V") & vbCrLf
                    For j = 1 To 20
                        tmp3 = CLSLIMS.Piece(tmp2, "~", j)
                        If tmp3 = "" Then Exit For
                        txtPrevOrd.Text = txtPrevOrd.Text & CLSLIMS.Piece(tmp3, ";", 1) & "-" & CLSLIMS.RJ(CLSLIMS.Piece(tmp3, ";", 2), 3) & " " & CLSLIMS.Piece(tmp3, ";", 3) & vbCrLf
                    Next j
                    txtPrevOrd.Text = txtPrevOrd.Text & "---------------------------------" & vbCrLf
                Next i
                '----------------------------------------------------------------------------------------------------------------------------

                cmbItem.Focus()
            End If
    End Sub

    Private Sub txtOrdNo_KeyUp(sender As Object, e As KeyEventArgs) Handles txtOrdNo.KeyUp
        If e.KeyCode = Keys.Enter Then
            If UCase(txtOrdNo.Text) = "N" Then
                ClearFields()
                txtOrdNo.Text = "NEW"
                txtAcctNo.Focus()
            Else
                LoadOrder(txtOrdNo.Text)
            End If
        End If
    End Sub
    Public Sub LoadOrder(ByVal ord As String)
        ClearFields()
        txtOrdNo.Text = ord
        txtOrdNo.Focus()
        SendKeys.Send("{HOME}") : SendKeys.Send("+{END}")
        'Retrieve order from ORDERS
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Dim r As String = "", a As String = "", b As String = "", tmp As String = "", tmp2 As String = "", tmp3 As String = "", Status As String = ""
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select * From Orders Where OrderNum='" & ord & "' And HSEQ=0"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            Status = Trim((rs(2)))
            lblStatus.Text = CHECKSTUFF("STATUS", Status)
            txtAcctNo.Text = Trim(rs(3)) : txtAcctName.Text = Trim(rs(4)) : txtContact.Text = Trim(rs(5)) : txtAddr.Text = Trim(rs(6))
            txtAddr2.Text = Trim(rs(7)) : txtCity.Text = Trim(rs(8)) : txtState.Text = Trim(rs(9)) : txtZIP.Text = Trim(rs(10))
            txtPhone.Text = Trim(rs(11)) : txtFax.Text = Trim(IIf(IsDBNull(rs(12)), "", rs(12))) : cmbCourier.Text = Trim(rs(13)) : txtTrackNum.Text = Trim(IIf(IsDBNull(rs(14)), "", rs(14))) : txtWt.Text = Trim(IIf(IsDBNull(rs(15)), "", rs(15))) : txtPkgs.Text = Trim(IIf(IsDBNull(rs(16)), "", rs(16)))
            lblOrdDate.Text = CLSLIMS.CDT(Trim(rs(18)), "V") : lblPickDate.Text = CLSLIMS.CDT(Trim(rs(19)), "V") : lblShipDate.Text = CLSLIMS.CDT(Trim(rs(20)), "V") : lblRecvDate.Text = CLSLIMS.CDT(Trim(IIf(IsDBNull(rs(21)), "", rs(21))), "V")
            txtNotes.Text = CHECKSTUFF("ACCTNOTE", txtAcctNo.Text) & vbCrLf & Chr(3) & CHECKSTUFF("ORDERNOTE", txtOrdNo.Text)
            '----------------------------------------------------------------------------------------------------------------------------
            r = GetPrevOrd(txtAcctNo.Text)
            a = CLSLIMS.Piece(r, "|", 1) : b = CLSLIMS.Piece(r, "|", 2)
            For i = 1 To 20
                tmp = CLSLIMS.Piece(a, "^", i) : tmp2 = CLSLIMS.Piece(b, "^", i)
                If tmp = "" Then Exit For
                txtPrevOrd.Text = txtPrevOrd.Text & "Order#: " & CLSLIMS.Piece(tmp, ";", 1) & vbCrLf & "Shipped: " & CLSLIMS.CDT(CLSLIMS.Piece(tmp, ";", 3), "V") & vbCrLf
                For j = 1 To 20
                    tmp3 = CLSLIMS.Piece(tmp2, "~", j)
                    If tmp3 = "" Then Exit For
                    txtPrevOrd.Text = txtPrevOrd.Text & CLSLIMS.Piece(tmp3, ";", 1) & "-" & CLSLIMS.RJ(CLSLIMS.Piece(tmp3, ";", 2), 3) & " " & CLSLIMS.Piece(tmp3, ";", 3) & vbCrLf
                Next j
                txtPrevOrd.Text = txtPrevOrd.Text & "---------------------------------" & vbCrLf
            Next i
        Else
            rs.Close()
            ConnMySql.Close()
            txtOrdNo.Text = ""
            txtOrdNo.Focus()
            Exit Sub
        End If
        rs.Close()
        'Retrieve order items from ORDERITEMS
        Cmd.CommandText = "Select * From ORDERITEMS Where OrderNum='" & ord & "' And HSEQ=0"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            txtQty.Text = rs(4) : cmbItem.Text = rs(3)
            FormatItemDesc(rs(3))
            lstItems.Items.Add(CLSLIMS.RJ(lstItems.Items.Count + 1, 3) & ") " & CLSLIMS.RJ(Trim(txtQty.Text), 4) & " " & lblItemDesc.Text)
            lstItemsOrdered.Items.Add(txtQty.Text & "^" & cmbItem.Text)
            txtQty.Text = "" : lblItemDesc.Text = "" : cmbItem.Text = ""
        Loop
        rs.Close()
        Cmd.Dispose()
        ConnMySql.Close()
        ConnMySql.Dispose()
        If Status = "99" Then
            txtOrdNo.BackColor = Color.Red
            btnSave.Enabled = False
            btnSaveApp.Enabled = False
            lblStatus.BackColor = Color.Red
        End If
    End Sub
    Public Function GetAcctInfo(ByVal Acct) As String
        GetAcctInfo = ""
        Dim xSel As String
        xSel = "select acct,aname,acontact,aad1,aad2,acity,astate,azip,aphone,afax " &
               "from acct " &
               "where Acct = '" & Acct & "' and aact=1"

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctInfo = Replace(cRS(1), ",", " ") & "^" & cRS(2) & "^" & cRS(3) & "^" & cRS(4) & "^" & cRS(5) & "^" & cRS(6) & "^" & cRS(7) & "^" & cRS(8) & "^" & cRS(9)
            Else
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function

    Private Sub btnOrder_Click(sender As Object, e As EventArgs) Handles btnOrder.Click
        Dim Msg As String = ""
        If cmbItem.Text = "" Or Val(txtQty.Text) = 0 Then
            Msg = "Must have an Item or valid quantity."
        End If
        '====================================================
        '    Check for product/account mismatch
        '====================================================
        If cmbItem.Text = "TCUP" And Mid(txtAcctNo.Text, 1, 1) = "S" Then
            Msg = "Trying to order Tox Cups on Oral Fluid account."
        End If
        If cmbItem.Text = "OSWAB" And Mid(txtAcctNo.Text, 1, 1) <> "S" Then
            Msg = "Trying to order Oral Swabs on Tox account."
        End If
        If cmbItem.Text = "TREQ" And Mid(txtAcctNo.Text, 1, 1) = "S" Then
            Msg = "Trying to order Tox Reqs on Oral Fluid account."
        End If
        If cmbItem.Text = "OREQ" And Mid(txtAcctNo.Text, 1, 1) <> "S" Then
            Msg = "Trying to order Oral Reqs on Tox account."
        End If
        If cmbItem.Text = "REDTOP" And Mid(txtAcctNo.Text, 1, 1) = "S" Then
            Msg = "Trying to order Red Top Specimen Tubes on Oral Fluid account."
        End If
        If cmbItem.Text = "PHAT" And Mid(txtAcctNo.Text, 1, 1) = "S" Then
            Msg = "Trying to order Urine Collection Hat on Oral Fluid account."
        End If
        If Msg <> "" Then
            MsgBox(Msg)
            cmbItem.Text = ""
            txtQty.Text = ""
            Exit Sub
        End If
        lstItems.Items.Add(CLSLIMS.RJ(lstItems.Items.Count + 1, 3) & ") " & CLSLIMS.RJ(Trim(txtQty.Text), 4) & " " & lblItemDesc.Text)
        lstItemsOrdered.Items.Add(txtQty.Text & "^" & cmbItem.Text)
        '====================================================
        '    Auto order product combinations
        '====================================================
        If cmbItem.Text = "TCUP" Or cmbItem.Text = "OSWAB" Then
            'cmbItem.Text = "SBAGS" : lblItemDesc.Text = "Non Bioharzard Specimen Bag"
            'lstItems.Items.Add(CLSLIMS.RJ(lstItems.Items.Count + 1, 3) & ") " & CLSLIMS.RJ(Trim(txtQty.Text), 4) & " " & lblItemDesc.Text)
            'lstItemsOrdered.Items.Add(txtQty.Text & "^" & cmbItem.Text)
        ElseIf cmbItem.Text = "FELBOX" Or cmbItem.Text = "FEMBOX" Then
            cmbItem.Text = "FELP" : lblItemDesc.Text = "FedEx Clinical LabPak"
            lstItems.Items.Add(CLSLIMS.RJ(lstItems.Items.Count + 1, 3) & ") " & CLSLIMS.RJ((Val(Trim(txtQty.Text)) * 20), 4) & " " & lblItemDesc.Text)
            lstItemsOrdered.Items.Add(Val(txtQty.Text) * 20 & "^" & cmbItem.Text)
        ElseIf cmbItem.Text = "UPSMBOX" Then
            cmbItem.Text = "UPSLP" : lblItemDesc.Text = "UPS Clinical LabPak"
            lstItems.Items.Add(CLSLIMS.RJ(lstItems.Items.Count + 1, 3) & ") " & CLSLIMS.RJ((Val(Trim(txtQty.Text)) * 20), 4) & " " & lblItemDesc.Text)
            lstItemsOrdered.Items.Add(Val(txtQty.Text) * 20 & "^" & cmbItem.Text)
        ElseIf cmbItem.Text = "REDTOP" Then
            cmbItem.Text = "SBAGS" : lblItemDesc.Text = "Non Bioharzard Specimen Bag"
            lstItems.Items.Add(CLSLIMS.RJ(lstItems.Items.Count + 1, 3) & ") " & CLSLIMS.RJ(Trim(txtQty.Text), 4) & " " & lblItemDesc.Text)
            lstItemsOrdered.Items.Add(txtQty.Text & "^" & cmbItem.Text)
            cmbItem.Text = "TRNSPIP" : lblItemDesc.Text = "Transfer Pipettes"
            lstItems.Items.Add(CLSLIMS.RJ(lstItems.Items.Count + 1, 3) & ") " & CLSLIMS.RJ(Trim(txtQty.Text), 4) & " " & lblItemDesc.Text)
            lstItemsOrdered.Items.Add(txtQty.Text & "^" & cmbItem.Text)
        End If
        txtQty.Text = "" : lblItemDesc.Text = "" : cmbItem.Text = ""
        cmbItem.Focus()
    End Sub
    Private Sub btnSaveApp_Click(sender As Object, e As EventArgs) Handles btnSaveApp.Click
        SaveOrder(True)
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        SaveOrder(False)
    End Sub
    Public Sub SaveOrder(ByVal Apprv As Boolean)
        Dim NewOrd As Boolean = False, TodayNow As String = Format(Now, "yyyyMMddHHmmss"), Status As String = "0", IStatus As String = "0", HSEQ As Integer = 0
        If cmbCourier.Text = "" Then
            MsgBox("No courier selected.")
            Exit Sub
        End If
        If lstItemsOrdered.Items.Count = 0 Then
            MsgBox("No items ordered.")
            Exit Sub
        End If
        If txtAcctNo.Text = "" Then
            MsgBox("No account number given.")
            Exit Sub
        End If
        txtAcctName.Text = Replace(txtAcctName.Text, "'", " ")
        txtAddr.Text = Replace(txtAddr.Text, "'", " ")
        txtAddr2.Text = Replace(txtAddr2.Text, "'", " ")
        txtAddr.Text = Replace(txtAddr.Text, ",", " ")
        txtAddr2.Text = Replace(txtAddr2.Text, ",", " ")
        txtContact.Text = Replace(txtContact.Text, "'", " ")
        If txtOrdNo.Text = "NEW" Then
            NewOrd = True
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim pCmd As New SqlCommand("dbo.xTNCL_NEXTVAL", Conn)
            pCmd.CommandType = CommandType.StoredProcedure
            pCmd.Parameters.Add("SYSVALKEY", SqlDbType.VarChar).Value = "Orders"
            pCmd.Parameters.Add("SYSVALVAL", SqlDbType.Float).Value = 0
            pCmd.Parameters("SYSVALVAL").Direction = ParameterDirection.Output
            Try
                pCmd.ExecuteNonQuery()
            Catch
                MsgBox("x")
            End Try
            txtOrdNo.Text = CInt(pCmd.Parameters("SYSVALVAL").Value)
            pCmd.Dispose()
            Conn.Close()
        End If
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Dim SQL As String = ""
        Cmd.Connection = ConnMySql
        Dim rs As MySqlDataReader = Nothing

        If NewOrd Then
            Status = "0" : IStatus = 0
        Else
            Cmd.CommandText = "Select Max(HSEQ)+1 From Orders Where OrderNum='" & txtOrdNo.Text & "'"
            rs = Cmd.ExecuteReader
            If rs.Read Then HSEQ = rs(0)
            rs.Close()
            Cmd.CommandText = "Update Orders Set HSEQ=" & HSEQ & " Where HSEQ=0 And OrderNum='" & txtOrdNo.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "Update OrderItems Set HSEQ=" & HSEQ & " Where HSEQ=0 And OrderNum='" & txtOrdNo.Text & "'"
            Cmd.ExecuteNonQuery()
            'Cmd.CommandText = "Select Status from Orders where OrderNum='" & txtOrdNo.Text & "'"
            'rs = Cmd.ExecuteReader
            'If rs.Read Then Status = rs(0)
            'rs.Close()
        End If
        If Status = 0 Then
            If CHECKSTUFF("APPROVE", CLSLIMS.UID) = "Y" And Apprv Then
                Status = 5 : IStatus = 5
            End If
        End If
        SQL = "Insert into Orders Values ('" & txtOrdNo.Text & "',0,'" & Status & "','" & txtAcctNo.Text & "','" & Replace(txtAcctName.Text, "'", " ") & "','" & Replace(txtContact.Text, "'", " ") & "','" & Replace(txtAddr.Text, "'", " ") & "','" & Replace(txtAddr2.Text, "'", " ") & "','" & Replace(txtCity.Text, "'", " ") &
                "','" & txtState.Text & "','" & txtZIP.Text & "','" & txtPhone.Text & "','" & txtFax.Text & "','" & cmbCourier.Text & "','','','','" & CLSLIMS.UID & "','" & TodayNow & "','','','','')"
        Cmd.CommandText = SQL
        Cmd.ExecuteNonQuery()
        For i = 0 To lstItems.Items.Count - 1
            Cmd.CommandText = "Insert into OrderItems Values ('" & txtOrdNo.Text & "',0," & i + 1 & ",'" & CLSLIMS.Piece(lstItemsOrdered.Items(i), "^", 2) & "'," & CLSLIMS.Piece(lstItemsOrdered.Items(i), "^", 1) & ",'" & IStatus & "')"
            Cmd.ExecuteNonQuery()
        Next i
        If CLSLIMS.Piece(txtNotes.Text, Chr(3), 2) <> "" Then
            Cmd.CommandText = "Delete From Stuff Where WHAT='ORDERNOTE' and LKUP='" & txtOrdNo.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "Insert Into Stuff Values ('ORDERNOTE','" & txtOrdNo.Text & "','" & CLSLIMS.Piece(txtNotes.Text, Chr(3), 2) & "')"
            Cmd.ExecuteNonQuery()
        End If
        Cmd.Dispose()
        ConnMySql.Close()
        If Status > 4 Then
            PrintPick(txtOrdNo.Text, True)
        End If
        ClearFields()
    End Sub
    Public Function CHECKSTUFF(ByVal WHAT As String, LKUP As String) As String
        CHECKSTUFF = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select VALUE From STUFF Where WHAT='" & WHAT & "' And LKUP='" & LKUP & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            CHECKSTUFF = rs(0)
        End If
        rs.Close()
        Cmd.Dispose()
        ConnMySql.Close()
        ConnMySql.Dispose()
    End Function
    Public Function FormatItemDesc(ByVal itemabbr As String) As String
        FormatItemDesc = ""
        For i = 0 To lstData.Items.Count - 1
            If itemabbr = CLSLIMS.Piece(lstData.Items(i), "^", 1) Then
                lblItemDesc.Text = CLSLIMS.Piece(lstData.Items(i), "^", 4) & " (" & CLSLIMS.Piece(lstData.Items(i), "^", 3) & " per " & CLSLIMS.Piece(lstData.Items(i), "^", 2) & ")"
                lblUnit.Text = CLSLIMS.Piece(lstData.Items(i), "^", 2)
                Exit For
            End If
        Next i
    End Function
    Public Sub ClearFields()
        lblItemDesc.Text = ""
        lblOrdDate.Text = ""
        lblPickDate.Text = ""
        lblRecvDate.Text = ""
        lblShipDate.Text = ""
        lblStatus.Text = ""
        lblStatus.BackColor = Color.LightSteelBlue
        lblUnit.Text = ""
        txtAcctName.Text = ""
        txtAcctNo.Text = ""
        cmbCourier.Text = ""
        txtAddr.Text = ""
        txtAddr2.Text = ""
        txtCity.Text = ""
        txtContact.Text = ""
        txtInfo.Text = ""
        txtNotes.Text = ""
        txtOrdNo.Text = ""
        txtOrdNo.BackColor = Color.White
        txtPhone.Text = ""
        txtFax.Text = ""
        txtPkgs.Text = ""
        txtPrevOrd.Text = ""
        txtQty.Text = ""
        txtState.Text = ""
        txtTrackNum.Text = ""
        txtWt.Text = ""
        txtZIP.Text = ""
        wbAcctVol.Stop()
        wbAcctVol.Document.Write("")
        wbAcctVol.Refresh()
        lstItems.Items.Clear()
        lstItemsOrdered.Items.Clear()
        lstPrtPick.Items.Clear()
        btnSave.Enabled = True
        btnSaveApp.Enabled = True
    End Sub
    Public Function GetVol(ByVal acct As String) As String
        GetVol = ""  'YYYYMM;cnt^YYYYMM;cnt^YYYYMM;cnt^YYYYMM;cnt^
        If GetAcctInfo(acct) = "" Then
            Exit Function
        End If
        Dim first As String = Format(DateAdd(DateInterval.Month, -4, Now), "yyyyMM01")
        Dim last As String = Format(Now, "yyyyMMdd")
        Dim SQL As String = "Select substring(A.PSRCVDT,1,6) as dt,COUNT(*) as cnt " &
                            "From PORD C, PSPEC A " &
                            "Where C.PORD = A.PORD And C.POEFDT = A.POEFDT " &
                            "And substring(A.PSRCVDT,1,8) Between '" & first & "' and '" & last & "' " &
                            "And C.ACCT = '" & acct & "' " &
                            "And POACT=1 And PSACT=1 " &
                            "Group by substring(A.PSRCVDT,1,6) " &
                            "Order by substring(A.PSRCVDT,1,6) desc "
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(SQL, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                GetVol = GetVol & cRS(0) & ";" & cRS(1) & "^"
            Loop
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Public Function GetPrevOrd(ByVal acct As String) As String
        GetPrevOrd = ""  'OrderNum;UID;OrderDate;ShippedDate^OrderNum;UID;OrderDate;ShippedDate^OrderNum;UID;OrderDate;ShippedDate^ . . .
        '|OrderSeq;Qty;ItemID~OrderSeq;Qty;ItemID^OrderSeq;Qty;ItemID~^
        Dim Data1 As String = "", Data2 As String = ""
        If GetAcctInfo(acct) = "" Then
            Exit Function
        End If
        Dim first As String = Format(DateAdd(DateInterval.Month, -4, Now), "yyyyMM01")
        Dim last As String = Format(Now, "yyyyMMdd")
        Dim SQL As String = "Select OrderNum,UID,OrderDate,ShippedDate From Orders " &
                            "Where Acct='" & acct & "' And HSEQ=0 Order by OrderNum Desc"
        Dim SQL2 As String = ""
        Using Conn As New MySqlConnection(CLSLIMS.strConMySQL)
            Conn.Open()
            Dim Conn2 As New MySqlConnection(CLSLIMS.strConMySQL)
            Conn2.Open()
            Dim Cmd As New MySqlCommand(SQL, Conn), Cmd2 As New MySqlCommand(SQL2, Conn2)
            Dim cRS As MySqlDataReader = Cmd.ExecuteReader, cRS2 As MySqlDataReader = Nothing
            Do While cRS.Read
                Data1 = Data1 & cRS(0) & ";" & cRS(1) & ";" & cRS(2) & ";" & cRS(3) & "^"
                SQL2 = "Select OrderSeq,Qty,ItemID From OrderItems Where OrderNum='" & cRS(0) & "' And HSEQ=0 Order By OrderSeq"
                Cmd2.CommandText = SQL2
                cRS2 = Cmd2.ExecuteReader
                Do While cRS2.Read
                    Data2 = Data2 & cRS2(0) & ";" & cRS2(1) & ";" & cRS2(2) & "~"
                Loop
                cRS2.Close()
                Data2 = Data2 & "^"
            Loop
            cRS.Close()
            Conn.Close()
        End Using
        GetPrevOrd = Data1 & "|" & Data2
    End Function

    Private Sub lstItems_MouseDown(sender As Object, e As MouseEventArgs) Handles lstItems.MouseDown
        Dim ndx As Integer = -1
        If e.Button = MouseButtons.Right Then
            ndx = lstItems.SelectedIndex
            If ndx > -1 Then
                Dim Rsp As MsgBoxResult = MsgBox("Are you sure you wish to delete " & lstItems.SelectedItem & "?")
                If Rsp = MsgBoxResult.Ok Then
                    lstItems.Items.RemoveAt(ndx)
                    lstItemsOrdered.Items.RemoveAt(ndx)
                End If
            End If
            lstItems.Refresh()
            lstItemsOrdered.Refresh()
            'MsgBox(lstItems.SelectedIndex)

        End If
    End Sub
    Public Sub PrintPick(ByVal ordernum As String, b As Boolean)

        'Hewlett-Packard HP Laserjet M402n on compass-ship
        Call BuildPick(ordernum)
        'If UCase(System.Net.Dns.GetHostName) = "COMPASS-SHIP" Then
        '    PrintPck.PrinterSettings.PrinterName = "EPSON FX-890"
        'ElseIf UCase(System.Net.Dns.GetHostName) = "COMPASS-NED" Then

        'Else
        '    'Hewlett-Packard HP Laserjet M402n on compass-ship
        '    PrintPck.PrinterSettings.PrinterName = "Hewlett-Packard HP Laserjet M402n on compass-ship"
        'End If
        'Hewlett-Packard HP Laserjet M402n on compass-ship
        PrintPck.DefaultPageSettings.Margins.Left = 10
        PrintPck.DefaultPageSettings.Margins.Right = 10
        PrintPck.DefaultPageSettings.Margins.Top = 10
        PrintPck.DefaultPageSettings.Margins.Bottom = 10
        PrintPck.DefaultPageSettings.Landscape = False

        PrintPck.Print()
        'If b Then
        '    'Print blank order form
        '    Dim PrtProc As New ProcessStartInfo
        '    PrtProc.UseShellExecute = True
        '    PrtProc.Verb = "Print"
        '    PrtProc.WindowStyle = ProcessWindowStyle.Hidden
        '    'PrtProc.Arguments = OrderBlanks.PrinterSettings.PrinterName("Hewlett-Packard HP Laserjet M402n on compass-ship")
        '    If Mid(txtAcctNo.Text, 1, 1) = "S" Then
        '        PrtProc.FileName = "C:\CLSLIMS\Supply Order Form Oral Fluid Only.pdf"
        '    Else
        '        PrtProc.FileName = "C:\CLSLIMS\Supply Order Form PDM Only.pdf"
        '    End If
        '    Process.Start(PrtProc)
        'End If
    End Sub
    Public Sub BuildPick(ByVal ordernum As String)
        lstPrtPick.Items.Clear()
        lstPrtPick.Items.Add(ordernum)
        lstPrtPick.Items.Add(frmQues.GetOrderInfo(ordernum, 0))
        For i = 0 To lstItems.Items.Count - 1
            lstPrtPick.Items.Add(GetOrderInfoItems(ordernum, i + 1))
        Next i
    End Sub
    Public Function GetOrderInfoItems(ByVal ord As String, ByVal ordseq As String) As String
        GetOrderInfoItems = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select * From OrderItems Where OrderNum='" & ord & "' and HSEQ=0 And OrderSeq='" & ordseq & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            'ItemId^Item Name^Qty
            GetOrderInfoItems = rs(3) & "^" & GetItemDesc(rs(3)) & "^" & rs(4)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function
    Public Function GetItemDesc(ByVal item As String) As String
        GetItemDesc = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select ItemDesc From Items Where ItemID='" & item & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            'ItemId^Item Name^Qty
            GetItemDesc = rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function

    Private Sub PrintPck_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintPck.PrintPage
        Dim y As Integer = 550, r As String = "", y2 As Integer = 370
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font6 As New Font("Tacoma", 14, FontStyle.Bold Or FontStyle.Underline)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim LogoImage As Image = My.Resources.Compass_Logo
        'Header
        e.Graphics.DrawImage(LogoImage, 25, 25)
        e.Graphics.DrawString("Compass Laboratory Services", Font2, Brushes.Black, 200, 90)
        e.Graphics.DrawString("1910 Nonconnah Blvd, Suite 108", Font2, Brushes.Black, 180, 115)
        e.Graphics.DrawString("Memphis, TN 38132", Font2, Brushes.Black, 250, 140)
        e.Graphics.DrawString("(901) 348-5774", Font2, Brushes.Black, 270, 165)

        e.Graphics.DrawString("Order #: " & lstPrtPick.Items(0), Font2, Brushes.Black, 40, 290)
        r = lstPrtPick.Items(1)
        e.Graphics.DrawString("Account #: " & CLSLIMS.Piece(r, "^", 1), Font3, Brushes.Black, 40, 345)
        e.Graphics.DrawString(CLSLIMS.Piece(r, "^", 2), Font3, Brushes.Black, 100, y2) : y2 = y2 + 15
        e.Graphics.DrawString(CLSLIMS.Piece(r, "^", 3), Font3, Brushes.Black, 100, y2) : y2 = y2 + 15
        e.Graphics.DrawString(CLSLIMS.Piece(r, "^", 4), Font3, Brushes.Black, 100, y2) : y2 = y2 + 15
        If CLSLIMS.Piece(r, "^", 5) <> "" Then e.Graphics.DrawString(CLSLIMS.Piece(r, "^", 5), Font3, Brushes.Black, 100, y2) : y2 = y2 + 15
        e.Graphics.DrawString(CLSLIMS.Piece(r, "^", 6) & ", " & CLSLIMS.Piece(r, "^", 7) & "  " & CLSLIMS.Piece(r, "^", 8), Font3, Brushes.Black, 100, y2) : y2 = y2 + 25
        e.Graphics.DrawString("Phone: " & CLSLIMS.Piece(r, "^", 9), Font3, Brushes.Black, 100, y2) : y2 = y2 + 15
        e.Graphics.DrawString(" Fax: " & CLSLIMS.Piece(r, "^", 10), Font3, Brushes.Black, 118, y2) : y2 = y2 + 15
        'Detail
        For i = 2 To lstPrtPick.Items.Count - 1
            r = CLSLIMS.FormatListBox(CLSLIMS.Piece(lstPrtPick.Items(i), "^", 1) & ";10;LJ^") & CLSLIMS.FormatListBox(Mid(CLSLIMS.Piece(lstPrtPick.Items(i), "^", 2), 1, 40) & ";40;LJ^") & CLSLIMS.FormatListBox(CLSLIMS.Piece(lstPrtPick.Items(i), "^", 3) & ";5;^")
            e.Graphics.DrawString(i - 1 & ") " & r & "  [_]", Font5, Brushes.Black, 50, y)
            y = y + 20
        Next i
        'e.Graphics.DrawString("Prepared by:   ______________________________", Font3, Brushes.Black, 200, y + 50)
    End Sub

    Private Sub btnVol_Click(sender As Object, e As EventArgs) Handles btnVol.Click
        Dim r As String = GetVol(txtAcctNo.Text)
        wbAcctVol.Stop() : wbAcctVol.Document.OpenNew(False)
        Dim html As String = "<html><table border=1><th>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 1), ";", 1) & "</th><th>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 2), ";", 1) & "</th><th>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 3), ";", 1) & "</th><th>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 4), ";", 1) & "</th><tr><td>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 1), ";", 2) & "</td><td>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 2), ";", 2) & "</td><td>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 3), ";", 2) & "</td><td>" & CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 4), ";", 2) & "</td></tr></table></html>"
        wbAcctVol.Document.Write(html) : wbAcctVol.Refresh()
        '----------------------------------------------------------------------------------------------------------------------------
    End Sub

    Private Sub btnPrtPck_Click(sender As Object, e As EventArgs) Handles btnPrtPck.Click
        PrintPick(txtOrdNo.Text, False)
    End Sub

    Private Sub frmOrders_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        Dim Rsp As String = "", HSEQ As Integer = 99, Status As String = "99", IStatus As String = "99", TodayNow As String = Format(Now, "yyyyMMddHHmmss")
        If e.Control And e.KeyCode = Keys.D Then
            Rsp = MsgBox("Are you sure you want to delete Order " & txtOrdNo.Text & "?", vbOKCancel)
            If Rsp = vbOK Then
                Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
                ConnMySql.Open()
                Dim Cmd As New MySqlCommand
                Dim rs As MySqlDataReader = Nothing
                Dim SQL As String = ""
                Cmd.Connection = ConnMySql
                Cmd.CommandText = "Select Max(HSEQ)+1 From Orders Where OrderNum='" & txtOrdNo.Text & "'"
                rs = Cmd.ExecuteReader
                If rs.Read Then HSEQ = rs(0)
                rs.Close()
                Cmd.CommandText = "Update Orders Set HSEQ=" & HSEQ & " Where HSEQ=0 And OrderNum='" & txtOrdNo.Text & "'"
                Cmd.ExecuteNonQuery()
                Cmd.CommandText = "Update OrderItems Set HSEQ=" & HSEQ & " Where HSEQ=0 And OrderNum='" & txtOrdNo.Text & "'"
                Cmd.ExecuteNonQuery()
                SQL = "Insert into Orders Values ('" & txtOrdNo.Text & "',0,'" & Status & "','" & txtAcctNo.Text & "','" & Replace(txtAcctName.Text, "'", " ") & "','" & Replace(txtContact.Text, "'", " ") & "','" & Replace(txtAddr.Text, "'", " ") & "','" & Replace(txtAddr2.Text, "'", " ") & "','" & Replace(txtCity.Text, "'", " ") &
                "','" & txtState.Text & "','" & txtZIP.Text & "','" & txtPhone.Text & "','" & txtFax.Text & "','" & cmbCourier.Text & "','','','','" & CLSLIMS.UID & "','" & TodayNow & "','','','','')"
                Cmd.CommandText = SQL
                Cmd.ExecuteNonQuery()
                For i = 0 To lstItems.Items.Count - 1
                    Cmd.CommandText = "Insert into OrderItems Values ('" & txtOrdNo.Text & "',0," & i + 1 & ",'" & CLSLIMS.Piece(lstItemsOrdered.Items(i), "^", 2) & "'," & CLSLIMS.Piece(lstItemsOrdered.Items(i), "^", 1) & ",'" & IStatus & "')"
                    Cmd.ExecuteNonQuery()
                Next i
                Cmd.Dispose()
                ConnMySql.Close()
                ClearFields()
            End If
        End If
    End Sub
    Public Function GetStatus(ByVal ord As String) As String
        GetStatus = ""
        Dim SQL As String = "Select Status From Orders " &
                            "Where OrderNum='" & ord & "' And HSEQ=0"
        Using Conn As New MySqlConnection(CLSLIMS.strConMySQL)
            Conn.Open()
            Dim Cmd As New MySqlCommand(SQL, Conn)
            Dim cRS As MySqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetStatus = cRS(0)
            End If
            cRS.Close()
            Conn.Close()
        End Using
    End Function

    Private Sub btnNotes_Click(sender As Object, e As EventArgs) Handles btnNotes.Click
        frmIntCom.Text = "Order Notes"
        frmIntCom.txtIntText.Text = CLSLIMS.Piece(txtNotes.Text, Chr(3), 2)
        CLSLIMS.IntText = ""
        frmIntCom.ShowDialog()
        txtNotes.Text = CHECKSTUFF("ACCTNOTE", txtAcctNo.Text) & vbCrLf & Chr(3) & CLSLIMS.IntText
    End Sub
End Class