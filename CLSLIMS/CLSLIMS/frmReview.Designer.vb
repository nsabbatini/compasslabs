﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReview
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReview))
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lblMBN = New System.Windows.Forms.Label()
        Me.txtMBN = New System.Windows.Forms.TextBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lstBatchSpecNo = New System.Windows.Forms.ListBox()
        Me.ScanPDF = New AxAcroPDFLib.AxAcroPDF()
        Me.ReportPDF = New AxAcroPDFLib.AxAcroPDF()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblSeq = New System.Windows.Forms.Label()
        Me.AWorker = New System.ComponentModel.BackgroundWorker()
        Me.BWorker = New System.ComponentModel.BackgroundWorker()
        Me.CWorker = New System.ComponentModel.BackgroundWorker()
        Me.DWorker = New System.ComponentModel.BackgroundWorker()
        Me.WatchWorker = New System.ComponentModel.BackgroundWorker()
        Me.lblCurrNdx = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFolder = New System.Windows.Forms.TextBox()
        Me.txtTray = New System.Windows.Forms.TextBox()
        Me.btnForward = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnFastForward = New System.Windows.Forms.Button()
        Me.btnFastBack = New System.Windows.Forms.Button()
        Me.btnCom = New System.Windows.Forms.Button()
        CType(Me.ScanPDF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportPDF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnLoad
        '
        Me.btnLoad.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoad.Location = New System.Drawing.Point(577, 26)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(63, 30)
        Me.btnLoad.TabIndex = 0
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'lblMBN
        '
        Me.lblMBN.AutoSize = True
        Me.lblMBN.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMBN.Location = New System.Drawing.Point(28, 26)
        Me.lblMBN.Name = "lblMBN"
        Me.lblMBN.Size = New System.Drawing.Size(52, 19)
        Me.lblMBN.TabIndex = 1
        Me.lblMBN.Text = "MBN:"
        '
        'txtMBN
        '
        Me.txtMBN.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMBN.Location = New System.Drawing.Point(87, 26)
        Me.txtMBN.Name = "txtMBN"
        Me.txtMBN.Size = New System.Drawing.Size(98, 27)
        Me.txtMBN.TabIndex = 0
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(799, 486)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(89, 30)
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lstBatchSpecNo
        '
        Me.lstBatchSpecNo.FormattingEnabled = True
        Me.lstBatchSpecNo.Location = New System.Drawing.Point(799, 573)
        Me.lstBatchSpecNo.Name = "lstBatchSpecNo"
        Me.lstBatchSpecNo.Size = New System.Drawing.Size(73, 30)
        Me.lstBatchSpecNo.TabIndex = 8
        Me.lstBatchSpecNo.Visible = False
        '
        'ScanPDF
        '
        Me.ScanPDF.Enabled = True
        Me.ScanPDF.Location = New System.Drawing.Point(12, 82)
        Me.ScanPDF.Name = "ScanPDF"
        Me.ScanPDF.OcxState = CType(resources.GetObject("ScanPDF.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ScanPDF.Size = New System.Drawing.Size(761, 799)
        Me.ScanPDF.TabIndex = 9
        '
        'ReportPDF
        '
        Me.ReportPDF.Enabled = True
        Me.ReportPDF.Location = New System.Drawing.Point(914, 82)
        Me.ReportPDF.Name = "ReportPDF"
        Me.ReportPDF.OcxState = CType(resources.GetObject("ReportPDF.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ReportPDF.Size = New System.Drawing.Size(761, 799)
        Me.ReportPDF.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(1249, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 19)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "of"
        '
        'lblSeq
        '
        Me.lblSeq.BackColor = System.Drawing.SystemColors.Menu
        Me.lblSeq.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSeq.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeq.Location = New System.Drawing.Point(799, 94)
        Me.lblSeq.Name = "lblSeq"
        Me.lblSeq.Size = New System.Drawing.Size(89, 36)
        Me.lblSeq.TabIndex = 12
        Me.lblSeq.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'AWorker
        '
        Me.AWorker.WorkerReportsProgress = True
        Me.AWorker.WorkerSupportsCancellation = True
        '
        'BWorker
        '
        Me.BWorker.WorkerReportsProgress = True
        Me.BWorker.WorkerSupportsCancellation = True
        '
        'CWorker
        '
        Me.CWorker.WorkerReportsProgress = True
        Me.CWorker.WorkerSupportsCancellation = True
        '
        'DWorker
        '
        Me.DWorker.WorkerReportsProgress = True
        Me.DWorker.WorkerSupportsCancellation = True
        '
        'WatchWorker
        '
        Me.WatchWorker.WorkerReportsProgress = True
        Me.WatchWorker.WorkerSupportsCancellation = True
        '
        'lblCurrNdx
        '
        Me.lblCurrNdx.AutoSize = True
        Me.lblCurrNdx.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrNdx.Location = New System.Drawing.Point(1204, 37)
        Me.lblCurrNdx.Name = "lblCurrNdx"
        Me.lblCurrNdx.Size = New System.Drawing.Size(0, 19)
        Me.lblCurrNdx.TabIndex = 13
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(1289, 37)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(0, 19)
        Me.lblTotal.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(1036, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(162, 19)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Currently Building:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(203, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 19)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Folder:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(377, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 19)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Tray:"
        '
        'txtFolder
        '
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFolder.Location = New System.Drawing.Point(275, 26)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.Size = New System.Drawing.Size(98, 27)
        Me.txtFolder.TabIndex = 18
        '
        'txtTray
        '
        Me.txtTray.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTray.Location = New System.Drawing.Point(435, 26)
        Me.txtTray.Name = "txtTray"
        Me.txtTray.Size = New System.Drawing.Size(122, 27)
        Me.txtTray.TabIndex = 19
        '
        'btnForward
        '
        Me.btnForward.Enabled = False
        Me.btnForward.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnForward.Image = CType(resources.GetObject("btnForward.Image"), System.Drawing.Image)
        Me.btnForward.Location = New System.Drawing.Point(848, 276)
        Me.btnForward.Name = "btnForward"
        Me.btnForward.Size = New System.Drawing.Size(60, 101)
        Me.btnForward.TabIndex = 4
        Me.btnForward.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Enabled = False
        Me.btnBack.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.Image = CType(resources.GetObject("btnBack.Image"), System.Drawing.Image)
        Me.btnBack.Location = New System.Drawing.Point(784, 276)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(60, 101)
        Me.btnBack.TabIndex = 3
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnFastForward
        '
        Me.btnFastForward.Enabled = False
        Me.btnFastForward.Image = Global.CLSLIMS.My.Resources.Resources.fast_forwward_double_right_arrow
        Me.btnFastForward.Location = New System.Drawing.Point(784, 383)
        Me.btnFastForward.Name = "btnFastForward"
        Me.btnFastForward.Size = New System.Drawing.Size(124, 94)
        Me.btnFastForward.TabIndex = 20
        Me.btnFastForward.UseVisualStyleBackColor = True
        '
        'btnFastBack
        '
        Me.btnFastBack.Enabled = False
        Me.btnFastBack.Image = CType(resources.GetObject("btnFastBack.Image"), System.Drawing.Image)
        Me.btnFastBack.Location = New System.Drawing.Point(784, 168)
        Me.btnFastBack.Name = "btnFastBack"
        Me.btnFastBack.Size = New System.Drawing.Size(124, 102)
        Me.btnFastBack.TabIndex = 21
        Me.btnFastBack.UseVisualStyleBackColor = True
        '
        'btnCom
        '
        Me.btnCom.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCom.Location = New System.Drawing.Point(784, 641)
        Me.btnCom.Name = "btnCom"
        Me.btnCom.Size = New System.Drawing.Size(124, 50)
        Me.btnCom.TabIndex = 22
        Me.btnCom.Text = "Internal Comments"
        Me.btnCom.UseVisualStyleBackColor = True
        '
        'frmReview
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1741, 920)
        Me.Controls.Add(Me.btnCom)
        Me.Controls.Add(Me.btnFastBack)
        Me.Controls.Add(Me.btnFastForward)
        Me.Controls.Add(Me.txtTray)
        Me.Controls.Add(Me.txtFolder)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblCurrNdx)
        Me.Controls.Add(Me.lblSeq)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ReportPDF)
        Me.Controls.Add(Me.ScanPDF)
        Me.Controls.Add(Me.lstBatchSpecNo)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnForward)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.txtMBN)
        Me.Controls.Add(Me.lblMBN)
        Me.Controls.Add(Me.btnLoad)
        Me.Name = "frmReview"
        Me.Text = "Review Samples by Batch"
        CType(Me.ScanPDF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportPDF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnLoad As Button
    Friend WithEvents lblMBN As Label
    Friend WithEvents txtMBN As TextBox
    Friend WithEvents btnBack As Button
    Friend WithEvents btnForward As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents lstBatchSpecNo As ListBox
    Friend WithEvents ScanPDF As AxAcroPDF
    Friend WithEvents ReportPDF As AxAcroPDF
    Friend WithEvents Label1 As Label
    Friend WithEvents lblSeq As Label
    Friend WithEvents AWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents BWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents CWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents DWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents WatchWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblCurrNdx As Label
    Friend WithEvents lblTotal As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtFolder As TextBox
    Friend WithEvents txtTray As TextBox
    Friend WithEvents btnFastForward As Button
    Friend WithEvents btnFastBack As Button
    Friend WithEvents btnCom As Button
End Class
