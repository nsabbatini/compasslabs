﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Sql.Database
Imports System.Text
Imports DevExpress.XtraEditors.DXErrorProvider

Public Class frmPurchaseOrderMain
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dtPO As DataTable = New DataTable
    Private _selectItem As String = String.Empty
    Private _subtotal As Double
    Private _total As Double
    Private _ponumber As String
    Private _salestax As Double = 0
    Private _departmentId = 0
    Private _departmentText As String
    Private Sub frmPurchaseOrder_Load(sender As Object, e As EventArgs) Handles Me.Load


        LoadVendorCombo()
        grdVendors.Focus()
        txtPO.Text = GetNextPoNumber()
        grdVendors.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        grdGetItems.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        grdLookup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True

        Dim departments = GetDepartments()

        lookupDepartments.Properties.DataSource = departments
        lookupDepartments.Properties.DisplayMember = "DepartmentName"
        lookupDepartments.Properties.ValueMember = "DepartmentId"
        lookupDepartments.EditValue = _departmentId

        GridView1.OptionsView.ShowGroupPanel = False
        GridView1.OptionsView.ShowIndicator = False

        _dtPO = CreateData()

    End Sub
#Region "Methods"
    Private Function GetDepartments() As Object
        Try

            Using getDepartmentinfo As New Departments()
                _fileName = _fileDir & GetDepartmentsSql
                getDepartmentinfo._sqlText = ReadSqlFile(_fileName)
                Return getDepartmentinfo.GetDepartmentInfo.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderMain.GetDepartments", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Function
    Private Sub LoadVendorCombo()

        Try
            Dim vendors = GetVendorDb()

            grdVendors.Properties.DataSource = vendors
            grdVendors.Properties.DisplayMember = "VendorName"
            grdVendors.Properties.ValueMember = "Id"


        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderMain.LoadVendorCombo", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Sub
    Private Function GetVendorDb() As Object

        Try

            Using getVendors As New GetInventoryDb()
                _fileName = _fileDir & VendorSqlFile
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetVendorInfo.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderMain.GetVendorDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function
    Private Function GetInventoryDb() As Object

        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & InventorySqlItems
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.GetInventory.Where(Function(x) x.VId = grdVendors.EditValue).ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderMain.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function
    Private Function GetNextPoNumber() As String
        _fileName = _fileDir & GetNextPurchaseNumberSql
        Dim pon As String

        Using po As New PurchaseOrders() With {._sqlText = ReadSqlFile(_fileName)}
            _ponumber = po.GetNextPONumber()
            If _ponumber <= 999 Then
                pon = CType("0" & _ponumber, String)
            Else
                pon = _ponumber
            End If

        End Using

        Return pon

    End Function
    Private Function GetClientInformation() As Object

        Try

            Using getInventory As New GetClientInfoDb()

                _fileName = _fileDir & GetClientInformationSql
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.SetClientInfo.ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderMain.GetClientInformation", ex.StackTrace.ToString)
            End Using
        End Try


    End Function
#End Region
#Region "Button Events"
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If Not CheckEdit1.Checked Then
            If grdGetItems.EditValue Is Nothing Then
                grdGetItems.BackColor = Color.Red
                grdGetItems.Focus()
                txtQty.Text = String.Empty
                Exit Sub
            End If
        End If

        If txtQty.Text = String.Empty Then
            txtQty.BackColor = Color.Red
            txtQty.Focus()
            Exit Sub
        End If
        grdGetItems.BackColor = Color.White
        txtQty.BackColor = Color.White
        AddItemsToGrid()

    End Sub
    Private Sub btnRecalculate_Click(sender As Object, e As EventArgs) Handles btnRecalculate.Click
        If Not IsNumeric(txtTax.Text) OrElse txtTax.Text = String.Empty Then
            MsgBox("Must have a tax amount in tax field to re-calculate total amount!", MsgBoxStyle.Critical, "Invalid option")
            Exit Sub
        End If

        lblTotalPo.Text = cTax().ToString("C")


    End Sub
    Private Sub btnCancel_Click_1(sender As Object, e As EventArgs) Handles btnCancel.Click
        If GridView1.DataRowCount >= 1 Then
            Dim x = MessageBox.Show(Me, "You have not saved this PO! Do you want to save before exiting?", "New Purchase Order", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If x = DialogResult.Yes Then
                SavePurchaseOrder()
                ClearTheForm()
            Else
                Me.Close()
            End If
        Else
            Me.Close()
        End If
    End Sub
    Private Sub btnSave_Click_1(sender As Object, e As EventArgs) Handles btnSave.Click
        DxValidationProvider1.Validate()

        If GridView1.DataRowCount = 0 Then
            MessageBox.Show(Me, "You have not entered any items to order!.", "Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If (SavePurchaseOrder()) Then
            MessageBox.Show(Me, "Purchase order has been successfully created!.", "Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Information)
            SalesPerson = txtSalesPerson.Text.Trim()
            QuoteNumber = txtQuoteNumber.Text.Trim()
            Department = _departmentText
            PaymentTerms = txtPaymentTerms.Text()

            If Not chkNeedsApproval.Checked Then
                Using frm As New frmRptViewer
                    frm._ponumber = txtPO.Text.ToString()
                    frm._clientAcct = CustomerAcctNumber
                    frm._subTotal = _subtotal
                    frm._salesTax = _salestax
                    frm._totalSum = CType(lblTotalPo.Text, Double)
                    frm._notes = txtNotes.Text.Trim()
                    frm._needsApproval = chkNeedsApproval.Checked
                    frm._nonInventory = CheckEdit1.Checked
                    If chkShipping.Checked Then
                        frm._IsClient = True
                    Else
                        frm._IsClient = False
                    End If
                    frm._reportType = "PO"
                    frm.ShowDialog()

                End Using
            End If
            ClearTheForm()
        Else
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ClearTheForm()
        End If
    End Sub
#End Region
#Region "Grid Events"
    Private Sub grdVendors_EditValueChanged(sender As Object, e As EventArgs) Handles grdVendors.EditValueChanged
        VendorId = grdVendors.EditValue

        Dim results = GetInventoryDb()


        grdGetItems.Properties.DataSource = results
        grdGetItems.Properties.DisplayMember = "ItemDesc"
        grdGetItems.Properties.ValueMember = "Id"
        grdGetItems.Properties.SearchMode = DevExpress.XtraEditors.Repository.GridLookUpSearchMode.AutoSuggest

    End Sub
    Private Function CreateData() As DataTable

        Dim _dtItems As DataTable = New DataTable
        _dtItems.Columns.Add("ItemDesc", GetType(String))
        _dtItems.Columns.Add("ItemId", GetType(Int16))
        _dtItems.Columns.Add("Qty", GetType(Int16))
        _dtItems.Columns.Add("UnitCost", GetType(Double))
        _dtItems.Columns.Add("Price", GetType(Double))

        Return _dtItems

    End Function
    Private Sub AddItemsToGrid()
        Dim arow As DataRow
        Dim itemLineTotal As Decimal = 0

        itemLineTotal = (CType(txtCost.Text, Double) * CType(txtQty.Text, Int16))



        With _dtPO
            If Not CheckEdit1.Checked Then
                If Not CheckIfAlreadyExits(grdGetItems.EditValue) Then
                    arow = .NewRow
                    arow(0) = _selectItem
                    arow(1) = grdGetItems.EditValue
                    arow(2) = txtQty.Text
                    arow(3) = txtCost.Text
                    arow(4) = itemLineTotal

                    .Rows.Add(arow)
                Else
                    MsgBox("This item already exits in the grid!", MsgBoxStyle.Information, "Item Exists")
                    Exit Sub
                End If
            Else
                arow = .NewRow
                arow(0) = StrConv(txtVendor.Text.Trim(), vbProperCase)
                arow(1) = 0
                arow(2) = txtQty.Text
                arow(3) = txtCost.Text
                arow(4) = itemLineTotal

                .Rows.Add(arow)

            End If

        End With

        _subtotal += itemLineTotal

        lblSubTotal.Text = _subtotal.ToString("C")

        lblTotalPo.Text = _subtotal.ToString("C")


        grdOrders.DataSource = _dtPO.DefaultView


    End Sub
    Private Function CheckIfAlreadyExits(ByVal theId As Int16) As Boolean

        For i = 0 To GridView1.DataRowCount - 1
            If theId = CType(GridView1.GetRowCellValue(i, "ItemId"), Int16) Then
                Return True
            End If

        Next

    End Function
    Private Sub grdGetItems_EditValueChanged(sender As Object, e As EventArgs) Handles grdGetItems.EditValueChanged
        Dim arow = grdGetItems.GetSelectedDataRow()
        If arow Is Nothing Then
            Exit Sub
        End If

        txtCost.Text = arow.GetType().GetProperty("ItemVendorCost")?.GetValue(arow)
        _selectItem = arow.GetType().GetProperty("ItemDesc")?.GetValue(arow) & " - " & arow.GetType().GetProperty("Vendoritemid")?.GetValue(arow)
        txtQty.Text = String.Empty
        txtQty.Focus()

    End Sub
    Private Sub grdLookup_EditValueChanged(sender As Object, e As EventArgs) Handles grdLookup.EditValueChanged
        Dim arow = grdLookup.GetSelectedDataRow

        If arow Is Nothing Then
            Exit Sub
        End If

        CustomerAcctNumber = arow.GetType().GetProperty("Acct")?.GetValue(arow)
        CustomerBusinessName = CustomerAcctNumber & " - " & arow.GetType().GetProperty("Aname")?.GetValue(arow)

    End Sub
#End Region
#Region "Tax Calculation"
    Private Function cTax() As Double
        _salestax = _subtotal * (txtTax.Text / 100)

        cTax = (_subtotal + _salestax)

    End Function
#End Region
#Region "Form Cleanup"
    Private Sub ClearTheForm()

        If chkShipping.Checked Then
            grdLookup.EditValue = Nothing
            grdLookup.SelectedText = String.Empty
            grdLookup.Visible = False
            chkShipping.Checked = False
            lblClient.Visible = False
        End If

        txtPO.Text = String.Empty
        grdVendors.EditValue = Nothing
        grdVendors.SelectedText = String.Empty
        grdGetItems.EditValue = Nothing
        grdGetItems.SelectedText = String.Empty
        txtQty.Text = String.Empty
        txtCost.Text = String.Empty
        txtNotes.Text = String.Empty
        txtTax.Text = String.Empty
        txtSalesPerson.Text = String.Empty
        txtQuoteNumber.Text = String.Empty
        txtPaymentTerms.Text = String.Empty
        SalesPerson = String.Empty
        Department = String.Empty
        QuoteNumber = String.Empty
        PaymentTerms = String.Empty
        lblSubTotal.Text = String.Empty
        lblTotalPo.Text = String.Empty
        _dtPO.Clear()
        _dtPO = CreateData()
        LoadVendorCombo()
        txtPO.Text = GetNextPoNumber()
        grdVendors.Focus()
        _subtotal = 0
        _total = 0
        _salestax = 0
        CustomerAcctNumber = String.Empty
        CustomerBusinessName = String.Empty
        chkNeedsApproval.Checked = False
        txtVendor.Text = ""
        txtItem.Text = ""
        CheckEdit1.Checked = False
        txtItem.Visible = False
        txtVendor.Visible = False


    End Sub
#End Region
#Region "Click Events"
    Private Sub chkShipping_CheckedChanged(sender As Object, e As EventArgs) Handles chkShipping.CheckedChanged
        If chkShipping.Checked Then
            grdLookup.Visible = True
            lblClient.Visible = True

            Dim client = GetClientInformation()

            grdLookup.Properties.DataSource = client
            grdLookup.Properties.DisplayMember = "Aname"
            grdLookup.Properties.ValueMember = "Acct"

        Else
            grdLookup.Visible = False
            lblClient.Visible = False
        End If
    End Sub
    Private Sub txtQty_KeyDown(sender As Object, e As KeyEventArgs) Handles txtQty.KeyDown
        If e.KeyCode = Keys.Enter Then
            If grdGetItems.EditValue Is Nothing Then
                grdGetItems.BackColor = Color.Red
                grdGetItems.Focus()
                txtQty.Text = String.Empty
                Exit Sub
            End If
            If txtQty.Text = String.Empty Then
                txtQty.BackColor = Color.Red
                txtQty.Focus()
                Exit Sub
            End If
            grdGetItems.BackColor = Color.White
            txtQty.BackColor = Color.White
            AddItemsToGrid()
            grdGetItems.EditValue = Nothing
            grdGetItems.SelectedText = String.Empty
            txtQty.Text = String.Empty
            grdGetItems.Focus()

        End If
    End Sub
#End Region
#Region "Save Data Events"
    Private Function SavePurchaseOrder() As Boolean

        _fileName = _fileDir & InsertPoSql

        Try
            Using pos As New PurchaseOrders() With {
             ._sqlText = ReadSqlFile(_fileName),
             ._poNumber = _ponumber,
             ._department = lookupDepartments.EditValue,
             ._terms = txtTerms.Text.Trim(),
             ._notes = txtNotes.Text,
             ._total = CType(lblTotalPo.Text, Double),
             ._salesTax = _salestax,
             ._salesPerson = txtSalesPerson.Text.Trim(),
             ._quoteNumber = txtQuoteNumber.Text.Trim(),
             ._paymentTerms = txtPaymentTerms.Text.Trim(),
             ._needsapproval = IIf(chkNeedsApproval.Checked, 1, 0),
             ._nonInventory = IIf(CheckEdit1.Checked, 1, 0)
             }
                If chkShipping.Checked Then
                    pos._shipTo = grdLookup.EditValue
                Else
                    'Compass labs
                    pos._shipTo = 1
                End If
                If Not CheckEdit1.Checked Then
                    pos._vendorId = grdVendors.EditValue
                Else
                    pos._vendorId = 0
                End If
                If pos.InsertPurchaseOrder() Then
                    _fileName = _fileDir & InsertPoDetailsSql

                    Using posdetails As New PurchaseOrders() With {._sqlText = ReadSqlFile(_fileName)}
                        For i = 0 To GridView1.DataRowCount - 1
                            posdetails._itemId = CType(GridView1.GetRowCellValue(i, "ItemId"), Int16)
                            posdetails._lineItemCost = CType(GridView1.GetRowCellValue(i, "Price"), Double)
                            posdetails._qty = CType(GridView1.GetRowCellValue(i, "Qty"), Int16)
                            posdetails._unitCost = CType(GridView1.GetRowCellValue(i, "UnitCost"), Double)
                            posdetails._itemDescription = CType(GridView1.GetRowCellValue(i, "ItemDesc"), String)

                            posdetails.InsertPurchaseOrderDetails()
                        Next
                    End Using
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderMain.SavePurchaseOrder", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True


    End Function

    Private Sub HyperLinkEdit1_OpenLink(sender As Object, e As DevExpress.XtraEditors.Controls.OpenLinkEventArgs) Handles HyperLinkEdit1.OpenLink
        Using frm As New frmEditVendors
            frm.ShowDialog()
            LoadVendorCombo()

        End Using
    End Sub

    Private Sub CheckEdit1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEdit1.CheckedChanged
        If CheckEdit1.Checked Then
            txtVendor.Visible = True
            txtItem.Visible = True
            txtItem.TabIndex = 6
        Else
            txtVendor.Visible = False
            txtItem.Visible = False
        End If
    End Sub

    Private Sub lookupDepartments_EditValueChanged(sender As Object, e As EventArgs) Handles lookupDepartments.EditValueChanged
        Dim arow = lookupDepartments.GetSelectedDataRow()
        If arow Is Nothing Then
            Exit Sub
        End If

        _departmentText = arow.GetType().GetProperty("DepartmentName")?.GetValue(arow)

    End Sub

    Private Sub RepositoryItemButtonEdit1_Click(sender As Object, e As EventArgs) Handles RepositoryItemButtonEdit1.Click
        GridView1.DeleteSelectedRows()
    End Sub
#End Region
End Class