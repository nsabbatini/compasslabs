﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmRevOCLkup
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnLkUp.Click
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = conn
        Dim rs As SqlDataReader
        Dim strTstCode As String = ""
        Dim Screen As Boolean = False, Conf As Boolean = False
        Screen = rbScreening.Checked : Conf = rbConf.Checked
        Dim TCcnt As Integer = 0, AtLeastOne As Boolean = False
        lbDisplayResults.Items.Clear()
        If Not Screen And Not Conf Then
            lbDisplayResults.Items.Add("Must select Screening or Confirmation")
            Exit Sub
        End If
        TCcnt = 0
        If Screen Then
            If cbAlc.Checked Then
                strTstCode = strTstCode & "'208020',"
                TCcnt = TCcnt + 1
            End If
            If cbAmp.Checked Then
                strTstCode = strTstCode & "'201010','307110',"
                TCcnt = TCcnt + 2
            End If
            If cbADSer.Checked Then
                strTstCode = strTstCode & "'301100','301120','301130','301110','301080',"
                TCcnt = TCcnt + 5
            End If
            If cbADTri.Checked Then
                strTstCode = strTstCode & "'301170','301160','301150',"
                TCcnt = TCcnt + 3
            End If
            If cbAD.Checked Then
                strTstCode = strTstCode & "'301180','301140','301090',"
                TCcnt = TCcnt + 3
            End If
            If cbAntiEp.Checked Then
                strTstCode = strTstCode & "'301240',"
                TCcnt = TCcnt + 1
            End If
            If cbAntiPys.Checked Then
                strTstCode = strTstCode & "'301230','301210','301250','301190','301200',"
                TCcnt = TCcnt + 5
            End If
            If cbBarb.Checked Then
                strTstCode = strTstCode & "'202010',"
                TCcnt = TCcnt + 1
            End If
            If cbBenz.Checked Then
                strTstCode = strTstCode & "'203010','303070',"
                TCcnt = TCcnt + 2
            End If
            If cbBup.Checked Then
                strTstCode = strTstCode & "'205030',"
                TCcnt = TCcnt + 1
            End If
            If cbTHC.Checked Then
                strTstCode = strTstCode & "'206015',"
                TCcnt = TCcnt + 1
            End If
            If cbCOC.Checked Then
                strTstCode = strTstCode & "'206020',"
                TCcnt = TCcnt + 1
            End If
            If cbFent.Checked Then
                strTstCode = strTstCode & "'305020','305070','305110',"
                TCcnt = TCcnt + 3
            End If
            If cbGab.Checked Then
                strTstCode = strTstCode & "'307040',"
                TCcnt = TCcnt + 1
            End If
            If cbHero.Checked Then
                strTstCode = strTstCode & "'204010',"
                TCcnt = TCcnt + 1
            End If
            If cbMtd.Checked Then
                strTstCode = strTstCode & "'205010','205020',"
                TCcnt = TCcnt + 2
            End If
            If cbMethAmp.Checked Then
                strTstCode = strTstCode & "'201020',"
                TCcnt = TCcnt + 1
            End If
            If cbMethy.Checked Then
                strTstCode = strTstCode & "'306040','306050',"
                TCcnt = TCcnt + 2
            End If
            If cbNic.Checked Then
                strTstCode = strTstCode & "'207020',"
                TCcnt = TCcnt + 1
            End If
            If cbOpi.Checked Then
                strTstCode = strTstCode & "'204010',"
                TCcnt = TCcnt + 1
            End If
            If cbOpiAnal.Checked Then
                strTstCode = strTstCode & "'305030','305080','305050','305060',"
                TCcnt = TCcnt + 4
            End If
            If cbOxy.Checked Then
                strTstCode = strTstCode & "'204020',"
                TCcnt = TCcnt + 1
            End If
            If cbPhen.Checked Then
                strTstCode = strTstCode & "'206030',"
                TCcnt = TCcnt + 1
            End If
            If cbPreGab.Checked Then
                strTstCode = strTstCode & "'307070',"
                TCcnt = TCcnt + 1
            End If
            If cbPro.Checked Then
                strTstCode = strTstCode & "'205060',"
                TCcnt = TCcnt + 1
            End If
            If cbSedHyp.Checked Then
                strTstCode = strTstCode & "'309030',"
                TCcnt = TCcnt + 1
            End If
            If cbSkel.Checked Then
                strTstCode = strTstCode & "'204030','303130',"
                TCcnt = TCcnt + 2
            End If
            If cbTap.Checked Then
                strTstCode = strTstCode & "'305130',"
                TCcnt = TCcnt + 1
            End If
            If cbTram.Checked Then
                strTstCode = strTstCode & "'305120','303140',"
                TCcnt = TCcnt + 2
            End If
        ElseIf Conf Then
            If cbAlc.Checked Then
                strTstCode = strTstCode & "'308010','308020',"
                TCcnt = TCcnt + 2
            End If
            If cbAmp.Checked Then
                strTstCode = strTstCode & "'301010','301020','307110',"
                TCcnt = TCcnt + 3
            End If
            If cbADSer.Checked Then
                strTstCode = strTstCode & "'301100','301120','301130','301110','301080',"
                TCcnt = TCcnt + 5
            End If
            If cbADTri.Checked Then
                strTstCode = strTstCode & "'301170','301160','301150',"
                TCcnt = TCcnt + 3
            End If
            If cbAD.Checked Then
                strTstCode = strTstCode & "'301180','301140','301090',"
                TCcnt = TCcnt + 3
            End If
            If cbAntiEp.Checked Then
                strTstCode = strTstCode & "'301240',"
                TCcnt = TCcnt + 1
            End If
            If cbAntiPys.Checked Then
                strTstCode = strTstCode & "'301230','301210','301250','301190','301200',"
                TCcnt = TCcnt + 5
            End If
            If cbBarb.Checked Then
                strTstCode = strTstCode & "'302050','302060','302070',"
                TCcnt = TCcnt + 3
            End If
            If cbBenz.Checked Then
                strTstCode = strTstCode & "'303010','303020','303030','303120','303040','303090','303070','303080','307100','303100','303110','307080','307090',"
                TCcnt = TCcnt + 13
            End If
            If cbBup.Checked Then
                strTstCode = strTstCode & "'307050','307060',"
                TCcnt = TCcnt + 2
            End If
            If cbTHC.Checked Then
                strTstCode = strTstCode & "'302080',"
                TCcnt = TCcnt + 1
            End If
            If cbCOC.Checked Then
                strTstCode = strTstCode & "'306020',"
                TCcnt = TCcnt + 1
            End If
            If cbFent.Checked Then
                strTstCode = strTstCode & "'305020','305070','305110',"
                TCcnt = TCcnt + 3
            End If
            If cbGab.Checked Then
                strTstCode = strTstCode & "'307040',"
                TCcnt = TCcnt + 1
            End If
            If cbHero.Checked Then
                strTstCode = strTstCode & "'306010',"
                TCcnt = TCcnt + 1
            End If
            If cbMtd.Checked Then
                strTstCode = strTstCode & "'305040','305010',"
                TCcnt = TCcnt + 2
            End If
            If cbMethAmp.Checked Then
                strTstCode = strTstCode & "'301030','301040','301050',"
                TCcnt = TCcnt + 3
            End If
            If cbMethy.Checked Then
                strTstCode = strTstCode & "'306040','306050',"
                TCcnt = TCcnt + 2
            End If
            If cbNic.Checked Then
                strTstCode = strTstCode & "'307010',"
                TCcnt = TCcnt + 1
            End If
            If cbOpi.Checked Then
                strTstCode = strTstCode & "'304010','304020','304030','304040','301070',"
                TCcnt = TCcnt + 5
            End If
            If cbOpiAnal.Checked Then
                strTstCode = strTstCode & "'305030','305080','305050','305060',"
                TCcnt = TCcnt + 4
            End If
            If cbOxy.Checked Then
                strTstCode = strTstCode & "'304050','304060','301060',"
                TCcnt = TCcnt + 3
            End If
            If cbPhen.Checked Then
                strTstCode = strTstCode & "'306030',"
                TCcnt = TCcnt + 1
            End If
            If cbPreGab.Checked Then
                strTstCode = strTstCode & "'307070',"
                TCcnt = TCcnt + 1
            End If
            If cbPro.Checked Then
                strTstCode = strTstCode & "'305100',"
                TCcnt = TCcnt + 1
            End If
            If cbSedHyp.Checked Then
                strTstCode = strTstCode & "'309030',"
                TCcnt = TCcnt + 1
            End If
            If cbSkel.Checked Then
                strTstCode = strTstCode & "'302010','303130','302020',"
                TCcnt = TCcnt + 3
            End If
            If cbTap.Checked Then
                strTstCode = strTstCode & "'305130',"
                TCcnt = TCcnt + 1
            End If
            If cbTram.Checked Then
                strTstCode = strTstCode & "'305120','303140',"
                TCcnt = TCcnt + 2
            End If
        End If
        strTstCode = Mid(strTstCode, 1, Len(strTstCode) - 1)

        Cmd.CommandText = "Select A.OC, ONAME,Count(*) From OC A, OCTC B Where A.OC=B.OC And A.OEFDT=B.OEFDT And OACT=1 And TC In (" & strTstCode & ") " &
                          "And (Select Count(TC) From OCTC Where OC=A.OC And OEFDT=A.OEFDT)=" & TCcnt & " " &
                          "Group By a.OC, ONAME Having Count(*)=" & TCcnt
        rs = Cmd.ExecuteReader
        AtLeastOne = False
        Do While rs.Read
            AtLeastOne = True
            lbDisplayResults.BackColor = Color.LightGreen
            lbDisplayResults.ForeColor = Color.Black
            lbDisplayResults.Items.Add(rs(0) & " - " & rs(1))
        Loop
        rs.Close()
        If Not AtLeastOne Then
            lbDisplayResults.BackColor = Color.IndianRed
            lbDisplayResults.ForeColor = Color.White
            lbDisplayResults.Items.Add("No matching ordering codes.")

        End If
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub cbAD_Click(sender As Object, e As EventArgs) Handles cbAD.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub
    Private Sub cbADSer_Click(sender As Object, e As EventArgs) Handles cbADSer.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbADTri_Click(sender As Object, e As EventArgs) Handles cbADTri.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbAlc_Click(sender As Object, e As EventArgs) Handles cbAlc.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbAmp_Click(sender As Object, e As EventArgs) Handles cbAmp.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbAntiEp_Click(sender As Object, e As EventArgs) Handles cbAntiEp.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbAntiPys_Click(sender As Object, e As EventArgs) Handles cbAntiPys.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbBarb_Click(sender As Object, e As EventArgs) Handles cbBarb.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbBenz_Click(sender As Object, e As EventArgs) Handles cbBenz.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbBup_Click(sender As Object, e As EventArgs) Handles cbBup.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbCOC_Click(sender As Object, e As EventArgs) Handles cbCOC.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbFent_Click(sender As Object, e As EventArgs) Handles cbFent.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbGab_Click(sender As Object, e As EventArgs) Handles cbGab.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbHero_Click(sender As Object, e As EventArgs) Handles cbHero.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbMethAmp_Click(sender As Object, e As EventArgs) Handles cbMethAmp.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbMethy_Click(sender As Object, e As EventArgs) Handles cbMethy.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbMtd_Click(sender As Object, e As EventArgs) Handles cbMtd.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbNic_Click(sender As Object, e As EventArgs) Handles cbNic.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbOpi_Click(sender As Object, e As EventArgs) Handles cbOpi.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbOpiAnal_Click(sender As Object, e As EventArgs) Handles cbOpiAnal.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbOxy_Click(sender As Object, e As EventArgs) Handles cbOxy.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbPhen_Click(sender As Object, e As EventArgs) Handles cbPhen.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbPreGab_Click(sender As Object, e As EventArgs) Handles cbPreGab.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbPro_Click(sender As Object, e As EventArgs) Handles cbPro.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbSedHyp_Click(sender As Object, e As EventArgs) Handles cbSedHyp.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbSkel_Click(sender As Object, e As EventArgs) Handles cbSkel.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbTap_Click(sender As Object, e As EventArgs) Handles cbTap.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbTHC_Click(sender As Object, e As EventArgs) Handles cbTHC.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub cbTram_Click(sender As Object, e As EventArgs) Handles cbTram.Click
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbAD.Checked = False
        cbADSer.Checked = False
        cbADTri.Checked = False
        cbAlc.Checked = False
        cbAmp.Checked = False
        cbAntiEp.Checked = False
        cbAntiPys.Checked = False
        cbBarb.Checked = False
        cbBenz.Checked = False
        cbBup.Checked = False
        cbCOC.Checked = False
        cbFent.Checked = False
        cbGab.Checked = False
        cbHero.Checked = False
        cbMethAmp.Checked = False
        cbMethy.Checked = False
        cbMtd.Checked = False
        cbNic.Checked = False
        cbOpi.Checked = False
        cbOpiAnal.Checked = False
        cbOxy.Checked = False
        cbPhen.Checked = False
        cbPreGab.Checked = False
        cbPro.Checked = False
        cbSedHyp.Checked = False
        cbSkel.Checked = False
        cbTap.Checked = False
        cbTHC.Checked = False
        cbTram.Checked = False
        lbDisplayResults.Items.Clear()
        lbDisplayResults.BackColor = Color.LightGray
        lbDisplayResults.ForeColor = Color.White
    End Sub
End Class