﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmReconcilePo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReconcilePo))
        Me.grdLookup = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.pnlNewItems = New DevExpress.XtraEditors.PanelControl()
        Me.btnNewItemRow = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNewitem = New DevExpress.XtraEditors.TextEdit()
        Me.grdGetItems = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnNewRow = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.grdPODetails = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.chkNotInventory = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.grdLookup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.pnlNewItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNewItems.SuspendLayout()
        CType(Me.txtNewitem.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdGetItems.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdPODetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNotInventory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdLookup
        '
        Me.grdLookup.Location = New System.Drawing.Point(12, 41)
        Me.grdLookup.Name = "grdLookup"
        Me.grdLookup.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.grdLookup.Properties.Appearance.Options.UseFont = True
        Me.grdLookup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdLookup.Properties.PopupView = Me.GridLookUpEdit1View
        Me.grdLookup.Size = New System.Drawing.Size(207, 30)
        Me.grdLookup.TabIndex = 0
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "PO #"
        Me.GridColumn1.FieldName = "PoNumber"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "GridColumn2"
        Me.GridColumn2.FieldName = "PId"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.pnlNewItems)
        Me.GroupControl1.Controls.Add(Me.btnNewRow)
        Me.GroupControl1.Controls.Add(Me.btnCancel)
        Me.GroupControl1.Controls.Add(Me.btnSave)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl1.Location = New System.Drawing.Point(0, 460)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(804, 160)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Actions"
        '
        'pnlNewItems
        '
        Me.pnlNewItems.Controls.Add(Me.btnNewItemRow)
        Me.pnlNewItems.Controls.Add(Me.LabelControl3)
        Me.pnlNewItems.Controls.Add(Me.LabelControl2)
        Me.pnlNewItems.Controls.Add(Me.txtNewitem)
        Me.pnlNewItems.Controls.Add(Me.grdGetItems)
        Me.pnlNewItems.Location = New System.Drawing.Point(5, 66)
        Me.pnlNewItems.Name = "pnlNewItems"
        Me.pnlNewItems.Size = New System.Drawing.Size(787, 89)
        Me.pnlNewItems.TabIndex = 3
        Me.pnlNewItems.Visible = False
        '
        'btnNewItemRow
        '
        Me.btnNewItemRow.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.btnNewItemRow.Appearance.Options.UseFont = True
        Me.btnNewItemRow.ImageOptions.SvgImage = CType(resources.GetObject("btnNewItemRow.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnNewItemRow.Location = New System.Drawing.Point(475, 14)
        Me.btnNewItemRow.Name = "btnNewItemRow"
        Me.btnNewItemRow.Size = New System.Drawing.Size(151, 46)
        Me.btnNewItemRow.TabIndex = 11
        Me.btnNewItemRow.Text = "Add New Item"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(357, 14)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = "Quantity Received"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 14)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl2.TabIndex = 9
        Me.LabelControl2.Text = "Select Item to add"
        '
        'txtNewitem
        '
        Me.txtNewitem.Location = New System.Drawing.Point(357, 34)
        Me.txtNewitem.Name = "txtNewitem"
        Me.txtNewitem.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtNewitem.Properties.Appearance.Options.UseFont = True
        Me.txtNewitem.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNewitem.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtNewitem.Size = New System.Drawing.Size(94, 26)
        Me.txtNewitem.TabIndex = 8
        '
        'grdGetItems
        '
        Me.grdGetItems.Location = New System.Drawing.Point(5, 34)
        Me.grdGetItems.Name = "grdGetItems"
        Me.grdGetItems.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.grdGetItems.Properties.Appearance.Options.UseFont = True
        Me.grdGetItems.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdGetItems.Properties.PopupView = Me.GridView2
        Me.grdGetItems.Size = New System.Drawing.Size(330, 26)
        Me.grdGetItems.TabIndex = 7
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.GridColumn4})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Item"
        Me.GridColumn3.FieldName = "ItemDesc"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Id"
        Me.GridColumn4.FieldName = "ItemId"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'btnNewRow
        '
        Me.btnNewRow.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnNewRow.Appearance.Options.UseFont = True
        Me.btnNewRow.ImageOptions.SvgImage = CType(resources.GetObject("btnNewRow.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnNewRow.Location = New System.Drawing.Point(12, 26)
        Me.btnNewRow.Name = "btnNewRow"
        Me.btnNewRow.Size = New System.Drawing.Size(187, 34)
        Me.btnNewRow.TabIndex = 2
        Me.btnNewRow.Text = "Add Item to P.O."
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.ImageOptions.SvgImage = CType(resources.GetObject("btnCancel.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnCancel.Location = New System.Drawing.Point(682, 26)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(110, 34)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Exit"
        '
        'btnSave
        '
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.ImageOptions.SvgImage = CType(resources.GetObject("btnSave.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnSave.Location = New System.Drawing.Point(545, 26)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(110, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(99, 23)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Select PO #"
        '
        'grdPODetails
        '
        Me.grdPODetails.Location = New System.Drawing.Point(12, 77)
        Me.grdPODetails.MainView = Me.GridView1
        Me.grdPODetails.Name = "grdPODetails"
        Me.grdPODetails.Size = New System.Drawing.Size(780, 377)
        Me.grdPODetails.TabIndex = 4
        Me.grdPODetails.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.grdPODetails
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView1.OptionsDetail.EnableMasterViewMode = False
        Me.GridView1.OptionsFind.AlwaysVisible = True
        Me.GridView1.OptionsMenu.ShowGroupSummaryEditorItem = True
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsSelection.ShowCheckBoxSelectorInPrintExport = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsView.AllowHtmlDrawHeaders = True
        '
        'chkNotInventory
        '
        Me.chkNotInventory.Location = New System.Drawing.Point(242, 49)
        Me.chkNotInventory.Name = "chkNotInventory"
        Me.chkNotInventory.Properties.Caption = "Check if non inventory item"
        Me.chkNotInventory.Size = New System.Drawing.Size(181, 20)
        Me.chkNotInventory.TabIndex = 5
        '
        'frmReconcilePo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(804, 620)
        Me.Controls.Add(Me.chkNotInventory)
        Me.Controls.Add(Me.grdPODetails)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.grdLookup)
        Me.Name = "frmReconcilePo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reconcile Purchase Orders"
        CType(Me.grdLookup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.pnlNewItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNewItems.ResumeLayout(False)
        Me.pnlNewItems.PerformLayout()
        CType(Me.txtNewitem.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdGetItems.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdPODetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNotInventory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grdLookup As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdPODetails As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnNewRow As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pnlNewItems As DevExpress.XtraEditors.PanelControl
    Friend WithEvents grdGetItems As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnNewItemRow As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNewitem As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkNotInventory As DevExpress.XtraEditors.CheckEdit
End Class
