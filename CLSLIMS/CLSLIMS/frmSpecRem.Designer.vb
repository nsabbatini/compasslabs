﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSpecRem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.txtSpecNo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnRem = New System.Windows.Forms.Button()
        Me.rbAll = New System.Windows.Forms.RadioButton()
        Me.rbSel = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'lstInfo
        '
        Me.lstInfo.Font = New System.Drawing.Font("Courier New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.FormattingEnabled = True
        Me.lstInfo.ItemHeight = 22
        Me.lstInfo.Location = New System.Drawing.Point(18, 139)
        Me.lstInfo.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(493, 202)
        Me.lstInfo.TabIndex = 11
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(14, 69)
        Me.lblName.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(0, 19)
        Me.lblName.TabIndex = 10
        '
        'txtSpecNo
        '
        Me.txtSpecNo.Location = New System.Drawing.Point(117, 23)
        Me.txtSpecNo.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.txtSpecNo.Name = "txtSpecNo"
        Me.txtSpecNo.Size = New System.Drawing.Size(130, 27)
        Me.txtSpecNo.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 23)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 19)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Specimen:"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(357, 361)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(123, 31)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnRem
        '
        Me.btnRem.Location = New System.Drawing.Point(61, 361)
        Me.btnRem.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.btnRem.Name = "btnRem"
        Me.btnRem.Size = New System.Drawing.Size(132, 31)
        Me.btnRem.TabIndex = 8
        Me.btnRem.Text = "Remove"
        Me.btnRem.UseVisualStyleBackColor = True
        '
        'rbAll
        '
        Me.rbAll.AutoSize = True
        Me.rbAll.Location = New System.Drawing.Point(61, 109)
        Me.rbAll.Name = "rbAll"
        Me.rbAll.Size = New System.Drawing.Size(130, 23)
        Me.rbAll.TabIndex = 12
        Me.rbAll.TabStop = True
        Me.rbAll.Text = "All Worklists"
        Me.rbAll.UseVisualStyleBackColor = True
        '
        'rbSel
        '
        Me.rbSel.AutoSize = True
        Me.rbSel.Location = New System.Drawing.Point(287, 109)
        Me.rbSel.Name = "rbSel"
        Me.rbSel.Size = New System.Drawing.Size(170, 23)
        Me.rbSel.TabIndex = 13
        Me.rbSel.TabStop = True
        Me.rbSel.Text = "Selected Worklist"
        Me.rbSel.UseVisualStyleBackColor = True
        '
        'frmSpecRem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(525, 414)
        Me.Controls.Add(Me.rbSel)
        Me.Controls.Add(Me.rbAll)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtSpecNo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRem)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmSpecRem"
        Me.Text = "frmSpecRem"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstInfo As ListBox
    Friend WithEvents lblName As Label
    Friend WithEvents txtSpecNo As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnExit As Button
    Friend WithEvents btnRem As Button
    Friend WithEvents rbAll As RadioButton
    Friend WithEvents rbSel As RadioButton
End Class
