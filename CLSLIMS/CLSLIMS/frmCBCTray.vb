﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmCBCTray

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim Sel As String = "", Fnd As Boolean, CBtxt As String = ""
        Dim printer As New PCPrint()
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        txtTray.Text = UCase(txtTray.Text)
        Dim txt As String = "CBCs in Tray: " & txtTray.Text & vbCrLf & vbCrLf

        If txtTray.Text = "" Then
            MsgBox("No Tray")
            Exit Sub
        End If

        Sel = "Select Distinct A.PSPECNO From XTRAYS A, POCTC B Where TRAY='" & txtTray.Text & "' And A.PSPECNO <> 'HOLDER' And A.PSPECNO=B.PSPECNO And TC In ('1700','4015')"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Fnd = False
        Do While rs.Read()
            lstCBC.Items.Add(rs(0))
            Fnd = True
        Loop
        rs.Close()
        If Not Fnd Then
            MsgBox("Tray not found")
        Else
            'Print List box
            'Set the font we want to use
            printer.PrinterFont = New Font("Courier New", 10)
            'Set Margins
            printer.DefaultPageSettings.Margins = mar

            For i = 0 To lstCBC.Items.Count - 1
                txt = txt & vbCrLf & "  []  " & lstCBC.Items(i)
            Next i
            printer.TextToPrint = " " & txt
            'Issue print command
            printer.Print()

            Me.Close()
            Me.Dispose()
        End If
    End Sub
End Class