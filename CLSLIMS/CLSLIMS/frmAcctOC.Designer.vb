﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAcctOC
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbAntiPys = New System.Windows.Forms.CheckBox()
        Me.rbConf = New System.Windows.Forms.RadioButton()
        Me.rbScreening = New System.Windows.Forms.RadioButton()
        Me.cbTram = New System.Windows.Forms.CheckBox()
        Me.cbTap = New System.Windows.Forms.CheckBox()
        Me.cbSkel = New System.Windows.Forms.CheckBox()
        Me.cbSedHyp = New System.Windows.Forms.CheckBox()
        Me.cbPro = New System.Windows.Forms.CheckBox()
        Me.cbPreGab = New System.Windows.Forms.CheckBox()
        Me.cbPhen = New System.Windows.Forms.CheckBox()
        Me.cbOxy = New System.Windows.Forms.CheckBox()
        Me.cbOpiAnal = New System.Windows.Forms.CheckBox()
        Me.cbOpi = New System.Windows.Forms.CheckBox()
        Me.cbNic = New System.Windows.Forms.CheckBox()
        Me.cbMethy = New System.Windows.Forms.CheckBox()
        Me.cbMethAmp = New System.Windows.Forms.CheckBox()
        Me.cbMtd = New System.Windows.Forms.CheckBox()
        Me.cbHero = New System.Windows.Forms.CheckBox()
        Me.cbGab = New System.Windows.Forms.CheckBox()
        Me.cbFent = New System.Windows.Forms.CheckBox()
        Me.cbCOC = New System.Windows.Forms.CheckBox()
        Me.cbTHC = New System.Windows.Forms.CheckBox()
        Me.cbBup = New System.Windows.Forms.CheckBox()
        Me.cbBenz = New System.Windows.Forms.CheckBox()
        Me.cbBarb = New System.Windows.Forms.CheckBox()
        Me.cbAntiEp = New System.Windows.Forms.CheckBox()
        Me.cbAD = New System.Windows.Forms.CheckBox()
        Me.cbADTri = New System.Windows.Forms.CheckBox()
        Me.cbADSer = New System.Windows.Forms.CheckBox()
        Me.cbAmp = New System.Windows.Forms.CheckBox()
        Me.cbAlc = New System.Windows.Forms.CheckBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.lblAcct = New System.Windows.Forms.Label()
        Me.cbXREF = New System.Windows.Forms.CheckBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpProfDt = New System.Windows.Forms.DateTimePicker()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.cbChiral = New System.Windows.Forms.CheckBox()
        Me.cbAlk = New System.Windows.Forms.CheckBox()
        Me.cbLevo = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'cbAntiPys
        '
        Me.cbAntiPys.AutoSize = True
        Me.cbAntiPys.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntiPys.Location = New System.Drawing.Point(22, 264)
        Me.cbAntiPys.Name = "cbAntiPys"
        Me.cbAntiPys.Size = New System.Drawing.Size(123, 20)
        Me.cbAntiPys.TabIndex = 64
        Me.cbAntiPys.Text = "Antipsychotics"
        Me.cbAntiPys.UseVisualStyleBackColor = True
        '
        'rbConf
        '
        Me.rbConf.AutoSize = True
        Me.rbConf.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbConf.Location = New System.Drawing.Point(21, 343)
        Me.rbConf.Name = "rbConf"
        Me.rbConf.Size = New System.Drawing.Size(141, 23)
        Me.rbConf.TabIndex = 63
        Me.rbConf.TabStop = True
        Me.rbConf.Text = "Confirmations"
        Me.rbConf.UseVisualStyleBackColor = True
        '
        'rbScreening
        '
        Me.rbScreening.AutoSize = True
        Me.rbScreening.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbScreening.Location = New System.Drawing.Point(21, 323)
        Me.rbScreening.Name = "rbScreening"
        Me.rbScreening.Size = New System.Drawing.Size(107, 23)
        Me.rbScreening.TabIndex = 62
        Me.rbScreening.TabStop = True
        Me.rbScreening.Text = "Screening"
        Me.rbScreening.UseVisualStyleBackColor = True
        '
        'cbTram
        '
        Me.cbTram.AutoSize = True
        Me.cbTram.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTram.Location = New System.Drawing.Point(518, 316)
        Me.cbTram.Name = "cbTram"
        Me.cbTram.Size = New System.Drawing.Size(86, 20)
        Me.cbTram.TabIndex = 60
        Me.cbTram.Text = "Tramadol"
        Me.cbTram.UseVisualStyleBackColor = True
        '
        'cbTap
        '
        Me.cbTap.AutoSize = True
        Me.cbTap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTap.Location = New System.Drawing.Point(518, 290)
        Me.cbTap.Name = "cbTap"
        Me.cbTap.Size = New System.Drawing.Size(99, 20)
        Me.cbTap.TabIndex = 59
        Me.cbTap.Text = "Tapentadol"
        Me.cbTap.UseVisualStyleBackColor = True
        '
        'cbSkel
        '
        Me.cbSkel.AutoSize = True
        Me.cbSkel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSkel.Location = New System.Drawing.Point(518, 264)
        Me.cbSkel.Name = "cbSkel"
        Me.cbSkel.Size = New System.Drawing.Size(194, 20)
        Me.cbSkel.TabIndex = 58
        Me.cbSkel.Text = "Skeletal Muscle Relaxants"
        Me.cbSkel.UseVisualStyleBackColor = True
        '
        'cbSedHyp
        '
        Me.cbSedHyp.AutoSize = True
        Me.cbSedHyp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSedHyp.Location = New System.Drawing.Point(518, 238)
        Me.cbSedHyp.Name = "cbSedHyp"
        Me.cbSedHyp.Size = New System.Drawing.Size(152, 20)
        Me.cbSedHyp.TabIndex = 57
        Me.cbSedHyp.Text = "Sedative Hypnotics"
        Me.cbSedHyp.UseVisualStyleBackColor = True
        '
        'cbPro
        '
        Me.cbPro.AutoSize = True
        Me.cbPro.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPro.Location = New System.Drawing.Point(518, 212)
        Me.cbPro.Name = "cbPro"
        Me.cbPro.Size = New System.Drawing.Size(120, 20)
        Me.cbPro.TabIndex = 56
        Me.cbPro.Text = "Propoxyphene"
        Me.cbPro.UseVisualStyleBackColor = True
        '
        'cbPreGab
        '
        Me.cbPreGab.AutoSize = True
        Me.cbPreGab.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPreGab.Location = New System.Drawing.Point(518, 186)
        Me.cbPreGab.Name = "cbPreGab"
        Me.cbPreGab.Size = New System.Drawing.Size(95, 20)
        Me.cbPreGab.TabIndex = 55
        Me.cbPreGab.Text = "Pregabalin"
        Me.cbPreGab.UseVisualStyleBackColor = True
        '
        'cbPhen
        '
        Me.cbPhen.AutoSize = True
        Me.cbPhen.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPhen.Location = New System.Drawing.Point(518, 160)
        Me.cbPhen.Name = "cbPhen"
        Me.cbPhen.Size = New System.Drawing.Size(114, 20)
        Me.cbPhen.TabIndex = 54
        Me.cbPhen.Text = "Phencyclidine"
        Me.cbPhen.UseVisualStyleBackColor = True
        '
        'cbOxy
        '
        Me.cbOxy.AutoSize = True
        Me.cbOxy.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOxy.Location = New System.Drawing.Point(518, 134)
        Me.cbOxy.Name = "cbOxy"
        Me.cbOxy.Size = New System.Drawing.Size(98, 20)
        Me.cbOxy.TabIndex = 53
        Me.cbOxy.Text = "Oxycodone"
        Me.cbOxy.UseVisualStyleBackColor = True
        '
        'cbOpiAnal
        '
        Me.cbOpiAnal.AutoSize = True
        Me.cbOpiAnal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOpiAnal.Location = New System.Drawing.Point(518, 108)
        Me.cbOpiAnal.Name = "cbOpiAnal"
        Me.cbOpiAnal.Size = New System.Drawing.Size(219, 20)
        Me.cbOpiAnal.TabIndex = 52
        Me.cbOpiAnal.Text = "Opioids and Opiate Analogues"
        Me.cbOpiAnal.UseVisualStyleBackColor = True
        '
        'cbOpi
        '
        Me.cbOpi.AutoSize = True
        Me.cbOpi.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOpi.Location = New System.Drawing.Point(518, 82)
        Me.cbOpi.Name = "cbOpi"
        Me.cbOpi.Size = New System.Drawing.Size(76, 20)
        Me.cbOpi.TabIndex = 51
        Me.cbOpi.Text = "Opiates"
        Me.cbOpi.UseVisualStyleBackColor = True
        '
        'cbNic
        '
        Me.cbNic.AutoSize = True
        Me.cbNic.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbNic.Location = New System.Drawing.Point(518, 56)
        Me.cbNic.Name = "cbNic"
        Me.cbNic.Size = New System.Drawing.Size(78, 20)
        Me.cbNic.TabIndex = 50
        Me.cbNic.Text = "Nicotine"
        Me.cbNic.UseVisualStyleBackColor = True
        '
        'cbMethy
        '
        Me.cbMethy.AutoSize = True
        Me.cbMethy.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMethy.Location = New System.Drawing.Point(266, 316)
        Me.cbMethy.Name = "cbMethy"
        Me.cbMethy.Size = New System.Drawing.Size(136, 20)
        Me.cbMethy.TabIndex = 49
        Me.cbMethy.Text = "Methylphenidate"
        Me.cbMethy.UseVisualStyleBackColor = True
        '
        'cbMethAmp
        '
        Me.cbMethAmp.AutoSize = True
        Me.cbMethAmp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMethAmp.Location = New System.Drawing.Point(266, 290)
        Me.cbMethAmp.Name = "cbMethAmp"
        Me.cbMethAmp.Size = New System.Drawing.Size(231, 20)
        Me.cbMethAmp.TabIndex = 48
        Me.cbMethAmp.Text = "Methylenedioxy-Amphetamines"
        Me.cbMethAmp.UseVisualStyleBackColor = True
        '
        'cbMtd
        '
        Me.cbMtd.AutoSize = True
        Me.cbMtd.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMtd.Location = New System.Drawing.Point(266, 264)
        Me.cbMtd.Name = "cbMtd"
        Me.cbMtd.Size = New System.Drawing.Size(100, 20)
        Me.cbMtd.TabIndex = 47
        Me.cbMtd.Text = "Methadone"
        Me.cbMtd.UseVisualStyleBackColor = True
        '
        'cbHero
        '
        Me.cbHero.AutoSize = True
        Me.cbHero.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbHero.Location = New System.Drawing.Point(266, 238)
        Me.cbHero.Name = "cbHero"
        Me.cbHero.Size = New System.Drawing.Size(142, 20)
        Me.cbHero.TabIndex = 46
        Me.cbHero.Text = "Heroin Metabolite"
        Me.cbHero.UseVisualStyleBackColor = True
        '
        'cbGab
        '
        Me.cbGab.AutoSize = True
        Me.cbGab.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbGab.Location = New System.Drawing.Point(266, 212)
        Me.cbGab.Name = "cbGab"
        Me.cbGab.Size = New System.Drawing.Size(173, 20)
        Me.cbGab.TabIndex = 45
        Me.cbGab.Text = "Gabapentin, Non-Blood"
        Me.cbGab.UseVisualStyleBackColor = True
        '
        'cbFent
        '
        Me.cbFent.AutoSize = True
        Me.cbFent.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbFent.Location = New System.Drawing.Point(266, 186)
        Me.cbFent.Name = "cbFent"
        Me.cbFent.Size = New System.Drawing.Size(89, 20)
        Me.cbFent.TabIndex = 44
        Me.cbFent.Text = "Fentanyls"
        Me.cbFent.UseVisualStyleBackColor = True
        '
        'cbCOC
        '
        Me.cbCOC.AutoSize = True
        Me.cbCOC.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCOC.Location = New System.Drawing.Point(266, 160)
        Me.cbCOC.Name = "cbCOC"
        Me.cbCOC.Size = New System.Drawing.Size(77, 20)
        Me.cbCOC.TabIndex = 43
        Me.cbCOC.Text = "Cocaine"
        Me.cbCOC.UseVisualStyleBackColor = True
        '
        'cbTHC
        '
        Me.cbTHC.AutoSize = True
        Me.cbTHC.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTHC.Location = New System.Drawing.Point(266, 134)
        Me.cbTHC.Name = "cbTHC"
        Me.cbTHC.Size = New System.Drawing.Size(167, 20)
        Me.cbTHC.TabIndex = 42
        Me.cbTHC.Text = "Cannabinoids, Natural"
        Me.cbTHC.UseVisualStyleBackColor = True
        '
        'cbBup
        '
        Me.cbBup.AutoSize = True
        Me.cbBup.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBup.Location = New System.Drawing.Point(266, 108)
        Me.cbBup.Name = "cbBup"
        Me.cbBup.Size = New System.Drawing.Size(122, 20)
        Me.cbBup.TabIndex = 41
        Me.cbBup.Text = "Buprenorphine"
        Me.cbBup.UseVisualStyleBackColor = True
        '
        'cbBenz
        '
        Me.cbBenz.AutoSize = True
        Me.cbBenz.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBenz.Location = New System.Drawing.Point(266, 82)
        Me.cbBenz.Name = "cbBenz"
        Me.cbBenz.Size = New System.Drawing.Size(134, 20)
        Me.cbBenz.TabIndex = 40
        Me.cbBenz.Text = "Benzodiazepines"
        Me.cbBenz.UseVisualStyleBackColor = True
        '
        'cbBarb
        '
        Me.cbBarb.AutoSize = True
        Me.cbBarb.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBarb.Location = New System.Drawing.Point(266, 56)
        Me.cbBarb.Name = "cbBarb"
        Me.cbBarb.Size = New System.Drawing.Size(109, 20)
        Me.cbBarb.TabIndex = 39
        Me.cbBarb.Text = "Barbiturates"
        Me.cbBarb.UseVisualStyleBackColor = True
        '
        'cbAntiEp
        '
        Me.cbAntiEp.AutoSize = True
        Me.cbAntiEp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAntiEp.Location = New System.Drawing.Point(22, 238)
        Me.cbAntiEp.Name = "cbAntiEp"
        Me.cbAntiEp.Size = New System.Drawing.Size(115, 20)
        Me.cbAntiEp.TabIndex = 38
        Me.cbAntiEp.Text = "Antiepileptics"
        Me.cbAntiEp.UseVisualStyleBackColor = True
        '
        'cbAD
        '
        Me.cbAD.AutoSize = True
        Me.cbAD.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAD.Location = New System.Drawing.Point(22, 212)
        Me.cbAD.Name = "cbAD"
        Me.cbAD.Size = New System.Drawing.Size(135, 20)
        Me.cbAD.TabIndex = 37
        Me.cbAD.Text = "Antidepressants"
        Me.cbAD.UseVisualStyleBackColor = True
        '
        'cbADTri
        '
        Me.cbADTri.AutoSize = True
        Me.cbADTri.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbADTri.Location = New System.Drawing.Point(22, 186)
        Me.cbADTri.Name = "cbADTri"
        Me.cbADTri.Size = New System.Drawing.Size(194, 20)
        Me.cbADTri.TabIndex = 36
        Me.cbADTri.Text = "Antidepressants, Tricyclic"
        Me.cbADTri.UseVisualStyleBackColor = True
        '
        'cbADSer
        '
        Me.cbADSer.AutoSize = True
        Me.cbADSer.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbADSer.Location = New System.Drawing.Point(22, 160)
        Me.cbADSer.Name = "cbADSer"
        Me.cbADSer.Size = New System.Drawing.Size(227, 20)
        Me.cbADSer.TabIndex = 35
        Me.cbADSer.Text = "Antidepressants, Serotonergic"
        Me.cbADSer.UseVisualStyleBackColor = True
        '
        'cbAmp
        '
        Me.cbAmp.AutoSize = True
        Me.cbAmp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAmp.Location = New System.Drawing.Point(22, 108)
        Me.cbAmp.Name = "cbAmp"
        Me.cbAmp.Size = New System.Drawing.Size(123, 20)
        Me.cbAmp.TabIndex = 34
        Me.cbAmp.Text = "Amphetamines"
        Me.cbAmp.UseVisualStyleBackColor = True
        '
        'cbAlc
        '
        Me.cbAlc.AutoSize = True
        Me.cbAlc.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAlc.Location = New System.Drawing.Point(22, 56)
        Me.cbAlc.Name = "cbAlc"
        Me.cbAlc.Size = New System.Drawing.Size(150, 20)
        Me.cbAlc.TabIndex = 33
        Me.cbAlc.Text = "Alcohol Biomarkers"
        Me.cbAlc.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(235, 423)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(157, 28)
        Me.btnClear.TabIndex = 66
        Me.btnClear.Text = "Clear Selections"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(644, 423)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(78, 28)
        Me.btnExit.TabIndex = 65
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(559, 423)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(78, 28)
        Me.btnSave.TabIndex = 67
        Me.btnSave.Text = "SAVE"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 19)
        Me.Label1.TabIndex = 68
        Me.Label1.Text = "Account:"
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(110, 10)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(115, 27)
        Me.txtAcct.TabIndex = 0
        '
        'lblAcct
        '
        Me.lblAcct.AutoSize = True
        Me.lblAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcct.Location = New System.Drawing.Point(251, 18)
        Me.lblAcct.Name = "lblAcct"
        Me.lblAcct.Size = New System.Drawing.Size(0, 19)
        Me.lblAcct.TabIndex = 70
        '
        'cbXREF
        '
        Me.cbXREF.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbXREF.Location = New System.Drawing.Point(21, 384)
        Me.cbXREF.Name = "cbXREF"
        Me.cbXREF.Size = New System.Drawing.Size(209, 55)
        Me.cbXREF.TabIndex = 71
        Me.cbXREF.Text = "Medications - presumptive positive (XREF)"
        Me.cbXREF.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(425, 423)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(128, 28)
        Me.btnPrint.TabIndex = 72
        Me.btnPrint.Text = "PRINT+SAVE"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(401, 370)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 19)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Profile Date:"
        '
        'dtpProfDt
        '
        Me.dtpProfDt.CalendarFont = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProfDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProfDt.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProfDt.Location = New System.Drawing.Point(521, 368)
        Me.dtpProfDt.Name = "dtpProfDt"
        Me.dtpProfDt.Size = New System.Drawing.Size(148, 27)
        Me.dtpProfDt.TabIndex = 74
        '
        'PrintDoc
        '
        '
        'lstData
        '
        Me.lstData.Font = New System.Drawing.Font("Courier New", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 17
        Me.lstData.Location = New System.Drawing.Point(683, 18)
        Me.lstData.Margin = New System.Windows.Forms.Padding(2)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(40, 4)
        Me.lstData.TabIndex = 75
        Me.lstData.Visible = False
        '
        'cbChiral
        '
        Me.cbChiral.AutoSize = True
        Me.cbChiral.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbChiral.Location = New System.Drawing.Point(21, 134)
        Me.cbChiral.Name = "cbChiral"
        Me.cbChiral.Size = New System.Drawing.Size(172, 20)
        Me.cbChiral.TabIndex = 76
        Me.cbChiral.Text = "Stereoisomer Analysis"
        Me.cbChiral.UseVisualStyleBackColor = True
        '
        'cbAlk
        '
        Me.cbAlk.AutoSize = True
        Me.cbAlk.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAlk.Location = New System.Drawing.Point(22, 82)
        Me.cbAlk.Name = "cbAlk"
        Me.cbAlk.Size = New System.Drawing.Size(165, 20)
        Me.cbAlk.TabIndex = 77
        Me.cbAlk.Text = "Alkaloids, Unspecified"
        Me.cbAlk.UseVisualStyleBackColor = True
        '
        'cbLevo
        '
        Me.cbLevo.AutoSize = True
        Me.cbLevo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbLevo.Location = New System.Drawing.Point(518, 343)
        Me.cbLevo.Name = "cbLevo"
        Me.cbLevo.Size = New System.Drawing.Size(107, 20)
        Me.cbLevo.TabIndex = 78
        Me.cbLevo.Text = "Levorphanol"
        Me.cbLevo.UseVisualStyleBackColor = True
        '
        'frmAcctOC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(747, 463)
        Me.Controls.Add(Me.cbLevo)
        Me.Controls.Add(Me.cbAlk)
        Me.Controls.Add(Me.cbChiral)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.dtpProfDt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.cbXREF)
        Me.Controls.Add(Me.lblAcct)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.cbAntiPys)
        Me.Controls.Add(Me.rbConf)
        Me.Controls.Add(Me.rbScreening)
        Me.Controls.Add(Me.cbTram)
        Me.Controls.Add(Me.cbTap)
        Me.Controls.Add(Me.cbSkel)
        Me.Controls.Add(Me.cbSedHyp)
        Me.Controls.Add(Me.cbPro)
        Me.Controls.Add(Me.cbPreGab)
        Me.Controls.Add(Me.cbPhen)
        Me.Controls.Add(Me.cbOxy)
        Me.Controls.Add(Me.cbOpiAnal)
        Me.Controls.Add(Me.cbOpi)
        Me.Controls.Add(Me.cbNic)
        Me.Controls.Add(Me.cbMethy)
        Me.Controls.Add(Me.cbMethAmp)
        Me.Controls.Add(Me.cbMtd)
        Me.Controls.Add(Me.cbHero)
        Me.Controls.Add(Me.cbGab)
        Me.Controls.Add(Me.cbFent)
        Me.Controls.Add(Me.cbCOC)
        Me.Controls.Add(Me.cbTHC)
        Me.Controls.Add(Me.cbBup)
        Me.Controls.Add(Me.cbBenz)
        Me.Controls.Add(Me.cbBarb)
        Me.Controls.Add(Me.cbAntiEp)
        Me.Controls.Add(Me.cbAD)
        Me.Controls.Add(Me.cbADTri)
        Me.Controls.Add(Me.cbADSer)
        Me.Controls.Add(Me.cbAmp)
        Me.Controls.Add(Me.cbAlc)
        Me.Name = "frmAcctOC"
        Me.Text = "1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbAntiPys As CheckBox
    Friend WithEvents rbConf As RadioButton
    Friend WithEvents rbScreening As RadioButton
    Friend WithEvents cbTram As CheckBox
    Friend WithEvents cbTap As CheckBox
    Friend WithEvents cbSkel As CheckBox
    Friend WithEvents cbSedHyp As CheckBox
    Friend WithEvents cbPro As CheckBox
    Friend WithEvents cbPreGab As CheckBox
    Friend WithEvents cbPhen As CheckBox
    Friend WithEvents cbOxy As CheckBox
    Friend WithEvents cbOpiAnal As CheckBox
    Friend WithEvents cbOpi As CheckBox
    Friend WithEvents cbNic As CheckBox
    Friend WithEvents cbMethy As CheckBox
    Friend WithEvents cbMethAmp As CheckBox
    Friend WithEvents cbMtd As CheckBox
    Friend WithEvents cbHero As CheckBox
    Friend WithEvents cbGab As CheckBox
    Friend WithEvents cbFent As CheckBox
    Friend WithEvents cbCOC As CheckBox
    Friend WithEvents cbTHC As CheckBox
    Friend WithEvents cbBup As CheckBox
    Friend WithEvents cbBenz As CheckBox
    Friend WithEvents cbBarb As CheckBox
    Friend WithEvents cbAntiEp As CheckBox
    Friend WithEvents cbAD As CheckBox
    Friend WithEvents cbADTri As CheckBox
    Friend WithEvents cbADSer As CheckBox
    Friend WithEvents cbAmp As CheckBox
    Friend WithEvents cbAlc As CheckBox
    Friend WithEvents btnClear As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtAcct As TextBox
    Friend WithEvents lblAcct As Label
    Friend WithEvents cbXREF As CheckBox
    Friend WithEvents btnPrint As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents dtpProfDt As DateTimePicker
    Friend WithEvents PrintDoc As Printing.PrintDocument
    Friend WithEvents lstData As ListBox
    Friend WithEvents cbChiral As CheckBox
    Friend WithEvents cbAlk As CheckBox
    Friend WithEvents cbLevo As CheckBox
End Class
