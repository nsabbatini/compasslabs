﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Public Class frmSVT

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click

        Call BuildSVT()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        PrintDoc.Print()

        Me.Close()
        Me.Dispose()
    End Sub
    Private Sub btnPreview_Click(sender As Object, e As EventArgs) Handles btnPreview.Click

        Call BuildSVT()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        PrintPreview.ShowDialog()

    End Sub
    Private Sub frmSVT_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DateTimePicker1.Format = DateTimePickerFormat.Custom
        DateTimePicker1.CustomFormat = "MM/dd/yyyy"
        DateTimePicker1.Text = DateAdd(DateInterval.Day, -1, Now)
    End Sub
    Public Sub BuildSVT()
        Dim Sel As String, p As String, c As Integer = 1, Tst As String = "", Dt As String = CLSLIMS.Piece(DateTimePicker1.Text, "/", 3) & CLSLIMS.Piece(DateTimePicker1.Text, "/", 1) & CLSLIMS.Piece(DateTimePicker1.Text, "/", 2)

        Sel = "select a.pspecno,substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4),rc,prslt from pspec a , pres b " &
              "where a.pspecno=b.pspecno and psact=1 and pract=1 " &
              "and substring(psrcvdt,1,8)='" & Dt & "' and substring(prslt,1,1) != '>' and prslt !='' " &
              "and ( " &
              "(rc='209033' and cast(prslt as decimal(9,2)) < 2.0) " &
              "or (rc='209011' and (cast(prslt as decimal(9,2)) <= 3.0 or cast(prslt as decimal(9,2)) >= 11.0)) " &
              "or (rc='209043' and cast(prslt as decimal(9,2)) > 200))"

        'Sel = Sel & " and a.pspecno in ('10000162','10000163')"

        lstData.Items.Clear()
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            If rs(2) = "209033" Then Tst = "Creatinine"
            If rs(2) = "209011" Then Tst = "pH"
            If rs(2) = "209043" Then Tst = "Oxidant"
            p = Space(2) & CLSLIMS.RJ(rs(0), 10) & Space(5) & CLSLIMS.LJ(rs(1), 10) & Space(3) & CLSLIMS.LJ(Tst, 10) & Space(8) & rs(3)
            lstData.Items.Add(p)
            c = c + 1
        Loop
        rs.Close()
        Conn.Close()
        Conn.Dispose()
    End Sub


    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 28


        Static PageNo As Integer = 1
        Dim y As Integer, PrevSOLAB As String = "", SOLAB As String = ""
        e.Graphics.DrawString("Abnormal SVTs", Font2, Brushes.Black, 290, 100)
        e.Graphics.DrawString("For " & DateTimePicker1.Text, Font3, Brushes.Black, 25, 158)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 690, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        e.Graphics.DrawString("               Receive    ", Font, Brushes.Black, 25, 185)
        e.Graphics.DrawString("Specimen #      Date          Test         Result", Font, Brushes.Black, 25, 200)

        e.Graphics.DrawLine(Pens.Black, 25, 180, 1075, 180)   'Horizonal line above headings
        e.Graphics.DrawLine(Pens.Black, 25, 220, 1075, 220)   'Horizonal line under headings

        e.Graphics.DrawLine(Pens.Black, 108, 180, 108, 1080)   'Veritcal line 1
        e.Graphics.DrawLine(Pens.Black, 200, 180, 200, 1080)   'Veritcal line 2
        e.Graphics.DrawLine(Pens.Black, 300, 180, 300, 1080)   'Veritcal line 3
        e.Graphics.DrawLine(Pens.Black, 400, 180, 400, 1080)   'Vertical line 4
        'e.Graphics.DrawLine(Pens.Black, 520, 180, 520, 1080)   'Veritcal line 5
        'e.Graphics.DrawLine(Pens.Black, 590, 180, 590, 1080)   'Veritcal line 6


        Dim c As Integer
        Static intStart As Integer

        c = 1

        y = 225

        For i = intStart To lstData.Items.Count - 1
            'data line
            e.Graphics.DrawString(lstData.Items(i).ToString, Font, Brushes.Black, 1, y)
            e.Graphics.DrawLine(Pens.Black, 25, y + 20, 1075, y + 20) 'Horizonal line after sample

            y = y + 30

            c = c + 1
            If c > PageLngth Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i
    End Sub
End Class