﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class frmAcctOC
    Dim SaveNothing As Boolean = False

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        ClearCLick()
        SaveNothing = True
    End Sub
    Public Sub ClearCLick()
        cbAD.Checked = False
        cbADSer.Checked = False
        cbADTri.Checked = False
        cbAlc.Checked = False
        cbAlk.Checked = False
        cbAmp.Checked = False
        cbChiral.Checked = False
        cbAntiEp.Checked = False
        cbAntiPys.Checked = False
        cbBarb.Checked = False
        cbBenz.Checked = False
        cbBup.Checked = False
        cbCOC.Checked = False
        cbFent.Checked = False
        cbGab.Checked = False
        cbHero.Checked = False
        cbMethAmp.Checked = False
        cbMethy.Checked = False
        cbMtd.Checked = False
        cbNic.Checked = False
        cbOpi.Checked = False
        cbOpiAnal.Checked = False
        cbOxy.Checked = False
        cbPhen.Checked = False
        cbPreGab.Checked = False
        cbPro.Checked = False
        cbSedHyp.Checked = False
        cbSkel.Checked = False
        cbTap.Checked = False
        cbTHC.Checked = False
        cbTram.Checked = False
        cbLevo.Checked = False
        cbXREF.Checked = False
        rbConf.Checked = False
        rbScreening.Checked = False
        dtpProfDt.Text = Now
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        SaveOCs()

    End Sub
    Public Sub SaveOCs()
        Dim strTstCode As String = "", Hero As Boolean = False, Chiral As Boolean = False
        Dim Screen As Boolean = False, Conf As Boolean = False
        Screen = rbScreening.Checked : Conf = rbConf.Checked
        Dim TCcnt As Integer = 0, AtLeastOne As Boolean = False
        If Not SaveNothing Then
            If Not Screen And Not Conf Then
                MsgBox("Must select Screening or Confirmation")
                Exit Sub
            End If
        End If
        'Temporarily auto check XREF
        'cbXREF.Checked = True
        TCcnt = 0
        If Screen Then
            If cbAlc.Checked Then
                strTstCode = strTstCode & "'351000',"
                TCcnt = TCcnt + 1
            End If
            If cbAlk.Checked Then
                strTstCode = strTstCode & "'395000',"
                TCcnt = TCcnt + 1
            End If
            If cbAmp.Checked Then
                strTstCode = strTstCode & "'353000',"
                TCcnt = TCcnt + 1
            End If
            If cbChiral.Checked Then
                Chiral = True
                strTstCode = strTstCode & "'301027',"
                TCcnt = TCcnt + 1
            End If
            If cbADSer.Checked Then
                strTstCode = strTstCode & "'354000',"
                TCcnt = TCcnt + 1
            End If
            If cbADTri.Checked Then
                strTstCode = strTstCode & "'355000',"
                TCcnt = TCcnt + 1
            End If
            If cbAD.Checked Then
                strTstCode = strTstCode & "'356000',"
                TCcnt = TCcnt + 1
            End If
            If cbAntiEp.Checked Then
                strTstCode = strTstCode & "'357000',"
                TCcnt = TCcnt + 1
            End If
            If cbAntiPys.Checked Then
                strTstCode = strTstCode & "'358000',"
                TCcnt = TCcnt + 1
            End If
            If cbBarb.Checked Then
                strTstCode = strTstCode & "'360000',"
                TCcnt = TCcnt + 1
            End If
            If cbBenz.Checked Then
                strTstCode = strTstCode & "'362000',"
                TCcnt = TCcnt + 1
            End If
            If cbBup.Checked Then
                strTstCode = strTstCode & "'364000',"
                TCcnt = TCcnt + 1
            End If
            If cbTHC.Checked Then
                strTstCode = strTstCode & "'366000',"
                TCcnt = TCcnt + 1
            End If
            If cbCOC.Checked Then
                strTstCode = strTstCode & "'368000',"
                TCcnt = TCcnt + 1
            End If
            If cbFent.Checked Then
                strTstCode = strTstCode & "'369000',"
                TCcnt = TCcnt + 1
            End If
            If cbGab.Checked Then
                strTstCode = strTstCode & "'370000',"
                TCcnt = TCcnt + 1
            End If
            If cbMtd.Checked Then
                strTstCode = strTstCode & "'374000',"
                TCcnt = TCcnt + 1
            End If
            If cbMethAmp.Checked Then
                strTstCode = strTstCode & "'376000',"
                TCcnt = TCcnt + 1
            End If
            If cbMethy.Checked Then
                strTstCode = strTstCode & "'377000',"
                TCcnt = TCcnt + 1
            End If
            If cbNic.Checked Then
                strTstCode = strTstCode & "'379000',"
                TCcnt = TCcnt + 1
            End If
            If cbOpi.Checked Then
                strTstCode = strTstCode & "'381000',"
                TCcnt = TCcnt + 1
            End If
            If cbHero.Checked Then
                If cbOpi.Checked Then
                    Hero = True
                Else
                    strTstCode = strTstCode & "'372000',"
                    TCcnt = TCcnt + 1
                End If
            End If
            If cbOpiAnal.Checked Then
                strTstCode = strTstCode & "'382000',"
                TCcnt = TCcnt + 1
            End If
            If cbOxy.Checked Then
                strTstCode = strTstCode & "'384000',"
                TCcnt = TCcnt + 1
            End If
            If cbPhen.Checked Then
                strTstCode = strTstCode & "'386000',"
                TCcnt = TCcnt + 1
            End If
            If cbPreGab.Checked Then
                strTstCode = strTstCode & "'387000',"
                TCcnt = TCcnt + 1
            End If
            If cbPro.Checked Then
                strTstCode = strTstCode & "'389000',"
                TCcnt = TCcnt + 1
            End If
            If cbSedHyp.Checked Then
                strTstCode = strTstCode & "'390000',"
                TCcnt = TCcnt + 1
            End If
            If cbSkel.Checked Then
                strTstCode = strTstCode & "'392000',"
                TCcnt = TCcnt + 1
            End If
            If cbTap.Checked Then
                strTstCode = strTstCode & "'393000',"
                TCcnt = TCcnt + 1
            End If
            If cbTram.Checked Then
                strTstCode = strTstCode & "'394000',"
                TCcnt = TCcnt + 1
            End If
            If cbLevo.Checked Then
                strTstCode = strTstCode & "'308130',"
                TCcnt = TCcnt + 1
            End If
            If cbXREF.Checked Then
                strTstCode = strTstCode & "'800000',"
                TCcnt = TCcnt + 1
            End If
        ElseIf Conf Then
            If cbAlc.Checked Then
                strTstCode = strTstCode & "'350000',"
                TCcnt = TCcnt + 1
            End If
            If cbAlk.Checked Then
                strTstCode = strTstCode & "'395000',"
                TCcnt = TCcnt + 1
            End If
            If cbAmp.Checked Then
                strTstCode = strTstCode & "'352000',"
                TCcnt = TCcnt + 1
            End If
            If cbChiral.Checked Then
                Chiral = True
            End If
            If cbADSer.Checked Then
                strTstCode = strTstCode & "'354000',"
                TCcnt = TCcnt + 1
            End If
            If cbADTri.Checked Then
                strTstCode = strTstCode & "'355000',"
                TCcnt = TCcnt + 1
            End If
            If cbAD.Checked Then
                strTstCode = strTstCode & "'356000',"
                TCcnt = TCcnt + 1
            End If
            If cbAntiEp.Checked Then
                strTstCode = strTstCode & "'357000',"
                TCcnt = TCcnt + 1
            End If
            If cbAntiPys.Checked Then
                strTstCode = strTstCode & "'358000',"
                TCcnt = TCcnt + 1
            End If
            If cbBarb.Checked Then
                strTstCode = strTstCode & "'359000',"
                TCcnt = TCcnt + 1
            End If
            If cbBenz.Checked Then
                strTstCode = strTstCode & "'361000',"
                TCcnt = TCcnt + 1
            End If
            If cbBup.Checked Then
                strTstCode = strTstCode & "'363000',"
                TCcnt = TCcnt + 1
            End If
            If cbTHC.Checked Then
                strTstCode = strTstCode & "'365000',"
                TCcnt = TCcnt + 1
            End If
            If cbCOC.Checked Then
                strTstCode = strTstCode & "'367000',"
                TCcnt = TCcnt + 1
            End If
            If cbFent.Checked Then
                strTstCode = strTstCode & "'369000',"
                TCcnt = TCcnt + 1
            End If
            If cbGab.Checked Then
                strTstCode = strTstCode & "'370000',"
                TCcnt = TCcnt + 1
            End If
            If cbHero.Checked Then
                strTstCode = strTstCode & "'371000',"
                TCcnt = TCcnt + 1
            End If
            If cbMtd.Checked Then
                strTstCode = strTstCode & "'373000',"
                TCcnt = TCcnt + 1
            End If
            If cbMethAmp.Checked Then
                strTstCode = strTstCode & "'375000',"
                TCcnt = TCcnt + 1
            End If
            If cbMethy.Checked Then
                strTstCode = strTstCode & "'377000',"
                TCcnt = TCcnt + 1
            End If
            If cbNic.Checked Then
                strTstCode = strTstCode & "'378000',"
                TCcnt = TCcnt + 1
            End If
            If cbOpi.Checked Then
                strTstCode = strTstCode & "'380000',"
                TCcnt = TCcnt + 1
            End If
            If cbOpiAnal.Checked Then
                strTstCode = strTstCode & "'382000',"
                TCcnt = TCcnt + 1
            End If
            If cbOxy.Checked Then
                strTstCode = strTstCode & "'383000',"
                TCcnt = TCcnt + 1
            End If
            If cbPhen.Checked Then
                strTstCode = strTstCode & "'385000',"
                TCcnt = TCcnt + 1
            End If
            If cbPreGab.Checked Then
                strTstCode = strTstCode & "'387000',"
                TCcnt = TCcnt + 1
            End If
            If cbPro.Checked Then
                strTstCode = strTstCode & "'388000',"
                TCcnt = TCcnt + 1
            End If
            If cbSedHyp.Checked Then
                strTstCode = strTstCode & "'390000',"
                TCcnt = TCcnt + 1
            End If
            If cbSkel.Checked Then
                strTstCode = strTstCode & "'391000',"
                TCcnt = TCcnt + 1
            End If
            If cbTap.Checked Then
                strTstCode = strTstCode & "'393000',"
                TCcnt = TCcnt + 1
            End If
            If cbTram.Checked Then
                strTstCode = strTstCode & "'394000',"
                TCcnt = TCcnt + 1
            End If
            If cbLevo.Checked Then
                strTstCode = strTstCode & "'308130',"
                TCcnt = TCcnt + 1
            End If
            If cbXREF.Checked Then
                strTstCode = strTstCode & "'800000',"
                TCcnt = TCcnt + 1
            End If
        End If
        'Temporarily add SVT to everything
        strTstCode = strTstCode & "'209000'," : TCcnt = TCcnt + 1
        strTstCode = Mid(strTstCode, 1, Len(strTstCode) - 1)
        Dim TstMethod As String = IIf(Screen, "SCRN", "CONF")
        Dim AcctEffdt As String = CLSLIMS.CDT, PrevAcctEffdt As String = ""
        Dim InsCmd As String = "", UpdCmd As String = ""
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Dim rs As SqlDataReader
        Cmd.Connection = conn
        Cmd.CommandText = "Select AEFDT From ACCT Where ACCT='" & txtAcct.Text & "' And AACT=1"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            PrevAcctEffdt = rs(0)
        End If
        rs.Close()
        InsCmd = "Insert into ACCT Select ACCT, '" & AcctEffdt & "', AACT, AABRV, ANAME, ANAME2, AAD1, AAD2, ACITY, ACNTY, ASTATE, ACNTRY, AZIP, APHONE, AFAX, ACONTACT, AEMAIL, ASPRFX, AMRO, AMROA, ASALESRG, ATYPE, '" & TstMethod & "', AMACCT, ABACCT, ADFLTREQ, '" & IIf(Hero, "HERO", "") & "', '" & IIf(Chiral, "CHIRAL", "") & "', AOCREST, ARQUAN, BFEE, APOFEE, ABUNDLE, BCFEE, BMFEE,'" & CLSLIMS.UID & "', AOCORDER, ABILLTO, AMISC1,'" & dtpProfDt.Text & "', AMISC3 " &
                               "From ACCT Where ACCT='" & txtAcct.Text & "' And AACT=1"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        UpdCmd = "Update ACCT Set AACT=0 Where ACCT='" & txtAcct.Text & "' And AACT=1 And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = UpdCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTMC Select ACCT, '" & AcctEffdt & "', ACCTMCx, MC From ACCTMC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTPHY Select ACCT, '" & AcctEffdt & "', PHY From ACCTPHY Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTRPQUE Select ACCT, '" & AcctEffdt & "', ACCTRPQUEx, ARACCT, ARPTTO, ANRPT, AARPT, RPQUE, PHY, ARPRIMARY From ACCTRPQUE Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTLOC Select ACCT, '" & AcctEffdt & "', ACCTLOCx, ALOC, ALOCAD1, ALOCAD2, ALOCCITY, ALOCSTATE, ALOCZIP, ALOCPHONE From ACCTLOC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()

        For i = 1 To TCcnt
            InsCmd = "Insert into ACCTOC Values ('" & txtAcct.Text & "','" & AcctEffdt & "'," & i & "," & CLSLIMS.Piece(strTstCode, ",", i) & ",1)"
            Cmd.CommandText = InsCmd
            Cmd.ExecuteNonQuery()
        Next i
        Cmd.Dispose()
        conn.Close()
        txtAcct.Text = ""
        lblAcct.Text = ""
        ClearCLick()

        txtAcct.Focus()
    End Sub

    Private Sub txtAcct_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtAcct.Text = UCase(txtAcct.Text)
            If Mid(txtAcct.Text, 1, 1) = "S" Or Mid(txtAcct.Text, 1, 1) = "C" Or Mid(txtAcct.Text, 1, 1) = "D" Then
                MsgBox("Only PDM Accounts.")
                txtAcct.Text = ""
                Exit Sub
            End If
            lblAcct.Text = GetAcctName(txtAcct.Text)
            If lblAcct.Text = "" Then
                MsgBox("Invalid account.")
                Exit Sub
            End If
            ClearCLick()
            CLSLIMS.AcctNum = txtAcct.Text
            GetDefOC()
        End If
    End Sub
    Public Sub GetDefOC()
        Dim OC As String = "", TstMethod As String = ""
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = conn
        Dim rs As SqlDataReader
        Cmd.CommandText = "Select OC,ACCTOCx From ACCT A, ACCTOC B Where A.ACCT=B.ACCT And A.AEFDT=B.AEFDT And AACT=1 And A.ACCT='" & CLSLIMS.AcctNum & "' Order By ACCTOCx"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            OC = rs(0)
            Select Case OC
                Case "351000" : cbAlc.Checked = True
                Case "350000" : cbAlc.Checked = True
                Case "395000" : cbAlk.Checked = True
                Case "353000" : cbAmp.Checked = True
                Case "352000" : cbAmp.Checked = True
                Case "309010" : cbChiral.Checked = True
                Case "301027" : cbChiral.Checked = True
                Case "354000" : cbADSer.Checked = True
                Case "355000" : cbADTri.Checked = True
                Case "356000" : cbAD.Checked = True
                Case "357000" : cbAntiEp.Checked = True
                Case "358000" : cbAntiPys.Checked = True
                Case "360000" : cbBarb.Checked = True
                Case "359000" : cbBarb.Checked = True
                Case "362000" : cbBenz.Checked = True
                Case "361000" : cbBenz.Checked = True
                Case "364000" : cbBup.Checked = True
                Case "363000" : cbBup.Checked = True
                Case "366000" : cbTHC.Checked = True
                Case "365000" : cbTHC.Checked = True
                Case "368000" : cbCOC.Checked = True
                Case "367000" : cbCOC.Checked = True
                Case "369000" : cbFent.Checked = True
                Case "370000" : cbGab.Checked = True
                Case "372000" : cbHero.Checked = True
                Case "371000" : cbHero.Checked = True
                Case "374000" : cbMtd.Checked = True
                Case "373000" : cbMtd.Checked = True
                Case "376000" : cbMethAmp.Checked = True
                Case "375000" : cbMethAmp.Checked = True
                Case "377000" : cbMethy.Checked = True
                Case "379000" : cbNic.Checked = True
                Case "378000" : cbNic.Checked = True
                Case "381000" : cbOpi.Checked = True
                Case "380000" : cbOpi.Checked = True
                Case "382000" : cbOpiAnal.Checked = True
                Case "384000" : cbOxy.Checked = True
                Case "383000" : cbOxy.Checked = True
                Case "386000" : cbPhen.Checked = True
                Case "385000" : cbPhen.Checked = True
                Case "387000" : cbPreGab.Checked = True
                Case "389000" : cbPro.Checked = True
                Case "388000" : cbPro.Checked = True
                Case "390000" : cbSedHyp.Checked = True
                Case "391000" : cbSkel.Checked = True
                Case "392000" : cbSkel.Checked = True
                Case "393000" : cbTap.Checked = True
                Case "394000" : cbTram.Checked = True
                Case "308130" : cbLevo.Checked = True
                Case "800000" : cbXREF.Checked = True
            End Select
        Loop
        rs.Close()
        Cmd.CommandText = "Select AAVRSPIN,AMISC2,ADFLTRFMT,ABCONT From ACCT Where AACT=1 And ACCT='" & txtAcct.Text & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            TstMethod = rs(0)
            dtpProfDt.Text = rs(1)
            If rs(2) = "HERO" Then
                cbHero.Checked = True
            End If
            If rs(3) = "CHIRAL" Then
                cbChiral.Checked = True
            End If
        End If
        rs.Close()
        If TstMethod = "SCRN" Then
            rbScreening.Checked = True
        ElseIf TstMethod = "CONF" Then
            rbConf.Checked = True
        End If

        conn.Close()
    End Sub
    Public Function GetAcctName(ByVal Acct) As String
        GetAcctName = ""

        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where Acct = '" & txtAcct.Text & "' and aact=1 "

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(1)
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click



        Call BuildProfile()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        PrintDoc.Print()

        SaveOCs()

    End Sub

    Private Sub frmAcctOC_Load(sender As Object, e As EventArgs) Handles Me.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpProfDt.Format = DateTimePickerFormat.Custom
        dtpProfDt.CustomFormat = "MM/dd/yyyy"
        dtpProfDt.Text = Now
    End Sub

    Public Sub BuildProfile()
        Dim Screen As Boolean = False, Conf As Boolean = False
        Screen = rbScreening.Checked : Conf = rbConf.Checked
        Dim TCcnt As Integer = 0
        Dim p As String = ""
        If Screen Then
            If cbAlc.Checked Then
                p = p & "351000-Alcohol BioMarkers^"
                TCcnt = TCcnt + 1
            End If
            If cbAlk.Checked Then
                p = p & "395000-Alkaloids, Unspecified^"
                TCcnt = TCcnt + 1
            End If
            If cbAmp.Checked Then
                p = p & "353000-Amphetamines^"
                TCcnt = TCcnt + 1
            End If
            If cbChiral.Checked Then
                p = p & "** Chiral Ordered on Positive Methamphetamine **^"
                TCcnt = TCcnt + 1
            End If
            If cbADSer.Checked Then
                p = p & "354000-Antidepressants, Serotonergic^"
                TCcnt = TCcnt + 1
            End If
            If cbADTri.Checked Then
                p = p & "355000-Antidepressants, Tricyclic^"
                TCcnt = TCcnt + 1
            End If
            If cbAD.Checked Then
                p = p & "356000-Antidepressants^"
                TCcnt = TCcnt + 1
            End If
            If cbAntiEp.Checked Then
                p = p & "357000-Antiepileptics^"
                TCcnt = TCcnt + 1
            End If
            If cbAntiPys.Checked Then
                p = p & "358000-Antipsychotics^"
                TCcnt = TCcnt + 1
            End If
            If cbBarb.Checked Then
                p = p & "360000-Barbiturates^"
                TCcnt = TCcnt + 1
            End If
            If cbBenz.Checked Then
                p = p & "362000-Benzodiazepines^"
                TCcnt = TCcnt + 1
            End If
            If cbBup.Checked Then
                p = p & "364000-Buprenorphine^"
                TCcnt = TCcnt + 1
            End If
            If cbTHC.Checked Then
                p = p & "366000-Cannabinoids, Natural^"
                TCcnt = TCcnt + 1
            End If
            If cbCOC.Checked Then
                p = p & "368000-Cocaine^"
                TCcnt = TCcnt + 1
            End If
            If cbFent.Checked Then
                p = p & "369000-Fentanyls^"
                TCcnt = TCcnt + 1
            End If
            If cbGab.Checked Then
                p = p & "370000-Gabapetin, Non-Blood^"
                TCcnt = TCcnt + 1
            End If
            If cbHero.Checked Then
                If cbOpi.Checked Then
                    p = p & "372000-Heroin Metabolite (Removed. Duplicate Opiate Screen.)^"
                Else
                    p = p & "372000-Heroin Metabolite^"
                End If
                TCcnt = TCcnt + 1
            End If
            If cbMtd.Checked Then
                p = p & "374000-Methadone^"
                TCcnt = TCcnt + 1
            End If
            If cbMethAmp.Checked Then
                p = p & "376000-Methylenedioxy-Amphetamines^"
                TCcnt = TCcnt + 1
            End If
            If cbMethy.Checked Then
                p = p & "377000-Methylphenidate^"
                TCcnt = TCcnt + 1
            End If
            If cbNic.Checked Then
                p = p & "379000-Nicotine^"
                TCcnt = TCcnt + 1
            End If
            If cbOpi.Checked Then
                p = p & "381000-Opiates^"
                TCcnt = TCcnt + 1
            End If
            If cbOpiAnal.Checked Then
                p = p & "382000-Opioids and Opiate Analogues^"
                TCcnt = TCcnt + 1
            End If
            If cbOxy.Checked Then
                p = p & "384000-Oxycodone^"
                TCcnt = TCcnt + 1
            End If
            If cbPhen.Checked Then
                p = p & "386000-Phencyclidine^"
                TCcnt = TCcnt + 1
            End If
            If cbPreGab.Checked Then
                p = p & "387000-Pregabalin^"
                TCcnt = TCcnt + 1
            End If
            If cbPro.Checked Then
                p = p & "389000-Propoxyphene^"
                TCcnt = TCcnt + 1
            End If
            If cbSedHyp.Checked Then
                p = p & "390000-Sedative Hypnotics^"
                TCcnt = TCcnt + 1
            End If
            If cbSkel.Checked Then
                p = p & "392000-Skeletal Muscle Relaxants^"
                TCcnt = TCcnt + 1
            End If
            If cbTap.Checked Then
                p = p & "393000-Tapentadol^"
                TCcnt = TCcnt + 1
            End If
            If cbTram.Checked Then
                p = p & "394000-Tramadol^"
                TCcnt = TCcnt + 1
            End If
            If cbLevo.Checked Then
                p = p & "308130-Levorhanol^"
                TCcnt = TCcnt + 1
            End If
            If cbXREF.Checked Then
                p = p & "800000-XREF (Medications Presumptive Positives)^"
                TCcnt = TCcnt + 1
            End If
        ElseIf Conf Then
            If cbAlc.Checked Then
                p = p & "350000-Alcohol BioMarkers^"
                TCcnt = TCcnt + 1
            End If
            If cbAlk.Checked Then
                p = p & "395000-Alkaloids, Unspecified^"
                TCcnt = TCcnt + 1
            End If
            If cbAmp.Checked Then
                p = p & "352000-Amphetamines^"
                TCcnt = TCcnt + 1
            End If
            If cbChiral.Checked Then
                p = p & "** Chiral Ordered on Positive Methamphetamine **^"
                TCcnt = TCcnt + 1
            End If
            If cbADSer.Checked Then
                p = p & "354000-Antidepressants, Serotonergic^"
                TCcnt = TCcnt + 1
            End If
            If cbADTri.Checked Then
                p = p & "355000-Antidepressants, Tricyclic^"
                TCcnt = TCcnt + 1
            End If
            If cbAD.Checked Then
                p = p & "356000-Antidepressants^"
                TCcnt = TCcnt + 1
            End If
            If cbAntiEp.Checked Then
                p = p & "357000-Antiepileptics^"
                TCcnt = TCcnt + 1
            End If
            If cbAntiPys.Checked Then
                p = p & "358000-Antipsychotics^"
                TCcnt = TCcnt + 1
            End If
            If cbBarb.Checked Then
                p = p & "359000-Barbiturates^"
                TCcnt = TCcnt + 1
            End If
            If cbBenz.Checked Then
                p = p & "361000-Benzodiazepines^"
                TCcnt = TCcnt + 1
            End If
            If cbBup.Checked Then
                p = p & "363000-Buprenorphine^"
                TCcnt = TCcnt + 1
            End If
            If cbTHC.Checked Then
                p = p & "365000-Cannabinoids, Natural^"
                TCcnt = TCcnt + 1
            End If
            If cbCOC.Checked Then
                p = p & "367000-Cocaine^"
                TCcnt = TCcnt + 1
            End If
            If cbFent.Checked Then
                p = p & "369000-Fentanyls^"
                TCcnt = TCcnt + 1
            End If
            If cbGab.Checked Then
                p = p & "370000-Gabapentin, Non-Blood^"
                TCcnt = TCcnt + 1
            End If
            If cbHero.Checked Then
                p = p & "371000-Heroin Metabolite^"
                TCcnt = TCcnt + 1
            End If
            If cbMtd.Checked Then
                p = p & "373000-Methadone^"
                TCcnt = TCcnt + 1
            End If
            If cbMethAmp.Checked Then
                p = p & "375000-Methylenedioxy-Amphetamines^"
                TCcnt = TCcnt + 1
            End If
            If cbMethy.Checked Then
                p = p & "377000-Methylphenidate^"
                TCcnt = TCcnt + 1
            End If
            If cbNic.Checked Then
                p = p & "378000-Nicotine^"
                TCcnt = TCcnt + 1
            End If
            If cbOpi.Checked Then
                p = p & "380000-Opiates^"
                TCcnt = TCcnt + 1
            End If
            If cbOpiAnal.Checked Then
                p = p & "382000-Opioids and Opiate Analogues^"
                TCcnt = TCcnt + 1
            End If
            If cbOxy.Checked Then
                p = p & "383000-Oxycodone^"
                TCcnt = TCcnt + 1
            End If
            If cbPhen.Checked Then
                p = p & "385000-Phencyclidine^"
                TCcnt = TCcnt + 1
            End If
            If cbPreGab.Checked Then
                p = p & "387000-Pregabalin^"
                TCcnt = TCcnt + 1
            End If
            If cbPro.Checked Then
                p = p & "388000-Propoxyphene^"
                TCcnt = TCcnt + 1
            End If
            If cbSedHyp.Checked Then
                p = p & "390000-Sedative Hypnotics^"
                TCcnt = TCcnt + 1
            End If
            If cbSkel.Checked Then
                p = p & "391000-Skeletal Muscle Relaxants^"
                TCcnt = TCcnt + 1
            End If
            If cbTap.Checked Then
                p = p & "393000-Tapentadol^"
                TCcnt = TCcnt + 1
            End If
            If cbTram.Checked Then
                p = p & "394000-Tramadol^"
                TCcnt = TCcnt + 1
            End If
            If cbLevo.Checked Then
                p = p & "308130-Levorhanol^"
                TCcnt = TCcnt + 1
            End If
            If cbXREF.Checked Then
                p = p & "800000-XREF (Medications Presumptive Positives)^"
                TCcnt = TCcnt + 1
            End If
        End If
        lstData.Items.Clear()
        For i = 1 To TCcnt
            lstData.Items.Add(Space(2) & CLSLIMS.Piece(p, "^", i))
        Next i

    End Sub

    Private Sub PrintDoc_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Screen As Boolean = False, Conf As Boolean = False
        Screen = rbScreening.Checked : Conf = rbConf.Checked
        Dim y As Integer = 190
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font6 As New Font("Tacoma", 14, FontStyle.Bold Or FontStyle.Underline)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        'Header
        e.Graphics.DrawString("Account Profile Selection Verification", Font2, Brushes.Black, 180, 50)
        e.Graphics.DrawString(txtAcct.Text & ": " & lblAcct.Text, Font6, Brushes.Black, 50, 100)
        e.Graphics.DrawString("Profile Date: " & dtpProfDt.Text, Font3, Brushes.Black, 90, 125)
        If Screen Then
            e.Graphics.DrawString("Perform Screen (if available)", Font3, Brushes.Black, 150, 160)
        ElseIf Conf Then
            e.Graphics.DrawString("Confirmation Only", Font3, Brushes.Black, 150, 160)
        End If

        'Detail
        For i = 0 To lstData.Items.Count - 1
            e.Graphics.DrawString(lstData.Items(i).ToString, Font4, Brushes.Black, 100, y)
            y = y + 20
        Next i
        'Footer
        e.Graphics.DrawString("Checked/Approved by: _________________________", Font5, Brushes.Black, 150, y + 60)
        e.Graphics.DrawString("               Date: _________________________", Font5, Brushes.Black, 150, y + 90)
    End Sub

End Class