﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel


Public Class FldrClip

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Dim Sel As String, Fnd As Boolean, CBtxt As String = ""
        If txtFldr.Text = "" Then
            MsgBox("No Folder")
            Exit Sub
        End If
        Dim prfx As String = "", prfx2 As String = ""
        prfx = Mid(txtFldr.Text, 1, 1)
        If prfx <> "E" And prfx <> "T" Then
            Sel = "Select POS,PSPECNO From XTRAYS Where FOLDER='" & txtFldr.Text & "' Order by POS"
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Fnd = False
            Do While rs.Read()
                CBtxt = CBtxt & rs(1) & vbCrLf
                Fnd = True
            Loop
            rs.Close()
            If Not Fnd Then
                MsgBox("Folder not found")
            Else
                Clipboard.SetText(CBtxt)
                Me.Close()
                Me.Dispose()
            End If
        Else
            If prfx = "E" Then prfx2 = "ETG"
            If prfx = "T" Then prfx2 = "THC"
            Dim XLApp As Excel.Application
            Dim XLWBS As Excel.Workbooks
            Dim XLWB As Excel.Workbook
            Dim XLWS As Excel.Worksheet
            XLApp = New Excel.Application
            XLApp.Visible = False
            XLWBS = XLApp.Workbooks
            XLWB = XLWBS.Open("\\cl-agilent\shared\EP Motion\Archive\" & prfx2 & Mid(txtFldr.Text, 2) & ".xlsx")
            XLWS = XLWB.Sheets(1)

            For i = 4 To 11
                For j = 2 To 13
                    If IfSample(XLWS.Cells(i, j).value) Then CBtxt = CBtxt & XLWS.Cells(i, j).Value & vbCrLf
                Next j
            Next i
            For i = 14 To 21
                For j = 2 To 13
                    If IfSample(XLWS.Cells(i, j).value) Then CBtxt = CBtxt & XLWS.Cells(i, j).Value & vbCrLf
                Next j
            Next i

            XLApp.DisplayAlerts = False
            XLWB.Close()
            XLWBS.Close()
            XLApp.DisplayAlerts = True
            XLApp.Quit()

            Clipboard.SetText(CBtxt)
            Me.Close()
            Me.Dispose()
        End If

    End Sub
    Private Function IfSample(ByRef samp As String) As Boolean
        IfSample = False
        If samp = "" Then
            Exit Function
        End If
        samp = Replace(Replace(samp, ")", ""), "(", "")
        samp = CLSLIMS.Piece(samp, " ", 1)
        Dim Sel As String = "Select TRAY From XTRAYS Where PSPECNO = '" & samp & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                IfSample = True
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
End Class