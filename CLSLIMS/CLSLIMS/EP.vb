﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports CLSLIMS.CLSLIMS
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Collections.Specialized

Public Class EP

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim XLApp As Excel.Application
        Dim XLWBS As Excel.Workbooks
        Dim XLWB As Excel.Workbook
        Dim XLWS As Excel.Worksheet
        Dim varCell As String
        Dim colSamples As NameValueCollection
        Dim arrSamples(196) As String
        For i = 1 To 196 : arrSamples(i) = "" : Next i

        TextBox1.Text = UCase(TextBox1.Text)
        If Mid(TextBox1.Text, 1, 1) = "T" Then
            If Not System.IO.File.Exists("\\cl-agilent\shared\EP Motion\Archive\THC" & Mid(TextBox1.Text, 2) & ".xlsx") Then
                MsgBox("Folder not found.")
                Exit Sub
            End If
        Else
            If Not System.IO.File.Exists("\\cl-agilent\shared\EP Motion\Archive\Grid" & TextBox1.Text & ".xlsx") Then
                MsgBox("Folder not found.")
                Exit Sub
            End If
        End If
        colSamples = New NameValueCollection
        colSamples.Clear()
        colSamples.Add("LOW1", "")
        colSamples.Add("LOW2", "")
        colSamples.Add("HIGH1", "")
        colSamples.Add("HIGH2", "")
        colSamples.Add("HIGH3", "")
        colSamples.Add("HYD", "")
        colSamples.Add("NEG", "")
        colSamples.Add("Plate QC", "")
        colSamples.Add("PRIMER1", "")
        colSamples.Add("PRIMER2", "")
        colSamples.Add("PRIMER3", "")
        colSamples.Add("PRIMER4", "")
        colSamples.Add("PRIMER5", "")
        colSamples.Add("BLANK", "")
        colSamples.Add("P1", "")
        colSamples.Add("P2", "")
        colSamples.Add("H1", "")
        colSamples.Add("H2", "")
        colSamples.Add("H3", "")
        colSamples.Add("WB", "")

        'Open Excel file
        XLApp = New Excel.Application
        XLApp.Visible = False
        XLWBS = XLApp.Workbooks
        If Mid(TextBox1.Text, 1, 1) = "T" Then
            XLWB = XLWBS.Open("\\cl-agilent\shared\EP Motion\Archive\THC" & Mid(TextBox1.Text, 2) & ".xlsx")
        Else
            XLWB = XLWBS.Open("\\cl-agilent\shared\EP Motion\Archive\Grid" & TextBox1.Text & ".xlsx")
        End If
        XLWS = XLWB.Sheets(1)
        Dim varRows As String = "xxxABCDEFGH"
        Dim pos As String, c As Integer, dat As String
        c = 1
        For i = 4 To 11
            For j = 2 To 13
                varCell = Replace(Replace(CLSLIMS.Piece(XLWS.Cells(i, j).Value, " ", 1), "(", ""), ")", "")
                pos = "P1" & Mid(varRows, i, 1) & (j - 1)
                Select Case varCell
                    Case "LOW1" : colSamples("LOW1") = pos
                    Case "LOW2" : colSamples("LOW2") = pos
                    Case "HIGH1" : colSamples("HIGH1") = pos
                    Case "HIGH2" : colSamples("HIGH2") = pos
                        colSamples("HIGH3") = pos
                    Case "HYDRO" : colSamples("HYD") = pos
                    Case "NEG" : colSamples("NEG") = pos
                    Case "Plate_QC" : colSamples("Plate_QC") = pos
                    Case "BLANK" : colSamples("BLANK") = pos
                    Case "P1" : colSamples("P1") = pos
                    Case "P2" : colSamples("P2") = pos
                    Case "H1" : colSamples("H1") = pos
                    Case "H2" : colSamples("H2") = pos
                        colSamples("H3") = pos

                    Case Else
                        If varCell <> "" Then
                            arrSamples(c) = varCell & "^" & pos
                            c = c + 1
                        End If
                End Select
                ListBox1.Items.Add(varCell & Chr(9) & pos)
                ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            Next j
        Next i
        For i = 14 To 21
            For j = 2 To 13
                varCell = Replace(Replace(CLSLIMS.Piece(XLWS.Cells(i, j).Value, " ", 1), "(", ""), ")", "")
                pos = "P2" & Mid(varRows, (i - 10), 1) & (j - 1)
                Select Case varCell
                    Case "LOW1" : colSamples("LOW1") = pos
                    Case "LOW2" : colSamples("LOW2") = pos
                    Case "HIGH1" : colSamples("HIGH1") = pos
                    Case "HIGH2" : colSamples("HIGH2") = pos
                        colSamples("HIGH3") = pos
                    Case "HYDRO" : colSamples("HYD") = pos
                    Case "NEG" : colSamples("NEG") = pos
                    Case "Plate_QC" : colSamples("Plate_QC") = pos
                    Case "BLANK" : colSamples("BLANK") = pos
                    Case "P1" : colSamples("P1") = pos
                    Case "P2" : colSamples("P2") = pos
                    Case "H1" : colSamples("H1") = pos
                    Case "H2" : colSamples("H2") = pos
                        colSamples("H3") = pos
                    Case Else
                        If varCell <> "" Then
                            arrSamples(c) = varCell & "^" & pos
                            c = c + 1
                        End If
                End Select
                ListBox1.Items.Add(varCell & Chr(9) & pos)
                ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            Next j
        Next i

        If System.IO.File.Exists("\\cl-agilent\shared\Agilent\imports\" & TextBox1.Text & ".csv") Then
            System.IO.File.Delete("\\cl-agilent\shared\Agilent\imports\" & TextBox1.Text & ".csv")
        End If
        'If System.IO.File.Exists("D:\Agilent\imports\" & TextBox1.Text & ".csv") Then
        '    System.IO.File.Delete("D:\Agilent\imports\" & TextBox1.Text & ".csv")
        'End If

        Dim file As System.IO.StreamWriter
        file = My.Computer.FileSystem.OpenTextFileWriter("\\cl-agilent\shared\Agilent\imports\" & TextBox1.Text & ".csv", True)
        'file = My.Computer.FileSystem.OpenTextFileWriter("D:\Agilent\imports\" & TextBox1.Text & ".csv", True)
        file.WriteLine(",Sample Name,Sample Position,Method,Data File,Sample Type,Level Name,Comment,Sample Group,Info.")
        file.WriteLine(",PRIMER1,1," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",PRIMER1.d,Sample,,,,")
        file.WriteLine(",PRIMER2,1," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",PRIMER2.d,Sample,,,,")
        file.WriteLine(",PRIMER3,1," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",PRIMER3.d,Sample,,,,")
        file.WriteLine(",PRIMER4,2," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",PRIMER4.d,Sample,,,,")
        file.WriteLine(",PRIMER5,2," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",PRIMER5.d,Sample,,,,")
        file.WriteLine(",C1,1," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",C1.d,Calibration,1,,,") 'Changed To P1 And P2 per Natalie 10/30/2018 NS
        file.WriteLine(",C2,2," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",C2.d,Calibration,2,,,")
        file.WriteLine(",C3,3," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",C3.d,Calibration,3,,,") 'Changed To P1 And P2 per Natalie 10/30/2018 NS
        file.WriteLine(",C4,4," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",C4.d,Calibration,4,,,")
        If Mid(TextBox1.Text, 1, 1) <> "T" Then
            file.WriteLine(",WB,10," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",WB.d,Sample,,,,")
        End If
        file.WriteLine(",NEG," & colSamples("NEG") & "," & ComboBox1.Text & ",NEG.d,Sample,,,,")
        'file.WriteLine(",LOW3," & colSamples("LOW3") & "," & ComboBox1.Text & ",LOW3.d,Sample,,,,")
        file.WriteLine(",P1," & colSamples("P1") & "," & ComboBox1.Text & ",P1,Sample,,,,")
        file.WriteLine(",P2," & colSamples("P2") & "," & ComboBox1.Text & ",P2.d,Sample,,,,")
        file.WriteLine(",H1," & colSamples("H1") & "," & ComboBox1.Text & ",H1.d,Sample,,,,")
        file.WriteLine(",H2," & colSamples("H2") & "," & ComboBox1.Text & ",H2.d,Sample,,,,")
        file.WriteLine(",BLANK," & colSamples("BLANK") & "," & ComboBox1.Text & ",BLANK.d,Sample,,,,")
        If Mid(TextBox1.Text, 1, 1) <> "T" Then
            'file.WriteLine(",WB,10," & CLSLIMS.Piece(ComboBox1.Text, ".", 1) & " VIAL.m" & ",WB.d,Sample,,,,")
            file.WriteLine(",Plate_QC," & colSamples("Plate_QC") & "," & ComboBox1.Text & ",Plate_QC.d,Sample,,,,")
        End If
        For i = 1 To 196
            If arrSamples(i) <> "" Then
                dat = CLSLIMS.Piece(arrSamples(i), "^", 1) : pos = CLSLIMS.Piece(arrSamples(i), "^", 2)
                file.WriteLine("," & dat & "," & pos & "," & ComboBox1.Text & "," & dat & ".d,Sample,,,,")
            End If
        Next i
        file.WriteLine(",H3," & colSamples("H3") & "," & ComboBox1.Text & ",H3.d,Sample,,,,")
        file.WriteLine(",FLUSH,VIAL 10," & "Flush Out.m" & ",FLUSH.d,Sample,,,,")
        'file.WriteLine(",Script: SCP_InstrumentStandby(){MH_Acq_Scripts.exe}")
        file.Close()
        TextBox1.Text = ""
        ListBox1.Items.Add("FINISHED")
        ListBox1.SelectedIndex = ListBox1.Items.Count - 1
        XLWB.Close()
        XLWBS.Close()
        XLApp.Quit()


    End Sub

    Private Sub EP_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ComboBox1.Items.Add("PAIN-AGILENT 1.m")
        ComboBox1.Items.Add("PAIN-AGILENT 2.m")
        ComboBox1.Items.Add("PAIN-AGILENT 3.m")
        ComboBox1.Items.Add("PAIN-AGILENT 4.m")
        ComboBox1.Items.Add("PAIN-AGILENT 5.m")
        ComboBox1.Items.Add("PAIN-AGILENT 8.m")
        ComboBox1.Items.Add("THC-Barb.m")
        'ComboBox1.Items.Add("PAIN-AGILENT 6_AJS.m")
        'ComboBox1.Items.Add("Barb-THCA.m")
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class
