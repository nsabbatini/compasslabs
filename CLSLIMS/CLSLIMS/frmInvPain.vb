﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmInvPain
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim xSel As String = "Select a.PSPECNO,PLNAME,PFNAME,PSCDTA From PSPEC a, PDEM b, POC c, PORD d Where PSACT= 1 And a.puid=b.puid and a.pefdt=b.pefdt " &
            "and substring(PSRCVDT,5,2)+'/'+substring(PSRCVDT,1,4)='" & txtMonYr.Text & "' And a.PSPECNO=c.PSPECNO and a.ADSQ1=c.ADSQ1 And OC='990000' " &
            "And A.PORD=d.PORD and a.poefdt=d.poefdt and acct in ('182','S182')"
        Dim CollDt As String = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                CollDt = Mid(cRS(3), 5, 2) & "/" & Mid(cRS(3), 7, 2) & "/" & Mid(cRS(3), 1, 4)
                lstRpt.Items.Add(CLSLIMS.LJ(cRS(0), 12) & CLSLIMS.LJ(cRS(1) & ", " & cRS(2), 30) & CLSLIMS.RJ(CollDt, 15))
                GetComments(cRS(0))
            Loop
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Sub
    Public Sub GetComments(ByVal specno As String)
        Dim xSel As String = "Select pscmttxt From PSCMT a, PSCMTi b Where PSCMTACT=1 And a.PSCMTTYPE='E' And a.pspecno=b.pspecno And a.pspecno='" & specno & "' " &
            "And a.PSCMTEFDT=b.PSCMTEFDT And a.pscmttype=b.pscmttype Order by PSCMTIx"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read
                lstRpt.Items.Add("       " & rs(0))
            Loop
            rs.Close()
            Cmd.Dispose()
        End Using

        lstRpt.Items.Add("  ")
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Dim printer As New PCPrint()
        'Set the font we want to use
        printer.PrinterFont = New Font("Courier New", 10)
        'Set Margins
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        printer.DefaultPageSettings.Margins = mar

        Dim txt As String = "Interventional Pain Reject Report for " & txtMonYr.Text & vbCrLf & "---------------------------------------------" & vbCrLf
        For i = 0 To lstRpt.Items.Count - 1
            txt = txt & vbCrLf & lstRpt.Items(i)
        Next i
        printer.TextToPrint = " " & txt
        'Issue print command
        printer.Print()
    End Sub
End Class