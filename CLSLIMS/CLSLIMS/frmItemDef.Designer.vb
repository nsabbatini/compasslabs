﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemDef
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtItemDesc = New System.Windows.Forms.TextBox()
        Me.txtItmUnit = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtVndItemID = New System.Windows.Forms.TextBox()
        Me.txtMinQty = New System.Windows.Forms.TextBox()
        Me.txtTm2Recv = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtShipInfo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.rbYes = New System.Windows.Forms.RadioButton()
        Me.rbNo = New System.Windows.Forms.RadioButton()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtDept = New System.Windows.Forms.TextBox()
        Me.lblNew = New System.Windows.Forms.Label()
        Me.cbLoc = New System.Windows.Forms.ComboBox()
        Me.txtItmAbbr = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmbItemId = New System.Windows.Forms.ComboBox()
        Me.cmbVndID = New System.Windows.Forms.ComboBox()
        Me.cbUnits = New System.Windows.Forms.ComboBox()
        Me.cbVndUnits = New System.Windows.Forms.ComboBox()
        Me.txtVndItmUnit = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Vendor = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtVndItmCost = New System.Windows.Forms.TextBox()
        Me.Vendor.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(463, 575)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 33)
        Me.btnExit.TabIndex = 16
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(363, 575)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 33)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(122, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 19)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Item ID:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(49, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(151, 19)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Item Description:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(143, 124)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 19)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Units:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(67, 157)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(133, 19)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Items per Unit:"
        '
        'txtItemDesc
        '
        Me.txtItemDesc.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemDesc.Location = New System.Drawing.Point(206, 55)
        Me.txtItemDesc.Name = "txtItemDesc"
        Me.txtItemDesc.Size = New System.Drawing.Size(332, 27)
        Me.txtItemDesc.TabIndex = 2
        '
        'txtItmUnit
        '
        Me.txtItmUnit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItmUnit.Location = New System.Drawing.Point(206, 154)
        Me.txtItmUnit.Name = "txtItmUnit"
        Me.txtItmUnit.Size = New System.Drawing.Size(49, 27)
        Me.txtItmUnit.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(101, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(97, 19)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Vendor ID:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(57, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(141, 19)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Vendor Item ID:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(36, 491)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(164, 19)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Minimum Quantity:"
        '
        'txtVndItemID
        '
        Me.txtVndItemID.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVndItemID.Location = New System.Drawing.Point(204, 46)
        Me.txtVndItemID.Name = "txtVndItemID"
        Me.txtVndItemID.Size = New System.Drawing.Size(251, 27)
        Me.txtVndItemID.TabIndex = 7
        '
        'txtMinQty
        '
        Me.txtMinQty.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinQty.Location = New System.Drawing.Point(206, 488)
        Me.txtMinQty.Name = "txtMinQty"
        Me.txtMinQty.Size = New System.Drawing.Size(49, 27)
        Me.txtMinQty.TabIndex = 12
        '
        'txtTm2Recv
        '
        Me.txtTm2Recv.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTm2Recv.Location = New System.Drawing.Point(206, 422)
        Me.txtTm2Recv.Name = "txtTm2Recv"
        Me.txtTm2Recv.Size = New System.Drawing.Size(49, 27)
        Me.txtTm2Recv.TabIndex = 10
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(53, 425)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(147, 19)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "Time to Receive:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(261, 425)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 19)
        Me.Label9.TabIndex = 38
        Me.Label9.Text = "in days"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(77, 460)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(123, 19)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "Shipping Info:"
        '
        'txtShipInfo
        '
        Me.txtShipInfo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShipInfo.Location = New System.Drawing.Point(206, 455)
        Me.txtShipInfo.Name = "txtShipInfo"
        Me.txtShipInfo.Size = New System.Drawing.Size(251, 27)
        Me.txtShipInfo.TabIndex = 11
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(116, 555)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(84, 19)
        Me.Label11.TabIndex = 41
        Me.Label11.Text = "Location:"
        '
        'rbYes
        '
        Me.rbYes.AutoSize = True
        Me.rbYes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbYes.Location = New System.Drawing.Point(215, 388)
        Me.rbYes.Name = "rbYes"
        Me.rbYes.Size = New System.Drawing.Size(58, 24)
        Me.rbYes.TabIndex = 8
        Me.rbYes.TabStop = True
        Me.rbYes.Text = "Yes"
        Me.rbYes.UseVisualStyleBackColor = True
        '
        'rbNo
        '
        Me.rbNo.AutoSize = True
        Me.rbNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbNo.Location = New System.Drawing.Point(275, 388)
        Me.rbNo.Name = "rbNo"
        Me.rbNo.Size = New System.Drawing.Size(49, 24)
        Me.rbNo.TabIndex = 9
        Me.rbNo.TabStop = True
        Me.rbNo.Text = "No"
        Me.rbNo.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(53, 390)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(147, 19)
        Me.Label12.TabIndex = 44
        Me.Label12.Text = "Lot/Serial Nums:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(146, 522)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(54, 19)
        Me.Label13.TabIndex = 46
        Me.Label13.Text = "Dept:"
        '
        'txtDept
        '
        Me.txtDept.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDept.Location = New System.Drawing.Point(206, 519)
        Me.txtDept.Name = "txtDept"
        Me.txtDept.Size = New System.Drawing.Size(169, 27)
        Me.txtDept.TabIndex = 13
        '
        'lblNew
        '
        Me.lblNew.AutoSize = True
        Me.lblNew.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNew.ForeColor = System.Drawing.Color.Green
        Me.lblNew.Location = New System.Drawing.Point(330, 14)
        Me.lblNew.Name = "lblNew"
        Me.lblNew.Size = New System.Drawing.Size(45, 19)
        Me.lblNew.TabIndex = 47
        Me.lblNew.Text = "New"
        Me.lblNew.Visible = False
        '
        'cbLoc
        '
        Me.cbLoc.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbLoc.FormattingEnabled = True
        Me.cbLoc.Location = New System.Drawing.Point(206, 552)
        Me.cbLoc.Name = "cbLoc"
        Me.cbLoc.Size = New System.Drawing.Size(121, 27)
        Me.cbLoc.TabIndex = 14
        '
        'txtItmAbbr
        '
        Me.txtItmAbbr.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItmAbbr.Location = New System.Drawing.Point(206, 88)
        Me.txtItmAbbr.Name = "txtItmAbbr"
        Me.txtItmAbbr.Size = New System.Drawing.Size(125, 27)
        Me.txtItmAbbr.TabIndex = 3
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(102, 91)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(98, 19)
        Me.Label14.TabIndex = 64
        Me.Label14.Text = "Item Abbr:"
        '
        'cmbItemId
        '
        Me.cmbItemId.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbItemId.FormattingEnabled = True
        Me.cmbItemId.Location = New System.Drawing.Point(206, 14)
        Me.cmbItemId.Name = "cmbItemId"
        Me.cmbItemId.Size = New System.Drawing.Size(121, 27)
        Me.cmbItemId.TabIndex = 65
        '
        'cmbVndID
        '
        Me.cmbVndID.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbVndID.FormattingEnabled = True
        Me.cmbVndID.Location = New System.Drawing.Point(205, 14)
        Me.cmbVndID.Name = "cmbVndID"
        Me.cmbVndID.Size = New System.Drawing.Size(121, 27)
        Me.cmbVndID.TabIndex = 66
        '
        'cbUnits
        '
        Me.cbUnits.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbUnits.FormattingEnabled = True
        Me.cbUnits.Location = New System.Drawing.Point(207, 121)
        Me.cbUnits.Name = "cbUnits"
        Me.cbUnits.Size = New System.Drawing.Size(140, 27)
        Me.cbUnits.TabIndex = 67
        '
        'cbVndUnits
        '
        Me.cbVndUnits.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbVndUnits.FormattingEnabled = True
        Me.cbVndUnits.Location = New System.Drawing.Point(205, 79)
        Me.cbVndUnits.Name = "cbVndUnits"
        Me.cbVndUnits.Size = New System.Drawing.Size(140, 27)
        Me.cbVndUnits.TabIndex = 68
        '
        'txtVndItmUnit
        '
        Me.txtVndItmUnit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVndItmUnit.Location = New System.Drawing.Point(204, 112)
        Me.txtVndItmUnit.Name = "txtVndItmUnit"
        Me.txtVndItmUnit.Size = New System.Drawing.Size(49, 27)
        Me.txtVndItmUnit.TabIndex = 69
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(35, 82)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(164, 19)
        Me.Label15.TabIndex = 70
        Me.Label15.Text = "Vendor Item Units:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(2, 112)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(196, 19)
        Me.Label16.TabIndex = 71
        Me.Label16.Text = "Vendor Items per Unit:"
        '
        'Vendor
        '
        Me.Vendor.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Vendor.Controls.Add(Me.txtVndItmCost)
        Me.Vendor.Controls.Add(Me.Label17)
        Me.Vendor.Controls.Add(Me.txtVndItemID)
        Me.Vendor.Controls.Add(Me.Label16)
        Me.Vendor.Controls.Add(Me.Label5)
        Me.Vendor.Controls.Add(Me.Label15)
        Me.Vendor.Controls.Add(Me.Label6)
        Me.Vendor.Controls.Add(Me.txtVndItmUnit)
        Me.Vendor.Controls.Add(Me.cmbVndID)
        Me.Vendor.Controls.Add(Me.cbVndUnits)
        Me.Vendor.Location = New System.Drawing.Point(2, 187)
        Me.Vendor.Name = "Vendor"
        Me.Vendor.Size = New System.Drawing.Size(474, 185)
        Me.Vendor.TabIndex = 72
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(39, 150)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(158, 19)
        Me.Label17.TabIndex = 72
        Me.Label17.Text = "Vendor Item Cost:"
        '
        'txtVndItmCost
        '
        Me.txtVndItmCost.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVndItmCost.Location = New System.Drawing.Point(203, 145)
        Me.txtVndItmCost.Name = "txtVndItmCost"
        Me.txtVndItmCost.Size = New System.Drawing.Size(103, 27)
        Me.txtVndItmCost.TabIndex = 73
        '
        'frmItemDef
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(557, 613)
        Me.Controls.Add(Me.Vendor)
        Me.Controls.Add(Me.cbUnits)
        Me.Controls.Add(Me.cmbItemId)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtItmAbbr)
        Me.Controls.Add(Me.cbLoc)
        Me.Controls.Add(Me.lblNew)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtDept)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.rbNo)
        Me.Controls.Add(Me.rbYes)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtShipInfo)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtTm2Recv)
        Me.Controls.Add(Me.txtMinQty)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtItmUnit)
        Me.Controls.Add(Me.txtItemDesc)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Name = "frmItemDef"
        Me.Text = "Item Definition"
        Me.Vendor.ResumeLayout(False)
        Me.Vendor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExit As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtItemDesc As TextBox
    Friend WithEvents txtItmUnit As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtVndItemID As TextBox
    Friend WithEvents txtMinQty As TextBox
    Friend WithEvents txtTm2Recv As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtShipInfo As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents rbYes As RadioButton
    Friend WithEvents rbNo As RadioButton
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents txtDept As TextBox
    Friend WithEvents lblNew As Label
    Friend WithEvents cbLoc As ComboBox
    Friend WithEvents txtItmAbbr As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents cmbItemId As ComboBox
    Friend WithEvents cmbVndID As ComboBox
    Friend WithEvents cbUnits As ComboBox
    Friend WithEvents cbVndUnits As ComboBox
    Friend WithEvents txtVndItmUnit As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Vendor As Panel
    Friend WithEvents txtVndItmCost As TextBox
    Friend WithEvents Label17 As Label
End Class
