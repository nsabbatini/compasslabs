﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmRecvInv
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmRecvInv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpRecvDt.Format = DateTimePickerFormat.Custom
        dtpRecvDt.CustomFormat = "MM/dd/yyyy"
        dtpRecvDt.Text = Now
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        Dim Sel As String = ""
        Sel = "Select itemabbr,itemunit,itemperunit,itemdesc,Vendoritemid from ITEMS"
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            cbItem.Items.Add(rs(0))
            If rs(4) <> "" Then cbVendorItem.Items.Add(rs(4))
            lstData.Items.Add(rs(0) & "^" & rs(1) & "^" & rs(2) & "^" & rs(3))
        Loop
        rs.Close()
        Cmd.CommandText = "Select vendorid from vendors"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cbVendorId.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()
    End Sub
    Public Function GetVendorName(ByVal vndid As String) As String
        GetVendorName = ""
        Dim Sel As String = ""
        Sel = "Select VendorName from VENDORS Where VendorId = '" & vndid & "'"
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetVendorName = rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function

    Private Sub cbVendorId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbVendorId.SelectedIndexChanged
        lblVndName.Text = GetVendorName(cbVendorId.Text)
    End Sub
End Class