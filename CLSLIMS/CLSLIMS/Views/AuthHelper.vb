﻿Imports System.IO
Imports Compass.Sql.Database
Imports Compass.Global
Imports Compass.MySql.Database
Imports System.Linq

Public Class AuthHelper
    Implements IDisposable

    Public Sub New()
        MyBase.New()
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                ' mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Private _dirPathToFIle As String = System.AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = ""
    Public Property _userName As String
    Public Property _password As String
    Public Function CheckUserCreds() As Boolean

        Dim getuserinfo = IsUserValid()

        Dim username = getuserinfo(0).GetType().GetProperty("UID")?.GetValue(getuserinfo(0))

        If username = _userName Then
            Return True
        Else
            Return False
        End If


    End Function
    Public Function IsUserValid() As Object

        Try
            Using validate As New AuthorizeUser()

                _fileName = _fileDir & AuthUserSql
                validate._sqlText = ReadSqlFile(_fileName)
                Return validate.LogInUser.Where(Function(x) x.UID = _userName AndAlso x.UPWD = _password).ToList()

            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "AuthHelper.IsUserValid", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try

    End Function
End Class
