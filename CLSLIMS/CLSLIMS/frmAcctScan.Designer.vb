﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAcctScan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ScanImage = New Dynamsoft.DotNet.TWAIN.DynamicDotNetTwain()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lblAcctName = New System.Windows.Forms.Label()
        Me.lstAcctNameSearch = New System.Windows.Forms.ListBox()
        Me.rbComplete = New System.Windows.Forms.RadioButton()
        Me.rbAppend = New System.Windows.Forms.RadioButton()
        Me.btnA = New System.Windows.Forms.Button()
        Me.btnB = New System.Windows.Forms.Button()
        Me.btnC = New System.Windows.Forms.Button()
        Me.btnD = New System.Windows.Forms.Button()
        Me.btnE = New System.Windows.Forms.Button()
        Me.btnF = New System.Windows.Forms.Button()
        Me.btnG = New System.Windows.Forms.Button()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(26, 27)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Account:"
        '
        'ScanImage
        '
        Me.ScanImage.AnnotationFillColor = System.Drawing.Color.White
        Me.ScanImage.AnnotationPen = Nothing
        Me.ScanImage.AnnotationTextColor = System.Drawing.Color.Black
        Me.ScanImage.AnnotationTextFont = Nothing
        Me.ScanImage.IfShowCancelDialogWhenImageTransfer = False
        Me.ScanImage.IfThrowException = False
        Me.ScanImage.Location = New System.Drawing.Point(210, 58)
        Me.ScanImage.LogLevel = CType(0, Short)
        Me.ScanImage.Margin = New System.Windows.Forms.Padding(2)
        Me.ScanImage.Name = "ScanImage"
        Me.ScanImage.PDFMarginBottom = CType(0UI, UInteger)
        Me.ScanImage.PDFMarginLeft = CType(0UI, UInteger)
        Me.ScanImage.PDFMarginRight = CType(0UI, UInteger)
        Me.ScanImage.PDFMarginTop = CType(0UI, UInteger)
        Me.ScanImage.PDFXConformance = CType(0UI, UInteger)
        Me.ScanImage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ScanImage.Size = New System.Drawing.Size(535, 615)
        Me.ScanImage.TabIndex = 1
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(88, 25)
        Me.txtAcct.Margin = New System.Windows.Forms.Padding(2)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(76, 24)
        Me.txtAcct.TabIndex = 0
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(55, 592)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(2)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(56, 26)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lblAcctName
        '
        Me.lblAcctName.AutoSize = True
        Me.lblAcctName.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcctName.Location = New System.Drawing.Point(223, 25)
        Me.lblAcctName.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblAcctName.Name = "lblAcctName"
        Me.lblAcctName.Size = New System.Drawing.Size(0, 19)
        Me.lblAcctName.TabIndex = 5
        '
        'lstAcctNameSearch
        '
        Me.lstAcctNameSearch.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAcctNameSearch.FormattingEnabled = True
        Me.lstAcctNameSearch.ItemHeight = 16
        Me.lstAcctNameSearch.Location = New System.Drawing.Point(187, 11)
        Me.lstAcctNameSearch.Margin = New System.Windows.Forms.Padding(2)
        Me.lstAcctNameSearch.Name = "lstAcctNameSearch"
        Me.lstAcctNameSearch.Size = New System.Drawing.Size(85, 20)
        Me.lstAcctNameSearch.TabIndex = 6
        Me.lstAcctNameSearch.Visible = False
        '
        'rbComplete
        '
        Me.rbComplete.AutoSize = True
        Me.rbComplete.Checked = True
        Me.rbComplete.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbComplete.Location = New System.Drawing.Point(27, 58)
        Me.rbComplete.Margin = New System.Windows.Forms.Padding(2)
        Me.rbComplete.Name = "rbComplete"
        Me.rbComplete.Size = New System.Drawing.Size(84, 21)
        Me.rbComplete.TabIndex = 7
        Me.rbComplete.TabStop = True
        Me.rbComplete.Text = "Complete"
        Me.rbComplete.UseVisualStyleBackColor = True
        '
        'rbAppend
        '
        Me.rbAppend.AutoSize = True
        Me.rbAppend.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbAppend.Location = New System.Drawing.Point(108, 58)
        Me.rbAppend.Margin = New System.Windows.Forms.Padding(2)
        Me.rbAppend.Name = "rbAppend"
        Me.rbAppend.Size = New System.Drawing.Size(73, 21)
        Me.rbAppend.TabIndex = 8
        Me.rbAppend.TabStop = True
        Me.rbAppend.Text = "Append"
        Me.rbAppend.UseVisualStyleBackColor = True
        '
        'btnA
        '
        Me.btnA.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnA.Location = New System.Drawing.Point(27, 97)
        Me.btnA.Margin = New System.Windows.Forms.Padding(2)
        Me.btnA.Name = "btnA"
        Me.btnA.Size = New System.Drawing.Size(119, 49)
        Me.btnA.TabIndex = 9
        Me.btnA.Text = "A"
        Me.btnA.UseVisualStyleBackColor = True
        '
        'btnB
        '
        Me.btnB.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnB.Location = New System.Drawing.Point(27, 164)
        Me.btnB.Margin = New System.Windows.Forms.Padding(2)
        Me.btnB.Name = "btnB"
        Me.btnB.Size = New System.Drawing.Size(119, 49)
        Me.btnB.TabIndex = 10
        Me.btnB.Text = "B"
        Me.btnB.UseVisualStyleBackColor = True
        '
        'btnC
        '
        Me.btnC.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnC.Location = New System.Drawing.Point(27, 231)
        Me.btnC.Margin = New System.Windows.Forms.Padding(2)
        Me.btnC.Name = "btnC"
        Me.btnC.Size = New System.Drawing.Size(119, 49)
        Me.btnC.TabIndex = 11
        Me.btnC.Text = "C"
        Me.btnC.UseVisualStyleBackColor = True
        '
        'btnD
        '
        Me.btnD.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnD.Location = New System.Drawing.Point(27, 298)
        Me.btnD.Margin = New System.Windows.Forms.Padding(2)
        Me.btnD.Name = "btnD"
        Me.btnD.Size = New System.Drawing.Size(119, 49)
        Me.btnD.TabIndex = 12
        Me.btnD.Text = "D"
        Me.btnD.UseVisualStyleBackColor = True
        '
        'btnE
        '
        Me.btnE.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnE.Location = New System.Drawing.Point(27, 365)
        Me.btnE.Margin = New System.Windows.Forms.Padding(2)
        Me.btnE.Name = "btnE"
        Me.btnE.Size = New System.Drawing.Size(119, 49)
        Me.btnE.TabIndex = 13
        Me.btnE.Text = "E"
        Me.btnE.UseVisualStyleBackColor = True
        '
        'btnF
        '
        Me.btnF.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnF.Location = New System.Drawing.Point(27, 432)
        Me.btnF.Margin = New System.Windows.Forms.Padding(2)
        Me.btnF.Name = "btnF"
        Me.btnF.Size = New System.Drawing.Size(119, 49)
        Me.btnF.TabIndex = 14
        Me.btnF.Text = "F"
        Me.btnF.UseVisualStyleBackColor = True
        '
        'btnG
        '
        Me.btnG.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnG.Location = New System.Drawing.Point(29, 502)
        Me.btnG.Margin = New System.Windows.Forms.Padding(2)
        Me.btnG.Name = "btnG"
        Me.btnG.Size = New System.Drawing.Size(119, 49)
        Me.btnG.TabIndex = 15
        Me.btnG.Text = "G"
        Me.btnG.UseVisualStyleBackColor = True
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.BackColor = System.Drawing.Color.RoyalBlue
        Me.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblInfo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.ForeColor = System.Drawing.Color.White
        Me.lblInfo.Location = New System.Drawing.Point(27, 635)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(111, 21)
        Me.lblInfo.TabIndex = 16
        Me.lblInfo.Text = "                    "
        '
        'frmAcctScan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(756, 684)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.btnG)
        Me.Controls.Add(Me.btnF)
        Me.Controls.Add(Me.btnE)
        Me.Controls.Add(Me.btnD)
        Me.Controls.Add(Me.btnC)
        Me.Controls.Add(Me.btnB)
        Me.Controls.Add(Me.btnA)
        Me.Controls.Add(Me.rbAppend)
        Me.Controls.Add(Me.rbComplete)
        Me.Controls.Add(Me.lblAcctName)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.ScanImage)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstAcctNameSearch)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmAcctScan"
        Me.Text = "Scan Account Docs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ScanImage As Dynamsoft.DotNet.TWAIN.DynamicDotNetTwain
    Friend WithEvents txtAcct As System.Windows.Forms.TextBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lblAcctName As System.Windows.Forms.Label
    Friend WithEvents lstAcctNameSearch As System.Windows.Forms.ListBox
    Friend WithEvents rbComplete As System.Windows.Forms.RadioButton
    Friend WithEvents rbAppend As System.Windows.Forms.RadioButton
    Friend WithEvents btnA As Button
    Friend WithEvents btnB As Button
    Friend WithEvents btnC As Button
    Friend WithEvents btnD As Button
    Friend WithEvents btnE As Button
    Friend WithEvents btnF As Button
    Friend WithEvents btnG As Button
    Friend WithEvents lblInfo As Label
End Class
