﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpC
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.btnImpC = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lstInfo
        '
        Me.lstInfo.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.FormattingEnabled = True
        Me.lstInfo.ItemHeight = 19
        Me.lstInfo.Location = New System.Drawing.Point(14, 13)
        Me.lstInfo.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(882, 346)
        Me.lstInfo.TabIndex = 8
        '
        'btnImpC
        '
        Me.btnImpC.Location = New System.Drawing.Point(351, 382)
        Me.btnImpC.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.btnImpC.Name = "btnImpC"
        Me.btnImpC.Size = New System.Drawing.Size(104, 34)
        Me.btnImpC.TabIndex = 7
        Me.btnImpC.Text = "Import C"
        Me.btnImpC.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(657, 383)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(84, 33)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'frmImpC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(932, 457)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.btnImpC)
        Me.Controls.Add(Me.btnExit)
        Me.Name = "frmImpC"
        Me.Text = "frmImpC"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lstInfo As ListBox
    Friend WithEvents btnImpC As Button
    Friend WithEvents btnExit As Button
End Class
