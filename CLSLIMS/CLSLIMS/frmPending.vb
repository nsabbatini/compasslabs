﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class frmPending
    Private Sub frmPending_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpFrom.Format = DateTimePickerFormat.Custom
        dtpFrom.CustomFormat = "MM/dd/yyyy"
        dtpFrom.Text = DateAdd(DateInterval.Day, -3, Now)
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        Call BuildPending()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        PrintDoc.Print()

        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPreview_Click(sender As Object, e As EventArgs) Handles btnPreview.Click
        Call BuildPending()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        PrintPreview.ShowDialog()

    End Sub

    Public Sub BuildPending()
        Dim Sel As String, p As String, c As Integer = 1, Age As Integer = 3, SelDt As String = ""
        SelDt = "#" & dtpFrom.Text & "#"
        Age = CInt(DateDiff(DateInterval.Day, CDate(SelDt), Now))
        Sel = "select distinct acct,a.pspecno,plname+', '+pfname,Substring(PSRCVDT,5,2)+'/'+Substring(PSRCVDT,7,2)+'/'+Substring(PSRCVDT,1,4)," &
            "datediff(day,substring(psrcvdt,1,4)+'-'+substring(psrcvdt,5,2)+'-'+substring(psrcvdt,7,2),current_timestamp) as Age " &
            "from qpres a, pspec b, pord c, pdem d " &
            "where a.pspecno=b.pspecno and b.pord=c.pord and b.poefdt=c.poefdt and b.puid=d.puid and b.pefdt=d.pefdt " &
            "and psact=1 and poact=1 and datediff(day,substring(psrcvdt,1,4)+'-'+substring(psrcvdt,5,2)+'-'+substring(psrcvdt,7,2),current_timestamp) > " & Age & " " &
            "order by acct,Age desc"

        '"and psrcvdt > '" & CLSLIMS.Piece(dtpFrom.Text, "/", 3) & CLSLIMS.Piece(dtpFrom.Text, "/", 1) & CLSLIMS.Piece(dtpFrom.Text, "/", 2) & "000000'" &

        lstData.Items.Clear()
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            If cbIncStatCd.Checked = False Or (cbIncStatCd.Checked = True And CheckHoldStatus(rs(1))) Then
                p = CLSLIMS.RJ(c, 4) & ")" & Space(2) & CLSLIMS.RJ(rs(0), 12) & Space(3) & CLSLIMS.LJ(rs(1), 12) & Space(3) & CLSLIMS.LJ(rs(2), 30) & Space(3) & rs(3) & Space(3) & CLSLIMS.RJ(rs(4), 10)
                lstData.Items.Add(p)
                Debug.Print(p)
                c = c + 1
            End If
        Loop
        rs.Close()
        Conn.Close()
        Conn.Dispose()
    End Sub
    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 25

        Static PageNo As Integer = 1
        Dim y As Integer
        'e.Graphics.DrawString("AEL", Font2, Brushes.Black, 330, 50)
        e.Graphics.DrawString("Pending Report", Font2, Brushes.Black, 250, 100)
        e.Graphics.DrawString("For " & dtpFrom.Text, Font3, Brushes.Black, 25, 145)
        e.Graphics.DrawString(IIf(cbIncStatCd.Checked, "EXCLUDE STATUS CODES", "INCLUDE STATUS CODES"), Font3, Brushes.Black, 180, 145)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 690, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        e.Graphics.DrawString("                                                                    Receive         Specimen", Font, Brushes.Black, 45, 185)
        e.Graphics.DrawString("Seq     Account   Specimen #        Patient Name                     Date             Age", Font, Brushes.Black, 45, 200)

        e.Graphics.DrawLine(Pens.Black, 25, 180, 1075, 180)   'Horizonal line above headings
        e.Graphics.DrawLine(Pens.Black, 25, 220, 1075, 220)   'Horizonal line under headings

        'e.Graphics.DrawLine(Pens.Black, 108, 180, 108, 1080)   'Veritcal line 2
        'e.Graphics.DrawLine(Pens.Black, 315, 180, 315, 1080)   'Veritcal line 3
        'e.Graphics.DrawLine(Pens.Black, 405, 180, 405, 1080)   'Veritcal line 4
        'e.Graphics.DrawLine(Pens.Black, 435, 180, 435, 1080)      'Vertical line 1
        'e.Graphics.DrawLine(Pens.Black, 520, 180, 520, 1080)   'Veritcal line 5
        'e.Graphics.DrawLine(Pens.Black, 590, 180, 590, 1080)   'Veritcal line 6


        Dim c As Integer
        Static intStart As Integer

        c = 1
        y = 250
        For i = intStart To lstData.Items.Count - 1
            'data line
            e.Graphics.DrawString(lstData.Items(i).ToString, Font, Brushes.Black, 25, y)
            e.Graphics.DrawLine(Pens.Black, 25, y + 20, 1075, y + 20) 'Horizonal line after sample
            y = y + 30
            c = c + 1
            If c > PageLngth Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i
    End Sub
    Public Function CheckHoldStatus(ByVal specno As String) As Boolean
        CheckHoldStatus = True
        Dim Sel As String
        Sel = "select count(*) from psstat a, pspec b, statcd c where a.pspecno='" & specno & "' And a.adsq1=b.adsq1 and a.pspecno=b.pspecno and psact=1 " &
            "and c.statcd=a.statcd and stcdact=1 and stcdnvr=0"

        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            If Val(rs(0)) > 0 Then
                CheckHoldStatus = False
            End If
        Loop
        rs.Close()
        Conn.Close()
        Conn.Dispose()
    End Function
End Class