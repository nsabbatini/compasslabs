﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmSOSMonRpt
    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim Sel As String = "select acct,aname,city,state,b.itemid,itemdesc,sum(qty) " &
        "From orders a, orderitems b, items c " &
        "where a.hseq=0 and b.hseq=0 and a.ordernum=b.ordernum and b.itemid=c.itemid " &
        "and a.status=10 and substring(orderdate,1,4)='" & CLSLIMS.Piece(cmbDate.Text, "/", 1) & "' and substring(orderdate,5,2)='" & CLSLIMS.Piece(cmbDate.Text, "/", 2) & "' " &
        "group by acct,aname,city,state,b.itemid,itemdesc "
        lvRpt.Items.Clear()
        Dim r As String = "", Acct As String = "", MoYr As String = "", pAcct As String = ""
        Dim lv As ListViewItem = Nothing
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            Acct = rs(0)
            If Acct <> pAcct Then
                r = GetVol(Acct, Replace(cmbDate.Text, "/", ""))
                pAcct = Acct
            End If
            lv = lvRpt.Items.Add(rs(0))
            lv.SubItems.Add(rs(1))
            lv.SubItems.Add(rs(2))
            lv.SubItems.Add(rs(3))
            lv.SubItems.Add(rs(4))
            lv.SubItems.Add(rs(5))
            lv.SubItems.Add(rs(6))
            'Add account volume
            lv.SubItems.Add(r)
        Loop
        rs.Close()
        ConnMySql.Close()


    End Sub
    Private Sub frmSOSMonRpt_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim Sel As String = "Select distinct substring(orderdate,1,4) ,substring(orderdate,5,2) from orders order by substring(orderdate,1,4) ,substring(orderdate,5,2)"
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            cmbDate.Items.Add(rs(0) & "/" & rs(1))
        Loop
        rs.Close()
        ConnMySql.Close()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\MonAcctRpt.csv", False)
        file.WriteLine("," & cmbDate.Text)
        file.WriteLine("Account,Account Name,City,State,Item,Item Description,Totals,Volume")
        For i = 0 To lvRpt.Items.Count - 1
            For j = 0 To 7
                If Not (lvRpt.Items(i).SubItems(j) Is Nothing) Then file.Write(Replace(lvRpt.Items(i).SubItems(j).Text, ",", " ") & ",")
            Next j
            file.WriteLine()
        Next i
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\MonAcctRpt.csv")
        Me.Close()
        Me.Dispose()
    End Sub
    Public Function GetVol(ByVal acct As String, ByVal dt As String) As String
        'dt = "YYYYMM"
        GetVol = ""
        Dim SQL As String = "Select COUNT(*) as cnt " &
                            "From PORD C, PSPEC A " &
                            "Where C.PORD = A.PORD And C.POEFDT = A.POEFDT " &
                            "And substring(A.PSRCVDT,1,6) ='" & dt & "' " &
                            "And C.ACCT = '" & acct & "' " &
                            "And POACT=1 And PSACT=1 "
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(SQL, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetVol = cRS(0)
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
End Class