﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetClientOrders
    Inherits DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetClientOrders))
        Me.FluentDesignFormContainer1 = New DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer()
        Me.AccordionControl1 = New DevExpress.XtraBars.Navigation.AccordionControl()
        Me.AccordionControlElement1 = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnNewOrder = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnOrderHistory = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnPrintPickSheet = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.AccordionControlSeparator1 = New DevExpress.XtraBars.Navigation.AccordionControlSeparator()
        Me.FluentDesignFormControl1 = New DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl()
        Me.FluentFormDefaultManager1 = New DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager(Me.components)
        Me.AccordionControlElement2 = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnApproveOrder = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.btnSaveOrder = New DevExpress.XtraBars.Navigation.AccordionControlElement()
        Me.mainPic = New System.Windows.Forms.PictureBox()
        Me.FluentDesignFormContainer1.SuspendLayout()
        CType(Me.AccordionControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FluentDesignFormControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FluentFormDefaultManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mainPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FluentDesignFormContainer1
        '
        Me.FluentDesignFormContainer1.Controls.Add(Me.mainPic)
        Me.FluentDesignFormContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FluentDesignFormContainer1.Location = New System.Drawing.Point(302, 31)
        Me.FluentDesignFormContainer1.Name = "FluentDesignFormContainer1"
        Me.FluentDesignFormContainer1.Size = New System.Drawing.Size(1496, 1068)
        Me.FluentDesignFormContainer1.TabIndex = 0
        '
        'AccordionControl1
        '
        Me.AccordionControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.AccordionControl1.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {Me.AccordionControlElement1})
        Me.AccordionControl1.Location = New System.Drawing.Point(0, 31)
        Me.AccordionControl1.Name = "AccordionControl1"
        Me.AccordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Touch
        Me.AccordionControl1.Size = New System.Drawing.Size(302, 1068)
        Me.AccordionControl1.TabIndex = 1
        Me.AccordionControl1.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu
        '
        'AccordionControlElement1
        '
        Me.AccordionControlElement1.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {Me.btnNewOrder, Me.btnOrderHistory, Me.btnPrintPickSheet, Me.AccordionControlSeparator1, Me.AccordionControlElement2})
        Me.AccordionControlElement1.Expanded = True
        Me.AccordionControlElement1.Name = "AccordionControlElement1"
        Me.AccordionControlElement1.Text = "Actions"
        '
        'btnNewOrder
        '
        Me.btnNewOrder.ImageOptions.Image = CType(resources.GetObject("AccordionControlElement2.ImageOptions.Image"), System.Drawing.Image)
        Me.btnNewOrder.Name = "btnNewOrder"
        Me.btnNewOrder.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnNewOrder.Text = "New Order"
        '
        'btnOrderHistory
        '
        Me.btnOrderHistory.ImageOptions.Image = CType(resources.GetObject("AccordionControlElement3.ImageOptions.Image"), System.Drawing.Image)
        Me.btnOrderHistory.Name = "btnOrderHistory"
        Me.btnOrderHistory.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnOrderHistory.Text = "Order History"
        '
        'btnPrintPickSheet
        '
        Me.btnPrintPickSheet.ImageOptions.Image = CType(resources.GetObject("AccordionControlElement4.ImageOptions.Image"), System.Drawing.Image)
        Me.btnPrintPickSheet.Name = "btnPrintPickSheet"
        Me.btnPrintPickSheet.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnPrintPickSheet.Text = "Print Pick Sheet"
        '
        'AccordionControlSeparator1
        '
        Me.AccordionControlSeparator1.Name = "AccordionControlSeparator1"
        '
        'FluentDesignFormControl1
        '
        Me.FluentDesignFormControl1.FluentDesignForm = Me
        Me.FluentDesignFormControl1.Location = New System.Drawing.Point(0, 0)
        Me.FluentDesignFormControl1.Manager = Me.FluentFormDefaultManager1
        Me.FluentDesignFormControl1.Name = "FluentDesignFormControl1"
        Me.FluentDesignFormControl1.Size = New System.Drawing.Size(1798, 31)
        Me.FluentDesignFormControl1.TabIndex = 2
        Me.FluentDesignFormControl1.TabStop = False
        '
        'FluentFormDefaultManager1
        '
        Me.FluentFormDefaultManager1.DockingEnabled = False
        Me.FluentFormDefaultManager1.Form = Me
        '
        'AccordionControlElement2
        '
        Me.AccordionControlElement2.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {Me.btnApproveOrder, Me.btnSaveOrder})
        Me.AccordionControlElement2.Expanded = True
        Me.AccordionControlElement2.Name = "AccordionControlElement2"
        Me.AccordionControlElement2.Text = "Approve Actions"
        '
        'btnApproveOrder
        '
        Me.btnApproveOrder.ImageOptions.Image = CType(resources.GetObject("btnApproveOrder.ImageOptions.Image"), System.Drawing.Image)
        Me.btnApproveOrder.Name = "btnApproveOrder"
        Me.btnApproveOrder.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnApproveOrder.Text = "Approve Order"
        '
        'btnSaveOrder
        '
        Me.btnSaveOrder.ImageOptions.Image = CType(resources.GetObject("btnSaveOrder.ImageOptions.Image"), System.Drawing.Image)
        Me.btnSaveOrder.Name = "btnSaveOrder"
        Me.btnSaveOrder.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
        Me.btnSaveOrder.Text = "Save Order"
        '
        'mainPic
        '
        Me.mainPic.BackColor = System.Drawing.Color.Olive
        Me.mainPic.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mainPic.Image = CType(resources.GetObject("mainPic.Image"), System.Drawing.Image)
        Me.mainPic.Location = New System.Drawing.Point(0, 0)
        Me.mainPic.Name = "mainPic"
        Me.mainPic.Size = New System.Drawing.Size(1496, 1068)
        Me.mainPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.mainPic.TabIndex = 0
        Me.mainPic.TabStop = False
        '
        'frmSetClientOrders
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1798, 1099)
        Me.ControlContainer = Me.FluentDesignFormContainer1
        Me.Controls.Add(Me.FluentDesignFormContainer1)
        Me.Controls.Add(Me.AccordionControl1)
        Me.Controls.Add(Me.FluentDesignFormControl1)
        Me.FluentDesignFormControl = Me.FluentDesignFormControl1
        Me.Name = "frmSetClientOrders"
        Me.NavigationControl = Me.AccordionControl1
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Client Order Form"
        Me.FluentDesignFormContainer1.ResumeLayout(False)
        CType(Me.AccordionControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FluentDesignFormControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FluentFormDefaultManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mainPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FluentDesignFormContainer1 As DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer
    Friend WithEvents AccordionControl1 As DevExpress.XtraBars.Navigation.AccordionControl
    Friend WithEvents AccordionControlElement1 As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents FluentDesignFormControl1 As DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl
    Friend WithEvents FluentFormDefaultManager1 As DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager
    Friend WithEvents btnNewOrder As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnOrderHistory As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnPrintPickSheet As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents AccordionControlSeparator1 As DevExpress.XtraBars.Navigation.AccordionControlSeparator
    Friend WithEvents AccordionControlElement2 As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnApproveOrder As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents btnSaveOrder As DevExpress.XtraBars.Navigation.AccordionControlElement
    Friend WithEvents mainPic As PictureBox
End Class
