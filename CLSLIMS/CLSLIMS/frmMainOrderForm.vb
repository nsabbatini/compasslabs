﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Report.Library
Imports GrapeCity.ActiveReports
Imports Compass.Sql.Database
Imports Compass.User.Controls

Public Class frmMainOrderForm
    Public _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dtInventory As DataTable = New DataTable
    Private _line As Int16 = 0
    Private _orderNumber As Int16
    Private ucOrders = New uClientInfo
    Private Sub frmMainOrderForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If

        _line = 0

    End Sub

    Private Sub btnNewOrder_Click(sender As Object, e As EventArgs) Handles btnNewOrder.Click
        With ucOrders
            ._fileDir = _fileDir
            ._fileName = _fileName
            .LoadTheForm()
        End With

        pnlMainOrder.Visible = True
        pnlMainOrder.Controls.Add(ucOrders)


    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()

    End Sub
End Class
