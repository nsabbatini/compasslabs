﻿Public Class frmGeneric
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub tmGeneric_Tick(sender As Object, e As EventArgs) Handles tmGeneric.Tick
        tmGeneric.Enabled = False
        Me.Close()
        Me.Dispose()
    End Sub
End Class