﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class FldrAglnt

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ins As String
        Dim Sel As String = "Select count(*) From XTRAYS Where FOLDER='" & txtFldr.Text & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        txtFldr.Text = UCase(txtFldr.Text)
        Debug.Print(txtFldr.Text)
        If Mid(txtFldr.Text, 1, 1) <> "T" Then
            'Check valid Folder
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            rs.Read()
            If rs(0) = 0 Then
                MsgBox("Invalid Folder.")
                txtFldr.Focus()
                Exit Sub
            End If
            rs.Close()
        ElseIf Mid(txtFldr.Text, 1, 1) = "T" Then
            If Not System.IO.File.Exists("\\cl-agilent\shared\EP Motion\Archive\" & "THC" & txtFldr.Text & ".xlsx") Then
                MsgBox("Invalid THC Folder.")
                txtFldr.Focus()
                Exit Sub
            End If
        End If
        If cmbAgilent.Text = "" Then
            MsgBox("Select an Agilent.")
            cmbAgilent.Focus()
            Exit Sub
        End If
        'Get nxtSeq
        Dim nxtSeq As Integer
        Cmd.CommandText = "Select Max(ADSQ1) From XFOLDERS Where FOLDER='" & txtFldr.Text & "'"
        Dim rs2 As SqlDataReader = Cmd.ExecuteReader
        rs2.Read()
        nxtSeq = Val(IIf(IsDBNull(rs2(0)), 0, rs2(0))) + 1
        'Insert
        Dim CmdIns As New SqlCommand
        CmdIns.Connection = Conn
        ins = "Insert into XFOLDERS (FOLDER, INSTRUMENT, ADSQ1, FLDEFDT) " & _
             "Values ('" & txtFldr.Text & "','" & cmbAgilent.Text & "'," & nxtSeq & ",'" & Format(Now, "yyyyMMddHHmmss") & "')"
        CmdIns.CommandText = ins
        CmdIns.ExecuteNonQuery()


        CmdIns.Dispose()
        Cmd.Dispose()
        txtFldr.Text = ""
        cmbAgilent.SelectedIndex = -1
        txtFldr.Focus()

    End Sub

    Private Sub txtFldr_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFldr.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        lbFolder.Items.Clear()
        Dim sel = "Select INSTRUMENT, ADSQ1, FLDEFDT From XFOLDERS Where FOLDER ='" & txtFldr.Text & "' Order By ADSQ1"
        Dim strDt
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            strDt = Mid(rs(2), 5, 2) & "/" & Mid(rs(2), 7, 2) & "/" & Mid(rs(2), 1, 4) & Space(2) & Mid(rs(2), 9, 2) & ":" & Mid(rs(2), 11, 2)
            lbFolder.Items.Add(CLSLIMS.RJ(rs(1), 4) & Space(4) & CLSLIMS.RJ(rs(0), 10) & Space(5) & strDt)
        Loop
        cmbAgilent.Focus()
    End Sub
End Class