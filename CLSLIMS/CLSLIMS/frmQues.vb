﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports iTextSharp
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports System.Runtime.InteropServices

Public Class frmQues
    Public OrdNumApp(1000) As String, OrdNumPrt(1000) As String, OrdNumPick(1000) As String, OrdNumShip(1000) As String, OrdNumReq(1000) As String
    Public OrderRec As String = "", OrderPage As Boolean = False, CurrPrinter As String = ""
    Public TextToDisplay As String = ""
    Public pdfDoc As Document
    Public pdfWrite As PdfWriter
    Public TahomaFont As BaseFont = BaseFont.CreateFont("C:\Windows\Fonts\tahomabd.ttf", BaseFont.CP1250, False)
    Public Tahoma6 As Font = FontFactory.GetFont("Tahoma", 6, FontStyle.Regular)
    Public Tahoma8 As Font = FontFactory.GetFont("Tahoma", 8, FontStyle.Regular)
    Public Tahoma8B As Font = FontFactory.GetFont("Tahoma", 8, FontStyle.Bold)
    Public Tahoma10 As Font = FontFactory.GetFont("Tahoma", 10, FontStyle.Bold)
    Public Tahoma10R As Font = FontFactory.GetFont("Tahoma", 10, FontStyle.Bold)
    Public Tahoma12 As Font = FontFactory.GetFont("Tahoma", 12, FontStyle.Bold)
    Public Tahoma15 As Font = FontFactory.GetFont("Tahoma", 15, FontStyle.Bold)
    Public Tahoma20 As Font = FontFactory.GetFont("Tahoma", 20, FontStyle.Bold)
    Public Tahoma15U As Font = FontFactory.GetFont("Tahoma", 15, FontStyle.Bold + FontStyle.Underline)
    Public Barcode128 As Font = FontFactory.GetFont("Code 128", 48, FontStyle.Regular)
    Public imgLogo As Image = Image.GetInstance("Compass logo.jpg")
    'Public imgLeft As Image = Image.GetInstance("LeftArrow.jpg")
    Private Sub frmQues_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'PrintReqCOVID(3303, 1)
        LoadQueues()
    End Sub
    Public Sub LoadQueues()
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        ReDim OrdNumApp(1000) : ReDim OrdNumPrt(1000) : ReDim OrdNumPick(1000) : ReDim OrdNumReq(1000)
        lstOrdApp.Items.Clear()
        lstPrt.Items.Clear()
        lstReqNum.Items.Clear()
        lstPick.Items.Clear()
        lstShip.Items.Clear()
        'Approval
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        'Need Approving
        Cmd.CommandText = "Select * From Orders Where Status < 5 and HSEQ=0"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            lstOrdApp.Items.Add(CLSLIMS.FormatListBox(rs(0) & ";5^" & rs(3) & ";10^" & "  " & ";2;^" & rs(4) & ";30;LJ^"))
            OrdNumApp(lstOrdApp.Items.Count - 1) = rs(0)
        Loop
        rs.Close()
        'Need Printing
        Cmd.CommandText = "Select * From OrderItems Where Status = 5 and HSEQ=0 and ItemID in ('OREQ','TREQ','CREQ','UPSLABEL','FELABELS')"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            lstPrt.Items.Add(CLSLIMS.FormatListBox(rs(0) & ";5^" & rs(3) & ";10^" & rs(4) & ";10"))
            OrdNumPrt(lstPrt.Items.Count - 1) = rs(0) & "^" & rs(2) & "^" & rs(3)
        Loop
        rs.Close()
        'Need Req nums
        Cmd.CommandText = "Select * From OrderItems Where Status = 6 and HSEQ=0"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            lstReqNum.Items.Add(CLSLIMS.FormatListBox(rs(0) & ";5^" & rs(3) & ";10^" & rs(4) & ";10^"))
            OrdNumReq(lstReqNum.Items.Count - 1) = rs(0) & "^" & rs(2) & "^" & rs(3)
        Loop
        rs.Close()
        'Need Picking
        Cmd.CommandText = "Select * From OrderItems Where Status < 8 and HSEQ=0"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            If (rs(3) = "OREQ" Or rs(3) = "TREQ" Or rs(3) = "UPSLABEL" Or rs(3) = "FELABELS") And rs(5) = 7 Then
                lstPick.Items.Add(CLSLIMS.FormatListBox(rs(0) & ";5^" & rs(3) & ";10^" & rs(4) & ";15"))
                OrdNumPick(lstPick.Items.Count - 1) = rs(0) & "^" & rs(2) & "^" & rs(3)
            ElseIf rs(3) <> "OREQ" And rs(3) <> "TREQ" And rs(3) <> "UPSLABEL" And rs(3) <> "FELABELS" Then
                lstPick.Items.Add(CLSLIMS.FormatListBox(rs(0) & ";5^" & rs(3) & ";10^" & rs(4) & ";15"))
                OrdNumPick(lstPick.Items.Count - 1) = rs(0) & "^" & rs(2) & "^" & rs(3)
            End If
        Loop
        rs.Close()
        'Need shipping
        Cmd.CommandText = "Select * From Orders a Where a.Status = 5 And a.HSEQ=0 " &
                    "and ordernum in (select ordernum from orderitems b where b.OrderNum=a.ordernum and hseq=0 and status = 8)"

        rs = Cmd.ExecuteReader
        Do While rs.Read
            lstShip.Items.Add(CLSLIMS.FormatListBox(rs(0) & ";5^" & rs(3) & ";10^" & "  " & ";2;^" & rs(4) & ";30;LJ^"))
            OrdNumShip(lstShip.Items.Count - 1) = rs(0)
        Loop
        rs.Close()

    End Sub

    Private Sub PrintReq_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintReq.PrintPage
        Dim ReqFont As System.Drawing.Font = New System.Drawing.Font("Tahoma", 10, FontStyle.Bold)
        Dim OrderFont As System.Drawing.Font = New System.Drawing.Font("Tahoma", 26)
        Dim MRNFont As System.Drawing.Font = New Drawing.Font("Tahoma", 14)
        Dim CernerAcct As String = ""
        If Not OrderPage Then
            e.Graphics.DrawString("Account #:      " & CLSLIMS.Piece(OrderRec, "^", 1), ReqFont, Brushes.Black, 0, 0)
            e.Graphics.DrawString(CLSLIMS.Piece(OrderRec, "^", 2), ReqFont, Brushes.Black, 0, 15)
            e.Graphics.DrawString(CLSLIMS.Piece(OrderRec, "^", 4), ReqFont, Brushes.Black, 0, 30)
            If Trim(CLSLIMS.Piece(OrderRec, "^", 5)) <> "" Then
                e.Graphics.DrawString(CLSLIMS.Piece(OrderRec, "^", 5), ReqFont, Brushes.Black, 0, 45)
                e.Graphics.DrawString(CLSLIMS.Piece(OrderRec, "^", 6) & ", " & CLSLIMS.Piece(OrderRec, "^", 7) & "  " & CLSLIMS.Piece(OrderRec, "^", 8), ReqFont, Brushes.Black, 0, 60)
            Else
                e.Graphics.DrawString(CLSLIMS.Piece(OrderRec, "^", 6) & ", " & CLSLIMS.Piece(OrderRec, "^", 7) & "  " & CLSLIMS.Piece(OrderRec, "^", 8), ReqFont, Brushes.Black, 0, 45)
            End If
            e.Graphics.DrawString(CLSLIMS.Piece(OrderRec, "^", 9), ReqFont, Brushes.Black, 250, 0)   'Phone #
            CernerAcct = CLSLIMS.Piece(OrderRec, "^", 1)
            If CernerAcct = "1289" Or CernerAcct = "1335" Or CernerAcct = "79" Or CernerAcct = "1341" Or CernerAcct = "1362" Or CernerAcct = "1404" Or CernerAcct = "S79" Then
                e.Graphics.DrawString("MRN#: _____________", ReqFont, Brushes.Black, 190, 22)
                e.Graphics.DrawString("CPI#: _____________", ReqFont, Brushes.Black, 190, 37)
                e.Graphics.DrawString("Account#: _________", ReqFont, Brushes.Black, 190, 52)
            Else
                e.Graphics.DrawString(CLSLIMS.Piece(OrderRec, "^", 10), ReqFont, Brushes.Black, 250, 15)  'Fax #
            End If
            If CLSLIMS.Piece(OrderRec, "^", 1) = "220" Then
                e.Graphics.DrawString("DIRECT BILL ACCOUNT", ReqFont, Brushes.Black, 0, 180)  'Ins Check box
                e.Graphics.DrawString("DIRECT BILL ACCOUNT", ReqFont, Brushes.Black, 0, 195)  'Ins Number
                e.Graphics.DrawString("DIRECT BILL ACCOUNT  DIRECT BILL ACCOUNT", ReqFont, Brushes.Black, 0, 215)  'Insurance Provider
            End If
            'If CLSLIMS.Piece(OrderRec, "^", 12) = "OREQ" Then
            '     e.Graphics.DrawString("[ ] STANDARD RISK PROFILE (SEE PROFILE AUTHORIZATION FORM FOR SIGNATURE)", ReqFont, Brushes.Black, 100, 840)
            'End If
        Else
            e.Graphics.DrawString("ORDER NUMBER:", OrderFont, Brushes.Black, 0, 0)
            e.Graphics.DrawString(OrderRec, OrderFont, Brushes.Black, 20, 40)
        End If
        'OrderRec = ""
    End Sub

    Private Sub lstOrdApp_DoubleClick(sender As Object, e As EventArgs) Handles lstOrdApp.DoubleClick
        CLSLIMS.OrderNum = OrdNumApp(lstOrdApp.SelectedIndex)
        frmOrders.ShowDialog()
        LoadQueues()
    End Sub

    Private Sub lstPrt_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lstPrt.MouseDoubleClick
        Dim OrderNum As String = CLSLIMS.Piece(OrdNumPrt(lstPrt.SelectedIndex), "^", 1), OrderSeq As String = CLSLIMS.Piece(OrdNumPrt(lstPrt.SelectedIndex), "^", 2)
        Dim FrmTyp As String = BarCodePrint(OrderNum, OrderSeq)
        If FrmTyp = "TREQ" Or FrmTyp = "OREQ" Then
            Do While CurrPrinter = ""
                frmPtr.ShowDialog()
            Loop
            Dim Rsp As String = MsgBox("Submit Forms to Print?", MsgBoxStyle.YesNo, "Print forms.")
            If Rsp = vbYes Then
                'Acct^Account Name^Contact^Addr1^Addr2^City^State^ZIP^Phone^Fax^Form Type
                OrderRec = GetOrderInfo(OrderNum, OrderSeq)
                PrintForms(OrderNum, OrderSeq, FrmTyp, False)
                PrintForms(OrderNum, OrderSeq, FrmTyp, True)
                UpdStatusOI(OrderNum, OrderSeq, "6")
                OrderRec = "" : OrderPage = False
            End If
            CurrPrinter = ""
        ElseIf FrmTyp = "FELABELS" Then
            Dim Rsp As String = MsgBox("Create FedEx Labels File? (Alt-F5 in Fedx Mgr)", MsgBoxStyle.YesNo, "Print Labels.")
            If Rsp = vbYes Then
                CreateCSVFedex(OrderNum, OrderSeq)
                UpdStatusOI(OrderNum, OrderSeq, "7")
            End If
        ElseIf FrmTyp = "UPSLABEL" Then
            Dim Rsp As String = MsgBox("Create UPS Labels File for Batch Import?", MsgBoxStyle.YesNo, "Print Labels.")
            If Rsp = vbYes Then
                CreateCSVUPS(OrderNum, OrderSeq)
                UpdStatusOI(OrderNum, OrderSeq, "7")
            End If
        ElseIf FrmTyp = "CREQ" Then
            Dim Rsp As String = MsgBox("Print COVID Reqs?", MsgBoxStyle.YesNo, "Load COVID Reqs in Tray")
            If Rsp = vbYes Then
                PrintReqCOVID(OrderNum, OrderSeq)
                UpdStatusOI(OrderNum, OrderSeq, "7")
            End If
        End If
            LoadQueues()
    End Sub
    Public Sub PrintForms(ByVal ordernum As String, ByVal orderseq As String, ByVal frmtyp As String, ByVal ordpg As Boolean)
        PrintReq.PrinterSettings.PrinterName = CurrPrinter
        'PrintReq.OriginAtMargins = True

        PrintReq.DefaultPageSettings.Margins.Left = 0
        PrintReq.DefaultPageSettings.Margins.Right = 0
        PrintReq.DefaultPageSettings.Margins.Top = 0
        PrintReq.DefaultPageSettings.Margins.Bottom = 0
        PrintReq.DefaultPageSettings.Landscape = False
        If ordpg Then
            OrderPage = True
            OrderRec = ordernum
            PrintReq.PrinterSettings.Copies = 1
        Else
            OrderPage = False
            PrintReq.PrinterSettings.Copies = Val(PrintCount(ordernum, orderseq))
        End If
        If PrintReq.PrinterSettings.IsValid Then
            PrintReq.Print()
        End If

    End Sub
    Public Function BarCodePrint(ByVal ord As String, ByVal ordseq As String) As String
        BarCodePrint = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select ItemID From OrderItems Where HSEQ=0 And OrderNum='" & ord & "' And OrderSeq='" & ordseq & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If rs(0) = "FELABELS" Or rs(0) = "UPSLABEL" Or rs(0) = "TREQ" Or rs(0) = "OREQ" Or rs(0) = "CREQ" Then
                BarCodePrint = rs(0)
            End If
        End If
        rs.Close()
        Cmd.Dispose()
        ConnMySql.Close()
        ConnMySql.Dispose()
    End Function
    Public Sub CreateCSVFedex(ByVal ord As String, ByVal ordseq As String)
        OrderRec = GetOrderInfo(ord, ordseq)
        Dim Cnt = PrintCount(ord, ordseq), r As String = ""
        Dim Extfile As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\ProgramData\FedEx\Integration\Projects\CompassImport.csv", False)
        Extfile.WriteLine("Reference,Account,SCompany,SContact,SAdd1,SAdd2,SCity,SState,SZip,SPhone,Weight,Count")
        OrderRec = Replace(OrderRec, ",", " ")
        'Extfile.WriteLine("7,144,Pain South,Holleigh Seales,2700 10th Ave. South,#444,Birmingham,AL,35205,205-297-9801,1,2")
        r = ord & "," & CLSLIMS.Piece(OrderRec, "^", 1) & "," & CLSLIMS.Piece(OrderRec, "^", 2) & "," & CLSLIMS.Piece(OrderRec, "^", 3) & "," & CLSLIMS.Piece(OrderRec, "^", 4) & "," & CLSLIMS.Piece(OrderRec, "^", 5) & "," & CLSLIMS.Piece(OrderRec, "^", 6) & "," & CLSLIMS.Piece(OrderRec, "^", 7) & "," & CLSLIMS.Piece(OrderRec, "^", 8) & "," & CLSLIMS.Piece(OrderRec, "^", 9) & ",1," & Cnt
        Extfile.WriteLine(r)
        Extfile.Close()
        OrderRec = ""
    End Sub
    Public Sub CreateCSVUPS(ByVal ord As String, ByVal ordseq As String)
        OrderRec = GetOrderInfo(ord, ordseq)
        Dim Cnt = PrintCount(ord, ordseq), r As String = ""
        Dim Extfile As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\RETURN\Return.csv", False)
        Extfile.WriteLine("ShipFromName,ShipFromContact,ShipFromCountry,ShipFromAddr1,ShipFromAddr2,ShipFromCity,ShipFromState,ShipFromZIP,Weight,Height,Length,Width,ShippingType,SHP,CP,Y,PRL,LABSAMPLE,ShipToName,ShipToContact,ShipToCountry,ShipToAddr1,ShipToAddr2,ShipToCity,ShipToState,ShipToZIP")
        'Acct^Account Name^Contact^Addr1^Addr2^City^State^ZIP^Phone^Fax
        'Account Name,Contact,Addr1,Addr2,City,State,ZIP,Weight,Height,Length,Width,ShipType,SHP,CP,Y,PRL,LABSAMPLE,Compass Laboratory,Receiving,US,1910 Nonconnah Blvd,Suite 108,Memhis,TN,38132
        For i = 1 To Val(Cnt)
            'r = CLSLIMS.Piece(OrderRec, "^", 2) & "," & CLSLIMS.Piece(OrderRec, "^", 3) & ",US," & CLSLIMS.Piece(OrderRec, "^", 4) & "," & CLSLIMS.Piece(OrderRec, "^", 5) & "," & CLSLIMS.Piece(OrderRec, "^", 6) & "," & CLSLIMS.Piece(OrderRec, "^", 7) & "," & CLSLIMS.Piece(OrderRec, "^", 8) & ",1,16,11,3,Next Day Air,SHP,CP,Y,PRL,Ord#: " & ord & ",Compass Laboratory,Receiving,US,1910 Nonconnah Blvd,Suite 108,Memphis,TN,38132"
            r = CLSLIMS.Piece(OrderRec, "^", 2) & "," & CLSLIMS.Piece(OrderRec, "^", 3) & ",US," & CLSLIMS.Piece(OrderRec, "^", 4) & "," & CLSLIMS.Piece(OrderRec, "^", 5) & "," & CLSLIMS.Piece(OrderRec, "^", 6) & "," & CLSLIMS.Piece(OrderRec, "^", 7) & "," & CLSLIMS.Piece(OrderRec, "^", 8) & ",1,16,11,3,Next Day Air,SHP,CP,Y,PRL,Ord#: " & ord & ",Compass Laboratory,(901)348-5774,US,2971 Carrier St,,Memphis,TN,38116"
            Extfile.WriteLine(r)
        Next i
        Extfile.Close()
        OrderRec = ""
    End Sub
    Public Function NxtSpecNo() As String
        NxtSpecNo = "XXXXXXX"
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select VALUE From STUFF Where WHAT='COVID' And LKUP='NEXTSEQ'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            NxtSpecNo = Val(rs(0)) + 1
        End If
        rs.Close()
        Cmd.CommandText = "Update STUFF Set VALUE='" & NxtSpecNo & "' Where WHAT='COVID' And LKUP='NEXTSEQ'"
        Cmd.ExecuteNonQuery()
        Cmd.Dispose()
        ConnMySql.Close()
        ConnMySql.Dispose()

    End Function
    Public Sub PrintReqCOVID(ByVal ord As String, seq As Integer)
        '==================================================================================================================
        'IMPORTANT:  On whichever PC prints the reqs
        '  (1) Printer must be named exactly KONICA  
        '  (2) Tray2 must be set in preferences as the default tray
        '  (3) Load COVID req printer stock in Tray2
        '==================================================================================================================
        Dim AcctInfo As String = GetOrderInfo(ord, seq)    'Acct^Account Name^Contact^Addr1^Addr2^City^State^ZIP^Phone^Fax
        Dim Qty As Integer = Val(PrintCount(ord, seq))
        Dim BillLine1 As String = "", BillLine2 As String = "", BillLine3 As String = "", BillLine4 As String = ""
        Dim SpecNo As String = ""
        Dim dest As String = "COVIDreq.pdf"
        Dim work As String = "work.pdf"
        Dim tblTitle As PdfPTable = New PdfPTable(3)
        tblTitle.LockedWidth = True
        tblTitle.TotalWidth = 580
        tblTitle.DefaultCell.Border = Rectangle.NO_BORDER
        Dim SecCol As PdfPTable = New PdfPTable(1)
        SecCol.LockedWidth = True
        SecCol.TotalWidth = 200
        SecCol.DefaultCell.Border = Rectangle.NO_BORDER
        Dim ThirdCol As PdfPTable = New PdfPTable(1)
        ThirdCol.LockedWidth = True
        ThirdCol.TotalWidth = 200
        ThirdCol.DefaultCell.Border = Rectangle.NO_BORDER
        pdfDoc = New Document
        Dim fs As New FileStream(dest, FileMode.Create)
        pdfWrite = PdfWriter.GetInstance(pdfDoc, fs)

        Dim pdfcell As New PdfPCell
        pdfDoc.Open()
        pdfDoc.SetMargins(0, 0, 0, 0)

        Dim cb As PdfContentByte = pdfWrite.DirectContent
        Dim Code128 As New Barcode128
        Code128.CodeType = pdf.Barcode.CODE128
        Code128.BarHeight = 15
        Dim bc As iTextSharp.text.Image
        Dim cb2 As PdfContentByte = pdfWrite.DirectContent
        Dim tblInfo As PdfPTable = New PdfPTable({190, 350})
        Dim tblInfoSym As PdfPTable = New PdfPTable({290, 290})
        Dim tblInfoDx As PdfPTable = New PdfPTable({180, 180, 180})
        Dim tblInfoSig As PdfPTable = New PdfPTable({350, 190})
        '============================================================================================================================================
        For i = 1 To Qty
            tblTitle.DeleteBodyRows() : tblInfo.DeleteBodyRows() : tblInfoDx.DeleteBodyRows() : tblInfoSig.DeleteBodyRows()
            tblInfoSym.DeleteBodyRows() : SecCol.DeleteBodyRows() : ThirdCol.DeleteBodyRows()
            If i > 1 Then
                pdfDoc.Add(New Paragraph(" ")) : pdfDoc.Add(New Paragraph(" "))
            End If
            SpecNo = NxtSpecNo()
            Code128.Code = SpecNo
            bc = Code128.CreateImageWithBarcode(cb, BaseColor.BLACK, BaseColor.WHITE)
            imgLogo.ScalePercent(30.0F)
            imgLogo.Border = PdfPCell.NO_BORDER
            imgLogo.Alignment = 0
            tblTitle.AddCell(imgLogo)
            pdfcell = New PdfPCell(New Paragraph("   1910 Nonconnah Blvd", Tahoma12)) : pdfcell.Border = PdfPCell.NO_BORDER
            SecCol.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("   Memphis, TN  38132", Tahoma12)) : pdfcell.Border = PdfPCell.NO_BORDER
            SecCol.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("   (877) 836-1140", Tahoma12)) : pdfcell.Border = PdfPCell.NO_BORDER
            SecCol.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma15)) : pdfcell.Border = PdfPCell.NO_BORDER
            SecCol.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("SARS-CoV-2 Assay", Tahoma20)) : pdfcell.Border = PdfPCell.NO_BORDER
            SecCol.AddCell(pdfcell)
            tblTitle.AddCell(SecCol)
            ThirdCol.AddCell(bc)
            pdfcell = New PdfPCell(New Paragraph(SpecNo, Tahoma15)) : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER
            ThirdCol.AddCell(pdfcell)
            tblTitle.AddCell(ThirdCol)
            pdfDoc.Add(tblTitle)
            cb2.SetLineWidth(2) : cb2.MoveTo(28, 700) : cb2.LineTo(pdfDoc.PageSize.Width - 28, 700) : cb2.Stroke()
            tblInfo.LockedWidth = True
            tblInfo.TotalWidth = 540
            tblInfo.DefaultCell.Border = Rectangle.NO_BORDER
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.Border = PdfPCell.NO_BORDER
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.Border = PdfPCell.NO_BORDER
            tblInfo.AddCell(pdfcell)
            BillLine1 = "Bill Type:  []  Bill Insurance (below)  []  Direct Bill Account   []  Pre-Pay "
            BillLine2 = "Insurer:  ______________________________________________"
            BillLine3 = "Policy #:  ___________________Group #:  ________________"
            BillLine4 = " "
            BillLine1 = "Insurer: _______________________                 UNINSURED Info"
            BillLine2 = "Policy#: _______________________      SSN:  ______________________"
            BillLine3 = "Group#: _______________________      ID#:  _______________________"
            BillLine4 = "                                                                    (One required for Uninsured)"
            cb2.Rectangle(218, 455, 178, 80) : cb2.SetLineWidth(1.2) : cb2.Stroke()
            cb2.Rectangle(396, 455, 172, 80) : cb2.SetLineWidth(1.2) : cb2.Stroke()
            '  LINE 1
            pdfcell = New PdfPCell(New Paragraph("Account Information", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.BackgroundColor = BaseColor.LIGHT_GRAY : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Patient Information", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.BackgroundColor = BaseColor.LIGHT_GRAY : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER
            tblInfo.AddCell(pdfcell)
            '  LINE 2
            pdfcell = New PdfPCell(New Paragraph("Account:  " & CLSLIMS.Piece(AcctInfo, "^", 1), Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Patient Name (Last, First): ______________________________________ ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            '  LINE 3
            pdfcell = New PdfPCell(New Paragraph(CLSLIMS.Piece(AcctInfo, "^", 2), Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Date of Birth:  ____/_____/______      SSN: ________________________ ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            '  LINE 4
            pdfcell = New PdfPCell(New Paragraph(CLSLIMS.Piece(AcctInfo, "^", 4), Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Gender:   M   F    Ethnicity: White Black Hispanic  Asian  Other _______", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            '  LINE 5
            pdfcell = New PdfPCell(New Paragraph(CLSLIMS.Piece(AcctInfo, "^", 6) & ", " & CLSLIMS.Piece(AcctInfo, "^", 7) & "  " & CLSLIMS.Piece(AcctInfo, "^", 8), Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Home Address:  ______________________________________________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER
            tblInfo.AddCell(pdfcell)
            '  LINE 6
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("City:  ____________________________  State: ______   ZIP: __________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER
            tblInfo.AddCell(pdfcell)
            '  LINE 7
            pdfcell = New PdfPCell(New Paragraph("Sample Information", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.BackgroundColor = BaseColor.LIGHT_GRAY : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Phone:  ____________________________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER
            tblInfo.AddCell(pdfcell)
            '  LINE 8
            pdfcell = New PdfPCell(New Paragraph("Date of Collection: ___/___/_____", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Billing Information", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.BackgroundColor = BaseColor.LIGHT_GRAY : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER
            tblInfo.AddCell(pdfcell)
            '  LINE 9
            pdfcell = New PdfPCell(New Paragraph("Time of Collection:  __:__AM  PM", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(BillLine1, Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            '  LINE 10
            pdfcell = New PdfPCell(New Paragraph("Sample Swab Type:  (check one)", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(BillLine2, Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            '  LINE 11
            pdfcell = New PdfPCell(New Paragraph("[] Oropharyngeal  [] Nasopharyngeal", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(BillLine3, Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            '  LINE 12
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(BillLine4, Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)

            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            cb2.SetLineWidth(2) : cb2.MoveTo(28, 450) : cb2.LineTo(pdfDoc.PageSize.Width - 28, 450) : cb2.Stroke()
            'cb2.Rectangle(128, 421, 13, 13) : cb2.SetLineWidth(1.5) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("   SARS-CoV-2 Nucleic Acid Amplification Test", Tahoma15)) : pdfcell.Colspan = 2 : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.MinimumHeight = 20
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 2 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 2 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("PATIENT SYMPTOMS: (PLEASE SELECT ALL THAT APPLY)", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.BackgroundColor = BaseColor.LIGHT_GRAY : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.Colspan = 2
            tblInfo.AddCell(pdfcell)
            pdfDoc.Add(tblInfo)
            tblInfoSym.LockedWidth = True
            tblInfoSym.TotalWidth = 540
            tblInfoSym.DefaultCell.Border = Rectangle.NO_BORDER
            cb2.Rectangle(28, 373, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has a cough", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(300, 373, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has a sore throat", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(28, 353, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has a fever", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(300, 353, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has muscle pain", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(28, 333, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient is a healthcare worker", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(300, 333, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has headache", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(28, 313, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has loss of smell/taste", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(300, 313, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has difficulty breathing", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(28, 293, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has chills or repeated shaking", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            cb2.Rectangle(300, 293, 8, 8) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Patient has been within 6 ft. of COVID-19 case", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSym.AddCell(pdfcell)
            pdfDoc.Add(tblInfoSym)
            tblInfo.DeleteBodyRows()
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 2 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 2 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfo.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("DIAGNOSTIC CODES: (PLEASE SELECT ALL THAT APPLY)", Tahoma12)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.BackgroundColor = BaseColor.LIGHT_GRAY : pdfcell.VerticalAlignment = PdfPCell.ALIGN_CENTER : pdfcell.Colspan = 2
            tblInfo.AddCell(pdfcell)
            pdfDoc.Add(tblInfo)
            tblInfoDx.LockedWidth = True
            tblInfoDx.TotalWidth = 540
            tblInfoDx.DefaultCell.Border = Rectangle.NO_BORDER
            cb2.Rectangle(28, 252, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Abdominal Pain (R10.9)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(208, 252, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Chills without fever (R68.83)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(388, 252, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Headache (R51)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(28, 241, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Acute respiratory distress (R06.03)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(208, 241, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Cough (R05)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(388, 241, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Myalgia (M79.1)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(28, 230, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Anosmia/Loss of Smell (R430)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(208, 230, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Fatigue (R53.83)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(388, 230, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Pain in throat (R07.0)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(28, 218, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Fever (R50.9)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(208, 218, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Shortness of breath (R06.02)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            cb2.Rectangle(388, 218, 6, 6) : cb2.SetLineWidth(1.0) : cb2.Stroke()
            pdfcell = New PdfPCell(New Paragraph("     Contact and/or Viral exposure (Z20.828)", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoDx.AddCell(pdfcell)
            pdfDoc.Add(tblInfoDx)
            cb2.SetLineWidth(2) : cb2.MoveTo(28, 212) : cb2.LineTo(pdfDoc.PageSize.Width - 28, 212) : cb2.Stroke()
            tblInfoSig.LockedWidth = True
            tblInfoSig.TotalWidth = 540
            tblInfoSig.DefaultCell.Border = Rectangle.NO_BORDER
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 5 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 5 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Provider Signature: ____________________________________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Date:  _____/_____/________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Signature indicates that provider finds it medically necessary to order molecular diagnostic tests that are required to properly treat the patient.", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM : pdfcell.Colspan = 2
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Patient Signature: ____________________________________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 25 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Date:  _____/_____/________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Signature authorizes laboratory to perform testing as ordered by my physician.  I authorize laboratory to release results to my healthcare provider and bill my insurer.  I understand that insurance", Tahoma6)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM : pdfcell.Colspan = 2
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("coverage is a personal contract between me and my insurance company.  I am aware that laboratory may be an out of network provider with my insurer.", Tahoma6)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM : pdfcell.Colspan = 2
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM : pdfcell.Colspan = 2
            tblInfoSig.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma8)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM : pdfcell.Colspan = 2
            tblInfoSig.AddCell(pdfcell)
            pdfDoc.Add(tblInfoSig)
            Dim tblInfoLbl As PdfPTable = New PdfPTable({170, 370})
            tblInfoLbl.LockedWidth = True
            tblInfoLbl.TotalWidth = 580
            tblInfoLbl.DefaultCell.Border = Rectangle.NO_BORDER

            pdfcell = New PdfPCell(New Paragraph("          " & SpecNo, Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_TOP
            tblInfoLbl.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph(" ", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_TOP
            tblInfoLbl.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("DOB: ____/_____/______", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 10 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoLbl.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Write Name and Date of Birth as they appear on this requisition above.", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_TOP
            tblInfoLbl.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("Name: ____________________", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM
            tblInfoLbl.AddCell(pdfcell)
            pdfcell = New PdfPCell(New Paragraph("          Peel off label and affix to specimen tube.", Tahoma10)) : pdfcell.HorizontalAlignment = PdfPCell.ALIGN_LEFT : pdfcell.Border = PdfPCell.NO_BORDER : pdfcell.MinimumHeight = 20 : pdfcell.VerticalAlignment = PdfPCell.ALIGN_TOP
            tblInfoLbl.AddCell(pdfcell)

            pdfDoc.Add(tblInfoLbl)
            'imgLeft.ScalePercent(75)
            'imgLeft.SetAbsolutePosition(160, 35)
            'cb2.AddImage(imgLeft)
            pdfDoc.NewPage()
            InSertYREQL(SpecNo, CLSLIMS.Piece(AcctInfo, "^", 1))
        Next i
        pdfDoc.CloseDocument()
        pdfDoc.Close()
        fs.Close()

        Dim ps As New PrinterSettings
        Dim DefPtr As String = ps.PrinterName
        Shell(String.Format("rundll32 printui.dll,PrintUIEntry /y /n ""{0}""", "KONICA"))
        ps.DefaultPageSettings.PaperSource = ps.PaperSources.Item(2)
        Dim startInfo As New ProcessStartInfo()
        startInfo.Verb = "Print"
        startInfo.FileName = "COVIDreq.pdf"
        startInfo.UseShellExecute = True
        startInfo.CreateNoWindow = True
        startInfo.WindowStyle = ProcessWindowStyle.Hidden

        Using print As System.Diagnostics.Process = Process.Start(startInfo)
            'Close the application after X milliseconds with WaitForExit(X)   
            print.WaitForExit(10000)
            print.Close()
        End Using
        Shell(String.Format("rundll32 printui.dll,PrintUIEntry /y /n ""{0}""", DefPtr))
        AcctInfo = ""
        'End
    End Sub
    <DllImport("winspool.drv", CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Function SetDefaultPrinter(Name As String) As Boolean
    End Function
    Private Sub InSertYREQL(ByVal SpecNo As String, ByVal acct As String)
        Dim Ins As String = "Insert into YREQL Values ('" & SpecNo & "','CREQ','','" & acct & "','" & CLSLIMS.UID & "','" & CLSLIMS.CDT & "')"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand
            Cmd.Connection = Conn
            Cmd.CommandText = Ins
            Cmd.ExecuteNonQuery()
        End Using
    End Sub
    Public Function GetOrderInfo(ByVal ord As String, ByVal seq As String) As String
        GetOrderInfo = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select * From Orders Where OrderNum='" & ord & "' and HSEQ=0"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            'Acct^Account Name^Contact^Addr1^Addr2^City^State^ZIP^Phone^Fax
            GetOrderInfo = rs(3) & "^" & rs(4) & "^" & rs(5) & "^" & rs(6) & "^" & rs(7) & "^" & rs(8) & "^" & rs(9) & "^" & rs(10) & "^" & rs(11) & "^" & rs(12) & "^" & rs(13)
        End If
        rs.Close()
        Cmd.CommandText = "Select itemid from OrderItems Where OrderNum='" & ord & "' And OrderSeq='" & seq & "' And HSEQ=0"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            GetOrderInfo = GetOrderInfo & "^" & rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function
    Public Function PrintCount(ByVal ord As String, ordseq As String) As String
        PrintCount = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select Qty From OrderItems Where OrderNum='" & ord & "' and HSEQ=0 And OrderSeq='" & ordseq & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            PrintCount = rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function
    Public Function OrderAcct(ByVal ord As String) As String
        OrderAcct = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select Acct From Orders Where OrderNum='" & ord & "' and HSEQ=0"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            OrderAcct = rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function
    Public Function UpdStatusO(ByVal ord As String, ordseq As String, ByVal status As String) As Boolean
        UpdStatusO = False
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Update Orders Set Status='" & status & "' Where OrderNum='" & ord & "' and HSEQ=0"
        Try
            Cmd.ExecuteNonQuery()
            UpdStatusO = True
        Catch
            UpdStatusO = False
        End Try
        ConnMySql.Close()
    End Function
    Public Function UpdStatusOI(ByVal ord As String, ordseq As String, ByVal status As String) As Boolean
        UpdStatusOI = False
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Update OrderItems Set Status='" & status & "' Where OrderNum='" & ord & "' and HSEQ=0 And OrderSeq='" & ordseq & "'"
        Try
            Cmd.ExecuteNonQuery()
            UpdStatusOI = True
        Catch
            UpdStatusOI = False
        End Try
        ConnMySql.Close()
    End Function

    Private Sub lstPick_DoubleClick(sender As Object, e As EventArgs) Handles lstPick.DoubleClick
        Dim x As String = OrdNumPick(lstPick.SelectedIndex)
        Dim OrderNum As String = CLSLIMS.Piece(x, "^", 1), OrderSeq As String = CLSLIMS.Piece(x, "^", 2)
        UpdStatusOI(OrderNum, OrderSeq, "8")
        LoadQueues()
    End Sub

    Private Sub lstReqNum_DoubleClick(sender As Object, e As EventArgs) Handles lstReqNum.DoubleClick
        Dim x As String = OrdNumReq(lstReqNum.SelectedIndex)
        Dim OrderNum As String = CLSLIMS.Piece(x, "^", 1), OrderSeq As String = CLSLIMS.Piece(x, "^", 2)
        If CLSLIMS.Piece(x, "^", 3) = "TREQ" Or CLSLIMS.Piece(x, "^", 3) = "OREQ" Then
            frmReqNumInput.txtOrdNum.Text = OrderNum
            frmReqNumInput.txtSeq.Text = OrderSeq
            frmReqNumInput.lblFormType.Text = CLSLIMS.Piece(x, "^", 3)
            frmReqNumInput.lblQty.Text = PrintCount(OrderNum, OrderSeq)
            frmReqNumInput.lblAcct.Text = OrderAcct(OrderNum)
            frmReqNumInput.ShowDialog()
            UpdStatusOI(OrderNum, OrderSeq, "7")
        End If
        LoadQueues()
    End Sub

    Private Sub lstShip_DoubleClick(sender As Object, e As EventArgs) Handles lstShip.DoubleClick
        Dim x As String = OrdNumShip(lstShip.SelectedIndex)
        Dim OrderNum As String = CLSLIMS.Piece(x, "^", 1), OrderSeq As String = CLSLIMS.Piece(x, "^", 2)
        UpdStatusO(OrderNum, OrderSeq, "10")
        LoadQueues()
    End Sub

End Class