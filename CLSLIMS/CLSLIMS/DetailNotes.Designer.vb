﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DetailNotes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DetailNotes))
        Me.lbTemplate = New System.Windows.Forms.ListBox()
        Me.txtNotes = New System.Windows.Forms.TextBox()
        Me.lblspecnum = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.ppd = New System.Windows.Forms.PrintPreviewDialog()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblPatName = New System.Windows.Forms.Label()
        Me.CheckSecond = New System.Windows.Forms.CheckBox()
        Me.CheckThird = New System.Windows.Forms.CheckBox()
        Me.CheckFirst = New System.Windows.Forms.CheckBox()
        Me.CheckTevix = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'lbTemplate
        '
        Me.lbTemplate.FormattingEnabled = True
        Me.lbTemplate.Location = New System.Drawing.Point(12, 34)
        Me.lbTemplate.Name = "lbTemplate"
        Me.lbTemplate.Size = New System.Drawing.Size(122, 498)
        Me.lbTemplate.TabIndex = 0
        '
        'txtNotes
        '
        Me.txtNotes.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.Location = New System.Drawing.Point(210, 40)
        Me.txtNotes.MaxLength = 3000
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.Size = New System.Drawing.Size(780, 498)
        Me.txtNotes.TabIndex = 1
        Me.txtNotes.WordWrap = False
        '
        'lblspecnum
        '
        Me.lblspecnum.AutoSize = True
        Me.lblspecnum.BackColor = System.Drawing.SystemColors.Window
        Me.lblspecnum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblspecnum.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblspecnum.Location = New System.Drawing.Point(161, 8)
        Me.lblspecnum.Name = "lblspecnum"
        Me.lblspecnum.Size = New System.Drawing.Size(2, 18)
        Me.lblspecnum.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(139, 17)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Specimen Number:"
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(670, 544)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(103, 23)
        Me.btnPrint.TabIndex = 7
        Me.btnPrint.Text = "Preview/Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(915, 544)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ppd
        '
        Me.ppd.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.ppd.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.ppd.ClientSize = New System.Drawing.Size(400, 300)
        Me.ppd.Document = Me.PrintDoc
        Me.ppd.Enabled = True
        Me.ppd.Icon = CType(resources.GetObject("ppd.Icon"), System.Drawing.Icon)
        Me.ppd.Name = "ppd"
        Me.ppd.Visible = False
        '
        'PrintDoc
        '
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(811, 544)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(282, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 17)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Patient Name:  "
        '
        'lblPatName
        '
        Me.lblPatName.AutoSize = True
        Me.lblPatName.BackColor = System.Drawing.SystemColors.Window
        Me.lblPatName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPatName.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPatName.Location = New System.Drawing.Point(427, 8)
        Me.lblPatName.Name = "lblPatName"
        Me.lblPatName.Size = New System.Drawing.Size(2, 18)
        Me.lblPatName.TabIndex = 11
        '
        'CheckSecond
        '
        Me.CheckSecond.AutoSize = True
        Me.CheckSecond.Location = New System.Drawing.Point(322, 544)
        Me.CheckSecond.Name = "CheckSecond"
        Me.CheckSecond.Size = New System.Drawing.Size(87, 17)
        Me.CheckSecond.TabIndex = 12
        Me.CheckSecond.Text = "2nd Request"
        Me.CheckSecond.UseVisualStyleBackColor = True
        '
        'CheckThird
        '
        Me.CheckThird.AutoSize = True
        Me.CheckThird.Location = New System.Drawing.Point(415, 544)
        Me.CheckThird.Name = "CheckThird"
        Me.CheckThird.Size = New System.Drawing.Size(84, 17)
        Me.CheckThird.TabIndex = 13
        Me.CheckThird.Text = "3rd Request"
        Me.CheckThird.UseVisualStyleBackColor = True
        '
        'CheckFirst
        '
        Me.CheckFirst.AutoSize = True
        Me.CheckFirst.Location = New System.Drawing.Point(233, 544)
        Me.CheckFirst.Name = "CheckFirst"
        Me.CheckFirst.Size = New System.Drawing.Size(83, 17)
        Me.CheckFirst.TabIndex = 14
        Me.CheckFirst.Text = "1st Request"
        Me.CheckFirst.UseVisualStyleBackColor = True
        '
        'CheckTevix
        '
        Me.CheckTevix.AutoSize = True
        Me.CheckTevix.Location = New System.Drawing.Point(505, 544)
        Me.CheckTevix.Name = "CheckTevix"
        Me.CheckTevix.Size = New System.Drawing.Size(52, 17)
        Me.CheckTevix.TabIndex = 15
        Me.CheckTevix.Text = "Tevix"
        Me.CheckTevix.UseVisualStyleBackColor = True
        '
        'DetailNotes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1002, 575)
        Me.Controls.Add(Me.CheckTevix)
        Me.Controls.Add(Me.CheckFirst)
        Me.Controls.Add(Me.CheckThird)
        Me.Controls.Add(Me.CheckSecond)
        Me.Controls.Add(Me.lblPatName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblspecnum)
        Me.Controls.Add(Me.txtNotes)
        Me.Controls.Add(Me.lbTemplate)
        Me.Name = "DetailNotes"
        Me.Text = "DetailNotes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbTemplate As ListBox
    Friend WithEvents txtNotes As TextBox
    Public WithEvents lblspecnum As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnPrint As Button
    Friend WithEvents btnClose As Button
    Friend WithEvents ppd As PrintPreviewDialog
    Friend WithEvents PrintDoc As Printing.PrintDocument
    Friend WithEvents btnSave As Button
    Friend WithEvents Label2 As Label
    Public WithEvents lblPatName As Label
    Friend WithEvents CheckSecond As CheckBox
    Friend WithEvents CheckThird As CheckBox
    Friend WithEvents CheckFirst As CheckBox
    Friend WithEvents CheckTevix As CheckBox
End Class
