﻿Imports CLSLIMS.CLSLIMS
Imports System.Data.SqlClient
Public Class frmOCGrid
    Private Sub frmOCGrid_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GetDefOC()
    End Sub
    Public Sub GetDefOC()
        ClearCLick()
        Dim OC As String = "", TstMethod As String = ""
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = conn
        Dim rs As SqlDataReader
        Cmd.CommandText = "Select OC,ACCTOCx From ACCT A, ACCTOC B Where A.ACCT=B.ACCT And A.AEFDT=B.AEFDT And AACT=1 And A.ACCT='" & CLSLIMS.AcctNum & "' Order By ACCTOCx"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            OC = rs(0)
            Select Case OC
                Case "351000" : cbAlc.Checked = True
                Case "350000" : cbAlc.Checked = True
                Case "395000" : cbAlk.Checked = True
                Case "353000" : cbAmp.Checked = True
                Case "352000" : cbAmp.Checked = True
                Case "309010" : cbChiral.Checked = True
                Case "301027" : cbChiral.Checked = True
                Case "354000" : cbADSer.Checked = True
                Case "355000" : cbADTri.Checked = True
                Case "356000" : cbAD.Checked = True
                Case "357000" : cbAntiEp.Checked = True
                Case "358000" : cbAntiPys.Checked = True
                Case "360000" : cbBarb.Checked = True
                Case "359000" : cbBarb.Checked = True
                Case "362000" : cbBenz.Checked = True
                Case "361000" : cbBenz.Checked = True
                Case "364000" : cbBup.Checked = True
                Case "363000" : cbBup.Checked = True
                Case "366000" : cbTHC.Checked = True
                Case "365000" : cbTHC.Checked = True
                Case "368000" : cbCOC.Checked = True
                Case "367000" : cbCOC.Checked = True
                Case "369000" : cbFent.Checked = True
                Case "370000" : cbGab.Checked = True
                Case "372000" : cbHero.Checked = True
                Case "371000" : cbHero.Checked = True
                Case "374000" : cbMtd.Checked = True
                Case "373000" : cbMtd.Checked = True
                Case "376000" : cbMethAmp.Checked = True
                Case "375000" : cbMethAmp.Checked = True
                Case "377000" : cbMethy.Checked = True
                Case "379000" : cbNic.Checked = True
                Case "378000" : cbNic.Checked = True
                Case "381000" : cbOpi.Checked = True
                Case "380000" : cbOpi.Checked = True
                Case "382000" : cbOpiAnal.Checked = True
                Case "384000" : cbOxy.Checked = True
                Case "383000" : cbOxy.Checked = True
                Case "386000" : cbPhen.Checked = True
                Case "385000" : cbPhen.Checked = True
                Case "387000" : cbPreGab.Checked = True
                Case "389000" : cbPro.Checked = True
                Case "388000" : cbPro.Checked = True
                Case "390000" : cbSedHyp.Checked = True
                Case "391000" : cbSkel.Checked = True
                Case "392000" : cbSkel.Checked = True
                Case "393000" : cbTap.Checked = True
                Case "394000" : cbTram.Checked = True
                Case "308130" : cbLevo.Checked = True
                    'Case "800000" : cbXREF.Checked = True
            End Select
        Loop
        rs.Close()
        Cmd.CommandText = "Select AAVRSPIN,AMISC2,ADFLTRFMT,ABCONT From ACCT Where AACT=1 And ACCT='" & CLSLIMS.AcctNum & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            TstMethod = rs(0)
            lblProfileDate.Text = rs(1)
            If rs(2) = "HERO" Then
                cbHero.Checked = True
            End If
            If rs(3) = "CHIRAL" Then
                cbChiral.Checked = True
            End If
        End If
        rs.Close()
        If TstMethod = "SCRN" Then
            rbScreening.Checked = True
        ElseIf TstMethod = "CONF" Then
            rbConf.Checked = True
        End If

        conn.Close()
    End Sub
    Public Sub ClearCLick()
        cbAD.Checked = False
        cbADSer.Checked = False
        cbADTri.Checked = False
        cbAlc.Checked = False
        cbAlk.Checked = False
        cbAmp.Checked = False
        cbChiral.Checked = False
        cbAntiEp.Checked = False
        cbAntiPys.Checked = False
        cbBarb.Checked = False
        cbBenz.Checked = False
        cbBup.Checked = False
        cbCOC.Checked = False
        cbFent.Checked = False
        cbGab.Checked = False
        cbHero.Checked = False
        cbMethAmp.Checked = False
        cbMethy.Checked = False
        cbMtd.Checked = False
        cbNic.Checked = False
        cbOpi.Checked = False
        cbOpiAnal.Checked = False
        cbOxy.Checked = False
        cbPhen.Checked = False
        cbPreGab.Checked = False
        cbPro.Checked = False
        cbSedHyp.Checked = False
        cbSkel.Checked = False
        cbTap.Checked = False
        cbTHC.Checked = False
        cbTram.Checked = False
        cbLevo.Checked = False
        'cbXREF.Checked = False
        rbConf.Checked = False
        rbScreening.Checked = False
        'dtpProfDt.Text = Now
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        PrintForm1.Print()
    End Sub
End Class