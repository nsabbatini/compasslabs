﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmReqNumInput
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        'Save in YREQL
        Dim Ins As String = "", YDt As String = CLSLIMS.CDT
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Dim rs As SqlDataReader = Nothing
        Cmd.CommandText = "select * from yreql where req between '" & txtFirst.Text & "' And '" & txtLast.Text & "' and Len(req)=" & Len(txtFirst.Text)
        rs = Cmd.ExecuteReader
        If rs.Read Then
            MsgBox("All or part of Range in use for account " & rs(3))
            rs.Close()
            Conn.Close()
            Exit Sub
        End If
        rs.Close()
        For i = Val(txtFirst.Text) To Val(txtLast.Text)
            Ins = "Insert into YREQL Values ('" & i & "','" & lblFormType.Text & "','','" & lblAcct.Text & "','" & CLSLIMS.UID & "','" & YDt & "')"
            Cmd.CommandText = Ins
            Cmd.ExecuteNonQuery()
        Next
        CLSLIMS.Log(" Order:" & txtOrdNum.Text & "/" & txtSeq.Text & " - " & lblFormType.Text & " : " & lblAcct.Text & " : " & txtFirst.Text & " - " & txtLast.Text & " --> " & lblQty.Text & "    by " & CLSLIMS.UID)
        Conn.Close()
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub txtFirst_KeyUp(sender As Object, e As KeyEventArgs) Handles txtFirst.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtLast.Text = Val(txtFirst.Text) + Val(lblQty.Text) - 1
            txtLast.Focus()
        End If
    End Sub

    Private Sub txtOrdNum_KeyUp(sender As Object, e As KeyEventArgs) Handles txtOrdNum.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtSeq.Focus()
        End If
    End Sub

    Private Sub txtSeq_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSeq.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtFirst.Focus()
            Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
            ConnMySql.Open()
            Dim Cmd As New MySqlCommand
            Cmd.Connection = ConnMySql
            Cmd.CommandText = "Select Acct,itemid,qty From Orders a, orderitems b Where a.OrderNum='" & txtOrdNum.Text & "' and a.HSEQ=0 and b.HSEQ=0 and a.ordernum=b.ordernum and orderseq='" & txtSeq.Text & "'"
            Dim rs As MySqlDataReader = Cmd.ExecuteReader
            If rs.Read Then
                lblAcct.Text = rs(0)
                lblformtype.text = rs(1)
                lblQty.Text = rs(2)
            End If
            rs.Close()
            ConnMySql.Close()
        End If
    End Sub

    Private Sub frmReqNumInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
    End Sub
End Class