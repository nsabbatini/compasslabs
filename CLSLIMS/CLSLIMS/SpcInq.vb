﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing


Public Class SpcInq
    Dim strFolder As String = ""
    Dim strTray As String = ""
    Dim PageNo As Integer = 1
    Dim ListStartPositionSC As String = 0 : Dim ListStartPositionWL As String = 2 : Dim ListStartPositionRPT As String = 0
    Dim ListStartPositionMed As String = 0 : Dim ListStartPositionOC As String = 0 : Dim ListStartPositionComm As String = 0
    Dim OralSamp As Boolean = False

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
    Private Sub txtOrderKey_KeyUp(sender As Object, e As KeyEventArgs) Handles txtOrderKey.KeyUp
        If e.KeyCode = Keys.Enter Then

            Dim specno As String = ""
            specno = GetOrderkey(txtOrderKey.Text)
            If specno = "" Then
                MsgBox("Order Key not found.")
                Exit Sub
            End If
            ClearFields()
            FillScreen(specno)
        End If

    End Sub
    Private Sub txtSpec_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSpec.KeyUp
        If e.KeyCode = Keys.Enter Then
            ClearFields()
            FillScreen(txtSpec.Text)
        End If
    End Sub
    Private Sub FillScreen(ByVal specno As String)
        Dim puid As String, pefdt As String, adsq1 As String, Tries As Integer = 1
        Dim Sel As String = ""
        txtSpec.Text = specno
        If Not IsNumeric(txtSpec.Text) And Mid(txtSpec.Text, 1, 1) <> "D" And Mid(txtSpec.Text, 1, 1) <> "W" And Mid(txtSpec.Text, 1, 1) <> "Z" Then
            lstPatNameSearch.Height = 340
            lstPatNameSearch.Width = 955
            lstPatNameSearch.Location = New Drawing.Point(223, 45)
            lstPatNameSearch.Visible = True
            lstPatNameSearch.BringToFront()
            txtSpec.Text = GetSpecNoFromName(txtSpec.Text)
            Exit Sub
        End If
Again:
        Sel = "select pspecno,acct,reqphy,req,pscdta,psctma,a.puid,a.pefdt,adsq1,psrcvdt,pstat,PSID3 from pspec a, pord b where a.pord + a.poefdt = b.pord + b.poefdt and psact=1 and poact=1 and (pspecno='" & txtSpec.Text & "' or Req='" & txtSpec.Text & "')"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Nothing
        Try
            rs = Cmd.ExecuteReader
            If rs.Read() Then
                lblRetries.Text = ""
                lblSpecNo.Text = rs(0)
                lblReqNum.Text = rs(3)
                lblPhy.Text = IIf(IsDBNull(rs(2)), "", rs(2))
                lblAcctInfo.Text = rs(1) & " - " & GetAcctName(rs(1))
                If IsDBNull(rs(11)) Then
                    txtOrderKey.Text = ""
                Else
                    If rs(11) <> "" Then
                        txtOrderKey.Text = rs(11) 'lstWrkPos.Items.Add("OrderKey: " & rs(11))
                    End If
                End If
                If IIf(IsDBNull(rs(4)), "", rs(4)) <> "" Then
                    lblColl.Text = Mid(rs(4), 5, 2) & "/" & Mid(rs(4), 7, 2) & "/" & Mid(rs(4), 1, 4)
                End If
                If IIf(IsDBNull(rs(5)), "", rs(5)) <> "" Then
                    lblColl.Text = lblColl.Text & "  " & rs(5)
                End If
                If rs(9) <> "" Then
                    lblRcv.Text = Mid(rs(9), 5, 2) & "/" & Mid(rs(9), 7, 2) & "/" & Mid(rs(9), 1, 4) & "  " & Mid(rs(9), 9, 2) & ":" & Mid(rs(9), 11, 2)
                End If
                If rs(10) <> "" Then
                    Select Case rs(10)
                        Case 0 : lblStatus.Text = "Label Printed"
                        Case 1 : lblStatus.Text = "Prepped"
                        Case 2 : lblStatus.Text = "Data Entered"
                        Case 3 : lblStatus.Text = "Reported"
                        Case Else : lblStatus.Text = "Unknown"
                    End Select
                End If
                puid = IIf(IsDBNull(rs(6)), "", rs(6)) : pefdt = IIf(IsDBNull(rs(7)), "", rs(7)) : adsq1 = rs(8)
                rs.Close()
                Cmd.CommandText = "select plname,pfname,pmname,pdob from PDEM where puid='" & puid & "' and pefdt='" & pefdt & "'"  ' and pact=1"
                Dim rs2 As SqlDataReader = Cmd.ExecuteReader
                Dim OC As String = ""
                If rs2.Read Then
                    lblPatName.Text = rs2(0) & ", " & rs2(1) & " " & Mid(rs2(2), 1, 1)
                    lblDOB.Text = IIf(IsDBNull(rs2(3)), "N/A", Mid(rs2(3), 5, 2) & "/" & Mid(rs2(3), 7, 2) & "/" & Mid(rs2(3), 1, 4))
                End If
                rs2.Close()
                Cmd.CommandText = "select pocx,a.oc,oname from poc a, oc b where a.oc = b.oc And a.oefdt = b.oefdt and pspecno='" & lblSpecNo.Text & "' and adsq1=" & adsq1 & " order by pocx"
                Dim rs3 As SqlDataReader = Cmd.ExecuteReader
                lstOC.Items.Clear() : OralSamp = False
                Do While rs3.Read
                    If Mid(rs3(1), 1, 1) = "S" Then OralSamp = True
                    lstOC.Items.Add(rs3(1) & Space(10 - Len(rs3(1))) & rs3(2))
                Loop
                rs3.Close()
                btnOCGrid.Enabled = True
                Cmd.CommandText = "Select RPTDT,RPQUE From PRPT Where PSPECNO='" & lblSpecNo.Text & "' Order By RPTDT"
                Dim rs8 As SqlDataReader = Cmd.ExecuteReader
                Do While rs8.Read
                    lstRptDt.Items.Add(Mid(rs8(0), 5, 2) & "/" & Mid(rs8(0), 7, 2) & "/" & Mid(rs8(0), 1, 4) & Space(1) & Mid(rs8(0), 9, 2) & ":" & Mid(rs8(0), 11, 2) & Space(2) & rs8(1))
                Loop
                rs8.Close()
                Cmd.CommandText = "Select STATCD From PSSTAT A, PSPEC B Where A.PSPECNO = B.PSPECNO And A.ADSQ1=B.ADSQ1 And PSACT=1 And A.PSPECNO='" & txtSpec.Text & "'"
                Dim rs10 As SqlDataReader = Cmd.ExecuteReader
                Do While rs10.Read
                    lstStatusCode.Items.Add(rs10(0) & "-" & CLSLIMS.GetStatusCodeDesc(rs10(0)))
                Loop
                rs10.Close()
                Cmd.CommandText = "select phy,phylname,phyfname,phymname from doctor where phyact=1 and phy='" & lblPhy.Text & "'"
                Dim rs4 As SqlDataReader = Cmd.ExecuteReader
                If rs4.Read Then
                    lblPhyName.Text = rs4(1) & ", " & rs4(2) & " " & Mid(rs4(3), 1, 1)
                End If
                rs4.Close()
                Cmd.CommandText = "select pomedx,med from pomed a,  pspec b where a.pord = b.pord And a.poefdt = b.poefdt and pspecno='" & lblSpecNo.Text & "' and psact=1 order by pomedx"
                Dim rs5 As SqlDataReader = Cmd.ExecuteReader
                Do While rs5.Read
                    lblMeds.Text = lblMeds.Text & rs5(1) & ", "
                Loop
                If Len(lblMeds.Text) > 2 Then
                    lblMeds.Text = Mid(lblMeds.Text, 1, Len(lblMeds.Text) - 2)
                End If
                rs5.Close()
                Cmd.CommandText = "Select FOLDER, POS, TRAY, TRAYx From XTRAYS Where PSPECNO='" & lblSpecNo.Text & "'"
                Dim rs9 As SqlDataReader = Cmd.ExecuteReader, strFolder As String = ""
                'lstWrkPos.Items.Clear()
                Do While rs9.Read
                    If Not IsDBNull(rs9(0)) Then
                        lstWrkPos.Items.Add("Folder:" & Space(8) & rs9(0) & Space(10 - Len(rs9(0))) & rs9(1))
                        strFolder = rs9(0)
                    End If
                    Dim TL As String
                    Dim sq As Integer = Val(Mid(rs9(2), 3, 2))
                    TL = Mid("ABCDEFGHJKLMNPQRSTUVWXYZ", sq, 1)
                    lstWrkPos.Items.Add("Tray:" & Space(10) & rs9(2) & Space(15 - Len(rs9(2))) & TL & rs9(3))
                Loop
                rs9.Close()
                Cmd.CommandText = "Select INSTRUMENT, ADSQ1, FLDEFDT From XFOLDERS Where FOLDER='" & strFolder & "' ORDER BY ADSQ1"
                Dim rs11 As SqlDataReader = Cmd.ExecuteReader, strDt As String = ""
                Do While rs11.Read
                    strDt = "[" & Mid(rs11(2), 5, 2) & "/" & Mid(rs11(2), 7, 2) & "/" & Mid(rs11(2), 1, 4) & Space(1) & Mid(rs11(2), 9, 2) & ":" & Mid(rs11(2), 11, 2) & "]"
                    lstWrkPos.Items.Add("Instrument: " & rs11(0) & Space(1) & strDt)
                Loop
                rs11.Close()
                Cmd.CommandText = "select b.wl,mbatch, pbix from pb a, pbi b where pspecno='" & lblSpecNo.Text & "' and a.batch=b.batch and a.befdt=b.befdt and bact=1 and a.wl=b.wl"
                Dim rs6 As SqlDataReader = Cmd.ExecuteReader
                Do While rs6.Read
                    lstWrkPos.Items.Add(rs6(0) & Space(15 - Len(rs6(0))) & rs6(1) & Space(10 - Len(rs6(1))) & rs6(2))
                Loop
                rs6.Close()
                Cmd.CommandText = "select pscmttxt from pscmt a, pscmti b where a.pspecno = b.pspecno and a.pscmttype=b.pscmttype and a.pscmtefdt=b.pscmtefdt " &
                              "and a.pspecno='" & lblSpecNo.Text & "' and pscmtact=1 order by a.pscmttype,a.pscmtefdt,pscmtix"
                Dim rs7 As SqlDataReader = Cmd.ExecuteReader

                Do While rs7.Read
                    lstCom.Items.Add(rs7(0))
                Loop
                rs7.Close()
                Cmd.CommandText = "select '\\CMPFS1\Image\' + IMGVOL + '\' + IMGFILE From PIMG Where PSPECNO='" & lblSpecNo.Text & "'"
                Dim rs12 As SqlDataReader = Cmd.ExecuteReader, ImgFile As String = "", SecondImgFile As String = ""
                If rs12.Read Then
                    ImgFile = rs12(0)
                End If
                rs12.Close()
                SecondImgFile = CLSLIMS.ReplacePiece(ImgFile, "\", 3, "CL-AGILENT")
                'Remove trailing "\" ???
                SecondImgFile = Mid(SecondImgFile, 1, Len(SecondImgFile) - 1)
                If ImgFile <> "" Then
                    If System.IO.File.Exists(ImgFile) Then
                        Button1.Enabled = True
                    Else
                        If System.IO.File.Exists(SecondImgFile) Then
                            Button1.Enabled = True
                        End If
                    End If
                End If
                If File.Exists("\\cmpfs1\affidavit\" & lblSpecNo.Text & "AFF.pdf") Then
                    Button2.Enabled = True
                ElseIf File.Exists("\\cmpfs1\affidavit\" & lblSpecNo.Text & "AFF.pdf") Then
                    Button2.Enabled = True
                End If
                If File.Exists("\\cmpfs1\MedRec\" & lblSpecNo.Text & "MR.pdf") Then
                    Button3.Enabled = True
                End If
                btnEdit.Enabled = True
                btnPrint.Enabled = True
            Else
                MsgBox("Specimen not found.")
                ClearFields()
                rs.Close()
                Conn.Close()
                txtSpec.Text = ""
                txtSpec.Focus()
                Exit Sub
            End If
        Catch
            Tries = Tries + 1
            If Tries < 8 Then
                'rs.Close()
                lblRetries.Text = Tries : lblRetries.Refresh()
                Conn.Close()
                lblStatus.Text = Tries
                lblStatus.Refresh()
                System.Threading.Thread.Sleep(2000)
                lblStatus.Refresh()
                GoTo Again
            End If
        End Try
        rs.Close()
        Conn.Close()
        txtSpec.Text = ""
        txtSpec.Focus()

    End Sub
    Private Sub ClearFields()

        lblSpecNo.Text = "" : lblAcctInfo.Text = "" : lblRcv.Text = "" : lblStatusCode.Text = ""
        lblPatName.Text = "" : lblColl.Text = "" : lblReqNum.Text = "" : lblDOB.Text = ""
        lblStatus.Text = "" : lblMeds.Text = "" : lblPhy.Text = "" : lblPhyName.Text = "" : lblRetries.Text = ""
        txtOrderKey.Text = ""
        lstOC.Items.Clear()
        lstCom.Items.Clear()
        lstWrkPos.Items.Clear()
        lstPatNameSearch.Items.Clear()
        lstPatNameSearch.Visible = False
        txtSpec.Focus()
        lstStatusCode.Items.Clear()
        lstWrkPos.Items.Clear()
        lstRptDt.Items.Clear()
        lstRptDt.Refresh()
        txtOrderKey.Text = ""
        lstOC.Items.Clear()
        btnOCGrid.Enabled = False
        Button2.Enabled = False
        Button1.Enabled = False
        btnEdit.Enabled = False
        btnPrint.Enabled = False
        Me.Refresh()
    End Sub
    Private Function GetSpecNoFromName(ByVal PatName As String) As String

        Dim xSel As String
        xSel = "select pspecno,plname+','+pfname,c.acct,aname,req,substring(pscdta,5,2)+'/'+substring(pscdta,7,2)+'/'+substring(pscdta,3,2) " & _
               "from pspec a, pord b, acct c, pdem d " & _
               "where a.pord + a.poefdt = b.pord + b.poefdt And a.psact = 1 And b.poact = 1 " & _
               "and a.puid + a.pefdt = d.puid + d.pefdt " & _
               "and c.acct = b.acct and c.aefdt = b.aefdt " & _
               "and pact=1 and plname+','+pfname like '" & PatName & "%' " & _
               "order by plname+','+pfname"

        Dim strCol As String = "", strPatName As String = ""
        GetSpecNoFromName = ""
        If PatName = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strPatName = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 30) & Space(32 - Len(cRS(1))) & cRS(2) & Space(6 - Len(cRS(2))) & Mid(cRS(3), 1, 30) & Space(32 - Len(Mid(cRS(3), 1, 30))) & cRS(4) & Space(10 - Len(cRS(4))) & cRS(5)
                lstPatNameSearch.Items.Add(strPatName)
            Loop
            If lstPatNameSearch.Items.Count > 0 Then
                lstPatNameSearch.Visible = True
                lstPatNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Private Function GetAcctName(ByVal acct As String) As String
        Dim xSel As String = "Select ANAME from ACCT where aact=1 and acct= '" & acct & "'"
        GetAcctName = ""
        If acct = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(0)
            End If
        End Using

    End Function
    Private Function GetOrderkey(ByVal orderkey As String) As String
        Dim xSel As String = "Select PSPECNO From PSPEC Where PSACT=1 and PSID3= '" & orderkey & "'"
        GetOrderkey = ""
        If orderkey = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetOrderkey = cRS(0)
            End If
        End Using

    End Function
    Private Sub SpcInq_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        'ClearFields()
        txtSpec.Focus()
    End Sub

    Private Sub SpcInq_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ClearFields()
        txtSpec.Focus()
    End Sub

    Private Sub lstPatNameSearch_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles lstPatNameSearch.MouseDoubleClick
        txtSpec.Text = Piece(lstPatNameSearch.SelectedItem.ToString, " ", 1)
        lstPatNameSearch.Visible = False
        FillScreen(txtSpec.Text)
    End Sub
    Public Function Piece(ByVal str As String, Optional ByVal charz As String = "^", Optional ByVal pos As Integer = 1) As String
        Dim pc As Array
        If pos < 1 Then pos = 1
        If InStr(str, charz) = 0 Then
            Piece = str
            Exit Function
        End If
        pc = Split(str, charz)
        If UBound(pc) < pos - 1 Then
            Piece = ""
        Else
            Piece = pc(pos - 1)
        End If

    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Sel As String, ImgName As String = "", SecondImgName As String = ""
        'Sel = "select '\\10.65.1.14\ApolloLIMS\Image\' + IMGVOL + '\' + IMGFILE From PIMG Where PSPECNO='" & lblSpecNo.Text & "'"
        'Sel = "select '\\COMPASS-SCANPC\C\Compass\Image\' + IMGVOL + '\' + IMGFILE From PIMG Where PSPECNO='" & lblSpecNo.Text & "'"
        Sel = "select '\\CMPFS1\Image\' + IMGVOL + '\' + IMGFILE From PIMG Where PSPECNO='" & lblSpecNo.Text & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim cRS As SqlDataReader = Cmd.ExecuteReader
        If cRS.Read Then
            ImgName = cRS(0)
        End If
        cRS.Close()
        Conn.Close()
        SecondImgName = CLSLIMS.ReplacePiece(ImgName, "\", 3, "CL-AGILENT")
        'Remove trailing "\" ???
        SecondImgName = Mid(SecondImgName, 1, Len(SecondImgName) - 1)
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not indexed.")
        End If
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        ' If cmbSO.Text = "Lab Corp" Then
        PrintDoc.DefaultPageSettings.Landscape = False
        'Else
        ' PrintDoc.DefaultPageSettings.Landscape = False
        ' End If

        PrintPreview.Document = PrintDoc
        PrintPreview.ShowDialog()

        '   PrintDoc.Print()
    End Sub

    Private Sub PrintDoc_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Courier New", 9, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 50
        PageLngth = 50
        Dim numberoflines As Integer = 0
        Dim y As Integer, PrevSOLAB As String = "", SOLAB As String = ""
        Dim LogoImage As Image = My.Resources.Compass_Logo
        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        If PageNo = 1 Then
            e.Graphics.DrawString("Specimen #: ", Font4, Brushes.Black, 25, 100)
            e.Graphics.DrawString(lblSpecNo.Text, Font4, Brushes.Black, 125, 100)
            e.Graphics.DrawString("Req #: ", Font4, Brushes.Black, 300, 100)
            e.Graphics.DrawString(lblReqNum.Text, Font4, Brushes.Black, 400, 100)
            e.Graphics.DrawString("Status: ", Font4, Brushes.Black, 600, 100)
            e.Graphics.DrawString(lblStatus.Text, Font4, Brushes.Black, 700, 100)
            e.Graphics.DrawString("Order Key: ", Font4, Brushes.Black, 25, 125)
            e.Graphics.DrawString(txtOrderKey.Text, Font4, Brushes.Black, 150, 125)
            e.Graphics.DrawString("Account: ", Font4, Brushes.Black, 25, 150)
            e.Graphics.DrawString(lblAcctInfo.Text, Font4, Brushes.Black, 150, 150)
            e.Graphics.DrawString("Patient Name: ", Font4, Brushes.Black, 25, 175)
            e.Graphics.DrawString(lblPatName.Text, Font4, Brushes.Black, 150, 175)
            e.Graphics.DrawString("Receive Date: ", Font4, Brushes.Black, 500, 175)
            e.Graphics.DrawString(lblRcv.Text, Font4, Brushes.Black, 625, 175)
            e.Graphics.DrawString("Phy Name: ", Font4, Brushes.Black, 25, 200)
            e.Graphics.DrawString(lblPhyName.Text, Font4, Brushes.Black, 150, 200)
            e.Graphics.DrawString("Collect Date: ", Font4, Brushes.Black, 500, 200)
            e.Graphics.DrawString(lblColl.Text, Font4, Brushes.Black, 625, 200)
            e.Graphics.DrawString("Folder: ", Font4, Brushes.Black, 25, 225)
            e.Graphics.DrawString(strFolder, Font4, Brushes.Black, 150, 225)
            e.Graphics.DrawString("Tray: ", Font4, Brushes.Black, 500, 225)
            e.Graphics.DrawString(strTray, Font4, Brushes.Black, 625, 225)
            numberoflines = 11
        End If

        Dim i As Integer : Dim x As Integer : Dim numberofOC As Integer
        If PageNo = 1 Then
            x = 150 : y = 250
        Else
            x = 150 : y = 100
        End If

        '**********************************************************
        'Status Codes
        If ListStartPositionSC < lstStatusCode.Items.Count Then
            e.Graphics.DrawLine(Pens.Black, 25, y, 1075, y)
            e.Graphics.DrawString("Status Codes: ", Font4, Brushes.Black, 25, y)
        End If
        For i = ListStartPositionSC To lstStatusCode.Items.Count - 1
            e.Graphics.DrawString(lstStatusCode.Items(i), Font, Brushes.Black, x, y)
            y = y + 25
            numberoflines = numberoflines + 1
            If numberoflines > PageLngth Then
                ListStartPositionSC = i
                PageNo = PageNo + 1
                e.HasMorePages = True
                Exit Sub
            End If
        Next
        ListStartPositionSC = i
        '**********************************************************
        'Medications
        If PageNo = 1 Then
            e.Graphics.DrawLine(Pens.Black, 25, y, 1075, y)
            e.Graphics.DrawString("Medications: ", Font4, Brushes.Black, 25, y)
            e.Graphics.DrawString(lblMeds.Text, Font, Brushes.Black, 150, y)
            numberoflines = numberoflines + 1
            y = y + 25
        End If
        '**********************************************************
        'Worklist
        If ListStartPositionWL < lstWrkPos.Items.Count Then
            e.Graphics.DrawLine(Pens.Black, 25, y, 1075, y)
            e.Graphics.DrawString("Worklist: ", Font4, Brushes.Black, 25, y)
        End If
        x = 150
        For i = ListStartPositionWL To lstWrkPos.Items.Count - 1
            e.Graphics.DrawString(lstWrkPos.Items(i), Font, Brushes.Black, x, y)
            y = y + 25
            numberoflines = numberoflines + 1
            If numberoflines > PageLngth Then
                ListStartPositionWL = i
                PageNo = PageNo + 1
                e.HasMorePages = True
                Exit Sub
            End If
        Next
        ListStartPositionWL = i
        '**********************************************************
        'Reported
        If ListStartPositionRPT < lstRptDt.Items.Count Then
            e.Graphics.DrawLine(Pens.Black, 25, y, 1075, y)
            e.Graphics.DrawString("Reported: ", Font4, Brushes.Black, 25, y)
        End If
        x = 150
        For i = ListStartPositionRPT To lstRptDt.Items.Count - 1
            e.Graphics.DrawString(lstRptDt.Items(i), Font, Brushes.Black, x, y)
            y = y + 25
            numberoflines = numberoflines + 1
            If numberoflines > PageLngth Then
                ListStartPositionRPT = i
                PageNo = PageNo + 1
                e.HasMorePages = True
                Exit Sub
            End If
        Next
        numberofOC = y
        ListStartPositionRPT = i
        '**********************************************************
        'Ordering Codes
        If ListStartPositionOC < lstOC.Items.Count Then
            e.Graphics.DrawLine(Pens.Black, 25, y, 1075, y)
            e.Graphics.DrawString("Ordering Codes: ", Font4, Brushes.Black, 25, y)
        End If
        x = 150

        Dim column As Integer = 1
        For i = ListStartPositionOC To lstOC.Items.Count - 1
            Dim splitOC() As String = Split(lstOC.Items(i), "  ")
            Dim OCAbbr As String = FindOCAbbr(splitOC(0))
            '    If i <= TotPerColWhole Then
            e.Graphics.DrawString(splitOC(0) & " - " & OCAbbr, Font, Brushes.Black, x, y)
            numberofOC = numberofOC + 25

            If column <> 3 Then x = x + 250
            If column = 3 Then
                x = 150
                y = y + 25
                column = 1
            Else
                column = column + 1
            End If
            numberoflines = numberoflines + 1
            If numberoflines >= PageLngth Then
                ListStartPositionOC = i + 1
                PageNo = PageNo + 1
                e.HasMorePages = True
                Exit Sub
            End If
        Next
        y = y + 25
        ListStartPositionOC = i

        '**********************************************************
        'Comments

        x = 25
        If ListStartPositionComm < lstCom.Items.Count Then
            e.Graphics.DrawLine(Pens.Black, 25, y, 1075, y)
            e.Graphics.DrawString("Comments: ", Font4, Brushes.Black, 25, y)
        End If
        y = y + 25
        For i = 0 To lstCom.Items.Count - 1
            e.Graphics.DrawString(lstCom.Items(i), Font, Brushes.Black, x, y)
            y = y + 25
            numberoflines = numberoflines + 1
            If numberoflines > PageLngth Then
                ListStartPositionComm = i
                PageNo = PageNo + 1
                e.HasMorePages = True
                Exit Sub
            End If
        Next
        ListStartPositionComm = i
        ListStartPositionOC = i

        ListStartPositionSC = 0 : ListStartPositionWL = 2 : ListStartPositionRPT = 0
        ListStartPositionMed = 0 : ListStartPositionOC = 0 : ListStartPositionComm = 0
        PageNo = 1
    End Sub

    Function FindOCAbbr(OCcode)
        Dim xSel As String
        xSel = "select oabrv " &
               "from oc o " &
               "where o.oact = '1' and o.oc = '" & OCcode & "'"

        FindOCAbbr = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                FindOCAbbr = cRS(0)
            Loop
            cRS.Close()
            Conn.Close()
        End Using
    End Function
    Private Sub PrintPreview_Load(sender As Object, e As EventArgs) Handles PrintPreview.Load

    End Sub

    Private Sub btnOCGrid_Click(sender As Object, e As EventArgs) Handles btnOCGrid.Click
        If OralSamp Then
            frmSOEGridInq.ShowDialog()
        Else
            frmOEGridInq.ShowDialog()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim ImgName As String = "\\CMPFS1\Affidavit\" & lblSpecNo.Text & "AFF.pdf", SecondImgName As String = ""
        SecondImgName = CLSLIMS.ReplacePiece(ImgName, "\", 3, "CL-AGILENT")
        'Remove trailing "\" ???
        SecondImgName = Mid(SecondImgName, 1, Len(SecondImgName) - 1)
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click

        UpdIns.ShowDialog()
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        PDFView.PDF1.src = ""

        Dim CurrDtTm As String = Format(Now, "yyyyMMddhhmmss"), Cmd As New ProcessStartInfo
        If Mid(lblSpecNo.Text, 1, 1) = "5" Then
            If File.Exists("C:\CLSLIMS\MoleRpt.pdf") Then
                File.Delete("C:\CLSLIMS\MoleRpt.pdf")
            End If

            Cmd.FileName = "C:\CLSLIMS\MoleRpt.exe"
            Cmd.Arguments = lblSpecNo.Text
            Cmd.WindowStyle = ProcessWindowStyle.Hidden
            Cmd.WorkingDirectory = "C:\CLSLIMS\"
            Process.Start(Cmd)
        Else
            If File.Exists("C:\CLSLIMS\Files\" & lblSpecNo.Text & "r.pdf") Then
                File.Delete("C:\CLSLIMS\Files\" & lblSpecNo.Text & "r.pdf")
            End If
            Shell("C:\CLSLIMS\ApolloPDFB.exe " & lblSpecNo.Text & " 500 r C:\CLSLIMS\ " & CurrDtTm & " " & lblSpecNo.Text & " False HL7-TNCL-PDF~999~1~0~1~" & Pnd(lblSpecNo.Text) & "~EDI")
            Dim FileNotFound As Boolean = True
            Do While FileNotFound
                If File.Exists("C:\CLSLIMS\Files\" & lblSpecNo.Text & "r.pdf") Then
                    FileNotFound = False
                Else
                    System.Threading.Thread.Sleep(2000)
                End If
            Loop
            CLSLIMS.ShellExecute("C:\CLSLIMS\Files\" & lblSpecNo.Text & "r.pdf")
            'PDFView.PDF1.src = "C:\CLSLIMS\Files\" & lblSpecNo.Text & "r.pdf"
            'PDFView.ShowDialog()
            'PDFView.Close()
        End If
    End Sub
    Public Function Pnd(ByVal specno As String) As Integer
        Pnd = 1
        Dim xSel As String
        xSel = "Select count(*) from QPRES Where PSPECNO='" & lblSpecNo.Text & "'"

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                If cRS(0) = 0 Then
                    Pnd = 0
                End If
            End If
            cRS.Close()
            Conn.Close()
        End Using
    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim ImgName As String = "\\CMPFS1\MedRec\" & lblSpecNo.Text & "MR.pdf", SecondImgName As String = ""
        SecondImgName = CLSLIMS.ReplacePiece(ImgName, "\", 3, "CL-AGILENT")
        'Remove trailing "\" ???
        SecondImgName = Mid(SecondImgName, 1, Len(SecondImgName) - 1)
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        End If
    End Sub
End Class