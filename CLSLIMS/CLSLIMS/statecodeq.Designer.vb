﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Queue
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Queue))
        Me.cbDept = New System.Windows.Forms.ComboBox()
        Me.cbType = New System.Windows.Forms.ComboBox()
        Me.lblType = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblspecno = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblspecnum = New System.Windows.Forms.Label()
        Me.lblpflname = New System.Windows.Forms.Label()
        Me.lblpdob = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblage = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblsex = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblprace = New System.Windows.Forms.Label()
        Me.lblcolldatetime = New System.Windows.Forms.Label()
        Me.lblreqphy = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblaname = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblfax = New System.Windows.Forms.Label()
        Me.lblacctphone = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lstintcom = New System.Windows.Forms.ListBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lststatcodes = New System.Windows.Forms.ListBox()
        Me.lblrecvdatetime = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lbldept = New System.Windows.Forms.Label()
        Me.btnPDF = New System.Windows.Forms.Button()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.ppd = New System.Windows.Forms.PrintPreviewDialog()
        Me.Notes = New System.Windows.Forms.Button()
        Me.DGView1 = New System.Windows.Forms.DataGridView()
        Me.ListHeader = New System.Windows.Forms.ListBox()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.DGView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbDept
        '
        Me.cbDept.FormattingEnabled = True
        Me.cbDept.Location = New System.Drawing.Point(134, 9)
        Me.cbDept.Name = "cbDept"
        Me.cbDept.Size = New System.Drawing.Size(121, 21)
        Me.cbDept.TabIndex = 0
        '
        'cbType
        '
        Me.cbType.FormattingEnabled = True
        Me.cbType.Location = New System.Drawing.Point(134, 42)
        Me.cbType.Name = "cbType"
        Me.cbType.Size = New System.Drawing.Size(121, 21)
        Me.cbType.TabIndex = 1
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(71, 44)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(44, 19)
        Me.lblType.TabIndex = 3
        Me.lblType.Text = "Type"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(139, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Specimen Number:"
        '
        'lblspecno
        '
        Me.lblspecno.AutoSize = True
        Me.lblspecno.Location = New System.Drawing.Point(122, 18)
        Me.lblspecno.Name = "lblspecno"
        Me.lblspecno.Size = New System.Drawing.Size(0, 16)
        Me.lblspecno.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Patient Name:"
        '
        'lblspecnum
        '
        Me.lblspecnum.AutoSize = True
        Me.lblspecnum.BackColor = System.Drawing.SystemColors.Window
        Me.lblspecnum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblspecnum.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblspecnum.Location = New System.Drawing.Point(217, 6)
        Me.lblspecnum.Name = "lblspecnum"
        Me.lblspecnum.Size = New System.Drawing.Size(2, 18)
        Me.lblspecnum.TabIndex = 4
        '
        'lblpflname
        '
        Me.lblpflname.AutoSize = True
        Me.lblpflname.BackColor = System.Drawing.SystemColors.Window
        Me.lblpflname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblpflname.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpflname.Location = New System.Drawing.Point(217, 30)
        Me.lblpflname.Name = "lblpflname"
        Me.lblpflname.Size = New System.Drawing.Size(2, 18)
        Me.lblpflname.TabIndex = 5
        '
        'lblpdob
        '
        Me.lblpdob.AutoSize = True
        Me.lblpdob.BackColor = System.Drawing.SystemColors.Window
        Me.lblpdob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblpdob.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpdob.Location = New System.Drawing.Point(755, 48)
        Me.lblpdob.Name = "lblpdob"
        Me.lblpdob.Size = New System.Drawing.Size(2, 18)
        Me.lblpdob.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(703, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 17)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "DOB:"
        '
        'lblage
        '
        Me.lblage.AutoSize = True
        Me.lblage.BackColor = System.Drawing.SystemColors.Window
        Me.lblage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblage.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblage.Location = New System.Drawing.Point(951, 50)
        Me.lblage.Name = "lblage"
        Me.lblage.Size = New System.Drawing.Size(2, 18)
        Me.lblage.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(872, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 17)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Age:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(8, 140)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(142, 17)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Receive Date/Time:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 167)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(136, 17)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "Collect Date/Time:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(8, 191)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(106, 17)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Ord Physician:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(703, 79)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 17)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Sex:"
        '
        'lblsex
        '
        Me.lblsex.AutoSize = True
        Me.lblsex.BackColor = System.Drawing.SystemColors.Window
        Me.lblsex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblsex.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblsex.Location = New System.Drawing.Point(753, 77)
        Me.lblsex.Name = "lblsex"
        Me.lblsex.Size = New System.Drawing.Size(2, 18)
        Me.lblsex.TabIndex = 19
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(872, 81)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(46, 17)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Race:"
        '
        'lblprace
        '
        Me.lblprace.AutoSize = True
        Me.lblprace.BackColor = System.Drawing.SystemColors.Window
        Me.lblprace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblprace.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblprace.Location = New System.Drawing.Point(951, 76)
        Me.lblprace.Name = "lblprace"
        Me.lblprace.Size = New System.Drawing.Size(2, 18)
        Me.lblprace.TabIndex = 21
        '
        'lblcolldatetime
        '
        Me.lblcolldatetime.AutoSize = True
        Me.lblcolldatetime.BackColor = System.Drawing.SystemColors.Window
        Me.lblcolldatetime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcolldatetime.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcolldatetime.Location = New System.Drawing.Point(217, 170)
        Me.lblcolldatetime.Name = "lblcolldatetime"
        Me.lblcolldatetime.Size = New System.Drawing.Size(2, 18)
        Me.lblcolldatetime.TabIndex = 22
        '
        'lblreqphy
        '
        Me.lblreqphy.AutoSize = True
        Me.lblreqphy.BackColor = System.Drawing.SystemColors.Window
        Me.lblreqphy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblreqphy.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblreqphy.Location = New System.Drawing.Point(217, 191)
        Me.lblreqphy.Name = "lblreqphy"
        Me.lblreqphy.Size = New System.Drawing.Size(2, 18)
        Me.lblreqphy.TabIndex = 24
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(425, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 17)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Acct Name:"
        '
        'lblaname
        '
        Me.lblaname.AutoSize = True
        Me.lblaname.BackColor = System.Drawing.SystemColors.Window
        Me.lblaname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblaname.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblaname.Location = New System.Drawing.Point(520, 9)
        Me.lblaname.Name = "lblaname"
        Me.lblaname.Size = New System.Drawing.Size(2, 18)
        Me.lblaname.TabIndex = 26
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblfax)
        Me.Panel1.Controls.Add(Me.lblacctphone)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.lblAddress)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.lstintcom)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.lststatcodes)
        Me.Panel1.Controls.Add(Me.lblrecvdatetime)
        Me.Panel1.Controls.Add(Me.lblaname)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.lblreqphy)
        Me.Panel1.Controls.Add(Me.lblcolldatetime)
        Me.Panel1.Controls.Add(Me.lblprace)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.lblsex)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.lblage)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lblpdob)
        Me.Panel1.Controls.Add(Me.lblpflname)
        Me.Panel1.Controls.Add(Me.lblspecnum)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.lblspecno)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(12, 358)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1075, 334)
        Me.Panel1.TabIndex = 5
        '
        'lblfax
        '
        Me.lblfax.AutoSize = True
        Me.lblfax.BackColor = System.Drawing.SystemColors.Window
        Me.lblfax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblfax.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfax.Location = New System.Drawing.Point(520, 62)
        Me.lblfax.Name = "lblfax"
        Me.lblfax.Size = New System.Drawing.Size(2, 18)
        Me.lblfax.TabIndex = 37
        '
        'lblacctphone
        '
        Me.lblacctphone.AutoSize = True
        Me.lblacctphone.BackColor = System.Drawing.SystemColors.Window
        Me.lblacctphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblacctphone.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblacctphone.Location = New System.Drawing.Point(519, 35)
        Me.lblacctphone.Name = "lblacctphone"
        Me.lblacctphone.Size = New System.Drawing.Size(2, 18)
        Me.lblacctphone.TabIndex = 36
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(426, 57)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 17)
        Me.Label15.TabIndex = 35
        Me.Label15.Text = "Acct Fax:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(425, 31)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 17)
        Me.Label14.TabIndex = 34
        Me.Label14.Text = "Acct Phone:"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.BackColor = System.Drawing.SystemColors.Window
        Me.lblAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(218, 57)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(2, 18)
        Me.lblAddress.TabIndex = 33
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(8, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(148, 17)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "Patient City, ST ZIP:"
        '
        'lstintcom
        '
        Me.lstintcom.FormattingEnabled = True
        Me.lstintcom.ItemHeight = 16
        Me.lstintcom.Location = New System.Drawing.Point(712, 245)
        Me.lstintcom.Name = "lstintcom"
        Me.lstintcom.Size = New System.Drawing.Size(358, 84)
        Me.lstintcom.TabIndex = 31
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(558, 245)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(148, 17)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "Internal Comments:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(8, 245)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(105, 17)
        Me.Label11.TabIndex = 29
        Me.Label11.Text = "Status Codes:"
        '
        'lststatcodes
        '
        Me.lststatcodes.FormattingEnabled = True
        Me.lststatcodes.ItemHeight = 16
        Me.lststatcodes.Location = New System.Drawing.Point(119, 245)
        Me.lststatcodes.Name = "lststatcodes"
        Me.lststatcodes.Size = New System.Drawing.Size(398, 84)
        Me.lststatcodes.TabIndex = 28
        '
        'lblrecvdatetime
        '
        Me.lblrecvdatetime.AutoSize = True
        Me.lblrecvdatetime.BackColor = System.Drawing.SystemColors.Window
        Me.lblrecvdatetime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblrecvdatetime.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblrecvdatetime.Location = New System.Drawing.Point(217, 145)
        Me.lblrecvdatetime.Name = "lblrecvdatetime"
        Me.lblrecvdatetime.Size = New System.Drawing.Size(2, 18)
        Me.lblrecvdatetime.TabIndex = 27
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(997, 698)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lbldept
        '
        Me.lbldept.AutoSize = True
        Me.lbldept.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldept.Location = New System.Drawing.Point(71, 9)
        Me.lbldept.Name = "lbldept"
        Me.lbldept.Size = New System.Drawing.Size(42, 19)
        Me.lbldept.TabIndex = 7
        Me.lbldept.Text = "Dept"
        '
        'btnPDF
        '
        Me.btnPDF.Location = New System.Drawing.Point(805, 698)
        Me.btnPDF.Name = "btnPDF"
        Me.btnPDF.Size = New System.Drawing.Size(75, 23)
        Me.btnPDF.TabIndex = 9
        Me.btnPDF.Text = "Requisition"
        Me.btnPDF.UseVisualStyleBackColor = True
        '
        'ppd
        '
        Me.ppd.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.ppd.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.ppd.ClientSize = New System.Drawing.Size(400, 300)
        Me.ppd.Document = Me.PrintDoc
        Me.ppd.Enabled = True
        Me.ppd.Icon = CType(resources.GetObject("ppd.Icon"), System.Drawing.Icon)
        Me.ppd.Name = "PrintPreviewDialog1"
        Me.ppd.Visible = False
        '
        'Notes
        '
        Me.Notes.Location = New System.Drawing.Point(901, 698)
        Me.Notes.Name = "Notes"
        Me.Notes.Size = New System.Drawing.Size(75, 23)
        Me.Notes.TabIndex = 11
        Me.Notes.Text = "Affidavit"
        Me.Notes.UseVisualStyleBackColor = True
        '
        'DGView1
        '
        Me.DGView1.AllowUserToAddRows = False
        Me.DGView1.AllowUserToDeleteRows = False
        Me.DGView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGView1.Location = New System.Drawing.Point(19, 85)
        Me.DGView1.Name = "DGView1"
        Me.DGView1.ReadOnly = True
        Me.DGView1.Size = New System.Drawing.Size(1074, 267)
        Me.DGView1.TabIndex = 12
        '
        'ListHeader
        '
        Me.ListHeader.Font = New System.Drawing.Font("Courier New", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListHeader.FormattingEnabled = True
        Me.ListHeader.ItemHeight = 16
        Me.ListHeader.Location = New System.Drawing.Point(12, 75)
        Me.ListHeader.Name = "ListHeader"
        Me.ListHeader.Size = New System.Drawing.Size(1081, 4)
        Me.ListHeader.TabIndex = 4
        Me.ListHeader.Visible = False
        '
        'btnExport
        '
        Me.btnExport.Location = New System.Drawing.Point(713, 698)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(75, 23)
        Me.btnExport.TabIndex = 13
        Me.btnExport.Text = "Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'Queue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1108, 733)
        Me.Controls.Add(Me.btnExport)
        Me.Controls.Add(Me.DGView1)
        Me.Controls.Add(Me.Notes)
        Me.Controls.Add(Me.btnPDF)
        Me.Controls.Add(Me.lbldept)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ListHeader)
        Me.Controls.Add(Me.lblType)
        Me.Controls.Add(Me.cbType)
        Me.Controls.Add(Me.cbDept)
        Me.Name = "Queue"
        Me.Text = "Status Code Queue"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbDept As ComboBox
    Friend WithEvents cbType As ComboBox
    Friend WithEvents lblType As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblspecno As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblpflname As Label
    Friend WithEvents lblpdob As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblage As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblsex As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblprace As Label
    Friend WithEvents lblcolldatetime As Label
    Friend WithEvents lblreqphy As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents lblaname As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnExit As Button
    Friend WithEvents lbldept As Label
    Friend WithEvents lblrecvdatetime As Label
    Friend WithEvents btnPDF As Button
    Friend WithEvents lststatcodes As ListBox
    Friend WithEvents Label11 As Label
    Friend WithEvents lstintcom As ListBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents lblAddress As Label
    Friend WithEvents lblfax As Label
    Friend WithEvents lblacctphone As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents PrintDoc As Printing.PrintDocument
    Friend WithEvents ppd As PrintPreviewDialog
    Friend WithEvents Notes As Button
    Public WithEvents lblspecnum As Label
    Friend WithEvents DGView1 As DataGridView
    Public WithEvents ListHeader As ListBox
    Friend WithEvents btnExport As Button
End Class
