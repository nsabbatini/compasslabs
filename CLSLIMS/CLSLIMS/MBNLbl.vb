﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class MBNLbl
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim Sel As String = "", Fnd As Boolean, CBtxt As String = ""
        If txtMBN.Text = "" And txtTray.Text = "" Then
            MsgBox("No MBN or Tray")
            Exit Sub
        End If
        If txtMBN.Text <> "" Then
            Sel = "Select distinct PBIx,PSPECNO From PBI B, PB C Where B.WL=C.WL And B.BATCH=C.BATCH And BACT=1 And B.BEFDT=C.BEFDT And MBATCH='" & txtMBN.Text & "' Order by PBIx"
        ElseIf txtTray.Text <> "" Then
            Sel = "Select TRAYx, PSPECNO From XTRAYS Where TRAY='" & txtTray.Text & "' And PSPECNO <> 'HOLDER' Order By TRAYx"
        Else
            Exit Sub
        End If
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Fnd = False
        Do While rs.Read()
            QuickOE.PrintSampLabel(rs(1))
            Fnd = True
        Loop
        rs.Close()
        If Not Fnd Then
            MsgBox("MBN/Tray not found")
        Else
            Me.Close()
            Me.Dispose()
        End If
    End Sub

End Class