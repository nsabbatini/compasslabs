﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmEMRList

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmEMRList_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If lstEMR.Items.Count = 0 Then
            Dim Sel As String = ""
            Sel = "Select EMRNAME, A.ACCT, ANAME, XREFACCT, RPTSUBDIR, EXT From xaccthl7 a, acct b Where a.acct=b.acct And aact=1 Order By EMRNAME,ANAME"
            lstEMR.Items.Clear()
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                Conn.Open()
                Dim Cmd As New SqlCommand(Sel, Conn)
                Dim rs As SqlDataReader = Cmd.ExecuteReader
                Do While rs.Read()
                    lstEMR.Items.Add(CLSLIMS.LJ(rs(0), 15) & CLSLIMS.LJ(rs(1), 15) & CLSLIMS.LJ(rs(2), 50) & CLSLIMS.LJ(rs(3), 15) & CLSLIMS.LJ(rs(4), 50) & CLSLIMS.LJ(rs(5), 5))
                Loop
                rs.Close()
                Conn.Close()
            End Using
        End If
    End Sub
End Class