﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmSOSMonNum
    Private Sub frmSOSMonNum_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Sel As String = "Select distinct substring(orderdate,1,4) ,substring(orderdate,5,2) from orders order by substring(orderdate,1,4) ,substring(orderdate,5,2)"
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            cmbDate.Items.Add(rs(0) & "/" & rs(1))
        Loop
        rs.Close()
        ConnMySql.Close()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim Sel As String = "select b.itemid,itemdesc,sum(qty) " &
            "From orders a, orderitems b, items c " &
            "where a.hseq=0 and b.hseq=0 and a.ordernum=b.ordernum and b.itemid=c.itemid " &
            "and a.status=10 and substring(orderdate,1,4)='" & CLSLIMS.Piece(cmbDate.Text, "/", 1) & "' and substring(orderdate,5,2)='" & CLSLIMS.Piece(cmbDate.Text, "/", 2) & "' " &
            "group by b.itemid,itemdesc "
        lvRpt.Items.Clear()
        Dim r As String = "", Acct As String = "", MoYr As String = "", pAcct As String = ""
        Dim lv As ListViewItem = Nothing
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            lv = lvRpt.Items.Add(rs(0))
            lv.SubItems.Add(rs(1))
            lv.SubItems.Add(Format(rs(2), "###,###"))
        Loop
        rs.Close()
        ConnMySql.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\MonRpt.csv", False)
        file.WriteLine("," & cmbDate.Text)
        file.WriteLine("Item,Item Description,Totals")
        For i = 0 To lvRpt.Items.Count - 1
            For j = 0 To 2
                If Not (lvRpt.Items(i).SubItems(j) Is Nothing) Then file.Write(Replace(lvRpt.Items(i).SubItems(j).Text, ",", "") & ",")
            Next j
            file.WriteLine()
        Next i
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\MonRpt.csv")
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class