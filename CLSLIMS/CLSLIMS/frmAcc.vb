﻿
Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class frmAcc
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmPay_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" And CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "JONATHON.MORGAN" And CLSLIMS.UID <> "STEVE.ALEXANDER" And CLSLIMS.UID <> "KELLY.ELMORE" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpStart.Format = DateTimePickerFormat.Custom
        dtpStart.CustomFormat = "MM/dd/yyyy"
        dtpStart.Text = DateAdd(DateInterval.Month, -3, Now)
        dtpEnd.Format = DateTimePickerFormat.Custom
        dtpEnd.CustomFormat = "MM/dd/yyyy"
        dtpEnd.Text = Now
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        If rbDOS.Checked = False And rbTrans.Checked = False Then
            MsgBox("Must select date type.")
            Exit Sub
        End If
        If rbCnts.Checked = False And rbDoll.Checked = False Then
            MsgBox("Must select report type.")
            Exit Sub
        End If
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\AccCnt.csv", False)
        Dim Title As String = "", r As String = "", p As Integer = 0, SQL As String = "", Acct As String = "", PrevAcct As String = "", cnt As Integer = 1
        file.WriteLine(",,,,,Accessions by Account by Payor")
        If rbDOS.Checked Then
            Title = "Date of Service: " & dtpStart.Text & " - " & dtpEnd.Text
            If rbCnts.Checked Then
                SQL = "select acct,finclass,count(distinct accession) from lbsdata " &
                  "where str_to_date(servicedate,'%m/%d/%Y') between str_to_date('" & dtpStart.Text & "','%m/%d/%Y') and str_to_date('" & dtpEnd.Text & "','%m/%d/%Y') " &
                  "group by acct,finclass order by acct,finclass"
                '"and txncodedesc in ('Medicare payment','Payment','Recoup Payment') group by acct,finclass order by acct,finclass"
            ElseIf rbDoll.Checked Then
                SQL = "select acct,finclass,sum(amount) from lbsdata " &
                  "where str_to_date(servicedate,'%m/%d/%Y') between str_to_date('" & dtpStart.Text & "','%m/%d/%Y') and str_to_date('" & dtpEnd.Text & "','%m/%d/%Y') " &
                  "group by acct,finclass order by acct,finclass"
            End If
        ElseIf rbTrans.Checked Then
            Title = "Transaction Date: " & dtpStart.Text & " - " & dtpEnd.Text
            If rbCnts.Checked Then
                SQL = "select acct,finclass,count(distinct accession) from lbsdata " &
                  "where str_to_date(posteddate,'%m/%d/%Y') between str_to_date('" & dtpStart.Text & "','%m/%d/%Y') and str_to_date('" & dtpEnd.Text & "','%m/%d/%Y') " &
                  "group by acct,finclass order by acct,finclass"
            ElseIf rbDoll.Checked Then
                SQL = "select acct,finclass,sum(amount) from lbsdata " &
                  "where str_to_date(posteddate,'%m/%d/%Y') between str_to_date('" & dtpStart.Text & "','%m/%d/%Y') and str_to_date('" & dtpEnd.Text & "','%m/%d/%Y') " &
                  "group by acct,finclass order by acct,finclass"
            End If
        End If
        file.WriteLine(",,,,," & Title)
        file.WriteLine("Account,Account Name,AETNA,ANTHEM,BCBS,BHEALT,CIGNA,COM,HUMANA,MC,MCD,MCDMCO,MCMCO,MD,P,TRIC,UHC,WC,Totals")
        Dim ConnMySqlFin As New MySqlConnection(CLSLIMS.strConMySQLFin)
        ConnMySqlFin.Open()
        Dim Cmd As New MySqlCommand(SQL, ConnMySqlFin)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            Acct = rs(0)
            If Acct <> PrevAcct Then
                If PrevAcct <> "" Then
                    r = Totals(r)
                    file.WriteLine(r)
                    cnt = cnt + 1
                    Debug.Print(Acct & ":" & cnt)
                End If
                r = Acct & "," & GetAcctName(Acct) & ",,,,,,,,,,,,,,,,,,"
                r = CLSLIMS.ReplacePiece(r, ",", FinClassPiece(rs(1)) + 2, rs(2))
                PrevAcct = Acct
            Else
                r = CLSLIMS.ReplacePiece(r, ",", FinClassPiece(rs(1)) + 2, rs(2))
            End If
        Loop
        rs.Close()
        ConnMySqlFin.Close()
        cnt = cnt + 3
        r = Totals(r)
        file.WriteLine(r)
        file.WriteLine(",,= SUM(c4: c" & cnt & "),=SUM(D4:D" & cnt & "),=SUM(E4:E" & cnt & "),=SUM(F4:F" & cnt & "),=SUM(G4:G" & cnt & "),=SUM(H4:H" & cnt & "),=SUM(I4:I" & cnt & "),=SUM(J4:J" & cnt & "),=SUM(K4:K" & cnt & "),=SUM(L4:L" & cnt & "),=SUM(M4:M" & cnt & "),=SUM(N4:N" & cnt & "),=SUM(O4:O" & cnt & "),=SUM(P4:P" & cnt & "),=SUM(Q4:Q" & cnt & "),=SUM(R4:R" & cnt & "),=SUM(S4:S" & cnt & ")")
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\AccCnt.csv")
        Me.Close()
        Me.Dispose()
    End Sub
    Public Function FinClassPiece(ByVal finclass As String) As Integer
        FinClassPiece = 0
        If finclass = "SP" Then finclass = "P"
        Dim FinClassstr As String = "AETNA^ANTHEM^BCBS^BHEALT^CIGNA^COM^HUMANA^MC^MCD^MCDMCO^MCMCO^MD^P^TRIC^UHC^WC"
        For i = 1 To 16
            If finclass = CLSLIMS.Piece(FinClassstr, "^", i) Then
                FinClassPiece = i
                Exit Function
            End If
        Next i
    End Function
    Public Function GetAcctName(ByVal Acct) As String
        GetAcctName = ""
        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where Acct = '" & Acct & "' order by aefdt desc"   ' and aact=1 "

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = Replace(cRS(1), ",", " ")
            Else
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Public Function Totals(ByVal str As String) As String
        Totals = str
        Dim Tot As Double = 0.00
        For i = 3 To 17
            Tot = Tot + Val(CLSLIMS.Piece(str, ",", i))
        Next i
        Totals = CLSLIMS.ReplacePiece(str, ",", 19, Tot)
    End Function
End Class