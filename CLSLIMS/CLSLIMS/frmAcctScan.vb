﻿
Imports CLSLIMS.CLSLIMS
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports iTextSharp.text.pdf
Imports iTextSharp.text

Public Class frmAcctScan
    Public scanner As String = "", AcctID As String = ""
    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub TextBox1_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.Enter Then
            ValidateAcct()
        End If
    End Sub

    Public Sub ValidateAcct()
        AcctID = ""
        If Not IsNumeric(Mid(txtAcct.Text, 2)) And Len(txtAcct.Text) > 1 And txtAcct.Text <> "PTSAMPLES" Then
            txtAcct.Text = GetAcctFromName(txtAcct.Text)
        End If
        If txtAcct.Text = "" Then Exit Sub
        Dim Sel As String = "Select ANAME from ACCT where ACCT='" & txtAcct.Text & "' and AACT=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            lblAcctName.Text = txtAcct.Text & ": " & rs(0)
            AcctID = txtAcct.Text
            btnA.Text = "New Acct Application" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "A"), "*", "")
            btnB.Text = "Profile Authorization" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "B"), "*", "")
            btnC.Text = "Physician Acknw + Bill" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "C"), "*", "")
            btnD.Text = "Web Authorization" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "D"), "*", "")
            btnE.Text = "Direct Bill" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "E"), "*", "")
            btnF.Text = "Misc" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "F"), "*", "")
            btnG.Text = "MOU" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "G"), "*", "")
        Else
            MsgBox("Account not found.")
            txtAcct.Focus()
            AcctID = ""
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
        txtAcct.Text = ""
        txtAcct.Focus()
    End Sub
    Private Function GetAcctFromName(ByVal AcctName As String) As String

        Dim xSel As String
        xSel = "select acct,aname " & _
               "from acct " & _
               "where upper(aname) like '" & UCase(AcctName) & "%' and aact=1 " & _
               "order by aname"

        Dim strAcct As String = ""
        GetAcctFromName = ""
        If AcctName = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strAcct = cRS(0) & Space(6 - Len(cRS(0))) & Mid(cRS(1), 1, 30)
                lstAcctNameSearch.Items.Add(strAcct)
            Loop
            If lstAcctNameSearch.Items.Count > 0 Then
                lstAcctNameSearch.Visible = True
                lstAcctNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
            lstAcctNameSearch.Width = 350
            lstAcctNameSearch.Height = 132
            lstAcctNameSearch.Location = New Point(88, 46)
            lstAcctNameSearch.BringToFront()
        End Using

    End Function

    Private Sub lstAcctNameSearch_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles lstAcctNameSearch.MouseDoubleClick
        txtAcct.Text = CLSLIMS.Piece(lstAcctNameSearch.SelectedItem, " ", 1)
        lstAcctNameSearch.Visible = False
        ValidateAcct()
    End Sub

    Private Sub frmAcctScan_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            Debug.Print(CLSLIMS.UID)
        End If
        If CLSLIMS.UID = "" Or CLSLIMS.GetDept(CLSLIMS.UID) <> "SALESUPPORT" Then
            If CLSLIMS.UID <> "NED.SABBATINI" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        txtAcct.Focus()
        btnA.Text = "New Acct Application"
        btnB.Text = "Profile Authorization"
        btnC.Text = "Physician Acknw + Bill"
        btnD.Text = "Web Authorization"
        btnE.Text = "Direct Bill"
        btnF.Text = "Misc"
        btnG.Text = "MOU"
        Try
            Dim inifile As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader("C:\CLSLIMS\Scanner.ini")
            scanner = inifile.ReadLine
            inifile.Close()
        Catch
        End Try

    End Sub

    Private Sub ScanDocs(ByVal ID As String)

        lblInfo.Text = "Scanning . . ."
        Dim Success As Boolean = False, AppendNotNec As Boolean = True
        'Check WorkingFile Folder - C:\CLSLIMS\Accounts
        If Not Directory.Exists("C:\CLSLIMS\Accounts") Then
            Directory.CreateDirectory("C:\CLSLIMS\Accounts")
        End If
        Dim WorkingFile As String = "C:\CLSLIMS\Accounts\" & Trim(AcctID) & ID & ".pdf"
        'Dim CopyFile As String = "\\Compass-scanpc\C\Compass\Accounts\" & Trim(AcctID) & ID & ".pdf"
        Dim CopyFile As String = "\\CMPFS1\Accounts\" & Trim(AcctID) & ID & ".pdf"
        Dim CopyFile2 As String = "\\CL-AGILENT\Accounts\" & Trim(AcctID) & ID & ".pdf"
        Dim tempscan As String = "C:\CLSLIMS\Accounts\tempscan.pdf", PDF12(2) As String
        Dim cmbscan As String = "C:\CLSLIMS\Accounts\ScanCmb.pdf"
        ScanImage.RemoveAllImages()

        If rbComplete.Checked = True Then
            If File.Exists(WorkingFile) Then
                File.Delete(WorkingFile)
            End If
        ElseIf rbAppend.Checked = True Then
            If File.Exists(CopyFile) Then
                FileCopy(CopyFile, WorkingFile)
                AppendNotNec = False
            Else
                AppendNotNec = True
                lblInfo.Text = "Append not needed."
            End If
        End If
            'ScanImage.LicenseKeys = "E378C7266C834EDE2B70E88E236EEE1C"
            'ScanImage.LicenseKeys = "50663FBE5F990BEFC170A6AC986CE8BA"
            ScanImage.LicenseKeys = "E9C5E61C6C3BEE4361A6AE53AB47C116"
        ScanImage.IfThrowException = False
        ScanImage.IfDisableSourceAfterAcquire = True
        ScanImage.IfShowUI = False
        Dim int As Integer = ScanImage.SourceCount
        For i = 0 To int
            If ScanImage.SourceNameItems(i) = scanner Then
                ScanImage.SelectSourceByIndex(i)
                Exit For
            End If
        Next i

        If ScanImage.CurrentSourceName = "" Then
            ScanImage.SelectSource()
        End If
        Debug.Print(ScanImage.CurrentSourceName)
        Success = ScanImage.CloseSource()
        Success = ScanImage.OpenSource()
        If Success Then
            'ScanImage.Resolution = 600
            Success = ScanImage.AcquireImage()
            If Success Then
                If rbComplete.Checked Then
                    Success = ScanImage.SaveAllAsPDF(WorkingFile)
                ElseIf rbAppend.Checked Then
                    If AppendNotNec Then
                        Success = ScanImage.SaveAllAsPDF(WorkingFile)
                    Else
                        ScanImage.LoadImage(WorkingFile)
                        Success = ScanImage.SaveAllAsPDF(WorkingFile)
                    End If
                End If
            End If
            If Success Then
                FileCopy(WorkingFile, CopyFile)
                FileCopy(WorkingFile, CopyFile2)
                btnA.Text = "New Acct Application" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "A"), "*", "")
                btnB.Text = "Profile Authorization" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "B"), "*", "")
                btnC.Text = "Physician Acknw + Bill" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "C"), "*", "")
                btnD.Text = "Web Authorization" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "D"), "*", "")
                btnE.Text = "Direct Bill" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "E"), "*", "")
                btnF.Text = "Misc" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "F"), "*", "")
                btnG.Text = "MOU" & IIf(CLSLIMS.ImgFile(txtAcct.Text, "G"), "*", "")
            End If
        End If
        lblInfo.Text = "Scan complete."
    End Sub
    Public Function CombinePDFs(ByVal FirstPDF As String, ByVal SecondPDF As String) As String
        CombinePDFs = FirstPDF
        Dim reader As iTextSharp.text.pdf.PdfReader = Nothing
        Dim reader2 As iTextSharp.text.pdf.PdfReader = Nothing
        Dim writer As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim cb As iTextSharp.text.pdf.PdfContentByte = Nothing
        Dim page As iTextSharp.text.pdf.PdfImportedPage = Nothing
        Dim rotation As Integer, PageCount As Integer, i As Integer = 1
        Dim WorkingFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim CombineFile = Path.Combine(WorkingFolder, "ScanCmb.pdf")
        Dim fs As New FileStream(CombineFile, FileMode.Create)
        Dim pdfDoc As New Document
        Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, fs)

        pdfDoc.Open()

        reader = New iTextSharp.text.pdf.PdfReader(FirstPDF)
        PageCount = reader.NumberOfPages
        cb = pdfWrite.DirectContent
        pdfDoc.NewPage()
        Do While i <= PageCount
            page = pdfWrite.GetImportedPage(reader, i)
            rotation = reader.GetPageRotation(i)
            cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
            pdfDoc.NewPage()
            i = i + 1
        Loop

        reader2 = New iTextSharp.text.pdf.PdfReader(SecondPDF)
        PageCount = reader2.NumberOfPages
        i = 1
        Do While i <= PageCount
            page = pdfWrite.GetImportedPage(reader2, i)
            rotation = reader2.GetPageRotation(i)
            cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
            pdfDoc.NewPage()
            i = i + 1
        Loop
        pdfDoc.Close()
        reader.Close()
        reader2.Close()

        fs.Close()

        'My.Computer.FileSystem.CopyFile(CombineFile, FirstPDF, overwrite:=True)
        CombinePDFs = CombineFile

    End Function
    Public Shared Function MergePdfFiles(ByVal pdfFiles() As String, ByVal outputPath As String) As Boolean
        Dim result As Boolean = False
        Dim pdfCount As Integer = 0     'total input pdf file count
        Dim f As Integer = 0    'pointer to current input pdf file
        Dim fileName As String
        Dim reader As iTextSharp.text.pdf.PdfReader = Nothing
        Dim pageCount As Integer = 0
        Dim pdfDoc As iTextSharp.text.Document = Nothing    'the output pdf document
        Dim writer As PdfWriter = Nothing
        Dim cb As PdfContentByte = Nothing

        Dim page As PdfImportedPage = Nothing
        Dim rotation As Integer = 0

        Try
            pdfCount = pdfFiles.Length
            If pdfCount > 1 Then
                'Open the 1st item in the array PDFFiles
                fileName = pdfFiles(f)
                reader = New iTextSharp.text.pdf.PdfReader(fileName)
                'Get page count
                pageCount = reader.NumberOfPages

                pdfDoc = New iTextSharp.text.Document(reader.GetPageSizeWithRotation(1), 18, 18, 18, 18)

                writer = PdfWriter.GetInstance(pdfDoc, New FileStream(outputPath, FileMode.OpenOrCreate))


                With pdfDoc
                    .Open()
                End With
                'Instantiate a PdfContentByte object
                cb = writer.DirectContent
                'Now loop thru the input pdfs
                While f < pdfCount
                    'Declare a page counter variable
                    Dim i As Integer = 0
                    'Loop thru the current input pdf's pages starting at page 1
                    While i < pageCount
                        i += 1
                        'Get the input page size
                        pdfDoc.SetPageSize(reader.GetPageSizeWithRotation(i))
                        'Create a new page on the output document
                        pdfDoc.NewPage()
                        'If it is the 1st page, we add bookmarks to the page
                        'Now we get the imported page
                        page = writer.GetImportedPage(reader, i)
                        'Read the imported page's rotation
                        rotation = reader.GetPageRotation(i)
                        'Then add the imported page to the PdfContentByte object as a template based on the page's rotation
                        If rotation = 90 Then
                            cb.AddTemplate(page, 0, -1.0F, 1.0F, 0, 0, reader.GetPageSizeWithRotation(i).Height)
                        ElseIf rotation = 270 Then
                            cb.AddTemplate(page, 0, 1.0F, -1.0F, 0, reader.GetPageSizeWithRotation(i).Width + 60, -30)
                        Else
                            cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
                        End If
                    End While
                    'Increment f and read the next input pdf file
                    f += 1
                    reader.Close()
                    If f < pdfCount Then
                        fileName = pdfFiles(f)
                        reader = New iTextSharp.text.pdf.PdfReader(fileName)
                        pageCount = reader.NumberOfPages
                    End If
                End While
                'When all done, we close the document so that the pdfwriter object can write it to the output file
                pdfDoc.Close()
                reader.Close()
                result = True
            End If
        Catch ex As Exception
            Return False
        End Try
        Return result
    End Function
    Private Sub btnA_Click(sender As Object, e As EventArgs) Handles btnA.Click
        ScanDocs("A")
    End Sub

    Private Sub btnB_Click(sender As Object, e As EventArgs) Handles btnB.Click
        ScanDocs("B")
    End Sub

    Private Sub btnC_Click(sender As Object, e As EventArgs) Handles btnC.Click
        ScanDocs("C")
    End Sub

    Private Sub btnD_Click(sender As Object, e As EventArgs) Handles btnD.Click
        ScanDocs("D")
    End Sub

    Private Sub btnE_Click(sender As Object, e As EventArgs) Handles btnE.Click
        ScanDocs("E")
    End Sub

    Private Sub btnF_Click(sender As Object, e As EventArgs) Handles btnF.Click
        ScanDocs("F")
    End Sub
    Private Sub btnG_Click(sender As Object, e As EventArgs) Handles btnG.Click
        ScanDocs("G")
    End Sub

End Class