﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRptViewer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rptView = New GrapeCity.ActiveReports.Viewer.Win.Viewer()
        Me.SuspendLayout()
        '
        'rptView
        '
        Me.rptView.CurrentPage = 0
        Me.rptView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rptView.Location = New System.Drawing.Point(0, 0)
        Me.rptView.Name = "rptView"
        Me.rptView.PreviewPages = 0
        '
        '
        '
        '
        '
        '
        Me.rptView.Sidebar.ParametersPanel.ContextMenu = Nothing
        Me.rptView.Sidebar.ParametersPanel.Text = "Parameters"
        Me.rptView.Sidebar.ParametersPanel.Width = 200
        '
        '
        '
        Me.rptView.Sidebar.SearchPanel.ContextMenu = Nothing
        Me.rptView.Sidebar.SearchPanel.Text = "Search results"
        Me.rptView.Sidebar.SearchPanel.Width = 200
        '
        '
        '
        Me.rptView.Sidebar.ThumbnailsPanel.ContextMenu = Nothing
        Me.rptView.Sidebar.ThumbnailsPanel.Text = "Page thumbnails"
        Me.rptView.Sidebar.ThumbnailsPanel.Width = 200
        Me.rptView.Sidebar.ThumbnailsPanel.Zoom = 0.1R
        '
        '
        '
        Me.rptView.Sidebar.TocPanel.ContextMenu = Nothing
        Me.rptView.Sidebar.TocPanel.Expanded = True
        Me.rptView.Sidebar.TocPanel.Text = "Document map"
        Me.rptView.Sidebar.TocPanel.Width = 200
        Me.rptView.Sidebar.Width = 200
        Me.rptView.Size = New System.Drawing.Size(1202, 862)
        Me.rptView.TabIndex = 0
        '
        'frmRptViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1202, 862)
        Me.Controls.Add(Me.rptView)
        Me.Name = "frmRptViewer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRptViewer"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents rptView As GrapeCity.ActiveReports.Viewer.Win.Viewer
End Class
