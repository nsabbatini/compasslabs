﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDefGene
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lblNew = New System.Windows.Forms.Label()
        Me.cmbGene = New System.Windows.Forms.ComboBox()
        Me.cmbCPT = New System.Windows.Forms.ComboBox()
        Me.cmbAbbr = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(69, 162)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(76, 33)
        Me.btnNew.TabIndex = 35
        Me.btnNew.Text = "New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(185, 162)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(76, 33)
        Me.btnSave.TabIndex = 33
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(306, 162)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 33)
        Me.btnExit.TabIndex = 34
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lblNew
        '
        Me.lblNew.AutoSize = True
        Me.lblNew.ForeColor = System.Drawing.Color.Red
        Me.lblNew.Location = New System.Drawing.Point(457, 26)
        Me.lblNew.Name = "lblNew"
        Me.lblNew.Size = New System.Drawing.Size(47, 19)
        Me.lblNew.TabIndex = 32
        Me.lblNew.Text = "NEW"
        '
        'cmbGene
        '
        Me.cmbGene.FormattingEnabled = True
        Me.cmbGene.Location = New System.Drawing.Point(148, 21)
        Me.cmbGene.Name = "cmbGene"
        Me.cmbGene.Size = New System.Drawing.Size(302, 27)
        Me.cmbGene.TabIndex = 27
        '
        'cmbCPT
        '
        Me.cmbCPT.FormattingEnabled = True
        Me.cmbCPT.Location = New System.Drawing.Point(148, 94)
        Me.cmbCPT.Name = "cmbCPT"
        Me.cmbCPT.Size = New System.Drawing.Size(140, 27)
        Me.cmbCPT.TabIndex = 31
        '
        'cmbAbbr
        '
        Me.cmbAbbr.FormattingEnabled = True
        Me.cmbAbbr.Location = New System.Drawing.Point(149, 56)
        Me.cmbAbbr.Name = "cmbAbbr"
        Me.cmbAbbr.Size = New System.Drawing.Size(139, 27)
        Me.cmbAbbr.TabIndex = 29
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(81, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 19)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "CPT:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 19)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Abbreviation:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(72, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 19)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Gene:"
        '
        'frmDefGene
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(513, 237)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblNew)
        Me.Controls.Add(Me.cmbGene)
        Me.Controls.Add(Me.cmbCPT)
        Me.Controls.Add(Me.cmbAbbr)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmDefGene"
        Me.Text = "Define Genes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnNew As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents lblNew As Label
    Friend WithEvents cmbGene As ComboBox
    Friend WithEvents cmbCPT As ComboBox
    Friend WithEvents cmbAbbr As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
End Class
