﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports DevExpress.XtraEditors.DXErrorProvider

Public Class frmNewInventoryItems
    Public VendorId As Int16 = 0
    Private _departmentId As Int16 = 0
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dtAddItems As DataTable
    Private Sub frmNewInventoryItems_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim result = GetVendorDb()

        Dim departments = GetDepartments()

        lookupDepartments.Properties.DataSource = departments
        lookupDepartments.Properties.DisplayMember = "DepartmentName"
        lookupDepartments.Properties.ValueMember = "DepartmentId"
        lookupDepartments.EditValue = _departmentId

        lookupVendors.Properties.DataSource = result
        lookupVendors.Properties.DisplayMember = "VendorName"
        lookupVendors.Properties.ValueMember = "Id"


        lookupVendors.EditValue = VendorId
        If Not VendorId = 0 Then
            lookupVendors.ReadOnly = True
        End If

        DxValidationProvider1.ValidationMode = ValidationMode.Manual

        txtItemDesc.Focus()

        _dtAddItems = New DataTable
        _dtAddItems = CreateDataTable()
        InitValidationRules()

    End Sub
    Private Function GetDepartments() As Object
        Try

            Using getDepartmentinfo As New Departments()
                _fileName = _fileDir & GetDepartmentsSql
                getDepartmentinfo._sqlText = ReadSqlFile(_fileName)
                Return getDepartmentinfo.GetDepartmentInfo.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmNewInventoryItems.GetVendorDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Function
    Private Function GetVendorDb() As Object

        Try

            Using getVendors As New GetInventoryDb()
                _fileName = _fileDir & VendorSqlFile
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetVendorInfo.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmNewInventoryItems.GetVendorDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If GridView1.DataRowCount = 0 Then
            MsgBox("You have not added any items to be saved!", MsgBoxStyle.Exclamation, "No Data")
            Exit Sub
        End If

        If SaveInventoryItems() Then
            MessageBox.Show(Me, "Inventory items successfully saved!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ClearTheControls(True)
        End If


    End Sub

    Private Function SaveInventoryItems() As Boolean
        Dim arr As New ArrayList()
        _fileName = _fileDir & InsertNewItemsSql

        Try
            Using items As New InventoryControl() With {
                ._sqlText = ReadSqlFile(_fileName),
                ._departmentId = lookupDepartments.EditValue
            }
                For i = 0 To GridView1.DataRowCount - 1
                    arr.Add(CType(GridView1.GetRowCellValue(i, "VendorId"), Int16))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "ItemDesc"), String))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "Units"), String))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "ItemPerUnit"), Int16))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "ItemCost"), Double))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "StockNumber"), String))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "MinQty"), Int16))
                    arr.Add(CType(GridView1.GetRowCellValue(i, "CurrentQty"), Int16))
                    items.SaveInventoryItems(arr)
                    arr.Clear()
                Next

            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmNewInventoryItems.SaveInventoryItems", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            Return False
        End Try

        Return True


    End Function

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If lookupVendors.EditValue = Nothing Then
            MsgBox("Must select a vendor!", MsgBoxStyle.Exclamation, "No Vendor Selected")
            Exit Sub
        End If
        If lookupDepartments.EditValue = Nothing Then
            MsgBox("Must select a department!", MsgBoxStyle.Exclamation, "No department Selected")
            Exit Sub
        End If

        If DxValidationProvider1.Validate() Then
            RemoveControlErrors()
            Dim arow As DataRow

            With _dtAddItems
                arow = .NewRow
                arow(0) = lookupVendors.EditValue
                arow(1) = StrConv(txtItemDesc.Text.Trim(), vbProperCase)
                arow(2) = cboUnits.Text
                arow(3) = txtItemsPerUnit.Text
                arow(4) = txtItemCost.Text
                arow(5) = txtStockNumber.Text
                arow(6) = txtMinQty.Text
                arow(7) = txtCurrentQty.Text
                'arow(8) = txtDaysToReceive.Text
                .Rows.Add(arow)
            End With

            grdItems.DataSource = _dtAddItems.DefaultView

        End If


    End Sub
    Private Function CreateDataTable() As DataTable

        Dim _dtItems As DataTable = New DataTable

        With _dtItems.Columns
            .Add("VendorId", GetType(Int16))
            .Add("ItemDesc", GetType(String))
            .Add("Units", GetType(String))
            .Add("ItemPerUnit", GetType(Int16))
            .Add("ItemCost", GetType(Double))
            .Add("StockNumber", GetType(String))
            .Add("MinQty", GetType(Int16))
            .Add("CurrentQty", GetType(Int16))
            '.Add("DaysReceive", GetType(Int16))
        End With

        Return _dtItems


    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If GridView1.DataRowCount >= 1 Then
            Dim x = MessageBox.Show(Me, "Are you sure you want to cancel? You have unsaved items!", "UnSaved Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = DialogResult.Yes Then
                Me.Close()
            End If
        Else
            Me.Close()

        End If

    End Sub
#Region "Validation Rules"
    Private Sub InitValidationRules()

        Dim notEmptyValidationRule As New ConditionValidationRule()
        notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank
        notEmptyValidationRule.ErrorText = "Please enter a value"

        DxValidationProvider1.SetValidationRule(txtCurrentQty, notEmptyValidationRule)
        DxValidationProvider1.SetValidationRule(txtMinQty, notEmptyValidationRule)
        DxValidationProvider1.SetValidationRule(txtItemDesc, notEmptyValidationRule)
        DxValidationProvider1.SetValidationRule(txtItemsPerUnit, notEmptyValidationRule)
        DxValidationProvider1.SetValidationRule(cboUnits, notEmptyValidationRule)
        DxValidationProvider1.SetValidationRule(txtItemCost, notEmptyValidationRule)
        'DxValidationProvider1.SetValidationRule(txtDaysToReceive, notEmptyValidationRule)


    End Sub
    Private Sub RemoveControlErrors()

        DxValidationProvider1.RemoveControlError(txtCurrentQty)
        DxValidationProvider1.RemoveControlError(txtMinQty)
        DxValidationProvider1.RemoveControlError(txtItemDesc)
        DxValidationProvider1.RemoveControlError(txtItemsPerUnit)
        DxValidationProvider1.RemoveControlError(cboUnits)
        DxValidationProvider1.RemoveControlError(txtItemCost)
        'DxValidationProvider1.RemoveControlError(txtDaysToReceive)


    End Sub
#End Region

    Private Sub btnAddNewItem_Click(sender As Object, e As EventArgs) Handles btnAddNewItem.Click
        ClearTheControls()
        txtItemDesc.Focus()

    End Sub
    Private Sub ClearTheControls(Optional ByVal isAddItemSameVendor As Boolean = False)
        If Not isAddItemSameVendor Then
            txtCurrentQty.Text = String.Empty
            'txtDaysToReceive.Text = String.Empty
            txtItemCost.Text = String.Empty
            txtItemDesc.Text = String.Empty
            txtItemsPerUnit.Text = String.Empty
            txtMinQty.Text = String.Empty
            txtStockNumber.Text = String.Empty
        Else
            lookupVendors.EditValue = Nothing
            lookupVendors.SelectedText = String.Empty
            txtCurrentQty.Text = String.Empty
            'txtDaysToReceive.Text = String.Empty
            txtItemCost.Text = String.Empty
            txtItemDesc.Text = String.Empty
            txtItemsPerUnit.Text = String.Empty
            txtMinQty.Text = String.Empty
            txtStockNumber.Text = String.Empty

            _dtAddItems.Clear()

        End If


    End Sub
End Class