﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDefCutRng
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCutOff = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblLowTop = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblModTop = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtLowBot = New System.Windows.Forms.TextBox()
        Me.txtModBot = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblHighBot = New System.Windows.Forms.Label()
        Me.btnLatest = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(65, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 19)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Cutoff:"
        '
        'txtCutOff
        '
        Me.txtCutOff.Location = New System.Drawing.Point(137, 35)
        Me.txtCutOff.Name = "txtCutOff"
        Me.txtCutOff.Size = New System.Drawing.Size(100, 27)
        Me.txtCutOff.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(178, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 19)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Ranges"
        '
        'lblLowTop
        '
        Me.lblLowTop.AutoSize = True
        Me.lblLowTop.Location = New System.Drawing.Point(235, 124)
        Me.lblLowTop.Name = "lblLowTop"
        Me.lblLowTop.Size = New System.Drawing.Size(73, 19)
        Me.lblLowTop.TabIndex = 6
        Me.lblLowTop.Text = "LowTop"
        Me.lblLowTop.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(213, 124)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(16, 19)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "-"
        '
        'lblModTop
        '
        Me.lblModTop.AutoSize = True
        Me.lblModTop.Location = New System.Drawing.Point(235, 158)
        Me.lblModTop.Name = "lblModTop"
        Me.lblModTop.Size = New System.Drawing.Size(74, 19)
        Me.lblModTop.TabIndex = 8
        Me.lblModTop.Text = "ModTop"
        Me.lblModTop.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(213, 158)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(16, 19)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "-"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(178, 195)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(22, 19)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "<"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(82, 124)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 19)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Low:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(37, 158)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(93, 19)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Moderate:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(78, 195)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 19)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "High:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtLowBot
        '
        Me.txtLowBot.Location = New System.Drawing.Point(141, 121)
        Me.txtLowBot.Name = "txtLowBot"
        Me.txtLowBot.Size = New System.Drawing.Size(66, 27)
        Me.txtLowBot.TabIndex = 15
        '
        'txtModBot
        '
        Me.txtModBot.Location = New System.Drawing.Point(141, 155)
        Me.txtModBot.Name = "txtModBot"
        Me.txtModBot.Size = New System.Drawing.Size(66, 27)
        Me.txtModBot.TabIndex = 16
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(154, 284)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 31)
        Me.btnSave.TabIndex = 18
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(248, 284)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 31)
        Me.btnExit.TabIndex = 19
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(254, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 19)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "ND if > cutoff"
        '
        'lblHighBot
        '
        Me.lblHighBot.AutoSize = True
        Me.lblHighBot.Location = New System.Drawing.Point(200, 195)
        Me.lblHighBot.Name = "lblHighBot"
        Me.lblHighBot.Size = New System.Drawing.Size(74, 19)
        Me.lblHighBot.TabIndex = 21
        Me.lblHighBot.Text = "HighBot"
        Me.lblHighBot.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnLatest
        '
        Me.btnLatest.Location = New System.Drawing.Point(55, 284)
        Me.btnLatest.Name = "btnLatest"
        Me.btnLatest.Size = New System.Drawing.Size(75, 31)
        Me.btnLatest.TabIndex = 22
        Me.btnLatest.Text = "Latest"
        Me.btnLatest.UseVisualStyleBackColor = True
        '
        'frmDefCutRng
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 336)
        Me.Controls.Add(Me.btnLatest)
        Me.Controls.Add(Me.lblHighBot)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtModBot)
        Me.Controls.Add(Me.txtLowBot)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblModTop)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblLowTop)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCutOff)
        Me.Controls.Add(Me.Label2)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmDefCutRng"
        Me.Text = "Define Cutoff/Ranges"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents txtCutOff As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents lblLowTop As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lblModTop As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtLowBot As TextBox
    Friend WithEvents txtModBot As TextBox
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents lblHighBot As Label
    Friend WithEvents btnLatest As Button
End Class
