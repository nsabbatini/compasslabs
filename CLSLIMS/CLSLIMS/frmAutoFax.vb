﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System
Imports System.Data
Imports System.Data.SqlClient


Public Class frmAutoFax
    Public NewDate As Boolean = False
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmAutoFax_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpCompDt.Format = DateTimePickerFormat.Custom
        dtpCompDt.CustomFormat = "MM/dd/yyyy"
        dtpCompDt.Text = Now
        Call Upd()
    End Sub

    Private Sub btnUpd_Click(sender As Object, e As EventArgs) Handles btnUpd.Click
        Call Upd()
    End Sub
    Public Sub Upd()
        Dim TranDt As String = CLSLIMS.CDT(Format(Now, "yyyyMMdd"), "V")
        TranDt = Format(DateAdd(DateInterval.Day, -2, CDate(TranDt)), "yyyyMMdd")
        If NewDate Then
            TranDt = Format(CDate(CLSLIMS.CDT(CLSLIMS.Piece(dtpCompDt.Text, "/", 3) & CLSLIMS.Piece(dtpCompDt.Text, "/", 1) & CLSLIMS.Piece(dtpCompDt.Text, "/", 2), "V")), "yyyyMMdd")
            NewDate = False
        End If

        Dim root As New TreeNode
        Dim Sel As String = "", Data As String = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Dim rs As MySqlDataReader
        tvInfo.Nodes.Clear()
        root = New TreeNode("Queues")
        tvInfo.Nodes.Add(root)
        tvInfo.Nodes(0).Nodes.Add(New TreeNode("Pending"))
        Sel = "Select jobid,transdt,acct,speccnt,statusdesc from autofax where hseq=0 and (status is null or status='PEND')"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader
        Do While rs.Read
            Data = rs(0) & "^" & CLSLIMS.CDT(rs(1), "V") & "^" & rs(2) & "^^" & CLSLIMS.GetAcctName(rs(2)) & "^        " & rs(3) & "^" & rs(4)
            AddtoTree(0, Data)
        Loop
        rs.Close()
        tvInfo.Nodes(0).Nodes.Add(New TreeNode("Failed"))
        Sel = "Select jobid,transdt,acct,speccnt,numoftries,statusdesc from autofax where hseq=0 and status = 'FAIL'"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader
        Do While rs.Read
            Data = rs(0) & "^" & CLSLIMS.CDT(rs(1), "V") & "^" & rs(2) & "^" & rs(3) & "^" & rs(4) & "^" & rs(5)
            AddtoTree(1, Data)
        Loop
        rs.Close()
        tvInfo.Nodes(0).Nodes.Add(New TreeNode("Completed since " & Mid(TranDt, 5, 2) & "/" & Mid(TranDt, 7, 2) & "/" & Mid(TranDt, 1, 4)))
        Sel = "Select jobid,transdt,senddt,acct,speccnt,xmittime,pagecnt,actualcnt,statusdesc from autofax where hseq=0 and status in ('COMP','FLCM') and substring(transdt,1,8)>'" & TranDt & "' order by Senddt desc"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader
        Do While rs.Read
            Data = rs(0) & "^" & rs(2) & "^" & rs(3) & "^" & CLSLIMS.GetAcctName(rs(3)) & "^" & rs(4) & "^" & rs(5) & "^" & rs(6) & "^" & rs(7) & "^" & rs(8)
            AddtoTree(2, Data)
        Loop
        rs.Close()
        ConnMySql.Close()
        tvInfo.Nodes(0).Expand()
    End Sub
    Public Sub AddtoTree(ByVal ndx As Integer, ByVal data As String)
        Dim str As String = ""
        For i = 1 To CLSLIMS.NumberofPieces(data, "^")
            str = str & CLSLIMS.LJ(CLSLIMS.Piece(data, "^", i), Len(CLSLIMS.Piece(data, "^", i)) + 4)
        Next i
        tvInfo.Nodes(0).Nodes(ndx).Nodes.Add(New TreeNode(str))

        AddSpecNodes(CLSLIMS.Piece(data, "^", 1), ndx)

    End Sub
    Public Sub AddSpecNodes(ByVal jobid As String, ByVal ndx As Integer)
        Dim nc As Integer = tvInfo.Nodes(0).Nodes(ndx).GetNodeCount(False) - 1
        If nc < 0 Then Exit Sub
        Dim ConnSql As New SqlConnection(CLSLIMS.strCon)
        ConnSql.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = ConnSql
        Dim rs As SqlDataReader
        Cmd.CommandText = "Select distinct pspecno from prpt where rpdevice = '" & jobid & "'"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            tvInfo.Nodes(0).Nodes(ndx).Nodes(nc).Nodes.Add(CLSLIMS.LJ(rs(0), 15) & CLSLIMS.GetPatName(rs(0)))
        Loop
        rs.Close()
        ConnSql.Close()
    End Sub

    Private Sub dtpCompDt_TextChanged(sender As Object, e As EventArgs) Handles dtpCompDt.TextChanged
        NewDate = True
    End Sub

    Private Sub btnMove_Click(sender As Object, e As EventArgs) Handles btnMove.Click
        Dim DoUpd As Boolean = False, HSEQ As Integer = 99
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Dim rs As MySqlDataReader
        Cmd.CommandText = "Select jobid from autofax where jobid='" & txtJobID.Text & "' And status='FAIL' And HSEQ=0"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            DoUpd = True
        End If
        rs.Close()
        If DoUpd Then
            Cmd.CommandText = "Select MAX(HSEQ)+1 From AUTOFAX Where JOBID='" & txtJobID.Text & "'"
            rs = Cmd.ExecuteReader
            If rs.Read Then
                HSEQ = rs(0)
            End If
            rs.Close()
            Cmd.CommandText = "Update AUTOFAX Set HSEQ=" & HSEQ & " Where HSEQ=0 And JOBID='" & txtJobID.Text & "'"
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "Insert into AUTOFAX Select JOBID,0,'" & CLSLIMS.CDT() & "',ACCT,status,SENDDT,COMPDT,XMITTIME,SPECCNT,PAGECNT,ACTUALCNT,NUMOFTRIES,STATUSDESC From AUTOFAX Where JOBID='" & txtJobID.Text & "' And HSEQ=" & HSEQ
            Cmd.ExecuteNonQuery()
            Cmd.CommandText = "Update Autofax set status='FLCM',transdt='" & CLSLIMS.CDT & "' where jobid='" & txtJobID.Text & "' And status='FAIL' And HSEQ=0"
            Cmd.ExecuteNonQuery()
            txtJobID.Text = ""
            Upd()
        Else
            MsgBox(txtJobID.Text & " not in FAILED status or invalid Job ID.")
        End If
        ConnMySql.Close()
    End Sub
End Class