﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class frmAddDrugDiag
    Private Sub btnRpt_Click(sender As Object, e As EventArgs) Handles btnRpt.Click
        Call BuildRpt()

        'PrintDoc.DefaultPageSettings.Margins.Left = 10
        'PrintDoc.DefaultPageSettings.Margins.Right = 10
        'PrintDoc.DefaultPageSettings.Margins.Top = 10
        'PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        'PrintDoc.DefaultPageSettings.Landscape = False

        'PrintDoc.Print()
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\Compass\DrugDiag" & Format(Now, "yyMMdd") & ".csv", False)
        file.WriteLine("Specimen number,Patient Name,Acct,Acct Name,Physician,SalesRepID,SalesRepName,Diagnosis,Medications,Receive Date")
        For i = 0 To lstData.Items.Count - 1
            file.WriteLine(lstData.Items(i))
        Next i
        file.Close()

    End Sub
    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Public Sub BuildRpt()
        Dim Sel As String, p As String, c As Integer = 1, SO As String = "", TC As String = ""
        Sel = "select pspecno from pomed a, pspec c " &
              "where substring(a.poefdt,1,6)='201711' and med in ('Buprenorphine','Suboxone','Subutex','Methadone') " &
              "and a.pord not in (select b.pord from podiag b where a.pord=b.pord and a.poefdt=b.poefdt " &
              "and diag in ('F11.20','F11.10')) " &
              "and a.pord=c.pord and a.poefdt=c.poefdt and psact=1 "
        lstData.Items.Clear()
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Dim x As String = ""
        Do While rs.Read()
            x = GetPatInfo(rs(0)) 'Specimen number^Last Name^First Name^Acct^Acct Name^physician^SalesRepID^SalesRepName^Diag1,Diag2,Diag3^med1,med2,med3^recvdate
            p = CLSLIMS.Piece(x, "^", 1) & "," & Chr(34) & CLSLIMS.Piece(x, "^", 2) & ", " & CLSLIMS.Piece(x, "^", 3) & Chr(34) & "," & CLSLIMS.Piece(x, "^", 4) & "," & Chr(34) & CLSLIMS.Piece(x, "^", 5) & Chr(34) & "," & Chr(34) & CLSLIMS.Piece(x, "^", 6) & Chr(34) & "," & CLSLIMS.Piece(x, "^", 7) & "," & CLSLIMS.Piece(x, "^", 8) & "," & Chr(34) & CLSLIMS.Piece(x, "^", 9) & Chr(34) & "," & Chr(34) & CLSLIMS.Piece(x, "^", 10) & Chr(34) & "," & CLSLIMS.Piece(x, "^", 11)
            lstData.Items.Add(p) : lstData.SelectedIndex = lstData.Items.Count - 1
            c = c + 1
        Loop
        rs.Close()
        Conn.Close()
        Conn.Dispose()
    End Sub
    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 28

        Static PageNo As Integer = 1
        Dim y As Integer, PrevSOLAB As String = "", SOLAB As String = ""
        'e.Graphics.DrawString(cmbSO.Text, Font2, Brushes.Black, 330, 50)
        'e.Graphics.DrawString("AEL", Font2, Brushes.Black, 330, 50)
        'e.Graphics.DrawString("Send Out Specimens" & IIf(cmbSO.Text = "Lab Corp", " - Lab Corp Account #: 41005270", "") & IIf(cmbSO.Text = "AEL", " - AEL Account #: CLSTN", ""), Font3, Brushes.Black, 150, 100)
        'e.Graphics.DrawString("Send Out Specimens", Font2, Brushes.Black, 250, 100)
        'e.Graphics.DrawString("For " & DateTimePicker1.Text, Font3, Brushes.Black, 25, 158)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 690, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        e.Graphics.DrawString("                                             Date of        Collection   SendOut", Font, Brushes.Black, 25, 185)
        e.Graphics.DrawString("Specimen #     Patient Name                   Birth     Sex     Date       Lab         Tests", Font, Brushes.Black, 25, 200)

        e.Graphics.DrawLine(Pens.Black, 25, 180, 1075, 180)   'Horizonal line above headings
        e.Graphics.DrawLine(Pens.Black, 25, 220, 1075, 220)   'Horizonal line under headings

        e.Graphics.DrawLine(Pens.Black, 108, 180, 108, 1080)   'Veritcal line 2
        e.Graphics.DrawLine(Pens.Black, 315, 180, 315, 1080)   'Veritcal line 3
        e.Graphics.DrawLine(Pens.Black, 405, 180, 405, 1080)   'Veritcal line 4
        e.Graphics.DrawLine(Pens.Black, 435, 180, 435, 1080)      'Vertical line 1
        e.Graphics.DrawLine(Pens.Black, 520, 180, 520, 1080)   'Veritcal line 5
        e.Graphics.DrawLine(Pens.Black, 590, 180, 590, 1080)   'Veritcal line 6


        Dim c As Integer
        Static intStart As Integer

        c = 1
        y = 225
        For i = intStart To lstData.Items.Count - 1
            'data line
            e.Graphics.DrawString(lstData.Items(i).ToString, Font, Brushes.Black, 1, y)
            e.Graphics.DrawLine(Pens.Black, 25, y + 20, 1075, y + 20) 'Horizonal line after sample
            y = y + 30
            c = c + 1
            If c > PageLngth Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i
    End Sub
    Public Function GetPatInfo(ByVal specno As String) As String
        'Specimen number^Last Name^First Name^Acct^Acct Name^physician^SalesRepID^SalesRepName^Diag1,Diag2,Diag3^med1,med2,med3^recvdate
        GetPatInfo = ""
        Dim r As String = "^^^^^^^^^^^", PUID As String = "", PEFDT As String = "", adsq1 As String = "", ACCT As String = "", PHY As String = ""
        Dim PORD As String = "", MEDS As String = "", DIAGS As String = "", POEFDT As String = "", RCVDT As String = ""
        Dim Sel = "select pspecno,acct,reqphy,a.puid,a.pefdt,adsq1,substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4),a.PORD,a.POEFDT from pspec a, pord b where a.pord + a.poefdt = b.pord + b.poefdt and psact=1 and poact=1 and pspecno='" & specno & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            r = CLSLIMS.ReplacePiece(r, "^", 1, rs(0))
            r = CLSLIMS.ReplacePiece(r, "^", 4, rs(1))
            ACCT = rs(1)
            PHY = rs(2)
            PUID = rs(3)
            PEFDT = rs(4)
            adsq1 = rs(5)
            r = CLSLIMS.ReplacePiece(r, "^", 11, rs(6))
            PORD = rs(7)
            POEFDT = rs(8)
        End If
        rs.Close()
        Cmd.CommandText = "Select plname,pfname from pdem where puid='" & PUID & "' and pefdt='" & PEFDT & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            r = CLSLIMS.ReplacePiece(r, "^", 2, rs(0))
            r = CLSLIMS.ReplacePiece(r, "^", 3, rs(1))
        End If
        rs.Close()
        Cmd.CommandText = "Select aname,asalesrg,syslistopt from acct a, syslist b where acct='" & ACCT & "' and asalesrg=syslistval and aact=1 and syslistkey='ASALESRG'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            r = CLSLIMS.ReplacePiece(r, "^", 5, rs(0))
            r = CLSLIMS.ReplacePiece(r, "^", 7, rs(1))
            r = CLSLIMS.ReplacePiece(r, "^", 8, rs(2))
        End If
        rs.Close()
        Cmd.CommandText = "Select phylname+','+phyfname from doctor where PHY='" & PHY & "' and phyact=1"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            r = CLSLIMS.ReplacePiece(r, "^", 6, rs(0))
        End If
        rs.Close()
        Cmd.CommandText = "Select MED From POMED Where PORD='" & PORD & "' And POEFDT='" & POEFDT & "'"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            MEDS = MEDS & rs(0) & ","
        Loop
        rs.Close()
        If MEDS <> "" Then r = CLSLIMS.ReplacePiece(r, "^", 10, Mid(MEDS, 1, Len(MEDS) - 1))
        Cmd.CommandText = "Select DIAG From PODIAG Where PORD='" & PORD & "' And POEFDT='" & POEFDT & "'"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            DIAGS = DIAGS & rs(0) & ","
        Loop
        rs.Close()
        If DIAGS <> "" Then r = CLSLIMS.ReplacePiece(r, "^", 9, Mid(DIAGS, 1, Len(DIAGS) - 1))
        GetPatInfo = r
    End Function
End Class