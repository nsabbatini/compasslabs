﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Specialized

Public Class frmHist
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        If txtFldr.Text = "" Then
            MsgBox("No folder.")
            Exit Sub
        End If
        Dim Drugs As NameValueCollection
        Drugs = New NameValueCollection
        Dim spec As String = "", speclist As String = "", r As String = "", tmpspec As String = "", cnt As Integer = 0, NofP As Integer = 0
        Dim lv As ListViewItem = Nothing
        Dim SQL As String = "Select pspecno from xtrays where folder='" & txtFldr.Text & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(SQL, Conn)
        Dim Cmd2 As New SqlCommand
        Cmd2.Connection = Conn

        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Dim rs2 As SqlDataReader
        Do While rs.Read
            spec = rs(0)
            spec = "1382564"
            r = GetDemo(spec)
            Cmd2.CommandText = "Select top 5 pspecno from pspec where psact=1 and puid in (select puid from pdem where plname='" & CLSLIMS.Piece(r, "^", 1) & "' and pfname='" & CLSLIMS.Piece(r, "^", 2) & "' and pdob='" & CLSLIMS.Piece(r, "^", 3) & "') Order by psrcvdt desc"
            rs2 = Cmd2.ExecuteReader
            Do While rs2.Read
                speclist = speclist & "'" & rs2(0) & "',"
            Loop
            rs2.Close()
            speclist = Mid(speclist, 1, Len(speclist) - 1)
            Cmd2.CommandText = "select distinct xdgdesc,substring(m.rc,1,5) " &
                "from pres m,xdrug where pract=1 and xdgcqn=substring(m.rc,1,5)+'1' " &
                "and ((m.rc like '%4' and m.prslt='Inconsistent') or (m.rc like '%3' and m.prslt='Positive')) " &
                " And substring(m.rc, 1, 1) ='3' and m.pspecno in (" & speclist & ") " &
                "union " &
                "Select distinct xdgdesc,substring(m.rc,1,6) " &
                "from pres m,xdrug where pract=1 and xdgcqn=substring(m.rc,1,6)+'1' " &
                "And ((m.rc Like '%4' and m.prslt='Inconsistent') or (m.rc like '%3' and m.prslt='Positive')) " &
                "and substring(m.rc,1,2)='S3' and m.pspecno in (" & speclist & ")"
            rs2 = Cmd2.ExecuteReader
            Do While rs2.Read
                '         rc-1   rc-1           desc
                Drugs.Add(rs2(1), rs2(1) & "^" & rs2(0) & "^^^^^^^^^^^^^")
            Loop
            rs2.Close()
            For i = 1 To CLSLIMS.NumberofPieces(speclist, ",")
                tmpspec = CLSLIMS.Piece(speclist, ",")
                Cmd2.CommandText = "Select m.pspecno,m.prslt,substring(m.rc,1,5), " &
               "(select a.prslt from pres a where pspecno=m.pspecno And pract=1 And a.rc=substring(m.rc,1,5)+'4'), " &
               "(select a.prslt from pres a where pspecno=m.pspecno And pract=1 And a.rc=substring(m.rc,1,5)+'3'), " &
               "(select a.prslt from pres a where pspecno=m.pspecno And pract=1 And a.rc=substring(m.rc,1,5)+'1') " &
               "From pres m, xdrug Where pract = 1 And xdgcqn = substring(m.rc, 1, 5) +'1' " &
               "And ((m.rc Like '%4' and m.prslt='Inconsistent') or (m.rc like '%3' and m.prslt='Positive')) " &
               "And substring(m.rc, 1, 1) ='3' and m.pspecno=" & tmpspec & " " &
               "union " &
               "select m.pspecno,m.prslt,substring(m.rc,1,6), " &
               "(select a.prslt from pres a where pspecno=m.pspecno And pract=1 And a.rc=substring(m.rc,1,6)+'4'), " &
               "(select a.prslt from pres a where pspecno=m.pspecno And pract=1 And a.rc=substring(m.rc,1,6)+'3'), " &
              "(select a.prslt from pres a where pspecno=m.pspecno And pract=1 And a.rc=substring(m.rc,1,6)+'1') " &
               "From pres m,xdrug where pract=1 and xdgcqn=substring(m.rc,1,6)+'1' " &
               "and ((m.rc like '%4' and m.prslt='Inconsistent') or (m.rc like '%3' and m.prslt='Positive')) " &
               "and SUBSTRING(m.rc,1,2)='S3' and m.pspecno=" & tmpspec & " " &
               "order by m.pspecno"
                rs2 = Cmd2.ExecuteReader
                Do While rs2.Read
                    Drugs(rs2(2)) = CLSLIMS.ReplacePiece(Drugs(rs2(2)), "^", i + 2, Mid(rs2(3), 1, 3) & "-" & Mid(rs2(4), 1, 3) & "-" & rs2(5))
                Loop
                rs2.Close()
            Next i
            speclist = Replace(speclist, "'", "") : NofP = CLSLIMS.NumberofPieces(speclist, ",")
            lv = lvSamp.Items.Add("Specimen #:")
            For i = 1 To NofP
                lv.SubItems.Add(CLSLIMS.Piece(speclist, ",", i))
            Next i
            For Each drg In Drugs
                lv = lvSamp.Items.Add(CLSLIMS.Piece(Drugs(drg), "^", 2))
                For i = 1 To NofP
                    lv.SubItems.Add(CLSLIMS.Piece(speclist, ",", i + 2))
                Next i
            Next drg
            speclist = "" : spec = ""
            Exit Do
        Loop
        rs.Close()
        Conn.Close()

    End Sub
    Public Function GetDemo(ByVal spec As String) As String
        GetDemo = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand("select plname,pfname,pdob from pdem a, pspec b where a.puid=b.puid and a.pefdt=b.pefdt and psact=1 and pspecno='" & spec & "'", Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetDemo = rs(0) & "^" & rs(1) & "^" & rs(2)
        End If
        rs.Close()
        Conn.Close()
    End Function
End Class