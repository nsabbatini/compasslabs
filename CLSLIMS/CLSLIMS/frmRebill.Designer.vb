﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRebill
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSpecNum = New System.Windows.Forms.TextBox()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.btnRebill = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(162, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Specimen Number:"
        '
        'txtSpecNum
        '
        Me.txtSpecNum.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpecNum.Location = New System.Drawing.Point(193, 46)
        Me.txtSpecNum.Name = "txtSpecNum"
        Me.txtSpecNum.Size = New System.Drawing.Size(172, 27)
        Me.txtSpecNum.TabIndex = 1
        '
        'lstInfo
        '
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.FormattingEnabled = True
        Me.lstInfo.ItemHeight = 16
        Me.lstInfo.Location = New System.Drawing.Point(28, 91)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(522, 196)
        Me.lstInfo.TabIndex = 2
        '
        'btnRebill
        '
        Me.btnRebill.Enabled = False
        Me.btnRebill.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRebill.Location = New System.Drawing.Point(349, 300)
        Me.btnRebill.Name = "btnRebill"
        Me.btnRebill.Size = New System.Drawing.Size(91, 28)
        Me.btnRebill.TabIndex = 3
        Me.btnRebill.Text = "Rebill"
        Me.btnRebill.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(459, 300)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(91, 28)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'frmRebill
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(573, 340)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRebill)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.txtSpecNum)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmRebill"
        Me.Text = "Rebill Sample"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtSpecNum As TextBox
    Friend WithEvents lstInfo As ListBox
    Friend WithEvents btnRebill As Button
    Friend WithEvents btnExit As Button
End Class
