﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.ComponentModel
Public Class frmABGene
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Public Sub ReloadCombo()
        lstNonEff.Items.Clear()
        lstEff.Items.Clear()
        cmbAB.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select Antibiotic from ANTIBIOTIC Where Active=1 Order by Antibiotic"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cmbAB.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()

    End Sub

    Private Sub frmABEff_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ReloadCombo()
    End Sub

    Private Sub cmbAB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAB.SelectedIndexChanged
        LoadListBoxes()
    End Sub
    Public Sub LoadListBoxes()
        lstEff.Items.Clear()
        lstNonEff.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select OGABBR from CROSSREF a, MOLECULAR b Where a.ANTIBIOTIC='" & cmbAB.Text & "' And SEQ=0 And a.TYPE='GENE' And a.OGNAME=b.OGNAME Order by OGABBR"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            lstEff.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select OGABBR from MOLECULAR Where TYPE='GENE' And OGNAME Not in (Select OGNAME From CROSSREF Where ANTIBIOTIC='" & cmbAB.Text & "' And SEQ=0 And TYPE='GENE')"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            lstNonEff.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()

    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        ReloadCombo()
    End Sub

    Public Sub MoveRightToLeft()
        If lstNonEff.SelectedItem = "" Then
            Exit Sub
        End If
        lstEff.Items.Add(lstNonEff.SelectedItem)
        lstNonEff.Items.Remove(lstNonEff.SelectedItem)
    End Sub
    Public Sub MoveLeftToRight()
        If lstEff.SelectedItem = "" Then
            Exit Sub
        End If
        lstNonEff.Items.Add(lstEff.SelectedItem)
        lstEff.Items.Remove(lstEff.SelectedItem)
    End Sub

    Private Sub btnLeft_Click(sender As Object, e As EventArgs) Handles btnLeft.Click
        MoveRightToLeft()
    End Sub

    Private Sub btnRight_Click(sender As Object, e As EventArgs) Handles btnRight.Click
        MoveLeftToRight()
    End Sub

    Private Sub lstEff_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lstEff.MouseDoubleClick
        MoveLeftToRight()
    End Sub

    Private Sub lstNonEff_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lstNonEff.MouseDoubleClick
        MoveRightToLeft()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim NxtSeq As Integer = 999999, OGName As String = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select MAX(SEQ)+1 from CROSSREF Where ANTIBIOTIC='" & cmbAB.Text & "' And TYPE='GENE'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            NxtSeq = IIf(IsDBNull(rs(0)), 1, rs(0))
        End If
        rs.Close()
        Cmd.CommandText = "Update CROSSREF Set SEQ=" & NxtSeq & " Where ANTIBIOTIC='" & cmbAB.Text & "' And SEQ=0 And TYPE='GENE'"
        Cmd.ExecuteNonQuery()
        For i = 0 To lstEff.Items.Count - 1
            OGName = GetName(lstEff.Items(i))
            Cmd.CommandText = "Insert into CROSSREF Values ('" & cmbAB.Text & "','" & OGName & "','GENE',0)"
            Cmd.ExecuteNonQuery()
        Next i
        ConnMySql.Close()
        ReloadCombo()
    End Sub
    Public Function GetName(ByVal gene As String) As String
        GetName = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select OGNAME from MOLECULAR Where OGABBR='" & gene & "' And TYPE='GENE'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetName = rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function
End Class