﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class QuickOE
    Public OrderNum As String = ""

    Private Sub QuickOE_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Dim OC As String
        If e.KeyCode = Keys.F10 Then
            For i = 0 To lbOC.Items.Count - 1
                OC = Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1))
                If Mid(OC, 1, 1) = "7" Or Mid(OC, 1, 1) = "4" Then
                    OC = "6" & Mid(OC, 2)
                    If GetOCName(OC) = "" Then
                        MsgBox("Ordering Code not found.")
                    Else
                        lbOC.Items(i) = OC & " - " & GetOCAbbr(OC)
                    End If
                End If
            Next i
        ElseIf e.KeyCode = Keys.F9 Then
            For i = 0 To lbOC.Items.Count - 1
                OC = Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1))
                If Mid(OC, 1, 1) = "7" Or Mid(OC, 1, 1) = "6" Then
                    OC = "4" & Mid(OC, 2)
                    If GetOCName(OC) = "" Then
                        MsgBox("Ordering Code not found.")
                    Else
                        lbOC.Items(i) = OC & " - " & GetOCAbbr(OC)
                    End If
                End If
            Next i
        ElseIf e.KeyCode = Keys.F11 Then
            For i = 0 To lbOC.Items.Count - 1
                OC = Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1))
                If Mid(OC, 1, 1) = "4" Or Mid(OC, 1, 1) = "6" Then
                    OC = "7" & Mid(OC, 2)
                    If GetOCName(OC) = "" Then
                        MsgBox("Ordering Code not found.")
                    Else
                        lbOC.Items(i) = OC & " - " & GetOCAbbr(OC)
                    End If
                End If
            Next i
        ElseIf e.KeyCode = Keys.F4 Then
            lbOC.Items.Clear()
        ElseIf e.KeyCode = Keys.F5 Then
            lbOC.Items.Add("800000 - XREF")
        ElseIf e.KeyCode = Keys.F1 Then
            frmIntCom.ShowDialog()
            If CLSLIMS.IntText <> "" Then
                lblIntCom.Text = "*"
            Else
                lblIntCom.Text = ""
            End If
        ElseIf e.KeyCode = Keys.F2 Then
            If txtAcct.Text = "180" Then
                lbOC.Items.Clear()
                lbOC.Items.Add("709100 - P9CD")
                lbOC.Items.Add("800000 - XREF")
            End If
        End If
    End Sub

    Private Sub QuickOE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMsg.Text = ""
        lblSelItem.Text = ""
        CLSLIMS.IntText = ""
        'If UCase(System.Net.Dns.GetHostName) = "COMPASS-NEDPC" Then CLSLIMS.UID = "NED.SABBATINI" : CLSLIMS.UNAME = "Sabbatini, Ned"
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        lblUID.Text = CLSLIMS.UNAME
        lblF1.Text = " F1" & vbCrLf & "Cmts"
        'lblF2.Text = "      F2" & vbCrLf & "180DirBill"
        lblF4.Text = "F4" & vbCrLf & "Del"
        lblF5.Text = "  F5" & vbCrLf & "XREF"
        lblF6.Text = "    F6" & vbCrLf & "Search"
        lblF9.Text = "F9" & vbCrLf & " 4"
        lblF10.Text = "F10" & vbCrLf & " 6"
        lblF11.Text = "F11" & vbCrLf & " 7"
    End Sub


    Private Sub txtTray_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTray.KeyPress
        Dim sel As String, NewSeq As String, i As Integer
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 9 Then
            Exit Sub
        End If
        If txtTray.Text = "" Then
            Exit Sub
        End If
        lbOC.Items.Clear()
        txtOC.Text = ""
        i = 1
        If UCase(txtTray.Text) = "N" Then
            'New tray number for the day TR01-071014, TR02-071014
            sel = "Select TRAY From XTRAYS Where TRAY Like 'TR%-" & Format(Now, "MMddyy") & "' And substring(TRAY,3,2) < '20' Order By TRAY Desc"
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                NewSeq = Format(Val(Mid(rs(0), 3, 2)) + 1, "00")
                txtTray.Text = "TR" & NewSeq & "-" & Format(Now, "MMddyy")
            Else
                txtTray.Text = "TR01-" & Format(Now, "MMddyy")
            End If
            rs.Close()
            'Insert holder spot in XTRAYS so others don't get same tray #
            Cmd.CommandText = "Insert Into XTRAYS (TRAY,PSPECNO,UID) Values ('" & txtTray.Text & "','HOLDER','" & CLSLIMS.UID & "')"
            Cmd.ExecuteNonQuery()
            Cmd.Dispose()
            Conn.Close()
            txtTray.Enabled = False
            txtReqNum.Enabled = True
            txtAcct.Enabled = True
            txtOC.Enabled = True
            txtStatusCode.Enabled = True
            btnSave.Enabled = True
            btnLblSave.Enabled = True
            btnPrtLbl.Enabled = True
            Button1.Enabled = True
            btnGrid.Enabled = True
            'To skip check at end for "Tray not found"
            i = 2
            PrintTrayLabel(txtTray.Text)
            txtReqNum.Focus()
        Else
            'Pull up existing tray
            sel = "Select TRAY,PSPECNO,ACCT,OCS From XTRAYS Where TRAY ='" & txtTray.Text & "' And PSPECNO != 'HOLDER' Order By TRAYx"
            sel = "Select TRAY,PSPECNO,ACCT,OCS,TRAYx From XTRAYS Where TRAY ='" & txtTray.Text & "' Order By TRAYx"
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                If rs(1) <> "HOLDER" Then lbSamples.Items.Add(CLSLIMS.RJ(rs(4), 3) & " - " & CLSLIMS.LJ(rs(1), 10) & CLSLIMS.LJ(rs(2), 8) & GetAcctName(rs(2))) : lbSamples.SelectedIndex = lbSamples.Items.Count - 1
                i = i + 1
            Loop
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            txtReqNum.Focus()
            txtTray.Enabled = False
            txtReqNum.Enabled = True
            txtAcct.Enabled = True
            txtOC.Enabled = True
            txtStatusCode.Enabled = True
            btnSave.Enabled = True
            btnLblSave.Enabled = True
            btnPrtLbl.Enabled = True
            Button1.Enabled = True
            btnGrid.Enabled = True
            txtReqNum.Focus()
        End If
        If i = 1 Then
            lblMsg.ForeColor = Color.Red
            lblMsg.Text = "Tray not found.  Enter N for new."
            txtTray.Text = ""
            txtTray.Focus()
        End If

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        If lbSamples.Items.Count > 0 Then
            Dim res As Integer
            res = MsgBox("Print Tray?", MsgBoxStyle.OkCancel)
            If res = DialogResult.OK Then
                Dim printer As New PCPrint()
                'Set the font we want to use
                printer.PrinterFont = New Font("Courier New", 10)
                'Set Margins
                Dim mar As New Printing.Margins(50, 50, 40, 50)
                printer.DefaultPageSettings.Margins = mar

                Dim txt As String = "TRAY: " & txtTray.Text & vbCrLf & vbCrLf
                'Print lbSamples
                For i = 0 To lbSamples.Items.Count - 1
                    txt = txt & lbSamples.Items(i) & vbCrLf
                Next i
                txt = txt & vbCrLf & vbCrLf & "                                         Complete Date: ________________" & vbCrLf & vbCrLf
                txt = txt & "                                         Complete Time: ________________" & vbCrLf
                printer.TextToPrint = " " & txt
                'Issue print command
                printer.Print()
            End If
        End If
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub txtReqNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtReqNum.KeyPress
        Dim sel As String, checkReq As String, strFN As String = ""
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtReqNum.Text = "" Then
            Exit Sub
        End If
        'Check for leading/trailing spaces
        txtReqNum.Text = Trim(txtReqNum.Text)
        'Uppercase all letters
        txtReqNum.Text = UCase(txtReqNum.Text)
        'Check for leading zero
        If Mid(txtReqNum.Text, 1, 1) = "0" Then
            txtReqNum.Text = Mid(txtReqNum.Text, 2)
        End If
        'Check for pre-Orders
        OrderNum = ""
        If CheckOrders(txtReqNum.Text) = "" Then
            If Not IsNumeric(Mid(txtReqNum.Text, 1, 1)) And Mid(txtReqNum.Text, 1, 1) <> "W" Then
                checkReq = Mid(txtReqNum.Text, 2)
            Else
                checkReq = txtReqNum.Text
            End If
            'Check for all numeric
            'If Not IsNumeric(checkReq) Then
            '    MsgBox("Only numeric allowed.")
            '    txtReqNum.Text = ""
            '    txtReqNum.Focus()
            '    Exit Sub
            'End If
            'Check for account number entry
            If Len(checkReq) < 6 Then
                lblMsg.Text = "Account number entered. New specimen generated."
                txtAcct.Text = txtReqNum.Text
                txtReqNum.Text = FindLastSpecNo(checkReq)
                txtOC.Focus()
            End If
            'Check to see if Specimen Number in use
            If CheckSpecNo(txtReqNum.Text) Then
                lblMsg.ForeColor = Color.Red
                lblMsg.Text = txtReqNum.Text & " Specimen number already exists."
                txtReqNum.Text = ""
                txtReqNum.Focus()
                Exit Sub
            End If
            'Check for Duplicates in this tray
            For i = 0 To lbSamples.Items.Count - 1
                If txtReqNum.Text = Mid(lbSamples.Items(i), 6, Len(txtReqNum.Text)) Then
                    lblMsg.ForeColor = Color.Red
                    lblMsg.Text = txtReqNum.Text & " duplicate requisition number." & vbCrLf & "Position: " & (i + 1)
                    txtReqNum.Text = ""
                    txtReqNum.Focus()
                    Exit Sub
                End If
            Next
            'Check for Req num flashnote
            strFN = FlashNoteReqNum(txtReqNum.Text)
            If strFN <> "" Then
                MsgBox(strFN)
            End If
            '
            'txtAcct.Text = ""
            lbOC.Items.Clear()
            txtOC.Text = ""
            sel = "Select ACCT From YREQL Where REQ='" & txtReqNum.Text & "'"
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                txtAcct.Text = rs(0)
                lblMsg.ForeColor = Color.Black
                lblMsg.Text = txtAcct.Text & "-" & GetAcctName(txtAcct.Text)
                'if ACCT name contains HOLD then account on HOLD
                If InStr(UCase(lblMsg.Text), "HOLD") > 0 And InStr(UCase(lblMsg.Text), "ACCOUNT") > 0 Then
                    lblMsg.ForeColor = Color.Red
                    lblMsg.Text = "Account on Hold."
                    txtAcct.Text = ""
                    txtAcct.Focus()
                    Exit Sub
                End If
            Else
                lblMsg.ForeColor = Color.DarkSlateGray
                lblMsg.Text = "Requisition number not on file. Enter account."
                txtAcct.Focus()
            End If
            rs.Close()

            If txtAcct.Text <> "" Then
                ProcessAcct()
            End If

            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        ElseIf CheckOrders(txtreqnum.Text) = "HL7" Then  'Have pre-order info
            OrderNum = txtReqNum.Text
            Dim DupSpecNum As String = AlreadyOrderNum(OrderNum)
            If DupSpecNum <> "" Then
                MsgBox("Duplicate Order Key.  Order key used on specimen(s) " & DupSpecNum)
                Exit Sub
            End If
            Dim Acct As String = GetOrderAcct(OrderNum)
            txtReqNum.Text = FindLastSpecNo(Acct)
            txtAcct.Text = "C" & Acct
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim OBRoc As String = ""
            Dim selx As String = "Select Message From XORDERS Where OrderKey='" & OrderNum & "' And MSGTYPE='OBR' And LoadDt=(Select Max(LoadDt) From XORDERS Where OrderKey='" & OrderNum & "')"
            Dim Cmd As New SqlCommand(selx, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read
                OBRoc = CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "^", 1), "|", 5)
                If GetOCAbbr(OBRoc) <> "" Then
                    lbOC.Items.Add(OBRoc)
                Else
                    lblMsg.Text = lblMsg.Text & "Invalid OC: " & OBRoc
                End If
            Loop
            rs.Close()
            selx = "Select Message From XORDERS Where OrderKey='" & OrderNum & "' And MSGTYPE='PID' And LoadDt=(Select Max(LoadDt) From XORDERS Where OrderKey='" & OrderNum & "')"
            Cmd.CommandText = selx
            rs = Cmd.ExecuteReader
            If rs.Read Then
                Label6.Text = CLSLIMS.Piece(rs(0), "|", 6)
            End If
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
            lblMsg.Text = lblMsg.Text & " Pre-Order"
        ElseIf CheckOrders(txtReqNum.Text) = "REQ" Then  'Have pre-order info
            OrderNum = txtReqNum.Text
            Dim DupSpecNum As String = AlreadyOrderNum(OrderNum)
            If DupSpecNum <> "" Then
                MsgBox("Duplicate Order Key.  Order key used on specimen(s) " & DupSpecNum)
                Exit Sub
            End If
            If CheckSpecNo(txtReqNum.Text) Then
                MsgBox("Duplicate Specimen number.")
                Exit Sub
            End If
            Dim info As String = "", Tsts As String = "", OC As String = "", TstType As String = ""            'Tests
            info = GetReqInfo("Tests", OrderNum)
            If CLSLIMS.Piece(info, "^", 1) = "YES" Then
                info = GetReqInfo("PatInfo", OrderNum)
                txtAcct.Text = CLSLIMS.Piece(info, "^", 1)
                ProcessAcct()
                lblPatInfo.Text = CLSLIMS.Piece(info, "^", 5) & ", " & CLSLIMS.Piece(info, "^", 6) & "  DOB: " & CLSLIMS.Piece(info, "^", 14)
            ElseIf CLSLIMS.Piece(info, "^", 1) = "NO" Then
                TstType = CLSLIMS.Piece(info, "^", 3)
                Tsts = CLSLIMS.Piece(info, "^", 4)
                For i = 1 To CLSLIMS.NumberofPieces(Tsts, ";")
                    OC = CLSLIMS.Piece(Tsts, ";", i)
                    If TstType = "SCRN" Then
                        Select Case OC
                            'Screen specific
                            Case "AlcBio" : lbOC.Items.Add("351000" & " - " & GetOCAbbr("351000"))
                            Case "Amp" : lbOC.Items.Add("353000" & " - " & GetOCAbbr("353000"))
                            Case "Barb" : lbOC.Items.Add("360000" & " - " & GetOCAbbr("360000"))
                            Case "Benz" : lbOC.Items.Add("362000" & " - " & GetOCAbbr("362000"))
                            Case "Bup" : lbOC.Items.Add("364000" & " - " & GetOCAbbr("364000"))
                            Case "THC" : lbOC.Items.Add("366000" & " - " & GetOCAbbr("366000"))
                            Case "Coc" : lbOC.Items.Add("368000" & " - " & GetOCAbbr("368000"))
                            Case "Her" : lbOC.Items.Add("372000" & " - " & GetOCAbbr("372000"))
                            Case "Meth" : lbOC.Items.Add("374000" & " - " & GetOCAbbr("374000"))
                            Case "MethylAmp" : lbOC.Items.Add("376000" & " - " & GetOCAbbr("376000"))
                            Case "Nic" : lbOC.Items.Add("379000" & " - " & GetOCAbbr("379000"))
                            Case "Opi" : lbOC.Items.Add("381000" & " - " & GetOCAbbr("381000"))
                            Case "Oxy" : lbOC.Items.Add("384000" & " - " & GetOCAbbr("384000"))
                            Case "Phen" : lbOC.Items.Add("386000" & " - " & GetOCAbbr("386000"))
                            Case "Prop" : lbOC.Items.Add("389000" & " - " & GetOCAbbr("389000"))
                            Case "Skel" : lbOC.Items.Add("392000" & " - " & GetOCAbbr("392000"))
                             'Conf either way
                            Case "Alk" : lbOC.Items.Add("395000" & " - " & GetOCAbbr("395000"))
                            Case "Ster" : lbOC.Items.Add("309010" & " - " & GetOCAbbr("309010"))
                            Case "AntiSero" : lbOC.Items.Add("354000" & " - " & GetOCAbbr("354000"))
                            Case "AntiTri" : lbOC.Items.Add("355000" & " - " & GetOCAbbr("355000"))
                            Case "Anti" : lbOC.Items.Add("356000" & " - " & GetOCAbbr("356000"))
                            Case "AntiEp" : lbOC.Items.Add("357000" & " - " & GetOCAbbr("357000"))
                            Case "AntiPsy" : lbOC.Items.Add("358000" & " - " & GetOCAbbr("358000"))
                            Case "Fent" : lbOC.Items.Add("369000" & " - " & GetOCAbbr("369000"))
                            Case "Gaba" : lbOC.Items.Add("370000" & " - " & GetOCAbbr("370000"))
                            Case "MethylPhen" : lbOC.Items.Add("377000" & " - " & GetOCAbbr("377000"))
                            Case "OpiAnal" : lbOC.Items.Add("382000" & " - " & GetOCAbbr("382000"))
                            Case "Pregab" : lbOC.Items.Add("387000" & " - " & GetOCAbbr("387000"))
                            Case "Sed" : lbOC.Items.Add("390000" & " - " & GetOCAbbr("390000"))
                            Case "Tap" : lbOC.Items.Add("393000" & " - " & GetOCAbbr("393000"))
                            Case "Tram" : lbOC.Items.Add("394000" & " - " & GetOCAbbr("394000"))
                            Case "Levor" : lbOC.Items.Add("308130" & " - " & GetOCAbbr("308130"))
                        End Select
                    ElseIf TstType = "CONF" Then
                        Select Case OC
                            Case "AlcBio" : lbOC.Items.Add("350000" & " - " & GetOCAbbr("350000"))
                            Case "Alk" : lbOC.Items.Add("395000" & " - " & GetOCAbbr("395000"))
                            Case "Amp" : lbOC.Items.Add("352000" & " - " & GetOCAbbr("352000"))
                            Case "Ster" : lbOC.Items.Add("309010" & " - " & GetOCAbbr("309010"))
                            Case "AntiSero" : lbOC.Items.Add("354000" & " - " & GetOCAbbr("354000"))
                            Case "AntiTri" : lbOC.Items.Add("355000" & " - " & GetOCAbbr("355000"))
                            Case "Anti" : lbOC.Items.Add("356000" & " - " & GetOCAbbr("356000"))
                            Case "AntiEp" : lbOC.Items.Add("357000" & " - " & GetOCAbbr("357000"))
                            Case "AntiPsy" : lbOC.Items.Add("358000" & " - " & GetOCAbbr("358000"))
                            Case "Barb" : lbOC.Items.Add("359000" & " - " & GetOCAbbr("359000"))
                            Case "Benz" : lbOC.Items.Add("361000" & " - " & GetOCAbbr("361000"))
                            Case "Bup" : lbOC.Items.Add("363000" & " - " & GetOCAbbr("363000"))
                            Case "THC" : lbOC.Items.Add("365000" & " - " & GetOCAbbr("365000"))
                            Case "Coc" : lbOC.Items.Add("367000" & " - " & GetOCAbbr("367000"))
                            Case "Fent" : lbOC.Items.Add("369000" & " - " & GetOCAbbr("369000"))
                            Case "Gaba" : lbOC.Items.Add("370000" & " - " & GetOCAbbr("370000"))
                            Case "Her" : lbOC.Items.Add("371000" & " - " & GetOCAbbr("371000"))
                            Case "Meth" : lbOC.Items.Add("373000" & " - " & GetOCAbbr("373000"))
                            Case "MethylAmp" : lbOC.Items.Add("375000" & " - " & GetOCAbbr("375000"))
                            Case "MethylPhen" : lbOC.Items.Add("377000" & " - " & GetOCAbbr("377000"))
                            Case "Nic" : lbOC.Items.Add("378000" & " - " & GetOCAbbr("378000"))
                            Case "Opi" : lbOC.Items.Add("380000" & " - " & GetOCAbbr("380000"))
                            Case "OpiAnal" : lbOC.Items.Add("382000" & " - " & GetOCAbbr("382000"))
                            Case "Oxy" : lbOC.Items.Add("383000" & " - " & GetOCAbbr("383000"))
                            Case "Phen" : lbOC.Items.Add("385000" & " - " & GetOCAbbr("385000"))
                            Case "Pregab" : lbOC.Items.Add("387000" & " - " & GetOCAbbr("387000"))
                            Case "Prop" : lbOC.Items.Add("388000" & " - " & GetOCAbbr("388000"))
                            Case "Sed" : lbOC.Items.Add("390000" & " - " & GetOCAbbr("390000"))
                            Case "Skel" : lbOC.Items.Add("391000" & " - " & GetOCAbbr("31000"))
                            Case "Tap" : lbOC.Items.Add("393000" & " - " & GetOCAbbr("393000"))
                            Case "Tram" : lbOC.Items.Add("394000" & " - " & GetOCAbbr("394000"))
                            Case "Levor" : lbOC.Items.Add("308130" & " - " & GetOCAbbr("308130"))
                            Case "SAlk" : lbOC.Items.Add("S395000" & " - " & GetOCAbbr("S395000"))
                            Case "SAmp" : lbOC.Items.Add("S352000" & " - " & GetOCAbbr("S352000"))
                            Case "SAntiSero" : lbOC.Items.Add("S354000" & " - " & GetOCAbbr("S354000"))
                            Case "SAntiTri" : lbOC.Items.Add("S355000" & " - " & GetOCAbbr("S355000"))
                            Case "SAnti" : lbOC.Items.Add("S356000" & " - " & GetOCAbbr("S356000"))
                            Case "SAntiEp" : lbOC.Items.Add("S357000" & " - " & GetOCAbbr("S560000"))
                            Case "SAntiPsy" : lbOC.Items.Add("S358000" & " - " & GetOCAbbr("S580000"))
                            Case "SBenz" : lbOC.Items.Add("S361000" & " - " & GetOCAbbr("S361000"))
                            Case "SBup" : lbOC.Items.Add("S363000" & " - " & GetOCAbbr("S363000"))
                            Case "STHC" : lbOC.Items.Add("S365000" & " - " & GetOCAbbr("S365000"))
                            Case "SCoc" : lbOC.Items.Add("S367000" & " - " & GetOCAbbr("S367000"))
                            Case "SFent" : lbOC.Items.Add("S369000" & " - " & GetOCAbbr("S369000"))
                            Case "SGaba" : lbOC.Items.Add("S370000" & " - " & GetOCAbbr("S370000"))
                            Case "SHer" : lbOC.Items.Add("S371000" & " - " & GetOCAbbr("S371000"))
                            Case "SMeth" : lbOC.Items.Add("S373000" & " - " & GetOCAbbr("S373000"))
                            Case "SMethylAmp" : lbOC.Items.Add("S375000" & " - " & GetOCAbbr("S375000"))
                            Case "SMethylPhen" : lbOC.Items.Add("S377000" & " - " & GetOCAbbr("S377000"))
                            Case "SNic" : lbOC.Items.Add("S378000" & " - " & GetOCAbbr("S378000"))
                            Case "SOpi" : lbOC.Items.Add("S380000" & " - " & GetOCAbbr("S380000"))
                            Case "SOpiAnal" : lbOC.Items.Add("S382000" & " - " & GetOCAbbr("S382000"))
                            Case "SOxy" : lbOC.Items.Add("S383000" & " - " & GetOCAbbr("S383000"))
                            Case "SPhen" : lbOC.Items.Add("S385000" & " - " & GetOCAbbr("S385000"))
                            Case "SPregab" : lbOC.Items.Add("S387000" & " - " & GetOCAbbr("S387000"))
                            Case "SProp" : lbOC.Items.Add("S388000" & " - " & GetOCAbbr("S388000"))
                            Case "SSed" : lbOC.Items.Add("S390000" & " - " & GetOCAbbr("S390000"))
                            Case "SSkel" : lbOC.Items.Add("S391000" & " - " & GetOCAbbr("S391000"))
                            Case "STap" : lbOC.Items.Add("S393000" & " - " & GetOCAbbr("S393000"))
                            Case "STram" : lbOC.Items.Add("S394000" & " - " & GetOCAbbr("S394000"))
                        End Select
                    End If
                Next i
                'XREF
                info = GetReqInfo("MedDx", OrderNum)
                If CLSLIMS.Piece(info, "^", 1) = "YES" Then lbOC.Items.Add("800000")
                info = GetReqInfo("PatInfo", OrderNum)
                txtAcct.Text = CLSLIMS.Piece(info, "^", 1)
            End If

            lblMsg.Text = lblMsg.Text & " Pre-Order"
        End If
    End Sub
    Public Function FlashNoteReqNum(ByVal ReqNum As String) As String
        FlashNoteReqNum = ""
        Dim xSel As String = "Select MC, FREETXT From XFN Where PSPECNO='" & ReqNum & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                If cRS(1) <> "" Then
                    FlashNoteReqNum = cRS(1)
                ElseIf cRS(0) <> "" Then
                    FlashNoteReqNum = CLSLIMS.GetMC(cRS(0))
                End If
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Public Function FlashNoteAcct(ByVal acct As String) As String
        FlashNoteAcct = ""
        Dim xSel As String = "Select MC,FREETXT From XFN Where ACCT='" & acct & "' And FNACT=1"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                If cRS(1) <> "" Then
                    FlashNoteAcct = cRS(1)
                ElseIf cRS(0) <> "" Then
                    FlashNoteAcct = CLSLIMS.GetMC(cRS(0))
                End If
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Function CheckOrders(ByVal OrderNum As String) As String
        'Returns HL7 or REQ
        Dim xSel As String = "Select count(*) From XORDERS Where OrderKey='" & OrderNum & "' and MSGTYPE='MSH'"
        CheckOrders = ""
        If OrderNum <> "" Then
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                Conn.Open()
                Dim Cmd As New SqlCommand(xSel, Conn)
                Dim cRS As SqlDataReader = Cmd.ExecuteReader
                If cRS.Read Then
                    If cRS(0) > 0 Then
                        CheckOrders = "HL7"
                    End If
                End If
                cRS.Close()
                xSel = "Select count(*) From XORDERS Where OrderKey='" & OrderNum & "' and MSGTYPE='PatInfo'"
                Cmd.CommandText = xSel
                cRS = Cmd.ExecuteReader
                If cRS.Read Then
                    If cRS(0) > 0 Then
                        CheckOrders = "REQ"
                    End If
                End If
                cRS.Close()
                Cmd.Dispose()
            End Using
        End If
    End Function
    Public Function AlreadyOrderNum(ByVal OK As String) As String
        AlreadyOrderNum = ""
        Dim xSel As String = "Select pspecno From PSPEC Where PSACT=1 And PSID3='" & OK & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                AlreadyOrderNum = AlreadyOrderNum & cRS(0) & ","
            Loop
            cRS.Close()
            Cmd.Dispose()
        End Using
        If AlreadyOrderNum <> "" Then If Mid(AlreadyOrderNum, Len(AlreadyOrderNum), 1) = "," Then AlreadyOrderNum = Mid(AlreadyOrderNum, 1, Len(AlreadyOrderNum) - 1)
    End Function
    Public Function GetOrderAcct(ByVal OrderNum As String) As String
        GetOrderAcct = ""
        Dim xSel As String = "Select message from xorders where orderkey='" & OrderNum & "' and msgtype like '%MSH' And LoadDt=(Select Max(LoadDt) From XORDERS Where OrderKey='" & OrderNum & "')"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(xSel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetOrderAcct = CLSLIMS.Piece(rs(0), "|", 4)
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Dispose()
        If GetOrderAcct = "SMALAB" Then
            GetOrderAcct = "C902"
        ElseIf Not IsNumeric(Mid(GetOrderAcct, 1, 1)) Then
            GetOrderAcct = Mid(GetOrderAcct, 2)
        End If
    End Function
    Public Function GetReqInfo(ByVal RecType As String, ByVal OrderNum As String) As String
        GetReqInfo = ""
        Dim xSel As String = "Select message from xorders where orderkey='" & OrderNum & "' and msgtype = '" & RecType & "' And LoadDt=(Select Max(LoadDt) From XORDERS Where OrderKey='" & OrderNum & "')"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(xSel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetReqInfo = rs(0)
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Dispose()
    End Function
    Public Function FindLastSpecNo(ByVal acct As String) As String
        'Dim NewSpecId As String = acct & "000001"
        'Dim Length As Integer = Len(NewSpecId)
        ''Dim xSel As String = "Select Max(pspecno) from pspec where psact=1 And Len(pspecno)=" & Length & " And pspecno between '" & acct & "000000" & "' And '" & acct & "999999" & "'"
        'Dim xSel As String = "Select Max(pspecno) from pspec where Len(pspecno)=" & Length & " And pspecno between '" & acct & "000000" & "' And '" & acct & "999999" & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        'Conn.Open()
        'Dim Cmd As New SqlCommand(xSel, Conn)
        'Dim rs As SqlDataReader = Cmd.ExecuteReader
        'If rs.Read Then
        '    If Not IsDBNull(rs(0)) Then
        '        FindLastSpecNo = CStr(Val(rs(0)) + 1)
        '    Else
        '        FindLastSpecNo = NewSpecId
        '    End If
        'Else
        '    FindLastSpecNo = NewSpecId
        'End If
        'rs.Close()
        'Cmd.Dispose()
        'Conn.Dispose()
        FindLastSpecNo = ""
        Conn.Open()
        Dim pCmd As New SqlCommand("dbo.xTNCL_NEXTVAL", Conn)
        pCmd.CommandType = CommandType.StoredProcedure
        pCmd.Parameters.Add("SYSVALKEY", SqlDbType.VarChar).Value = acct
        pCmd.Parameters.Add("SYSVALVAL", SqlDbType.Float).Value = 0
        pCmd.Parameters("SYSVALVAL").Direction = ParameterDirection.Output
        Try
            pCmd.ExecuteNonQuery()
        Catch
            MsgBox("x")
        End Try
        FindLastSpecNo = CInt(pCmd.Parameters("SYSVALVAL").Value)
        pCmd.Dispose()
        'FindLastSpecNo = CLSLIMS.RJ(FindLastSpecNo, 6, "0")
        FindLastSpecNo = acct & FindLastSpecNo.PadLeft(6, "0")
        Conn.Close()

    End Function
    Private Function GetSCName(ByVal SC As String) As String
        Dim xSel As String = "Select stcddesc From statcd Where stcdACT=1 And Statcd='" & SC & "'"
        GetSCName = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetSCName = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Function GetAcctName(ByVal acct As String) As String
        Dim xSel As String = "Select ANAME From ACCT Where AACT=1 And ACCT='" & acct & "'"
        GetAcctName = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Private Function GetOCName(ByVal oc As String) As String
        Dim xSel As String = "Select ONAME From OC Where OACT=1 And OC='" & oc & "'"
        GetOCName = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetOCName = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Public Function CheckSpecNo(ByVal specno As String) As Boolean
        Dim xSel As String = "Select PSPECNO From PSPEC Where PSACT=1 And PSPECNO='" & specno & "'"
        CheckSpecNo = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                CheckSpecNo = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Public Function GetOCAbbr(ByVal oc As String) As String
        Dim xSel As String = "Select OABRV From OC Where OACT=1 And OC='" & oc & "'"
        GetOCAbbr = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetOCAbbr = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Function GetOCefdt(ByVal oc As String) As String
        Dim xSel As String = "Select OEFDT From OC Where OACT=1 And OC='" & oc & "'"
        GetOCefdt = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetOCefdt = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Private Function GetWL(ByVal tc As String) As String
        Dim xSel As String = "Select WL, WEFDT From TC Where TACT=1 And TC='" & tc & "'"
        GetWL = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetWL = cRS(0) & "^" & cRS(1)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Private Function GetAcctefdt(ByVal acct As String) As String
        Dim xSel As String = "Select AEFDT From ACCT Where AACT=1 And ACCT='" & acct & "'"
        GetAcctefdt = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctefdt = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Function GetTCefdt(ByVal tc As String) As String
        Dim xSel As String = "Select TEFDT From TC Where TACT=1 And TC='" & tc & "'"
        GetTCefdt = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetTCefdt = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Sub txtStatusCode_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtStatusCode.KeyUp
        If e.KeyCode = Keys.F6 Then
            lstStatusCodeSearch.Height = 338
            lstStatusCodeSearch.Width = 350
            lstStatusCodeSearch.Location = New Drawing.Point(283, 134)
            lstStatusCodeSearch.Visible = True
            txtStatusCode.Text = GetSCFromName(txtStatusCode.Text)
            Exit Sub
        End If
        If e.KeyCode <> Keys.Enter Then
            txtStatusCode.Text = UCase(txtStatusCode.Text)
            Exit Sub
        End If
        If txtStatusCode.Text = "" Then
            Exit Sub
        End If
        ProcessStatCode()
    End Sub
    Private Sub ProcessStatCode()

        'lstStatusCode.Items.Clear()
        'If StatusCode  not on file then GetSCName will be null
        Dim StatCdDesc As String = GetSCName(txtStatusCode.Text)
        If StatCdDesc = "" Then
            lblMsg.Text = ""
            lblMsg.ForeColor = Color.Red
            lblMsg.Text = "Status Code not found."
            txtStatusCode.Text = ""
            txtStatusCode.Focus()
            Exit Sub
        Else
            lblMsg.Text = ""
            lblMsg.ForeColor = Color.Black
            lblMsg.Text = lblMsg.Text & vbCrLf & txtStatusCode.Text & "-" & StatCdDesc
            lstStatusCode.Items.Add(txtStatusCode.Text)
        End If
        txtStatusCode.Text = ""
        txtStatusCode.Focus()
    End Sub
    Private Function GetSCFromName(ByVal StatCode As String) As String

        Dim xSel As String
        xSel = "select statcd,stcddesc " &
               "from statcd " &
               "where upper(statcd+stcddesc) like '" & UCase(StatCode) & "%' and stcdact=1" &
               "order by statcd"

        Dim strSC As String = ""
        GetSCFromName = ""
        'If AcctName = "" Then Exit Function
        lstStatusCodeSearch.Items.Clear()
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strSC = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 50)
                lstStatusCodeSearch.Items.Add(strSC)
            Loop
            If lstStatusCodeSearch.Items.Count > 0 Then
                lstStatusCodeSearch.Visible = True
                lstStatusCodeSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function

    Private Sub txtAcct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.F6 Then
            lstAcctNameSearch.Height = 338
            lstAcctNameSearch.Width = 350
            lstAcctNameSearch.Location = New Drawing.Point(283, 134)
            lstAcctNameSearch.Visible = True
            txtAcct.Text = GetAcctFromName(txtAcct.Text)
            Exit Sub
        End If
        If e.KeyCode <> Keys.Enter Then
            Exit Sub
        End If
        If txtAcct.Text = "" Then
            Exit Sub
        End If
        ProcessAcct()
    End Sub
    Private Sub ProcessAcct()
        Dim sel As String, strFN As String = ""
        'Clear Acodes - ready for defaults
        lbOC.Items.Clear()
        txtOC.Text = ""
        lblMsg.ForeColor = Color.Black
        lblMsg.Text = txtAcct.Text & "-" & GetAcctName(txtAcct.Text)
        'If ACCT not on file then GetAcctName will be null
        If lblMsg.Text = txtAcct.Text & "-" Then
            lblMsg.ForeColor = Color.Red
            lblMsg.Text = "Account not found."
            txtAcct.Text = ""
            txtAcct.Focus()
            Exit Sub
        End If
        'if ACCT name contains HOLD then account on HOLD
        If InStr(UCase(lblMsg.Text), "HOLD") > 0 And InStr(UCase(lblMsg.Text), "ACCOUNT") > 0 Then
            lblMsg.ForeColor = Color.Red
            lblMsg.Text = "Account on Hold."
            txtAcct.Text = ""
            txtAcct.Focus()
            Exit Sub
        End If
        'Check for Acct flashnote
        strFN = FlashNoteAcct(txtAcct.Text)
        If strFN <> "" Then
            MsgBox(strFN)
        End If
        '
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        sel = "Select OC From ACCT, ACCTOC Where ACCT.ACCT='" & txtAcct.Text & "' And ACCT.ACCT=ACCTOC.ACCT And ACCT.AACT=1 and ACCT.AEFDT=ACCTOC.AEFDT and ACCTOC.AAUTOORD=1"
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            lbOC.Items.Add(rs(0) & " - " & GetOCAbbr(rs(0))) ': lbOC.SelectedIndex = lbOC.Items.Count - 1
            lblMsg.Text = lblMsg.Text & vbCrLf & GetOCName(rs(0))
        Loop
        rs.Close()
        If txtAcct.Text = "409" Or txtAcct.Text = "1427" Then
            lbOC.Items.Add("209000" & " - " & GetOCAbbr("209000")) ': lbOC.SelectedIndex = lbOC.Items.Count - 1
        End If

        If lbOC.Items.Count > 0 Then
            btnLblSave.Focus()
        Else
            txtOC.Focus()
        End If

        If txtAcct.Text = "182" Or txtAcct.Text = "409" Or txtAcct.Text = "1427" Then
            txtOC.Focus()
        End If
        'Get Specimen Type for the different tests based on Account number ("S","M",no prefix)
        cmbSpecType.Items.Clear()
        If UCase(Mid(txtAcct.Text, 1, 1)) = "M" Then
            Cmd.CommandText = "Select STYPE From STYPE Where STCLASS='MOLE'"
        ElseIf Ucase(Mid(txtAcct.Text, 1, 1)) = "S" Then
            Cmd.CommandText = "Select STYPE From STYPE Where STCLASS='TOXSAL'"
        Else
            Cmd.CommandText = "Select STYPE From STYPE Where STCLASS='TOXURN'"
        End If
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbSpecType.Items.Add(rs(0))
        Loop
        rs.Close()
        Conn.Close()
        If txtAcct.Text = "PTSAMPLES" Then cmbSpecType.Items.Add("SALIVA")
        If UCase(Mid(txtAcct.Text, 1, 1)) = "M" Then
            cmbSpecType.Text = cmbSpecType.Items(2)
        ElseIf ucase(Mid(txtAcct.Text, 1, 1)) = "S" Then
            cmbSpecType.Text = cmbSpecType.Items(0)
        Else
            cmbSpecType.Text = cmbSpecType.Items(0)
        End If
    End Sub

    Public Function COVIDpos(ByVal i As Integer) As String
        COVIDpos = ""
        If i < 1 Or i > 94 Then
            COVIDpos = "Err"
            Exit Function
        End If
        Dim Row As String = "", Col As Integer = 0
        If i < 11 Then
            Row = "A" : Col = i - 2
        ElseIf i < 23 Then
            Row = "B" : Col = i - 10
        ElseIf i < 35 Then
            Row = "C" : Col = i - 22
        ElseIf i < 47 Then
            Row = "D" : Col = i - 34
        ElseIf i < 59 Then
            Row = "E" : Col = i - 46
        ElseIf i < 71 Then
            Row = "F" : Col = i - 58
        ElseIf i < 83 Then
            Row = "G" : Col = i - 70
        ElseIf i < 95 Then
            Row = "H" : Col = i - 82
        End If
        COVIDpos = Row + Format(Col, "09")
    End Function
    Private Function GetAcctFromName(ByVal AcctName As String) As String

        Dim xSel As String
        xSel = "Select acct,aname " &
               "from acct " &
               "where upper(aname) Like '" & UCase(AcctName) & "%' and aact=1" &
               "order by aname"

        Dim strAcct As String = ""
        GetAcctFromName = ""
        'If AcctName = "" Then Exit Function
        lstAcctNameSearch.Items.Clear()
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strAcct = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 30)
                lstAcctNameSearch.Items.Add(strAcct)
            Loop
            If lstAcctNameSearch.Items.Count > 0 Then
                lstAcctNameSearch.Visible = True
                lstAcctNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Private Sub txtOC_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOC.KeyUp
        If e.KeyCode = Keys.F6 Then
            lstOCNameSearch.Height = 200
            lstOCNameSearch.Width = 350
            lstOCNameSearch.Location = New Drawing.Point(283, 170)
            lstOCNameSearch.Visible = True
            txtOC.Text = GetOCFromName(txtOC.Text)
            Exit Sub
        End If

        If e.KeyCode <> Keys.Enter Then
            Exit Sub
        End If
        If txtOC.Text = "" Then
            Exit Sub
        End If
        ProcessOC()

    End Sub
    Private Sub ProcessOC()
        Dim sel As String
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        sel = "Select OC,OABRV From OC Where OACT=1 And (OC='" & txtOC.Text & "' Or OABRV='" & txtOC.Text & "')"
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            Dim x As String = CheckOCDupes()
            If x <> "" Then
                MsgBox(x, , "Duplicate Test Code found")
                Exit Sub
            End If
            lbOC.Items.Add(rs(0) & " - " & rs(1)) : lbOC.SelectedIndex = lbOC.Items.Count - 1
            If lblMsg.ForeColor = Color.Red Then lblMsg.Text = ""
            lblMsg.ForeColor = Color.Black
            lblMsg.Text = lblMsg.Text & vbCrLf & GetOCName(rs(0))
            txtOC.Text = ""
            txtOC.Focus()
        Else
            lblMsg.ForeColor = Color.Red
            lblMsg.Text = "Ordering Code " & txtOC.Text & " not found."
            txtOC.Text = ""
            txtOC.Focus()
        End If
        rs.Close()
        Conn.Close()
    End Sub
    Private Function GetOCFromName(ByVal OCName As String) As String

        Dim xSel As String
        xSel = "select OC,oname " &
               "from OC " &
               "where upper(oname) like '" & UCase(OCName) & "%' and oact=1" &
               "order by oname"

        Dim strOC As String = ""
        GetOCFromName = ""
        'If OCName = "" Then Exit Function
        lstOCNameSearch.Items.Clear()
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strOC = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 30)
                lstOCNameSearch.Items.Add(strOC)
            Loop
            If lstOCNameSearch.Items.Count > 0 Then
                lstOCNameSearch.Visible = True
                lstOCNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function

    Private Function CheckOCDupes() As String

        CheckOCDupes = ""

        Dim sel As String, arrTC() As String, j As Integer = 1, DupeFound As Boolean = False, OC As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        ReDim Preserve arrTC(j)
        arrTC(1) = ""
        For i = 0 To lbOC.Items.Count - 1
            OC = Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1))
            sel = "select a.oc,a.tc from octc a,oc b where a.oc='" & OC & "' and oact=1 and a.oc=b.oc and a.oefdt=b.oefdt"
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read() And Not DupeFound
                For k = 1 To arrTC.Count - 1
                    If rs(1) = CLSLIMS.Piece(arrTC(k), "^", 1) Then
                        CheckOCDupes = "Ordering Code: " & OC & " Test Code: " & rs(1)
                        DupeFound = True
                        Exit For
                    End If
                Next k
                ReDim Preserve arrTC(j)
                arrTC(j) = rs(1) & "^" & rs(0)
                j = j + 1
            Loop
            rs.Close()
            Cmd.Dispose()
            If DupeFound Then Exit For
        Next i
        If txtOC.Text <> "" Then
            For k = 1 To arrTC.Count - 1
                If txtOC.Text = CLSLIMS.Piece(arrTC(k), "^", 1) Then
                    CheckOCDupes = "Ordering Code: " & CLSLIMS.Piece(arrTC(k), "^", 2) & " Test Code: " & txtOC.Text
                    DupeFound = True
                    Exit For
                End If
            Next k
        End If

    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim x As String = CheckOCDupes()
        If x <> "" Then
            MsgBox(x, , "Duplicate Test Code found")
            Exit Sub
        End If
        SaveSample()

    End Sub

    Private Sub btnLblSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLblSave.Click

        Dim SecondLabel As Boolean = False
        Dim x As String = CheckOCDupes()
        If x <> "" Then
            MsgBox(x, , "Duplicate Test Code found")
            Exit Sub
        End If
        Dim NoToPrt As Integer = 2
        If OrderNum <> "" Then NoToPrt = 2
        'Determine Tray Letter
        Dim TL As String
        Dim sq As Integer = Val(Mid(txtTray.Text, 3, 2))
        TL = Mid("ABCDEFGHJKLMNPQRSTUVWXYZ", sq, 1)
        'Check for ALC-BARB-THC and print extra label    09/19/2018 NS
        For i = 0 To lbOC.Items.Count - 1
            If Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1)) = "350000" Then
                SecondLabel = True
                Exit For
            ElseIf Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1)) = "359000" Then
                SecondLabel = True
                Exit For
            ElseIf Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1)) = "365000" Then
                SecondLabel = True
                Exit For
            End If
        Next i
        For k = 1 To NoToPrt     'Number of labels to print
            PrintSampLabel(txtReqNum.Text, TL & IIf(lbSamples.Items.Count + 1 < 10, "0", "") & lbSamples.Items.Count + 1)
        Next k
        If SecondLabel Then
            PrintSampLabel(txtReqNum.Text, TL & IIf(lbSamples.Items.Count + 1 < 10, "0", "") & lbSamples.Items.Count + 1, True)
            frmGeneric.Text = "Need extra aliquot."
            frmGeneric.lblGeneric.Text = "Need Extra Aliquot."
            frmGeneric.BackColor = Color.Yellow
            frmGeneric.lblGeneric.BackColor = Color.Yellow
            frmGeneric.lblGeneric.ForeColor = Color.Blue
            frmGeneric.tmGeneric.Enabled = True
            frmGeneric.tmGeneric.Interval = 5000
            'frmGeneric.WindowState = WindowState.Maximized
            frmGeneric.ShowDialog()
        End If
        SaveSample()

    End Sub
    Private Sub SaveSample()
        Dim strOCs As String = "", i As Integer, k As Integer = 0
        Dim ComDt As String = "", txtCom As String = "", iSpc As Integer = 0, Cmt(50) As String, strBillTo As String = ""
        If lbOC.Items.Count = 0 Then
            MsgBox("No Ordering codes.")
            Exit Sub
        End If
        If txtAcct.Text = "" Then
            MsgBox("Must have an account.")
            Exit Sub
        Else
            strBillTo = GetAcctBillTo(txtAcct.Text)
        End If
        If txtReqNum.Text = "" Then
            MsgBox("No requisition number.")
            Exit Sub
        End If
        For i = 0 To lbOC.Items.Count - 1
            strOCs = strOCs & Trim(CLSLIMS.Piece(lbOC.Items(i), "-", 1)) & ","
        Next i
        'Add SVT to all Urine
        'If Mid(txtAcct.Text, 1, 1) <> "S" Then
        ' If Not InStr(strOCs, ",209000,") Then
        'strOCs = strOCs & "209000,"
        ' End If
        'End If
        'Remove trailing comma
        strOCs = Mid(strOCs, 1, Len(strOCs) - 1)
        Debug.Print(strOCs)
        'Check to see that Opiates and 6MAM are not ordered together    11/20/2017  NS
        If InStr(strOCs, "372000") Then                                                                  'If 6MAM Sreeen ordered
            If InStr(strOCs, "381000") Then                                                              'If Opiate Screen ordered
                strOCs = Replace(strOCs, "372000", "")                                                   'Remove 6MAM Screen
                strOCs = Replace(strOCs, ",,", ",")                                                      'Remove double commas if 6MAM removed from middle of strOCs
                If Mid(strOCs, 1, 1) = "," Then strOCs = Mid(strOCs, 2)                                  'Remove leading comma if 6MAM removed from front of strOCs
                If Mid(strOCs, Len(strOCs), 1) = "," Then strOCs = Mid(strOCs, 1, Len(strOCs) - 1)       'Remove trailing comma if 6MAM removed from end of strOCs
            End If
        End If
        Debug.Print(strOCs)
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        If lblSelItem.Text = "" Then
            Cmd.CommandText = "Insert Into XTRAYS (TRAY,PSPECNO,ACCT,OCs,UID,TRAYx) Values ('" & txtTray.Text & "','" & txtReqNum.Text & "','" & txtAcct.Text & "','" & strOCs & "','" & CLSLIMS.UID & "'," & lbSamples.Items.Count + 1 & ")"
        Else
            Cmd.CommandText = "Update XTRAYS Set ACCT='" & txtAcct.Text & "',OCs='" & strOCs & "',UID='" & CLSLIMS.UID & "' Where TRAY='" & txtTray.Text & "' And PSPECNO='" & txtReqNum.Text & "'"
        End If
        Cmd.ExecuteNonQuery()

        'Add to lbSamples
        If lblSelItem.Text = "" Then
            lbSamples.Items.Add(CLSLIMS.RJ(lbSamples.Items.Count + 1, 2) & " - " & CLSLIMS.LJ(txtReqNum.Text, 10) & CLSLIMS.LJ(txtAcct.Text, 8) & GetAcctName(txtAcct.Text)) : lbSamples.SelectedIndex = lbSamples.Items.Count - 1
        Else
            lbSamples.Items(lblSelItem.Text) = Mid(lbSamples.Items(lblSelItem.Text).ToString, 1, 2) & " - " & CLSLIMS.LJ(txtReqNum.Text, 10) & CLSLIMS.LJ(txtAcct.Text, 8) & GetAcctName(txtAcct.Text)
        End If
        Dim parOCS As String = ""
        Dim strResponse As String
        For i = 1 To 99
            If CLSLIMS.Piece(strOCs, ",", i) <> "" Then
                parOCS = parOCS & "'" & CLSLIMS.Piece(strOCs, ",", i) & "',"
            Else
                Exit For
            End If
        Next i
        'Remove trailing comma
        parOCS = Mid(parOCS, 1, Len(parOCS) - 1)
        '==========================================================================================================================================================================================================================
        'Save in Apollo
        '==========================================================================================================================================================================================================================
        'Check to see if sample already saved once
        If lblSelItem.Text <> "" Then
            CLSLIMS.ResetSpecNo(txtReqNum.Text)
        End If
        Dim proc As String = "dbo.xTNCL_QO"
        Dim pCmd As New SqlCommand(proc, Conn)
        pCmd.CommandType = CommandType.StoredProcedure
        pCmd.Parameters.Add("PSPECNO", SqlDbType.VarChar).Value = txtReqNum.Text
        pCmd.Parameters.Add("ACCT", SqlDbType.VarChar).Value = txtAcct.Text
        pCmd.Parameters.Add("OCS", SqlDbType.VarChar).Value = strOCs
        pCmd.Parameters.Add("UID", SqlDbType.VarChar).Value = CLSLIMS.UID
        pCmd.Parameters.Add("CDT", SqlDbType.VarChar).Value = CLSLIMS.CDT
        pCmd.Parameters.Add("COLLDT", SqlDbType.VarChar).Value = ""
        pCmd.Parameters.Add("COLLTM", SqlDbType.VarChar).Value = ""
        pCmd.Parameters.Add("BILLTO", SqlDbType.VarChar).Value = strBillTo
        pCmd.Parameters.Add("REQPHY", SqlDbType.VarChar).Value = ""
        pCmd.Parameters.Add("ADSQ1", SqlDbType.Int).Value = 1
        pCmd.Parameters.Add("ADSQ3", SqlDbType.Int).Value = 1
        pCmd.Parameters.Add("SPTYP", SqlDbType.VarChar).Value = cmbSpecType.Text
        pCmd.Parameters.Add("RESPONSE", SqlDbType.VarChar).Value = ""
        pCmd.Parameters.Add("ERRMSG", SqlDbType.VarChar).Value = ""
        pCmd.Parameters("RESPONSE").Direction = ParameterDirection.Output
        pCmd.Parameters("ERRMSG").Direction = ParameterDirection.Output
        pCmd.ExecuteNonQuery()
        strResponse = IIf(IsDBNull(pCmd.Parameters("RESPONSE").Value), "", pCmd.Parameters("RESPONSE").Value)
        pCmd.Dispose()

        '==========================================================================================================================================================================================================================
        'Status Code
        k = 2
        For i = 0 To lstStatusCode.Items.Count - 1
            Cmd.CommandText = "Insert Into PSSTAT Values ('" & txtReqNum.Text & "',1," & k & ",'" & lstStatusCode.Items(i) & "')"
            Cmd.ExecuteNonQuery()
            k = k + 1
        Next i

        '==========================================================================================================================================================================================================================
        If CLSLIMS.IntText <> "" Then
            'Insert any internal comments
            ComDt = CLSLIMS.CDT
            Cmd.CommandText = "insert into PSCMT values ('" & txtReqNum.Text & "','I','" & ComDt & "',1,'" & CLSLIMS.UID & "','QE','')"
            Cmd.ExecuteNonQuery()
            txtCom = Replace(CLSLIMS.IntText, "'", " ")
            i = 1
            Cmt = Split(txtCom, vbCrLf)
            For j = 0 To UBound(Cmt)
                Do While Len(Cmt(j)) > 80
                    iSpc = CLSLIMS.FirstSpace(Mid(Cmt(j), 1, 80))
                    Cmd.CommandText = "Insert Into PSCMTI Values ('" & txtReqNum.Text & "','I','" & ComDt & "'," & i & ",'" & Mid(Cmt(j), 1, iSpc) & "')"
                    Cmd.ExecuteNonQuery()
                    Cmt(j) = Mid(Cmt(j), iSpc + 1)
                    i = i + 1
                Loop
                Cmd.CommandText = "Insert Into PSCMTI Values ('" & txtReqNum.Text & "','I','" & ComDt & "'," & i & ",'" & Cmt(j) & "')"
                Cmd.ExecuteNonQuery()
                i = i + 1
            Next j
        End If
        '==========================================================================================================================================================================================================================
        ' If OrderNum then Update rest of demographics
        '==========================================================================================================================================================================================================================
        If CheckOrders(OrderNum) = "HL7" Then
            SaveOrder(OrderNum, txtReqNum.Text)
        ElseIf CheckOrders(OrderNum) = "REQ" Then
            SaveOrderReq(OrderNum, txtReqNum.Text)
        End If
        '==========================================================================================================================================================================================================================

        'Clear fields
        txtReqNum.Text = ""
        txtAcct.Text = ""
        txtOC.Text = ""
        lbOC.Items.Clear()
        lblMsg.Text = ""
        lblSelItem.Text = ""
        txtReqNum.Focus()
        OrderNum = ""
        lblIntCom.Text = ""
        lstStatusCode.Items.Clear()
        txtStatusCode.Text = ""
        CLSLIMS.IntText = ""
        lblPatInfo.Text = ""
    End Sub

    Public Sub SaveOrder(ByVal ordernum As String, ByVal specno As String)
        Dim ComDt As String = "", txtCom As String = "", iSpc As Integer = 0, Cmt(50) As String, i As Integer
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Dim Msg As String = "", Msg1 As String = "", Msg2 As String = "", First As String = "", Last As String = "", DOB As String = "", Gender As String = "", PUID As String = "", xCmdText As String = "", PEFDT As String = "", Race As String = ""
        Dim CollDate As String = "", CollTime As String = "", PHY As String = "", REQPHY As String = "", Diag(99) As String, ndx As Integer = 1, PORD As String = "", POEFDT As String = "", BillTo As String = "I", PID As String = "", ACCT As String = ""
        'Check PDEM for recurring patient, get PUID
        Cmd.CommandText = "Select MESSAGE From XORDERS Where ORDERKEY='" & ordernum & "' And MSGTYPE='PID'"
        Dim rs As SqlDataReader
        rs = Cmd.ExecuteReader
        If rs.Read Then
            Msg = rs(0)
            First = CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 6), "^", 2)
            Last = CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 6), "^", 1)
            DOB = Mid(CLSLIMS.Piece(Msg, "|", 8), 1, 8)
            Gender = CLSLIMS.Piece(Msg, "|", 9)
            Race = CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 11), "^", 1)
            PID = CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 3), "^", 1)
            If PID = "" Then PID = CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 4), "^", 1)
            If CLSLIMS.Piece(Msg, "|", 5) = "P" Then BillTo = "$"
            If Mid(ordernum, 1, 1) = "C" Then BillTo = "C"
            Select Case Race
                Case "AMERICAN INDIAN", "ALASKAN NATIVE" : Race = "AM"
                Case "ASIAN" : Race = "A"
                Case "BLACK", "AFRICAN AMERICAN" : Race = "AA"
                Case "OTHER PACIFIC ISLANDER" : Race = "O"
                Case "UNREPORTED/REFUSED TO REPORT" : Race = "NS"
                Case "WHITE" : Race = "C"
                Case Else : Race = "O"
            End Select
        End If
        rs.Close()
        'PDEM
        Cmd.CommandText = "Select PUID, PEFDT From PDEM Where PACT=1 And Upper(PFNAME)='" & UCase(First) & "' And Upper(PLNAME)='" & Last & "' And PDOB='" & DOB & "' And Upper(SEX)='" & UCase(Gender) & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            PUID = rs(0)
            PEFDT = rs(1)
            xCmdText = "Update PDEM SET PID1='" & PID & "', PAD1='" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 1) & "', PAD2='" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 2) & "',PCITY='" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 3) &
                "',PSTATE='" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 4) & "',PZIP='" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 5) & "',PPHONE='" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 14), "^", 1) & "',PID2='" & CLSLIMS.Piece(Msg, "|", 20) & "',PRACE='" & Race & "',PBILLTO='" & BillTo & "' Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "' And PACT=1"
        Else
            PUID = GetNextPUID()
            PEFDT = CLSLIMS.CDT
            'Insert/Create New PUID/PDEM
            xCmdText = "Insert into PDEM (PUID, PACT, PEFDT, PFNAME, PMNAME, PLNAME, PDOB, SEX, PRACE, PID1, PAD1, PAD2, PCITY, PSTATE, PZIP, PPHONE, PID2, PBILLTO) Values ('" & PUID & "',1,'" & PEFDT & "','" & First & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "^", 3), "|", 6) &
                  "','" & Last & "','" & DOB & "','" & Gender & "','" & Race & "','" & PID & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 1) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 2) &
                  "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 3) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 4) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 12), "^", 5) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg, "|", 14), "^", 1) &
                  "','" & CLSLIMS.Piece(Msg, "|", 20) & "','" & BillTo & "')"
        End If
        rs.Close()
        Debug.Print(xCmdText)
        Cmd.CommandText = xCmdText
        Cmd.ExecuteNonQuery()
        'PSPEC
        '==================================
        Cmd.CommandText = "Select MESSAGE From XORDERS Where ORDERKEY='" & ordernum & "' And MSGTYPE='OBR'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            CollDate = Mid(CLSLIMS.Piece(rs(0), "|", 8), 1, 8)
            CollTime = Mid(CLSLIMS.Piece(rs(0), "|", 8), 9, 4)
            If CollDate = "" Then
                CollDate = Mid(CLSLIMS.Piece(rs(0), "|", 7), 1, 8)
                CollTime = Mid(CLSLIMS.Piece(rs(0), "|", 7), 9, 4)
            End If
            REQPHY = CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 17), "^", 1)
        End If
        rs.Close()
        Cmd.CommandText = "Update PSPEC Set PSTAT='2',PUID='" & PUID & "', PEFDT='" & PEFDT & "',PSCDTA='" & CollDate & "', PSCTMA='" & CollTime & "', PSID3='" & ordernum & "' Where PSPECNO='" & specno & "' And PSACT=1"
        Cmd.ExecuteNonQuery()
        'PORD
        '===================================
        If REQPHY <> "" Then
            Cmd.CommandText = "Select PHY From DOCTOR Where PHYACT=1 And PHYNPI='" & REQPHY & "'"
            rs = Cmd.ExecuteReader
            If rs.Read Then
                REQPHY = rs(0)
            End If
            rs.Close()
        End If
        Cmd.CommandText = "Select PORD, POEFDT,ACCT From PORD Where PORD+POEFDT=(Select PORD+POEFDT From PSPEC Where PSPECNO='" & specno & "' And PSACT=1) And POACT=1"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            PORD = rs(0)
            POEFDT = rs(1)
            ACCT = rs(2)
        End If
        rs.Close()
        BillTo = GetAcctBillTo(ACCT)
        Cmd.CommandText = "Update PORD Set REQPHY='" & REQPHY & "', PUID='" & PUID & "', PEFDT='" & PEFDT & "', POBILLTO='" & BillTo & "' Where PORD='" & PORD & "' And POEFDT='" & POEFDT & "' And POACT=1"
        Cmd.ExecuteNonQuery()
        'Guarantor
        If CheckForMsg(ordernum, "GT1") Then

        End If
        'Diagnosis
        '=============================================
        If CheckForMsg(ordernum, "DG1") Then
            ndx = 1
            Cmd.CommandText = "Select MESSAGE From XORDERS Where ORDERKEY='" & ordernum & "' And MSGTYPE='DG1' Order By MSGSEQ"
            rs = Cmd.ExecuteReader
            Do While rs.Read
                If Replace(CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 5), "^", 3), "-", "") = "ICD10" Then
                    Diag(ndx) = CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 5), "^", 1)
                ElseIf Replace(CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 4), "^", 3), "-", "") = "ICD10" Then
                    Diag(ndx) = CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 4), "^", 1)
                ElseIf Replace(CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 4), "^", 3), "-", "") = "I10" Then
                    Diag(ndx) = CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 4), "^", 1)
                ElseIf Replace(CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 3), "^", 1), "-", "") = "I10" Then
                    Diag(ndx) = CLSLIMS.Piece(CLSLIMS.Piece(rs(0), "|", 4), "^", 1)
                End If
                ndx = ndx + 1
            Loop
            rs.Close()
            For i = 1 To ndx - 1
                Cmd.CommandText = "Insert Into PODIAG Values ('" & PORD & "','" & POEFDT & "'," & i & ",'" & Diag(i) & "','')"
                Cmd.ExecuteNonQuery()
            Next i
        End If
        'Comments
        '=========================================
        If CheckForMsg(ordernum, "NTE") Then
            Cmd.CommandText = "Select MESSAGE From XORDERS Where ORDERKEY='" & ordernum & "' And MSGTYPE='NTE' Order By MSGSEQ"
            rs = Cmd.ExecuteReader
            CLSLIMS.IntText = ""
            Do While rs.Read
                CLSLIMS.IntText = CLSLIMS.IntText & CLSLIMS.Piece(rs(0), "|", 4) & vbCrLf
            Loop
            rs.Close()
            If CLSLIMS.IntText <> "" Then
                'Insert any internal comments
                ComDt = CLSLIMS.CDT
                Cmd.CommandText = "insert into PSCMT values ('" & specno & "','I','" & ComDt & "',1,'" & CLSLIMS.UID & "','QE','')"
                Cmd.ExecuteNonQuery()
                txtCom = Replace(CLSLIMS.IntText, "'", " ")
                i = 1
                Cmt = Split(txtCom, vbCrLf)
                For j = 0 To UBound(Cmt)
                    Do While Len(Cmt(j)) > 80
                        iSpc = CLSLIMS.FirstSpace(Mid(Cmt(j), 1, 80))
                        Cmd.CommandText = "Insert Into PSCMTI Values ('" & specno & "','I','" & ComDt & "'," & i & ",'" & Mid(Cmt(j), 1, iSpc) & "')"
                        Cmd.ExecuteNonQuery()
                        Cmt(j) = Mid(Cmt(j), iSpc + 1)
                        i = i + 1
                    Loop
                    Cmd.CommandText = "Insert Into PSCMTI Values ('" & specno & "','I','" & ComDt & "'," & i & ",'" & Cmt(j) & "')"
                    Cmd.ExecuteNonQuery()
                    i = i + 1
                Next j
            End If
        End If
        'Insurance
        '=========================================
        'IN1|1||AARP HEALTH|AARP HEALTH CARE OPTIONS|PO BOX 740819^^ATLANTA^GA^30374^USA||800-523-5800||||||||OTHER|TEST^INSURANCE^|SE|19590218|2134 Ruthwood Dr.^^IRVING^TX^75039^|||||||||||||||||45648||||||TEST,INSURANCE|M
        If CheckForMsg(ordernum, "IN1") Then
            Dim bins As String = "", binsefdt As String = "", Rel As String = "", Name As String = "", GrpNum As String = "", SSN As String = "", IDOB As String = "", IFirst As String = "", ILast As String = ""
            Dim IAddr As String = "", Pol As String = "", InsertStr(5) As String, DelStr(5) As String, binsinfo As String = "'", RecNo As String = "", PRecNo As String = ""
            i = 1
            Cmd.CommandText = "Select msgtype,message From XORDERS Where ORDERKEY='" & ordernum & "' And MSGTYPE in ('IN1','IN2') Order By MSGSEQ"
            rs = Cmd.ExecuteReader
            Do While rs.Read
                Msg = rs(1)
                If rs(0) = "IN1" Then
                    Msg1 = Msg
                    RecNo = CLSLIMS.Piece(Msg1, "|", 2)
                    If PRecNo <> "" Then
                        If RecNo <> PRecNo Then
                            If UCase(bins) = "MEDICARE" Or UCase(bins) = "MEDI" Or UCase(bins) = "MC" Then
                                InsertStr(i) = "Insert Into PIMC Values ('" & PUID & "','" & PEFDT & "',0,1,'" & CLSLIMS.Piece(CLSLIMS.Piece(Msg1, "|", 8), "^", 1) & "','','')"
                                DelStr(i) = "Delete From PIMC Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
                            ElseIf InStr(UCase(bins), "MEDICAID") > 0 Then
                                InsertStr(i) = "Insert Into PIMD Values ('" & PUID & "','" & PEFDT & "',0,1,'" & CLSLIMS.Piece(IAddr, "^", 4) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg1, "|", 8), "^", 1) & "','','','')"
                                DelStr(i) = "Delete From PIMD Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
                            Else
                                InsertStr(i) = "Insert Into PIPI Values ('" & PUID & "','" & PEFDT & "'," & (i - 1) & "," & i & ",'" & bins & "','" & binsefdt &
                                    "','" & Pol & "','" & GrpNum & "','" & Rel & "','" & SSN & "','" & IFirst & "','','" & ILast & "','" & IDOB & "','" &
                                    CLSLIMS.Piece(IAddr, "^", 1) & "','" & CLSLIMS.Piece(IAddr, "^", 2) & "','" & CLSLIMS.Piece(IAddr, "^", 3) & "','','" &
                                    CLSLIMS.Piece(IAddr, "^", 4) & "','','" & CLSLIMS.Piece(IAddr, "^", 5) & "','','','','')"
                                DelStr(i) = "Delete From PIPI Where PIPIx=" & (i - 1) & " And PUID='" & PUID & "' And PEFDT='" & PEFDT & "' And BINS='" & bins & "' And BINSEFDT='" & binsefdt & "'"
                            End If
                            i = i + 1
                            bins = "" : binsinfo = "" : binsefdt = "" : Rel = "" : Name = "" : GrpNum = "" : IDOB = "" : IAddr = "" : Pol = "" : SSN = ""
                            PRecNo = RecNo
                        End If
                    End If
                    PRecNo = RecNo
                    bins = CLSLIMS.Piece(Msg1, "|", 4) : If bins = "" Then bins = CLSLIMS.Piece(Msg1, "|", 5)
                    binsinfo = CLSLIMS.Piece(Msg1, "|", 5) & "~" & CLSLIMS.Piece(Msg1, "|", 6) & "~" & CLSLIMS.Piece(Msg1, "|", 8)
                    If Len(bins) > 15 Then bins = Mid(bins, 1, 12) & "-" & CLSLIMS.Piece(CLSLIMS.Piece(binsinfo, "~", 2), "^", 4) 'if too long 12 + "-ST"
                    If UCase(bins) <> "MEDICARE" And UCase(bins) <> "MEDI" And InStr(UCase(bins), "MEDICAID") = 0 Then
                        binsefdt = GetBINSEffDate(bins, binsinfo)
                    End If
                    Rel = RelLookUp(CLSLIMS.Piece(Msg1, "|", 18))
                    Name = CLSLIMS.Piece(Msg1, "|", 17) : IFirst = CLSLIMS.Piece(Name, "^", 2) : ILast = CLSLIMS.Piece(Name, "^", 1)
                    GrpNum = CLSLIMS.Piece(Msg1, "|", 9)
                    IDOB = Mid(CLSLIMS.Piece(Msg1, "|", 19), 1, 8)
                    IAddr = CLSLIMS.Piece(Msg, "|", 20)
                    Pol = CLSLIMS.Piece(Msg1, "|", 37)
                End If
                If rs(0) = "IN2" Then
                    Msg2 = Msg
                    SSN = CLSLIMS.Piece(Msg2, "|", 3)
                End If
            Loop
            rs.Close()
            If UCase(bins) = "MEDICARE" Or UCase(bins) = "MEDI" Or UCase(bins) = "MC" Then
                InsertStr(i) = "Insert Into PIMC Values ('" & PUID & "','" & PEFDT & "',0,1,'" & CLSLIMS.Piece(CLSLIMS.Piece(Msg1, "|", 8), "^", 1) & "','','')"
                DelStr(i) = "Delete From PIMC Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
            ElseIf InStr(UCase(bins), "MEDICAID") > 0 Then
                InsertStr(i) = "Insert Into PIMD Values ('" & PUID & "','" & PEFDT & "',0,1,'" & CLSLIMS.Piece(IAddr, "^", 4) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(Msg1, "|", 8), "^", 1) & "','','','')"
                DelStr(i) = "Delete From PIMD Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
            Else
                InsertStr(i) = "Insert Into PIPI Values ('" & PUID & "','" & PEFDT & "'," & (i - 1) & "," & i & ",'" & bins & "','" & binsefdt &
                    "','" & Pol & "','" & GrpNum & "','" & Rel & "','" & SSN & "','" & IFirst & "','','" & ILast & "','" & IDOB & "','" &
                    CLSLIMS.Piece(IAddr, "^", 1) & "','" & CLSLIMS.Piece(IAddr, "^", 2) & "','" & CLSLIMS.Piece(IAddr, "^", 3) & "','','" &
                    CLSLIMS.Piece(IAddr, "^", 4) & "','','" & CLSLIMS.Piece(IAddr, "^", 5) & "','','','','')"
                DelStr(i) = "Delete From PIPI Where PIPIx=" & (i - 1) & " And PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"      ' And BINS='" & bins & "' And BINSEFDT='" & binsefdt & "'"
            End If
            For j = 1 To 5
                If InsertStr(j) <> "" Then
                    Cmd.CommandText = DelStr(j)
                    Cmd.ExecuteNonQuery()
                    Cmd.CommandText = InsertStr(j)
                    Cmd.ExecuteNonQuery()
                End If
            Next j
        End If

        Cmd.Dispose()
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub SaveOrderReq(ByVal ordernum As String, ByVal specno As String)
        Dim ComDt As String = "", txtCom As String = "", iSpc As Integer = 0, Cmt(50) As String, i As Integer
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Dim rs As SqlDataReader
        Dim Info As String = "", Msg1 As String = "", Msg2 As String = "", First As String = "", Last As String = "", DOB As String = "", Gender As String = "", PUID As String = "", xCmdText As String = "", PEFDT As String = "", Race As String = "", MI As String = ""
        Dim CollDate As String = "", CollTime As String = "", PHY As String = "", REQPHY As String = "", Diag(99) As String, ndx As Integer = 1, PORD As String = "", POEFDT As String = "", BillTo As String = "I", PID As String = "", ACCT As String = ""
        Dim Addr1 As String = "", Addr2 As String = "", City As String = "", ST As String = "", ZIP As String = "", PPhone As String = "", Proc As String = ""
        'PatInfo
        Info = GetReqInfo("PatInfo", ordernum)
        txtAcct.Text = CLSLIMS.Piece(Info, "^", 1)
        PHY = CLSLIMS.Piece(Info, "^", 2) : Proc = CLSLIMS.Piece(Info, "^", 3) : PUID = CLSLIMS.Piece(Info, "^", 4) : Last = CLSLIMS.Piece(Info, "^", 5) : First = CLSLIMS.Piece(Info, "^", 6) : MI = CLSLIMS.Piece(Info, "^", 7)
        Addr1 = CLSLIMS.Piece(Info, "^", 8) : Addr2 = CLSLIMS.Piece(Info, "^", 9) : City = CLSLIMS.Piece(Info, "^", 10) : ST = CLSLIMS.Piece(Info, "^", 11) : ZIP = CLSLIMS.Piece(Info, "^", 12) : PPhone = CLSLIMS.Piece(Info, "^", 13)
        DOB = CLSLIMS.Piece(Info, "^", 14) : Gender = CLSLIMS.Piece(Info, "^", 15) : CollDate = CLSLIMS.Piece(Info, "^", 16) : CollTime = CLSLIMS.Piece(Info, "^", 17)
        DOB = Format(Date.Parse(DOB), "MM/dd/yyyy")
        DOB = CLSLIMS.Piece(DOB, "/", 3) & CLSLIMS.Piece(DOB, "/", 1) & CLSLIMS.Piece(DOB, "/", 2)
        CollDate = CLSLIMS.Piece(CollDate, "/", 3) & CLSLIMS.Piece(CollDate, "/", 1) & CLSLIMS.Piece(CollDate, "/", 2)
        If CollTime <> "" Then CollTime = Format(CDate(CollTime), "HHmm")
        If CLSLIMS.Piece(GetReqInfo("Ins", ordernum), "^", 1) = "" Then
            BillTo = "C"
        Else
            BillTo = GetAcctBillTo(ACCT)
        End If
        'PDEM
        If PUID <> "" Then
            Cmd.CommandText = "Select PEFDT From PDEM Where PACT=1 And PUID='" & PUID & "'"
            rs = Cmd.ExecuteReader
            If rs.Read Then
                PEFDT = rs(0)
                xCmdText = "Update PDEM SET PID1='" & PID & "', PAD1='" & Addr1 & "', PAD2='" & Addr2 & "',PCITY='" & City &
                "',PSTATE='" & ST & "',PZIP='" & ZIP & "',PPHONE='" & PPhone & "' Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "' And PACT=1"
            End If
            rs.Close()
        Else
            PUID = GetNextPUID()
            PEFDT = CLSLIMS.CDT
            'Insert/Create New PUID/PDEM
            xCmdText = "Insert into PDEM (PUID, PACT, PEFDT, PFNAME, PMNAME, PLNAME, PDOB, SEX, PAD1, PAD2, PCITY, PSTATE, PZIP, PPHONE, PBILLTO) Values ('" & PUID & "',1,'" & PEFDT & "','" & First & "','" & MI &
                  "','" & Last & "','" & DOB & "','" & Gender & "','" & Addr1 & "','" & Addr2 & "','" & City & "','" & ST & "','" & ZIP & "','" & PPhone & "','" & BillTo & "')"
        End If

        Debug.Print(xCmdText)
        Cmd.CommandText = xCmdText
        Try
            Cmd.ExecuteNonQuery()
        Catch
            CLSLIMS.Log(Cmd.CommandText)
        End Try
        'PSPEC
        '==================================

        Cmd.CommandText = "Update PSPEC Set PSTAT='2',PUID='" & PUID & "', PEFDT='" & PEFDT & "',PSCDTA='" & CollDate & "', PSCTMA='" & CollTime & "', PSID3='" & ordernum & "' Where PSPECNO='" & specno & "' And PSACT=1"
        Try
            Cmd.ExecuteNonQuery()
        Catch
            CLSLIMS.Log(Cmd.CommandText)
        End Try
        'PORD
        '===================================
        Cmd.CommandText = "Select PORD, POEFDT,ACCT From PORD Where PORD+POEFDT=(Select PORD+POEFDT From PSPEC Where PSPECNO='" & specno & "' And PSACT=1) And POACT=1"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            PORD = rs(0)
            POEFDT = rs(1)
            ACCT = rs(2)
        End If
        rs.Close()
        Cmd.CommandText = "Update PORD Set REQPHY='" & PHY & "', PUID='" & PUID & "', PEFDT='" & PEFDT & "', POBILLTO='" & BillTo & "' Where PORD='" & PORD & "' And POEFDT='" & POEFDT & "' And POACT=1"
        Try
            Cmd.ExecuteNonQuery()
        Catch
            CLSLIMS.Log(Cmd.CommandText)
        End Try
        'Medications/Diagnosis
        '=============================================
        Info = GetReqInfo("MedDx", ordernum)
        Dim cnt As Integer = 1
        For i = 2 To 11
            If CLSLIMS.Piece(Info, "^", i) <> "" Then
                Cmd.CommandText = "Insert Into POMED Values ('" & PORD & "','" & POEFDT & "'," & cnt & ",'" & CLSLIMS.Piece(Info, "^", i) & "','','','','','','','','','','','')"
                Cmd.ExecuteNonQuery()
                cnt = cnt + 1
            End If
        Next i
        cnt = 1
        For i = 12 To 17
            If CLSLIMS.Piece(Info, "^", i) <> "" Then
                Cmd.CommandText = "Insert Into PODIAG Values ('" & PORD & "','" & POEFDT & "'," & i & ",'" & CLSLIMS.Piece(Info, "^", i) & "','')"
                Cmd.ExecuteNonQuery()
                cnt = cnt + 1
            End If
        Next i
        'Insurance
        '=========================================
        'IN1|1||AARP HEALTH|AARP HEALTH CARE OPTIONS|PO BOX 740819^^ATLANTA^GA^30374^USA||800-523-5800||||||||OTHER|TEST^INSURANCE^|SE|19590218|2134 Ruthwood Dr.^^IRVING^TX^75039^|||||||||||||||||45648||||||TEST,INSURANCE|M
        Info = GetReqInfo("Ins", ordernum)
        Dim bins As String = "", binsefdt As String = "", Rel As String = "", Name As String = "", GrpNum As String = "", SSN As String = "", IDOB As String = "", IFirst As String = "", ILast As String = ""
        Dim IAddr As String = "", Pol As String = "", InsertStr(5) As String, DelStr(5) As String, binsinfo As String = "'", RecNo As String = "", PRecNo As String = ""
        Dim InsOrd As Integer = 0, MCNum As String = "", IMI As String = ""
        i = 1
        If CLSLIMS.Piece(Info, "^", 1) = "MC" Or CLSLIMS.Piece(Info, "^", 2) = "MC" Or CLSLIMS.Piece(Info, "^", 3) = "MC" Then
            If CLSLIMS.Piece(Info, "^", 1) = "MC" Then
                InsOrd = 1
            ElseIf CLSLIMS.Piece(Info, "^", 2) = "MC" Then
                InsOrd = 2
            ElseIf CLSLIMS.Piece(Info, "^", 3) = "MC" Then
                InsOrd = 3
            End If
            MCNum = CLSLIMS.Piece(Info, "^", 4)
            InsertStr(i) = "Insert Into PIMC Values ('" & PUID & "','" & PEFDT & "',0," & InsOrd & ",'" & MCNum & "','','')"
            DelStr(i) = "Delete From PIMC Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
            i = i + 1
        ElseIf CLSLIMS.Piece(Info, "^", 1) = "MD" Or CLSLIMS.Piece(Info, "^", 2) = "MD" Or CLSLIMS.Piece(Info, "^", 3) = "MD" Then
            If CLSLIMS.Piece(Info, "^", 1) = "MD" Then
                InsOrd = 1
            ElseIf CLSLIMS.Piece(Info, "^", 2) = "MD" Then
                InsOrd = 2
            ElseIf CLSLIMS.Piece(Info, "^", 3) = "MD" Then
                InsOrd = 3
            End If
            InsertStr(i) = "Insert Into PIMD Values ('" & PUID & "','" & PEFDT & "',0," & InsOrd & ",'" & CLSLIMS.Piece(Info, "^", 6) & "','" & CLSLIMS.Piece(Info, "^", 5) & "','','','')"
            DelStr(i) = "Delete From PIMD Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
            i = i + 1
        ElseIf CLSLIMS.Piece(Info, "^", 1) = "SELF" Or CLSLIMS.Piece(Info, "^", 2) = "SELF" Or CLSLIMS.Piece(Info, "^", 3) = "SELF" Then
            If CLSLIMS.Piece(Info, "^", 1) = "SELF" Then
                InsOrd = 1
            ElseIf CLSLIMS.Piece(Info, "^", 2) = "SELF" Then
                InsOrd = 2
            ElseIf CLSLIMS.Piece(Info, "^", 3) = "SELF" Then
                InsOrd = 3
            End If
            bins = "SELFPAY" : binsinfo = "" : binsefdt = "" : Rel = "" : Name = "" : GrpNum = "" : IDOB = "" : IAddr = "" : Pol = "" : SSN = "" : IMI = ""
            Name = CLSLIMS.Piece(Info, "^", 15)
            If CLSLIMS.NumberofPieces(Name, " ") = 3 Then
                IFirst = CLSLIMS.Piece(Name, " ", 1) : IMI = CLSLIMS.Piece(Name, " ", 2) : ILast = CLSLIMS.Piece(Name, " ", 3)
            ElseIf CLSLIMS.NumberofPieces(Name, " ") Then
                IFirst = CLSLIMS.Piece(Name, " ", 1) : ILast = CLSLIMS.Piece(Name, " ", 2)
            End If
            binsefdt = GetBINSEffDate(bins, binsinfo)
            InsertStr(i) = "Insert into PIPI Values ('" & PUID & "','" & PEFDT & "'," & (i - 1) & "," & InsOrd & ",'" & bins & "','" & binsefdt & "','','','SE','','" &
                          IFirst & "','" & IMI & "','" & ILast & "','" & DOB & "','" & CLSLIMS.Piece(Info, "^", 16) & "','','" & CLSLIMS.Piece(Info, "^", 17) & "','','" &
                          CLSLIMS.Piece(Info, "^", 18) & "','','" & CLSLIMS.Piece(Info, "^", 19) & "','" & CLSLIMS.Piece(Info, "^", 20) & "','','','')"
            DelStr(i) = "Delete From PIPI Where PIPIx=" & (i - 1) & " And PUID='" & PUID & "' And PEFDT='" & PEFDT & "' And BINS='" & bins & "' And BINSEFDT='" & binsefdt & "'"
            i = i + 1
        End If

        For j = 1 To 5
            If InsertStr(j) <> "" Then
                Cmd.CommandText = DelStr(j)
                Cmd.ExecuteNonQuery()
                Cmd.CommandText = InsertStr(j)
                Cmd.ExecuteNonQuery()
            End If
        Next j
        Cmd.Dispose()
        Conn.Close()
        Conn.Dispose()
    End Sub
    Public Function GetNextPUID() As Integer
        GetNextPUID = 0
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim pCmd As New SqlCommand("dbo.SYSVAL_NEXTVAL", Conn)
        pCmd.CommandType = CommandType.StoredProcedure
        pCmd.Parameters.Add("SYSVALKEY", SqlDbType.VarChar).Value = "PUID"
        pCmd.Parameters.Add("SYSVALVAL", SqlDbType.Int).Value = ""
        pCmd.Parameters("SYSVALVAL").Direction = ParameterDirection.Output
        pCmd.ExecuteNonQuery()
        GetNextPUID = CInt(pCmd.Parameters("SYSVALVAL").Value)
        pCmd.Dispose()
        Conn.Close()

    End Function
    Public Function CheckForMsg(ByVal Key As String, ByVal Msg As String) As Boolean
        CheckForMsg = False
        Dim Sel As String = "Select count(*) from XORDERS Where ORDERKEY='" & Key & "' And MSGTYPE='" & Msg & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If rs(0) > 0 Then CheckForMsg = True
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
    End Function

    Public Function GetBINSEffDate(ByVal bins As String, ByVal binsinfo As String) As String
        GetBINSEffDate = ""
        Dim Sel As String = "Select BINSEFDT from BINS Where BINS='" & UCase(bins) & "' And BINSACT=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetBINSEffDate = rs(0)
        Else
            rs.Close()
            GetBINSEffDate = CLSLIMS.CDT
            If binsinfo <> "" Then
                Cmd.CommandText = "Insert into BINS Values ('" & bins & "','" & GetBINSEffDate & "',1,'" & CLSLIMS.Piece(binsinfo, "~", 1) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(binsinfo, "~", 2), "^", 1) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(binsinfo, "~", 2), "^", 2) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(binsinfo, "~", 2), "^", 3) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(binsinfo, "~", 2), "^", 4) &
                "','" & Mid(CLSLIMS.Piece(CLSLIMS.Piece(binsinfo, "~", 2), "^", 5), 1, 5) & "','" & CLSLIMS.Piece(CLSLIMS.Piece(binsinfo, "~", 3), "^", 1) & "','','',0,'AUTO',0)"
                Cmd.ExecuteNonQuery()
            End If
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
    End Function

    Public Function RelLookUp(ByVal rel As String) As String
        RelLookUp = "OT"
        Dim Sel As String = "Select SYSLISTVAL from SYSLIST Where SYSLISTKEY='GREL' And SYSLISTOPT='" & rel & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            RelLookUp = rs(0)
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
    End Function
    Public Function GetAcctBillTo(ByVal acct As String) As String
        GetAcctBillTo = ""
        Dim Sel As String = "Select ABILLTO From ACCT Where ACCT='" & acct & "' And AACT=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetAcctBillTo = rs(0)
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()

    End Function

    Public Function COVID(ByVal spec As String) As Boolean
        COVID = False
        Dim Sel As String = "Select count(*) From POC a, PSPEC b Where a.pspecno='" & spec & "' And a.pspecno=b.pspecno and a.adsq1=b.adsq1 and psact=1 and oc in ('M0052','M0070')"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If rs(0) > 0 Then
                COVID = True
            End If
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
        If Not COVID Then
            For i = 0 To lbOC.Items.Count - 1
                If lbOC.Items(i) = "M0052 - COVID19" Or lbOC.Items(i) = "M0070 - COVID" Then
                    COVID = True
                End If
            Next i
        End If
    End Function
    Private Sub lstAcctNameSearch_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstAcctNameSearch.DoubleClick
        txtAcct.Text = CLSLIMS.Piece(lstAcctNameSearch.SelectedItem.ToString, " ", 1)
        lstAcctNameSearch.Visible = False
        ProcessAcct()
    End Sub

    Private Sub lstAcctNameSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstAcctNameSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtAcct.Text = CLSLIMS.Piece(lstAcctNameSearch.SelectedItem.ToString, " ", 1)
            lstAcctNameSearch.Visible = False
            ProcessAcct()
        End If
        If e.KeyCode = Keys.Escape Then
            lstAcctNameSearch.Visible = False
            txtAcct.Text = ""
            txtAcct.Focus()
        End If
    End Sub
    Private Sub lstOCNameSearch_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstOCNameSearch.DoubleClick
        txtOC.Text = CLSLIMS.Piece(lstOCNameSearch.SelectedItem.ToString, " ", 1)
        lstOCNameSearch.Visible = False
        ProcessOC()
    End Sub

    Private Sub lstOCNameSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstOCNameSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtOC.Text = CLSLIMS.Piece(lstOCNameSearch.SelectedItem.ToString, " ", 1)
            lstOCNameSearch.Visible = False
            ProcessOC()
        End If
        If e.KeyCode = Keys.Escape Then
            lstOCNameSearch.Visible = False
            txtOC.Text = ""
            txtOC.Focus()
        End If
    End Sub

    Private Sub lbSamples_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSamples.DoubleClick
        Dim strData As String
        strData = lbSamples.SelectedItem
        'Invisible text field to store item that was double-clicked in list - reset after save
        lblSelItem.Text = lbSamples.SelectedIndex
        '         1         2         3
        '123456789012345678901234567890123456789
        '  1 - 123456    1234    Pain Clinic
        'Sq   Reqnum    Acct    Acct Name
        txtReqNum.Text = Trim(Mid(strData, 7, 10))
        Dim xSel As String = "Select ACCT,OCs From XTRAYS Where PSPECNO='" & txtReqNum.Text & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                txtAcct.Text = cRS(0)
                lblMsg.Text = txtAcct.Text & "-" & GetAcctName(txtAcct.Text)
                For i = 1 To 99
                    If CLSLIMS.Piece(cRS(1), ",", i) <> "" Then
                        lbOC.Items.Add(CLSLIMS.Piece(cRS(1), ",", i) & " - " & GetOCAbbr(CLSLIMS.Piece(cRS(1), ",", i))) : lbOC.SelectedIndex = lbOC.Items.Count - 1
                        lblMsg.Text = lblMsg.Text & vbCrLf & GetOCName(CLSLIMS.Piece(cRS(1), ",", i))
                    Else
                        Exit For
                    End If
                Next i
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Sub

    Public Sub PrintSampLabel(ByVal txtlbl As String, Optional ByVal txtPos As String = "", Optional ByVal ETGTHC As Boolean = False)

        Dim xSel As String = "Select ULASTBCPRT From SYSUSET Where UID='" & CLSLIMS.UID & "'"
        Dim ipAddress As String = CLSLIMS.PtrOne
        If ipAddress = "" Then
            ipAddress = "192.168.65.39" 'Specimen Prep
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                Conn.Open()
                Dim Cmd As New SqlCommand(xSel, Conn)
                Dim cRS As SqlDataReader = Cmd.ExecuteReader
                If cRS.Read Then
                    ipAddress = CLSLIMS.Piece(cRS(0), ":", 1)
                End If
                cRS.Close()
                Cmd.Dispose()
            End Using
        End If
        Dim port As Integer = 9100
        If CLSLIMS.UID = "NED.SABBATINI" Then ipAddress = "192.168.65.209"
        Dim ZPLString As String
        If IsNumeric(Mid(txtlbl, 1, 1)) Then
            If Mid(txtlbl, 1, 1) <> "5" Then
                ZPLString = "^XA" &
                      "^LH10,0" &
                      "^FO65,15" &
                      "^A0N" &
                      "^AE,N,5,5" &
                      "^FD" & txtlbl & "-" & txtPos &
                      "^FS" &
                      "^FO65,60" &
                      "^BY3^B2N,90,N,N,N" &
                      "^FD" & txtlbl &
                      "^FS"
            Else
                ZPLString = "^XA" & "^LH10,0" & "^FO50,35" & "^A0N" & "^AE,N,5,5" & "^FD" & txtlbl & "-" & txtPos & "^FS" &
                "^FO35,80" & "^BY3^BCN,90,N,N,N" & "^FD" & txtlbl & "^FS"
            End If
            If ETGTHC Then
                ZPLString = ZPLString & "^FO100,170^AE,N,5,5^FDETG-THC^FS"
            End If
            ZPLString = ZPLString & "^XZ"
        Else
            ZPLString = "^XA" &
                  "^LH10,0" &
                  "^FO65,15" &
                  "^A0N" &
                  "^AE,5,5" &
                  "^FD" & txtlbl & " " & txtPos &
                  "^FS" &
                  "^FO65,60" &
                  "^BY1" &
                  "^B3N,90,90,N,N" &
                  "^FD" & txtlbl &
                  "^FS" &
                  "^XZ"
        End If

        '^XA - start
        '^LH - reset Label Home
        '^LS0 - label shift 0
        '^BY - change value of narrow bar and space
        '^B2N,90,N,N,N - Barcode Interleve 2 of 5, normal orientation, height 90, print interpretation line NO, print interpretation line above code NO, mod 10 check digit NO
        '^A0N - Font 0-default, orientation - Normal
        '^AE,5,5 - font size 5 height 5 width
        '^FD  data ^FS   start field data, actual data, end field data
        '^XZ - end
        Try
            'Open Connection
            Dim client As New System.Net.Sockets.TcpClient
            client.Connect(ipAddress, port)
            'Write ZPL String to Connection
            Dim writer As New System.IO.StreamWriter(client.GetStream())
            writer.Write(ZPLString)
            writer.Flush()
            'Close Connection
            writer.Close()
            client.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            'Catch Exception Here
        End Try
        'Print additional label on Molecular printer only if not COVID
        If Mid(txtlbl, 1, 1) = "5" And Not COVID(txtlbl) Then
            ipAddress = CLSLIMS.PtrTwo      '"192.168.65.40"
            ZPLString = "^XA" &
              "^LH0,0" &
              "^FO84,5" &     '100-->84
              "^A0N" &
              "^AE,N,1,1" &
              "^FD" & txtlbl &
              "^FS" &
              "^FO67,35" &     '83-->67
              "^BY2^B2N,40,N,N,N" &
              "^FD" & txtlbl &
              "^FS" &
              "^FO279,34" &               'circle   295-->270  35 --->34
              "^A0N" &
              "^AE,N,1,1" &
              "^FD" & Mid(txtlbl, Len(txtlbl) - 3, 4) &
              "^FS" &
              "^FO84,138" &                    '2nd label
              "^A0N" &
              "^AE,N,1,1" &
              "^FD" & txtlbl &
              "^FS" &
              "^FO67,168" &
              "^BY2^B2N,40,N,N,N" &
              "^FD" & txtlbl &
              "^FS" &
              "^FO270,165" &                 'circle
              "^A0N" &
              "^AE,N,1,1" &
              "^FD" & Mid(txtlbl, Len(txtlbl) - 3, 4) &
              "^FS"

            ZPLString = ZPLString & "^XZ"
            Try
                'Open Connection
                Dim client As New System.Net.Sockets.TcpClient
                client.Connect(ipAddress, port)
                'Write ZPL String to Connection
                Dim writer As New System.IO.StreamWriter(client.GetStream())
                writer.Write(ZPLString)
                writer.Flush()
                'Close Connection
                writer.Close()
                client.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
                'Catch Exception Here
            End Try
        End If
    End Sub
    Private Sub PrintTrayLabel(ByVal txtlbl As String)

        Dim xSel As String = "Select ULASTBCPRT From SYSUSET Where UID='" & CLSLIMS.UID & "'"
        Dim ipAddress As String = CLSLIMS.PtrOne   '"192.168.65.30"
        Dim port As Integer = 9100
        If ipAddress = "" Then
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                Conn.Open()
                Dim Cmd As New SqlCommand(xSel, Conn)
                Dim cRS As SqlDataReader = Cmd.ExecuteReader
                If cRS.Read Then
                    ipAddress = CLSLIMS.Piece(cRS(0), ":", 1)
                End If
                cRS.Close()
                Cmd.Dispose()
            End Using
        End If
        Dim ZPLString As String =
              "^XA" &
              "^LH10,0" &
              "^FO20,15" &
              "^A0N" &
              "^AE,5,5" &
              "^FD" & txtlbl &
              "^FS" &
              "^BY1" &
              "^FO20,60" &
              "^B3N,90,100,N,N" &
              "^FD" & txtlbl &
              "^FS" &
              "^XZ"

        '"^BY3" & _
        '^XA - start
        '^LH - reset Label Home
        '^LS0 - label shift 0
        '^BY - change value of narrow bar and space
        '^B2N,90,N,N,N - Barcode Interleve 2 of 5, normal orientation, height 90, print interpretation line NO, print interpretation line above code NO, mod 10 check digit NO
        '^A0N - Font 0-default, orientation - Normal
        '^AE,5,5 - font size 5 height 5 width
        '^FD  data ^FS   start field data, actual data, end field data
        '^XZ - end
        Try
            'Open Connection
            Dim client As New System.Net.Sockets.TcpClient
            client.Connect(ipAddress, port)

            'Write ZPL String to Connection
            Dim writer As New System.IO.StreamWriter(client.GetStream())

            writer.Write(ZPLString)
            writer.Flush()

            'Close Connection
            writer.Close()
            client.Close()

        Catch ex As Exception

            'Catch Exception Here

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        PrintTrayLabel(txtTray.Text)
    End Sub

    Private Sub lbOC_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lbOC.KeyDown
        If e.KeyCode <> Keys.Delete Then
            Exit Sub
        End If
        Do While (lbOC.SelectedItems.Count > 0)
            lbOC.Items.Remove(lbOC.SelectedItem)
        Loop
    End Sub

    Private Sub txtTray_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles txtTray.PreviewKeyDown
        If e.KeyCode = Keys.Tab Then
            e.IsInputKey = True
        End If
    End Sub

    Private Sub btnPrtLbl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrtLbl.Click
        Dim lblSpecNum As String = InputBox("Specimen Number:")
        Dim TL As String, Seq As String
        Dim sq As Integer = Val(Mid(txtTray.Text, 3, 2))
        TL = Mid("ABCDEFGHJKLMNPQRSTUVWXYZ", sq, 1)
        Seq = GetTraySeq(lblSpecNum)
        If Val(Seq) < 10 Then Seq = "0" & Seq
        If lblSpecNum <> "" Then PrintSampLabel(lblSpecNum, TL & Seq)
    End Sub

    Public Function GetTraySeq(ByVal spec As String) As String
        Dim xSel As String = "Select TRAYx From XTRAYS Where PSPECNO ='" & spec & "'"
        GetTraySeq = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetTraySeq = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Sub lstStatusCodeSearch_DoubleClick(sender As Object, e As System.EventArgs) Handles lstStatusCodeSearch.DoubleClick
        txtStatusCode.Text = CLSLIMS.Piece(lstStatusCodeSearch.SelectedItem.ToString, " ", 1)
        lstStatusCodeSearch.Visible = False
        ProcessStatCode()
    End Sub

    Private Sub lstStatusCode_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles lstStatusCode.KeyDown
        If e.KeyCode <> Keys.Delete Then
            Exit Sub
        End If
        Do While (lstStatusCode.SelectedItems.Count > 0)
            lstStatusCode.Items.Remove(lstStatusCode.SelectedItem)
        Loop
    End Sub

    Private Sub lstStatusCode_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles lstStatusCode.MouseClick
        Dim statcd As String = lstStatusCode.Items(lstStatusCode.SelectedIndex)
        lblMsg.Text = statcd & "-" & GetSCName(statcd)
    End Sub

    Private Sub btnGrid_Click(sender As Object, e As EventArgs) Handles btnGrid.Click
        If Mid(txtAcct.Text, 1, 1) = "S" Then
            frmSOEGrid.ShowDialog()
        Else
            frmOEGrid.ShowDialog()
        End If
    End Sub

End Class