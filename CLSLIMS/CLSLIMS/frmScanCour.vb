﻿Imports CLSLIMS
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.Data.SqlClient

Public Class frmScanCour

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub txtScan_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtScan.KeyPress
        If e.KeyChar = vbCr Then
            If Len(txtScan.Text) > 20 Then txtScan.Text = Mid(txtScan.Text, Len(txtScan.Text) - 11)
            Dim CarrierInfo As String = "", OrderNum As String = "", Acct As String = "", Aname As String = ""
            Dim strConn As String = "Server=192.168.65.100;Port=3306;Database=compass;Uid=nsabbatini;Pwd=Compass1910;"
            Dim connection As New MySqlConnection
            connection.ConnectionString = strConn
            connection.Open()
            Dim Sel As String = "Select * From TRACKING Where TRACKNUM='" & txtScan.Text & "'"
            Dim iCmd As New MySqlCommand
            iCmd.Connection = connection
            iCmd.CommandText = Sel
            Dim rs As MySqlDataReader = iCmd.ExecuteReader
            If rs.Read Then
                Debug.Print(rs(0), rs(1))
                Sel = "Update TRACKING Set ScanDate='" & Format(Now, "yyyyMMddhhmmss") & "', STATUS='RECV' Where TRACKNUM='" & txtScan.Text & "'"
                lstBarcodeNum.Items.Add(CLSLIMS.LJ(rs(4), 10) & CLSLIMS.LJ(txtScan.Text, 25) & CLSLIMS.LJ(rs(2), 40)) : lstBarcodeNum.SelectedIndex = lstBarcodeNum.Items.Count - 1
            Else
                CarrierInfo = GetCarrierInfo(txtScan.Text) : OrderNum = CLSLIMS.Piece(CarrierInfo, "^", 1) : Acct = CLSLIMS.Piece(CarrierInfo, "^", 2) : Aname = CLSLIMS.Piece(CarrierInfo, "^", 3) : CarrierInfo = CLSLIMS.Piece(CarrierInfo, "^", 4)
                Sel = "Insert into TRACKING Values ('" & OrderNum & "','" & Acct & "','" & Aname & "','" & txtScan.Text & "','" & CarrierInfo & "','','RECV','','','" & Format(Now, "yyyyMMddhhmmss") & "','','') "
                lstBarcodeNum.Items.Add(Space(10) & CLSLIMS.LJ(txtScan.Text, 25)) : lstBarcodeNum.SelectedIndex = lstBarcodeNum.Items.Count - 1
            End If
            rs.Close()
            iCmd.CommandText = Sel
            iCmd.ExecuteNonQuery()
            iCmd.Dispose()
            connection.Close()
            txtScan.Text = ""
            txtScan.Focus()
            lblCount.Text = lstBarcodeNum.Items.Count : lblCount.Refresh()
        ElseIf Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        txtScan.Focus()
    End Sub

    Public Function GetCarrierInfo(trknum As String) As String
        GetCarrierInfo = ""
        'Order#^Acct^Aname^Carrier
        Dim strConn As String = "Server=192.168.65.100;Port=3306;Database=compass;Uid=nsabbatini;Pwd=Compass1910;"
        Dim conn As New MySqlConnection
        conn.ConnectionString = strConn
        conn.Open()
        Dim iCmd As New MySqlCommand
        iCmd.Connection = conn
        iCmd.CommandText = "Select OrderNum,Acct,Carrier From Inbound Where inbtrknum='" & trknum & "'"
        Dim rs As MySqlDataReader = iCmd.ExecuteReader
        If rs.Read Then
            GetCarrierInfo = rs(0) & "^" & rs(1) & "^" & GetAcctName(rs(1)) & "^" & rs(2)
        End If
        rs.Close()
        conn.Close()

    End Function
    Private Function GetAcctName(ByVal acct As String) As String
        Dim xSel As String = "Select ANAME from ACCT where aact=1 and acct= '" & acct & "'"
        GetAcctName = ""
        If acct = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(0)
            End If
        End Using

    End Function
    Private Sub frmScanCour_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Dim strConn As String = "Server=192.168.65.100;Port=3306;Database=compass;Uid=nsabbatini;Pwd=Compass1910;"
        'Dim connection As New MySqlConnection
        'connection.ConnectionString = strConn
        'connection.Open()
        'Dim Sel As String = "Select max(ESTDELDATE) From TRACKING"
        'Dim iCmd As New MySqlCommand
        'iCmd.Connection = connection
        'iCmd.CommandText = Sel
        'Dim rs As MySqlDataReader = iCmd.ExecuteReader
        'If rs.Read Then
        '    Label2.Text = Mid(rs(0), 5, 2) & "/" & Mid(rs(0), 7, 2) & "/" & Mid(rs(0), 1, 4)
        'End If
        txtScan.Focus()
    End Sub
End Class