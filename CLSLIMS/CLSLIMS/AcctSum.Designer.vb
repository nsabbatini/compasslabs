﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AcctSum
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.txtStartDt = New System.Windows.Forms.TextBox()
        Me.txtEndDt = New System.Windows.Forms.TextBox()
        Me.lstAcctNameSearch = New System.Windows.Forms.ListBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblAcct = New System.Windows.Forms.Label()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnXLS = New System.Windows.Forms.Button()
        Me.lstXLS = New System.Windows.Forms.ListBox()
        Me.btnRunZip = New System.Windows.Forms.Button()
        Me.lstClass = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 19)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Account:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 19)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Start Date:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(32, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 19)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "End Date:"
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(118, 29)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(86, 27)
        Me.txtAcct.TabIndex = 5
        '
        'txtStartDt
        '
        Me.txtStartDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDt.Location = New System.Drawing.Point(118, 64)
        Me.txtStartDt.Name = "txtStartDt"
        Me.txtStartDt.Size = New System.Drawing.Size(128, 27)
        Me.txtStartDt.TabIndex = 6
        Me.txtStartDt.Text = "01/01/2019"
        '
        'txtEndDt
        '
        Me.txtEndDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEndDt.Location = New System.Drawing.Point(118, 98)
        Me.txtEndDt.Name = "txtEndDt"
        Me.txtEndDt.Size = New System.Drawing.Size(128, 27)
        Me.txtEndDt.TabIndex = 7
        Me.txtEndDt.Text = "01/03/2019"
        '
        'lstAcctNameSearch
        '
        Me.lstAcctNameSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAcctNameSearch.FormattingEnabled = True
        Me.lstAcctNameSearch.ItemHeight = 14
        Me.lstAcctNameSearch.Location = New System.Drawing.Point(118, 54)
        Me.lstAcctNameSearch.Name = "lstAcctNameSearch"
        Me.lstAcctNameSearch.Size = New System.Drawing.Size(338, 186)
        Me.lstAcctNameSearch.TabIndex = 26
        Me.lstAcctNameSearch.Visible = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(550, 328)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 26)
        Me.Button1.TabIndex = 27
        Me.Button1.Text = "EXIT"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblAcct
        '
        Me.lblAcct.AutoSize = True
        Me.lblAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcct.Location = New System.Drawing.Point(224, 32)
        Me.lblAcct.Name = "lblAcct"
        Me.lblAcct.Size = New System.Drawing.Size(0, 19)
        Me.lblAcct.TabIndex = 28
        '
        'btnRun
        '
        Me.btnRun.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRun.Location = New System.Drawing.Point(445, 328)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 26)
        Me.btnRun.TabIndex = 29
        Me.btnRun.Text = "RUN"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'lstData
        '
        Me.lstData.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 18
        Me.lstData.Location = New System.Drawing.Point(26, 125)
        Me.lstData.Margin = New System.Windows.Forms.Padding(2)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(659, 166)
        Me.lstData.TabIndex = 30
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(354, 328)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 26)
        Me.btnPrint.TabIndex = 31
        Me.btnPrint.Text = "PRINT"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnXLS
        '
        Me.btnXLS.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnXLS.Location = New System.Drawing.Point(263, 328)
        Me.btnXLS.Name = "btnXLS"
        Me.btnXLS.Size = New System.Drawing.Size(75, 26)
        Me.btnXLS.TabIndex = 32
        Me.btnXLS.Text = "XLS"
        Me.btnXLS.UseVisualStyleBackColor = True
        '
        'lstXLS
        '
        Me.lstXLS.FormattingEnabled = True
        Me.lstXLS.Location = New System.Drawing.Point(592, 29)
        Me.lstXLS.Margin = New System.Windows.Forms.Padding(2)
        Me.lstXLS.Name = "lstXLS"
        Me.lstXLS.Size = New System.Drawing.Size(65, 43)
        Me.lstXLS.TabIndex = 33
        Me.lstXLS.Visible = False
        '
        'btnRunZip
        '
        Me.btnRunZip.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRunZip.Location = New System.Drawing.Point(493, 62)
        Me.btnRunZip.Name = "btnRunZip"
        Me.btnRunZip.Size = New System.Drawing.Size(75, 26)
        Me.btnRunZip.TabIndex = 34
        Me.btnRunZip.Text = "RUN ZIP"
        Me.btnRunZip.UseVisualStyleBackColor = True
        Me.btnRunZip.Visible = False
        '
        'lstClass
        '
        Me.lstClass.FormattingEnabled = True
        Me.lstClass.Location = New System.Drawing.Point(592, 78)
        Me.lstClass.Margin = New System.Windows.Forms.Padding(2)
        Me.lstClass.Name = "lstClass"
        Me.lstClass.Size = New System.Drawing.Size(65, 43)
        Me.lstClass.TabIndex = 35
        Me.lstClass.Visible = False
        '
        'AcctSum
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(692, 365)
        Me.Controls.Add(Me.lstClass)
        Me.Controls.Add(Me.btnRunZip)
        Me.Controls.Add(Me.lstXLS)
        Me.Controls.Add(Me.btnXLS)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.lstAcctNameSearch)
        Me.Controls.Add(Me.lblAcct)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtEndDt)
        Me.Controls.Add(Me.txtStartDt)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "AcctSum"
        Me.Text = "AcctSum"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtAcct As System.Windows.Forms.TextBox
    Friend WithEvents txtStartDt As System.Windows.Forms.TextBox
    Friend WithEvents txtEndDt As System.Windows.Forms.TextBox
    Friend WithEvents lstAcctNameSearch As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblAcct As System.Windows.Forms.Label
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents lstData As System.Windows.Forms.ListBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnXLS As System.Windows.Forms.Button
    Friend WithEvents lstXLS As System.Windows.Forms.ListBox
    Friend WithEvents btnRunZip As Button
    Friend WithEvents lstClass As ListBox
End Class
