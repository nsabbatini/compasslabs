﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSOSMonRpt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbDate = New System.Windows.Forms.ComboBox()
        Me.lvRpt = New System.Windows.Forms.ListView()
        Me.Account = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AcctName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.City = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ST = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ItemId = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ItemDesc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Total = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Vol = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(45, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Year/Month:"
        '
        'cmbDate
        '
        Me.cmbDate.FormattingEnabled = True
        Me.cmbDate.Location = New System.Drawing.Point(164, 26)
        Me.cmbDate.Name = "cmbDate"
        Me.cmbDate.Size = New System.Drawing.Size(121, 27)
        Me.cmbDate.TabIndex = 2
        '
        'lvRpt
        '
        Me.lvRpt.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Account, Me.AcctName, Me.City, Me.ST, Me.ItemId, Me.ItemDesc, Me.Total, Me.Vol})
        Me.lvRpt.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvRpt.GridLines = True
        Me.lvRpt.Location = New System.Drawing.Point(27, 85)
        Me.lvRpt.Name = "lvRpt"
        Me.lvRpt.Size = New System.Drawing.Size(1025, 273)
        Me.lvRpt.TabIndex = 3
        Me.lvRpt.UseCompatibleStateImageBehavior = False
        Me.lvRpt.View = System.Windows.Forms.View.Details
        '
        'Account
        '
        Me.Account.Text = "Account"
        Me.Account.Width = 80
        '
        'AcctName
        '
        Me.AcctName.Text = "Account Name"
        Me.AcctName.Width = 220
        '
        'City
        '
        Me.City.Text = "City"
        Me.City.Width = 160
        '
        'ST
        '
        Me.ST.Text = "ST"
        Me.ST.Width = 40
        '
        'ItemId
        '
        Me.ItemId.Text = "Item #"
        Me.ItemId.Width = 100
        '
        'ItemDesc
        '
        Me.ItemDesc.Text = "Item Description"
        Me.ItemDesc.Width = 210
        '
        'Total
        '
        Me.Total.Text = "Totals"
        Me.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Total.Width = 90
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(319, 21)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 34)
        Me.btnRun.TabIndex = 4
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(839, 376)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 34)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(697, 376)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(96, 34)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "Save XLS"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Vol
        '
        Me.Vol.Text = "Volume"
        Me.Vol.Width = 100
        '
        'frmSOSMonRpt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1081, 422)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.lvRpt)
        Me.Controls.Add(Me.cmbDate)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmSOSMonRpt"
        Me.Text = "frmSOSMonRpt"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents cmbDate As ComboBox
    Friend WithEvents lvRpt As ListView
    Friend WithEvents btnRun As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents Account As ColumnHeader
    Friend WithEvents AcctName As ColumnHeader
    Friend WithEvents City As ColumnHeader
    Friend WithEvents ST As ColumnHeader
    Friend WithEvents ItemId As ColumnHeader
    Friend WithEvents ItemDesc As ColumnHeader
    Friend WithEvents Total As ColumnHeader
    Friend WithEvents Vol As ColumnHeader
End Class
