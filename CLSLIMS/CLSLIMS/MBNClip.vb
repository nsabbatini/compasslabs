﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class MBNClip

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Dim Sel As String, Fnd As Boolean, CBtxt As String = ""
        If txtMBN.Text = "" Then
            MsgBox("No MBN")
        End If
        Sel = "Select distinct PBIx,PSPECNO From PBI B, PB C Where B.WL=C.WL And B.BATCH=C.BATCH And BACT=1 And B.BEFDT=C.BEFDT And MBATCH='" & txtMBN.Text & "' Order by PBIx"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Fnd = False
        Do While rs.Read()
            CBtxt = CBtxt & rs(1) & vbCrLf
            Fnd = True
        Loop
        rs.Close()
        If Not Fnd Then
            MsgBox("MBN not found")
        Else
            Clipboard.SetText(CBtxt)
            Me.Close()
            Me.Dispose()
        End If
    End Sub
End Class