﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.ComponentModel
Public Class frmDefCutRng
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub frmDefCutRng_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Clear()

    End Sub
    Public Sub Clear()
        txtCutOff.Text = ""
        txtLowBot.Text = ""
        txtModBot.Text = ""
        lblHighBot.Text = ""
        lblModTop.Text = ""
    End Sub
    Public Sub GetRanges()
        txtCutOff.Text = "" : txtLowBot.Text = "" : txtModBot.Text = "" : lblHighBot.Text = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Dim rs As MySqlDataReader = Nothing
        Dim Src As String = "", Del As String = ""
        Cmd.CommandText = "Select CUTOFF,LOW,MID,HIGH from RANGES Where active=1"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            txtCutOff.Text = rs(0)
            txtLowBot.Text = rs(1)
            txtModBot.Text = rs(2)
            lblHighBot.Text = rs(3)
        End If
        rs.Close()
        ConnMySql.Close()
        lblLowTop.Text = txtCutOff.Text
        lblModTop.Text = txtLowBot.Text
        lblHighBot.Text = txtModBot.Text
    End Sub

    Public Function GetOrgName(ByVal org As String) As String
        GetOrgName = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select OGNAME from MOLECULAR Where Active=1 and TYPE='ORG' And OGABBR='" & org & "'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetOrgName = rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        'Field Validations

        'Update RANGES
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Update Ranges Set Active=0 Where Active=1"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Insert into Ranges Values ('" & txtCutOff.Text & "','" & txtLowBot.Text & "','" & txtModBot.Text & "','" & lblHighBot.Text & "','" & Format(Now, "yyyyMMddhhmmss") & "',1)"
        Cmd.ExecuteNonQuery()
        ConnMySql.Close()
        Clear()
    End Sub

    Private Sub txtCutOff_TextChanged(sender As Object, e As EventArgs) Handles txtCutOff.TextChanged
        lblLowTop.Text = txtCutOff.Text
    End Sub

    Private Sub txtLowBot_TextChanged(sender As Object, e As EventArgs) Handles txtLowBot.TextChanged
        lblModTop.Text = txtLowBot.Text
    End Sub

    Private Sub txtModBot_TextChanged(sender As Object, e As EventArgs) Handles txtModBot.TextChanged
        lblHighBot.Text = txtModBot.Text
    End Sub

    Private Sub btnLatest_Click(sender As Object, e As EventArgs) Handles btnLatest.Click
        GetRanges()
    End Sub
End Class