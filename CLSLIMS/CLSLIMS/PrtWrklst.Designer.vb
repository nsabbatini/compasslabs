﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PrtWrklst
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PrtWrklst))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFldr = New System.Windows.Forms.TextBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.PrintPrev = New System.Windows.Forms.PrintPreviewDialog()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.lblWrklst = New System.Windows.Forms.Label()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.lblWrkLst2 = New System.Windows.Forms.Label()
        Me.lblWrkLst3 = New System.Windows.Forms.Label()
        Me.lblWrkLst4 = New System.Windows.Forms.Label()
        Me.lblWrkLst5 = New System.Windows.Forms.Label()
        Me.lstScrn = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(33, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Folder:"
        '
        'txtFldr
        '
        Me.txtFldr.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFldr.Location = New System.Drawing.Point(100, 22)
        Me.txtFldr.Name = "txtFldr"
        Me.txtFldr.Size = New System.Drawing.Size(128, 27)
        Me.txtFldr.TabIndex = 2
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(142, 65)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(64, 32)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(189, 112)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(52, 32)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'PrintDoc
        '
        '
        'PrintPrev
        '
        Me.PrintPrev.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPrev.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPrev.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPrev.Document = Me.PrintDoc
        Me.PrintPrev.Enabled = True
        Me.PrintPrev.Icon = CType(resources.GetObject("PrintPrev.Icon"), System.Drawing.Icon)
        Me.PrintPrev.Name = "PrintPrev"
        Me.PrintPrev.Visible = False
        '
        'lstData
        '
        Me.lstData.Font = New System.Drawing.Font("Courier New", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 17
        Me.lstData.Location = New System.Drawing.Point(9, 38)
        Me.lstData.Margin = New System.Windows.Forms.Padding(2)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(27, 21)
        Me.lstData.TabIndex = 5
        Me.lstData.Visible = False
        '
        'lblWrklst
        '
        Me.lblWrklst.AutoSize = True
        Me.lblWrklst.Location = New System.Drawing.Point(158, 112)
        Me.lblWrklst.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblWrklst.Name = "lblWrklst"
        Me.lblWrklst.Size = New System.Drawing.Size(0, 13)
        Me.lblWrklst.TabIndex = 6
        Me.lblWrklst.Visible = False
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Location = New System.Drawing.Point(55, 65)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(81, 32)
        Me.btnPreview.TabIndex = 7
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'lblWrkLst2
        '
        Me.lblWrkLst2.AutoSize = True
        Me.lblWrkLst2.Location = New System.Drawing.Point(119, 112)
        Me.lblWrkLst2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblWrkLst2.Name = "lblWrkLst2"
        Me.lblWrkLst2.Size = New System.Drawing.Size(0, 13)
        Me.lblWrkLst2.TabIndex = 8
        Me.lblWrkLst2.Visible = False
        '
        'lblWrkLst3
        '
        Me.lblWrkLst3.AutoSize = True
        Me.lblWrkLst3.Location = New System.Drawing.Point(97, 112)
        Me.lblWrkLst3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblWrkLst3.Name = "lblWrkLst3"
        Me.lblWrkLst3.Size = New System.Drawing.Size(0, 13)
        Me.lblWrkLst3.TabIndex = 9
        Me.lblWrkLst3.Visible = False
        '
        'lblWrkLst4
        '
        Me.lblWrkLst4.AutoSize = True
        Me.lblWrkLst4.Location = New System.Drawing.Point(61, 112)
        Me.lblWrkLst4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblWrkLst4.Name = "lblWrkLst4"
        Me.lblWrkLst4.Size = New System.Drawing.Size(0, 13)
        Me.lblWrkLst4.TabIndex = 10
        Me.lblWrkLst4.Visible = False
        '
        'lblWrkLst5
        '
        Me.lblWrkLst5.AutoSize = True
        Me.lblWrkLst5.Location = New System.Drawing.Point(34, 112)
        Me.lblWrkLst5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblWrkLst5.Name = "lblWrkLst5"
        Me.lblWrkLst5.Size = New System.Drawing.Size(0, 13)
        Me.lblWrkLst5.TabIndex = 11
        Me.lblWrkLst5.Visible = False
        '
        'lstScrn
        '
        Me.lstScrn.Font = New System.Drawing.Font("Courier New", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstScrn.FormattingEnabled = True
        Me.lstScrn.ItemHeight = 17
        Me.lstScrn.Location = New System.Drawing.Point(7, 65)
        Me.lstScrn.Margin = New System.Windows.Forms.Padding(2)
        Me.lstScrn.Name = "lstScrn"
        Me.lstScrn.Size = New System.Drawing.Size(27, 21)
        Me.lstScrn.TabIndex = 12
        Me.lstScrn.Visible = False
        '
        'PrtWrklst
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(251, 150)
        Me.Controls.Add(Me.lstScrn)
        Me.Controls.Add(Me.lblWrkLst5)
        Me.Controls.Add(Me.lblWrkLst4)
        Me.Controls.Add(Me.lblWrkLst3)
        Me.Controls.Add(Me.lblWrkLst2)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.lblWrklst)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.txtFldr)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "PrtWrklst"
        Me.Text = "Print Worklist"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFldr As System.Windows.Forms.TextBox
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents PrintDoc As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintPrev As System.Windows.Forms.PrintPreviewDialog
    Friend WithEvents lstData As System.Windows.Forms.ListBox
    Friend WithEvents lblWrklst As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents lblWrkLst2 As System.Windows.Forms.Label
    Friend WithEvents lblWrkLst3 As Label
    Friend WithEvents lblWrkLst4 As Label
    Friend WithEvents lblWrkLst5 As Label
    Friend WithEvents lstScrn As ListBox
End Class
