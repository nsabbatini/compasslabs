﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Printing

Public Class PrtReq

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Dim printer As New PCPrint()
        'Set the font we want to use
        printer.PrinterFont = New Font("Courier New", 10)
        'Set Margins
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        printer.DefaultPageSettings.Margins = mar
        'Set the TextToPrint property
        Dim strText As String
        strText = "Account #: 1234000" & vbCrLf & "Account Name" & vbCrLf & "Account Address" & vbCrLf & "Account City, ST  12345"
        strText = strText & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf
        strText = strText & Space(20) & "        [ ] Physician Name One           [ ] Physician Name Two" & vbCrLf
        strText = strText & Space(20) & "    [ ] Physician Name Three         [ ] Physician Name Four"
        printer.TextToPrint = " " & strText
        'Issue print command
        printer.Print()
    End Sub

End Class
