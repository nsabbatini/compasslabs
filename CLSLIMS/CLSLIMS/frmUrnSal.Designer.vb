﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUrnSal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUrnSal))
        Me.dtpProfDt = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.PrintPrev = New System.Windows.Forms.PrintPreviewDialog()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'dtpProfDt
        '
        Me.dtpProfDt.CalendarFont = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProfDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProfDt.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProfDt.Location = New System.Drawing.Point(165, 47)
        Me.dtpProfDt.Name = "dtpProfDt"
        Me.dtpProfDt.Size = New System.Drawing.Size(148, 27)
        Me.dtpProfDt.TabIndex = 75
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(36, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 19)
        Me.Label2.TabIndex = 76
        Me.Label2.Text = "Receive Date:"
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(148, 118)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(93, 28)
        Me.btnPrint.TabIndex = 77
        Me.btnPrint.Text = "PRINT"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'PrintDoc
        '
        '
        'lstData
        '
        Me.lstData.FormattingEnabled = True
        Me.lstData.Location = New System.Drawing.Point(351, 63)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(24, 30)
        Me.lstData.TabIndex = 78
        Me.lstData.Visible = False
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(247, 118)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(93, 28)
        Me.btnExit.TabIndex = 79
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'PrintPrev
        '
        Me.PrintPrev.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPrev.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPrev.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPrev.Document = Me.PrintDoc
        Me.PrintPrev.Enabled = True
        Me.PrintPrev.Icon = CType(resources.GetObject("PrintPrev.Icon"), System.Drawing.Icon)
        Me.PrintPrev.Name = "PrintPrev"
        Me.PrintPrev.Visible = False
        '
        'btnPrev
        '
        Me.btnPrev.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrev.Location = New System.Drawing.Point(33, 118)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(97, 28)
        Me.btnPrev.TabIndex = 80
        Me.btnPrev.Text = "PREVIEW"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'frmUrnSal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(387, 182)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpProfDt)
        Me.Name = "frmUrnSal"
        Me.Text = "Urine Saliva Samples on Same Date of Service"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dtpProfDt As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents btnPrint As Button
    Friend WithEvents PrintDoc As Printing.PrintDocument
    Friend WithEvents lstData As ListBox
    Friend WithEvents btnExit As Button
    Friend WithEvents PrintPrev As PrintPreviewDialog
    Friend WithEvents btnPrev As Button
End Class
