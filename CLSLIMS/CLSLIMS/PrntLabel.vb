﻿Public Class PrntLabel

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim ipAddress As String = "192.168.65.39"
        Dim port As Integer = 9100
        Dim cnt As Integer = 1530
        If InStr(cmbPrinter.Text, ".") Then
            ipAddress = cmbPrinter.Text
        End If
        Select Case cmbPrinter.Text
            Case "Anthony" : ipAddress = "192.168.65.253"
            Case "Adrian" : ipAddress = "192.168.65.31"
            Case "Nancy" : ipAddress = "192.168.65.206"
            Case "Virginia" : ipAddress = "192.168.65.199"
            Case "Laura" : ipAddress = "192.168.65.38"
            Case "Samp Prep" : ipAddress = "192.168.65.39"
            Case "Darran" : ipAddress = "192.168.65.207"
            Case "Molecular" : ipAddress = "192.168.65.37"
            Case "Ned" : ipAddress = "192.168.65.209"
        End Select
        'For j = 1 To 200
        Dim ZPLString As String
            ZPLString = "^XA" &
                      "^LH10,0" &
                      "^FO40,60" &
                      "^A0N" &
                      "^AE,3,3" &
                      "^FD" & txtLine1.Text &
                      "^FS" &
                      "^FO40,100" &
                      "^AE,3,3" &
                      "^FD" & txtLine2.Text &
                      "^FS" &
                      "^XZ"
        'cnt = cnt + 1
        '^XA - start
        '^LH - reset Label Home
        '^LS0 - label shift 0
        '^BY - change value of narrow bar and space
        '^B2N,90,N,N,N - Barcode Interleve 2 of 5, normal orientation, height 90, print interpretation line NO, print interpretation line above code NO, mod 10 check digit NO
        '^A0N - Font 0-default, orientation - Normal
        '^AE,5,5 - font size 5 height 5 width
        '^FD  data ^FS   start field data, actual data, end field data
        '^XZ - end
        Try
                'Open Connection
                Dim client As New System.Net.Sockets.TcpClient
                client.Connect(ipAddress, port)

                'Write ZPL String to Connection
                Dim writer As New System.IO.StreamWriter(client.GetStream())
                For i = 1 To Val(txtCount.Text)
                    writer.Write(ZPLString)
                    writer.Flush()
                Next i
                'Close Connection
                writer.Close()
                client.Close()

            Catch ex As Exception

                'Catch Exception Here

            End Try
        'txtLine1.Text = ""
        'txtLine2.Text = ""
        'Next j
    End Sub
End Class