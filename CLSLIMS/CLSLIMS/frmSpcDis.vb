﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmSpcDis
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub txtTray_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTray.KeyPress
        Me.BackColor = Color.Gray
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If Not CheckTray(txtTray.Text) Then
            MsgBox("Invalid Tray Nummber.")
            Exit Sub
        End If
        If CheckAff(txtTray.Text) Then
            MsgBox("Tray has status codes pending.")
            Me.BackColor = Color.Red
            Exit Sub
        End If
        If CheckRpted(txtTray.Text) Then
            MsgBox("Tray has unreported samples.")
            Me.BackColor = Color.Red
            Exit Sub
        End If
        Dim TrayDt As Date = CDate(Mid(txtTray.Text, 6, 2) & "/" & Mid(txtTray.Text, 8, 2) & "/" & Mid(txtTray.Text, 10, 2))
        lblTrayNum.Text = "Tray: " & txtTray.Text
        If DateDiff(DateInterval.Day, TrayDt, Now) > 27 Then
            lstInfo.Items.Add(txtTray.Text & " - DISPOSE")
            Me.BackColor = Color.Green
            lblStatus.Text = "OK TO DISPOSE."
            InsAudit(txtTray.Text)
        Else
            lstInfo.Items.Add(txtTray.Text & " - KEEP")
            Me.BackColor = Color.Red
            lblStatus.Text = "DO NOT DISPOSE."
        End If
        lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        txtTray.Text = ""
    End Sub

    Public Sub InsAudit(ByVal tray As String)
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Dim SQL As String = ""
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Insert into DISPOSAL values ('" & tray & "',Now(),'" & CLSLIMS.UID & "')"
        Cmd.ExecuteNonQuery()
        ConnMySql.Close()

    End Sub
    Private Sub frmSpcDis_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        lblStatus.Text = ""
        lblTrayNum.Text = ""
        Me.BackColor = Color.Gray
    End Sub
    Public Function CheckTray(ByVal tray As String) As Boolean
        Dim xSel As String = "Select count(*) From XTRAYS Where TRAY='" & tray & "'"
        CheckTray = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                If cRS(0) > 0 Then
                    CheckTray = True
                End If
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Public Function CheckAff(ByVal tray As String) As Boolean
        Dim xSel As String = "Select pspecno From XTRAYS Where TRAY='" & tray & "'"
        CheckAff = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                If Holds(cRS(0)) Then
                    CheckAff = True
                    Exit Do
                End If
            Loop
            cRS.Close()
            Cmd.Dispose()
        End Using

    End Function
    Public Function Holds(ByVal spec As String) As Boolean
        Dim xSel As String = "Select count(*) From PSSTAT a, pspec b Where a.pspecno=b.pspecno and psact=1 and a.adsq1=b.adsq1 and a.PSPECNO='" & spec & "'"
        Holds = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                If cRS(0) > 0 Then
                    lstInfo.Items.Add(spec & " has status code.")
                    Holds = True
                End If
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Public Function CheckRpted(ByVal tray As String) As Boolean
        Dim xSel As String = "Select a.pspecno From XTRAYS a, pspec b Where TRAY='" & tray & "' and a.pspecno=b.pspecno and psact=1 and psrptdt=''"
        CheckRpted = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                lstInfo.Items.Add(cRS(0) & " has not reported.")
                CheckRpted = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using

    End Function
End Class