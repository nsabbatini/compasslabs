﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MonStat
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtStartDt = New System.Windows.Forms.TextBox()
        Me.lstMonStat = New System.Windows.Forms.ListBox()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtEndDt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.btnRpt = New System.Windows.Forms.Button()
        Me.btnXLS = New System.Windows.Forms.Button()
        Me.lstXLS = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(550, 261)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(78, 31)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(319, 261)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(78, 31)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(30, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 19)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Start Date:"
        '
        'txtStartDt
        '
        Me.txtStartDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDt.Location = New System.Drawing.Point(126, 11)
        Me.txtStartDt.Name = "txtStartDt"
        Me.txtStartDt.Size = New System.Drawing.Size(109, 27)
        Me.txtStartDt.TabIndex = 0
        '
        'lstMonStat
        '
        Me.lstMonStat.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstMonStat.FormattingEnabled = True
        Me.lstMonStat.ItemHeight = 16
        Me.lstMonStat.Location = New System.Drawing.Point(3, 87)
        Me.lstMonStat.Name = "lstMonStat"
        Me.lstMonStat.Size = New System.Drawing.Size(625, 164)
        Me.lstMonStat.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(39, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 19)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "End Date:"
        '
        'txtEndDt
        '
        Me.txtEndDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEndDt.Location = New System.Drawing.Point(126, 51)
        Me.txtEndDt.Name = "txtEndDt"
        Me.txtEndDt.Size = New System.Drawing.Size(109, 27)
        Me.txtEndDt.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(278, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 19)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Account:"
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(358, 13)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(76, 27)
        Me.txtAcct.TabIndex = 2
        Me.txtAcct.Text = "ALL"
        '
        'btnRpt
        '
        Me.btnRpt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRpt.Location = New System.Drawing.Point(183, 261)
        Me.btnRpt.Name = "btnRpt"
        Me.btnRpt.Size = New System.Drawing.Size(78, 31)
        Me.btnRpt.TabIndex = 10
        Me.btnRpt.Text = "Report"
        Me.btnRpt.UseVisualStyleBackColor = True
        '
        'btnXLS
        '
        Me.btnXLS.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnXLS.Location = New System.Drawing.Point(403, 261)
        Me.btnXLS.Name = "btnXLS"
        Me.btnXLS.Size = New System.Drawing.Size(78, 31)
        Me.btnXLS.TabIndex = 11
        Me.btnXLS.Text = "XLS"
        Me.btnXLS.UseVisualStyleBackColor = True
        '
        'lstXLS
        '
        Me.lstXLS.FormattingEnabled = True
        Me.lstXLS.Location = New System.Drawing.Point(472, 5)
        Me.lstXLS.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.lstXLS.Name = "lstXLS"
        Me.lstXLS.Size = New System.Drawing.Size(59, 43)
        Me.lstXLS.TabIndex = 12
        Me.lstXLS.Visible = False
        '
        'MonStat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 301)
        Me.Controls.Add(Me.lstXLS)
        Me.Controls.Add(Me.btnXLS)
        Me.Controls.Add(Me.btnRpt)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtEndDt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lstMonStat)
        Me.Controls.Add(Me.txtStartDt)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnExit)
        Me.Name = "MonStat"
        Me.Text = "Monthly Status"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtStartDt As System.Windows.Forms.TextBox
    Friend WithEvents lstMonStat As System.Windows.Forms.ListBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEndDt As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtAcct As System.Windows.Forms.TextBox
    Friend WithEvents btnRpt As System.Windows.Forms.Button
    Friend WithEvents btnXLS As System.Windows.Forms.Button
    Friend WithEvents lstXLS As System.Windows.Forms.ListBox
End Class
