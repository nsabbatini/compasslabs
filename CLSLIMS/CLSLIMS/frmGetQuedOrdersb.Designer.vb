﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmGetQuedOrdersb
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGetQuedOrdersb))
        Me.grp1 = New DevExpress.XtraEditors.GroupControl()
        Me.grp2 = New DevExpress.XtraEditors.GroupControl()
        Me.grp3 = New DevExpress.XtraEditors.GroupControl()
        Me.imgList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.grp4 = New DevExpress.XtraEditors.GroupControl()
        Me.grp5 = New DevExpress.XtraEditors.GroupControl()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.grp1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grp2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grp3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grp4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grp5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grp1
        '
        Me.grp1.AllowTouchScroll = True
        Me.grp1.Dock = System.Windows.Forms.DockStyle.Top
        Me.grp1.FireScrollEventOnMouseWheel = True
        Me.grp1.Location = New System.Drawing.Point(0, 0)
        Me.grp1.Name = "grp1"
        Me.grp1.Size = New System.Drawing.Size(924, 144)
        Me.grp1.TabIndex = 0
        Me.grp1.Text = "Orders needing to be approved"
        '
        'grp2
        '
        Me.grp2.Dock = System.Windows.Forms.DockStyle.Top
        Me.grp2.Location = New System.Drawing.Point(0, 144)
        Me.grp2.Name = "grp2"
        Me.grp2.Size = New System.Drawing.Size(924, 144)
        Me.grp2.TabIndex = 1
        Me.grp2.Text = "Orders needing to be printed"
        '
        'grp3
        '
        Me.grp3.Dock = System.Windows.Forms.DockStyle.Top
        Me.grp3.Location = New System.Drawing.Point(0, 288)
        Me.grp3.Name = "grp3"
        Me.grp3.Size = New System.Drawing.Size(924, 144)
        Me.grp3.TabIndex = 2
        Me.grp3.Text = "Order needing Req #s"
        '
        'imgList1
        '
        Me.imgList1.ImageStream = CType(resources.GetObject("imgList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgList1.TransparentColor = System.Drawing.Color.Transparent
        Me.imgList1.Images.SetKeyName(0, "approval.png")
        Me.imgList1.Images.SetKeyName(1, "print.png")
        Me.imgList1.Images.SetKeyName(2, "purchaser.png")
        Me.imgList1.Images.SetKeyName(3, "pickup-counter-492034.png")
        Me.imgList1.Images.SetKeyName(4, "shipping.png")
        '
        'grp4
        '
        Me.grp4.Dock = System.Windows.Forms.DockStyle.Top
        Me.grp4.Location = New System.Drawing.Point(0, 432)
        Me.grp4.Name = "grp4"
        Me.grp4.Size = New System.Drawing.Size(924, 144)
        Me.grp4.TabIndex = 3
        Me.grp4.Text = "Order needing to be picked"
        '
        'grp5
        '
        Me.grp5.Dock = System.Windows.Forms.DockStyle.Top
        Me.grp5.Location = New System.Drawing.Point(0, 576)
        Me.grp5.Name = "grp5"
        Me.grp5.Size = New System.Drawing.Size(924, 144)
        Me.grp5.TabIndex = 4
        Me.grp5.Text = "Order needing to be shipped"
        '
        'Timer1
        '
        Me.Timer1.Interval = 180000
        '
        'frmGetQuedOrdersb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(924, 726)
        Me.Controls.Add(Me.grp5)
        Me.Controls.Add(Me.grp4)
        Me.Controls.Add(Me.grp3)
        Me.Controls.Add(Me.grp2)
        Me.Controls.Add(Me.grp1)
        Me.Name = "frmGetQuedOrdersb"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Order Items Queue"
        CType(Me.grp1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grp2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grp3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grp4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grp5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grp1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grp2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grp3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents imgList1 As ImageList
    Friend WithEvents grp4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents grp5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Timer1 As Timer
End Class
