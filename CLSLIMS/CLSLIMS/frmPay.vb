﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class frmPay
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmPay_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" And CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "JONATHON.MORGAN" And CLSLIMS.UID <> "STEVE.ALEXANDER" And CLSLIMS.UID <> "KELLY.ELMORE" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpStart.Format = DateTimePickerFormat.Custom
        dtpStart.CustomFormat = "MM/dd/yyyy"
        dtpStart.Text = DateAdd(DateInterval.Month, -3, Now)
        dtpEnd.Format = DateTimePickerFormat.Custom
        dtpEnd.CustomFormat = "MM/dd/yyyy"
        dtpEnd.Text = Now
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        If rbDOS.Checked = False And rbTrans.Checked = False Then
            MsgBox("Must select date type.")
            Exit Sub
        End If
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\PayRpt.csv", False)
        Dim Title As String = "", r As String = "", p As Integer = 0, SQL As String = "", Acct As String = "", PrevAcct As String = "", cnt As Integer = 1
        file.WriteLine(",,,,,Payments by Account by Payor")
        If rbDOS.Checked Then
            Title = "Date of Service: " & dtpStart.Text & " - " & dtpEnd.Text
            SQL = "select acct,finclass,abs(sum(amount)) from lbsdata " &
                  "where str_to_date(servicedate,'%m/%d/%Y') between str_to_date('" & dtpStart.Text & "','%m/%d/%Y') and str_to_date('" & dtpEnd.Text & "','%m/%d/%Y') " &
                  "and txncodedesc in ('Medicare payment','Payment','Recoup Payment') group by acct,finclass order by acct,finclass"
        Else
            Title = "Transaction Date: " & dtpStart.Text & " - " & dtpEnd.Text
            SQL = "select acct,finclass,abs(sum(amount)) from lbsdata " &
                  "where str_to_date(posteddate,'%m/%d/%Y') between str_to_date('" & dtpStart.Text & "','%m/%d/%Y') and str_to_date('" & dtpEnd.Text & "','%m/%d/%Y') " &
                  "and txncodedesc in ('Medicare payment','Payment','Recoup Payment') group by acct,finclass order by acct,finclass"
        End If
        file.WriteLine(",,,,," & Title)
        file.WriteLine("Account,Account Name,AETNA,BCBS,CIGNA,COM,HUMANA,MC,MCD,MCDMCO,MCMCO,MD,P,TRIC,UHC,WC,Totals")
        Dim ConnMySqlFin As New MySqlConnection(CLSLIMS.strConMySQLFin)
        ConnMySqlFin.Open()
        Dim Cmd As New MySqlCommand(SQL, ConnMySqlFin)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            Acct = rs(0)
            If Acct <> PrevAcct Then
                r = Totals(r)
                If PrevAcct <> "" Then
                    file.WriteLine(r)
                    cnt = cnt + 1
                    Debug.Print(Acct & ":" & cnt)
                End If
                r = Acct & "," & GetAcctName(Acct) & ",,,,,,,,,,,,,,,,,"
                r = CLSLIMS.ReplacePiece(r, ",", FinClassPiece(rs(1)) + 2, rs(2))
                PrevAcct = Acct
            Else
                r = CLSLIMS.ReplacePiece(r, ",", FinClassPiece(rs(1)) + 2, rs(2))
            End If
        Loop
        rs.Close()
        ConnMySqlFin.Close()
        cnt = cnt + 3
        r = Totals(r)
        file.WriteLine(r)
        file.WriteLine(",,=SUM(C4:C" & cnt & "),=SUM(D4:D" & cnt & "),=SUM(E4:E" & cnt & "),=SUM(F4:F" & cnt & "),=SUM(G4:G" & cnt & "),=SUM(H4:H" & cnt & "),=SUM(I4:I" & cnt & "),=SUM(J4:J" & cnt & "),=SUM(K4:K" & cnt & "),=SUM(L4:L" & cnt & "),=SUM(M4:M" & cnt & "),=SUM(N4:N" & cnt & "),=SUM(O4:O" & cnt & "),=SUM(P4:P" & cnt & "),=SUM(Q4:Q" & cnt & ")")
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\PayRpt.csv")
        Me.Close()
        Me.Dispose()
    End Sub
    Public Function FinClassPiece(ByVal finclass As String) As Integer
        FinClassPiece = 0
        If finclass = "SP" Then finclass = "P"
        Dim FinClassstr As String = "AETNA^BCBS^CIGNA^COM^HUMANA^MC^MCD^MCDMCO^MCMCO^MD^P^TRIC^UHC^WC"
        For i = 1 To 14
            If finclass = CLSLIMS.Piece(FinClassstr, "^", i) Then
                FinClassPiece = i
                Exit Function
            End If
        Next i
    End Function
    Public Function GetAcctName(ByVal Acct) As String
        GetAcctName = Acct
        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where Acct = '" & Acct & "' order by aefdt desc"   ' and aact=1 "

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = Replace(cRS(1), ",", " ")
            Else
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Public Function Totals(ByVal str As String) As String
        Totals = str
        Dim Tot As Double = 0.00
        For i = 3 To 16
            Tot = Tot + Val(CLSLIMS.Piece(str, ",", i))
        Next i
        Totals = CLSLIMS.ReplacePiece(str, ",", 17, Tot)
    End Function
End Class