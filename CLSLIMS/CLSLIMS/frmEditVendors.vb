﻿Imports Compass.MySql.Database
Imports Compass.Global

Public Class frmEditVendors
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private dtVendors As DataTable
    Private _gridrowcount As Int16
    Private _maxVendorId As Int16
    Private Sub frmEditVendors_Load(sender As Object, e As EventArgs) Handles Me.Load


        LoadTheVendors()


    End Sub
    Private Sub LoadTheVendors()
        Dim arow As DataRow
        dtVendors = New DataTable

        dtVendors = CreateTable()
        dtVendors.TableName = "Vendors"

        Dim vendors = GetVendorDb()

        For Each vendor In vendors
            arow = dtVendors.NewRow
            arow(0) = vendor.GetType().GetProperty("VendorId")?.GetValue(vendor)
            arow(1) = vendor.GetType().GetProperty("VendorName")?.GetValue(vendor)
            arow(2) = vendor.GetType().GetProperty("VendorAbbr")?.GetValue(vendor)
            arow(3) = vendor.GetType().GetProperty("Address")?.GetValue(vendor)
            arow(4) = vendor.GetType().GetProperty("Address2")?.GetValue(vendor)
            arow(5) = vendor.GetType().GetProperty("City")?.GetValue(vendor)
            arow(6) = vendor.GetType().GetProperty("State")?.GetValue(vendor)
            arow(7) = vendor.GetType().GetProperty("Zip")?.GetValue(vendor)
            arow(8) = vendor.GetType().GetProperty("Contact")?.GetValue(vendor)
            arow(9) = vendor.GetType().GetProperty("Phone")?.GetValue(vendor)
            arow(10) = vendor.GetType().GetProperty("Fax")?.GetValue(vendor)
            arow(11) = vendor.GetType().GetProperty("Notes")?.GetValue(vendor)
            arow(12) = vendor.GetType().GetProperty("Id")?.GetValue(vendor)

            dtVendors.Rows.Add(arow)
        Next

        _gridrowcount = dtVendors.Rows.Count
        grdVendors.DataSource = dtVendors

    End Sub
    Private Function CreateTable() As DataTable

        Dim _dtVendors As DataTable = New DataTable
        _dtVendors.TableName = "Vendors"
        With _dtVendors
            .Columns.Add("VendorId", GetType(String))
            .Columns.Add("VendorName", GetType(String))
            .Columns.Add("VendorAbbr", GetType(String))
            .Columns.Add("Address", GetType(String))
            .Columns.Add("Address2", GetType(String))
            .Columns.Add("City", GetType(String))
            .Columns.Add("State", GetType(String))
            .Columns.Add("Zip", GetType(String))
            .Columns.Add("Contact", GetType(String))
            .Columns.Add("Phone", GetType(String))
            .Columns.Add("Fax", GetType(String))
            .Columns.Add("Notes", GetType(String))
            .Columns.Add("Id", GetType(Int16))
        End With

        Return _dtVendors

    End Function
    Private Function GetVendorDb() As Object

        Try

            Using getVendors As New GetInventoryDb()
                _fileName = _fileDir & VendorSqlFile
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetVendorInfo.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmEditVendors.GetVendorDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function

    Private Sub btnDeleteItem_Click(sender As Object, e As EventArgs) Handles btnDeleteItem.Click
        Dim arow As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        If arow.IsNull(12) Then
            GridView1.DeleteSelectedRows()
            grdVendors.Refresh()
            Exit Sub
        End If

        Dim x = MessageBox.Show(Me, "Are you sure you want to delete this vendor?", "Delete Vendor", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If x = DialogResult.Yes Then
            DeleteVendor(arow(12))
            LoadTheVendors()
        End If

    End Sub
    Private Function DeleteVendor(ByVal vendorId As Int16) As Boolean
        _fileName = _fileDir & DeleteVendorsSql

        Using vendors As New InventoryControl() With {._vendorId = vendorId, ._sqlText = ReadSqlFile(_fileName)}

            If vendors.DeleteVendor Then
                Return True
            Else
                Return False
            End If

        End Using

    End Function

    Private Sub btnNewVendor_Click(sender As Object, e As EventArgs) Handles btnNewVendor.Click
        GridView1.AddNewRow()

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim x = GridView1.DataRowCount
        Dim blnAddItems As Boolean = False
        Dim blnValid As Boolean = False
        Dim vendorids As New ArrayList

        _fileName = _fileDir & UpdateVendorsSql

        Try
            'update current vendors
            Using save As New SetNewVendors() With {._sqlText = ReadSqlFile(_fileName)}
                For i = 0 To _gridrowcount - 1
                    save.VendorName = CType(GridView1.GetRowCellValue(i, "VendorName")?.ToString(), String)
                    save.Address = CType(GridView1.GetRowCellValue(i, "Address")?.ToString(), String)
                    save.Address2 = CType(GridView1.GetRowCellValue(i, "Address2")?.ToString(), String)
                    save.City = CType(GridView1.GetRowCellValue(i, "City")?.ToString(), String)
                    save.State = CType(GridView1.GetRowCellValue(i, "State")?.ToString(), String)
                    save.Zip = CType(GridView1.GetRowCellValue(i, "Zip")?.ToString(), String)
                    save.Contact = CType(GridView1.GetRowCellValue(i, "Contact")?.ToString(), String)
                    save.Phone = CType(GridView1.GetRowCellValue(i, "Phone")?.ToString(), String)
                    save.Fax = CType(GridView1.GetRowCellValue(i, "Fax")?.ToString(), String)
                    save.Notes = CType(GridView1.GetRowCellValue(i, "Notes")?.ToString(), String)
                    save.vId = CType(GridView1.GetRowCellValue(i, "Id"), Int16)
                    blnValid = save.UpdateVendors()

                Next
            End Using
            'new vendor added to grid
            If x > _gridrowcount Then
                blnAddItems = True
                _fileName = _fileDir & SaveVendorsSql
                Using save As New SetNewVendors() With {._sqlText = ReadSqlFile(_fileName)}
                    For i = _gridrowcount To x - 1
                        save.VendorName = CType(GridView1.GetRowCellValue(i, "VendorName")?.ToString(), String)
                        save.Address = CType(GridView1.GetRowCellValue(i, "Address")?.ToString(), String)
                        save.Address2 = CType(GridView1.GetRowCellValue(i, "Address2")?.ToString(), String)
                        save.City = CType(GridView1.GetRowCellValue(i, "City")?.ToString(), String)
                        save.State = CType(GridView1.GetRowCellValue(i, "State")?.ToString(), String)
                        save.Zip = CType(GridView1.GetRowCellValue(i, "Zip")?.ToString(), String)
                        save.Contact = CType(GridView1.GetRowCellValue(i, "Contact")?.ToString(), String)
                        save.Phone = CType(GridView1.GetRowCellValue(i, "Phone")?.ToString(), String)
                        save.Fax = CType(GridView1.GetRowCellValue(i, "Fax")?.ToString(), String)
                        save.Notes = CType(GridView1.GetRowCellValue(i, "Notes")?.ToString(), String)
                        blnValid = save.SaveNewVendor()
                    Next
                    Dim result = GetMaxVendorId()
                    _maxVendorId = result
                End Using
            End If
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmEditVendors.btnSave_Click", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try

        If blnValid AndAlso blnAddItems = False Then
            MessageBox.Show(Me, "Vendors information has been successfully updated!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        ElseIf blnAddItems AndAlso blnValid Then
            Dim j = MessageBox.Show(Me, "The vendor has been added to the database! Do you want to add inventory items for this vendor now?", "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If j = DialogResult.Yes Then
                Using frm As New frmNewInventoryItems
                    frm.VendorId = _maxVendorId
                    frm.ShowDialog()
                End Using
                Me.Close()
            Else
                Me.Close()
            End If
        ElseIf blnValid = False Then
            MessageBox.Show(Me, "An error occured during the update, the IT team has been notified of error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Me.Close()

    End Sub
    Private Function GetMaxVendorId() As Object

        Try

            Using getVendors As New GetInventoryDb()
                _fileName = _fileDir & VendorSqlFile
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetVendorInfo.Max(Function(x) x.Id)
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmEditVendors.GetMaxVendorId", ex.StackTrace.ToString)
            End Using
        End Try


    End Function
End Class