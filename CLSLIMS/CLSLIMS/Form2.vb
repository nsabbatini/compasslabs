﻿Imports Compass.MySql.Database
Imports Compass.Global
Public Class Form2
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dtPO As DataTable = New DataTable
    Private _selectItem As String = String.Empty
    Private _subtotal As Double
    Private _total As Double
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim results = GetInventoryDb()

        grdItems.Properties.DataSource = results
        grdItems.Properties.DisplayMember = "ItemDesc"
        grdItems.Properties.ValueMember = "VId"

    End Sub
    Private Function GetInventoryDb() As Object

        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & InventorySqlItems
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.GetInventory.ToList

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function
End Class