﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SpcInq
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SpcInq))
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSpec = New System.Windows.Forms.TextBox()
        Me.lblSpecNo = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblAcctInfo = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblPatName = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblPhy = New System.Windows.Forms.Label()
        Me.lblReqNum = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblColl = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblRcv = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblMeds = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lstWrkPos = New System.Windows.Forms.ListBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lstCom = New System.Windows.Forms.ListBox()
        Me.lstOC = New System.Windows.Forms.ListBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblPhyName = New System.Windows.Forms.Label()
        Me.lstPatNameSearch = New System.Windows.Forms.ListBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lstRptDt = New System.Windows.Forms.ListBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblStatusCode = New System.Windows.Forms.Label()
        Me.lstStatusCode = New System.Windows.Forms.ListBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtOrderKey = New System.Windows.Forms.TextBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreview = New System.Windows.Forms.PrintPreviewDialog()
        Me.btnOCGrid = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnReport = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblDOB = New System.Windows.Forms.Label()
        Me.lblRetries = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(931, 499)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(85, 32)
        Me.btnExit.TabIndex = 0
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(160, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Specimen Number:"
        '
        'txtSpec
        '
        Me.txtSpec.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSpec.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpec.Location = New System.Drawing.Point(178, 6)
        Me.txtSpec.Name = "txtSpec"
        Me.txtSpec.Size = New System.Drawing.Size(181, 27)
        Me.txtSpec.TabIndex = 0
        '
        'lblSpecNo
        '
        Me.lblSpecNo.AutoSize = True
        Me.lblSpecNo.BackColor = System.Drawing.SystemColors.Window
        Me.lblSpecNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSpecNo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecNo.Location = New System.Drawing.Point(447, 12)
        Me.lblSpecNo.Name = "lblSpecNo"
        Me.lblSpecNo.Size = New System.Drawing.Size(121, 21)
        Me.lblSpecNo.TabIndex = 4
        Me.lblSpecNo.Text = "12345678910"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 19)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Account:"
        '
        'lblAcctInfo
        '
        Me.lblAcctInfo.AutoSize = True
        Me.lblAcctInfo.BackColor = System.Drawing.SystemColors.Window
        Me.lblAcctInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAcctInfo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcctInfo.Location = New System.Drawing.Point(96, 84)
        Me.lblAcctInfo.Name = "lblAcctInfo"
        Me.lblAcctInfo.Size = New System.Drawing.Size(2, 21)
        Me.lblAcctInfo.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 118)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(127, 19)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Patient Name:"
        '
        'lblPatName
        '
        Me.lblPatName.AutoSize = True
        Me.lblPatName.BackColor = System.Drawing.SystemColors.Window
        Me.lblPatName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPatName.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPatName.Location = New System.Drawing.Point(143, 118)
        Me.lblPatName.Name = "lblPatName"
        Me.lblPatName.Size = New System.Drawing.Size(2, 21)
        Me.lblPatName.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(410, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 19)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Ord Physician:"
        '
        'lblPhy
        '
        Me.lblPhy.AutoSize = True
        Me.lblPhy.BackColor = System.Drawing.SystemColors.Window
        Me.lblPhy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPhy.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhy.Location = New System.Drawing.Point(541, 86)
        Me.lblPhy.Name = "lblPhy"
        Me.lblPhy.Size = New System.Drawing.Size(2, 21)
        Me.lblPhy.TabIndex = 10
        '
        'lblReqNum
        '
        Me.lblReqNum.AutoSize = True
        Me.lblReqNum.BackColor = System.Drawing.SystemColors.Window
        Me.lblReqNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblReqNum.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReqNum.Location = New System.Drawing.Point(632, 13)
        Me.lblReqNum.Name = "lblReqNum"
        Me.lblReqNum.Size = New System.Drawing.Size(121, 21)
        Me.lblReqNum.TabIndex = 11
        Me.lblReqNum.Text = "12345678910"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(18, 179)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(165, 19)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Collect Date/Time:"
        '
        'lblColl
        '
        Me.lblColl.AutoSize = True
        Me.lblColl.BackColor = System.Drawing.SystemColors.Window
        Me.lblColl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblColl.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColl.Location = New System.Drawing.Point(189, 179)
        Me.lblColl.Name = "lblColl"
        Me.lblColl.Size = New System.Drawing.Size(2, 21)
        Me.lblColl.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(173, 19)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Receive Date/Time:"
        '
        'lblRcv
        '
        Me.lblRcv.AutoSize = True
        Me.lblRcv.BackColor = System.Drawing.SystemColors.Window
        Me.lblRcv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRcv.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRcv.Location = New System.Drawing.Point(189, 151)
        Me.lblRcv.Name = "lblRcv"
        Me.lblRcv.Size = New System.Drawing.Size(2, 21)
        Me.lblRcv.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 239)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(154, 19)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Ordering Code(s):"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(11, 208)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(126, 19)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Medication(s):"
        '
        'lblMeds
        '
        Me.lblMeds.AutoSize = True
        Me.lblMeds.BackColor = System.Drawing.SystemColors.Window
        Me.lblMeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMeds.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeds.Location = New System.Drawing.Point(143, 208)
        Me.lblMeds.Name = "lblMeds"
        Me.lblMeds.Size = New System.Drawing.Size(2, 21)
        Me.lblMeds.TabIndex = 19
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(759, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 19)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Status:"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.BackColor = System.Drawing.SystemColors.Window
        Me.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(832, 14)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(2, 21)
        Me.lblStatus.TabIndex = 21
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(18, 321)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(279, 19)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Worklist(s)           Batch          Pos"
        '
        'lstWrkPos
        '
        Me.lstWrkPos.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstWrkPos.FormattingEnabled = True
        Me.lstWrkPos.HorizontalScrollbar = True
        Me.lstWrkPos.ItemHeight = 18
        Me.lstWrkPos.Location = New System.Drawing.Point(15, 345)
        Me.lstWrkPos.Name = "lstWrkPos"
        Me.lstWrkPos.Size = New System.Drawing.Size(384, 130)
        Me.lstWrkPos.TabIndex = 23
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(438, 321)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(115, 19)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Comment(s):"
        '
        'lstCom
        '
        Me.lstCom.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstCom.FormattingEnabled = True
        Me.lstCom.ItemHeight = 16
        Me.lstCom.Location = New System.Drawing.Point(414, 343)
        Me.lstCom.Name = "lstCom"
        Me.lstCom.Size = New System.Drawing.Size(596, 132)
        Me.lstCom.TabIndex = 25
        '
        'lstOC
        '
        Me.lstOC.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstOC.FormattingEnabled = True
        Me.lstOC.ItemHeight = 18
        Me.lstOC.Location = New System.Drawing.Point(166, 239)
        Me.lstOC.Name = "lstOC"
        Me.lstOC.Size = New System.Drawing.Size(370, 58)
        Me.lstOC.TabIndex = 26
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(566, 13)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 19)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Req#:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(381, 14)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(66, 19)
        Me.Label13.TabIndex = 28
        Me.Label13.Text = "Spec#:"
        '
        'lblPhyName
        '
        Me.lblPhyName.AutoSize = True
        Me.lblPhyName.BackColor = System.Drawing.SystemColors.Window
        Me.lblPhyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPhyName.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhyName.Location = New System.Drawing.Point(445, 116)
        Me.lblPhyName.Name = "lblPhyName"
        Me.lblPhyName.Size = New System.Drawing.Size(2, 21)
        Me.lblPhyName.TabIndex = 29
        '
        'lstPatNameSearch
        '
        Me.lstPatNameSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPatNameSearch.FormattingEnabled = True
        Me.lstPatNameSearch.ItemHeight = 14
        Me.lstPatNameSearch.Location = New System.Drawing.Point(143, 445)
        Me.lstPatNameSearch.Name = "lstPatNameSearch"
        Me.lstPatNameSearch.Size = New System.Drawing.Size(717, 270)
        Me.lstPatNameSearch.TabIndex = 30
        Me.lstPatNameSearch.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(559, 179)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 19)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "Reported:"
        '
        'lstRptDt
        '
        Me.lstRptDt.Font = New System.Drawing.Font("Courier New", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstRptDt.FormattingEnabled = True
        Me.lstRptDt.ItemHeight = 16
        Me.lstRptDt.Location = New System.Drawing.Point(563, 206)
        Me.lstRptDt.Margin = New System.Windows.Forms.Padding(2)
        Me.lstRptDt.Name = "lstRptDt"
        Me.lstRptDt.Size = New System.Drawing.Size(441, 132)
        Me.lstRptDt.TabIndex = 33
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(648, 81)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(113, 19)
        Me.Label15.TabIndex = 34
        Me.Label15.Text = "Status Code:"
        '
        'lblStatusCode
        '
        Me.lblStatusCode.AutoSize = True
        Me.lblStatusCode.BackColor = System.Drawing.SystemColors.Window
        Me.lblStatusCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStatusCode.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusCode.Location = New System.Drawing.Point(757, 81)
        Me.lblStatusCode.Name = "lblStatusCode"
        Me.lblStatusCode.Size = New System.Drawing.Size(2, 21)
        Me.lblStatusCode.TabIndex = 35
        '
        'lstStatusCode
        '
        Me.lstStatusCode.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstStatusCode.FormattingEnabled = True
        Me.lstStatusCode.ItemHeight = 16
        Me.lstStatusCode.Location = New System.Drawing.Point(757, 81)
        Me.lstStatusCode.Name = "lstStatusCode"
        Me.lstStatusCode.Size = New System.Drawing.Size(261, 68)
        Me.lstStatusCode.TabIndex = 36
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(12, 38)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(93, 20)
        Me.Label16.TabIndex = 37
        Me.Label16.Text = "Order Key:"
        '
        'txtOrderKey
        '
        Me.txtOrderKey.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrderKey.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderKey.Location = New System.Drawing.Point(111, 39)
        Me.txtOrderKey.Name = "txtOrderKey"
        Me.txtOrderKey.Size = New System.Drawing.Size(181, 27)
        Me.txtOrderKey.TabIndex = 38
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(832, 499)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(85, 32)
        Me.btnPrint.TabIndex = 39
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'PrintDoc
        '
        '
        'PrintPreview
        '
        Me.PrintPreview.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreview.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreview.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreview.Enabled = True
        Me.PrintPreview.Icon = CType(resources.GetObject("PrintPreview.Icon"), System.Drawing.Icon)
        Me.PrintPreview.Name = "PrintPreview"
        Me.PrintPreview.Visible = False
        '
        'btnOCGrid
        '
        Me.btnOCGrid.Enabled = False
        Me.btnOCGrid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOCGrid.Location = New System.Drawing.Point(96, 277)
        Me.btnOCGrid.Name = "btnOCGrid"
        Me.btnOCGrid.Size = New System.Drawing.Size(64, 20)
        Me.btnOCGrid.TabIndex = 40
        Me.btnOCGrid.Text = "OC Grid"
        Me.btnOCGrid.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Location = New System.Drawing.Point(729, 499)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(85, 32)
        Me.btnEdit.TabIndex = 42
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnReport
        '
        Me.btnReport.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.Location = New System.Drawing.Point(22, 499)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(85, 32)
        Me.btnReport.TabIndex = 43
        Me.btnReport.Text = "Report"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(370, 153)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(120, 19)
        Me.Label17.TabIndex = 44
        Me.Label17.Text = "Date of Birth:"
        '
        'lblDOB
        '
        Me.lblDOB.AutoSize = True
        Me.lblDOB.BackColor = System.Drawing.SystemColors.Window
        Me.lblDOB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDOB.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDOB.Location = New System.Drawing.Point(496, 149)
        Me.lblDOB.Name = "lblDOB"
        Me.lblDOB.Size = New System.Drawing.Size(2, 21)
        Me.lblDOB.TabIndex = 45
        '
        'lblRetries
        '
        Me.lblRetries.AutoSize = True
        Me.lblRetries.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetries.Location = New System.Drawing.Point(16, 283)
        Me.lblRetries.Name = "lblRetries"
        Me.lblRetries.Size = New System.Drawing.Size(0, 13)
        Me.lblRetries.TabIndex = 46
        '
        'Button3
        '
        Me.Button3.Enabled = False
        Me.Button3.Image = Global.CLSLIMS.My.Resources.Resources.MR
        Me.Button3.Location = New System.Drawing.Point(610, 480)
        Me.Button3.Margin = New System.Windows.Forms.Padding(2)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(86, 63)
        Me.Button3.TabIndex = 47
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Enabled = False
        Me.Button2.Image = Global.CLSLIMS.My.Resources.Resources.Aff
        Me.Button2.Location = New System.Drawing.Point(504, 480)
        Me.Button2.Margin = New System.Windows.Forms.Padding(2)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 63)
        Me.Button2.TabIndex = 41
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Image = Global.CLSLIMS.My.Resources.Resources.Req
        Me.Button1.Location = New System.Drawing.Point(414, 480)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 63)
        Me.Button1.TabIndex = 31
        Me.Button1.UseVisualStyleBackColor = True
        '
        'SpcInq
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 551)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.lblRetries)
        Me.Controls.Add(Me.lblDOB)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.btnReport)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnOCGrid)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.txtOrderKey)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.lstStatusCode)
        Me.Controls.Add(Me.lblStatusCode)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lstRptDt)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblPhyName)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lstOC)
        Me.Controls.Add(Me.lstCom)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lstWrkPos)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblMeds)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblRcv)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblColl)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblReqNum)
        Me.Controls.Add(Me.lblPhy)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblPatName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblAcctInfo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblSpecNo)
        Me.Controls.Add(Me.txtSpec)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lstPatNameSearch)
        Me.Name = "SpcInq"
        Me.Text = "Specimen Inquiry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSpec As System.Windows.Forms.TextBox
    Friend WithEvents lblSpecNo As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblAcctInfo As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblPatName As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblPhy As System.Windows.Forms.Label
    Friend WithEvents lblReqNum As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblColl As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblRcv As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblMeds As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lstWrkPos As System.Windows.Forms.ListBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lstCom As System.Windows.Forms.ListBox
    Friend WithEvents lstOC As System.Windows.Forms.ListBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblPhyName As System.Windows.Forms.Label
    Friend WithEvents lstPatNameSearch As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lstRptDt As System.Windows.Forms.ListBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblStatusCode As System.Windows.Forms.Label
    Friend WithEvents lstStatusCode As ListBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtOrderKey As TextBox
    Friend WithEvents btnPrint As Button
    Friend WithEvents PrintDoc As Printing.PrintDocument
    Friend WithEvents PrintPreview As PrintPreviewDialog
    Friend WithEvents btnOCGrid As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents btnEdit As Button
    Friend WithEvents btnReport As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents lblDOB As Label
    Friend WithEvents lblRetries As Label
    Friend WithEvents Button3 As Button
End Class
