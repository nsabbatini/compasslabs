﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmNMRBatch
    Public FN As String = ""
    Private Sub btnOpen_Click(sender As Object, e As EventArgs) Handles btnOpen.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "\\cl-nas\shared\NMR\BatchTemplates\"
        openFileDialog1.Filter = "CSV files (*.csv)|*.csv"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                FN = openFileDialog1.FileName
            Catch Ex As Exception
                MessageBox.Show("Cannot read file from disk. Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open. 
            End Try
        End If
        txtFN.Text = CLSLIMS.Piece(FN, "\", CLSLIMS.NumberofPieces(FN, "\"))
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub btnBuild_Click(sender As Object, e As EventArgs) Handles btnBuild.Click

        Dim r As String = "", SpecNum As String = "", wrklst As String = "", newBatch As String = "", wrklstef As String = "", mbatch As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon), befdt As String = Format(Now, "yyyyMMddhhmmss")
        Conn.Open()
        Dim insPBI As String = "", insPB As String = "", cnt As Integer = 1
        wrklst = "NMR"
        'Get new batch number for worklist = YMMDDSEQ
        newBatch = BuildPain.GetNewBatchNum(wrklst)
        'Get current Worklist Effective Date for worklist
        wrklstef = BuildPain.WorkListEffDate(wrklst)
        'Get Sequential batch number for batch
        mbatch = BuildPain.GetMBATCH()
        Dim file As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader(FN)
        Dim CmdIns As New SqlCommand
        CmdIns.Connection = Conn
        r = file.ReadLine
        Do While Not file.EndOfStream
            r = file.ReadLine
            SpecNum = CLSLIMS.Piece(r, ",", 3)
            If CheckSpecNo(SpecNum) Then
                If CheckSpecNoQWL(SpecNum) Then
                    insPBI = "Insert into PBI (WL, BATCH, BEFDT, PBIx, PBITYPE, PSPECNO, PCONT, QCSPECNO, QCID, PBIRTXT) " &
                                 "Values ('" & wrklst & "','" & newBatch & "','" & befdt & "'," & cnt & ",1,'" & SpecNum & "','01',null,null,null)"
                    CmdIns.CommandText = insPBI
                    CmdIns.ExecuteNonQuery()
                    'CmdIns.CommandText = "Delete From QWL Where PSPECNO='" & SpecNum & "' And WL='" & wrklst & "'"
                    'CmdIns.ExecuteNonQuery()
                    cnt = cnt + 1
                End If
            Else
                'cnt = cnt + 1
            End If
        Loop
        insPB = "Insert into PB (WL, BATCH, BEFDT, BACT, PBUID, WEFDT, PBNOS, PBBQCRUN, PBEQCRUN, MBATCH, PBCLOSE, PBCLOSEID, PBCLOSEDT) " &
                "Values ('" & wrklst & "','" & newBatch & "','" & befdt & "',1,'" & CLSLIMS.UID & "','" & wrklstef & "'," & cnt - 1 & ",null,null," & mbatch & ",0,null,null)"
        CmdIns.CommandText = insPB
        CmdIns.ExecuteNonQuery()
        Conn.Close()

        MsgBox("New Batch Number: " & mbatch, MsgBoxStyle.OkOnly, "New Batch Number")
    End Sub
    Public Function CheckSpecNoQWL(ByVal specno As String) As Boolean
        Dim xSel As String = "Select PSPECNO From QWL Where PSPECNO='" & specno & "' And WL='NMR'"
        CheckSpecNoQWL = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                CheckSpecNoQWL = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Public Function CheckSpecNo(ByVal specno As String) As Boolean
        Dim xSel As String = "Select PSPECNO From PSPEC Where PSACT=1 And PSPECNO='" & specno & "'"
        CheckSpecNo = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                CheckSpecNo = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
End Class