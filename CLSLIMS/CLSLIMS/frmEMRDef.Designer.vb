﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEMRDef
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbEMR = New System.Windows.Forms.ComboBox()
        Me.txtXrefAcct = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblAcct = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lstAcctNameSearch = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFolder = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.RbtnYes = New System.Windows.Forms.RadioButton()
        Me.RbtnNo = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtExt = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(140, 19)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(111, 27)
        Me.txtAcct.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(59, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Account:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(1, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(141, 20)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "XRef Account #:"
        '
        'cmbEMR
        '
        Me.cmbEMR.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEMR.FormattingEnabled = True
        Me.cmbEMR.Items.AddRange(New Object() {"ADPH", "ADVANCEDMD", "AMAZINGCHARTS", "ATHENA", "ATLAS", "BENCHMARK", "BIZMATICS", "CARECLOUD-LIA", "CARECLOUD-RR", "CENTRICITY", "CERNER", "CPL", "CQUENTIA", "DRCHRONO", "ECW", "EHEALTHLINE", "ELATION", "EMDEON", "GREENWAY", "HEALTHFUSION", "INSYNC", "LABSOFT", "MEDCONNECT", "PRACTICEFUSION", "PRISM", "SEVOCITY", "SPRINGCHARTS", "SUCCESSEHS", "SYSTEMEDX", "TDH", "WSR"})
        Me.cmbEMR.Location = New System.Drawing.Point(140, 59)
        Me.cmbEMR.Margin = New System.Windows.Forms.Padding(2)
        Me.cmbEMR.Name = "cmbEMR"
        Me.cmbEMR.Size = New System.Drawing.Size(215, 27)
        Me.cmbEMR.TabIndex = 4
        '
        'txtXrefAcct
        '
        Me.txtXrefAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXrefAcct.Location = New System.Drawing.Point(140, 108)
        Me.txtXrefAcct.Name = "txtXrefAcct"
        Me.txtXrefAcct.Size = New System.Drawing.Size(111, 27)
        Me.txtXrefAcct.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(86, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "EMR:"
        '
        'lblAcct
        '
        Me.lblAcct.AutoSize = True
        Me.lblAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcct.Location = New System.Drawing.Point(275, 21)
        Me.lblAcct.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblAcct.Name = "lblAcct"
        Me.lblAcct.Size = New System.Drawing.Size(0, 19)
        Me.lblAcct.TabIndex = 7
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(376, 268)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 33)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(199, 268)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 33)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "SAVE"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lstAcctNameSearch
        '
        Me.lstAcctNameSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAcctNameSearch.FormattingEnabled = True
        Me.lstAcctNameSearch.ItemHeight = 14
        Me.lstAcctNameSearch.Location = New System.Drawing.Point(408, 21)
        Me.lstAcctNameSearch.Name = "lstAcctNameSearch"
        Me.lstAcctNameSearch.Size = New System.Drawing.Size(171, 46)
        Me.lstAcctNameSearch.TabIndex = 26
        Me.lstAcctNameSearch.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(74, 148)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 20)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Folder:"
        '
        'txtFolder
        '
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFolder.Location = New System.Drawing.Point(140, 145)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.Size = New System.Drawing.Size(418, 27)
        Me.txtFolder.TabIndex = 28
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(28, 224)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 20)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Include PDF:"
        '
        'RbtnYes
        '
        Me.RbtnYes.AutoSize = True
        Me.RbtnYes.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RbtnYes.Location = New System.Drawing.Point(147, 222)
        Me.RbtnYes.Name = "RbtnYes"
        Me.RbtnYes.Size = New System.Drawing.Size(56, 23)
        Me.RbtnYes.TabIndex = 30
        Me.RbtnYes.TabStop = True
        Me.RbtnYes.Text = "Yes"
        Me.RbtnYes.UseVisualStyleBackColor = True
        '
        'RbtnNo
        '
        Me.RbtnNo.AutoSize = True
        Me.RbtnNo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RbtnNo.Location = New System.Drawing.Point(209, 224)
        Me.RbtnNo.Name = "RbtnNo"
        Me.RbtnNo.Size = New System.Drawing.Size(49, 23)
        Me.RbtnNo.TabIndex = 31
        Me.RbtnNo.TabStop = True
        Me.RbtnNo.Text = "No"
        Me.RbtnNo.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(46, 187)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 20)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Extension:"
        '
        'txtExt
        '
        Me.txtExt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExt.Location = New System.Drawing.Point(140, 184)
        Me.txtExt.Name = "txtExt"
        Me.txtExt.Size = New System.Drawing.Size(76, 27)
        Me.txtExt.TabIndex = 33
        Me.txtExt.Text = "HL7"
        '
        'frmEMRDef
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 313)
        Me.Controls.Add(Me.txtExt)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.RbtnNo)
        Me.Controls.Add(Me.RbtnYes)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtFolder)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblAcct)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtXrefAcct)
        Me.Controls.Add(Me.cmbEMR)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.lstAcctNameSearch)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmEMRDef"
        Me.Text = "EMR Definition"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtAcct As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbEMR As System.Windows.Forms.ComboBox
    Friend WithEvents txtXrefAcct As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblAcct As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lstAcctNameSearch As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents RbtnYes As System.Windows.Forms.RadioButton
    Friend WithEvents RbtnNo As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtExt As System.Windows.Forms.TextBox
End Class
