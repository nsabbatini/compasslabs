﻿Imports System.IO
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class frmImpB
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnImpB_Click(sender As Object, e As EventArgs) Handles btnImpB.Click
        'Scan dir for txt files
        Dim r As String = "", Res(99) As String, Org As String = "", CT As String = "", CT2 As String = "", CT3 As String = "", CTN As Double = 100.0, CutOff As String = "", Det As String = "", EqRes As String = "", Spec As String = ""
        Dim X As String = "", Y As String = "", AmpStatus As String = "", CT2AmpStatus As String = ""
        'Dim TrayNo As String = "CV" + GetNxtTrayNo() + "-" + Format(Now, "MMddyy"), TrayPos As Integer = 1
        Dim fn As System.IO.StreamReader = Nothing, ResTyp As String = ""
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim flnme As String = ""
        If Not File.Exists("MoleDir.ini") Then
            Dim SavFileI As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("MoleDir.ini", False)
            SavFileI.WriteLine("C:\")
            SavFileI.Close()
        End If
        Dim SavFileR As StreamReader = My.Computer.FileSystem.OpenTextFileReader("MoleDir.ini")
        fd.InitialDirectory = SavFileR.ReadLine
        SavFileR.Close()
        fd.Title = "Open COVID-19 Interface File"
        fd.Filter = "text files (*.txt)|*.txt"
        fd.RestoreDirectory = True
        fd.Multiselect = False
        If fd.ShowDialog = DialogResult.OK Then
            flnme = fd.FileName
        Else
            Exit Sub
        End If
        Dim j As Integer = 0
        For i = Len(flnme) To 1 Step -1
            If Mid(flnme, i, 1) = "\" Then
                j = i
                Exit For
            End If
        Next i
        Dim SavDir As String = Mid(flnme, 1, j)
        Dim SavFileW As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("MoleDir.ini", False)
        SavFileW.WriteLine(SavDir)
        SavFileW.Close()
        lstInfo.Items.Add(flnme) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        'lstInfo.Items.Add("TrayNo: " & TrayNo) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        FileCopy(flnme, "COVIDtmp.txt")
        fn = My.Computer.FileSystem.OpenTextFileReader(flnme)
        r = fn.ReadLine
        'Loop until [Results]
        Do While InStr(r, "[Results]") = 0 And Not fn.EndOfStream
            r = fn.ReadLine : Debug.Print(r)
        Loop
        'r = fn.ReadLine  'Read Title
        Do While Not fn.EndOfStream                      '===========  Sort by Specimen number , List box? ============
            r = fn.ReadLine
            Res = Split(r, Chr(9))
            'load variables, make comparisons, make determinations
            Spec = Res(3) : If Not CheckSpecNo(Spec) Then Continue Do
            ResTyp = Res(4)
            If ResTyp = "COVID19" Then
                CT = Res(8) : Det = ""
                AmpStatus = Res(25)
                If AmpStatus = "No Amp" Then CT = "Undetermined"
                CT2 = GetNxtCT(Spec, "endo")
                CT2AmpStatus = GetNxtCTAmpStatus(Spec, "endo")
                CT3 = GetNxtCT(Spec, "IEC")
                Debug.Print(ResTyp & ", " & CT & ", " & Det & ", " & CT2 & ", " & CT3)
                If CT2 = "Undetermined" Or CT3 = "Undetermined" Or Val(CT2) > 40 Or Val(CT3) > 40 Then
                    Det = "indeterminate"
                ElseIf Val(CT2) <= 40 And Val(CT3) <= 40 Then
                    If CT = "Undetermined" Or Val(CT) > 40 Or CT2AmpStatus = "No Amp" Then
                        Det = "negative"
                    ElseIf Val(CT) <= 40 Then
                        Det = "positive"
                    Else
                        Det = "indeterminate"
                    End If
                End If
                Debug.Print(ResTyp & ", " & CT & ", " & Det & ", " & CT2 & ", " & CT3)
                lstInfo.Items.Add(CLSLIMS.LJ(Spec, 10) & CLSLIMS.RJ(ResTyp, 9) & CLSLIMS.RJ(CT, 15) & CLSLIMS.RJ(Det, 15) & CLSLIMS.RJ(CT2, 15) & CLSLIMS.RJ(CT3, 15)) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
                'Insert into PRES
                '===============================================================================================================================
                InsPRES(Spec, Det, CT2, CT3)
                '===============================================================================================================================
            End If
            Org = "" : CT = "" : EqRes = "" : Det = ""       ': TrayPos = TrayPos + 1
        Loop
        fn.Close()
        lstInfo.Items.Add("Complete!") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1

    End Sub
    Public Function GetNxtCT(ByVal spec As String, ByVal typ As String) As String
        GetNxtCT = "Undetermined"
        Dim fntmp As System.IO.StreamReader = Nothing, r As String = "", Res(99) As String
        fntmp = My.Computer.FileSystem.OpenTextFileReader("COVIDtmp.txt")
        'Loop until [Results]
        Do While InStr(r, "[Results]") = 0 And Not fntmp.EndOfStream
            r = fntmp.ReadLine
        Loop
        r = fntmp.ReadLine  'Read Title
        Do While Not fntmp.EndOfStream
            r = fntmp.ReadLine
            Res = Split(r, Chr(9))
            If spec = Res(3) And Res(4) = typ Then
                If Res(25) = "Amp" Then
                    GetNxtCT = Res(8)
                    Exit Do
                End If
            End If
        Loop
        fntmp.Close()
    End Function
    Public Function GetNxtCTAmpStatus(ByVal spec As String, ByVal typ As String) As String
        GetNxtCTAmpStatus = ""
        Dim fntmp As System.IO.StreamReader = Nothing, r As String = "", Res(99) As String
        fntmp = My.Computer.FileSystem.OpenTextFileReader("COVIDtmp.txt")
        'Loop until [Results]
        Do While InStr(r, "[Results]") = 0 And Not fntmp.EndOfStream
            r = fntmp.ReadLine
        Loop
        r = fntmp.ReadLine  'Read Title
        Do While Not fntmp.EndOfStream
            r = fntmp.ReadLine
            Res = Split(r, Chr(9))
            If spec = Res(3) And Res(4) = typ Then
                GetNxtCTAmpStatus = Res(25)
                Exit Do
            End If
        Loop
        fntmp.Close()
    End Function
    Public Sub InsPRES(ByVal Spec As String, ByVal Det As String, ByVal CT2 As String, ByVal CT3 As String)
        Dim Ins As String = "", Upd As String = "", RC As String = "", REFDT As String = "", PRSTAT As String = "", r As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Debug.Print(Spec & ", " & Det & ", " & CT2 & ", " & CT3)
        'COVID19 - Det
        '===============================================================================================================================
        r = GetRCInfo("COVID19-QL") : RC = CLSLIMS.Piece(r, "^", 1) : REFDT = CLSLIMS.Piece(r, "^", 2) : PRSTAT = CLSLIMS.Piece(r, "^", 3)
        Upd = "Update PRES Set PRSLT='" & Det & "',PRSTAT=2,PREDT='" & CLSLIMS.CDT & "',PREID='%Ins'  Where PSPECNO='" & Spec & "' And RC='" & RC & "' And PRACT=1"
        Cmd.CommandText = Upd : Cmd.ExecuteNonQuery()
        'Endogenous
        '===============================================================================================================================
        r = GetRCInfo("COVID19-ENDG") : RC = CLSLIMS.Piece(r, "^", 1) : REFDT = CLSLIMS.Piece(r, "^", 2) : PRSTAT = CLSLIMS.Piece(r, "^", 3)
        If RC <> "" Then
            Upd = "Update PRES Set PRSLT='" & CT2 & "',PRSTAT=2,PREDT='" & CLSLIMS.CDT & "',PREID='%Ins'  Where PSPECNO='" & Spec & "' And RC='" & RC & "' And PRACT=1"
            Cmd.CommandText = Upd : Cmd.ExecuteNonQuery()
        End If
        'IEC
        '===============================================================================================================================
        r = GetRCInfo("COVID19-IEC") : RC = CLSLIMS.Piece(r, "^", 1) : REFDT = CLSLIMS.Piece(r, "^", 2) : PRSTAT = CLSLIMS.Piece(r, "^", 3)
        If RC <> "" Then
            Upd = "Update PRES Set PRSLT='" & CT3 & "',PRSTAT=2,PREDT='" & CLSLIMS.CDT & "',PREID='%Ins'  Where PSPECNO='" & Spec & "' And RC='" & RC & "' And PRACT=1"
            Cmd.CommandText = Upd : Cmd.ExecuteNonQuery()
        End If
        'Cmd.CommandText = "Delete From  XTRAYS Where PSPECNO='" & Spec & "'"
        'Cmd.ExecuteNonQuery()
        'Cmd.CommandText = "Insert into XTRAYS Values ('" & trayno & "','" & Spec & "','COVID','M0070','" & CLSLIMS.UID & "',''," & traypos & ",'')"
        'Cmd.ExecuteNonQuery()
        Conn.Close()
    End Sub
    Public Function GetNxtTrayNo() As String
        GetNxtTrayNo = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim pCmd As New SqlCommand("dbo.xTNCL_NEXTVAL", Conn)
        pCmd.CommandType = CommandType.StoredProcedure
        pCmd.Parameters.Add("SYSVALKEY", SqlDbType.VarChar).Value = "CV"
        pCmd.Parameters.Add("SYSVALVAL", SqlDbType.Float).Value = 0
        pCmd.Parameters("SYSVALVAL").Direction = ParameterDirection.Output
        Try
            pCmd.ExecuteNonQuery()
        Catch
            MsgBox("x")
        End Try
        GetNxtTrayNo = CInt(pCmd.Parameters("SYSVALVAL").Value)
        pCmd.Dispose()
        GetNxtTrayNo = GetNxtTrayNo.PadLeft(2, "0")
        Conn.Close()
    End Function
    Public Function GetRCInfo(ByVal org As String) As String
        GetRCInfo = ""   'RC^REFDT^PRSTAT
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select A.RC,B.REFDT,PRSTAT From PRES A, RC B Where A.RC=B.RC And A.REFDT=B.REFDT And PRACT=1 And RABRV='" & org & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetRCInfo = rs(0) & "^" & rs(1) & "^" & rs(2)
        End If
        rs.Close()
        Conn.Close()
    End Function

    Public Function CheckSpecNo(ByVal specno As String) As Boolean
        Dim xSel As String = "Select PSPECNO From PSPEC Where PSACT=1 And PSPECNO='" & specno & "'"
        CheckSpecNo = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                CheckSpecNo = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
End Class