﻿Imports System.Net
Imports System.Text
Imports Newtonsoft.Json
Imports System.IO
Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmEVW
    Public pwn_order_num As String = ""
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnChk_Click(sender As Object, e As EventArgs) Handles btnChk.Click
        ChkKit()
    End Sub
    Private Sub ChkKit()
        'lblLastName.Text = "Smith" : lblFirstName.Text = "John" : lblDOB.Text = "03/31/1944"
        'Exit Sub
        Dim url As String = "https://secure.everlywell.com/aapi/lab_portal/v2/barcodes/" & txtKitNo.Text
        Dim x As String = "", Resp As String = "", z As String
        ServicePointManager.Expect100Continue = True
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
        Dim MyCreds As New NetworkCredential("compass", "Z3L3vtDZNbdM")
        Dim webreqRSK As HttpWebRequest = WebRequest.Create(url)
        webreqRSK.Method = "GET"
        webreqRSK.ContentType = "application/json; charset=utf-8"
        webreqRSK.Credentials = MyCreds
        Dim ResRSK As WebResponse

        Try
            ResRSK = webreqRSK.GetResponse()
            Dim stream As Stream = ResRSK.GetResponseStream()
            Dim strread As New StreamReader(stream)
            x = strread.ReadToEnd
            Resp = x.ToString
            Dim jsObj = JsonConvert.DeserializeObject(Resp)
            If jsObj("state") = "registered" Then
                lstInfo.Items.Add(txtKitNo.Text & " - " & jsObj("last_name") & ", " & jsObj("first_name") & "  " & jsObj("date_of_birth")) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
                lblLastName.Text = jsObj("last_name") : lblFirstName.Text = jsObj("first_name") : lblDOB.Text = jsObj("date_of_birth")
                Button2.Enabled = True
            Else
                lstInfo.Items.Add(txtKitNo.Text & " kit not registered.") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
                Button2.Enabled = False
            End If
        Catch exp As WebException
            z = exp.Message
            If z = "The remote server returned an error: (404) Not Found." Then
                lstInfo.Items.Add(txtKitNo.Text & " not found on EverlyWell system.")
            End If
            txtKitNo.Text = ""
            txtKitNo.Focus()
            Button2.Enabled = False
        End Try

    End Sub
    Private Sub SaveSample()
        Dim strOCs As String = "", z As String = "", k As Integer = 0, strBillTo As String = "", Q As String = Chr(34), Resp As String = ""
        Dim parOCS As String = "", NxtSpecNo As String = GetNextSpecNo()
        Dim CollDt As String = CLSLIMS.Piece(dtpCollDt.Text, "/", 3) & CLSLIMS.Piece(dtpCollDt.Text, "/", 1) & CLSLIMS.Piece(dtpCollDt.Text, "/", 2)
        Dim DOB As String = CLSLIMS.Piece(lblDOB.Text, "-", 3) & CLSLIMS.Piece(lblDOB.Text, "-", 1) & CLSLIMS.Piece(lblDOB.Text, "-", 2)
        strBillTo = "C"
        strOCs = "M0070"
        parOCS = strOCs
        Debug.Print(strOCs)
        'Print two standard labels and two small labels
        QuickOE.PrintSampLabel(NxtSpecNo)
        QuickOE.PrintSampLabel(NxtSpecNo)
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Dim strResponse As String
        '==========================================================================================================================================================================================================================
        'Save in Apollo
        '==========================================================================================================================================================================================================================
        Dim proc As String = "dbo.xTNCL_QO"
        Dim pCmd As New SqlCommand(proc, Conn)
        pCmd.CommandType = CommandType.StoredProcedure
        pCmd.Parameters.Add("PSPECNO", SqlDbType.VarChar).Value = NxtSpecNo
        pCmd.Parameters.Add("ACCT", SqlDbType.VarChar).Value = "M1472"
        pCmd.Parameters.Add("OCS", SqlDbType.VarChar).Value = strOCs
        pCmd.Parameters.Add("UID", SqlDbType.VarChar).Value = CLSLIMS.UID
        pCmd.Parameters.Add("CDT", SqlDbType.VarChar).Value = CLSLIMS.CDT
        pCmd.Parameters.Add("COLLDT", SqlDbType.VarChar).Value = CollDt
        pCmd.Parameters.Add("COLLTM", SqlDbType.VarChar).Value = ""
        pCmd.Parameters.Add("BILLTO", SqlDbType.VarChar).Value = strBillTo
        pCmd.Parameters.Add("REQPHY", SqlDbType.VarChar).Value = ""
        pCmd.Parameters.Add("ADSQ1", SqlDbType.Int).Value = 1
        pCmd.Parameters.Add("ADSQ3", SqlDbType.Int).Value = 1
        pCmd.Parameters.Add("SPTYP", SqlDbType.VarChar).Value = "SWAB"
        pCmd.Parameters.Add("RESPONSE", SqlDbType.VarChar).Value = ""
        pCmd.Parameters.Add("ERRMSG", SqlDbType.VarChar).Value = ""
        pCmd.Parameters("RESPONSE").Direction = ParameterDirection.Output
        pCmd.Parameters("ERRMSG").Direction = ParameterDirection.Output
        pCmd.ExecuteNonQuery()
        strResponse = IIf(IsDBNull(pCmd.Parameters("RESPONSE").Value), "", pCmd.Parameters("RESPONSE").Value)
        pCmd.Dispose()

        '==========================================================================================================================================================================================================================
        'Submit kit for processing to EverlyWell
        Dim url As String = "https://secure.everlywell.com/aapi/lab_portal/v2/barcodes/" & txtKitNo.Text
        ServicePointManager.Expect100Continue = True
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
        Dim MyCreds As New NetworkCredential("compass", "Z3L3vtDZNbdM")
        Dim webreqSKP As HttpWebRequest = WebRequest.Create(url)
        webreqSKP.Method = "PUT"
        webreqSKP.ContentType = "application/json"
        webreqSKP.Credentials = MyCreds
        Try
            Dim streamSKP As Stream = webreqSKP.GetRequestStream()
            streamSKP.Close()
            Dim RespSKP As WebResponse = webreqSKP.GetResponse
            Dim stream As Stream = RespSKP.GetResponseStream()
            Dim strread As New StreamReader(stream)
            z = strread.ReadToEnd
            Resp = z.ToString
        Catch exp As WebException
            z = exp.Message
            If z = "The remote server returned an error: (404) Not Found." Then
                lstInfo.Items.Add(txtKitNo.Text & " not found on EverlyWell system.")
            End If
        End Try
        '==========================================================================================================================================================================================================================
        'Look for pwn_order_number
        '==========================================================================================================================================================================================================================
        Dim RespObj As Object = JsonConvert.DeserializeObject(Resp)
        pwn_order_num = RespObj("pwn_order_number")
        If pwn_order_num <> "" Then

        Else
            lstInfo.Items.Add("Problem submitting the kit For processing With EverlyWell.")
        End If
        '==========================================================================================================================================================================================================================
        'Create PDEM, Update PSPEC and PORD
        Dim PORD As String = "", POEFDT As String = ""
        Dim PUID As String = QuickOE.GetNextPUID()
        Dim PEFDT As String = CLSLIMS.CDT
        '"Insert into PDEM (PUID, PACT, PEFDT, PFNAME, PMNAME, PLNAME, PDOB, SEX, PAD1, PAD2, PCITY, PSTATE, PZIP, PPHONE, PBILLTO) Values ('" & PUID & "',1,'" & PEFDT & "','" & First & "','" & MI &
        '          "','" & Last & "','" & DOB & "','" & Gender & "','" & Addr1 & "','" & Addr2 & "','" & City & "','" & ST & "','" & ZIP & "','" & PPhone & "','I')"
        Cmd.CommandText = "Insert into PDEM (PUID, PACT, PEFDT, PFNAME, PMNAME, PLNAME, PDOB, SEX, PAD1, PAD2, PCITY, PSTATE, PZIP, PPHONE, PBILLTO) Values ('" & PUID & "',1,'" & PEFDT & "','" & lblFirstName.Text & "','" & "" &
                  "','" & lblLastName.Text & "','" & DOB & "','" & "" & "','" & "" & "','" & "" & "','" & "" & "','" & "" & "','" & "" & "','" & "" & "','I')"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update PSPEC Set PSTAT='2',PUID='" & PUID & "', PEFDT='" & PEFDT & "',PSID3='" & pwn_order_num & "',PSID2='" & txtKitNo.Text & "' Where PSPECNO='" & NxtSpecNo & "' And PSACT=1"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Select PORD, POEFDT From PORD Where PORD+POEFDT=(Select PORD+POEFDT From PSPEC Where PSPECNO='" & NxtSpecNo & "' And PSACT=1) And POACT=1"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            PORD = rs(0)
            POEFDT = rs(1)
        End If
        rs.Close()
        Cmd.CommandText = "Update PORD Set PUID='" & PUID & "', PEFDT='" & PEFDT & "', POBILLTO='" & "C" & "' Where PORD='" & PORD & "' And POEFDT='" & POEFDT & "' And POACT=1"
        Cmd.ExecuteNonQuery()
        '==========================================================================================================================================================================================================================
        lstInfo.Items.Add("Specimen number: " & NxtSpecNo & "    PWN Order number: " & pwn_order_num)
        'Clear fields
        txtKitNo.Text = "" : pwn_order_num = "" : lblDOB.Text = "" : lblFirstName.Text = "" : lblLastName.Text = ""
        txtKitNo.Focus()

    End Sub
    Private Function GetNextSpecNo() As String
        GetNextSpecNo = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select MAX(PSPECNO) + 1 From PSPEC Where PSACT = 1 And Substring(PSPECNO, 1, 2) ='51' And Len(PSPECNO)=7"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetNextSpecNo = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Private Sub txtKitNo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtKitNo.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 9 Then
            Exit Sub
        End If
        ChkKit()
    End Sub

    Private Sub frmEVW_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpCollDt.Format = DateTimePickerFormat.Custom
        dtpCollDt.CustomFormat = "MM/dd/yyyy"
        dtpCollDt.Text = DateAdd(DateInterval.Day, -1, Now)
        lblLastName.Text = ""
        lblFirstName.Text = ""
        lblDOB.Text = ""
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        SaveSample()
    End Sub
End Class