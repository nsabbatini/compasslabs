﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmEMRDef

    Private Sub txtAcct_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtAcct.Text = UCase(txtAcct.Text)
            FillAcct()
        End If

    End Sub
    Private Sub FillAcct()

        If Not IsNumeric(Mid(txtAcct.Text, 2)) And Len(txtAcct.Text) > 1 And txtAcct.Text <> "PTSAMPLES" Then
            txtAcct.Text = GetAcctFromName(txtAcct.Text)
            Exit Sub
        End If
        If txtAcct.Text = "" Then Exit Sub
        Dim Sel As String = "Select EMRNAME,XREFACCT,RPTSUBDIR,ANAME,INCLUDEPDF,EXT From XACCTHL7 A, ACCT B Where A.ACCT=B.ACCT And AACT=1 And A.ACCT='" & txtAcct.Text & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            lblAcct.Text = txtAcct.Text & " - " & rs(3)
            txtFolder.Text = rs(2)
            cmbEMR.Text = rs(0)
            txtXrefAcct.Text = rs(1)
            txtExt.Text = rs(5)
            If rs(4) Then
                RbtnYes.Checked = True
            Else
                RbtnNo.Checked = True
            End If
        Else
            rs.Close()
            Cmd.CommandText = "Select ANAME From ACCT Where AACT=1 And ACCT='" & txtAcct.Text & "'"
            rs = Cmd.ExecuteReader()
            If rs.Read Then
                lblAcct.Text = txtAcct.Text & " - " & rs(0)
            Else
                MsgBox("Invalid Account")
                txtAcct.Text = ""
            End If
        End If
        rs.Close()
        Conn.Close()
    End Sub
    Private Sub lstAcctNameSearch_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles lstAcctNameSearch.MouseDoubleClick
        txtAcct.Text = CLSLIMS.Piece(lstAcctNameSearch.SelectedItem.ToString, " ", 1)
        lstAcctNameSearch.Visible = False
        FillAcct()
    End Sub
    Private Function GetAcctFromName(ByVal AcctName As String) As String

        Dim xSel As String
        xSel = "select acct,aname " & _
               "from acct " & _
               "where upper(aname) like '" & UCase(AcctName) & "%' and aact=1 " & _
               "order by aname"

        Dim strAcct As String = ""
        GetAcctFromName = ""
        If AcctName = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strAcct = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 30)
                lstAcctNameSearch.Items.Add(strAcct)
            Loop
            If lstAcctNameSearch.Items.Count > 0 Then
                lstAcctNameSearch.Visible = True
                lstAcctNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function

    Private Sub frmEMRDef_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim Sel As String = "Select ACCT From ACCT Where ACCT='" & txtAcct.Text & "' And AACT=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Dim iPDF As Integer = 0
        Conn.Open()
        Dim CmdIns As New SqlCommand
        CmdIns.Connection = Conn
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If Not rs.Read() Then
            MsgBox("Invalid Account")
        End If
        rs.Close()
        If RbtnYes.Checked = True Then
            iPDF = 1
        Else
            iPDF = 0
        End If
        Cmd.CommandText = "Select * From XACCTHL7 Where ACCT='" & txtAcct.Text & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            CmdIns.CommandText = "Update XACCTHL7 Set EMRNAME='" & cmbEMR.Text & "', RPTSUBDIR='" & txtFolder.Text & "', XREFACCT='" & txtXrefAcct.Text & "', INCLUDEPDF=" & iPDF & ",EXT='" & txtExt.Text & "' Where ACCT='" & txtAcct.Text & "'"
        Else
            CmdIns.CommandText = "Insert Into XACCTHL7 Values ('" & txtAcct.Text & "','" & cmbEMR.Text & "','" & txtXrefAcct.Text & "','" & txtFolder.Text & "'," & iPDF & ",'" & txtExt.Text & "')"
        End If
        CmdIns.ExecuteNonQuery()
        rs.Close()
        Cmd.Dispose()
        txtAcct.Text = ""
        txtAcct.Focus()
        cmbEMR.Text = ""
        txtXrefAcct.Text = ""
        lblAcct.Text = ""
        txtFolder.Text = ""
        txtExt.Text = "HL7"
        RbtnYes.Checked = True
    End Sub

    Private Sub cmbEMR_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbEMR.SelectedIndexChanged
        If txtFolder.Text = "" Then
            Select Case cmbEMR.Text
                Case "ADPH" : txtFolder.Text = "E:\COMPASS\HL7\ADPH\Results\" : RbtnYes.Checked = False
                Case "ADVANCEDMD" : txtFolder.Text = "E:\COMPASS\HL7\ADVANCEDMD\" : RbtnYes.Checked = True
                Case "AMAZINGCHARTS" : txtFolder.Text = "E:\COMPASS\HL7\AMAZINGCHARTS\" : RbtnYes.Checked = False
                Case "ATHENA" : txtFolder.Text = "E:\COMPASS\HL7\ATHENA\" : RbtnYes.Checked = False
                Case "ATLAS" : txtFolder.Text = "M:\Compass\HL7\ATLAS\RESULTS\" : RbtnYes.Checked = True
                Case "CARECLOUD-RR" : txtFolder.Text = "E:\Compass\HL7\CareCloud-RR\Results\" : RbtnYes.Checked = True
                Case "CARECLOUD-LIA" : txtFolder.Text = "M:\Compass\HL7\Liaison\Results\" : RbtnYes.Checked = True
                Case "CENTRICITY" : txtFolder.Text = "M:\Compass\HL7\Centricity\Results\" : RbtnYes.Checked = True
                Case "CERNER" : txtFolder.Text = "E:\Compass\HL7\Cerner\Results\" : RbtnYes.Checked = True
                Case "CPL" : txtFolder.Text = "E:\COMPASS\HL7\CPL\" : RbtnYes.Checked = False
                Case "EHEALTHLINE" : txtFolder.Text = "M:\Compass\HL7\SFH\Prod\Results\"
                Case "EMDEON" : txtFolder.Text = "E:\COMPASS\HL7\Emdeon\" : RbtnYes.Checked = True
                Case "ELATION" : txtFolder.Text = "M:\COMPASS\HL7\Elation\" : RbtnYes.Checked = True
                Case "GREENWAY" : txtFolder.Text = "M:\COMPASS\HL7\GREENWAY\RESULTS\" : RbtnYes.Checked = True
                Case "HEALTHFUSION" : txtFolder.Text = "E:\COMPASS\HL7\HEALTHFUSION\" : RbtnYes.Checked = True
                Case "LABSOFT" : txtFolder.Text = "E:\COMPASS\HL7\LABSOFT\" : RbtnYes.Checked = True
                Case "MEDCONNECT" : txtFolder.Text = "E:\COMPASS\HL7\MEDCONNECT\" : RbtnYes.Checked = False
                Case "PRACTICEFUSION" : txtFolder.Text = "E:\PF HL7\HOLDING\" : RbtnYes.Checked = True
                Case "BENCHMARK" : txtFolder.Text = "E:\COMPASS\HL7\BENCHMARK\"
                Case "SEVOCITY" : txtFolder.Text = "E:\COMPASS\HL7\SEVOCITY\" : RbtnYes.Checked = True
                Case "BIZMATICS" : txtFolder.Text = "M:\Compass\HL7\718\Results\" : RbtnYes.Checked = True
                Case "WSR" : txtFolder.Text = "E:\Compass\HL7\WaitingRoomSolutions\Results\" : RbtnYes.Checked = True
                Case "INSYNC" : txtFolder.Text = "E:\Compass\HL7\Insync\Results\" : RbtnYes.Checked = True
                Case "SPRINGCHARTS" : txtFolder.Text = "E:\Compass\HL7\SpringCharts\" : RbtnYes.Checked = False
                Case "SUCCESSEHS" : txtFolder.Text = "E:\Compass\HL7\" : RbtnYes.Checked = False
                Case "SYSTEMEDX" : txtFolder.Text = "E:\Compass\HL7\Systemedx\Results\" : RbtnYes.Checked = False
                Case "ECW" : txtFolder.Text = "E:\Compass\HL7\ECW\Results\" : RbtnYes.Checked = True
                Case "DRCHRONO" : txtFolder.Text = "E:\Compass\HL7\HealthGorilla (Dr.Chrono)\Results\" : RbtnYes.Checked = True
                Case "PRISM" : txtFolder.Text = "E:\COMPASS\HL7\PRISM\" : RbtnYes.Checked = False
                Case "TDH" : txtFolder.Text = "E:\COMPASS\HL7\TDH\Results\" : RbtnYes.Checked = False
            End Select
        End If
    End Sub
End Class