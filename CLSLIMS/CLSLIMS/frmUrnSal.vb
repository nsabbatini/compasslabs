﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Public Class frmUrnSal
    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Call RunSQL()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = True

        PrintDoc.Print()
        Me.Close()

    End Sub
    Public Sub RunSQL()
        Dim xSel As String, RecvDate As String = Mid(dtpProfDt.Text, 7, 4) & Mid(dtpProfDt.Text, 1, 2) & Mid(dtpProfDt.Text, 4, 2), x As String = ""
        Dim Cnt As Integer = 0
        xSel = "Select pspecno from PSPEC Where PSACT=1 And substring(psrcvdt,1,8)='" & RecvDate & "'"
        lstData.Items.Clear()
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            lstData.Items.Add(CLSLIMS.LJ("Spec Num 1", 10) & Space(2) & CLSLIMS.LJ("Spec Num 2", 10) & Space(2) & CLSLIMS.LJ("Date of Service", 15) & Space(2) & CLSLIMS.LJ("Date of Birth", 13) & Space(2) & CLSLIMS.LJ("Sex", 3) & Space(2) & CLSLIMS.LJ("Patient Name", 50))
            lstData.Items.Add(CLSLIMS.LJ("==========", 10) & Space(2) & CLSLIMS.LJ("==========", 10) & Space(2) & CLSLIMS.LJ("===============", 15) & Space(2) & CLSLIMS.LJ("=============", 13) & Space(2) & CLSLIMS.LJ("===", 3) & Space(2) & CLSLIMS.LJ("============", 50))
            Do While cRS.Read
                If cRS(0) = "1262057" Then
                    Cnt = Cnt
                End If
                x = CheckForDupe(cRS(0))
                Do While x = "Redo" : x = CheckForDupe(cRS(0)) : Loop
                If x <> "" Then
                    lstData.Items.Add(CLSLIMS.LJ(cRS(0), 10) & Space(2) & CLSLIMS.LJ(CLSLIMS.Piece(x, "^", 1), 10) & Space(2) & CLSLIMS.LJ(CLSLIMS.Piece(x, "^", 2), 15) & Space(2) & CLSLIMS.LJ(CLSLIMS.Piece(x, "^", 3), 13) & Space(2) & CLSLIMS.LJ(CLSLIMS.Piece(x, "^", 4), 3) & Space(2) & CLSLIMS.LJ(CLSLIMS.Piece(x, "^", 5) & ", " & CLSLIMS.Piece(x, "^", 6), 50))
                End If
                Cnt = Cnt + 1
                If Cnt Mod 20 = 0 Then Debug.Print(Cnt)
            Loop
            cRS.Close()
            Conn.Close()
        End Using
        If lstData.Items.Count = 2 Then
            lstData.Items.Add("No Duplicates")
        End If
    End Sub
    Public Function CheckForDupe(ByVal specno As String) As String
        CheckForDupe = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand
            Cmd.Connection = Conn
            Cmd.CommandText = "Select PSPECNO,pscdta,pdob,sex,plname,pfname From PSPEC a, PDEM b Where a.PUID=b.PUID and a.PEFDT=b.PEFDT and psact=1 and pact=1 and pscdta+pdob+sex+plname = (Select distinct pscdta+pdob+sex+plname From PSPEC a, PDEM b Where a.PUID=b.PUID and a.PEFDT=b.PEFDT And PSACT=1 and PSPECNO='" & specno & "') and pspecno != '" & specno & "'"
            Dim rs As SqlDataReader = Nothing
            Try
                rs = Cmd.ExecuteReader
                If rs.Read Then
                    CheckForDupe = rs(0) & "^" & Mid(rs(1), 5, 2) & "/" & Mid(rs(1), 7, 2) & "/" & Mid(rs(1), 1, 4) & "^" & Mid(rs(2), 5, 2) & "/" & Mid(rs(2), 7, 2) & "/" & Mid(rs(2), 1, 4) & "^" & rs(3) & "^" & rs(4) & "^" & rs(5)
                End If
                rs.Close()
            Catch ex As Exception
                If Mid(ex.Message, 1, 26) = "Execution Timeout Expired." Then
                    CheckForDupe = "Redo"
                    Debug.Print("Redo " & specno)
                End If
            End Try
            Conn.Close()
        End Using
    End Function
    Private Sub PrintDoc_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim y As Integer = 190
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold Or FontStyle.Underline)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font6 As New Font("Tacoma", 14, FontStyle.Bold Or FontStyle.Underline)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        'Header
        e.Graphics.DrawString("Duplicate Patients on Same Date of Service", Font2, Brushes.Black, 130, 50)
        'e.Graphics.DrawString(txtAcct.Text & ": " & lblAcct.Text, Font6, Brushes.Black, 50, 100)
        e.Graphics.DrawString("Date Received: " & dtpProfDt.Text, Font3, Brushes.Black, 90, 125)
        e.Graphics.DrawString("Date Printed: " & Format(Now, "MM/dd/yyyy hh:mm tt"), Font3, Brushes.Black, 490, 125)
        'Detail
        For i = 0 To lstData.Items.Count - 1
            e.Graphics.DrawString(lstData.Items(i).ToString, Font5, Brushes.Black, 80, y)
            y = y + 20
        Next i
        'Footer
        'e.Graphics.DrawString("Checked/Approved by: _________________________", Font5, Brushes.Black, 150, y + 60)
        'e.Graphics.DrawString("               Date: _________________________", Font5, Brushes.Black, 150, y + 90)
    End Sub

    Private Sub frmUrnSal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpProfDt.Format = DateTimePickerFormat.Custom
        dtpProfDt.CustomFormat = "MM/dd/yyyy"
        dtpProfDt.Text = Now
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click
        Call RunSQL()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = True

        PrintPrev.ShowDialog()
        Me.Close()
    End Sub
End Class