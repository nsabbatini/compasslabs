﻿Imports System.Data.SqlClient

Public Class frmAcctDef
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub txtAcct_KeyUp(sender As Object, e As KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtAcct.Text = UCase(txtAcct.Text)
            lblAcctName.Text = CLSLIMS.GetAcctName(txtAcct.Text)
            If lblAcctName.Text = "" Then
                MsgBox("Invalid account.")
                Exit Sub
            Else
                cbSales.Text = GetSalesman(txtAcct.Text)
                cbProc.Text = GetProc(txtAcct.Text)
                cbProc2.Text = GetProc2(txtAcct.Text)
                dtpRenewal.Text = GetRenewal(txtAcct.Text)
            End If
        End If
    End Sub

    Public Function GetSalesman(ByVal acct As String) As String
        GetSalesman = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select asalesrg,syslistopt from acct,syslist where aact=1 and asalesrg=syslistval and syslistkey='ASALESRG' and acct='" & txtAcct.Text & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetSalesman = rs(0) & "-" & rs(1)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function GetProc(ByVal acct As String) As String
        GetProc = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select amisc1,syslistopt from acct,syslist where aact=1 and amisc1=syslistval and syslistkey='PROC' and acct='" & txtAcct.Text & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetProc = rs(0) & "-" & rs(1)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function GetProc2(ByVal acct As String) As String
        GetProc2 = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select amisc3,syslistopt from acct,syslist where aact=1 and amisc3=syslistval and syslistkey='PROC' and acct='" & txtAcct.Text & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetProc2 = rs(0) & "-" & rs(1)
        End If
        rs.Close()
        Conn.Close()
    End Function

    Public Function GetRenewal(ByVal acct As String) As String
        GetRenewal = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select amisc2 from acct where aact=1 and acct='" & txtAcct.Text & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetRenewal = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Private Sub frmAcctDef_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpRenewal.Format = DateTimePickerFormat.Custom
        dtpRenewal.CustomFormat = "MM/dd/yyyy"
        dtpRenewal.Text = Now
        ClearAndLoad()
    End Sub

    Public Sub ClearAndLoad()
        cbSales.Items.Clear()
        cbProc.Items.Clear()
        cbProc2.Items.Clear()
        dtpRenewal.Text = Now
        txtAcct.Text = ""
        dtpRenewal.Text = ""
        cbSales.Text = ""
        cbProc.Text = ""
        cbProc2.Text = ""
        lblAcctName.Text = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select syslistval,syslistopt from syslist where syslistkey='ASALESRG' order by syslistopt"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cbSales.Items.Add(rs(0) & "-" & rs(1))
        Loop
        rs.Close()
        Cmd.CommandText = "Select syslistval,syslistopt from syslist where syslistkey='PROC' order by syslistopt"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cbProc.Items.Add(rs(0) & "-" & rs(1))
            cbProc2.Items.Add(rs(0) & "-" & rs(1))
        Loop
        rs.Close()
        Conn.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtAcct.Text = "" Then
            MsgBox("Missing account.")
            Exit Sub
        End If
        If cbSales.Text = "" Then
            MsgBox("Missing Salesman.")
            Exit Sub
        End If
        If cbProc.Text = cbProc2.Text And cbProc.Text <> "" Then
            MsgBox("Processor 1 and Processor 2 must be different.")
            Exit Sub
        End If

        Dim AcctEffdt As String = CLSLIMS.CDT, PrevAcctEffdt As String = ""
        Dim InsCmd As String = "", UpdCmd As String = ""
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Dim rs As SqlDataReader
        Cmd.Connection = conn
        Cmd.CommandText = "Select AEFDT From ACCT Where ACCT='" & txtAcct.Text & "' And AACT=1"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            PrevAcctEffdt = rs(0)
        End If
        rs.Close()
        InsCmd = "Insert into ACCT Select ACCT, '" & AcctEffdt & "', AACT, AABRV, ANAME, ANAME2, AAD1, AAD2, ACITY, ACNTY, ASTATE, ACNTRY, AZIP, APHONE, AFAX, ACONTACT, AEMAIL, ASPRFX, AMRO, AMROA, '" & CLSLIMS.Piece(cbSales.Text, "-", 1) & "', ATYPE, AAVRSPIN, AMACCT, ABACCT, ADFLTREQ, ADFLTRFMT, ABCONT, AOCREST, ARQUAN, BFEE, APOFEE, ABUNDLE, BCFEE, BMFEE,'" & CLSLIMS.UID & "', AOCORDER, ABILLTO, '" & CLSLIMS.Piece(cbProc.Text, "-", 1) & "','" & dtpRenewal.Text & "','" & CLSLIMS.Piece(cbProc2.Text, "-", 1) & "' " &
                               "From ACCT Where ACCT='" & txtAcct.Text & "' And AACT=1"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        UpdCmd = "Update ACCT Set AACT=0 Where ACCT='" & txtAcct.Text & "' And AACT=1 And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = UpdCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTMC Select ACCT, '" & AcctEffdt & "', ACCTMCx, MC From ACCTMC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTPHY Select ACCT, '" & AcctEffdt & "', PHY From ACCTPHY Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTRPQUE Select ACCT, '" & AcctEffdt & "', ACCTRPQUEx, ARACCT, ARPTTO, ANRPT, AARPT, RPQUE, PHY, ARPRIMARY From ACCTRPQUE Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTOC Select ACCT, '" & AcctEffdt & "', ACCTOCx, OC, AAUTOORD From ACCTOC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTLOC Select ACCT, '" & AcctEffdt & "', ACCTLOCx, ALOC, ALOCAD1, ALOCAD2, ALOCCITY, ALOCSTATE, ALOCZIP, ALOCPHONE From ACCTLOC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()

        ClearAndLoad()
        txtAcct.Focus()
    End Sub
End Class