﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports Excel = Microsoft.Office.Interop.Excel

Public Class AcctSum
    Public TotSpec As Integer = 0, SpecType As String = "", NumRej As Integer = 0
    Private Sub txtAcct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.Enter Then
            ValidateAcct()
        End If
    End Sub
    Public Sub ValidateAcct()
        If Not IsNumeric(Mid(txtAcct.Text, 2)) And Len(txtAcct.Text) > 1 And txtAcct.Text <> "PTSAMPLES" Then
            txtAcct.Text = GetAcctFromName(txtAcct.Text)
            Exit Sub
        End If
        lblAcct.Text = GetAcctName(txtAcct.Text)
        If lblAcct.Text = "" Then
            MsgBox("Invalid Account")
            txtAcct.Focus()
        Else
            txtStartDt.Focus()
        End If

    End Sub

    Private Sub lstAcctNameSearch_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstAcctNameSearch.MouseDoubleClick
        txtAcct.Text = CLSLIMS.Piece(lstAcctNameSearch.SelectedItem.ToString, " ", 1)
        lblAcct.Text = GetAcctName(txtAcct.Text)
        If lblAcct.Text = "" Then
            MsgBox("Invalid Account")
            txtAcct.Focus()
        Else
            txtStartDt.Focus()
        End If
        lstAcctNameSearch.Visible = False

    End Sub
    Public Function GetAcctName(ByVal Acct) As String
        GetAcctName = ""
        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where Acct = '" & txtAcct.Text & "'"   ' and aact=1 "

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(1)
            Else
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Private Function GetAcctFromName(ByVal AcctName As String) As String

        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where upper(aname) like '" & UCase(AcctName) & "%'" &
               "order by aname"
        ' and aact=1 " & _

        Dim strAcct As String = ""
        GetAcctFromName = ""
        If AcctName = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strAcct = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 30)
                lstAcctNameSearch.Items.Add(strAcct)
            Loop
            If lstAcctNameSearch.Items.Count > 0 Then
                lstAcctNameSearch.Visible = True
                lstAcctNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Private Sub txtStartDt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartDt.KeyPress

        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtStartDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtStartDt.Text) Then
                MsgBox("Invalid Start Date.")
                txtStartDt.Focus()
            Else
                txtEndDt.Focus()
            End If
        End If
    End Sub
    Private Sub txtEndDt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndDt.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtEndDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtEndDt.Text) Then
                MsgBox("Invalid End Date.")
                txtEndDt.Focus()
            End If
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRun.Click
        Dim dFirst As Date, dLast As Date, sFirst As String, sLast As String
        dFirst = CDate(txtStartDt.Text) : dLast = CDate(txtEndDt.Text)
        'Dim CretTot As Integer, pHTot As Integer, SpGrTot As Integer, OxiTot As Integer
        Dim Cnt As Integer = 0, xClass As String
        lstData.Items.Clear() : lstXLS.Items.Clear() : lstClass.Items.Clear()
        sFirst = DatePart(DateInterval.Year, dFirst) & IIf(DatePart(DateInterval.Month, dFirst) < 10, "0", "") & DatePart(DateInterval.Month, dFirst) & IIf(DatePart(DateInterval.Day, dFirst) < 10, "0", "") & DatePart(DateInterval.Day, dFirst)
        sLast = DatePart(DateInterval.Year, dLast) & IIf(DatePart(DateInterval.Month, dLast) < 10, "0", "") & DatePart(DateInterval.Month, dLast) & IIf(DatePart(DateInterval.Day, dLast) < 10, "0", "") & DatePart(DateInterval.Day, dLast)
        lstData.Items.Add("   Account: " & txtAcct.Text & " - " & lblAcct.Text) ': lstXLS.Items.Add("Account:" & "," & txtAcct.Text & "," & lblAcct.Text)
        lstData.Items.Add("Start Date: " & dFirst) ': lstXLS.Items.Add("Start Date:" & "," & dFirst)
        lstData.Items.Add("  End Date: " & dLast) ': lstXLS.Items.Add("End Date:" & "," & dLast)
        lstData.Items.Add("") ': lstXLS.Items.Add(" ")
        lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        Dim Sel As String
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        'Total number of specimens
        Sel = "select count(distinct a.pspecno) " &
        "from pspec a, pord b " &
        "where a.pord = b.pord And a.poefdt = b.poefdt And psact = 1 And poact = 1 " &
        "and substring(a.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " &
        "and acct='" & txtAcct.Text & "' "
        Cmd.CommandText = Sel
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            lstData.Items.Add("Total specimens: " & rs(0)) ': lstXLS.Items.Add("Total Specimens:" & "," & rs(0))
            TotSpec = Val(rs(0))
        End If
        rs.Close()
        lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        'Number of patients
        'Sel = "select count(distinct a.puid) " &
        '"from pspec a, pord b " &
        '"where a.pord = b.pord And a.poefdt = b.poefdt And psact = 1 And poact = 1 " &
        '"and substring(a.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " &
        '"and acct='" & txtAcct.Text & "' "
        'Cmd.CommandText = Sel
        'rs = Cmd.ExecuteReader()
        'If rs.Read Then
        '    lstData.Items.Add("") : lstXLS.Items.Add(" ")
        '    lstData.Items.Add("Number of patients: " & rs(0)) : lstXLS.Items.Add("Number of Patients:" & "," & rs(0))
        '    lstData.Items.Add("") : lstXLS.Items.Add(" ")
        'End If
        'rs.Close()
        'lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        'Specimen Types
        Sel = "select stdesc,count(a.pspecno) " &
        "from pspec a, pord b, pcont c, stype d " &
        "where a.pord = b.pord And a.poefdt = b.poefdt And psact = 1 And poact = 1 " &
        "and a.pspecno=c.pspecno and pcact=1 " &
        "and c.stype=d.stype and stact=1 " &
        "and substring(a.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " &
        "and acct='" & txtAcct.Text & "' Group by stdesc"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        Do While rs.Read
            lstData.Items.Add("Specimen Type: " & rs(0) & " - " & rs(1)) ': lstXLS.Items.Add("Specimen Type:" & "," & rs(0) & "," & rs(1))
            SpecType = rs(0)
        Loop
        rs.Close()
        lstData.Items.Add("") ': lstXLS.Items.Add(" ")
        lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        'Abnormals
        'Sel = "select a.pspecno,rc,prslt " &
        '"from pres a, pspec b, pord c " &
        '"where a.pspecno = b.pspecno And b.pord = c.pord And b.poefdt = c.poefdt " &
        '"and psact=1 and poact=1 and pract=1" &
        '"and rc in ('209011','209021','209043','209033') " &
        '"and substring(b.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " &
        '"and acct='" & txtAcct.Text & "'"
        'Cmd.CommandText = Sel
        'rs = Cmd.ExecuteReader()
        'Dim rslt As Double
        'CretTot = 0 : pHTot = 0 : SpGrTot = 0 : OxiTot = 0
        'Do While rs.Read
        '    If Not IsDBNull(rs(2)) Then
        '        rslt = Val(rs(2))
        '        If rs(1) = "209033" And rslt < 20 Then
        '            CretTot = CretTot + 1
        '        ElseIf rs(1) = "209021" And (rslt < 1.0 Or rslt > 1.04) Then
        '            SpGrTot = SpGrTot + 1
        '        ElseIf rs(1) = "209011" And (rslt < 3.0 Or rslt > 11.0) Then
        '            pHTot = pHTot + 1
        '        ElseIf rs(1) = "209043" And rslt > 200 Then
        '            OxiTot = OxiTot + 1
        '        End If
        '    End If
        'Loop
        'lstData.Items.Add("Abnormal Creatinine: " & CretTot) : lstXLS.Items.Add("Abnormal Creatinine:" & "," & CretTot)
        'lstData.Items.Add("Abnormal Specific Gravity: " & SpGrTot) : lstXLS.Items.Add("Abnormal Specific Gravity:" & "," & SpGrTot)
        'lstData.Items.Add("Abnormal pH: " & pHTot) : lstXLS.Items.Add("Abnormal pH:" & "," & pHTot)
        'lstData.Items.Add("Abnormal Oxidants: " & OxiTot) : lstXLS.Items.Add("Abnormal Oxidants:" & "," & OxiTot)
        'lstData.Items.Add("") : lstXLS.Items.Add(" ")
        'rs.Close()
        'lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        'Rejects
        Sel = "select count(distinct a.pspecno) " &
        "from pspec a, pord b, poc c " &
        "where a.pord = b.pord And a.poefdt = b.poefdt And psact = 1 And poact = 1 " &
        "and c.pspecno=a.pspecno and a.adsq1=c.adsq1 and oc='990000' " &
        "and substring(a.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " &
        "and acct='" & txtAcct.Text & "' "
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        If rs.Read Then
            lstData.Items.Add("") ': lstXLS.Items.Add(" ")
            lstData.Items.Add("Number of Rejects: " & rs(0)) ': lstXLS.Items.Add("Number of Rejects:" & "," & rs(0))
            lstData.Items.Add("") ': lstXLS.Items.Add(" ")
            NumRej = rs(0)
        End If
        rs.Close()
        lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        'Load col
        Dim colDRUG As NameValueCollection
        Dim colXDrug As NameValueCollection
        Dim colClass As NameValueCollection
        colDRUG = New NameValueCollection
        colXDrug = New NameValueCollection
        colClass = New NameValueCollection
        'colDRUG(xdrug) = Description ^ QualResultCode ^ IntResultCode ^ Illicit ^ #prescribed ^ #Pos/Con ^ #Pos/Incon ^ #Neg/Incon
        'Note:  #Pos/Incon for Illicity drugs = number of illicit 
        'colXDRUG(xdrug) = Description^Class
        'colClass(Class) = #prescribed ^ #Pos/Con ^ #Pos/Incon ^ #Neg/Incon
        Sel = "select xdrug,xdgdesc,xdgcql,xdgcint,xdgillic from xdrug where xdgcql <> ''"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        Do While rs.Read
            xClass = GetClass(Mid(rs(2), 1, Len(rs(2)) - 1) & "0")
            colDRUG.Add(rs(0), Trim(rs(1)) & "^" & rs(2) & "^" & rs(3) & "^" & rs(4) & "^0^0^0^0")
            colXDrug.Add(rs(0), Trim(rs(1)) & "^" & xClass)
            If Not CLSLIMS.DD(colClass, xClass) Then
                colClass.Add(xClass, "0^0^0^0")
            End If
        Loop
        rs.Close()
        lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        'Results
        Dim PosIllicit As Integer, PosIncon As Integer, PosCon As Integer, NegCon As Integer, NegIncon As Integer
        PosIllicit = 0 : PosIncon = 0 : PosCon = 0 : NegCon = 0 : NegIncon = 0
        Dim QualRC As String, IntRC As String, QualRes As String, IntRes As String
        Sel = "Select a.pspecno,a.pord,a.poefdt " &
        "from pspec a, pord b " &
        "where a.pord = b.pord And a.poefdt = b.poefdt And psact = 1 And poact = 1 " &
        "and substring(a.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " &
        "and acct='" & txtAcct.Text & "' "
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        Do While rs.Read
            'lstData.Items.Add(rs(0) & ", " & rs(1) & ", " & rs(2))
            Cnt = Cnt + 1
            Debug.Print(Now & ": " & rs(0) & "  " & Cnt & " of " & TotSpec)
            For Each key In colXDrug.Keys
                'Debug.Print(key)
                xClass = CLSLIMS.Piece(colXDrug(key), "^", 2)
                QualRC = CLSLIMS.Piece(colDRUG(key), "^", 2)
                IntRC = CLSLIMS.Piece(colDRUG(key), "^", 3)
                QualRes = GetResult(rs(0), QualRC)
                IntRes = GetResult(rs(0), IntRC)
                If QualRes = "Negative" And IntRes = "Consistent" Then
                    NegCon = NegCon + 1
                ElseIf QualRes = "Negative" And IntRes = "Inconsistent" Then
                    NegIncon = NegIncon + 1
                    colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 5, Val(CLSLIMS.Piece(colDRUG(key), "^", 5)) + 1)
                    colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 8, Val(CLSLIMS.Piece(colDRUG(key), "^", 8)) + 1)
                    colClass(xClass) = CLSLIMS.ReplacePiece(colClass(xClass), "^", 1, Val(CLSLIMS.Piece(colClass(xClass), "^", 1)) + 1)
                    colClass(xClass) = CLSLIMS.ReplacePiece(colClass(xClass), "^", 4, Val(CLSLIMS.Piece(colClass(xClass), "^", 4)) + 1)
                ElseIf QualRes = "Positive" And IntRes = "Consistent" Then
                    PosCon = PosCon + 1
                    colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 5, Val(CLSLIMS.Piece(colDRUG(key), "^", 5)) + 1)
                    colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 6, Val(CLSLIMS.Piece(colDRUG(key), "^", 6)) + 1)
                    colClass(xClass) = CLSLIMS.ReplacePiece(colClass(xClass), "^", 1, Val(CLSLIMS.Piece(colClass(xClass), "^", 1)) + 1)
                    colClass(xClass) = CLSLIMS.ReplacePiece(colClass(xClass), "^", 2, Val(CLSLIMS.Piece(colClass(xClass), "^", 2)) + 1)
                ElseIf QualRes = "Positive" And IntRes = "Inconsistent" Then
                    PosIncon = PosIncon + 1
                    colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 7, Val(CLSLIMS.Piece(colDRUG(key), "^", 7)) + 1)
                    colClass(xClass) = CLSLIMS.ReplacePiece(colClass(xClass), "^", 3, Val(CLSLIMS.Piece(colClass(xClass), "^", 3)) + 1)
                End If
            Next key
        Loop
        rs.Close()
        Dim x As String
        For Each key In colDRUG.Keys
            If Val(CLSLIMS.Piece(colDRUG(key), "^", 5)) > 0 Or Val(CLSLIMS.Piece(colDRUG(key), "^", 6)) > 0 Or Val(CLSLIMS.Piece(colDRUG(key), "^", 7)) > 0 Or Val(CLSLIMS.Piece(colDRUG(key), "^", 8)) > 0 Then
                x = CLSLIMS.Piece(colDRUG(key), "^", 1) & "," & CLSLIMS.Piece(colDRUG(key), "^", 5) & "," & CLSLIMS.Piece(colDRUG(key), "^", 6) & "," & CLSLIMS.Piece(colDRUG(key), "^", 7) & "," & CLSLIMS.Piece(colDRUG(key), "^", 8)
                If CLSLIMS.Piece(colDRUG(key), "^", 4) = "True" Then
                    PosIllicit = PosIllicit + Val(CLSLIMS.Piece(colDRUG(key), "^", 7))
                    x = x & "," & Val(CLSLIMS.Piece(colDRUG(key), "^", 7))
                End If
                lstXLS.Items.Add(x)
            End If
        Next key

        For Each key In colClass.Keys
            If Val(CLSLIMS.Piece(colClass(key), "^", 1)) > 0 Or Val(CLSLIMS.Piece(colClass(key), "^", 2)) > 0 Or Val(CLSLIMS.Piece(colClass(key), "^", 3)) > 0 Or Val(CLSLIMS.Piece(colClass(key), "^", 4)) > 0 Then
                x = LookUpClass(key) & "^" & CLSLIMS.Piece(colClass(key), "^", 1) & "^" & CLSLIMS.Piece(colClass(key), "^", 2) & "^" & CLSLIMS.Piece(colClass(key), "^", 3) & "^" & CLSLIMS.Piece(colClass(key), "^", 4)
                If InStr(",D ALKALOIDS,D COCAINE,D HEROIN,D METHYLENEDIOX,D PHENCYCLIDINE,P COCAINE,P HEROIN,", "," & key & ",") > 0 Then
                    x = x & "^" & Val(CLSLIMS.Piece(colClass(key), "^", 4))
                End If
                lstClass.Items.Add(x)
                Debug.Print(x)
            End If
        Next key

        lstData.Items.Add("") ': lstXLS.Items.Add(" ")
        lstData.Items.Add("Positive for Rx: " & PosCon) ': lstXLS.Items.Add("Positive for Rx:" & "," & PosCon)
        lstData.Items.Add("Negative for Rx: " & NegIncon) ': lstXLS.Items.Add("Negative for Rx:" & "," & NegIncon)
        lstData.Items.Add("Expected Results: " & PosCon + NegCon) ': lstXLS.Items.Add("Expected Results:" & "," & PosCon + NegCon)
        lstData.Items.Add("Unexpected Results: " & PosIncon) ': lstXLS.Items.Add("Unexpected Results:" & "," & PosIncon)
        'lstData.Items.Add("Negative Illicit: " & PosIncon + NegIncon)
        lstData.Items.Add("Positive Illicit: " & PosIllicit) ': lstXLS.Items.Add("Positive Illicit:" & "," & PosIllicit)
        lstData.SelectedIndex = lstData.Items.Count - 1 : lstData.Refresh()
        Conn.Close()

    End Sub
    Public Function GetResult(ByVal pspecno As String, ByVal RC As String) As String
        GetResult = ""
        Dim Sel As String
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        'Total number of specimens
        Sel = "select prslt from pres where pspecno='" & pspecno & "' and rc='" & RC & "' and pract=1"
        Cmd.CommandText = Sel
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetResult = IIf(IsDBNull(rs(0)), "", rs(0))
        End If
        rs.Close()
        Conn.Close()
    End Function

    Public Function LookUpClass(ByVal cls As String) As String
        LookUpClass = ""
        Select Case cls
            Case "AD ADULTERANTS" : LookUpClass = "Adulterants"
            Case "D AD CYCLICS" : LookUpClass = "Antidepressants, Tricyclics"
            Case "D AD OTHER" : LookUpClass = "Antidepressants"
            Case "D AD SEROTONERG" : LookUpClass = "Antidepressants, Serotonergic"
            Case "D ALCOHOL BIO" : LookUpClass = "Alcohol Biomarkers"
            Case "D ALKALOIDS" : LookUpClass = "Alkaloids, Unspecified"
            Case "D AMPHETAMINES" : LookUpClass = "Amphetamines"
            Case "D ANTIEPILEPTIC" : LookUpClass = "Antiepileptics"
            Case "D ANTIPSYCHOTIC" : LookUpClass = "Antipsychotics"
            Case "D BARBITURATES" : LookUpClass = "Barbiturates"
            Case "D BENZO" : LookUpClass = "Benzodiazepines"
            Case "D BUPRENORPHINE" : LookUpClass = "Buprenorphine"
            Case "D CANNABINOIDS" : LookUpClass = "Cannabinoids, Natural"
            Case "D COCAINE" : LookUpClass = "Cocaine"
            Case "D FENTANYLS" : LookUpClass = "Fentanyls"
            Case "D GABAPENTIN" : LookUpClass = "Gabapentin"
            Case "D HEROIN" : LookUpClass = "Heroin Metabolite"
            Case "D HYPNOTICS SED" : LookUpClass = "Sedative Hypnotics"
            Case "D METHADONE" : LookUpClass = "Methadone"
            Case "D METHYLENEDIOX" : LookUpClass = "Methylenedioxy-Amphetamines"
            Case "D METHYLPHEN" : LookUpClass = "Methylphenidate"
            Case "D OPIATES" : LookUpClass = "Opiates"
            Case "D OPIOIDS" : LookUpClass = "Opioids & Opiate Analogues"
            Case "D OXYCODONE" : LookUpClass = "Oxycodone"
            Case "D PHENCYCLIDINE" : LookUpClass = "Phencyclidine"
            Case "D PREGABALIN" : LookUpClass = "Pregabalin"
            Case "D PROPOXYPHENE" : LookUpClass = "Propoxyphene"
            Case "D SKELETAL MUSC" : LookUpClass = "Skeletal Muscle Relaxants"
            Case "D STEREOISOMER" : LookUpClass = "Stereoisomer Analysis"
            Case "D TAPENTADOL" : LookUpClass = "Tapentadol"
            Case "D TRAMADOL" : LookUpClass = "Tramadol"
            Case "P ALCOHOL" : LookUpClass = "Alcohol"
            Case "P ALCOHOL BIO" : LookUpClass = "Alcohol Biomarkers"
            Case "P ALKALOIDS" : LookUpClass = "Alkaloids, Unspecified"
            Case "P AMPHETAMINES" : LookUpClass = "Amphetamines"
            Case "P BARBITURATES" : LookUpClass = "Barbiturates"
            Case "P BENZO" : LookUpClass = "Benzodiazepines"
            Case "P BUPRENORPHINE" : LookUpClass = "Buprenorphine"
            Case "P CANNABINOIDS" : LookUpClass = "Cannabinoids, Natural"
            Case "P COCAINE" : LookUpClass = "Cocaine"
            Case "P FENTANYLS" : LookUpClass = "Fentanyls"
            Case "P HEROIN" : LookUpClass = "Heroin Metabolite"
            Case "P HYPNOTICS SED" : LookUpClass = "Sedative Hypnotics"
            Case "P METHADONE" : LookUpClass = "Methadone"
            Case "P METHYLENEDIOX" : LookUpClass = "Methylenedioxyamphetamine"
            Case "P OPIATES" : LookUpClass = "Opiates"
            Case "P OXYCODONE" : LookUpClass = "Oxycodone"
            Case "P PHENCYCLIDINE" : LookUpClass = "Phencyclidine"
            Case "P PROPOXYPHENE" : LookUpClass = "Propoxyphene"
            Case "P SKELETAL MUSC" : LookUpClass = "Skeletal Muscle Relaxants"
            Case "P TRAMADOL" : LookUpClass = "Tramadol"
        End Select

    End Function

    Public Function GetClass(ByVal TC As String) As String
        GetClass = ""
        Dim Sel As String
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        'Total number of specimens
        Sel = "select tgroup from tc where tc='" & TC & "' and tact=1"
        Cmd.CommandText = Sel
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetClass = IIf(IsDBNull(rs(0)), "", rs(0))
        End If
        rs.Close()
        Conn.Close()
    End Function


    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim printer As New PCPrint()
        'Set the font we want to use
        printer.PrinterFont = New Font("Courier New", 10)
        'Set Margins
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        printer.DefaultPageSettings.Margins = mar
        printer.DefaultPageSettings.Landscape = True

        Dim txt As String = ""
        For i = 0 To lstData.Items.Count - 1
            txt = txt & vbCrLf & lstData.Items(i)
        Next i
        printer.TextToPrint = " " & txt
        'Issue print command
        printer.Print()
    End Sub

    Private Sub btnXLS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnXLS.Click
        Dim XLApp As Excel.Application
        Dim XLWBS As Excel.Workbooks
        Dim XLWB As Excel.Workbook
        Dim XLWS As Excel.Worksheet
        Dim AppPath As String = "", TotRow As Integer = 0
        Dim dialog As New FolderBrowserDialog()
        dialog.RootFolder = Environment.SpecialFolder.Desktop
        dialog.SelectedPath = "C:\"
        dialog.Description = "Select Application Configuration Files Path"
        If dialog.ShowDialog() = DialogResult.OK Then
            'If dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
            AppPath = dialog.SelectedPath
        End If
        If System.IO.File.Exists(AppPath & "\MRS-" & txtAcct.Text & ".xlsx") Then
            System.IO.File.Delete(AppPath & "\MRS-" & txtAcct.Text & ".xlsx")
        End If
        XLApp = New Excel.Application
        XLApp.Visible = False
        XLWBS = XLApp.Workbooks
        XLWB = XLWBS.Open("C:\CLSLIMS\MonthlyResultsSummary.xlsx")
        'Drug tab
        XLWS = XLWB.Sheets("AcctSum")
        XLWS.Cells(3, 2) = GetAcctName(txtAcct.Text)
        XLWS.Cells(4, 2) = txtStartDt.Text
        XLWS.Cells(5, 2) = txtEndDt.Text
        XLWS.Cells(7, 2) = TotSpec
        XLWS.Cells(9, 2) = SpecType
        XLWS.Cells(11, 2) = NumRej
        For i = 0 To lstXLS.Items.Count - 1
            For j = 1 To 6
                XLWS.Cells(15 + i, j) = CLSLIMS.Piece(lstXLS.Items(i), ",", j)
            Next j
            TotRow = i + 16
        Next i
        'Add totals and sums
        XLWS.Cells(TotRow, 1) = "Totals:"
        XLWS.Range("A" & TotRow, "F" & TotRow).RowHeight = 30
        XLWS.Range("A" & TotRow).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Color = Color.White
        XLWS.Range("A" & TotRow, "F" & TotRow).Interior.Color = Color.Black
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Bold = True
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Size = 12
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Name = "Arial"
        XLWS.Range("B" & 13, "F" & TotRow).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
        XLWS.Cells(TotRow, 2) = "=SUM(B15:B" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 3) = "=SUM(C15:C" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 4) = "=SUM(D15:D" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 5) = "=SUM(E15:E" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 6) = "=SUM(F15:F" & TotRow - 1 & ")"
        XLWS.Range("A15", "F" & TotRow).Borders.LineStyle = Excel.XlLineStyle.xlContinuous

        'Class Tab
        XLWS = XLWB.Sheets("ClassSum")
        XLWS.Cells(3, 2) = GetAcctName(txtAcct.Text)
        XLWS.Cells(4, 2) = txtStartDt.Text
        XLWS.Cells(5, 2) = txtEndDt.Text
        XLWS.Cells(7, 2) = TotSpec
        XLWS.Cells(9, 2) = SpecType
        XLWS.Cells(11, 2) = NumRej
        For i = 0 To lstClass.Items.Count - 1
            For j = 1 To 6
                XLWS.Cells(15 + i, j) = CLSLIMS.Piece(lstClass.Items(i), "^", j)
            Next j
            TotRow = i + 16
        Next i
        'Add totals and sums
        XLWS.Cells(TotRow, 1) = "Totals:"
        XLWS.Range("A" & TotRow, "F" & TotRow).RowHeight = 30
        XLWS.Range("A" & TotRow).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Color = Color.White
        XLWS.Range("A" & TotRow, "F" & TotRow).Interior.Color = Color.Black
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Bold = True
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Size = 12
        XLWS.Range("A" & TotRow, "F" & TotRow).Font.Name = "Arial"
        XLWS.Range("B" & 13, "F" & TotRow).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
        XLWS.Cells(TotRow, 2) = "=SUM(B15:B" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 3) = "=SUM(C15:C" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 4) = "=SUM(D15:D" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 5) = "=SUM(E15:E" & TotRow - 1 & ")"
        XLWS.Cells(TotRow, 6) = "=SUM(F15:F" & TotRow - 1 & ")"
        XLWS.Range("A15", "F" & TotRow).Borders.LineStyle = Excel.XlLineStyle.xlContinuous

        'Save XLSX
        XLWB.SaveAs(AppPath & "\MRS-" & txtAcct.Text & ".xlsx")
        XLWB.Close()
        XLWBS.Close()
        XLApp.Quit()

    End Sub

    Private Sub AcctSum_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "NED.SABBTINI" Then
            btnRunZip.Visible = True
        End If
    End Sub

    Private Sub btnRunZip_Click(sender As Object, e As EventArgs) Handles btnRunZip.Click
        Dim dFirst As Date, dLast As Date, sFirst As String, sLast As String
        dFirst = CDate(txtStartDt.Text) : dLast = CDate(txtEndDt.Text)
        Dim CretTot As Integer, pHTot As Integer, SpGrTot As Integer, OxiTot As Integer
        Dim Cnt As Integer = 0, TotSpec As Integer = 0, TotPat As Integer = 0, TotRej As Integer = 0, PUID As String = ""
        lstData.Items.Clear()
        sFirst = DatePart(DateInterval.Year, dFirst) & IIf(DatePart(DateInterval.Month, dFirst) < 10, "0", "") & DatePart(DateInterval.Month, dFirst) & IIf(DatePart(DateInterval.Day, dFirst) < 10, "0", "") & DatePart(DateInterval.Day, dFirst)
        sLast = DatePart(DateInterval.Year, dLast) & IIf(DatePart(DateInterval.Month, dLast) < 10, "0", "") & DatePart(DateInterval.Month, dLast) & IIf(DatePart(DateInterval.Day, dLast) < 10, "0", "") & DatePart(DateInterval.Day, dLast)
        lstData.Items.Add("   Account: " & txtAcct.Text & " - " & lblAcct.Text) : lstXLS.Items.Add("Account:" & "," & txtAcct.Text & "," & lblAcct.Text)
        lstData.Items.Add("Start Date: " & dFirst) : lstXLS.Items.Add("Start Date:" & "," & dFirst)
        lstData.Items.Add("  End Date: " & dLast) : lstXLS.Items.Add("End Date:" & "," & dLast)
        lstData.Items.Add("") : lstXLS.Items.Add(" ")
        Dim Sel As String
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        'Total number of specimens
        Sel = "select distinct pspecno " &
        "from pspec " &
        "where psact = 1 " &
        "and substring(psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "'"

        Cmd.CommandText = Sel
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        TotSpec = 0
        Do While rs.Read
            If InZipSpecNo(rs(0)) Then
                TotSpec = TotSpec + 1
            End If
        Loop
        rs.Close()
        lstData.Items.Add("Total specimens: " & TotSpec) : lstXLS.Items.Add("Total Specimens:" & "," & TotSpec)
        Debug.Print("Tot spec: " & TotSpec)
        'Number of patients
        Sel = "select distinct puid " &
        "from pspec " &
        "where psact = 1 And substring(psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "'"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        TotPat = 0
        Do While rs.Read
            If Not IsDBNull(rs(0)) Then
                If InZip(rs(0), "") Then
                    TotPat = TotPat + 1
                End If
            End If
        Loop
        rs.Close()
        lstData.Items.Add("") : lstXLS.Items.Add(" ")
        lstData.Items.Add("Number of patients: " & TotPat) : lstXLS.Items.Add("Number of Patients:" & "," & TotPat)
        lstData.Items.Add("") : lstXLS.Items.Add(" ")
        Debug.Print("Num of Pat: " & TotPat)
        'Specimen Types
        'Sel = "select stdesc,count(a.pspecno) " &
        '"from pspec a, pord b, pcont c, stype d " &
        '"where a.pord = b.pord And a.poefdt = b.poefdt And psact = 1 And poact = 1 " &
        '"and a.pspecno=c.pspecno and pcact=1 " &
        '"and c.stype=d.stype and stact=1 " &
        '"and substring(a.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " &
        '"and acct='" & txtAcct.Text & "' Group by stdesc"
        'Cmd.CommandText = Sel
        'rs = Cmd.ExecuteReader()
        'Do While rs.Read
        '    lstData.Items.Add("Specimen Type: " & rs(0) & " - " & rs(1)) : lstXLS.Items.Add("Specimen Type:" & "," & rs(0) & "," & rs(1))
        'Loop
        'rs.Close()
        'lstData.Items.Add("") : lstXLS.Items.Add(" ")
        'Abnormals
        Sel = "select a.pspecno,rc,prslt " &
        "from pres a, pspec b " &
        "where a.pspecno = b.pspecno " &
        "and psact=1 and pract=1" &
        "and rc in ('209011','209021','209043','209033') " &
        "and substring(psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "'"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        Dim rslt As Double
        CretTot = 0 : pHTot = 0 : SpGrTot = 0 : OxiTot = 0
        Do While rs.Read
            If InZipSpecNo(rs(0)) Then
                If Not IsDBNull(rs(2)) Then
                    rslt = Val(rs(2))
                    If rs(1) = "209033" And rslt < 20 Then
                        CretTot = CretTot + 1
                    ElseIf rs(1) = "209021" And (rslt < 1.0 Or rslt > 1.04) Then
                        SpGrTot = SpGrTot + 1
                    ElseIf rs(1) = "209011" And (rslt < 3.0 Or rslt > 11.0) Then
                        pHTot = pHTot + 1
                    ElseIf rs(1) = "209043" And rslt > 200 Then
                        OxiTot = OxiTot + 1
                    End If
                End If
            End If
        Loop
        lstData.Items.Add("Abnormal Creatinine: " & CretTot) : lstXLS.Items.Add("Abnormal Creatinine:" & "," & CretTot)
        lstData.Items.Add("Abnormal Specific Gravity: " & SpGrTot) : lstXLS.Items.Add("Abnormal Specific Gravity:" & "," & SpGrTot)
        lstData.Items.Add("Abnormal pH: " & pHTot) : lstXLS.Items.Add("Abnormal pH:" & "," & pHTot)
        lstData.Items.Add("Abnormal Oxidants: " & OxiTot) : lstXLS.Items.Add("Abnormal Oxidants:" & "," & OxiTot)
        lstData.Items.Add("") : lstXLS.Items.Add(" ")
        rs.Close()
        Debug.Print("Abn done.")
        'Rejects
        Sel = "select distinct a.pspecno " &
        "from pspec a, poc c " &
        "where psact = 1 " &
        "and c.pspecno=a.pspecno and a.adsq1=c.adsq1 and oc='990000' " &
        "and substring(psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "'"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        Do While rs.Read
            If InZipSpecNo(rs(0)) Then
                TotRej = TotRej + 1
            End If
        Loop
        rs.Close()
        lstData.Items.Add("") : lstXLS.Items.Add(" ")
        lstData.Items.Add("Number of Rejects: " & TotRej) : lstXLS.Items.Add("Number of Rejects:" & "," & TotRej)
        lstData.Items.Add("") : lstXLS.Items.Add(" ")
        Debug.Print("Rej done.")
        'Load col
        Dim colDRUG As NameValueCollection
        Dim colXDrug As NameValueCollection
        colDRUG = New NameValueCollection
        colXDrug = New NameValueCollection
        'colDRUG(xdrug) = Description ^ QualResultCode ^ IntResultCode ^ Illicit ^ #prescribed ^ #Pos/Con ^ #Pos/Incon ^ #Neg/Incon
        'Note:  #Pos/Incon for Illicity drugs = number of illicit 
        Sel = "select xdrug,xdgdesc,xdgcql,xdgcint,xdgillic from xdrug where xdgcql <> ''"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        Do While rs.Read
            colDRUG.Add(rs(0), Trim(rs(1)) & "^" & rs(2) & "^" & rs(3) & "^" & rs(4) & "^0^0^0^0")
            colXDrug.Add(rs(0), Trim(rs(1)))
        Loop
        rs.Close()
        'Results
        Dim PosIllicit As Integer, PosIncon As Integer, PosCon As Integer, NegCon As Integer, NegIncon As Integer
        PosIllicit = 0 : PosIncon = 0 : PosCon = 0 : NegCon = 0 : NegIncon = 0
        Dim QualRC As String, IntRC As String, QualRes As String, IntRes As String
        Sel = "Select pspecno " &
        "from pspec " &
        "where psact = 1 " &
        "and substring(psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "'"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader()
        Do While rs.Read
            'lstData.Items.Add(rs(0) & ", " & rs(1) & ", " & rs(2))
            Cnt = Cnt + 1
            Debug.Print(Now & ": " & rs(0) & "  " & Cnt & " of " & TotSpec)
            If InZipSpecNo(rs(0)) Then
                For Each key In colXDrug.Keys
                    'Debug.Print(key)
                    QualRC = CLSLIMS.Piece(colDRUG(key), "^", 2)
                    IntRC = CLSLIMS.Piece(colDRUG(key), "^", 3)
                    QualRes = GetResult(rs(0), QualRC)
                    IntRes = GetResult(rs(0), IntRC)
                    If QualRes = "Negative" And IntRes = "Consistent" Then
                        NegCon = NegCon + 1
                    ElseIf QualRes = "Negative" And IntRes = "Inconsistent" Then
                        NegIncon = NegIncon + 1
                        colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 5, Val(CLSLIMS.Piece(colDRUG(key), "^", 5)) + 1)
                        colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 8, Val(CLSLIMS.Piece(colDRUG(key), "^", 8)) + 1)
                    ElseIf QualRes = "Positive" And IntRes = "Consistent" Then
                        PosCon = PosCon + 1
                        colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 5, Val(CLSLIMS.Piece(colDRUG(key), "^", 5)) + 1)
                        colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 6, Val(CLSLIMS.Piece(colDRUG(key), "^", 6)) + 1)
                    ElseIf QualRes = "Positive" And IntRes = "Inconsistent" Then
                        PosIncon = PosIncon + 1
                        colDRUG(key) = CLSLIMS.ReplacePiece(colDRUG(key), "^", 7, Val(CLSLIMS.Piece(colDRUG(key), "^", 7)) + 1)
                    End If
                Next key
            End If
        Loop
        rs.Close()
        Dim x As String
        lstXLS.Items.Add("Drug,#Prescribed,#Expected and found,#Unexpected,#Expected and not found,Illicit")
        For Each key In colDRUG.Keys
            If Val(CLSLIMS.Piece(colDRUG(key), "^", 5)) > 0 Or Val(CLSLIMS.Piece(colDRUG(key), "^", 6)) > 0 Or Val(CLSLIMS.Piece(colDRUG(key), "^", 7)) > 0 Or Val(CLSLIMS.Piece(colDRUG(key), "^", 8)) > 0 Then
                lstData.Items.Add(CLSLIMS.Piece(colDRUG(key), "^", 1) & " - Prescribed: " & CLSLIMS.Piece(colDRUG(key), "^", 5) & ", Expected: " & CLSLIMS.Piece(colDRUG(key), "^", 6) & ", Unexpected: " & CLSLIMS.Piece(colDRUG(key), "^", 7) & ", Expected but not found: " & CLSLIMS.Piece(colDRUG(key), "^", 8))
                x = CLSLIMS.Piece(colDRUG(key), "^", 1) & "," & CLSLIMS.Piece(colDRUG(key), "^", 5) & "," & CLSLIMS.Piece(colDRUG(key), "^", 6) & "," & CLSLIMS.Piece(colDRUG(key), "^", 7) & "," & CLSLIMS.Piece(colDRUG(key), "^", 8)
                If CLSLIMS.Piece(colDRUG(key), "^", 4) = "True" Then
                    lstData.Items.Add("     Illicit Drug: " & CLSLIMS.Piece(colDRUG(key), "^", 7))
                    PosIllicit = PosIllicit + Val(CLSLIMS.Piece(colDRUG(key), "^", 7))
                    x = x & "," & Val(CLSLIMS.Piece(colDRUG(key), "^", 7))
                End If
                lstXLS.Items.Add(x)
            End If
        Next key

        lstData.Items.Add("") : lstXLS.Items.Add(" ")
        lstData.Items.Add("Positive for Rx: " & PosCon) : lstXLS.Items.Add("Positive for Rx:" & "," & PosCon)
        lstData.Items.Add("Negative for Rx: " & NegIncon) : lstXLS.Items.Add("Negative for Rx:" & "," & NegIncon)
        lstData.Items.Add("Expected Results: " & PosCon + NegCon) : lstXLS.Items.Add("Expected Results:" & "," & PosCon + NegCon)
        lstData.Items.Add("Unexpected Results: " & PosIncon) : lstXLS.Items.Add("Unexpected Results:" & "," & PosIncon)
        'lstData.Items.Add("Negative Illicit: " & PosIncon + NegIncon)
        lstData.Items.Add("Positive Illicit: " & PosIllicit) : lstXLS.Items.Add("Positive Illicit:" & "," & PosIllicit)

        Conn.Close()

    End Sub
    Public Function InZip(ByVal puid As String, pefdt As String) As Boolean
        InZip = False
        Dim Sel As String = "Select pzip from pdem where puid='" & puid & "' And pefdt='" & pefdt & "'"
        If pefdt = "" Then
            Sel = "Select pzip from pdem where puid='" & puid & "' And PACT=1"
        End If
        'Dim ZIPstr = "^35005^35006^35015^35020^35021^35022^35023^35036^35041^35048^35060^35061^35062^35064" &      'Birmingham
        '    "^35068^35071^35073^35091^35094^35111^35116^35117^35118^35119^35123^35126^35127^35139^35142" &
        '    "^35172^35173^35180^35181^35201^35202^35203^35204^35205^35206^35207^35208^35209^35210^35211" &
        '    "^35212^35213^35214^35215^35216^35217^35218^35219^35220^35221^35222^35223^35224^35225^35226" &
        '    "^35228^35229^35230^35231^35232^35233^35234^35235^35236^35237^35238^35240^35243^35244^35245" &
        '    "^35246^35249^35253^35254^35255^35259^35260^35261^35263^35266^35277^35278^35279^35280^35281" &
        '    "^35282^35283^35285^35286^35287^35288^35289^35290^35291^35292^35293^35294^35295^35296^35297" &
        '    "^35298^35299^"
        Dim ZIPstr = "^35741^35748^35749^35750^35757^35758^35759^35760^35761^35762^35763^35767^35773^35801^" &     'Huntsville
        "35802^35803^35804^35805^35806^35807^35808^35809^35810^35811^35812^35813^35814^35815^35816^" &
        "35824^35893^35894^35895^35896^35897^35898^35899^"

        'Dim ZIPstr = "^35902^35901^35904^35093^35906^35907^35952^35990^35954^35956^35972^"      'Gadsden
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = Sel
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If Not IsDBNull(rs(0)) Then
                If InStr(ZIPstr, "^" & rs(0) & "^") Then
                    InZip = True
                End If
            End If
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function InZipSpecNo(ByVal specno As String) As Boolean
        InZipSpecNo = False
        Dim Sel As String = "select puid,pefdt from pspec where pspecno='" & specno & "' and psact=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = Sel
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If Not IsDBNull(rs(0)) And Not IsDBNull(rs(1)) Then
                InZipSpecNo = InZip(rs(0), rs(1))
            End If
        End If
        rs.Close()
        Conn.Close()
    End Function
End Class