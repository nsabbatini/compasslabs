﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CLSLIMS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CLSLIMS))
        Me.MenuTab = New System.Windows.Forms.TabControl()
        Me.Admin = New System.Windows.Forms.TabPage()
        Me.btnAutoFax = New System.Windows.Forms.Button()
        Me.btnTAT = New System.Windows.Forms.Button()
        Me.btnMedRecScan = New System.Windows.Forms.Button()
        Me.btnAnthem = New System.Windows.Forms.Button()
        Me.btnFedxUPS = New System.Windows.Forms.Button()
        Me.btnSlsProcLst = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.btnSlsProc = New System.Windows.Forms.Button()
        Me.btnIntVen = New System.Windows.Forms.Button()
        Me.btnUrnSal = New System.Windows.Forms.Button()
        Me.btnAffScan = New System.Windows.Forms.Button()
        Me.btnAddDrugDiag = New System.Windows.Forms.Button()
        Me.btnRebill = New System.Windows.Forms.Button()
        Me.btnPending = New System.Windows.Forms.Button()
        Me.btnAcctOCs = New System.Windows.Forms.Button()
        Me.btnAcctOC = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.btnAbbrv = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btnstatcodeq = New System.Windows.Forms.Button()
        Me.btnaffdef = New System.Windows.Forms.Button()
        Me.btnTrackRpt = New System.Windows.Forms.Button()
        Me.btnTCalc = New System.Windows.Forms.Button()
        Me.btnEMRDef = New System.Windows.Forms.Button()
        Me.btnEMRList = New System.Windows.Forms.Button()
        Me.btnAcctScan = New System.Windows.Forms.Button()
        Me.btnAcctSum = New System.Windows.Forms.Button()
        Me.btnLabel = New System.Windows.Forms.Button()
        Me.btnChiral = New System.Windows.Forms.Button()
        Me.btnMonStat = New System.Windows.Forms.Button()
        Me.btnAddMeds = New System.Windows.Forms.Button()
        Me.btnPhyInq = New System.Windows.Forms.Button()
        Me.btnAcctInq = New System.Windows.Forms.Button()
        Me.btnOCInq = New System.Windows.Forms.Button()
        Me.btnBadFlag = New System.Windows.Forms.Button()
        Me.Proc = New System.Windows.Forms.TabPage()
        Me.btnEVW = New System.Windows.Forms.Button()
        Me.btnSpcDis = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.btnSpecScan = New System.Windows.Forms.Button()
        Me.btnCBC = New System.Windows.Forms.Button()
        Me.btnScanCourier = New System.Windows.Forms.Button()
        Me.btnSO = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnLabels = New System.Windows.Forms.Button()
        Me.btnOE = New System.Windows.Forms.Button()
        Me.SpecPrep = New System.Windows.Forms.TabPage()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnHam = New System.Windows.Forms.Button()
        Me.btnEPMotionETG = New System.Windows.Forms.Button()
        Me.btnEPMotionTHC = New System.Windows.Forms.Button()
        Me.btnMBNLbl = New System.Windows.Forms.Button()
        Me.btnBuildPain = New System.Windows.Forms.Button()
        Me.btnEPMotion = New System.Windows.Forms.Button()
        Me.Screen = New System.Windows.Forms.TabPage()
        Me.lblVer = New System.Windows.Forms.Label()
        Me.LCMS = New System.Windows.Forms.TabPage()
        Me.btnTrayClip = New System.Windows.Forms.Button()
        Me.btnRelTray = New System.Windows.Forms.Button()
        Me.btnHist = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.btnSpecRel = New System.Windows.Forms.Button()
        Me.btnPrintSVT = New System.Windows.Forms.Button()
        Me.btnBatchRev = New System.Windows.Forms.Button()
        Me.btnFldrClip = New System.Windows.Forms.Button()
        Me.btnSpecRev = New System.Windows.Forms.Button()
        Me.btnMBNClip = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnFldrAglnt = New System.Windows.Forms.Button()
        Me.btnPrintMBN = New System.Windows.Forms.Button()
        Me.btnPrtWrklst = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Cardio = New System.Windows.Forms.TabPage()
        Me.btnMonRpt = New System.Windows.Forms.Button()
        Me.btnAcctMonRpt = New System.Windows.Forms.Button()
        Me.btnORL = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.btnLocDef = New System.Windows.Forms.Button()
        Me.btnVndDef = New System.Windows.Forms.Button()
        Me.btnItemDef = New System.Windows.Forms.Button()
        Me.btnQue = New System.Windows.Forms.Button()
        Me.btnOrder = New System.Windows.Forms.Button()
        Me.Info = New System.Windows.Forms.TabPage()
        Me.btnInsQue = New System.Windows.Forms.Button()
        Me.wbDailyInfo = New System.Windows.Forms.WebBrowser()
        Me.Finance = New System.Windows.Forms.TabPage()
        Me.btnSpcNum = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.btnAcc = New System.Windows.Forms.Button()
        Me.btnPay = New System.Windows.Forms.Button()
        Me.Molecular = New System.Windows.Forms.TabPage()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.btnBldCovidXLS = New System.Windows.Forms.Button()
        Me.btnBldCovid = New System.Windows.Forms.Button()
        Me.btnImpC = New System.Windows.Forms.Button()
        Me.btnNNL = New System.Windows.Forms.Button()
        Me.btnImpB = New System.Windows.Forms.Button()
        Me.btnCutRng = New System.Windows.Forms.Button()
        Me.btnImpA = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.btnAntiBiotic = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSpcInq = New System.Windows.Forms.Button()
        Me.DailyInfo = New System.Windows.Forms.Timer(Me.components)
        Me.TrackBar1 = New System.Windows.Forms.TrackBar()
        Me.lblExt = New System.Windows.Forms.Label()
        Me.MenuTab.SuspendLayout()
        Me.Admin.SuspendLayout()
        Me.Proc.SuspendLayout()
        Me.SpecPrep.SuspendLayout()
        Me.Screen.SuspendLayout()
        Me.LCMS.SuspendLayout()
        Me.Cardio.SuspendLayout()
        Me.Info.SuspendLayout()
        Me.Finance.SuspendLayout()
        Me.Molecular.SuspendLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuTab
        '
        Me.MenuTab.Controls.Add(Me.Admin)
        Me.MenuTab.Controls.Add(Me.Proc)
        Me.MenuTab.Controls.Add(Me.SpecPrep)
        Me.MenuTab.Controls.Add(Me.Screen)
        Me.MenuTab.Controls.Add(Me.LCMS)
        Me.MenuTab.Controls.Add(Me.Cardio)
        Me.MenuTab.Controls.Add(Me.Info)
        Me.MenuTab.Controls.Add(Me.Finance)
        Me.MenuTab.Controls.Add(Me.Molecular)
        Me.MenuTab.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuTab.Location = New System.Drawing.Point(2, 0)
        Me.MenuTab.Name = "MenuTab"
        Me.MenuTab.SelectedIndex = 0
        Me.MenuTab.Size = New System.Drawing.Size(846, 308)
        Me.MenuTab.TabIndex = 0
        '
        'Admin
        '
        Me.Admin.Controls.Add(Me.btnAutoFax)
        Me.Admin.Controls.Add(Me.btnTAT)
        Me.Admin.Controls.Add(Me.btnMedRecScan)
        Me.Admin.Controls.Add(Me.btnAnthem)
        Me.Admin.Controls.Add(Me.btnFedxUPS)
        Me.Admin.Controls.Add(Me.btnSlsProcLst)
        Me.Admin.Controls.Add(Me.Button8)
        Me.Admin.Controls.Add(Me.btnSlsProc)
        Me.Admin.Controls.Add(Me.btnIntVen)
        Me.Admin.Controls.Add(Me.btnUrnSal)
        Me.Admin.Controls.Add(Me.btnAffScan)
        Me.Admin.Controls.Add(Me.btnAddDrugDiag)
        Me.Admin.Controls.Add(Me.btnRebill)
        Me.Admin.Controls.Add(Me.btnPending)
        Me.Admin.Controls.Add(Me.btnAcctOCs)
        Me.Admin.Controls.Add(Me.btnAcctOC)
        Me.Admin.Controls.Add(Me.Button5)
        Me.Admin.Controls.Add(Me.btnAbbrv)
        Me.Admin.Controls.Add(Me.Button4)
        Me.Admin.Controls.Add(Me.btnstatcodeq)
        Me.Admin.Controls.Add(Me.btnaffdef)
        Me.Admin.Controls.Add(Me.btnTrackRpt)
        Me.Admin.Controls.Add(Me.btnTCalc)
        Me.Admin.Controls.Add(Me.btnEMRDef)
        Me.Admin.Controls.Add(Me.btnEMRList)
        Me.Admin.Controls.Add(Me.btnAcctScan)
        Me.Admin.Controls.Add(Me.btnAcctSum)
        Me.Admin.Controls.Add(Me.btnLabel)
        Me.Admin.Controls.Add(Me.btnChiral)
        Me.Admin.Controls.Add(Me.btnMonStat)
        Me.Admin.Controls.Add(Me.btnAddMeds)
        Me.Admin.Controls.Add(Me.btnPhyInq)
        Me.Admin.Controls.Add(Me.btnAcctInq)
        Me.Admin.Controls.Add(Me.btnOCInq)
        Me.Admin.Controls.Add(Me.btnBadFlag)
        Me.Admin.Location = New System.Drawing.Point(4, 26)
        Me.Admin.Name = "Admin"
        Me.Admin.Padding = New System.Windows.Forms.Padding(3)
        Me.Admin.Size = New System.Drawing.Size(838, 278)
        Me.Admin.TabIndex = 4
        Me.Admin.Text = "Admin"
        Me.Admin.UseVisualStyleBackColor = True
        '
        'btnAutoFax
        '
        Me.btnAutoFax.AllowDrop = True
        Me.btnAutoFax.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAutoFax.Location = New System.Drawing.Point(716, 108)
        Me.btnAutoFax.Name = "btnAutoFax"
        Me.btnAutoFax.Size = New System.Drawing.Size(116, 47)
        Me.btnAutoFax.TabIndex = 35
        Me.btnAutoFax.Text = "Auto FAX"
        Me.btnAutoFax.UseVisualStyleBackColor = True
        '
        'btnTAT
        '
        Me.btnTAT.AllowDrop = True
        Me.btnTAT.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTAT.Location = New System.Drawing.Point(250, 155)
        Me.btnTAT.Name = "btnTAT"
        Me.btnTAT.Size = New System.Drawing.Size(116, 47)
        Me.btnTAT.TabIndex = 34
        Me.btnTAT.Text = "TAT Stats"
        Me.btnTAT.UseVisualStyleBackColor = True
        '
        'btnMedRecScan
        '
        Me.btnMedRecScan.AllowDrop = True
        Me.btnMedRecScan.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMedRecScan.Location = New System.Drawing.Point(716, 14)
        Me.btnMedRecScan.Name = "btnMedRecScan"
        Me.btnMedRecScan.Size = New System.Drawing.Size(116, 47)
        Me.btnMedRecScan.TabIndex = 33
        Me.btnMedRecScan.Text = "Medical Record Scan"
        Me.btnMedRecScan.UseVisualStyleBackColor = True
        '
        'btnAnthem
        '
        Me.btnAnthem.AllowDrop = True
        Me.btnAnthem.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnthem.Location = New System.Drawing.Point(598, 202)
        Me.btnAnthem.Name = "btnAnthem"
        Me.btnAnthem.Size = New System.Drawing.Size(116, 47)
        Me.btnAnthem.TabIndex = 32
        Me.btnAnthem.Text = "Anthem"
        Me.btnAnthem.UseVisualStyleBackColor = True
        '
        'btnFedxUPS
        '
        Me.btnFedxUPS.AllowDrop = True
        Me.btnFedxUPS.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFedxUPS.Location = New System.Drawing.Point(482, 202)
        Me.btnFedxUPS.Name = "btnFedxUPS"
        Me.btnFedxUPS.Size = New System.Drawing.Size(116, 47)
        Me.btnFedxUPS.TabIndex = 31
        Me.btnFedxUPS.Text = "Reconcile Fedex/UPS"
        Me.btnFedxUPS.UseVisualStyleBackColor = True
        '
        'btnSlsProcLst
        '
        Me.btnSlsProcLst.AllowDrop = True
        Me.btnSlsProcLst.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSlsProcLst.Location = New System.Drawing.Point(598, 155)
        Me.btnSlsProcLst.Name = "btnSlsProcLst"
        Me.btnSlsProcLst.Size = New System.Drawing.Size(116, 47)
        Me.btnSlsProcLst.TabIndex = 30
        Me.btnSlsProcLst.Text = "Sales  Processors List"
        Me.btnSlsProcLst.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.AllowDrop = True
        Me.Button8.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(599, 108)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(116, 47)
        Me.Button8.TabIndex = 29
        Me.Button8.Text = "Assign Sales  Processors"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'btnSlsProc
        '
        Me.btnSlsProc.AllowDrop = True
        Me.btnSlsProc.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSlsProc.Location = New System.Drawing.Point(599, 61)
        Me.btnSlsProc.Name = "btnSlsProc"
        Me.btnSlsProc.Size = New System.Drawing.Size(116, 47)
        Me.btnSlsProc.TabIndex = 28
        Me.btnSlsProc.Text = "Sales Proc Definition"
        Me.btnSlsProc.UseVisualStyleBackColor = True
        '
        'btnIntVen
        '
        Me.btnIntVen.AllowDrop = True
        Me.btnIntVen.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIntVen.Location = New System.Drawing.Point(599, 14)
        Me.btnIntVen.Name = "btnIntVen"
        Me.btnIntVen.Size = New System.Drawing.Size(116, 47)
        Me.btnIntVen.TabIndex = 27
        Me.btnIntVen.Text = "Interventional Pain"
        Me.btnIntVen.UseVisualStyleBackColor = True
        '
        'btnUrnSal
        '
        Me.btnUrnSal.AllowDrop = True
        Me.btnUrnSal.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUrnSal.Location = New System.Drawing.Point(482, 155)
        Me.btnUrnSal.Name = "btnUrnSal"
        Me.btnUrnSal.Size = New System.Drawing.Size(116, 47)
        Me.btnUrnSal.TabIndex = 26
        Me.btnUrnSal.Text = "Urine/Saliva Same DOS"
        Me.btnUrnSal.UseVisualStyleBackColor = True
        '
        'btnAffScan
        '
        Me.btnAffScan.AllowDrop = True
        Me.btnAffScan.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAffScan.Location = New System.Drawing.Point(716, 61)
        Me.btnAffScan.Name = "btnAffScan"
        Me.btnAffScan.Size = New System.Drawing.Size(116, 47)
        Me.btnAffScan.TabIndex = 25
        Me.btnAffScan.Text = "Affidavit Scan"
        Me.btnAffScan.UseVisualStyleBackColor = True
        '
        'btnAddDrugDiag
        '
        Me.btnAddDrugDiag.AllowDrop = True
        Me.btnAddDrugDiag.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddDrugDiag.Location = New System.Drawing.Point(483, 108)
        Me.btnAddDrugDiag.Name = "btnAddDrugDiag"
        Me.btnAddDrugDiag.Size = New System.Drawing.Size(116, 47)
        Me.btnAddDrugDiag.TabIndex = 24
        Me.btnAddDrugDiag.Text = "Addiction Drug/Diag"
        Me.btnAddDrugDiag.UseVisualStyleBackColor = True
        Me.btnAddDrugDiag.Visible = False
        '
        'btnRebill
        '
        Me.btnRebill.AllowDrop = True
        Me.btnRebill.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRebill.Location = New System.Drawing.Point(483, 61)
        Me.btnRebill.Name = "btnRebill"
        Me.btnRebill.Size = New System.Drawing.Size(116, 47)
        Me.btnRebill.TabIndex = 23
        Me.btnRebill.Text = "Rebill"
        Me.btnRebill.UseVisualStyleBackColor = True
        '
        'btnPending
        '
        Me.btnPending.AllowDrop = True
        Me.btnPending.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPending.Location = New System.Drawing.Point(483, 14)
        Me.btnPending.Name = "btnPending"
        Me.btnPending.Size = New System.Drawing.Size(116, 47)
        Me.btnPending.TabIndex = 22
        Me.btnPending.Text = "Pending List"
        Me.btnPending.UseVisualStyleBackColor = True
        '
        'btnAcctOCs
        '
        Me.btnAcctOCs.AllowDrop = True
        Me.btnAcctOCs.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctOCs.Location = New System.Drawing.Point(366, 155)
        Me.btnAcctOCs.Name = "btnAcctOCs"
        Me.btnAcctOCs.Size = New System.Drawing.Size(116, 47)
        Me.btnAcctOCs.TabIndex = 21
        Me.btnAcctOCs.Text = "S-Account Ordering Codes"
        Me.btnAcctOCs.UseVisualStyleBackColor = True
        '
        'btnAcctOC
        '
        Me.btnAcctOC.AllowDrop = True
        Me.btnAcctOC.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctOC.Location = New System.Drawing.Point(367, 108)
        Me.btnAcctOC.Name = "btnAcctOC"
        Me.btnAcctOC.Size = New System.Drawing.Size(116, 47)
        Me.btnAcctOC.TabIndex = 20
        Me.btnAcctOC.Text = "Account Ordering Codes"
        Me.btnAcctOC.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.AllowDrop = True
        Me.Button5.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(367, 61)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(116, 47)
        Me.Button5.TabIndex = 19
        Me.Button5.Text = "Reverse Ordering Code"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'btnAbbrv
        '
        Me.btnAbbrv.AllowDrop = True
        Me.btnAbbrv.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAbbrv.Location = New System.Drawing.Point(367, 14)
        Me.btnAbbrv.Name = "btnAbbrv"
        Me.btnAbbrv.Size = New System.Drawing.Size(116, 47)
        Me.btnAbbrv.TabIndex = 18
        Me.btnAbbrv.Text = "Test Abbreviations"
        Me.btnAbbrv.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.AllowDrop = True
        Me.Button4.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(366, 202)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(116, 47)
        Me.Button4.TabIndex = 17
        Me.Button4.Text = "Monthly Status Codes "
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btnstatcodeq
        '
        Me.btnstatcodeq.AllowDrop = True
        Me.btnstatcodeq.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnstatcodeq.Location = New System.Drawing.Point(250, 202)
        Me.btnstatcodeq.Name = "btnstatcodeq"
        Me.btnstatcodeq.Size = New System.Drawing.Size(116, 47)
        Me.btnstatcodeq.TabIndex = 16
        Me.btnstatcodeq.Text = "Status Code Queue"
        Me.btnstatcodeq.UseVisualStyleBackColor = True
        '
        'btnaffdef
        '
        Me.btnaffdef.AllowDrop = True
        Me.btnaffdef.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaffdef.Location = New System.Drawing.Point(134, 202)
        Me.btnaffdef.Name = "btnaffdef"
        Me.btnaffdef.Size = New System.Drawing.Size(116, 47)
        Me.btnaffdef.TabIndex = 15
        Me.btnaffdef.Text = "Affidavit Definition"
        Me.btnaffdef.UseVisualStyleBackColor = True
        '
        'btnTrackRpt
        '
        Me.btnTrackRpt.AllowDrop = True
        Me.btnTrackRpt.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTrackRpt.Location = New System.Drawing.Point(18, 202)
        Me.btnTrackRpt.Name = "btnTrackRpt"
        Me.btnTrackRpt.Size = New System.Drawing.Size(116, 47)
        Me.btnTrackRpt.TabIndex = 14
        Me.btnTrackRpt.Text = "Tracking Report"
        Me.btnTrackRpt.UseVisualStyleBackColor = True
        '
        'btnTCalc
        '
        Me.btnTCalc.AllowDrop = True
        Me.btnTCalc.Enabled = False
        Me.btnTCalc.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTCalc.Location = New System.Drawing.Point(716, 202)
        Me.btnTCalc.Name = "btnTCalc"
        Me.btnTCalc.Size = New System.Drawing.Size(116, 47)
        Me.btnTCalc.TabIndex = 13
        Me.btnTCalc.Text = "Testosterone Calc"
        Me.btnTCalc.UseVisualStyleBackColor = True
        Me.btnTCalc.Visible = False
        '
        'btnEMRDef
        '
        Me.btnEMRDef.AllowDrop = True
        Me.btnEMRDef.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEMRDef.Location = New System.Drawing.Point(134, 155)
        Me.btnEMRDef.Name = "btnEMRDef"
        Me.btnEMRDef.Size = New System.Drawing.Size(116, 47)
        Me.btnEMRDef.TabIndex = 12
        Me.btnEMRDef.Text = "EMR Definition"
        Me.btnEMRDef.UseVisualStyleBackColor = True
        '
        'btnEMRList
        '
        Me.btnEMRList.AllowDrop = True
        Me.btnEMRList.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEMRList.Location = New System.Drawing.Point(18, 155)
        Me.btnEMRList.Name = "btnEMRList"
        Me.btnEMRList.Size = New System.Drawing.Size(116, 47)
        Me.btnEMRList.TabIndex = 11
        Me.btnEMRList.Text = "EMR List"
        Me.btnEMRList.UseVisualStyleBackColor = True
        '
        'btnAcctScan
        '
        Me.btnAcctScan.AllowDrop = True
        Me.btnAcctScan.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctScan.Location = New System.Drawing.Point(251, 108)
        Me.btnAcctScan.Name = "btnAcctScan"
        Me.btnAcctScan.Size = New System.Drawing.Size(116, 47)
        Me.btnAcctScan.TabIndex = 10
        Me.btnAcctScan.Text = "Account Scan"
        Me.btnAcctScan.UseVisualStyleBackColor = True
        '
        'btnAcctSum
        '
        Me.btnAcctSum.AllowDrop = True
        Me.btnAcctSum.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctSum.Location = New System.Drawing.Point(251, 61)
        Me.btnAcctSum.Name = "btnAcctSum"
        Me.btnAcctSum.Size = New System.Drawing.Size(116, 47)
        Me.btnAcctSum.TabIndex = 9
        Me.btnAcctSum.Text = "Account Summary"
        Me.btnAcctSum.UseVisualStyleBackColor = True
        '
        'btnLabel
        '
        Me.btnLabel.AllowDrop = True
        Me.btnLabel.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLabel.Location = New System.Drawing.Point(251, 14)
        Me.btnLabel.Name = "btnLabel"
        Me.btnLabel.Size = New System.Drawing.Size(116, 47)
        Me.btnLabel.TabIndex = 8
        Me.btnLabel.Text = "Print Generic Label"
        Me.btnLabel.UseVisualStyleBackColor = True
        '
        'btnChiral
        '
        Me.btnChiral.AllowDrop = True
        Me.btnChiral.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChiral.Location = New System.Drawing.Point(135, 108)
        Me.btnChiral.Name = "btnChiral"
        Me.btnChiral.Size = New System.Drawing.Size(116, 47)
        Me.btnChiral.TabIndex = 7
        Me.btnChiral.Text = "Chiral List"
        Me.btnChiral.UseVisualStyleBackColor = True
        '
        'btnMonStat
        '
        Me.btnMonStat.AllowDrop = True
        Me.btnMonStat.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMonStat.Location = New System.Drawing.Point(135, 61)
        Me.btnMonStat.Name = "btnMonStat"
        Me.btnMonStat.Size = New System.Drawing.Size(116, 47)
        Me.btnMonStat.TabIndex = 5
        Me.btnMonStat.Text = "Monthly Status"
        Me.btnMonStat.UseVisualStyleBackColor = True
        '
        'btnAddMeds
        '
        Me.btnAddMeds.Location = New System.Drawing.Point(749, 241)
        Me.btnAddMeds.Name = "btnAddMeds"
        Me.btnAddMeds.Size = New System.Drawing.Size(59, 31)
        Me.btnAddMeds.TabIndex = 4
        Me.btnAddMeds.Text = "Add Medications"
        Me.btnAddMeds.UseVisualStyleBackColor = True
        Me.btnAddMeds.Visible = False
        '
        'btnPhyInq
        '
        Me.btnPhyInq.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPhyInq.Location = New System.Drawing.Point(18, 108)
        Me.btnPhyInq.Name = "btnPhyInq"
        Me.btnPhyInq.Size = New System.Drawing.Size(116, 47)
        Me.btnPhyInq.TabIndex = 3
        Me.btnPhyInq.Text = "Physician Inquiry"
        Me.btnPhyInq.UseVisualStyleBackColor = True
        '
        'btnAcctInq
        '
        Me.btnAcctInq.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctInq.Location = New System.Drawing.Point(18, 61)
        Me.btnAcctInq.Name = "btnAcctInq"
        Me.btnAcctInq.Size = New System.Drawing.Size(116, 47)
        Me.btnAcctInq.TabIndex = 2
        Me.btnAcctInq.Text = "Account Inquiry"
        Me.btnAcctInq.UseVisualStyleBackColor = True
        '
        'btnOCInq
        '
        Me.btnOCInq.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOCInq.Location = New System.Drawing.Point(18, 14)
        Me.btnOCInq.Name = "btnOCInq"
        Me.btnOCInq.Size = New System.Drawing.Size(116, 47)
        Me.btnOCInq.TabIndex = 1
        Me.btnOCInq.Text = "Order Code Inq"
        Me.btnOCInq.UseVisualStyleBackColor = True
        '
        'btnBadFlag
        '
        Me.btnBadFlag.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBadFlag.Location = New System.Drawing.Point(135, 14)
        Me.btnBadFlag.Name = "btnBadFlag"
        Me.btnBadFlag.Size = New System.Drawing.Size(116, 47)
        Me.btnBadFlag.TabIndex = 0
        Me.btnBadFlag.Text = "Status Codes"
        Me.btnBadFlag.UseVisualStyleBackColor = True
        '
        'Proc
        '
        Me.Proc.Controls.Add(Me.btnEVW)
        Me.Proc.Controls.Add(Me.btnSpcDis)
        Me.Proc.Controls.Add(Me.Button6)
        Me.Proc.Controls.Add(Me.btnSpecScan)
        Me.Proc.Controls.Add(Me.btnCBC)
        Me.Proc.Controls.Add(Me.btnScanCourier)
        Me.Proc.Controls.Add(Me.btnSO)
        Me.Proc.Controls.Add(Me.btnReset)
        Me.Proc.Controls.Add(Me.btnLabels)
        Me.Proc.Controls.Add(Me.btnOE)
        Me.Proc.Location = New System.Drawing.Point(4, 26)
        Me.Proc.Name = "Proc"
        Me.Proc.Padding = New System.Windows.Forms.Padding(3)
        Me.Proc.Size = New System.Drawing.Size(838, 278)
        Me.Proc.TabIndex = 0
        Me.Proc.Text = "Processing"
        Me.Proc.UseVisualStyleBackColor = True
        '
        'btnEVW
        '
        Me.btnEVW.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEVW.Location = New System.Drawing.Point(324, 45)
        Me.btnEVW.Name = "btnEVW"
        Me.btnEVW.Size = New System.Drawing.Size(80, 46)
        Me.btnEVW.TabIndex = 10
        Me.btnEVW.Text = "EverlyWell Order Entry"
        Me.btnEVW.UseVisualStyleBackColor = True
        '
        'btnSpcDis
        '
        Me.btnSpcDis.Location = New System.Drawing.Point(104, 169)
        Me.btnSpcDis.Name = "btnSpcDis"
        Me.btnSpcDis.Size = New System.Drawing.Size(80, 46)
        Me.btnSpcDis.TabIndex = 9
        Me.btnSpcDis.Text = "Specimen Disposal"
        Me.btnSpcDis.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(205, 43)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(80, 46)
        Me.Button6.TabIndex = 8
        Me.Button6.Text = "Edit Insurance"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'btnSpecScan
        '
        Me.btnSpecScan.Location = New System.Drawing.Point(104, 42)
        Me.btnSpecScan.Name = "btnSpecScan"
        Me.btnSpecScan.Size = New System.Drawing.Size(80, 46)
        Me.btnSpecScan.TabIndex = 7
        Me.btnSpecScan.Text = "Add to Spec Scan"
        Me.btnSpecScan.UseVisualStyleBackColor = True
        Me.btnSpecScan.Visible = False
        '
        'btnCBC
        '
        Me.btnCBC.Location = New System.Drawing.Point(27, 169)
        Me.btnCBC.Name = "btnCBC"
        Me.btnCBC.Size = New System.Drawing.Size(80, 46)
        Me.btnCBC.TabIndex = 6
        Me.btnCBC.Text = "CBC by Tray"
        Me.btnCBC.UseVisualStyleBackColor = True
        Me.btnCBC.Visible = False
        '
        'btnScanCourier
        '
        Me.btnScanCourier.Location = New System.Drawing.Point(27, 127)
        Me.btnScanCourier.Name = "btnScanCourier"
        Me.btnScanCourier.Size = New System.Drawing.Size(80, 46)
        Me.btnScanCourier.TabIndex = 5
        Me.btnScanCourier.Text = "Scan Courier"
        Me.btnScanCourier.UseVisualStyleBackColor = True
        '
        'btnSO
        '
        Me.btnSO.Location = New System.Drawing.Point(27, 85)
        Me.btnSO.Name = "btnSO"
        Me.btnSO.Size = New System.Drawing.Size(80, 46)
        Me.btnSO.TabIndex = 4
        Me.btnSO.Text = "Send Outs"
        Me.btnSO.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(509, 115)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(66, 26)
        Me.btnReset.TabIndex = 3
        Me.btnReset.Text = "Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        Me.btnReset.Visible = False
        '
        'btnLabels
        '
        Me.btnLabels.Location = New System.Drawing.Point(495, 42)
        Me.btnLabels.Name = "btnLabels"
        Me.btnLabels.Size = New System.Drawing.Size(80, 49)
        Me.btnLabels.TabIndex = 2
        Me.btnLabels.Text = "Print Labels"
        Me.btnLabels.UseVisualStyleBackColor = True
        Me.btnLabels.Visible = False
        '
        'btnOE
        '
        Me.btnOE.Location = New System.Drawing.Point(27, 41)
        Me.btnOE.Name = "btnOE"
        Me.btnOE.Size = New System.Drawing.Size(80, 46)
        Me.btnOE.TabIndex = 1
        Me.btnOE.Text = "Order Entry"
        Me.btnOE.UseVisualStyleBackColor = True
        '
        'SpecPrep
        '
        Me.SpecPrep.Controls.Add(Me.Button3)
        Me.SpecPrep.Controls.Add(Me.btnHam)
        Me.SpecPrep.Controls.Add(Me.btnEPMotionETG)
        Me.SpecPrep.Controls.Add(Me.btnEPMotionTHC)
        Me.SpecPrep.Controls.Add(Me.btnMBNLbl)
        Me.SpecPrep.Controls.Add(Me.btnBuildPain)
        Me.SpecPrep.Controls.Add(Me.btnEPMotion)
        Me.SpecPrep.Location = New System.Drawing.Point(4, 26)
        Me.SpecPrep.Name = "SpecPrep"
        Me.SpecPrep.Padding = New System.Windows.Forms.Padding(3)
        Me.SpecPrep.Size = New System.Drawing.Size(838, 278)
        Me.SpecPrep.TabIndex = 1
        Me.SpecPrep.Text = "Specimen Prep"
        Me.SpecPrep.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(266, 77)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(121, 48)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Build NMR Batch from CSV"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnHam
        '
        Me.btnHam.Location = New System.Drawing.Point(146, 77)
        Me.btnHam.Name = "btnHam"
        Me.btnHam.Size = New System.Drawing.Size(121, 48)
        Me.btnHam.TabIndex = 5
        Me.btnHam.Text = "Build Grid from CSV"
        Me.btnHam.UseVisualStyleBackColor = True
        '
        'btnEPMotionETG
        '
        Me.btnEPMotionETG.Location = New System.Drawing.Point(266, 32)
        Me.btnEPMotionETG.Name = "btnEPMotionETG"
        Me.btnEPMotionETG.Size = New System.Drawing.Size(121, 48)
        Me.btnEPMotionETG.TabIndex = 4
        Me.btnEPMotionETG.Text = "Folder/Plate Build ETG"
        Me.btnEPMotionETG.UseVisualStyleBackColor = True
        '
        'btnEPMotionTHC
        '
        Me.btnEPMotionTHC.Location = New System.Drawing.Point(146, 32)
        Me.btnEPMotionTHC.Name = "btnEPMotionTHC"
        Me.btnEPMotionTHC.Size = New System.Drawing.Size(121, 48)
        Me.btnEPMotionTHC.TabIndex = 3
        Me.btnEPMotionTHC.Text = "Folder/Plate Build THC"
        Me.btnEPMotionTHC.UseVisualStyleBackColor = True
        '
        'btnMBNLbl
        '
        Me.btnMBNLbl.Location = New System.Drawing.Point(27, 124)
        Me.btnMBNLbl.Name = "btnMBNLbl"
        Me.btnMBNLbl.Size = New System.Drawing.Size(121, 48)
        Me.btnMBNLbl.TabIndex = 2
        Me.btnMBNLbl.Text = "Print Labels by MBN"
        Me.btnMBNLbl.UseVisualStyleBackColor = True
        '
        'btnBuildPain
        '
        Me.btnBuildPain.Location = New System.Drawing.Point(27, 77)
        Me.btnBuildPain.Name = "btnBuildPain"
        Me.btnBuildPain.Size = New System.Drawing.Size(121, 48)
        Me.btnBuildPain.TabIndex = 1
        Me.btnBuildPain.Text = "Build Worklist(s)"
        Me.btnBuildPain.UseVisualStyleBackColor = True
        '
        'btnEPMotion
        '
        Me.btnEPMotion.Location = New System.Drawing.Point(27, 32)
        Me.btnEPMotion.Name = "btnEPMotion"
        Me.btnEPMotion.Size = New System.Drawing.Size(121, 48)
        Me.btnEPMotion.TabIndex = 0
        Me.btnEPMotion.Text = "Folder/Plate Build"
        Me.btnEPMotion.UseVisualStyleBackColor = True
        '
        'Screen
        '
        Me.Screen.Controls.Add(Me.lblVer)
        Me.Screen.Location = New System.Drawing.Point(4, 26)
        Me.Screen.Name = "Screen"
        Me.Screen.Padding = New System.Windows.Forms.Padding(3)
        Me.Screen.Size = New System.Drawing.Size(838, 278)
        Me.Screen.TabIndex = 2
        Me.Screen.Text = "Screening"
        Me.Screen.UseVisualStyleBackColor = True
        '
        'lblVer
        '
        Me.lblVer.AutoSize = True
        Me.lblVer.Location = New System.Drawing.Point(648, 244)
        Me.lblVer.Name = "lblVer"
        Me.lblVer.Size = New System.Drawing.Size(16, 17)
        Me.lblVer.TabIndex = 0
        Me.lblVer.Text = "A"
        '
        'LCMS
        '
        Me.LCMS.Controls.Add(Me.btnTrayClip)
        Me.LCMS.Controls.Add(Me.btnRelTray)
        Me.LCMS.Controls.Add(Me.btnHist)
        Me.LCMS.Controls.Add(Me.Button12)
        Me.LCMS.Controls.Add(Me.btnSpecRel)
        Me.LCMS.Controls.Add(Me.btnPrintSVT)
        Me.LCMS.Controls.Add(Me.btnBatchRev)
        Me.LCMS.Controls.Add(Me.btnFldrClip)
        Me.LCMS.Controls.Add(Me.btnSpecRev)
        Me.LCMS.Controls.Add(Me.btnMBNClip)
        Me.LCMS.Controls.Add(Me.Button1)
        Me.LCMS.Controls.Add(Me.btnFldrAglnt)
        Me.LCMS.Controls.Add(Me.btnPrintMBN)
        Me.LCMS.Controls.Add(Me.btnPrtWrklst)
        Me.LCMS.Controls.Add(Me.Button2)
        Me.LCMS.Location = New System.Drawing.Point(4, 26)
        Me.LCMS.Name = "LCMS"
        Me.LCMS.Padding = New System.Windows.Forms.Padding(3)
        Me.LCMS.Size = New System.Drawing.Size(838, 278)
        Me.LCMS.TabIndex = 3
        Me.LCMS.Text = "LCMS"
        Me.LCMS.UseVisualStyleBackColor = True
        '
        'btnTrayClip
        '
        Me.btnTrayClip.Location = New System.Drawing.Point(34, 205)
        Me.btnTrayClip.Name = "btnTrayClip"
        Me.btnTrayClip.Size = New System.Drawing.Size(107, 47)
        Me.btnTrayClip.TabIndex = 15
        Me.btnTrayClip.Text = "Tray to Clipboard"
        Me.btnTrayClip.UseVisualStyleBackColor = True
        '
        'btnRelTray
        '
        Me.btnRelTray.Location = New System.Drawing.Point(184, 205)
        Me.btnRelTray.Name = "btnRelTray"
        Me.btnRelTray.Size = New System.Drawing.Size(99, 47)
        Me.btnRelTray.TabIndex = 14
        Me.btnRelTray.Text = "Tray Release"
        Me.btnRelTray.UseVisualStyleBackColor = True
        '
        'btnHist
        '
        Me.btnHist.Location = New System.Drawing.Point(579, 30)
        Me.btnHist.Name = "btnHist"
        Me.btnHist.Size = New System.Drawing.Size(96, 47)
        Me.btnHist.TabIndex = 13
        Me.btnHist.Text = "History"
        Me.btnHist.UseVisualStyleBackColor = True
        Me.btnHist.Visible = False
        '
        'Button12
        '
        Me.Button12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.Location = New System.Drawing.Point(282, 163)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(99, 47)
        Me.Button12.TabIndex = 12
        Me.Button12.Text = "Remove Specimen from Batches"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'btnSpecRel
        '
        Me.btnSpecRel.Location = New System.Drawing.Point(184, 163)
        Me.btnSpecRel.Name = "btnSpecRel"
        Me.btnSpecRel.Size = New System.Drawing.Size(99, 47)
        Me.btnSpecRel.TabIndex = 11
        Me.btnSpecRel.Text = "Specimen Release"
        Me.btnSpecRel.UseVisualStyleBackColor = True
        '
        'btnPrintSVT
        '
        Me.btnPrintSVT.Location = New System.Drawing.Point(282, 76)
        Me.btnPrintSVT.Name = "btnPrintSVT"
        Me.btnPrintSVT.Size = New System.Drawing.Size(109, 47)
        Me.btnPrintSVT.TabIndex = 10
        Me.btnPrintSVT.Text = "Abnormal SVT"
        Me.btnPrintSVT.UseVisualStyleBackColor = True
        '
        'btnBatchRev
        '
        Me.btnBatchRev.Location = New System.Drawing.Point(448, 30)
        Me.btnBatchRev.Name = "btnBatchRev"
        Me.btnBatchRev.Size = New System.Drawing.Size(96, 47)
        Me.btnBatchRev.TabIndex = 9
        Me.btnBatchRev.Text = "Batch Review"
        Me.btnBatchRev.UseVisualStyleBackColor = True
        '
        'btnFldrClip
        '
        Me.btnFldrClip.Location = New System.Drawing.Point(34, 163)
        Me.btnFldrClip.Name = "btnFldrClip"
        Me.btnFldrClip.Size = New System.Drawing.Size(107, 47)
        Me.btnFldrClip.TabIndex = 8
        Me.btnFldrClip.Text = "Folder to Clipboard"
        Me.btnFldrClip.UseVisualStyleBackColor = True
        '
        'btnSpecRev
        '
        Me.btnSpecRev.Location = New System.Drawing.Point(448, 83)
        Me.btnSpecRev.Name = "btnSpecRev"
        Me.btnSpecRev.Size = New System.Drawing.Size(96, 47)
        Me.btnSpecRev.TabIndex = 7
        Me.btnSpecRev.Text = "Specimen Review"
        Me.btnSpecRev.UseVisualStyleBackColor = True
        Me.btnSpecRev.Visible = False
        '
        'btnMBNClip
        '
        Me.btnMBNClip.Location = New System.Drawing.Point(34, 121)
        Me.btnMBNClip.Name = "btnMBNClip"
        Me.btnMBNClip.Size = New System.Drawing.Size(107, 47)
        Me.btnMBNClip.TabIndex = 6
        Me.btnMBNClip.Text = "MBN to Clipboard"
        Me.btnMBNClip.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(282, 30)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 47)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Print  Oral Worklist"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnFldrAglnt
        '
        Me.btnFldrAglnt.Location = New System.Drawing.Point(34, 76)
        Me.btnFldrAglnt.Name = "btnFldrAglnt"
        Me.btnFldrAglnt.Size = New System.Drawing.Size(107, 47)
        Me.btnFldrAglnt.TabIndex = 4
        Me.btnFldrAglnt.Text = "Folder to Agilent"
        Me.btnFldrAglnt.UseVisualStyleBackColor = True
        '
        'btnPrintMBN
        '
        Me.btnPrintMBN.Location = New System.Drawing.Point(184, 76)
        Me.btnPrintMBN.Name = "btnPrintMBN"
        Me.btnPrintMBN.Size = New System.Drawing.Size(99, 47)
        Me.btnPrintMBN.TabIndex = 3
        Me.btnPrintMBN.Text = "Print Chiral Location"
        Me.btnPrintMBN.UseVisualStyleBackColor = True
        '
        'btnPrtWrklst
        '
        Me.btnPrtWrklst.Location = New System.Drawing.Point(184, 30)
        Me.btnPrtWrklst.Name = "btnPrtWrklst"
        Me.btnPrtWrklst.Size = New System.Drawing.Size(99, 47)
        Me.btnPrtWrklst.TabIndex = 2
        Me.btnPrtWrklst.Text = "Print Worklist"
        Me.btnPrtWrklst.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(34, 30)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(107, 47)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Agilent Import"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Cardio
        '
        Me.Cardio.Controls.Add(Me.btnMonRpt)
        Me.Cardio.Controls.Add(Me.btnAcctMonRpt)
        Me.Cardio.Controls.Add(Me.btnORL)
        Me.Cardio.Controls.Add(Me.Button10)
        Me.Cardio.Controls.Add(Me.Button7)
        Me.Cardio.Controls.Add(Me.btnLocDef)
        Me.Cardio.Controls.Add(Me.btnVndDef)
        Me.Cardio.Controls.Add(Me.btnItemDef)
        Me.Cardio.Controls.Add(Me.btnQue)
        Me.Cardio.Controls.Add(Me.btnOrder)
        Me.Cardio.Location = New System.Drawing.Point(4, 26)
        Me.Cardio.Margin = New System.Windows.Forms.Padding(2)
        Me.Cardio.Name = "Cardio"
        Me.Cardio.Size = New System.Drawing.Size(838, 278)
        Me.Cardio.TabIndex = 6
        Me.Cardio.Text = "SOS"
        Me.Cardio.UseVisualStyleBackColor = True
        '
        'btnMonRpt
        '
        Me.btnMonRpt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMonRpt.Location = New System.Drawing.Point(408, 77)
        Me.btnMonRpt.Name = "btnMonRpt"
        Me.btnMonRpt.Size = New System.Drawing.Size(109, 52)
        Me.btnMonRpt.TabIndex = 18
        Me.btnMonRpt.Text = "Monthly Report"
        Me.btnMonRpt.UseVisualStyleBackColor = True
        '
        'btnAcctMonRpt
        '
        Me.btnAcctMonRpt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcctMonRpt.Location = New System.Drawing.Point(408, 28)
        Me.btnAcctMonRpt.Name = "btnAcctMonRpt"
        Me.btnAcctMonRpt.Size = New System.Drawing.Size(109, 52)
        Me.btnAcctMonRpt.TabIndex = 17
        Me.btnAcctMonRpt.Text = "Acct Mon Report"
        Me.btnAcctMonRpt.UseVisualStyleBackColor = True
        '
        'btnORL
        '
        Me.btnORL.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnORL.Location = New System.Drawing.Point(41, 173)
        Me.btnORL.Name = "btnORL"
        Me.btnORL.Size = New System.Drawing.Size(109, 52)
        Me.btnORL.TabIndex = 16
        Me.btnORL.Text = "Online Req Labels"
        Me.btnORL.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(271, 28)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(109, 52)
        Me.Button10.TabIndex = 15
        Me.Button10.Text = "Inventory System"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(156, 28)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(109, 52)
        Me.Button7.TabIndex = 14
        Me.Button7.Text = "Enter Req #s"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'btnLocDef
        '
        Me.btnLocDef.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLocDef.Location = New System.Drawing.Point(537, 125)
        Me.btnLocDef.Name = "btnLocDef"
        Me.btnLocDef.Size = New System.Drawing.Size(109, 52)
        Me.btnLocDef.TabIndex = 12
        Me.btnLocDef.Text = "Location Definition"
        Me.btnLocDef.UseVisualStyleBackColor = True
        '
        'btnVndDef
        '
        Me.btnVndDef.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVndDef.Location = New System.Drawing.Point(537, 77)
        Me.btnVndDef.Name = "btnVndDef"
        Me.btnVndDef.Size = New System.Drawing.Size(109, 52)
        Me.btnVndDef.TabIndex = 11
        Me.btnVndDef.Text = "Vendor Definition"
        Me.btnVndDef.UseVisualStyleBackColor = True
        '
        'btnItemDef
        '
        Me.btnItemDef.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnItemDef.Location = New System.Drawing.Point(537, 28)
        Me.btnItemDef.Name = "btnItemDef"
        Me.btnItemDef.Size = New System.Drawing.Size(109, 52)
        Me.btnItemDef.TabIndex = 10
        Me.btnItemDef.Text = "Item Definition"
        Me.btnItemDef.UseVisualStyleBackColor = True
        '
        'btnQue
        '
        Me.btnQue.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQue.Location = New System.Drawing.Point(41, 77)
        Me.btnQue.Name = "btnQue"
        Me.btnQue.Size = New System.Drawing.Size(109, 52)
        Me.btnQue.TabIndex = 9
        Me.btnQue.Text = "Queues"
        Me.btnQue.UseVisualStyleBackColor = True
        '
        'btnOrder
        '
        Me.btnOrder.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOrder.Location = New System.Drawing.Point(41, 28)
        Me.btnOrder.Name = "btnOrder"
        Me.btnOrder.Size = New System.Drawing.Size(109, 52)
        Me.btnOrder.TabIndex = 8
        Me.btnOrder.Text = "Order Supplies"
        Me.btnOrder.UseVisualStyleBackColor = True
        '
        'Info
        '
        Me.Info.Controls.Add(Me.btnInsQue)
        Me.Info.Controls.Add(Me.wbDailyInfo)
        Me.Info.Location = New System.Drawing.Point(4, 26)
        Me.Info.Margin = New System.Windows.Forms.Padding(2)
        Me.Info.Name = "Info"
        Me.Info.Size = New System.Drawing.Size(838, 278)
        Me.Info.TabIndex = 5
        Me.Info.Text = "Daily Info"
        Me.Info.UseVisualStyleBackColor = True
        '
        'btnInsQue
        '
        Me.btnInsQue.AllowDrop = True
        Me.btnInsQue.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInsQue.Location = New System.Drawing.Point(612, 3)
        Me.btnInsQue.Name = "btnInsQue"
        Me.btnInsQue.Size = New System.Drawing.Size(102, 61)
        Me.btnInsQue.TabIndex = 24
        Me.btnInsQue.Text = "Monitor Instrument Queues"
        Me.btnInsQue.UseVisualStyleBackColor = True
        '
        'wbDailyInfo
        '
        Me.wbDailyInfo.Location = New System.Drawing.Point(0, 0)
        Me.wbDailyInfo.Margin = New System.Windows.Forms.Padding(2)
        Me.wbDailyInfo.MinimumSize = New System.Drawing.Size(15, 16)
        Me.wbDailyInfo.Name = "wbDailyInfo"
        Me.wbDailyInfo.Size = New System.Drawing.Size(612, 276)
        Me.wbDailyInfo.TabIndex = 0
        '
        'Finance
        '
        Me.Finance.Controls.Add(Me.btnSpcNum)
        Me.Finance.Controls.Add(Me.Button11)
        Me.Finance.Controls.Add(Me.Button9)
        Me.Finance.Controls.Add(Me.btnAcc)
        Me.Finance.Controls.Add(Me.btnPay)
        Me.Finance.Location = New System.Drawing.Point(4, 26)
        Me.Finance.Name = "Finance"
        Me.Finance.Padding = New System.Windows.Forms.Padding(3)
        Me.Finance.Size = New System.Drawing.Size(838, 278)
        Me.Finance.TabIndex = 7
        Me.Finance.Text = "Finance"
        Me.Finance.UseVisualStyleBackColor = True
        '
        'btnSpcNum
        '
        Me.btnSpcNum.AllowDrop = True
        Me.btnSpcNum.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpcNum.Location = New System.Drawing.Point(176, 76)
        Me.btnSpcNum.Name = "btnSpcNum"
        Me.btnSpcNum.Size = New System.Drawing.Size(98, 47)
        Me.btnSpcNum.TabIndex = 28
        Me.btnSpcNum.Text = "Specimen Numbers"
        Me.btnSpcNum.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.AllowDrop = True
        Me.Button11.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.Location = New System.Drawing.Point(30, 119)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(109, 46)
        Me.Button11.TabIndex = 27
        Me.Button11.Text = "Accessions by Receive Date"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.AllowDrop = True
        Me.Button9.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(176, 32)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(98, 47)
        Me.Button9.TabIndex = 26
        Me.Button9.Text = "Acct-Sls-Proc"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'btnAcc
        '
        Me.btnAcc.AllowDrop = True
        Me.btnAcc.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAcc.Location = New System.Drawing.Point(30, 76)
        Me.btnAcc.Name = "btnAcc"
        Me.btnAcc.Size = New System.Drawing.Size(109, 47)
        Me.btnAcc.TabIndex = 25
        Me.btnAcc.Text = "Accessions"
        Me.btnAcc.UseVisualStyleBackColor = True
        '
        'btnPay
        '
        Me.btnPay.AllowDrop = True
        Me.btnPay.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPay.Location = New System.Drawing.Point(30, 32)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(109, 47)
        Me.btnPay.TabIndex = 24
        Me.btnPay.Text = "Payments"
        Me.btnPay.UseVisualStyleBackColor = True
        '
        'Molecular
        '
        Me.Molecular.Controls.Add(Me.Button17)
        Me.Molecular.Controls.Add(Me.btnBldCovidXLS)
        Me.Molecular.Controls.Add(Me.btnBldCovid)
        Me.Molecular.Controls.Add(Me.btnImpC)
        Me.Molecular.Controls.Add(Me.btnNNL)
        Me.Molecular.Controls.Add(Me.btnImpB)
        Me.Molecular.Controls.Add(Me.btnCutRng)
        Me.Molecular.Controls.Add(Me.btnImpA)
        Me.Molecular.Controls.Add(Me.Button16)
        Me.Molecular.Controls.Add(Me.Button15)
        Me.Molecular.Controls.Add(Me.Button14)
        Me.Molecular.Controls.Add(Me.Button13)
        Me.Molecular.Controls.Add(Me.btnAntiBiotic)
        Me.Molecular.Location = New System.Drawing.Point(4, 26)
        Me.Molecular.Name = "Molecular"
        Me.Molecular.Size = New System.Drawing.Size(838, 278)
        Me.Molecular.TabIndex = 8
        Me.Molecular.Text = "Molecular"
        Me.Molecular.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(473, 103)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(107, 47)
        Me.Button17.TabIndex = 39
        Me.Button17.Text = "Tray to Clipboard"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'btnBldCovidXLS
        '
        Me.btnBldCovidXLS.Location = New System.Drawing.Point(607, 77)
        Me.btnBldCovidXLS.Name = "btnBldCovidXLS"
        Me.btnBldCovidXLS.Size = New System.Drawing.Size(121, 48)
        Me.btnBldCovidXLS.TabIndex = 38
        Me.btnBldCovidXLS.Text = "Build COVID Worklist by XLS"
        Me.btnBldCovidXLS.UseVisualStyleBackColor = True
        '
        'btnBldCovid
        '
        Me.btnBldCovid.Location = New System.Drawing.Point(607, 32)
        Me.btnBldCovid.Name = "btnBldCovid"
        Me.btnBldCovid.Size = New System.Drawing.Size(121, 48)
        Me.btnBldCovid.TabIndex = 37
        Me.btnBldCovid.Text = "Build COVID Worklist by Tray"
        Me.btnBldCovid.UseVisualStyleBackColor = True
        '
        'btnImpC
        '
        Me.btnImpC.AllowDrop = True
        Me.btnImpC.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImpC.Location = New System.Drawing.Point(335, 119)
        Me.btnImpC.Name = "btnImpC"
        Me.btnImpC.Size = New System.Drawing.Size(98, 47)
        Me.btnImpC.TabIndex = 36
        Me.btnImpC.Text = "Import QuantStudio"
        Me.btnImpC.UseVisualStyleBackColor = True
        '
        'btnNNL
        '
        Me.btnNNL.AllowDrop = True
        Me.btnNNL.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNNL.Location = New System.Drawing.Point(473, 32)
        Me.btnNNL.Name = "btnNNL"
        Me.btnNNL.Size = New System.Drawing.Size(98, 47)
        Me.btnNNL.TabIndex = 35
        Me.btnNNL.Text = "Non-negative List"
        Me.btnNNL.UseVisualStyleBackColor = True
        '
        'btnImpB
        '
        Me.btnImpB.AllowDrop = True
        Me.btnImpB.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImpB.Location = New System.Drawing.Point(335, 75)
        Me.btnImpB.Name = "btnImpB"
        Me.btnImpB.Size = New System.Drawing.Size(98, 47)
        Me.btnImpB.TabIndex = 34
        Me.btnImpB.Text = "Import COVID-19"
        Me.btnImpB.UseVisualStyleBackColor = True
        '
        'btnCutRng
        '
        Me.btnCutRng.AllowDrop = True
        Me.btnCutRng.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCutRng.Location = New System.Drawing.Point(37, 163)
        Me.btnCutRng.Name = "btnCutRng"
        Me.btnCutRng.Size = New System.Drawing.Size(98, 47)
        Me.btnCutRng.TabIndex = 33
        Me.btnCutRng.Text = "Define Ranges"
        Me.btnCutRng.UseVisualStyleBackColor = True
        '
        'btnImpA
        '
        Me.btnImpA.AllowDrop = True
        Me.btnImpA.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImpA.Location = New System.Drawing.Point(335, 32)
        Me.btnImpA.Name = "btnImpA"
        Me.btnImpA.Size = New System.Drawing.Size(98, 47)
        Me.btnImpA.TabIndex = 32
        Me.btnImpA.Text = "Import Molecular"
        Me.btnImpA.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.AllowDrop = True
        Me.Button16.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button16.Location = New System.Drawing.Point(185, 75)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(104, 47)
        Me.Button16.TabIndex = 31
        Me.Button16.Text = "Ab <--> Gene"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.AllowDrop = True
        Me.Button15.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button15.Location = New System.Drawing.Point(185, 32)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(104, 47)
        Me.Button15.TabIndex = 30
        Me.Button15.Text = "Ab <--> Org"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.AllowDrop = True
        Me.Button14.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.Location = New System.Drawing.Point(37, 119)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(98, 47)
        Me.Button14.TabIndex = 29
        Me.Button14.Text = "Define Genes"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.AllowDrop = True
        Me.Button13.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.Location = New System.Drawing.Point(37, 75)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(98, 47)
        Me.Button13.TabIndex = 28
        Me.Button13.Text = "Define Organisms"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'btnAntiBiotic
        '
        Me.btnAntiBiotic.AllowDrop = True
        Me.btnAntiBiotic.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAntiBiotic.Location = New System.Drawing.Point(37, 32)
        Me.btnAntiBiotic.Name = "btnAntiBiotic"
        Me.btnAntiBiotic.Size = New System.Drawing.Size(98, 47)
        Me.btnAntiBiotic.TabIndex = 27
        Me.btnAntiBiotic.Text = "Define Antibiotic"
        Me.btnAntiBiotic.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(625, 323)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 47)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSpcInq
        '
        Me.btnSpcInq.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpcInq.Location = New System.Drawing.Point(7, 325)
        Me.btnSpcInq.Name = "btnSpcInq"
        Me.btnSpcInq.Size = New System.Drawing.Size(97, 47)
        Me.btnSpcInq.TabIndex = 2
        Me.btnSpcInq.Text = "Specimen Inquiry"
        Me.btnSpcInq.UseVisualStyleBackColor = True
        '
        'DailyInfo
        '
        Me.DailyInfo.Interval = 20000
        '
        'TrackBar1
        '
        Me.TrackBar1.AutoSize = False
        Me.TrackBar1.LargeChange = 1
        Me.TrackBar1.Location = New System.Drawing.Point(122, 323)
        Me.TrackBar1.Margin = New System.Windows.Forms.Padding(2)
        Me.TrackBar1.Maximum = 30
        Me.TrackBar1.Minimum = 1
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.Size = New System.Drawing.Size(478, 27)
        Me.TrackBar1.TabIndex = 3
        Me.TrackBar1.Value = 1
        '
        'lblExt
        '
        Me.lblExt.AutoSize = True
        Me.lblExt.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblExt.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExt.ForeColor = System.Drawing.Color.White
        Me.lblExt.Location = New System.Drawing.Point(179, 355)
        Me.lblExt.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblExt.Name = "lblExt"
        Me.lblExt.Size = New System.Drawing.Size(0, 17)
        Me.lblExt.TabIndex = 4
        '
        'CLSLIMS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(879, 384)
        Me.Controls.Add(Me.lblExt)
        Me.Controls.Add(Me.TrackBar1)
        Me.Controls.Add(Me.btnSpcInq)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.MenuTab)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "CLSLIMS"
        Me.Text = "Compass Lab Services"
        Me.MenuTab.ResumeLayout(False)
        Me.Admin.ResumeLayout(False)
        Me.Proc.ResumeLayout(False)
        Me.SpecPrep.ResumeLayout(False)
        Me.Screen.ResumeLayout(False)
        Me.Screen.PerformLayout()
        Me.LCMS.ResumeLayout(False)
        Me.Cardio.ResumeLayout(False)
        Me.Info.ResumeLayout(False)
        Me.Finance.ResumeLayout(False)
        Me.Molecular.ResumeLayout(False)
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuTab As System.Windows.Forms.TabControl
    Friend WithEvents Proc As System.Windows.Forms.TabPage
    Friend WithEvents SpecPrep As System.Windows.Forms.TabPage
    Friend WithEvents Screen As System.Windows.Forms.TabPage
    Friend WithEvents LCMS As System.Windows.Forms.TabPage
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Admin As System.Windows.Forms.TabPage
    Friend WithEvents btnBadFlag As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnOCInq As System.Windows.Forms.Button
    Friend WithEvents btnAcctInq As System.Windows.Forms.Button
    Friend WithEvents btnPhyInq As System.Windows.Forms.Button
    Friend WithEvents btnSpcInq As System.Windows.Forms.Button
    Friend WithEvents btnEPMotion As System.Windows.Forms.Button
    Friend WithEvents btnAddMeds As System.Windows.Forms.Button
    Friend WithEvents btnMonStat As System.Windows.Forms.Button
    Friend WithEvents btnOE As System.Windows.Forms.Button
    Friend WithEvents btnLabels As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnBuildPain As System.Windows.Forms.Button
    Friend WithEvents btnPrtWrklst As System.Windows.Forms.Button
    Friend WithEvents btnPrintMBN As System.Windows.Forms.Button
    Friend WithEvents btnFldrAglnt As System.Windows.Forms.Button
    Friend WithEvents btnChiral As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Info As System.Windows.Forms.TabPage
    Friend WithEvents wbDailyInfo As System.Windows.Forms.WebBrowser
    Friend WithEvents DailyInfo As System.Windows.Forms.Timer
    Friend WithEvents btnMBNClip As System.Windows.Forms.Button
    Friend WithEvents btnLabel As System.Windows.Forms.Button
    Friend WithEvents btnMBNLbl As System.Windows.Forms.Button
    Friend WithEvents btnAcctSum As System.Windows.Forms.Button
    Friend WithEvents btnEPMotionTHC As System.Windows.Forms.Button
    Friend WithEvents btnEPMotionETG As System.Windows.Forms.Button
    Friend WithEvents btnSpecRev As System.Windows.Forms.Button
    Friend WithEvents btnFldrClip As System.Windows.Forms.Button
    Friend WithEvents btnSO As System.Windows.Forms.Button
    Friend WithEvents Cardio As System.Windows.Forms.TabPage
    Friend WithEvents btnHam As System.Windows.Forms.Button
    Friend WithEvents TrackBar1 As TrackBar
    Friend WithEvents lblExt As Label
    Friend WithEvents btnAcctScan As System.Windows.Forms.Button
    Friend WithEvents btnEMRList As System.Windows.Forms.Button
    Friend WithEvents btnEMRDef As System.Windows.Forms.Button
    Friend WithEvents btnTCalc As System.Windows.Forms.Button
    Friend WithEvents btnScanCourier As System.Windows.Forms.Button
    Friend WithEvents btnTrackRpt As System.Windows.Forms.Button
    Friend WithEvents btnCBC As System.Windows.Forms.Button
    Friend WithEvents Button3 As Button
    Friend WithEvents btnstatcodeq As Button
    Friend WithEvents btnaffdef As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents btnAbbrv As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents btnAcctOC As Button
    Friend WithEvents btnAcctOCs As Button
    Friend WithEvents btnPending As Button
    Friend WithEvents btnSpecScan As Button
    Friend WithEvents btnRebill As Button
    Friend WithEvents btnAddDrugDiag As Button
    Friend WithEvents btnAffScan As Button
    Friend WithEvents btnBatchRev As Button
    Friend WithEvents btnUrnSal As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Finance As TabPage
    Friend WithEvents btnPay As Button
    Friend WithEvents btnOrder As Button
    Friend WithEvents btnQue As Button
    Friend WithEvents btnIntVen As Button
    Friend WithEvents btnAcc As Button
    Friend WithEvents btnItemDef As Button
    Friend WithEvents btnVndDef As Button
    Friend WithEvents btnLocDef As Button
    Friend WithEvents lblVer As Label
    Friend WithEvents Button7 As Button
    Friend WithEvents btnPrintSVT As Button
    Friend WithEvents btnSpecRel As Button
    Friend WithEvents btnSlsProc As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents btnSlsProcLst As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents btnORL As Button
    Friend WithEvents btnFedxUPS As Button
    Friend WithEvents btnInsQue As Button
    Friend WithEvents Button11 As Button
    Friend WithEvents btnAnthem As Button
    Friend WithEvents btnMedRecScan As Button
    Friend WithEvents btnTAT As Button
    Friend WithEvents Button12 As Button
    Friend WithEvents btnSpcDis As Button
    Friend WithEvents Molecular As TabPage
    Friend WithEvents btnAntiBiotic As Button
    Friend WithEvents btnSpcNum As Button
    Friend WithEvents Button14 As Button
    Friend WithEvents Button13 As Button
    Friend WithEvents Button15 As Button
    Friend WithEvents Button16 As Button
    Friend WithEvents btnImpA As Button
    Friend WithEvents btnCutRng As Button
    Friend WithEvents btnAcctMonRpt As Button
    Friend WithEvents btnMonRpt As Button
    Friend WithEvents btnHist As Button
    Friend WithEvents btnAutoFax As Button
    Friend WithEvents btnEVW As Button
    Friend WithEvents btnImpB As Button
    Friend WithEvents btnNNL As Button
    Friend WithEvents btnRelTray As Button
    Friend WithEvents btnImpC As Button
    Friend WithEvents btnTrayClip As Button
    Friend WithEvents btnBldCovidXLS As Button
    Friend WithEvents btnBldCovid As Button
    Friend WithEvents Button17 As Button
End Class
