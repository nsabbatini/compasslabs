﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAutoFax
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnUpd = New System.Windows.Forms.Button()
        Me.tvInfo = New System.Windows.Forms.TreeView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpCompDt = New System.Windows.Forms.DateTimePicker()
        Me.btnMove = New System.Windows.Forms.Button()
        Me.txtJobID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(963, 379)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(91, 37)
        Me.btnExit.TabIndex = 0
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnUpd
        '
        Me.btnUpd.Location = New System.Drawing.Point(963, 10)
        Me.btnUpd.Name = "btnUpd"
        Me.btnUpd.Size = New System.Drawing.Size(91, 37)
        Me.btnUpd.TabIndex = 1
        Me.btnUpd.Text = "Update"
        Me.btnUpd.UseVisualStyleBackColor = True
        '
        'tvInfo
        '
        Me.tvInfo.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvInfo.Location = New System.Drawing.Point(12, 12)
        Me.tvInfo.Name = "tvInfo"
        Me.tvInfo.Size = New System.Drawing.Size(945, 406)
        Me.tvInfo.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(974, 86)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 45)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Complete    Date:"
        '
        'dtpCompDt
        '
        Me.dtpCompDt.CustomFormat = "MM/dd/yyyy"
        Me.dtpCompDt.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCompDt.Location = New System.Drawing.Point(963, 134)
        Me.dtpCompDt.Name = "dtpCompDt"
        Me.dtpCompDt.Size = New System.Drawing.Size(132, 26)
        Me.dtpCompDt.TabIndex = 8
        '
        'btnMove
        '
        Me.btnMove.Location = New System.Drawing.Point(963, 247)
        Me.btnMove.Name = "btnMove"
        Me.btnMove.Size = New System.Drawing.Size(122, 49)
        Me.btnMove.TabIndex = 9
        Me.btnMove.Text = "Move Failed to Complete"
        Me.btnMove.UseVisualStyleBackColor = True
        '
        'txtJobID
        '
        Me.txtJobID.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobID.Location = New System.Drawing.Point(963, 215)
        Me.txtJobID.Name = "txtJobID"
        Me.txtJobID.Size = New System.Drawing.Size(132, 26)
        Me.txtJobID.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(963, 193)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 19)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Job ID:"
        '
        'frmAutoFax
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1097, 428)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtJobID)
        Me.Controls.Add(Me.btnMove)
        Me.Controls.Add(Me.dtpCompDt)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tvInfo)
        Me.Controls.Add(Me.btnUpd)
        Me.Controls.Add(Me.btnExit)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmAutoFax"
        Me.Text = "Auto Fax Queue"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExit As Button
    Friend WithEvents btnUpd As Button
    Friend WithEvents tvInfo As TreeView
    Friend WithEvents Label1 As Label
    Friend WithEvents dtpCompDt As DateTimePicker
    Friend WithEvents btnMove As Button
    Friend WithEvents txtJobID As TextBox
    Friend WithEvents Label2 As Label
End Class
