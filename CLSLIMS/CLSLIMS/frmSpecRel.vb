﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmSpecRel

    Private Sub txtSpecNo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSpecNo.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        lstInfo.Items.Clear()
        Dim Name As String = ValidateSpec(txtSpecNo.Text)
        If Name <> "" Then
            lblName.Text = Name
        Else
            lblName.Text = "Not found."
            txtSpecNo.Focus()
            Exit Sub
        End If
    End Sub

    Public Function ValidateSpec(ByVal specno As String) As String
        ValidateSpec = ""
        Dim sel = "Select Pfname+' '+plname From PSPEC a, PDEM b Where a.puid=b.puid and a.pefdt=b.pefdt and psact=1 and pspecno = '" & specno & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            ValidateSpec = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmSpecRel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-NEDPC" Then CLSLIMS.UID = "NED.SABBATINI"
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "MARCIN.BARTCZAK" And CLSLIMS.UID <> "JOHN.SIDWELL" Then
            Me.Close()
            Me.Dispose()
        End If
        txtSpecNo.Focus()
    End Sub

    Private Sub btnRel_Click(sender As Object, e As EventArgs) Handles btnRel.Click
        lstInfo.Items.Clear()
        If ValidateSpec(txtSpecNo.Text) <> "" Then

        Else
            lblName.Text = "Invalid specimen number."
            txtSpecNo.Focus()
            txtSpecNo.Text = ""
            Exit Sub
        End If
        If Ready(txtSpecNo.Text) Then
            Verify(txtSpecNo.Text)
            txtSpecNo.Text = ""
            lblName.Text = ""
            lstInfo.Items.Add("Done!") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
            txtSpecNo.Focus()
        End If
    End Sub
    Public Sub Verify(ByVal specno As String)
        Dim upd As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        lstInfo.Items.Add("Verifying . . .")
        upd = "Update PRES Set PRSTAT=3,PRVID='" & CLSLIMS.UID & "',PRVDT='" & CLSLIMS.CDT & "' Where PSPECNO='" & specno & "' And PRSTAT <3 And PRACT=1"
        Cmd.CommandText = upd
        Cmd.ExecuteNonQuery()
        lstInfo.Items.Add("Removing from queues . . .") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        upd = "Delete From QPRES Where PSPECNO='" & specno & "'"
        Cmd.CommandText = upd
        Cmd.ExecuteNonQuery()
        upd = "Delete From QWL Where PSPECNO='" & specno & "'"
        Cmd.CommandText = upd
        Cmd.ExecuteNonQuery()
        If CheckQRPMON(specno) Then
            lstInfo.Items.Add("Adding to report monitor . . .") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
            upd = "Insert Into QRPMON Values ('" & CLSLIMS.CDT & "','" & specno & "')"
            Cmd.CommandText = upd
            Cmd.ExecuteNonQuery()
        End If
    End Sub
    Public Function Ready(ByVal spec As String) As Boolean
        Ready = True

        Dim StatCd As String = CheckStatusCode(spec)
        If StatCd <> "" Then
            lstInfo.Items.Add("Specimen has status code.")
            lblName.Text = StatCd : CLSLIMS.IntText = StatCd
            txtSpecNo.Text = ""
            txtSpecNo.Focus()
            Ready = False
            Exit Function
        End If
        Dim Res As String = ResultsNotEntered(spec)
        If Res <> "" Then
            lstInfo.Items.Add("Not all results entered.")
            lblName.Text = Res : CLSLIMS.IntText = Res
            txtSpecNo.Text = ""
            txtSpecNo.Focus()
            Ready = False
            Exit Function
        End If
        Dim Final As String = AlreadyVerified(spec)
        If Final <> "" Then
            lstInfo.Items.Add("All PAIN results verified.")
            lblName.Text = Final : CLSLIMS.IntText = Final
            txtSpecNo.Text = ""
            txtSpecNo.Focus()
            Ready = False
            Exit Function
        End If

    End Function
    Public Function CheckQRPMON(ByVal specno As String) As Boolean
        CheckQRPMON = False
        Dim sel = "Select Count(*) From QRPMON Where pspecno = '" & specno & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            If rs(0) = 0 Then
                CheckQRPMON = True
            End If
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function CheckStatusCode(ByVal specno As String) As String
        CheckStatusCode = ""
        Dim sel = "select a.statcd from statcd a, psstat b, pspec c where c.pspecno='" & specno & "' and psact=1 and b.pspecno=c.pspecno and a.statcd=b.statcd and b.adsq1=c.adsq1 and stcdnre=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            CheckStatusCode = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function

    Public Function ResultsNotEntered(ByVal specno As String) As String
        ResultsNotEntered = ""
        Dim sel = "select RABRV from pres a, rc b where pspecno='" & specno & "' and pract=1 and a.rc=b.rc and a.refdt=b.refdt and prstat < 2"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            ResultsNotEntered = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function AlreadyVerified(ByVal specno As String) As String
        AlreadyVerified = ""
        Dim sel = "select rname from pres a, rc b where pspecno='" & specno & "' and pract=1 and a.rc=b.rc and a.refdt=b.refdt and prstat < 3"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            AlreadyVerified = ""
        Else
            AlreadyVerified = "All results verified."
        End If
        rs.Close()
        Conn.Close()
    End Function
End Class