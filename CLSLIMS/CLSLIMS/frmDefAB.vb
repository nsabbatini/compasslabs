﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.ComponentModel

Public Class frmDefAB
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmDefAB_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblNew.Text = ""
        ReloadCombos()
    End Sub

    Public Sub ReloadCombos()
        cmbAB.Items.Clear()
        cmbCat.Items.Clear()
        cmbSubCat.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select Antibiotic from ANTIBIOTIC Where Active=1 Order by Antibiotic"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cmbAB.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select Distinct Category from ANTIBIOTIC Where Active=1 Order by Category"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbCat.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select Distinct SubCategory from ANTIBIOTIC Where Active=1 Order by SubCategory"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbSubCat.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()

    End Sub
    Private Sub cmbAB_Validating(sender As Object, e As CancelEventArgs) Handles cmbAB.Validating
        ValidateAB()
    End Sub
    Public Sub ValidateAB()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Dim rs As MySqlDataReader = Nothing
        Dim Src As String = "", Del As String = ""
        If Not cmbAB.Items.Contains(cmbAB.Text) Then
            lblNew.Text = "NEW"
        Else
            Cmd.CommandText = "Select Category, Subcategory, source,delivery from Antibiotic Where Antibiotic='" & cmbAB.Text & "' And active=1"
            rs = Cmd.ExecuteReader
            If rs.Read Then
                cmbCat.Text = rs(0) : cmbSubCat.Text = rs(1)
                Src = rs(2) : Del = rs(3)
                If InStr(Src, "RESP") Then cbResp.Checked = True
                If InStr(Src, "SSTI") Then cbSSTI.Checked = True
                If InStr(Src, "URN") Then cbUrn.Checked = True
                If InStr(Src, "SH") Then cbSH.Checked = True
                If InStr(Src, "EYE") Then cbEye.Checked = True
                If InStr(Del, "IV") Then cbIV.Checked = True
                If InStr(Del, "IM") Then cbIM.Checked = True
                If InStr(Del, "PO") Then cbPO.Checked = True
                If InStr(Del, "OPTH") Then cbOpth.Checked = True
            End If
            rs.Close()
            ConnMySql.Close()
        End If
        cmbAB.Enabled = False
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        ResetScrn()
    End Sub
    Public Sub ResetScrn()
        cmbAB.Enabled = True
        cmbAB.Text = ""
        cmbCat.Text = ""
        cmbSubCat.Text = ""
        cbEye.Checked = False
        cbIM.Checked = False
        cbIV.Checked = False
        cbOpth.Checked = False
        cbResp.Checked = False
        cbPO.Checked = False
        cbSH.Checked = False
        cbSSTI.Checked = False
        cbUrn.Checked = False
        lblNew.Text = ""
        ReloadCombos()
        cmbAB.Focus()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        'Check with Bill Bud on validations (at least one source, at least one delivery)
        Dim Src As String = "", Del As String = ""
        If cbResp.Checked Then Src = Src & "RESP" & "^"
        If cbSSTI.Checked Then Src = Src & "SSTI" & "^"
        If cbUrn.Checked Then Src = Src & "URN" & "^"
        If cbSH.Checked Then Src = Src & "SH" & "^"
        If cbEye.Checked Then Src = Src & "EYE"
        If cbIV.Checked Then Del = Del & "IV" & "^"
        If cbIM.Checked Then Del = Del & "IM" & "^"
        If cbPO.Checked Then Del = Del & "PO" & "^"
        If cbOpth.Checked Then Del = Del & "OPTH"
        Src = CLSLIMS.RemoveTrailingChar(Src, "^")
        Del = CLSLIMS.RemoveTrailingChar(Del, "^")
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        If lblNew.Text = "NEW" Then
            Cmd.CommandText = "Insert into ANTIBIOTIC values ('" & cmbAB.Text & "','" & cmbCat.Text & "','" & cmbSubCat.Text & "','" & Src & "','" & Del & "',1)"
        Else
            Cmd.CommandText = "Update ANTIBIOTIC Set Category='" & cmbCat.Text & "',SubCategory='" & cmbSubCat.Text & "',Source='" & Src & "',Delivery='" & Del & "' Where Antibiotic='" & cmbAB.Text & "' And Active=1"
        End If
        Cmd.ExecuteNonQuery()
        ConnMySql.Close()
        ResetScrn()
    End Sub

    Private Sub cmbAB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbAB.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        Else
            ValidateAB()
        End If
    End Sub

    Private Sub cmbAB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAB.SelectedIndexChanged
        ValidateAB()
    End Sub
End Class