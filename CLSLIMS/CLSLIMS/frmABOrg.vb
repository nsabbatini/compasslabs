﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.ComponentModel
Public Class frmABOrg
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Public Sub ReloadCombo()
        lstNonRes.Items.Clear()
        lstRes.Items.Clear()
        cmbAB.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select Antibiotic from ANTIBIOTIC Where Active=1 Order by Antibiotic"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cmbAB.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()

    End Sub

    Private Sub frmABOrg_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ReloadCombo()
    End Sub

    Private Sub cmbAB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAB.SelectedIndexChanged
        LoadListBoxes()
    End Sub
    Public Sub LoadListBoxes()
        lstRes.Items.Clear()
        lstNonRes.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select OGABBR from CROSSREF a, MOLECULAR b Where ANTIBIOTIC='" & cmbAB.Text & "' And SEQ=0 And a.TYPE='ORG' And a.OGNAME=b.OGNAME Order by OGABBR"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            lstRes.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select OGABBR from MOLECULAR Where TYPE='ORG' And OGNAME Not in (Select OGNAME From CROSSREF Where ANTIBIOTIC='" & cmbAB.Text & "' And SEQ=0 And TYPE='ORG')"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            lstNonRes.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()

    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        ReloadCombo()
    End Sub

    Public Sub MoveRightToLeft()
        If lstNonRes.SelectedItem = "" Then
            Exit Sub
        End If
        lstRes.Items.Add(lstNonRes.SelectedItem)
        lstNonRes.Items.Remove(lstNonRes.SelectedItem)
    End Sub
    Public Sub MoveLeftToRight()
        If lstRes.SelectedItem = "" Then
            Exit Sub
        End If
        lstNonRes.Items.Add(lstRes.SelectedItem)
        lstRes.Items.Remove(lstRes.SelectedItem)
    End Sub

    Private Sub btnLeft_Click(sender As Object, e As EventArgs) Handles btnLeft.Click
        MoveRightToLeft()
    End Sub

    Private Sub btnRight_Click(sender As Object, e As EventArgs) Handles btnRight.Click
        MoveLeftToRight()
    End Sub

    Private Sub lstRes_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lstRes.MouseDoubleClick
        MoveLeftToRight()
    End Sub

    Private Sub lstNonRes_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lstNonRes.MouseDoubleClick
        MoveRightToLeft()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim NxtSeq As Integer = 999999, OGName As String = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select MAX(SEQ)+1 from CROSSREF Where ANTIBIOTIC='" & cmbAB.Text & "' And TYPE='ORG'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            NxtSeq = IIf(IsDBNull(rs(0)), 1, rs(0))
        End If
        rs.Close()
        Cmd.CommandText = "Update CROSSREF Set SEQ=" & NxtSeq & " Where ANTIBIOTIC='" & cmbAB.Text & "' And SEQ=0 And TYPE='ORG'"
        Cmd.ExecuteNonQuery()
        For i = 0 To lstRes.Items.Count - 1
            OGName = GetName(lstRes.Items(i))
            Cmd.CommandText = "Insert into CROSSREF Values ('" & cmbAB.Text & "','" & OGName & "','ORG',0)"
            Cmd.ExecuteNonQuery()
        Next i
        ConnMySql.Close()
        ReloadCombo()
    End Sub
    Public Function GetName(ByVal org As String) As String
        GetName = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select OGNAME from MOLECULAR Where OGABBR='" & org & "' And TYPE='ORG'"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetName = rs(0)
        End If
        rs.Close()
        ConnMySql.Close()
    End Function
End Class