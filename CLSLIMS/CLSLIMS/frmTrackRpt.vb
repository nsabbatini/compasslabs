﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class frmTrackRpt

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
        Me.Dispose()
    End Sub

    Public Sub BuildTrack()
        Dim Sel As String, p As String, c As Integer = 1, strDate = DateTimePicker1.Text
        strDate = CLSLIMS.Piece(strDate, "/", 3) & CLSLIMS.Piece(strDate, "/", 1) & CLSLIMS.Piece(strDate, "/", 2)
        Sel = "Select * from TRACKING Where STATUS='InTransit' And Substring(ESTDELDATE,1,8)='" & strDate & "'"
        lstData.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySql)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            p = CLSLIMS.RJ("MISSING PKG", 12) & Space(3) & CLSLIMS.LJ(rs(5), 10) & Space(3) & CLSLIMS.LJ(rs(4), 7) & Space(3) & CLSLIMS.LJ(rs(3), 25) & Space(3) & CLSLIMS.LJ(rs(1), 10) & Space(5) & CLSLIMS.LJ(rs(2), 40) & Space(5) & CLSLIMS.LJ(rs(0), 10) & Space(2) & CLSLIMS.LJ(rs(7), 10)
            lstData.Items.Add(p)
            c = c + 1
        Loop
        rs.Close()
        Sel = "Select * From TRACKING Where CARRIER='UNKNOWN' And Substring(SCANDATE,1,8)='" & strDate & "'"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader
        Do While rs.Read()
            p = CLSLIMS.RJ(rs(6), 12) & Space(3) & CLSLIMS.LJ(CLSLIMS.Piece(rs(5), " ", 1), 10) & Space(3) & CLSLIMS.LJ(rs(4), 7) & Space(3) & CLSLIMS.LJ(rs(3), 25) & Space(3) & CLSLIMS.LJ(rs(1), 10) & Space(5) & CLSLIMS.LJ(rs(2), 40) & Space(5) & CLSLIMS.LJ(rs(0), 10) & Space(2) & CLSLIMS.LJ(rs(7), 10)
            lstData.Items.Add(p)
            c = c + 1
        Loop
        rs.Close()
        Sel = "Select * From TRACKING Where STATUS='RECV' And Substring(SCANDATE,1,8)='" & strDate & "' And CARRIER <> 'UNKNOWN'"
        Cmd.CommandText = Sel
        rs = Cmd.ExecuteReader
        Do While rs.Read()
            p = CLSLIMS.RJ(rs(6), 12) & Space(3) & CLSLIMS.LJ(CLSLIMS.Piece(rs(5), " ", 1), 10) & Space(3) & CLSLIMS.LJ(rs(4), 7) & Space(3) & CLSLIMS.LJ(rs(3), 25) & Space(3) & CLSLIMS.LJ(rs(1), 10) & Space(5) & CLSLIMS.LJ(rs(2), 40) & Space(5) & CLSLIMS.LJ(rs(0), 10) & Space(2) & CLSLIMS.LJ(rs(7), 10)
            lstData.Items.Add(p)
            c = c + 1
        Loop
        rs.Close()

        ConnMySql.Close()
        ConnMySql.Dispose()
    End Sub


    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 19

        Static PageNo As Integer = 1
        Dim y As Integer
        'e.Graphics.DrawString("Send Out Specimens" & IIf(cmbSO.Text = "Lab Corp", " - Lab Corp Account #: 41005270", ""), Font3, Brushes.Black, 150, 100)
        e.Graphics.DrawString("Specimen Tracking Report ", Font2, Brushes.Black, 250, 100)
        e.Graphics.DrawString("For " & DateTimePicker1.Text, Font3, Brushes.Black, 25, 158)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 690, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        'e.Graphics.DrawString("                                             Date of        Collection   SendOut", Font, Brushes.Black, 25, 185)
        e.Graphics.DrawString("Staus       Shipped     Courier    Tracking #                 Account        Account Name                                  Order       Type", Font, Brushes.Black, 25, 200)

        e.Graphics.DrawLine(Pens.Black, 25, 180, 1075, 180)   'Horizonal line above headings
        e.Graphics.DrawLine(Pens.Black, 25, 220, 1075, 220)   'Horizonal line under headings

        e.Graphics.DrawLine(Pens.Black, 100, 180, 100, 1080)   'Veritcal line 1
        e.Graphics.DrawLine(Pens.Black, 180, 180, 180, 1080)   'Veritcal line 2
        e.Graphics.DrawLine(Pens.Black, 250, 180, 250, 1080)   'Veritcal line 3
        e.Graphics.DrawLine(Pens.Black, 445, 180, 445, 1080)   'Vertical line 4
        e.Graphics.DrawLine(Pens.Black, 545, 180, 545, 1080)   'Vertical line 5
        e.Graphics.DrawLine(Pens.Black, 860, 180, 860, 1080)   'Veritcal line 6
        e.Graphics.DrawLine(Pens.Black, 938, 180, 938, 1080)   'Veritcal line 7


        Dim c As Integer
        Static intStart As Integer

        c = 1
        y = 225
        For i = intStart To lstData.Items.Count - 1

            'data line
            e.Graphics.DrawString(lstData.Items(i).ToString, Font, Brushes.Black, 1, y)
            e.Graphics.DrawLine(Pens.Black, 25, y + 20, 1075, y + 20) 'Horizonal line after sample
            y = y + 30
            c = c + 1
            If c > PageLngth Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i
    End Sub

    Private Sub SendOuts_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DateTimePicker1.Format = DateTimePickerFormat.Custom
        DateTimePicker1.CustomFormat = "MM/dd/yyyy"
        DateTimePicker1.Text = Now
    End Sub

    Private Sub btnPreview_Click_1(sender As System.Object, e As System.EventArgs) Handles btnPreview.Click
        Call BuildTrack()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = True

        PrintPreview.ShowDialog()

        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPrint_Click_1(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Call BuildTrack()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = True
        'PrintPrev.ShowDialog()
        PrintDoc.Print()

        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnExit_Click_1(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class