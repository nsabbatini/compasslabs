﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Base
Imports System.Globalization

Public Class frmPoHisotry
    Private _dirPathToFIle As String = System.AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private detailView As GridView = Nothing
    Private Sub frmPoHisotry_Load(sender As Object, e As EventArgs) Handles Me.Load
        GetPurchaseOrders()
    End Sub
    Private Sub GetPurchaseOrders()

        _fileName = _fileDir & GetPoHistorySql
        Dim dsMaster As New DataSet

        Using getHistory As New PurchaseOrders() With {._sqlText = ReadSqlFile(_fileName)}

            dsMaster = getHistory.GetPoHistoryDetails

            Dim keyColumn As DataColumn = dsMaster.Tables(0).Columns("pid")
            Dim foreignKeyColumn As DataColumn = dsMaster.Tables(1).Columns("pid")
            dsMaster.Relations.Add("POHistory", keyColumn, foreignKeyColumn)

            grdPoHistory.DataSource = dsMaster.Tables(0)
            grdPoHistory.ForceInitialize()


        End Using

    End Sub

    Private Sub grdPoHistory_ViewRegistered(sender As Object, e As ViewOperationEventArgs) Handles grdPoHistory.ViewRegistered
        Dim view As GridView = CType(e.View, GridView)

        For Each col As GridColumn In view.Columns
            If col.FieldName = "pid" Then
                col.Visible = False
            ElseIf col.FieldName = "did" Then
                col.Visible = False
            ElseIf col.FieldName = "itemid" Then
                col.Visible = False
            ElseIf col.FieldName = "received_qty" Then
                col.Visible = False
            ElseIf col.FieldName = "back_ordered" Then
                col.Visible = False
            ElseIf col.FieldName = "received" Then
                col.Visible = False
            ElseIf col.FieldName = "orderqty" Then
                col.Caption = "Ordered Qty"
            ElseIf col.FieldName = "itemcost" Then
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                col.DisplayFormat.FormatString = "{0:c}"
                col.Caption = "Total Item Cost"
            ElseIf col.FieldName = "unitprice" Then
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                col.DisplayFormat.FormatString = "{0:c}"
                col.Caption = "Per Unit Cost"
            End If
        Next
    End Sub

    Private Sub GridView1_MasterRowExpanded(sender As Object, e As CustomMasterRowEventArgs) Handles GridView1.MasterRowExpanded
        Dim view As GridView = TryCast(sender, GridView)
        If view IsNot Nothing Then
            detailView = TryCast(view.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
            If detailView IsNot Nothing Then
                detailView.ExpandGroupRow(-1)
            End If
        End If
    End Sub
End Class