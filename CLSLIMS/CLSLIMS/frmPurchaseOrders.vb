﻿Imports Compass.Global
Public Class frmPurchaseOrders
    Public _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private Sub btnNewPo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNewPo.ItemClick
        Using frm As New frmPurchaseOrderMain
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub btnReceiveInventory_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnReceiveInventory.ItemClick
        Using frm As New frmReceiveInventory
            frm.ShowDialog()
        End Using
    End Sub
    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        Using frm As New frmReceiveHistory
            frm.ShowDialog()
        End Using
    End Sub
    Private Sub btnSearchPos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSearchPos.ItemClick
        Using frm As New frmReconcilePo
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub btnOpenPos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOpenPos.ItemClick
        Using frm As New frmRptViewer
            frm._reportType = "OP"
            frm.ShowDialog()

        End Using
    End Sub

    Private Sub btnNewVendor_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNewVendor.ItemClick
        Using frm As New frmNewVendor
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        Using frm As New frmEditVendors
            frm.ShowDialog()

        End Using
    End Sub

    Private Sub btnPoHistory_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPoHistory.ItemClick
        Using frm As New frmPoHisotry
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Using frm As New frmUpdateInventory
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        Using frm As New frmNewInventoryItems
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub frmPurchaseOrders_Load(sender As Object, e As EventArgs) Handles Me.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If Not CLSLIMS.UID = "NED.SABBATINI" AndAlso Not CLSLIMS.UID = "REMOND.YOUNG" AndAlso Not CLSLIMS.UID = "L.MCLAUGHLIN" AndAlso Not CLSLIMS.UID = "KELLY.ELMORE" Then
            MsgBox("You do not have access rights to the Supply System!", MsgBoxStyle.Information, "Access Denied")
            Me.Close()
        Else
            LoggedinUserName = CLSLIMS.UID
        End If
    End Sub

    Private Sub btnPlaceOrder_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPlaceOrder.ItemClick
        Using frm As New frmMainOrderForm
            frm.ShowDialog()

        End Using
    End Sub

    Private Sub btnOrderQue_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOrderQue.ItemClick
        Using frm As New frmGetQuedOrdersb
            frm.ShowDialog()

        End Using
    End Sub

    Private Sub btnEditPo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEditPo.ItemClick
        Using frm As New frmPurchaseOrderEdit
            frm.ShowDialog()

        End Using
    End Sub

    Private Sub btnView_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnView.ItemClick
        Using frm As New frmViewPO
            frm.ShowDialog()
        End Using
    End Sub
End Class