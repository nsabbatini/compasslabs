﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.ComponentModel
Public Class frmDefGene
    Private Sub frmDefGene_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblNew.Text = ""
        ReloadCombos()
    End Sub
    Public Sub ReloadCombos()
        cmbGene.Items.Clear()
        cmbAbbr.Items.Clear()
        cmbCPT.Items.Clear()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select OGNAME from MOLECULAR Where Active=1 and TYPE='GENE' Order by OGNAME"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cmbGene.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select Distinct OGABBR from MOLECULAR Where Active=1 and TYPE='GENE' Order by OGABBR"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbAbbr.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select Distinct CPT from MOLECULAR Where Active=1 and TYPE='GENE' Order by CPT"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbCPT.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySql.Close()

    End Sub

    Private Sub cmbGene_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbGene.SelectedIndexChanged
        ValidateGene()
    End Sub

    Private Sub cmbGene_Validating(sender As Object, e As CancelEventArgs) Handles cmbGene.Validating
        ValidateGene()
    End Sub
    Public Sub ValidateGene()
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Dim rs As MySqlDataReader = Nothing
        Dim Src As String = "", Del As String = ""
        If Not cmbGene.Items.Contains(cmbGene.Text) Then
            lblNew.Text = "NEW"
        Else
            Cmd.CommandText = "Select OGABBR, CPT from MOLECULAR Where OGNAME='" & cmbGene.Text & "' And active=1 and TYPE='GENE'"
            rs = Cmd.ExecuteReader
            If rs.Read Then
                cmbAbbr.Text = rs(0) : cmbCPT.Text = rs(1)
            End If
            rs.Close()
            ConnMySql.Close()
        End If
        cmbGene.Enabled = False
    End Sub
    Public Sub ResetScrn()
        cmbGene.Enabled = True
        cmbGene.Text = ""
        cmbAbbr.Text = ""
        cmbCPT.Text = ""
        lblNew.Text = ""
        ReloadCombos()
        cmbGene.Focus()
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        ResetScrn()
    End Sub

    Private Sub cmbGene_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbGene.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        Else
            ValidateGene()
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        If lblNew.Text = "NEW" Then
            Cmd.CommandText = "Insert into MOLECULAR values ('" & cmbGene.Text & "','" & cmbAbbr.Text & "','" & cmbCPT.Text & "','GENE',1,'','')"
        Else
            Cmd.CommandText = "Update MOLECULAR Set OGABBR='" & cmbAbbr.Text & "',CPT='" & cmbCPT.Text & "' Where OGNAME='" & cmbGene.Text & "' And Active=1 and TYPE='GENE'"
        End If
        Cmd.ExecuteNonQuery()
        ConnMySql.Close()
        ResetScrn()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class