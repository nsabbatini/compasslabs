﻿Public Class CardioRpt

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Dim exe As String = ""
        If txtSpecNo.Text = "" Then
            MsgBox("No Specimen entered.")
        ElseIf Not QuickOE.CheckSpecNo(txtSpecNo.Text) Then
            MsgBox("Invalid Specimen Number")
        Else
            exe = "ClinRpt.exe " & txtSpecNo.Text & " N New"
            Shell(exe, AppWinStyle.NormalFocus)
            txtSpecNo.Text = ""
            Me.Dispose()
            Me.Close()
        End If
    End Sub
End Class