﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class PhyInq

    Private Sub TextBox1_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtPhy.KeyUp
        If e.KeyCode = Keys.Enter Then
            lstAcctPhy.Items.Clear()
            Dim Sel As String = "select PHYLNAME + ', ' + PHYFNAME + ' ' + PHYMNAME as DOC,PHY from DOCTOR where PHYLNAME like '" & txtPhy.Text & "%' and PHYACT=1 "
            If cbRes.Checked Then
                Sel = Sel & "And Substring(PHY,1,1)='R' order by DOC"
            Else
                Sel = Sel & "order by DOC"
            End If
            Dim Sel2 As String
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read
                'Check Acct association
                Sel2 = "select acct.acct,aname from acctphy, acct where acctphy.phy='" & rs(1) & "' and acctphy.acct=acct.acct and ACCTPHY.AEFDT = ACCT.AEFDT and AACT = 1"
                Dim Cmd2 As New SqlCommand(Sel2, Conn)
                Dim rs2 As SqlDataReader = Cmd2.ExecuteReader
                If rs2.Read Then
                    lstAcctPhy.Items.Add(rs(1) & Space(9 - Len(rs(1))) & rs(0) & Space(35 - Len(rs(0))) & rs2(0) & "-" & rs2(1))
                    Do While rs2.Read
                        lstAcctPhy.Items.Add(rs(1) & Space(9 - Len(rs(1))) & rs(0) & Space(35 - Len(rs(0))) & rs2(0) & "-" & rs2(1))
                    Loop
                Else
                    lstAcctPhy.Items.Add(rs(1) & Space(9 - Len(rs(1))) & rs(0) & Space(35 - Len(rs(0))) & "No Account")
                End If
                rs2.Close()
            Loop
            rs.Close()
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub PhyInq_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtPhy.Text = ""
        lstAcctPhy.Items.Clear()
        txtPhy.Focus()
    End Sub

    Private Sub lstAcctPhy_DoubleClick(sender As Object, e As System.EventArgs) Handles lstAcctPhy.DoubleClick

        CLSLIMS.AcctNum = CLSLIMS.Piece(Mid(lstAcctPhy.Text, 45), "-", 1)

        AcctInq.ShowDialog()

    End Sub

End Class