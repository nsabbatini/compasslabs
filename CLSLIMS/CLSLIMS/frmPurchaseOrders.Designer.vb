﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPurchaseOrders
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPurchaseOrders))
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.btnVendors = New DevExpress.XtraBars.BarButtonItem()
        Me.btnNewPo = New DevExpress.XtraBars.BarButtonItem()
        Me.btnSearchPos = New DevExpress.XtraBars.BarButtonItem()
        Me.btnItems = New DevExpress.XtraBars.BarButtonItem()
        Me.btnNewVendor = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnReceiveInventory = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnOpenPos = New DevExpress.XtraBars.BarButtonItem()
        Me.btnPoHistory = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnPlaceOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.btnOrderQue = New DevExpress.XtraBars.BarButtonItem()
        Me.btnOrderHistory = New DevExpress.XtraBars.BarButtonItem()
        Me.btnEditPo = New DevExpress.XtraBars.BarButtonItem()
        Me.btnView = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage3 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage4 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.RibbonControl.SearchEditItem, Me.btnVendors, Me.btnNewPo, Me.btnSearchPos, Me.btnItems, Me.btnNewVendor, Me.BarButtonItem2, Me.BarButtonItem3, Me.BarButtonItem4, Me.btnReceiveInventory, Me.BarButtonItem6, Me.BarButtonItem5, Me.btnOpenPos, Me.btnPoHistory, Me.BarButtonItem1, Me.btnPlaceOrder, Me.btnOrderQue, Me.btnOrderHistory, Me.btnEditPo, Me.btnView})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 20
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage1, Me.RibbonPage2, Me.RibbonPage3, Me.RibbonPage4})
        Me.RibbonControl.Size = New System.Drawing.Size(1212, 177)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'btnVendors
        '
        Me.btnVendors.Caption = "Vendors"
        Me.btnVendors.Id = 1
        Me.btnVendors.ImageOptions.SvgImage = CType(resources.GetObject("btnVendors.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnVendors.Name = "btnVendors"
        '
        'btnNewPo
        '
        Me.btnNewPo.Caption = "New Purchase Order"
        Me.btnNewPo.Id = 2
        Me.btnNewPo.ImageOptions.SvgImage = CType(resources.GetObject("btnNewPo.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnNewPo.LargeWidth = 150
        Me.btnNewPo.Name = "btnNewPo"
        '
        'btnSearchPos
        '
        Me.btnSearchPos.Caption = "Reconcile Purchase Orders"
        Me.btnSearchPos.Id = 3
        Me.btnSearchPos.ImageOptions.SvgImage = CType(resources.GetObject("btnSearchPos.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnSearchPos.LargeWidth = 150
        Me.btnSearchPos.Name = "btnSearchPos"
        '
        'btnItems
        '
        Me.btnItems.Caption = "Inventory Items"
        Me.btnItems.Id = 4
        Me.btnItems.ImageOptions.SvgImage = CType(resources.GetObject("btnItems.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnItems.Name = "btnItems"
        '
        'btnNewVendor
        '
        Me.btnNewVendor.Caption = "Add Vendor"
        Me.btnNewVendor.Id = 5
        Me.btnNewVendor.ImageOptions.SvgImage = CType(resources.GetObject("btnNewVendor.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnNewVendor.Name = "btnNewVendor"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Add / Edit Vendor"
        Me.BarButtonItem2.Id = 6
        Me.BarButtonItem2.ImageOptions.SvgImage = CType(resources.GetObject("BarButtonItem2.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.BarButtonItem2.LargeWidth = 100
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Add Inventory"
        Me.BarButtonItem3.Id = 7
        Me.BarButtonItem3.ImageOptions.SvgImage = CType(resources.GetObject("BarButtonItem3.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Add Inventory"
        Me.BarButtonItem4.Id = 8
        Me.BarButtonItem4.ImageOptions.SvgImage = CType(resources.GetObject("BarButtonItem4.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.BarButtonItem4.LargeWidth = 100
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'btnReceiveInventory
        '
        Me.btnReceiveInventory.Caption = "Receive"
        Me.btnReceiveInventory.Id = 9
        Me.btnReceiveInventory.ImageOptions.SvgImage = CType(resources.GetObject("btnReceiveInventory.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnReceiveInventory.LargeWidth = 100
        Me.btnReceiveInventory.Name = "btnReceiveInventory"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Update Inventory List"
        Me.BarButtonItem6.Id = 10
        Me.BarButtonItem6.ImageOptions.SvgImage = CType(resources.GetObject("BarButtonItem6.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Receive History"
        Me.BarButtonItem5.Id = 11
        Me.BarButtonItem5.ImageOptions.SvgImage = CType(resources.GetObject("BarButtonItem5.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.BarButtonItem5.LargeWidth = 100
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'btnOpenPos
        '
        Me.btnOpenPos.Caption = "Open Purchase Orders"
        Me.btnOpenPos.Hint = "Purchase orders that have not been reconciled"
        Me.btnOpenPos.Id = 12
        Me.btnOpenPos.ImageOptions.SvgImage = CType(resources.GetObject("btnOpenPos.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnOpenPos.LargeWidth = 150
        Me.btnOpenPos.Name = "btnOpenPos"
        '
        'btnPoHistory
        '
        Me.btnPoHistory.Caption = "Purchase Order History"
        Me.btnPoHistory.Id = 13
        Me.btnPoHistory.ImageOptions.SvgImage = CType(resources.GetObject("btnPoHistory.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnPoHistory.LargeWidth = 150
        Me.btnPoHistory.Name = "btnPoHistory"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Edit Inventory"
        Me.BarButtonItem1.Id = 14
        Me.BarButtonItem1.ImageOptions.SvgImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.BarButtonItem1.LargeWidth = 100
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'btnPlaceOrder
        '
        Me.btnPlaceOrder.Caption = "Order"
        Me.btnPlaceOrder.Id = 15
        Me.btnPlaceOrder.ImageOptions.SvgImage = CType(resources.GetObject("btnPlaceOrder.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnPlaceOrder.LargeWidth = 100
        Me.btnPlaceOrder.Name = "btnPlaceOrder"
        '
        'btnOrderQue
        '
        Me.btnOrderQue.Caption = "Order Que"
        Me.btnOrderQue.Id = 16
        Me.btnOrderQue.ImageOptions.SvgImage = CType(resources.GetObject("btnOrderQue.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnOrderQue.LargeWidth = 100
        Me.btnOrderQue.Name = "btnOrderQue"
        '
        'btnOrderHistory
        '
        Me.btnOrderHistory.Caption = "Order History"
        Me.btnOrderHistory.Id = 17
        Me.btnOrderHistory.ImageOptions.Image = CType(resources.GetObject("btnOrderHistory.ImageOptions.Image"), System.Drawing.Image)
        Me.btnOrderHistory.ImageOptions.LargeImage = CType(resources.GetObject("btnOrderHistory.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btnOrderHistory.LargeWidth = 100
        Me.btnOrderHistory.Name = "btnOrderHistory"
        '
        'btnEditPo
        '
        Me.btnEditPo.Caption = "Edit Purchase Order"
        Me.btnEditPo.Id = 18
        Me.btnEditPo.ImageOptions.SvgImage = CType(resources.GetObject("btnEditPo.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnEditPo.LargeWidth = 150
        Me.btnEditPo.Name = "btnEditPo"
        '
        'btnView
        '
        Me.btnView.Caption = "View / Reprint P.O"
        Me.btnView.Id = 19
        Me.btnView.ImageOptions.SvgImage = CType(resources.GetObject("BarButtonItem7.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnView.LargeWidth = 150
        Me.btnView.Name = "btnView"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.RibbonPage1.ImageOptions.SvgImage = CType(resources.GetObject("RibbonPage1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Purchase Orders"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnNewPo)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnEditPo)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnSearchPos)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnOpenPos)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnPoHistory)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnView)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Purchase Orders"
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2})
        Me.RibbonPage2.ImageOptions.SvgImage = CType(resources.GetObject("RibbonPage2.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "Receive Inventory"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.btnReceiveInventory)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem5)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Adjust Inventory"
        '
        'RibbonPage3
        '
        Me.RibbonPage3.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup3, Me.RibbonPageGroup4})
        Me.RibbonPage3.ImageOptions.SvgImage = CType(resources.GetObject("RibbonPage3.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.RibbonPage3.Name = "RibbonPage3"
        Me.RibbonPage3.Text = "Vendors and Inventory"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BarButtonItem2)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "Vendors"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.BarButtonItem4)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.BarButtonItem1)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "Inventory Items"
        '
        'RibbonPage4
        '
        Me.RibbonPage4.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup5})
        Me.RibbonPage4.ImageOptions.SvgImage = CType(resources.GetObject("RibbonPage4.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.RibbonPage4.Name = "RibbonPage4"
        Me.RibbonPage4.Text = "Order System"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.btnPlaceOrder)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.btnOrderQue)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.btnOrderHistory)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "Customer Order System"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 853)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(1212, 24)
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Olive
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 177)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(1212, 676)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'frmPurchaseOrders
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1212, 877)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.Name = "frmPurchaseOrders"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Purchase Orders"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents btnVendors As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnNewPo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSearchPos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnItems As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btnNewVendor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnReceiveInventory As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPage3 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOpenPos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPoHistory As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage4 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnPlaceOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOrderQue As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOrderHistory As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEditPo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnView As DevExpress.XtraBars.BarButtonItem
End Class
