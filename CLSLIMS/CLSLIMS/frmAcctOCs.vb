﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class frmAcctOCs

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        ClearCLick()
    End Sub
    Public Sub ClearCLick()
        cbOralAlk.Checked = False
        cbOralAmp.Checked = False
        cbOralADSer.Checked = False
        cbOralAntiTri.Checked = False
        cbOralAnti.Checked = False
        cbOralAntiEpi.Checked = False
        cbOralAntiPsych.Checked = False
        cbOralBenz.Checked = False
        cbOralBup.Checked = False
        cbOralCOC.Checked = False
        cbOralFent.Checked = False
        cbOralHero.Checked = False
        cbOralMethAmp.Checked = False
        cbOralMethy.Checked = False
        cbOralMtd.Checked = False
        cbOralOpi.Checked = False
        cbOralOpiAnal.Checked = False
        cbOralOxy.Checked = False
        cbOralPhen.Checked = False
        cbOralPro.Checked = False
        cbOralSkel.Checked = False
        cbOralTap.Checked = False
        cbOralTHC.Checked = False
        cbOralTram.Checked = False
        cbOralGaba.Checked = False
        cbOralPregaba.Checked = False
        cbOralSedHyp.Checked = False
        cbXREF.Checked = False
        dtpProfDt.Text = Now
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim strTstCode As String = ""
        Dim TCcnt As Integer = 0, AtLeastOne As Boolean = False
        TCcnt = 0
        If cbOralAlk.Checked Then
            strTstCode = strTstCode & "'S349000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralAmp.Checked Then
            strTstCode = strTstCode & "'S352000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralADSer.Checked Then
            strTstCode = strTstCode & "'S354000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralAntiTri.Checked Then
            strTstCode = strTstCode & "'S355000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralAnti.Checked Then
            strTstCode = strTstCode & "'S356000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralAntiEpi.Checked Then
            strTstCode = strTstCode & "'S357000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralAntiPsych.Checked Then
            strTstCode = strTstCode & "'S358000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralBenz.Checked Then
            strTstCode = strTstCode & "'S361000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralBup.Checked Then
            strTstCode = strTstCode & "'S363000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralTHC.Checked Then
            strTstCode = strTstCode & "'S365000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralCOC.Checked Then
            strTstCode = strTstCode & "'S306020',"
            TCcnt = TCcnt + 1
        End If
        If cbOralFent.Checked Then
            strTstCode = strTstCode & "'S369000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralHero.Checked Then
            strTstCode = strTstCode & "'S371000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralMtd.Checked Then
            strTstCode = strTstCode & "'S305040',"
            TCcnt = TCcnt + 1
        End If
        If cbOralMethAmp.Checked Then
            strTstCode = strTstCode & "'S375000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralMethy.Checked Then
            strTstCode = strTstCode & "'S377000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralOpi.Checked Then
            strTstCode = strTstCode & "'S380000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralOpiAnal.Checked Then
            strTstCode = strTstCode & "'S382000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralOxy.Checked Then
            strTstCode = strTstCode & "'S383000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralPhen.Checked Then
            strTstCode = strTstCode & "'S385000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralPro.Checked Then
            strTstCode = strTstCode & "'S388000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralSkel.Checked Then
            strTstCode = strTstCode & "'S391000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralTap.Checked Then
            strTstCode = strTstCode & "'S393000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralTram.Checked Then
            strTstCode = strTstCode & "'S305120',"
            TCcnt = TCcnt + 1
        End If
        If cbOralGaba.Checked Then
            strTstCode = strTstCode & "'S387000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralGaba.Checked Then
            strTstCode = strTstCode & "'S370000',"
            TCcnt = TCcnt + 1
        End If
        If cbOralSedHyp.Checked Then
            strTstCode = strTstCode & "'S390000',"
            TCcnt = TCcnt + 1
        End If
        If cbXREF.Checked Then
            strTstCode = strTstCode & "'800000',"
            TCcnt = TCcnt + 1
        End If
        If Len(strTstCode) > 1 Then
            strTstCode = Mid(strTstCode, 1, Len(strTstCode) - 1)
        Else
            strTstCode = ""
            TCcnt = 0
        End If
        Dim TstMethod As String = "CONF"
            Dim AcctEffdt As String = CLSLIMS.CDT, PrevAcctEffdt As String = ""
        Dim InsCmd As String = "", UpdCmd As String = ""
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Dim rs As SqlDataReader
        Cmd.Connection = conn
        Cmd.CommandText = "Select AEFDT From ACCT Where ACCT='" & txtAcct.Text & "' And AACT=1"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            PrevAcctEffdt = rs(0)
        End If
        rs.Close()
        InsCmd = "Insert into ACCT Select ACCT, '" & AcctEffdt & "', AACT, AABRV, ANAME, ANAME2, AAD1, AAD2, ACITY, ACNTY, ASTATE, ACNTRY, AZIP, APHONE, AFAX, ACONTACT, AEMAIL, ASPRFX, AMRO, AMROA, ASALESRG, ATYPE, '" & TstMethod & "', AMACCT, ABACCT, ADFLTREQ, ADFLTRFMT, ABCONT, AOCREST, ARQUAN, BFEE, APOFEE, ABUNDLE, BCFEE, BMFEE,'" & CLSLIMS.UID & "', AOCORDER, ABILLTO, AMISC1, '" & dtpProfDt.Text & "', AMISC3 " &
                               "From ACCT Where ACCT='" & txtAcct.Text & "' And AACT=1"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        UpdCmd = "Update ACCT Set AACT=0 Where ACCT='" & txtAcct.Text & "' And AACT=1 And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = UpdCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTMC Select ACCT, '" & AcctEffdt & "', ACCTMCx, MC From ACCTMC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTPHY Select ACCT, '" & AcctEffdt & "', PHY From ACCTPHY Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTRPQUE Select ACCT, '" & AcctEffdt & "', ACCTRPQUEx, ARACCT, ARPTTO, ANRPT, AARPT, RPQUE, PHY, ARPRIMARY From ACCTRPQUE Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()
        InsCmd = "Insert Into ACCTLOC Select ACCT, '" & AcctEffdt & "', ACCTLOCx, ALOC, ALOCAD1, ALOCAD2, ALOCCITY, ALOCSTATE, ALOCZIP, ALOCPHONE From ACCTLOC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & PrevAcctEffdt & "'"
        Cmd.CommandText = InsCmd
        Cmd.ExecuteNonQuery()

        If TCcnt > 0 Then
            For i = 1 To TCcnt
                InsCmd = "Insert into ACCTOC Values ('" & txtAcct.Text & "','" & AcctEffdt & "'," & i & "," & CLSLIMS.Piece(strTstCode, ",", i) & ",1)"
                Cmd.CommandText = InsCmd
                Cmd.ExecuteNonQuery()
            Next i
        End If
        txtAcct.Text = ""
        lblAcct.Text = ""
        ClearCLick()

        txtAcct.Focus()
    End Sub
    Private Sub txtAcct_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.Enter Then
            txtAcct.Text = UCase(txtAcct.Text)
            If Mid(txtAcct.Text, 1, 1) <> "S" Then
                MsgBox("Only Oral Fluid Accounts.")
                txtAcct.Text = ""
                Exit Sub
            End If
            lblAcct.Text = GetAcctName(txtAcct.Text)
            If lblAcct.Text = "" Then
                MsgBox("Invalid account.")
                Exit Sub
            End If
            GetDefOC()
        End If
    End Sub
    Public Sub GetDefOC()
        Dim OC As String = "", TstMethod As String = ""
        Dim conn As New SqlConnection(CLSLIMS.strCon)
        conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = conn
        Dim rs As SqlDataReader
        Cmd.CommandText = "Select OC,ACCTOCx From ACCT A, ACCTOC B Where A.ACCT=B.ACCT And A.AEFDT=B.AEFDT And AACT=1 And A.ACCT='" & txtAcct.Text & "' Order By ACCTOCx"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            OC = rs(0)
            Select Case OC
                Case "S349000" : cbOralAlk.Checked = True
                Case "S352000" : cbOralAmp.Checked = True
                Case "S354000" : cbOralADSer.Checked = True
                Case "S355000" : cbOralAntiTri.Checked = True
                Case "S356000" : cbOralAnti.Checked = True
                Case "S357000" : cbOralAntiEpi.Checked = True
                Case "S358000" : cbOralAntiPsych.Checked = True
                Case "S361000" : cbOralBenz.Checked = True
                Case "S363000" : cbOralBup.Checked = True
                Case "S365000" : cbOralTHC.Checked = True
                Case "S306020" : cbOralCOC.Checked = True
                Case "S369000" : cbOralFent.Checked = True
                Case "S371000" : cbOralHero.Checked = True
                Case "S305040" : cbOralMtd.Checked = True
                Case "S375000" : cbOralMethAmp.Checked = True
                Case "S377000" : cbOralMethy.Checked = True
                Case "S380000" : cbOralOpi.Checked = True
                Case "S382000" : cbOralOpiAnal.Checked = True
                Case "S383000" : cbOralOxy.Checked = True
                Case "S385000" : cbOralPhen.Checked = True
                Case "S388000" : cbOralPro.Checked = True
                Case "S391000" : cbOralSkel.Checked = True
                Case "S393000" : cbOralTap.Checked = True
                Case "S305120" : cbOralTram.Checked = True
                Case "S370000" : cbOralGaba.Checked = True
                Case "S387000" : cbOralPregaba.Checked = True
                Case "S390000" : cbOralSedHyp.Checked = True
                Case "800000" : cbXREF.Checked = True
            End Select
        Loop
        rs.Close()
        Cmd.CommandText = "Select AMISC2 From ACCT Where AACT=1 And ACCT='" & txtAcct.Text & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            dtpProfDt.Text = rs(0)
        End If
        rs.Close()
        conn.Close()
    End Sub
    Public Function GetAcctName(ByVal Acct) As String
        GetAcctName = ""

        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where Acct = '" & txtAcct.Text & "' and aact=1 "

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(1)
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        Call BuildProfile()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        PrintDoc.Print()
    End Sub

    Private Sub frmAcctOCs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpProfDt.Format = DateTimePickerFormat.Custom
        dtpProfDt.CustomFormat = "MM/dd/yyyy"
        dtpProfDt.Text = Now
    End Sub

    Public Sub BuildProfile()

        Dim p As String = ""
        Dim TCcnt As Integer = 0
        TCcnt = 0
        If cbOralAlk.Checked Then
            p = p & "S349000-Oral Alkaloids, Unspecified^"
            TCcnt = TCcnt + 1
        End If
        If cbOralAmp.Checked Then
            p = p & "S352000-Oral Amphetamines^"
            TCcnt = TCcnt + 1
        End If
        If cbOralADSer.Checked Then
            p = p & "S354000-Oral Antidepressants, Serotonergic^"
            TCcnt = TCcnt + 1
        End If
        If cbOralAntiTri.Checked Then
            p = p & "S355000-Oral Antidepressants, Tricyclic^"
            TCcnt = TCcnt + 1
        End If
        If cbOralAnti.Checked Then
            p = p & "S356000-Oral Antidepressants^"
            TCcnt = TCcnt + 1
        End If
        If cbOralAntiEpi.Checked Then
            p = p & "S357000-Oral Antiepileptics^"
            TCcnt = TCcnt + 1
        End If
        If cbOralAntiPsych.Checked Then
            p = p & "S357000-Oral Antipsychotics^"
            TCcnt = TCcnt + 1
        End If
        If cbOralBenz.Checked Then
            p = p & "S361000-Oral Benzodiazepines^"
            TCcnt = TCcnt + 1
        End If
        If cbOralBup.Checked Then
            p = p & "S363000-Oral Buprenorphine^"
            TCcnt = TCcnt + 1
        End If
        If cbOralTHC.Checked Then
            p = p & "S365000-Oral Cannabinoids, Natural^"
            TCcnt = TCcnt + 1
        End If
        If cbOralCOC.Checked Then
            p = p & "S306020-Oral Cocaine^"
            TCcnt = TCcnt + 1
        End If
        If cbOralFent.Checked Then
            p = p & "S369000-Oral Fentanyls^"
            TCcnt = TCcnt + 1
        End If
        If cbOralGaba.Checked Then
            p = p & "S370000-Oral Gabapentin, Non-Blood^"
            TCcnt = TCcnt + 1
        End If
        If cbOralHero.Checked Then
            p = p & "S371000-Oral Heroin Metabolite^"
            TCcnt = TCcnt + 1
        End If
        If cbOralMtd.Checked Then
            p = p & "S305040-Oral Methadone^"
            TCcnt = TCcnt + 1
        End If
        If cbOralMethAmp.Checked Then
            p = p & "S375000-Oral Methylenedioxy-Amphetamines^"
            TCcnt = TCcnt + 1
        End If
        If cbOralMethy.Checked Then
            p = p & "S377000-Oral Methylphenidate^"
            TCcnt = TCcnt + 1
        End If
        If cbOralOpi.Checked Then
            p = p & "S380000-Oral Opiates^"
            TCcnt = TCcnt + 1
        End If
        If cbOralOpiAnal.Checked Then
            p = p & "S382000-Oral Opioids and Opiate Analogues^"
            TCcnt = TCcnt + 1
        End If
        If cbOralOxy.Checked Then
            p = p & "S383000-Oral Oxycodone^"
            TCcnt = TCcnt + 1
        End If
        If cbOralPhen.Checked Then
            p = p & "S385000-Oral Phencyclidine^"
            TCcnt = TCcnt + 1
        End If
        If cbOralPregaba.Checked Then
            p = p & "S387000-Oral Pregabalin^"
            TCcnt = TCcnt + 1
        End If
        If cbOralPro.Checked Then
            p = p & "S388000-Oral Propoxyphene^"
            TCcnt = TCcnt + 1
        End If
        If cbOralSedHyp.Checked Then
            p = p & "S390000-Oral Sedative Hypnotics^"
            TCcnt = TCcnt + 1
        End If
        If cbOralSkel.Checked Then
            p = p & "S391000-Oral Skeletal Muscle Relaxants^"
            TCcnt = TCcnt + 1
        End If
        If cbOralTap.Checked Then
            p = p & "S393000-Oral Tapentadol^"
            TCcnt = TCcnt + 1
        End If
        If cbOralTram.Checked Then
            p = p & "S305120-Oral Tramadol^"
            TCcnt = TCcnt + 1
        End If
        If cbXREF.Checked Then
            p = p & "800000-XREF (Medications Presumptive Positives)^"
            TCcnt = TCcnt + 1
        End If
        lstData.Items.Clear()
        For i = 1 To TCcnt
            lstData.Items.Add(Space(2) & CLSLIMS.Piece(p, "^", i))
        Next i
    End Sub

    Private Sub PrintDoc_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim y As Integer = 190
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font6 As New Font("Tacoma", 14, FontStyle.Bold Or FontStyle.Underline)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        'Header
        e.Graphics.DrawString("Account Profile Oral Selection Verification", Font2, Brushes.Black, 180, 45)
        e.Graphics.DrawString(txtAcct.Text & ": " & lblAcct.Text, Font6, Brushes.Black, 50, 100)
        e.Graphics.DrawString("Profile Date: " & dtpProfDt.Text, Font3, Brushes.Black, 90, 125)

        e.Graphics.DrawString("Confirmation Only", Font3, Brushes.Black, 150, 160)

        'Detail
        For i = 0 To lstData.Items.Count - 1
            e.Graphics.DrawString(lstData.Items(i).ToString, Font4, Brushes.Black, 100, y)
            y = y + 20
        Next i
        'Footer
        e.Graphics.DrawString("Checked/Approved by: _________________________", Font5, Brushes.Black, 150, y + 60)
        e.Graphics.DrawString("               Date: _________________________", Font5, Brushes.Black, 150, y + 90)
    End Sub
End Class