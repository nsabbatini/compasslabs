﻿Public Class Login

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Not CLSLIMS.ValidateUser(UsernameTextBox.Text, PasswordTextBox.Text) Then
            MsgBox("Invalid Username/Password")
            CLSLIMS.UID = ""
        Else
            CLSLIMS.UID = UCase(UsernameTextBox.Text)
            Me.Close()
            Me.Dispose()
        End If

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        CLSLIMS.UID = ""
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub UsernameTextBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles UsernameTextBox.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
    End Sub

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-NED" Then
            CLSLIMS.UID = "NED.SABBATINI" : CLSLIMS.UNAME = "Sabbatini, Ned"
            Me.Close()
            Me.Dispose()
        End If
    End Sub
End Class
