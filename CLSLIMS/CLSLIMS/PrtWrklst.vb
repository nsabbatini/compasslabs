﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Specialized

Public Class PrtWrklst
    Public PosInc As NameValueCollection

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub GetMeds(ByRef specno As String, ByVal Meds As Array)
        Dim i As Integer
        For i = 1 To 20 : Meds(i) = "" : Next i
        Dim sel As String = "Select MED From POMED A, PSPEC B Where PSPECNO='" & specno & "' And A.PORD=B.PORD and A.POEFDT=B.POEFDT And PSACT=1"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        i = 1
        Do While rsf.Read()
            Meds(i) = rsf(0) & " ---> " & GetResults(rsf(0), specno)
            i = i + 1
        Loop
        rsf.Close()
        ConnF.Close()
    End Sub
    Public Function GetResults(ByVal med As String, ByVal specno As String) As String
        GetResults = ""
        Dim r As String = "", s As String = "", qual As String = "", int As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand : Cmd.Connection = Conn
        Dim Cmd2 As New SqlCommand : Cmd2.Connection = Conn
        Dim Cmd3 As New SqlCommand : Cmd3.Connection = Conn
        Cmd.CommandText = "Select xdrug from xbrand where med='" & med & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Dim rs2 As SqlDataReader = Nothing
        Dim rs3 As SqlDataReader = Nothing

        Do While rs.Read
            Cmd2.CommandText = "Select xdgcql,xdgcint,xdrug from xdrug where xdrug='" & rs(0) & "' or xdrug in (select xmetabolite from xparent where xparent='" & rs(0) & "')"
            rs2 = Cmd2.ExecuteReader
            Do While rs2.Read
                qual = "" : int = ""
                If r <> "" Then r = r & ","
                Cmd3.CommandText = "Select prslt from pres where pract=1 and rc='" & rs2(0) & "' and pspecno='" & specno & "'"
                rs3 = Cmd3.ExecuteReader
                If rs3.Read Then
                    qual = IIf(IsDBNull(rs3(0)), "", rs3(0))
                End If
                rs3.Close()
                Cmd3.CommandText = "Select prslt from pres where pract=1 and rc='" & rs2(1) & "' and pspecno='" & specno & "'"
                rs3 = Cmd3.ExecuteReader
                If rs3.Read Then
                    int = IIf(IsDBNull(rs3(0)), "", rs3(0))
                End If
                rs3.Close()
                If qual = "Positive" And int = "Consistent" Then r = r & rs2(2)
                If qual = "Negative" And int = "Inconsistent" Then r = r & rs2(2) & "-(O)"
            Loop
            rs2.Close()
        Loop
        rs.Close()
        If r <> "" Then
            r = CLSLIMS.RemoveTrailingChar(r, ",")
            r = Replace(r, ",,", ",") : r = Replace(r, ",,,", ",") : r = Replace(r, ",,,,", ",")
        End If
        GetResults = r
        Conn.Close()
    End Function
    Public Sub PosIncon(ByVal specno As String)
        Dim rc As String = "", rec As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand : Cmd.Connection = Conn
        Dim Cmd2 As New SqlCommand : Cmd2.Connection = Conn
        Dim Cmd3 As New SqlCommand : Cmd3.Connection = Conn
        Cmd.CommandText = "Select rc from pres where pspecno='" & specno & "' and pract=1 and prslt='Positive' and (substring(rc,1,1)='3' or substring(rc,1,2)='S3')"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Dim rs2 As SqlDataReader = Nothing
        Dim rs3 As SqlDataReader = Nothing
        Do While rs.Read
            rc = rs(0) : rc = Mid(rc, 1, Len(rc) - 1) & "4"
            Cmd2.CommandText = "Select rc from pres where pspecno='" & specno & "' and pract=1 and prslt='Inconsistent' and rc='" & rc & "'"
            rs2 = Cmd2.ExecuteReader
            If rs2.Read Then
                Cmd3.CommandText = "Select xdrug from xdrug where xdgcint='" & rc & "'"
                rs3 = Cmd3.ExecuteReader
                If rs3.Read Then
                    rec = rec & rs3(0) & ","
                End If
                rs3.Close()
            End If
            rs2.Close()
        Loop
        rs.Close()
        Conn.Close()
        rec = CLSLIMS.RemoveTrailingChar(rec, ",")
        PosInc.Add(specno, rec)
    End Sub
    Public Function ScrnRslts(ByVal spec As String) As String
        ScrnRslts = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select prslt,rabrv from pres a, rc b where pract=1 and pspecno='" & spec & "' And a.rc=b.rc and a.refdt=b.refdt and rabrv like '%-QN' and substring(a.rc,1,1)='2' And a.rc not in ('209011','209033','209043')"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            ScrnRslts = ScrnRslts & Mid(rs(1), 2, Len(rs(1)) - 4) & "-" & rs(0) & "^"
        Loop
        rs.Close()
        Conn.Close()
        ScrnRslts = CLSLIMS.RemoveTrailingChar(ScrnRslts, "^")
    End Function
    Public Function CheckforRudolph(ByVal specno As String) As Boolean
        CheckforRudolph = False
        Dim Sel As String = "Select count(*) from PORD A, PSPEC B Where A.PORD=B.PORD And A.POEFDT=B.POEFDT And PSACT=1 And POACT=1 And ACCT In ('371','372') And PSPECNO='" & specno & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                If rs(0) > 0 Then
                    CheckforRudolph = True
                End If
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
    Private Sub GetOCMNEs(ByRef specno As String, ByVal OCMNEs As Array)
        Dim i As Integer
        For i = 1 To 20 : OCMNEs(i) = "" : Next i
        Dim sel As String = "Select OC From POC A, PSPEC B Where A.PSPECNO='" & specno & "' And A.ADSQ1=B.ADSQ1 and PSACT=1 And A.PSPECNO=B.PSPECNO And OC != '800000'"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        i = 1
        Do While rsf.Read()
            OCMNEs(i) = CLSLIMS.GetOCAbbr(rsf(0))
            i = i + 1
        Loop
        rsf.Close()
        ConnF.Close()
    End Sub

    Private Sub CollectData()
        Dim Sel As String, p As String, c As Integer = 1, xPos As String, w As Integer, blnRudolph As Boolean = False, sr As String = ""
        PosInc = New NameValueCollection
        Sel = "Select pos,a.pspecno From XTRAYS A, PSPEC B Where FOLDER = '" & txtFldr.Text & "' And A.PSPECNO=B.PSPECNO And PSACT=1 Order By POS"
        lstData.Items.Clear() : lstScrn.Items.Clear()
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Dim Meds(20) As String
        Do While rs.Read()
            Call GetMeds(rs(1), Meds)
            Call PosIncon(rs(1))
            'sr = ScrnRslts(rs(1))
            xPos = IIf(IsDBNull(rs(0)), "", rs(0))
            blnRudolph = CheckforRudolph(rs(1))
            p = IIf(blnRudolph, "(", "") & rs(1) & IIf(blnRudolph, ")", "") & "^" & rs(0) & "^"
            For i = 1 To 20
                If Meds(i) <> "" Then
                    p = p & Meds(i) & "^"
                End If
            Next i
            lstData.Items.Add(p)
            lstScrn.Items.Add(sr)
        Loop
        rs.Close()
        Sel = "Select Distinct A.WL,MBATCH From PBI A, PB B Where PSPECNO In (Select PSPECNO From XTRAYS Where FOLDER='" & txtFldr.Text & "') And A.BATCH=B.BATCH And A.BEFDT=B.BEFDT And A.WL=B.WL"
        Dim Cmd2 As New SqlCommand(Sel, Conn)
        Dim rs2 As SqlDataReader = Cmd2.ExecuteReader
        lblWrklst.Text = "" : w = 1 : lblWrkLst2.Text = "" : lblWrkLst3.Text = "" : lblWrklst4.text = "" : lblWrklst5.text = ""
        Do While rs2.Read
            If w < 4 Then
                lblWrklst.Text = lblWrklst.Text & rs2(0) & "-" & rs2(1) & "; [    ] "
            ElseIf w < 8 Then
                lblWrkLst2.Text = lblWrkLst2.Text & rs2(0) & "-" & rs2(1) & "; [    ] "
            ElseIf w < 12 Then
                lblWrkLst3.Text = lblWrkLst3.Text & rs2(0) & "-" & rs2(1) & "; [    ] "
            ElseIf w < 16 Then
                lblWrkLst4.Text = lblWrkLst4.Text & rs2(0) & "-" & rs2(1) & "; [    ] "
            Else
                lblWrkLst5.Text = lblWrkLst5.Text & rs2(0) & "-" & rs2(1) & "; [    ] "
            End If
            w = w + 1
        Loop
        rs2.Close()
        Conn.Close()
        Conn.Dispose()

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Call CollectData()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = True

        'PrintPrev.ShowDialog()
        PrintDoc.Print()

        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Tacoma", 9, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 25, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 16, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim Font6 As New Font("Tacoma", 7, FontStyle.Regular)
        Dim txtHeight As Integer, PosIncon1 As String = "", PosIncon2 As String = "", specno As String = "", NoP As Integer = 0
        Dim NoP2 As Integer = 0, lp As Integer = 0, WF As Integer = 0
        PrintDoc.DefaultPageSettings.Landscape = True
        txtHeight = PrintDoc.DefaultPageSettings.PaperSize.Height - PrintDoc.DefaultPageSettings.Margins.Bottom - PrintDoc.DefaultPageSettings.Margins.Top
        Dim LinesPerPage As Integer = CInt(Math.Round(txtHeight / (Font.Height + 0.025)))

        'e.Graphics.DrawRectangle(Pens.Black, e.MarginBounds)

        Static PageNo As Integer = 1
        Dim y As Integer

        e.Graphics.DrawString("Folder: " & txtFldr.Text, Font2, Brushes.Black, 350, 25)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 900, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 900, 45)
        If PageNo = 1 Then
            e.Graphics.DrawString("Worklists: " & lblWrklst.Text, Font4, Brushes.Black, 40, 130)
            e.Graphics.DrawString(lblWrkLst2.Text, Font4, Brushes.Black, 110, 150)
            e.Graphics.DrawString(lblWrkLst3.Text, Font4, Brushes.Black, 110, 170)
            If lblWrkLst4.Text <> "" Then
                e.Graphics.DrawString(lblWrkLst4.Text, Font4, Brushes.Black, 110, 190)
                WF = WF + 20
            End If
            If lblWrkLst5.Text <> "" Then
                e.Graphics.DrawString(lblWrkLst5.Text, Font4, Brushes.Black, 110, 210)
                WF = WF + 20
            End If
            e.Graphics.DrawString("Instrument: _____________", Font4, Brushes.Black, 850, 70)
            e.Graphics.DrawString("Flush pump primer: _______", Font4, Brushes.Black, 850, 95)
            e.Graphics.DrawString("Wash Well filled: _________", Font4, Brushes.Black, 850, 120)
            e.Graphics.DrawString("   Specimen #     Position          Medications                        POS-INC       Comments", Font5, Brushes.Black, 25, 205 + WF)
            e.Graphics.DrawLine(Pens.Black, 25, 200 + WF, 1075, 200 + WF)   'Horizonal line above headings
            e.Graphics.DrawLine(Pens.Black, 25, 235 + WF, 1075, 235 + WF)   'Horizonal line under headings
            e.Graphics.DrawLine(Pens.Black, 205, 200 + WF, 205, 820)   'Veritcal line 1
            e.Graphics.DrawLine(Pens.Black, 320, 200 + WF, 320, 820)   'Veritcal line 2
            e.Graphics.DrawLine(Pens.Black, 600, 200 + WF, 600, 820)   'Veritcal line 3
            e.Graphics.DrawLine(Pens.Black, 750, 200 + WF, 750, 820)   'Veritcal line 4
            e.Graphics.DrawLine(Pens.Black, 875, 200 + WF, 875, 820)   'Veritcal line 5
        Else
            e.Graphics.DrawString("   Specimen #     Position          Medications                        POS-INC       Comments", Font5, Brushes.Black, 25, 112)
            e.Graphics.DrawLine(Pens.Black, 25, 97, 1075, 97)   'Horizonal line above headings
            e.Graphics.DrawLine(Pens.Black, 25, 135, 1075, 135)   'Horizonal line under headings
            e.Graphics.DrawLine(Pens.Black, 200, 97, 200, 820)   'Veritcal line 1
            e.Graphics.DrawLine(Pens.Black, 320, 97, 320, 820)   'Veritcal line 2
            e.Graphics.DrawLine(Pens.Black, 600, 97, 600, 820)   'Veritcal line 3
            e.Graphics.DrawLine(Pens.Black, 750, 97, 750, 820)   'Veritcal line 4
            e.Graphics.DrawLine(Pens.Black, 875, 97, 875, 820)   'Veritcal line 5
        End If

        Dim p As Integer
        Static intStart As Integer

        If PageNo = 1 Then
            y = 248 + WF
        Else
            y = 145
        End If

        For i = intStart To lstData.Items.Count - 1
            'Check number of pieces compared to y to determine if you should go to next page
            p = CLSLIMS.NumberofPieces(lstData.Items(i), "^") - 3
            If p < 0 Then p = 0
            'Debug.Print("intStart:" & intStart & "  i=" & i & "  y=" & y & "  p=" & p & "   " & lstData.Items(i))
            If y + (p * 30) < 800 Then
                'data line
                e.Graphics.DrawString(CLSLIMS.Piece(lstData.Items(i), "^", 1), Font, Brushes.Black, 70, y)
                e.Graphics.DrawString(CLSLIMS.Piece(lstData.Items(i), "^", 2), Font, Brushes.Black, 235, y)
                For j = 3 To 99
                    If j = 3 Then
                        e.Graphics.DrawString(CLSLIMS.Piece(lstData.Items(i), "^", j), Font, Brushes.Black, 350, y)
                        specno = CLSLIMS.Piece(lstData.Items(i), "^", 1)
                        If Len(PosInc(CLSLIMS.Piece(lstData.Items(i), "^", 1))) > 30 Then
                            NoP = CLSLIMS.NumberofPieces(PosInc(specno), ",")
                            For kw = 1 To NoP
                                If Len(PosIncon1) < 30 Then
                                    PosIncon1 = PosIncon1 & CLSLIMS.Piece(PosInc(specno), ",", kw) & ","
                                Else
                                    PosIncon2 = PosIncon2 & CLSLIMS.Piece(PosInc(specno), ",", kw) & ","
                                End If
                            Next kw
                            PosIncon1 = CLSLIMS.RemoveTrailingChar(PosIncon1, ",")
                            PosIncon2 = CLSLIMS.RemoveTrailingChar(PosIncon2, ",")
                            e.Graphics.DrawString(PosIncon1, Font, Brushes.Black, 750, y)
                            e.Graphics.DrawString(PosIncon2, Font, Brushes.Black, 750, y + 30)
                            If CLSLIMS.Piece(lstData.Items(i), "^", 4) = "" Then y = y + 30
                            PosIncon1 = ""
                            PosIncon2 = ""
                        Else
                            e.Graphics.DrawString(PosInc(CLSLIMS.Piece(lstData.Items(i), "^", 1)), Font, Brushes.Black, 750, y)
                        End If
                    ElseIf j > 3 And CLSLIMS.Piece(lstData.Items(i), "^", j) <> "" Then
                        y = y + 30
                        e.Graphics.DrawString(CLSLIMS.Piece(lstData.Items(i), "^", j), Font, Brushes.Black, 350, y)
                    End If
                    If CLSLIMS.Piece(lstData.Items(i), "^", j) = "" Then
                        Exit For
                    End If
                Next j
                '============================================================================
                'Print screen info
                If lstScrn.Items(i) <> "" Then y = y + 22
                e.Graphics.DrawString(Replace(lstScrn.Items(i), "^", " "), Font6, Brushes.Black, 80, y)
                '============================================================================
                y = y + 20
                e.Graphics.DrawLine(Pens.Black, 25, y, 1075, y) 'Horizonal line after sample
                y = y + 10
            Else
                y = 9999
                i = i - 1
            End If
            If y > 800 Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i

    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click

        Call CollectData()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = True

        PrintPrev.ShowDialog()

        Me.Close()
        Me.Dispose()

    End Sub
End Class