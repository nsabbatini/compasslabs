﻿Imports CLSLIMS.CLSLIMS
Imports CLSLIMS.Queue
Imports System.Data.SqlClient
Imports System
Imports System.Data

Imports System.Drawing.Printing



Public Class DetailNotes
    Dim sql As String = ""
    Dim rownumber As Integer
    Dim lbreader As SqlDataReader
    Dim vdescription As New ArrayList()
    Public Property Specnum As Integer
    Dim NeedtoSaveNotes As Boolean = False
    Dim numofline As Integer
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        lblPatName.Text = ""

        ' Add any initialization after the InitializeComponent() call.
        '      MsgBox(lblspecnum.ToString)
        lblspecnum.Text = Queue.hold_apspecno
        lblPatName.Text = Queue.PatientName

    End Sub

    Public Function LJ(ByVal strLJ As String, ByVal iLen As Integer) As String
        LJ = ""
        If Len(strLJ) > iLen Then
            LJ = strLJ
        Else
            LJ = strLJ & Space(iLen - Len(strLJ))
        End If
    End Function

    Private Sub DetailNotes_Load(sender As Object, e As EventArgs) Handles Me.Load
        lbTemplate.Items.Clear()
        NeedtoSaveNotes = False

        Try
            Using Conn As New SqlConnection(CLSLIMS.strCon)

                sql = "Select distinct TemplateName From XAffTemplate "
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim lbreader As SqlDataReader = Cmd.ExecuteReader
                rownumber = 0
                While lbreader.Read()
                    vdescription.Add(lbreader(0).ToString)
                    lbTemplate.Items.Add(LJ(lbreader(0), 15))
                    rownumber = rownumber + 1
                End While
                lbreader.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading Template List")
        End Try

        lbTemplate.Visible = True
        lbTemplate.Focus()

        Call CheckforAINotes()

        Me.Refresh()
        Me.Show()

    End Sub
    Sub CheckforAINotes()
        Dim AIFullNotes As String = ""
        Try
            Using Conn As New SqlConnection(CLSLIMS.strCon)

                sql = "select pscmttxt from PSCMTI I, PSCMT C where c.pspecno = i.pspecno and c.pscmttype = i.pscmttype and c.pscmtefdt = i.pscmtefdt and i.pspecno = '" & Queue.hold_apspecno & "' and i.pscmttype = 'I' and c.pscmtorig = 'AI' and PSCMTACT=1"

                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim AIReader As SqlDataReader = Cmd.ExecuteReader
                While AIReader.Read()
                    txtNotes.Text = txtNotes.Text & AIReader(0) & vbCrLf

                End While
                AIReader.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading AI List")
        End Try

    End Sub

    Private Sub lbTemplate_MouseClick(sender As Object, e As MouseEventArgs) Handles lbTemplate.MouseClick
        ' If lbTemplate.SelectedIndex < rownumber And lbTemplate.SelectedIndex <> "-1" 
        If lbTemplate.SelectedIndex <> "-1" Then
            Call FillNotes(lbTemplate.SelectedIndex, vdescription)
            NeedtoSaveNotes = True
        End If

    End Sub
    Sub FillNotes(selectedindex, vdescription)
        '   txtNotes.Clear()
        Dim lineCount As Integer = txtNotes.Lines.Count

        Try
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                sql = "Select Templatetext From XAffTemplate where templatename = '" & vdescription(selectedindex) & "'"
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim x As SqlDataReader = Cmd.ExecuteReader
                rownumber = 0
                While x.Read()
                    rownumber = rownumber + 1
                    If lineCount + rownumber > 28 Then
                        MsgBox("Adding this note will exceeed the number of allowable lines  ")
                    Else
                        txtNotes.Text = txtNotes.Text & x(0) & vbCrLf
                    End If
                End While


                x.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading Template List")
        End Try


    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        '  If Queue.ListHeader.SelectedIndex < rownumber And Queue.ListHeader.SelectedIndex <> "-1" Then
        Try
            Dim lineCount As Integer = txtNotes.Lines.Count
            If lineCount > 28 Then
                MsgBox("Too many lines for printing ")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Error calculating lines")
        End Try

        '  If Queue.ListHeader.SelectedIndex <> "-1" Then
        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        ppd.ShowDialog()
        '  Call SaveNotes(Queue.ListHeader.SelectedIndex)


        '    End If
    End Sub
    Sub SaveNotes() '(selectedindex)
        If txtNotes.Text <> "" Then
            Dim ComDt As String = "", txtCom As String = "", iSpc As Integer = 0, Cmt(50) As String
            Dim i As Integer = 1
            Dim x As Integer

            Dim Line1 As String = txtNotes.Text()
            Dim splitline1() As String = Split(Line1, vbCrLf)
            Dim currdate = Format(Now, "yyyyMMddhhmmss")

            For j = 0 To UBound(splitline1)
                If Len(splitline1(j)) > 80 Then
                    MsgBox("Line is > 80 characters:" & splitline1(j))
                    Exit Sub
                End If

            Next
            Try
                Dim Conn As New SqlConnection(CLSLIMS.strCon)
                Conn.Open()
                Dim Cmd As New SqlCommand
                Cmd.Connection = Conn
                Cmd.CommandText = "Insert into pscmt values ('" & Queue.hold_apspecno & "','I','" & currdate & "',1,'" & CLSLIMS.UID & "','AI','')"
                '  Cmd.CommandText = "Insert into pscmt values ('" & Queue.holdpspecno & "','I','" & currdate & "',1,'BETH.DUGAN','AI','')"
                Cmd.ExecuteNonQuery()
                x = 1
                For j = 0 To UBound(splitline1)
                    splitline1(j) = Replace(splitline1(j), "'", "")
                    Cmd.CommandText = "Insert Into PSCMTI Values ('" & Queue.hold_apspecno & "','I','" & currdate & "'," & i & ",'" & splitline1(j) & "')"
                    i = i + 1
                    Cmd.ExecuteNonQuery()
                Next
                i = i + 1
                Conn.Close()
            Catch ex As Exception
                MsgBox("Error Writing Notes")
            End Try
            NeedtoSaveNotes = False
        End If
    End Sub
    Public Function FirstSpace(ByVal str As String) As Integer
        FirstSpace = 0
        For i = Len(str) To 1 Step -1
            If Asc(Mid(str, i, 1)) = 32 Then
                FirstSpace = i
                Exit For
            End If
        Next i
    End Function

    Private Sub PrintDoc_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim username As String = ""
        Dim faxnumber As String = ""
        Try
            Using Conn As New SqlConnection(CLSLIMS.strCon)

                sql = "select distinct ulname, ufname, uemail from sysuser where uid = " & "'" & CLSLIMS.UID & "'" & " and uact = 1"

                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim UserReader As SqlDataReader = Cmd.ExecuteReader
                While UserReader.Read()
                    username = UserReader(1) & " " & UserReader(0)
                    faxnumber = "901-348-5738"     'UserReader(2)
                End While
                UserReader.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Loading Username Info")
        End Try


        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 = New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)
        Dim PageLngth As Integer = 28
        'Dim myvar23 = Queue.Varcrs23.ToString
        '  MsgBox(myvar23)

        Static PageNo As Integer = 1
        Dim PrevSOLAB As String = "", SOLAB As String = ""
        e.Graphics.DrawString("Certificate of Affidavit", Font, Brushes.Black, 30, 10)
        e.Graphics.DrawString("PRO-F-2004.01", Font, Brushes.Black, 30, 20)
        e.Graphics.DrawString("07/22/14", Font, Brushes.Black, 30, 30)
        e.Graphics.DrawString("CERTIFICATE OF AFFIDAVIT", Font3, Brushes.Black, 300, 190)

        Dim LogoImage As Image = My.Resources.Compass_Logo
        e.Graphics.DrawImage(LogoImage, 500, 10)


        e.Graphics.DrawString("Date:  ", Font3, Brushes.Black, 10, 240)
        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font3, Brushes.Black, 60, 240)
        e.Graphics.DrawLine(Pens.Black, 60, 260, 210, 260)

        If CheckFirst.Checked = True Then
            e.Graphics.DrawString("FIRST REQUEST ", Font3, Brushes.Black, 300, 240)
        Else
            If CheckSecond.Checked = True Then
                e.Graphics.DrawString("SECOND REQUEST ", Font3, Brushes.Black, 300, 240)
            Else
                If CheckThird.Checked = True Then
                    e.Graphics.DrawString("THIRD REQUEST ", Font3, Brushes.Black, 300, 240)
                End If
            End If
        End If

        e.Graphics.DrawString("Clinic Name:  ", Font3, Brushes.Black, 10, 280)
        e.Graphics.DrawString(Queue.Varcrs24.ToString, Font3, Brushes.Black, 140, 280)
        e.Graphics.DrawLine(Pens.Black, 140, 300, 380, 300)

        e.Graphics.DrawString("Req:  ", Font3, Brushes.Black, 620, 280)
        e.Graphics.DrawString(Queue.hold_apspecno, Font3, Brushes.Black, 700, 280)
        e.Graphics.DrawLine(Pens.Black, 700, 300, 800, 300)

        e.Graphics.DrawString("Patient Name:  ", Font3, Brushes.Black, 10, 310)
        e.Graphics.DrawString(Queue.PatientName.ToString, Font3, Brushes.Black, 143, 310)
        e.Graphics.DrawLine(Pens.Black, 143, 330, 420, 330)

        e.Graphics.DrawString("Collection Date:  ", Font3, Brushes.Black, 500, 310)
        If Format(Queue.colldatetime, "MM/dd/yy") <> "01/01/01" Then e.Graphics.DrawString(Format(Queue.colldatetime, "MM/dd/yy"), Font3, Brushes.Black, 700, 310)
        e.Graphics.DrawLine(Pens.Black, 700, 330, 800, 330)

        e.Graphics.DrawString("Reason(s) for Affidavit:  ", Font3, Brushes.Black, 10, 350)
        e.Graphics.DrawLine(Pens.Black, 10, 370, 250, 370)

        e.Graphics.DrawLine(Pens.Black, 10, 900, 830, 900)
        e.Graphics.DrawString("Please return completed affidavit to the lab. Attn: " & username, Font3, Brushes.Black, 40, 930)
        e.Graphics.DrawString("                 Fax Number:  " & faxnumber, Font3, Brushes.Black, 90, 960)

        e.Graphics.DrawString("Compass Laboratory Services", Font, Brushes.Black, 10, 1000)
        e.Graphics.DrawString("Share Drive", Font, Brushes.Black, 10, 1020)

        If txtNotes.Text <> "" Then
            Dim txtCom As String = "", iSpc As Integer = 0
            Dim i As Integer = 1
            Dim x As Integer
            Dim printline As Integer = 400

            Dim Line1 As String = txtNotes.Text()
            Dim splitline1() As String = Split(Line1, vbCrLf)
            '     Dim currdate = Format(Now, "yyyyMMddhhmmss")
            Try

                x = 1
                For j = 0 To UBound(splitline1)
                    Do While Len(splitline1(j)) > 80
                        iSpc = FirstSpace(Mid(splitline1(j), 1, 80))

                        e.Graphics.DrawString(Mid(splitline1(j), 1, iSpc), Font3, Brushes.Black, 30, printline)
                        splitline1(j) = Mid(splitline1(j), iSpc + 1)
                        i = i + 1
                        printline = printline + 20
                    Loop
                    e.Graphics.DrawString(splitline1(j), Font3, Brushes.Black, 30, printline)
                    i = i + 1
                    printline = printline + 20
                Next
            Catch ex As Exception
                MsgBox("Error Building Affidavit")
            End Try
        End If

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        If CheckFirst.Checked Then
            txtNotes.Text = "Affidavit Sent."
            Call SaveNotes()
        ElseIf CheckSecond.Checked Then
            txtNotes.Text = "Second Request Sent."
            Call SaveNotes()
        ElseIf CheckThird.Checked Then
            txtNotes.Text = "Third Request Sent."
            Call SaveNotes()
        ElseIf CheckTevix.Checked Then
            txtNotes.Text = "Verified in TEVIX. No Aff sent."
            Call SaveNotes()
        End If
        Me.Close()

        'If NeedtoSaveNotes = True Then
        '    Dim result As Integer = MessageBox.Show("Save Notes?", "Save Option", MessageBoxButtons.YesNoCancel)
        '    If result = DialogResult.Cancel Then
        '        Exit Sub
        '    ElseIf result = DialogResult.No Then
        '        Me.Close()
        '    ElseIf result = DialogResult.Yes Then
        '        Call DeleteAINotes()
        '        Call SaveNotes() '(Queue.ListHeader.SelectedIndex)
        '        NeedtoSaveNotes = False
        '        Me.Close()
        '        lblPatName.Text = ""

        '    Else

        '    End If
        'Else
        '    Me.Close()

        'End If

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        ''''''Delete current AI notes
        Call DeleteAINotes()
        Call SaveNotes() '(Queue.ListHeader.SelectedIndex)
        MsgBox("Notes Saved")
    End Sub

    Sub DeleteAINotes()
        ''''''''''''''''''''''First have to get the effective date so to delete the i table too.
        Dim findeffectivedate As String = ""
        Try
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                sql = "Select pscmtefdt from pscmt where pspecno = '" & Queue.hold_apspecno & "' and  pscmtorig = 'AI' and PSCMTACT=1"
                Conn.Open()
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim x As SqlDataReader = Cmd.ExecuteReader
                rownumber = 0
                While x.Read()
                    findeffectivedate = x(0)
                End While
                x.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error Finding Effective Date")
        End Try
        Try
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand
            Cmd.Connection = Conn
            Cmd.CommandText = "delete from pscmt where pspecno = '" & Queue.hold_apspecno & "' and  pscmtorig = 'AI' and pscmtefdt = '" & findeffectivedate & "'"
            Cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox("Error Deleting Notes")
        End Try
        Try
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand
            Cmd.Connection = Conn
            Cmd.CommandText = "delete from pscmti where pspecno = '" & Queue.hold_apspecno & "' and  pscmtefdt = '" & findeffectivedate & "'"
            Cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox("Error Deleting Notes")
        End Try
    End Sub

    Private Sub txtNotes_ModifiedChanged(sender As Object, e As EventArgs) Handles txtNotes.ModifiedChanged
        NeedtoSaveNotes = True
    End Sub

    Private Sub txtNotes_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNotes.KeyPress
        Dim x As Integer
        If Asc(e.KeyChar) <> 8 Then
            Try
                Dim numofchar() = Split(txtNotes.Text, vbCrLf)
                For x = 0 To UBound(numofchar)
                    If Len(numofchar(x)) > 79 Then
                        MsgBox("Reached number of characters allowed on line " & x + 1)
                    End If
                Next
            Catch ex As Exception
                MsgBox("Error calculating characters")
            End Try
            Try
                Dim lineCount As Integer = txtNotes.Lines.Count
                If lineCount > 28 Then
                    MsgBox("Reached number of allowables lines ")
                End If
            Catch ex As Exception
                MsgBox("Error calculating lines")
            End Try
        End If
    End Sub

    Private Sub CheckSecond_CheckedChanged(sender As Object, e As EventArgs) Handles CheckSecond.CheckedChanged
        If CheckSecond.Checked = True Then
            CheckThird.Checked = False
        End If
    End Sub

    Private Sub CheckThird_CheckedChanged(sender As Object, e As EventArgs) Handles CheckThird.CheckedChanged
        If CheckThird.Checked = True Then
            CheckSecond.Checked = False
        End If
    End Sub

End Class