﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Sql.Database
Imports System.Text
Imports DevExpress.XtraEditors.DXErrorProvider
Public Class frmPurchaseOrderEdit
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _departmentId = 0
    Private _departmentText As String
    Private _dtPO As DataTable = New DataTable
    Private _selectItem As String = String.Empty
    Private _subtotal As Double
    Private _total As Double
    Private _ponumber As String
    Private _salestax As Double = 0
    Private _ShipTo As Int16
    Private _POId As Int16

    Private Sub frmPurchaseOrderEdit_Load(sender As Object, e As EventArgs) Handles Me.Load

        LoadLookup()

        _dtPO = CreateData()

        GridView1.OptionsView.ShowGroupPanel = False
        GridView1.OptionsView.ShowIndicator = False

    End Sub

    Private Sub LoadLookup()

        Dim results = GetPoNumbers()

        lookupPo.Properties.DataSource = results
        lookupPo.Properties.DisplayMember = "PoNumber"
        lookupPo.Properties.ValueMember = "PId"



    End Sub
    Private Function GetPoNumbers() As Object

        Try

            Using purchaseOrders As New PurchaseOrders()
                _fileName = _fileDir & GetPurchaseOrdersSql
                purchaseOrders._sqlText = ReadSqlFile(_fileName)
                Return purchaseOrders.GetPurchaseOrder.Where(Function(x) x.Status = 4).OrderByDescending(Function(o) o.PId).ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderEdit.GetPoNumbers", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try

    End Function
    Private Function GetPoDetails() As Object

        Try

            Using purchaseOrders As New PurchaseOrders()
                _fileName = _fileDir & GetReconcilePoSql
                purchaseOrders._sqlText = ReadSqlFile(_fileName)
                Return purchaseOrders.ReconcilePo.Where(Function(x) x.PId = lookupPo.EditValue And x.Received = False).ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderEdit.GetPoNumbers", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try

    End Function

    Private Sub lookupPo_EditValueChanged(sender As Object, e As EventArgs) Handles lookupPo.EditValueChanged
        Dim arow = lookupPo.GetSelectedDataRow()


        txtSalesPerson.Text = arow.GetType().GetProperty("SalesPerson")?.GetValue(arow)
        txtQuoteNumber.Text = arow.GetType().GetProperty("QuoteNumber")?.GetValue(arow)
        txtNotes.Text = arow.GetType().GetProperty("Notes")?.GetValue(arow)
        txtPaymentTerms.Text = arow.GetType().GetProperty("PaymentTerms")?.GetValue(arow)
        lblTotalPo.Text = CDbl(arow.GetType().GetProperty("TotalPo")?.GetValue(arow)).ToString("c")
        _ShipTo = arow.GetType().GetProperty("ShipTo")?.GetValue(arow)
        VendorId = CInt(arow.GetType().GetProperty("VId")?.GetValue(arow))
        _POId = CInt(arow.GetType().GetProperty("PId")?.GetValue(arow))
        _ponumber = CInt(arow.GetType().GetProperty("PoNumber")?.GetValue(arow))
        _subtotal = 0
        _total = 0
        lblSubTotal.Text = String.Empty
        lblTotalPo.Text = String.Empty
        txtTax.Text = String.Empty


        LoadDepartments(CInt(arow.GetType().GetProperty("Department")?.GetValue(arow)))

        LoadItems(VendorId)
        _dtPO.Clear()
        LoadTheGrid()

    End Sub
    Private Function CreateData() As DataTable

        Dim _dtItems As DataTable = New DataTable
        _dtItems.Columns.Add("ItemDesc", GetType(String))
        _dtItems.Columns.Add("ItemId", GetType(Int16))
        _dtItems.Columns.Add("Qty", GetType(Int16))
        _dtItems.Columns.Add("UnitCost", GetType(Double))
        _dtItems.Columns.Add("Price", GetType(Double))
        _dtItems.Columns.Add("DId", GetType(Int16))

        Return _dtItems

    End Function
    Private Sub LoadTheGrid()
        Dim arow As DataRow
        Dim results = GetPoDetails()

        With _dtPO
            For Each item In results
                arow = .NewRow
                arow(0) = item.GetType().GetProperty("ItemDesc")?.GetValue(item)
                arow(1) = item.GetType().GetProperty("ItemId")?.GetValue(item)
                arow(2) = item.GetType().GetProperty("OrderQty")?.GetValue(item)
                arow(3) = item.GetType().GetProperty("UnitPrice")?.GetValue(item)
                arow(4) = item.GetType().GetProperty("ItemCost")?.GetValue(item)
                arow(5) = item.GetType().GetProperty("DId")?.GetValue(item)
                _subtotal += arow(4)
                .Rows.Add(arow)
            Next
        End With
        lblSubTotal.Text = _subtotal.ToString("c")
        lblTotalPo.Text = _subtotal.ToString("c")

        grdOrders.DataSource = _dtPO.DefaultView

    End Sub
    Private Sub LoadDepartments(ByVal deptId As Int16)

        Dim departments = GetDepartments()

        lookupDepartments.Properties.DataSource = departments
        lookupDepartments.Properties.DisplayMember = "DepartmentName"
        lookupDepartments.Properties.ValueMember = "DepartmentId"

        lookupDepartments.EditValue = deptId

    End Sub
    Private Sub LoadItems(ByVal vid As Int16)
        Dim results = GetInventoryDb(vid)


        grdGetItems.Properties.DataSource = results
        grdGetItems.Properties.DisplayMember = "ItemDesc"
        grdGetItems.Properties.ValueMember = "Id"
        grdGetItems.Properties.SearchMode = DevExpress.XtraEditors.Repository.GridLookUpSearchMode.AutoSuggest
    End Sub
    Private Function GetDepartments() As Object
        Try

            Using getDepartmentinfo As New Departments()
                _fileName = _fileDir & GetDepartmentsSql
                getDepartmentinfo._sqlText = ReadSqlFile(_fileName)
                Return getDepartmentinfo.GetDepartmentInfo.ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderEdit.GetDepartments", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Function
    Private Function GetInventoryDb(ByVal vendorId As Int16) As Object

        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & InventorySqlItems
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.GetInventory.Where(Function(x) x.VId = vendorId).ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderEdit.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function

    Private Sub grdGetItems_EditValueChanged(sender As Object, e As EventArgs) Handles grdGetItems.EditValueChanged
        Dim arow = grdGetItems.GetSelectedDataRow()
        If arow Is Nothing Then
            Exit Sub
        End If

        txtCost.Text = arow.GetType().GetProperty("ItemVendorCost")?.GetValue(arow)
        _selectItem = arow.GetType().GetProperty("ItemDesc")?.GetValue(arow) & " - " & arow.GetType().GetProperty("Vendoritemid")?.GetValue(arow)
        txtQty.Text = String.Empty
        txtQty.Focus()

    End Sub

    Private Sub txtQty_KeyDown(sender As Object, e As KeyEventArgs) Handles txtQty.KeyDown

        If e.KeyCode = Keys.Enter Then
            If grdGetItems.EditValue Is Nothing Then
                grdGetItems.BackColor = Color.Red
                grdGetItems.Focus()
                txtQty.Text = String.Empty
                Exit Sub
            End If
            If txtQty.Text = String.Empty Then
                txtQty.BackColor = Color.Red
                txtQty.Focus()
                Exit Sub
            End If
            grdGetItems.BackColor = Color.White
            txtQty.BackColor = Color.White
            AddItemsToGrid()
            grdGetItems.EditValue = Nothing
            grdGetItems.SelectedText = String.Empty
            txtQty.Text = String.Empty
            grdGetItems.Focus()

        End If
    End Sub

    Private Sub AddItemsToGrid()
        Dim arow As DataRow
        Dim itemLineTotal As Decimal = 0

        itemLineTotal = (CType(txtCost.Text, Double) * CType(txtQty.Text, Int16))



        With _dtPO
            If Not CheckIfAlreadyExits(grdGetItems.EditValue) Then
                arow = .NewRow
                arow(0) = _selectItem
                arow(1) = grdGetItems.EditValue
                arow(2) = txtQty.Text
                arow(3) = txtCost.Text
                arow(4) = itemLineTotal
                arow(5) = 0

                .Rows.Add(arow)
            Else
                MsgBox("This item already exits in the grid!", MsgBoxStyle.Information, "Item Exists")
                Exit Sub
            End If

        End With

        _subtotal += itemLineTotal

        lblSubTotal.Text = _subtotal.ToString("C")

        lblTotalPo.Text = _subtotal.ToString("C")


        grdOrders.DataSource = _dtPO.DefaultView


    End Sub
    Private Function CheckIfAlreadyExits(ByVal theId As Int16) As Boolean

        For i = 0 To GridView1.DataRowCount - 1
            If theId = CType(GridView1.GetRowCellValue(i, "ItemId"), Int16) Then
                Return True
            End If

        Next

    End Function

    Private Sub RepositoryItemButtonEdit1_Click(sender As Object, e As EventArgs) Handles RepositoryItemButtonEdit1.Click
        GridView1.DeleteSelectedRows()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If (SavePurchaseOrder()) Then
            MessageBox.Show(Me, "Purchase order has been successfully saved!.", "Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Information)
            SalesPerson = txtSalesPerson.Text.Trim()
            QuoteNumber = txtQuoteNumber.Text.Trim()
            Department = _departmentText
            PaymentTerms = txtPaymentTerms.Text()
            If Not chkNeedsApproval.Checked Then
                Using frm As New frmRptViewer
                    frm._ponumber = _ponumber
                    frm._clientAcct = CustomerAcctNumber
                    frm._subTotal = _subtotal
                    frm._salesTax = _salestax
                    frm._totalSum = CType(lblTotalPo.Text, Double)
                    frm._notes = txtNotes.Text.Trim()
                    frm._needsApproval = chkNeedsApproval.Checked
                    frm._IsClient = False
                    frm._reportType = "PO"
                    frm.ShowDialog()
                End Using
            End If

            Me.Close()

        Else
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If

    End Sub

    Private Sub btnRecalculate_Click(sender As Object, e As EventArgs) Handles btnRecalculate.Click
        If Not IsNumeric(txtTax.Text) OrElse txtTax.Text = String.Empty Then
            MsgBox("Must have a tax amount in tax field to re-calculate total amount!", MsgBoxStyle.Critical, "Invalid option")
            Exit Sub
        End If

        lblTotalPo.Text = cTax().ToString("C")
    End Sub
    Private Function cTax() As Double
        _salestax = _subtotal * (txtTax.Text / 100)

        cTax = (_subtotal + _salestax)

    End Function
    Private Function SavePurchaseOrder() As Boolean

        _fileName = _fileDir & UpdatePurchaseOrderSql

        Try
            Using pos As New PurchaseOrders() With {
             ._sqlText = ReadSqlFile(_fileName),
             ._poNumber = _ponumber,
             ._department = lookupDepartments.EditValue,
             ._notes = txtNotes.Text,
             ._total = CType(lblTotalPo.Text, Double),
             ._salesTax = _salestax,
             ._salesPerson = txtSalesPerson.Text.Trim(),
             ._quoteNumber = txtQuoteNumber.Text.Trim(),
             ._paymentTerms = txtPaymentTerms.Text.Trim(),
             ._needsapproval = IIf(chkNeedsApproval.Checked, 1, 0),
             ._shipTo = _ShipTo,
             ._vendorId = VendorId,
             ._pid = _POId
             }

                If pos.UpdatePurchaseOrder() Then
                    _fileName = _fileDir & UpdateEditedPODetailSql

                    Using posdetails As New PurchaseOrders() With {._sqlText = ReadSqlFile(_fileName)}
                        For i = 0 To GridView1.DataRowCount - 1

                            posdetails._itemId = CType(GridView1.GetRowCellValue(i, "ItemId"), Int16)
                            posdetails._lineItemCost = CType(GridView1.GetRowCellValue(i, "Price"), Double)
                            posdetails._qty = CType(GridView1.GetRowCellValue(i, "Qty"), Int16)
                            posdetails._unitCost = CType(GridView1.GetRowCellValue(i, "UnitCost"), Double)
                            posdetails._itemDescription = CType(GridView1.GetRowCellValue(i, "ItemDesc"), String)
                            posdetails._did = CType(GridView1.GetRowCellValue(i, "DId"), Int16)
                            posdetails._pid = _POId

                            posdetails.UpdatePurchaseOrderDetails()
                        Next
                    End Using
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmPurchaseOrderEdit.SavePurchaseOrder", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True


    End Function

    Private Sub lookupDepartments_EditValueChanged(sender As Object, e As EventArgs) Handles lookupDepartments.EditValueChanged
        Dim arow = lookupDepartments.GetSelectedDataRow()
        If arow Is Nothing Then
            Exit Sub
        End If

        _departmentText = arow.GetType().GetProperty("DepartmentName")?.GetValue(arow)
    End Sub
End Class