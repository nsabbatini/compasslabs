﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class frmTrayClip
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        Dim Sel As String, Fnd As Boolean, CBtxt As String = "", pos As Integer = 1
        If txtTray.Text = "" Then
            MsgBox("No Tray")
        End If
        Sel = "Select PSPECNO From XTRAYS Where Tray='" & txtTray.Text & "' And PSPECNO!='HOLDER' Order by Trayx"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Fnd = False
        Do While rs.Read()
            If pos = Val(txtNeg.Text) Then
                CBtxt = CBtxt & "NEG" & vbCrLf : pos = pos + 1
            End If
            If pos = Val(txtPos.Text) Then
                    CBtxt = CBtxt & "POS" & vbCrLf : pos = pos + 1
            End If
            If pos = Val(txtBlnk.Text) Then
                CBtxt = CBtxt & "BLANK" & vbCrLf : pos = pos + 1
            End If
            CBtxt = CBtxt & rs(0) & vbCrLf
            Fnd = True
            pos = pos + 1
        Loop
        rs.Close()
        If Not Fnd Then
            MsgBox("Tray not found")
        Else
            Clipboard.SetText(CBtxt)
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub txtTray_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTray.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 And Asc(e.KeyChar) <> 9 Then
            Exit Sub
        End If
        If txtTray.Text = "" Then
            Exit Sub
        End If
    End Sub
End Class