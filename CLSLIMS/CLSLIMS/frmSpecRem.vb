﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmSpecRem

    Private Sub frmSpecRem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-NEDPC" Then CLSLIMS.UID = "NED.SABBATINI"
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "MARCIN.BARTCZAK" And CLSLIMS.UID <> "JOHN.SIDWELL" And CLSLIMS.UID <> "LAURA.WICKS" Then
            Me.Close()
            Me.Dispose()
        End If
        txtSpecNo.Focus()
    End Sub
    Private Sub txtSpecNo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSpecNo.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        lstInfo.Items.Clear()
        Dim Name As String = ValidateSpec(txtSpecNo.Text)
        If Name <> "" Then
            lblName.Text = Name
        Else
            lblName.Text = "Not found."
            txtSpecNo.Focus()
            Exit Sub
        End If
    End Sub
    Public Function ValidateSpec(ByVal specno As String) As String
        ValidateSpec = ""
        Dim sel = "Select Pfname+' '+plname From PSPEC a, PDEM b Where a.puid=b.puid and a.pefdt=b.pefdt and psact=1 and pspecno = '" & specno & "'"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            ValidateSpec = rs(0)
        End If
        rs.Close()
        Cmd.CommandText = "select WL,BATCH,MBATCH from pb where bact=1 and wl+batch+befdt in (select wl+batch+Befdt from pbi where pspecno='" & specno & "')"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            lstInfo.Items.Add(CLSLIMS.LJ(rs(0), 18) & Space(3) & CLSLIMS.LJ(rs(1), 10) & Space(3) & CLSLIMS.LJ(rs(2), 8))
            'lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        Loop
        rs.Close()
        Conn.Close()
    End Function
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnRem_Click(sender As Object, e As EventArgs) Handles btnRem.Click
        'lstInfo.Items.Clear()
        Dim WrkLst As String = ""
        If rbAll.Checked = False And rbSel.Checked = False Then
            MsgBox("Must check ALL or Selected.")
            Exit Sub
        End If
        If rbSel.Checked Then
            If lstInfo.SelectedItem = "" Then
                MsgBox("No Worklist Selected.")
                Exit Sub
            Else
                WrkLst = Trim(Mid(lstInfo.SelectedItem, 1, 18))
            End If
        ElseIf rbAll.Checked Then
            WrkLst = "All"
        End If
        If ValidateSpec(txtSpecNo.Text) <> "" Then
            Dim upd As String = ""
            Dim Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            lstInfo.Items.Add("Removing " & txtSpecNo.Text & " from " & WrkLst & ". . .")
            Dim Cmd As New SqlCommand, CmdUpd As New SqlCommand
            Cmd.Connection = Conn : CmdUpd.Connection = Conn
            If rbAll.Checked Then
                Cmd.CommandText = "select WL,BATCH,befdt from pb where bact=1 and wl+batch+befdt in (select wl+batch+Befdt from pbi where pspecno='" & txtSpecNo.Text & "')"
            ElseIf rbSel.Checked Then
                Cmd.CommandText = "Select WL,BATCH,befdt From PB Where bact=1 And wl='" & WrkLst & "' And wl+batch+befdt in (select wl+batch+Befdt from pbi where pspecno='" & txtSpecNo.Text & "')"
            End If
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read
                upd = "Delete from pbi where wl='" & rs(0) & "' and batch='" & rs(1) & "' and befdt='" & rs(2) & "' and pspecno='" & txtSpecNo.Text & "'"
                CmdUpd.CommandText = upd
                CmdUpd.ExecuteNonQuery()
                upd = "Update pb Set PBNOS=PBNOS-1 where wl='" & rs(0) & "' and batch='" & rs(1) & "' and befdt='" & rs(2) & "'"
                CmdUpd.CommandText = upd
                CmdUpd.ExecuteNonQuery()
                'upd = "Insert into QWL Values ('" & rs(0) & "','" & txtSpecNo.Text & "','" & GetWEFDT(rs(0)) & "')"
                'CmdUpd.CommandText = upd
                'Cmd.ExecuteNonQuery()
            Loop
            rs.Close()
            Conn.Close()
            If rbAll.Checked Then
                txtSpecNo.Text = ""
                lblName.Text = ""
                txtSpecNo.Focus()
            End If
            lstInfo.Items.Add("Done!") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1

        Else
            lblName.Text = "Invalid specimen number."
            lstInfo.Items.Clear()
            txtSpecNo.Focus()
        End If
    End Sub
    Public Function GetWEFDT(ByVal wl As String) As String
        GetWEFDT = ""
        Dim sel = "select wefdt from wl where wl='" & wl & "' and wact=1"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            GetWEFDT = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function

End Class