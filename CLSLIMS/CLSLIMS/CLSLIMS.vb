﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.Collections.Specialized

Public Class CLSLIMS
    'Public strCon As String = "Server=10.65.1.12;Database=TNCL_TEST;User Id=sa;Password=C0mp@SS**;MultipleActiveResultSets=true"
    Public strCon As String = "Server=10.65.1.12;Database=TNCL_PROD;User Id=sa;Password=C0mp@SS**;MultipleActiveResultSets=true"
    Public strConMySQL As String = "Server=192.168.65.100;Port=3306;Database=compass;Uid=nsabbatini;Pwd=Compass1910;"
    Public strConMySQLFin As String = "Server=192.168.65.100;Port=3306;Database=billing;Uid=nsabbatini;Pwd=Compass1910;"
    Public Conn As SqlConnection
    Public ConnMySQL As MySqlConnection
    Public Version As Integer = "125"
    Public AcctNum As String
    Public UID As String = "", DEPT As String = "", UNAME As String = ""
    Public THC As Boolean = False, ETG As Boolean = False
    Public Phone(50) As String
    Public IntText As String = ""
    Public OrderNum As String = ""
    Public PtrOne As String = "192.168.65.39", PtrTwo As String = ""

    Private Sub CLSLIMS_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 And e.Control Then
            If InStr(strCon, "TNCL_PROD") Then
                strCon = "Server=10.65.1.12;Database=TNCL_TEST;User Id=sa;Password=C0mp@SS**;MultipleActiveResultSets=true"
                Me.Text = "Compass Laboratory Services (Ver." & Version & ")" & " - TEST"
            Else
                strCon = "Server=10.65.1.12;Database=TNCL_PROD;User Id=sa;Password=C0mp@SS**;MultipleActiveResultSets=true"
                Me.Text = "Compass Laboratory Services (Ver." & Version & ")" & " - PROD"
            End If
        End If
        If e.KeyCode = Keys.F8 And e.Control Then
            btnLabels.Visible = True
        End If
        If e.Control And e.KeyCode = Keys.U Then
            UpdateCLSLIMS(True)
        End If
    End Sub

    Private Sub CLSLIMS_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim strDir As String = System.IO.Directory.GetCurrentDirectory
        'If strDir <> "C:\CLSLIMS" And strDir <> "C:\Compass\VBNET\CLSLIMS\CLSLIMS\bin\Debug" Then
        '    End
        'End If
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-NEDPC" Then
            UID = "NED.SABBATINI" : UNAME = "Sabbatini, Ned"
            btnReset.Visible = True
            btnRebill.Visible = True
            btnAddDrugDiag.Visible = True
            btnLabels.Visible = True
            btnSpecScan.Visible = True
            btnSpecRev.Visible = True
            btnHist.Visible = True
        ElseIf UCase(System.Net.Dns.GetHostName) = "COMPASS-IT1" Then
            UID = "L.MCLAUGHLIN" : UNAME = "McLaughlin, Louie"
            btnReset.Visible = True
            btnRebill.Visible = True
            btnAddDrugDiag.Visible = True
            btnLabels.Visible = True
            btnSpecScan.Visible = True
            btnSpecRev.Visible = True
            btnHist.Visible = True
        End If
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-DEDRICK" Then
            UID = "DEDRICK.RUSSELL" : UNAME = "Russell, Dedrick"
            btnLabels.Visible = True
        End If
        UpdateCLSLIMS(False)
        Me.Text = "Compass Laboratory Services (Ver." & Version & ")" & " - PROD"
        If System.IO.File.Exists("C:\CLSLIMS\PhoneExt.txt") Then
            TrackBar1.Visible = True
            lblExt.Visible = True
            Dim PE As Integer = 1
            Dim Ext As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader("C:\CLSLIMS\PhoneExt.txt")
            Do While Not Ext.EndOfStream
                Phone(PE) = Ext.ReadLine
                PE = PE + 1
            Loop
            Ext.Close()
            TrackBar1.Value = 1
            lblExt.Text = Phone(1)
            TrackBar1.Maximum = PE - 1
        Else
            TrackBar1.Visible = False
            lblExt.Visible = False
        End If
        If System.IO.File.Exists("C:\CLSLIMS\BP.ini") Then
            Dim Ext As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader("C:\CLSLIMS\BP.ini")
            PtrOne = Piece(Ext.ReadLine, "=", 2)
            PtrTwo = Piece(Ext.ReadLine, "=", 2)
            Ext.Close()
        End If
        'My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Beep)
        'Threading.Thread.Sleep(1200)
        'My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Beep)
        'My.Computer.Audio.Play("C:\Misc\Misc\good_bad_ugly.wav")
    End Sub
    Public Function UpdateCLSLIMS(ByVal Force As Boolean) As Boolean
        UpdateCLSLIMS = False
        Dim x As String, strCopy As String
        Try
            Dim inifile As System.IO.StreamReader = My.Computer.FileSystem.OpenTextFileReader("\\192.168.65.10\shared\CLSLIMS\CLSLIMS.ini")
            x = inifile.ReadLine
            inifile.Close()
            If Val(x) > Version Or Force Then
                If System.IO.File.Exists("C:\CLSLIMS\Update.bat") Then
                    System.IO.File.Delete("C:\CLSLIMS\Update.bat")
                End If
                'Create Update.Bat file
                Dim updfile As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\Update.bat", False)
                strCopy = "COPY \\192.168.65.10\shared\CLSLIMS\*.* C:\CLSLIMS\"
                updfile.WriteLine(vbCrLf)
                updfile.WriteLine(strCopy)
                updfile.WriteLine("CLSLIMS.exe")
                updfile.WriteLine("Exit")
                updfile.Close()
                'Kill all running CLSLIMSs
                Dim proc() As Process = Process.GetProcessesByName("CLSLIMS")
                Dim CurrProc As Process = Process.GetCurrentProcess
                For Each procs As Process In proc
                    If procs.Id <> CurrProc.Id Then
                        procs.Kill()
                    End If
                Next procs
                'Run update.bat
                Dim p As New System.Diagnostics.Process()
                p.StartInfo.FileName = "C:\CLSLIMS\update.bat"
                p.Start()
                End
            End If
        Catch

        End Try

    End Function
    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        End
    End Sub

    Private Sub btnBadFlag_Click(sender As System.Object, e As System.EventArgs) Handles btnBadFlag.Click
        BadFlag.ShowDialog()
    End Sub

    Private Sub Button2_Click_1(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        EP.ShowDialog()
    End Sub

    Private Sub btnOCInq_Click(sender As System.Object, e As System.EventArgs) Handles btnOCInq.Click
        OCInq.ShowDialog()
    End Sub

    Private Sub btnAcctInq_Click(sender As System.Object, e As System.EventArgs) Handles btnAcctInq.Click
        AcctNum = ""
        AcctInq.ShowDialog()
    End Sub

    Private Sub btnPhyInq_Click(sender As System.Object, e As System.EventArgs) Handles btnPhyInq.Click
        PhyInq.ShowDialog()
    End Sub

    Private Sub btnSpcInq_Click(sender As System.Object, e As System.EventArgs) Handles btnSpcInq.Click
        SpcInq.ShowDialog()
    End Sub

    Private Sub btnEPMotion_Click(sender As System.Object, e As System.EventArgs) Handles btnEPMotion.Click
        THC = False : ETG = False
        epMotion.ShowDialog()
    End Sub

    Private Sub btnEPMotionTHC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPMotionTHC.Click
        THC = True : ETG = False
        epMotion.ShowDialog()
        THC = False
    End Sub
    Private Sub btnEPMotionETG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPMotionETG.Click
        THC = False : ETG = True
        epMotion.ShowDialog()
        THC = False : ETG = False
    End Sub

    Public Function Piece(ByVal str As String, Optional ByVal charz As String = "^", Optional ByVal pos As Integer = 1) As String
        Dim pc As Array
        If pos < 1 Then pos = 1
        If InStr(str, charz) = 0 Then
            Piece = str
            Exit Function
        End If
        pc = Split(str, charz)
        If UBound(pc) < pos - 1 Then
            Piece = ""
        Else
            Piece = pc(pos - 1)
        End If

    End Function
    Public Function NumberofPieces(ByVal str As String, Optional ByVal charz As String = "^") As Integer
        Dim c As Integer = 1, i As Integer
        NumberofPieces = 0
        For i = 1 To 9999
            str = Mid(str, Len(Piece(str, charz, 1)) + 2)
            If str = "" Then
                Exit For
            End If
        Next i
        NumberofPieces = i
    End Function

    Public Function ReplacePiece(ByVal str As String, ByVal charz As String, ByVal pos As Integer, ByVal data As String) As String
        ReplacePiece = str
        Dim r As String = ""
        Dim nop As Integer = NumberofPieces(str, charz)
        If pos > nop Then
            For i = 1 To (pos - nop - 1)
                r = r & charz
            Next i
            r = str & r & charz & data
        Else
            For i = 1 To nop
                If i = pos Then
                    r = r & data & charz
                Else
                    r = r & Piece(str, charz, i) & charz
                End If
            Next i
        End If
        ReplacePiece = r
    End Function

    Public Function DD(ByVal col As NameValueCollection, ByVal lkup As String) As Boolean
        DD = False
        For Each key In col
            If key = lkup Then
                DD = True
                Exit For
            End If
        Next key

    End Function


    Private Sub btnMonStat_Click(sender As System.Object, e As System.EventArgs) Handles btnMonStat.Click
        MonStat.ShowDialog()
    End Sub

    Private Sub btnAddMeds_Click(sender As System.Object, e As System.EventArgs) Handles btnAddMeds.Click
        AddMed.ShowDialog()
    End Sub

    Private Sub btnOE_Click(sender As System.Object, e As System.EventArgs) Handles btnOE.Click
        QuickOE.ShowDialog()
    End Sub
    Public Function CDT(Optional dt As String = "", Optional dir As String = "D") As String        'D=database, V=view
        CDT = ""
        If dt = "" And dir = "D" Then
            CDT = Format(Now, "yyyyMMddHHmmss")
        ElseIf dt <> "" And dir = "D" Then
            CDT = Format("#" & dt & "#", "yyyyMMddHHmmss")
        ElseIf dt <> "" And dir = "V" Then
            CDT = Mid(dt, 5, 2) & "/" & Mid(dt, 7, 2) & "/" & Mid(dt, 1, 4)
        End If
    End Function

    Public Function LJ(ByVal strLJ As String, ByVal iLen As Integer) As String
        LJ = ""
        If Len(strLJ) > iLen Then
            LJ = strLJ
        Else
            LJ = strLJ & Space(iLen - Len(strLJ))
        End If
    End Function
    Public Function RJ(ByVal strRJ As String, ByVal iLen As Integer) As String
        RJ = ""
        If Len(strRJ) > iLen Then
            RJ = strRJ
        Else
            RJ = Space(iLen - Len(strRJ)) & strRJ
        End If
    End Function

    Private Sub btnLabels_Click(sender As System.Object, e As System.EventArgs) Handles btnLabels.Click
        LabelPrt.ShowDialog()
    End Sub
    Public Function ImgFile(ByVal Acct As String, ByVal ID As String) As Boolean
        ImgFile = False
        Try
            If System.IO.File.Exists("\\CMPFS1\Accounts\" & Acct & ID & ".pdf") Then
                ImgFile = True
                Exit Function
            End If
        Catch
        End Try
        Try
            If System.IO.File.Exists("\\CL-AGILENT\Accounts\" & Acct & ID & ".pdf") Then
                ImgFile = True
            End If
        Catch
        End Try

    End Function
    Public Function FirstSpace(ByVal str As String) As Integer
        FirstSpace = 0
        For i = Len(str) To 1 Step -1
            If Asc(Mid(str, i, 1)) = 32 Then
                FirstSpace = i
                Exit For
            End If
        Next i
    End Function
    Public Function ValidateUser(usrnme As String, pass As String) As Boolean
        ValidateUser = False
        Dim syspass As String = "x"
        Dim xSel As String = "Select UPWD,UFNAME,ULNAME From SYSUSER Where UACT=1 And UID='" & usrnme & "'"
        Using Conn As New SqlConnection(strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                syspass = cRS(0)
                UNAME = cRS(2) & ", " & cRS(1)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
        If syspass = pass Then
            ValidateUser = True
        End If

    End Function

    Public Function GetDept(usrnme As String) As String
        GetDept = ""
        Dim xSel As String = "Select DEPT From SYSUSER Where UACT=1 And UID='" & usrnme & "'"
        Using Conn As New SqlConnection(strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetDept = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Private Sub btnPrtReq_Click(sender As System.Object, e As System.EventArgs)
        PrtReq.ShowDialog()
    End Sub
    Public Sub Log(ByRef txt As String)
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\Log\" & Format(Now, "yyMMdd") & ".txt", True)
        file.WriteLine(Now & "-" & txt)
        file.Close()
    End Sub
    Public Function ResetSpecNo(ByRef specno As String) As Boolean
        ResetSpecNo = False
        If specno = "" Then Exit Function
        Dim Conn As New SqlConnection(strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "delete from pcont where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pconttc where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from poc where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from poctc where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pspec where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pres where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from qacct where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from qpres where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from qrpmon where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from qwl where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from xtrays where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pord where req = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pbi where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from INSQDL where ispecno = '" & specno & "01'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pimg where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pscmt where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from pscmti where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from psstat where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from qbill where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from xorders where orderkey = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from qrpt where rpsort = '" & specno & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "delete from prpt where pspecno = '" & specno & "'"
        Cmd.ExecuteNonQuery()

        Cmd.Dispose()
        Conn.Close()
        Conn.Dispose()
        ResetSpecNo = True
    End Function
    Private Sub btnReset_Click(sender As System.Object, e As System.EventArgs) Handles btnReset.Click
        Dim xpspec As String = InputBox("SpecNo:")
        If xpspec = "" Then Exit Sub
        If ResetSpecNo(xpspec) Then MsgBox("Done.")

    End Sub


    Public Function GetAcctName(ByVal Acct) As String
        GetAcctName = ""

        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where Acct = '" & Acct & "' and aact=1 "

        Using Conn As New SqlConnection(strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = cRS(1)
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Public Function GetPatName(ByVal spec As String) As String
        GetPatName = ""

        Dim xSel As String
        xSel = "select plname,pmname,pfname " &
               "from pspec a, pdem b " &
               "where psact=1 and a.puid=b.puid and a.pefdt=b.pefdt and pspecno='" & spec & "'"

        Using Conn As New SqlConnection(strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetPatName = cRS(0) & ", " & cRS(2) & " " & Mid(cRS(1), 1, 1)
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Private Sub btnBuildPain_Click(sender As System.Object, e As System.EventArgs) Handles btnBuildPain.Click
        BuildPain.ShowDialog()
    End Sub

    Private Sub btnPrtWrklst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrtWrklst.Click
        PrtWrklst.ShowDialog()
    End Sub
    Public Function RemoveTrailingChar(ByVal x As String, ByVal ch As String) As String
        RemoveTrailingChar = x
        For i = Len(x) To 1 Step -1
            If Mid(x, Len(x), 1) = ch Then
                x = Mid(x, 1, Len(x) - 1)
            Else
                Exit For
            End If
        Next i
        RemoveTrailingChar = x
    End Function
    Public Function GetMC(ByVal MC As String) As String
        GetMC = ""
        Dim xSel As String = "Select MTEXT From MC a, MCI b Where a.MC=b.MC and MACT=1 And MC='" & MC & "' Order by MCIx"
        Using Conn As New SqlConnection(strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                GetMC = GetMC & cRS(0) & vbCrLf
            Loop
            cRS.Close()
            Cmd.Dispose()
            GetMC = Mid(GetMC, 1, Len(GetMC) - 2)
        End Using

    End Function
    Public Function GetStatusCodeDesc(ByVal statcd As String) As String
        GetStatusCodeDesc = ""
        Dim xSel As String = "Select STCDDESC From STATCD Where STATCD ='" & statcd & "'"
        Using Conn As New SqlConnection(strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                GetStatusCodeDesc = cRS(0)
            Loop
            cRS.Close()
            Cmd.Dispose()
        End Using

    End Function
    Private Sub MenuTab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuTab.Click
        Dim HTML As String = "<html><body>Building</body></html>"
        'Debug.Print(MenuTab.SelectedTab.Name)
        'Do While MenuTab.SelectedTab.Name = "Info"
        If MenuTab.SelectedTab.Name = "Info" Then
            BuildDiskSpace()
            'wbDailyInfo.DocumentText = HTML
            wbDailyInfo.Refresh()
            'System.Threading.Thread.Sleep(1000)
            'BuildDiskSpace()
            Try
                'wbDailyInfo.Navigate("\\cl-nas\shared\DSF\DiskSpace.html")
                wbDailyInfo.Navigate("C:\CLSLIMS\DailyInfo.html")
            Catch
                Debug.Print("Problem")
            End Try
            MenuTab.Refresh()
            System.Threading.Thread.Sleep(1000)
            Debug.Print(Now)
            'Loop
        End If
    End Sub
    Public Sub BuildDiskSpace()
        Dim datestr As String = Format(Now, "MMddyy"), TotalCnt As Integer = 0, TotalClin As Integer = 0, TotalTox As Integer = 0, Inh As String = "", htmlstr2 As String = "", InsQ As String = "", htmlstr3 As String = "", htmlstr4 As String = ""
        Dim QOE As String = "", DE As String = ""
        Dim xSel As String = "select tray,uid,count(*) from xtrays where pspecno != 'HOLDER' And TRAY like 'TR%-" & datestr & "' group by tray,uid order by tray"
        Dim htmlstr As String = "<body><table border=1><th>Tray</th><th>Processor</th><th>Count</th>"
        Dim Conn As New SqlConnection(strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(xSel, Conn)
        Dim cRS As SqlDataReader = Cmd.ExecuteReader
        Do While cRS.Read
            TotalTox = TotalTox + Val(cRS(2))
            htmlstr4 = htmlstr4 & "<tr><td style=background-color:yellow;>" & cRS(0) & "</td><td style=background-color:yellow;>" & cRS(1) & "</td><td style=background-color:yellow;>" & cRS(2) & "</td></tr>"
            TotalCnt = TotalCnt + Val(cRS(2))
        Loop
        cRS.Close()
        Cmd.CommandText = "select rpque,count(*) from qrpt group by rpque"
        cRS = Cmd.ExecuteReader
        Inh = "<br><u>Reporting Queues</u>"
        Do While cRS.Read
            Inh = Inh & "<br>" & cRS(0) & ": " & cRS(1) '& "</br>"
        Loop
        cRS.Close()
        InsQ = "<br><u>Instrument Queues</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
        InsQ = InsQ & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
        InsQ = InsQ & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Format(Now, "MM/dd/yyyy HH:mm:ss") '& "</br>"
        Cmd.CommandText = "select ins,count(*) from qiul group by ins"
        cRS = Cmd.ExecuteReader
        Do While cRS.Read
            InsQ = InsQ & "<br>" & cRS(0) & ":&nbsp" & cRS(1) '& "</br>"
        Loop
        cRS.Close()
        Cmd.CommandText = "select pstat,count(*) from pspec where substring(psrcvdt,1,8)='" & Format(Now, "yyyyMMdd") & "' and psact=1 group by pstat"
        cRS = Cmd.ExecuteReader
        Do While cRS.Read
            If cRS(0) = "1" Then QOE = "Quick OE only: " & cRS(1)
            If cRS(0) = "2" Then DE = "Data Entry: " & cRS(1)
        Loop
        cRS.Close()
        Cmd.Dispose()
        Conn.Close()

        htmlstr = InsQ & Inh
        htmlstr = htmlstr & "<br><u><b>Sample Counts for " & Format(Now, "MM/dd/yyyy") & "</b></u>"
        htmlstr = htmlstr & "</br><table border=0><tr><td style=background-color:yellow;>Total Tox:&nbsp&nbsp" & TotalTox & "</td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<td style=background-color:dodgerblue;>" & QOE & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp" & DE & "</td></tr></table></br>"
        htmlstr = htmlstr & "<table border=1>" & htmlstr4 & "</table></body>"
        Debug.Print(htmlstr)

        Dim outfile As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\DailyInfo.html", False)
        outfile.WriteLine("<html>" & htmlstr) '& "<p></p><head><u><b>Disk Space Usage</b></u></head><body><table border=1><th>Computer</th><th>Day</th><th>%Free</th><th>Free</th><th>Total</th>")
        'Dim infile As System.IO.StreamReader
        'Dim Free As String = "", Total As String = "", ComputerName As String = "", Dt As String = "", r As String = "", DOW As String = ""
        'Dim lFree As Long = 0, lTotal As Long = 0, percent As Double = 0.0, fpercent As String = ""
        'Dim DSF As New System.IO.DirectoryInfo("\\cl-nas\Shared\DSF")
        'For Each fl In DSF.GetFiles.OrderBy(Function(x) x.Name)
        '    If Piece(fl.Name, ".", 2) = "txt" Then
        '        DOW = Mid(fl.Name, Len(fl.Name) - 6, 3)
        '        infile = My.Computer.FileSystem.OpenTextFileReader(fl.FullName)
        '        Free = Trim(infile.ReadLine) : Total = Trim(infile.ReadLine) : ComputerName = Trim(infile.ReadLine) : Dt = Trim(infile.ReadLine)
        '        lFree = Val(Free) : lTotal = Val(Total) : percent = (lFree) / lTotal * 100 : fpercent = Format(percent, "##0.0")
        '        outfile.WriteLine("<tr><td>" & ComputerName & "</td><td>" & DOW & "-" & Piece(Dt, " ", 1) & "</td><td align=center>" & fpercent & "</td><td align=center>" & Format(lFree / 1073741824, "###,##0.00") & " GB</td><td align=center>" & Format(lTotal / 1073741824, "###,##0.00") & " GB</td></tr>")
        '        infile.Close() : Free = "" : Total = "" : ComputerName = "" : Dt = "" : lFree = 0 : lTotal = 0 : percent = 0.0 : fpercent = ""
        '    End If
        'Next fl
        outfile.WriteLine("</table></body></html>")
        outfile.Close()
    End Sub
    Public Function GetOCAbbr(ByVal oc As String) As String
        Dim xSel As String = "Select OABRV From OC Where OACT= 1 And oc ='" & oc & "'"
        GetOCAbbr = ""
        Using Conn As New SqlConnection(strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetOCAbbr = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Public Function FormatListBox(ByVal str As String) As String
        'str="Data1;Width1;LJ^Data2;Width2;RJ^ . . .
        FormatListBox = ""
        Dim x As String = ""
        For i = 1 To NumberofPieces(str)
            x = Piece(str, "^", i)
            If Piece(x, ";", 3) = "LJ" Then
                FormatListBox = FormatListBox & LJ(Piece(x, ";", 1), Piece(x, ";", 2))
            Else
                FormatListBox = FormatListBox & RJ(Piece(x, ";", 1), Piece(x, ";", 2))
            End If
        Next i
    End Function
    Public Function ShellExecute(ByVal File As String) As Boolean
        ShellExecute = True
        Dim myProcess As New Process
        myProcess.StartInfo.FileName = File
        myProcess.StartInfo.UseShellExecute = True
        myProcess.StartInfo.RedirectStandardOutput = False
        myProcess.Start()
        myProcess.Dispose()
    End Function

    Private Sub btnPrintMBN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintMBN.Click
        PrtMBN.ShowDialog()
    End Sub

    Private Sub btnFldrAglnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFldrAglnt.Click
        FldrAglnt.ShowDialog()
    End Sub

    Private Sub btnChiral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChiral.Click
        ChiralList.ShowDialog()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        PrtWrklstO.ShowDialog()
    End Sub

    Private Sub DailyInfo_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DailyInfo.Tick
        Try
            'wbDailyInfo.Navigate("\\cl-nas\shared\DailyCounts\DailyInfo.html")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnMBNClip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMBNClip.Click
        MBNClip.ShowDialog()
    End Sub

    Private Sub btnLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLabel.Click
        PrntLabel.ShowDialog()
    End Sub

    Private Sub btnMBNLbl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMBNLbl.Click
        MBNLbl.ShowDialog()
    End Sub

    Private Sub btnAcctSum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcctSum.Click
        AcctSum.ShowDialog()
    End Sub

    Private Sub btnSpecRev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecRev.Click
        SpecRev.ShowDialog()
    End Sub

    Private Sub btnFldrClip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFldrClip.Click
        FldrClip.ShowDialog()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub btnSO_Click(sender As System.Object, e As System.EventArgs) Handles btnSO.Click
        SendOuts.ShowDialog()
    End Sub

    Private Sub btnCardioRpt_Click(sender As System.Object, e As System.EventArgs)
        CardioRpt.ShowDialog()
    End Sub

    Private Sub TrackBar1_MouseWheel(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TrackBar1.MouseWheel

        'If e.Delta < 0 Then
        '    Debug.Print("B" & TrackBar1.Value)
        '    If TrackBar1.Value <> TrackBar1.Maximum Then
        '        TrackBar1.Value = TrackBar1.Value + 1
        '    End If
        '    Debug.Print("A" & TrackBar1.Value)
        'Else
        '    Debug.Print("B" & TrackBar1.Value)
        '    If TrackBar1.Value < 1 Then
        '        TrackBar1.Value = TrackBar1.Value - 1
        '    End If
        '    Debug.Print("A" & TrackBar1.Value)
        'End If

    End Sub

    Private Sub btnHam_Click(sender As System.Object, e As System.EventArgs) Handles btnHam.Click
        frmBuildHam.ShowDialog()
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs)
        TrackBar1.Value = TrackBar1.Value + 1
    End Sub

    Private Sub TrackBar1_ValueChanged(sender As Object, e As System.EventArgs) Handles TrackBar1.ValueChanged
        lblExt.Text = Phone(TrackBar1.Value)
    End Sub

    Private Sub btnAcctScan_Click(sender As System.Object, e As System.EventArgs) Handles btnAcctScan.Click
        frmAcctScan.ShowDialog()
    End Sub

    Private Sub btnEMRList_Click(sender As System.Object, e As System.EventArgs) Handles btnEMRList.Click
        frmEMRList.ShowDialog()
    End Sub

    Private Sub btnEMRDef_Click(sender As System.Object, e As System.EventArgs) Handles btnEMRDef.Click
        frmEMRDef.ShowDialog()
    End Sub

    Private Sub btnTCalc_Click(sender As System.Object, e As System.EventArgs) Handles btnTCalc.Click
        frmTCalc.ShowDialog()
    End Sub

    Private Sub btnScanCourier_Click(sender As System.Object, e As System.EventArgs) Handles btnScanCourier.Click
        frmScanCour.ShowDialog()
    End Sub

    Private Sub btnTrackRpt_Click(sender As System.Object, e As System.EventArgs) Handles btnTrackRpt.Click
        frmTrackRpt.ShowDialog()
    End Sub

    Private Sub btnCBC_Click(sender As System.Object, e As System.EventArgs) Handles btnCBC.Click
        frmCBCTray.ShowDialog()
    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
        frmNMRBatch.ShowDialog()
    End Sub

    Private Sub btnstatcodeq_Click(sender As Object, e As EventArgs) Handles btnstatcodeq.Click
        Queue.ShowDialog()

    End Sub

    Private Sub btnaffdef_Click(sender As Object, e As EventArgs) Handles btnaffdef.Click
        AffDef.ShowDialog()

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MonStatusCount.ShowDialog()

    End Sub

    Private Sub btnAbbrv_Click(sender As Object, e As EventArgs) Handles btnAbbrv.Click
        frmTestAbbrv.ShowDialog()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        frmRevOCLkup.ShowDialog()
    End Sub

    Private Sub btnAcctOC_Click(sender As Object, e As EventArgs) Handles btnAcctOC.Click
        frmAcctOC.ShowDialog()
    End Sub

    Private Sub btnAcctOCs_Click(sender As Object, e As EventArgs) Handles btnAcctOCs.Click
        frmAcctOCs.ShowDialog()
    End Sub

    Private Sub btnPending_Click(sender As Object, e As EventArgs) Handles btnPending.Click
        frmPending.ShowDialog()
    End Sub

    Private Sub btnRebill_Click(sender As Object, e As EventArgs) Handles btnRebill.Click
        frmRebill.ShowDialog()
    End Sub

    Private Sub btnAddDrugDiag_Click(sender As Object, e As EventArgs) Handles btnAddDrugDiag.Click
        frmAddDrugDiag.ShowDialog()
    End Sub

    Private Sub btnAffScan_Click(sender As Object, e As EventArgs) Handles btnAffScan.Click
        frmAffScan.ShowDialog()
    End Sub

    Private Sub btnBatchRev_Click(sender As Object, e As EventArgs) Handles btnBatchRev.Click
        frmReview.ShowDialog()
    End Sub

    Private Sub btnUrnSal_Click(sender As Object, e As EventArgs) Handles btnUrnSal.Click
        frmUrnSal.ShowDialog()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        UpdIns.ShowDialog()
    End Sub

    Private Sub btnPay_Click(sender As Object, e As EventArgs) Handles btnPay.Click
        frmPay.ShowDialog()
    End Sub

    Private Sub btnQue_Click(sender As Object, e As EventArgs) Handles btnQue.Click
        'Using frm As New frmGetQuedOrdersb
        '    frm.ShowDialog()
        'End Using
        frmQues.ShowDialog()
    End Sub

    Private Sub btnIntVen_Click(sender As Object, e As EventArgs) Handles btnIntVen.Click
        frmInvPain.ShowDialog()
    End Sub

    Private Sub btnAcc_Click(sender As Object, e As EventArgs) Handles btnAcc.Click
        frmAcc.ShowDialog()
    End Sub

    Private Sub btnItemDef_Click(sender As Object, e As EventArgs) Handles btnItemDef.Click
        frmItemDef.ShowDialog()
    End Sub

    Private Sub btnLocDef_Click(sender As Object, e As EventArgs) Handles btnLocDef.Click
        frmLoc.ShowDialog()
    End Sub

    Private Sub btnVndDef_Click(sender As Object, e As EventArgs) Handles btnVndDef.Click
        frmVndDef.ShowDialog()
    End Sub

    Private Sub btnRecvInv_Click(sender As Object, e As EventArgs)
        Using frm As New frmReceiveInventory
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        frmReqNumInput.ShowDialog()
    End Sub

    Private Sub btnPrintSVT_Click(sender As Object, e As EventArgs) Handles btnPrintSVT.Click
        frmSVT.ShowDialog()
    End Sub

    Private Sub btnSlsProc_Click(sender As Object, e As EventArgs) Handles btnSlsProc.Click
        frmSlsProc.ShowDialog()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        frmAcctDef.ShowDialog()
    End Sub

    Private Sub btnSlsProcLst_Click(sender As Object, e As EventArgs) Handles btnSlsProcLst.Click
        frmSlsProcList.ShowDialog()
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        frmAcctSlsProc.ShowDialog()
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Using frm As New frmPurchaseOrders
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub btnORL_Click(sender As Object, e As EventArgs) Handles btnORL.Click
        frmORL.ShowDialog()
    End Sub

    Private Sub btnFedxUPS_Click(sender As Object, e As EventArgs) Handles btnFedxUPS.Click
        frmFedxUPS.ShowDialog()
    End Sub

    Private Sub btnInsQue_Click(sender As Object, e As EventArgs) Handles btnInsQue.Click
        frmInqQue.ShowDialog()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        frmAccRcvDt.ShowDialog()
    End Sub

    Private Sub btnAnthem_Click(sender As Object, e As EventArgs) Handles btnAnthem.Click
        frmAnthem.ShowDialog()
    End Sub

    Private Sub btnMedRecScan_Click(sender As Object, e As EventArgs) Handles btnMedRecScan.Click
        frmMRScan.ShowDialog()
    End Sub

    Private Sub btnTAT_Click(sender As Object, e As EventArgs) Handles btnTAT.Click
        frmTAT.ShowDialog()
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        frmSpecRem.ShowDialog()
    End Sub

    Private Sub btnSpcDis_Click(sender As Object, e As EventArgs) Handles btnSpcDis.Click
        frmSpcDis.ShowDialog()
    End Sub

    Private Sub btnAntiBiotic_Click(sender As Object, e As EventArgs) Handles btnAntiBiotic.Click
        frmDefAB.ShowDialog()
    End Sub

    Private Sub btnSpcNum_Click(sender As Object, e As EventArgs) Handles btnSpcNum.Click
        frmSpcNum.ShowDialog()
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        frmDefOrg.ShowDialog()
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        frmDefGene.ShowDialog()
    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        frmABOrg.ShowDialog()
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        frmABGene.ShowDialog()
    End Sub

    Private Sub btnCutRng_Click(sender As Object, e As EventArgs) Handles btnCutRng.Click
        frmDefCutRng.ShowDialog()
    End Sub

    Private Sub btnImpA_Click(sender As Object, e As EventArgs) Handles btnImpA.Click
        frmImpA.ShowDialog()
    End Sub

    Private Sub btnMonRpt_Click(sender As Object, e As EventArgs) Handles btnAcctMonRpt.Click
        frmSOSMonRpt.ShowDialog()
    End Sub

    Private Sub btnMonRpt_Click_1(sender As Object, e As EventArgs) Handles btnMonRpt.Click

        frmSOSMonNum.ShowDialog()
    End Sub

    Private Sub btnHist_Click(sender As Object, e As EventArgs) Handles btnHist.Click
        frmHist.ShowDialog()
    End Sub

    Private Sub btnAutoFax_Click(sender As Object, e As EventArgs) Handles btnAutoFax.Click
        frmAutoFax.ShowDialog()
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles btnEVW.Click
        frmEVW.ShowDialog()
    End Sub

    Private Sub btnNNL_Click(sender As Object, e As EventArgs) Handles btnNNL.Click
        frmNNL.ShowDialog()
    End Sub

    Private Sub Button17_Click_1(sender As Object, e As EventArgs) Handles Button17.Click
        frmRelTray.ShowDialog()
    End Sub

    Private Sub btnReceiveHistory_Click(sender As Object, e As EventArgs)
        Using frm As New frmReceiveHistory
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub btnEstablish_Click(sender As Object, e As EventArgs)
        Using frm As New frmUpdateInventory
            frm.ShowDialog()
        End Using
    End Sub

    Private Sub btnImpB_Click(sender As Object, e As EventArgs) Handles btnImpB.Click
        frmImpB.ShowDialog()
    End Sub

    Private Sub btnSpecRel_Click(sender As Object, e As EventArgs) Handles btnSpecRel.Click
        frmSpecRel.ShowDialog()
    End Sub

    Private Sub btnPX_Click(sender As Object, e As EventArgs) Handles btnOrder.Click
        'Using frm As New frmMainOrderForm
        '    frm.ShowDialog()
        'End Using
        frmOrders.ShowDialog()
    End Sub

    Private Sub btnCorrFlag_Click(sender As System.Object, e As System.EventArgs)
        frmCorrectFlag.ShowDialog()
    End Sub

    Friend Class CLSLIMS
    End Class
End Class
Public Class PCPrint : Inherits Printing.PrintDocument
    Private _font As Font
    Private _text As String
    Public Property TextToPrint() As String
        Get
            Return _text
        End Get
        Set(ByVal Value As String)
            _text = Value
        End Set
    End Property
    Public Property PrinterFont() As Font

        ' Allows the user to override the default font
        Get
            Return _font
        End Get
        Set(ByVal Value As Font)
            _font = Value
        End Set
    End Property
    Public Sub New()
        'Call the base classes constructor
        MyBase.New()
        'Instantiate out Text property to an empty string
        _text = String.Empty
    End Sub
    Public Sub New(ByVal str As String)
        MyBase.New()
    End Sub
    Protected Overrides Sub onbeginPrint(ByVal e As Printing.PrintEventArgs)
        ' Run base code
        MyBase.OnBeginPrint(e)

        'Check to see if the user provided a font
        'if they didnt then we default to Times New Roman
        If _font Is Nothing Then
            'Create the font we need
            _font = New Font("Times New Roman", 10)
        End If
    End Sub
    Protected Overrides Sub OnPrintPage(ByVal e As Printing.PrintPageEventArgs)
        ' Run base code
        MyBase.OnPrintPage(e)
        'Declare local variables needed
        Static curChar As Integer
        Dim printHeight As Integer
        Dim printWidth As Integer
        Dim leftMargin As Integer
        Dim rightMargin As Integer
        Dim lines As Int32
        Dim chars As Int32

        'Set print area size and margins
        With MyBase.DefaultPageSettings
            printHeight = .PaperSize.Height - .Margins.Top - .Margins.Bottom
            printWidth = .PaperSize.Width - .Margins.Left - .Margins.Right
            leftMargin = .Margins.Left 'X
            rightMargin = .Margins.Top   'Y
        End With

        'Check if the user selected to print in Landscape mode
        'if they did then we need to swap height/width parameters
        If MyBase.DefaultPageSettings.Landscape Then
            Dim tmp As Integer
            tmp = printHeight
            printHeight = printWidth
            printWidth = tmp
        End If

        'Now we need to determine the total number of lines
        'we're going to be printing
        Dim numLines As Int32 = CInt(printHeight / PrinterFont.Height)

        'Create a rectangle printing are for our document
        Dim printArea As New RectangleF(leftMargin, rightMargin, printWidth, printHeight)

        'Use the StringFormat class for the text layout of our document
        Dim format As New StringFormat(StringFormatFlags.LineLimit)

        'Fit as many characters as we can into the print area     

        e.Graphics.MeasureString(_text.Substring(RemoveZeros(curChar)), PrinterFont, New SizeF(printWidth, printHeight), format, chars, lines)

        'Print the page
        e.Graphics.DrawString(_text.Substring(RemoveZeros(curChar)), PrinterFont, Brushes.Black, printArea, format)

        'Increase current char count
        curChar += chars

        'Detemine if there is more text to print, if
        'there is the tell the printer there is more coming
        If curChar < _text.Length Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            curChar = 0
        End If
    End Sub
    Public Function RemoveZeros(ByVal value As Integer) As Integer
        'Check the value passed into the function,
        'if the value is a 0 (zero) then return a 1,
        'otherwise return the value passed in
        Select Case value
            Case 0
                Return 1
            Case Else
                Return value
        End Select
    End Function

End Class