﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmNNL
    Public lvDataSortOrder As String = "Ascending"
    Private Sub frmNNL_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lvData.Items.Clear()
        lvData.Sorting = SortOrder.Ascending
        Dim item As ListViewItem = Nothing, St As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Sel As String = "select a.pspecno,b.acct,aname,prslt,substring(PSRCVDT,5,2)+'/'+substring(PSRCVDT,7,2)+'/'+substring(PSRCVDT,1,4),prstat,pstate " &
        "From pspec a, pord b, acct c, pres d, pdem e " &
        "where pract=1 and rc='M00521' and (prslt='positive' or prslt='indeterminate') and psact=1 " &
        "and a.pspecno=d.pspecno and a.pord=b.pord and a.poefdt=b.poefdt and b.acct=c.acct and b.aefdt=c.aefdt " &
        "and a.puid=e.puid and a.pefdt=e.pefdt " &
        "order by psrcvdt"
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            item = lvData.Items.Add(rs(0))
            item.SubItems.Add(rs(1))
            item.SubItems.Add(rs(2))
            item.SubItems.Add(rs(3))
            item.SubItems.Add(rs(4))
            Select Case rs(5)
                Case 1 : St = "No results"
                Case 2 : St = "Entered"
                Case 3 : St = "Verified"
                Case 6 : St = "Reported"
            End Select
            item.SubItems.Add(St)
            item.SubItems.Add(rs(6))
        Loop
        rs.Close()
        Conn.Close()
        lvData.Sort()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\NNL.csv", False)
        'file.WriteLine("Account,Account Name,Salesman,Salesman Name,Processor,Processor Name,State,Active")
        For i = 0 To lvData.Items.Count - 1
            For j = 0 To 6
                file.Write(lvData.Items(i).SubItems(j).Text & ",")
            Next j
            file.WriteLine()
        Next i
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\NNL.csv")
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub lvData_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles lvData.ColumnClick
        If lvDataSortOrder = "Ascending" Then
            lvData.ListViewItemSorter = New ListViewItemComparer(e.Column, SortOrder.Ascending)
            lvDataSortOrder = "Descending"
        Else
            lvData.ListViewItemSorter = New ListViewItemComparer(e.Column, SortOrder.Descending)
            lvDataSortOrder = "Ascending"
        End If
    End Sub
End Class