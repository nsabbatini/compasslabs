﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmRebill
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub txtSpecNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSpecNum.KeyPress
        btnRebill.Enabled = False
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtSpecNum.Text = "" Then
            Exit Sub
        End If
        'Check for leading/trailing spaces
        txtSpecNum.Text = Trim(txtSpecNum.Text)
        'Uppercase all letters
        txtSpecNum.Text = UCase(txtSpecNum.Text)
        'Check to see if Specimen Number in use
        If Not CheckSpecNo(txtSpecNum.Text) Then
            MsgBox("Specimen number does not exists.")
            txtSpecNum.Text = ""
            txtSpecNum.Focus()
            Exit Sub
        End If
        'Get Info
        Dim RcvDate As String = "", RptDate As String = "", Acct As String = "", AcctName As String = "", PORD As String = "", POEFDT As String = ""
        Dim OC(100) As String, BillDate As String = "", BillFile As String = "", i As Integer = 0
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select substring(PSRCVDT,5,2)+'/'+substring(PSRCVDT,7,2)+'/'+substring(PSRCVDT,1,4),substring(PSRPTDT,5,2)+'/'+substring(PSRPTDT,7,2)+'/'+substring(PSRPTDT,1,4),PORD,POEFDT From PSPEC Where PSPECNO='" & txtSpecNum.Text & "' And PSACT=1"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            If Not IsDBNull(rs(0)) Then RcvDate = rs(0)
            If Not IsDBNull(rs(1)) Then RptDate = rs(1)
            If Not IsDBNull(rs(2)) Then PORD = rs(2)
            If Not IsDBNull(rs(3)) Then POEFDT = rs(3)
        End If
        rs.Close()
        Cmd.CommandText = "Select A.ACCT,ANAME From PORD A, ACCT B Where POACT=1 And PORD='" & PORD & "' And POEFDT='" & POEFDT & "' And A.ACCT=B.ACCT And A.AEFDT=B.AEFDT"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            Acct = rs(0) : AcctName = rs(1)
        End If
        rs.Close()
        Cmd.CommandText = "Select A.OC,ONAME,substring(PBILLDT,5,2)+'/'+substring(PBILLDT,7,2)+'/'+substring(PBILLDT,1,4),PBMISC From PBILL A, OC B Where PSPECNO='" & txtSpecNum.Text & "' And A.OC=B.OC And A.OEFDT=B.OEFDT"
        rs = Cmd.ExecuteReader
        i = 1
        ReDim OC(100)
        Do While rs.Read
            OC(i) = rs(0) & "^" & rs(1)
            BillDate = rs(2) : BillFile = rs(3)
            i = i + 1
        Loop
        rs.Close()
        lstInfo.Items.Clear()
        lstInfo.Items.Add("Specimen Number: " & txtSpecNum.Text)
        lstInfo.Items.Add("Account: " & Acct & " " & AcctName)
        If i = 1 Then
            lstInfo.Items.Add(" ")
            lstInfo.Items.Add("This specimen has not been billed yet.")
            lstInfo.Items.Add(" ")
        Else
            lstInfo.Items.Add("Receive Date: " & RcvDate & "     Report Date: " & RptDate)
            lstInfo.Items.Add("Bill Date: " & BillDate & "     Bill File: " & BillFile)
            lstInfo.Items.Add("-------------------------------------------------") : lstInfo.Items.Add("ORDERING CODES:") : lstInfo.Items.Add("-------------------------------------------------")
            For j = 1 To i
                If OC(j) <> "" Then lstInfo.Items.Add(CLSLIMS.Piece(OC(j), "^", 1) & "-" & CLSLIMS.Piece(OC(j), "^", 2))
            Next j
            btnRebill.Enabled = True
        End If
        Conn.Close()
    End Sub
    Public Function CheckSpecNo(ByVal specno As String) As Boolean
        Dim xSel As String = "Select PSPECNO From PSPEC Where PSACT=1 And PSPECNO='" & specno & "'"
        CheckSpecNo = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                CheckSpecNo = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

    Private Sub frmRebill_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-NEDPC" Then
            CLSLIMS.UID = "NED.SABBATINI"
        Else
            If CLSLIMS.UID = "" Then
                Login.ShowDialog()
            End If
            If CLSLIMS.UID = "" Or CLSLIMS.UID <> "MEL.ROBERTSON" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        btnRebill.Enabled = False
    End Sub

    Private Sub btnRebill_Click(sender As Object, e As EventArgs) Handles btnRebill.Click
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Insert Into QBILL Select A.PSPECNO,OC,OEFDT,'" & Format(Now, "yyyyMMdd") & "'+SUBSTRING(PSRPTDT,9,6) From PBILL A, PSPEC B Where A.PSPECNO=B.PSPECNO And PSACT=1 and A.PSPECNO ='" & txtSpecNum.Text & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Delete From PBILL Where PSPECNO ='" & txtSpecNum.Text & "'"
        Cmd.ExecuteNonQuery()
        txtSpecNum.Text = ""
        btnRebill.Enabled = False
        lstInfo.Items.Clear()
        txtSpecNum.Focus()
    End Sub
End Class