﻿Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmItemDef

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub


    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        Dim Upd As String = "", NeedsLot As Integer = 0
        If rbYes.Checked Then
            NeedsLot = 1
        Else
            NeedsLot = 0
        End If
        If txtMinQty.Text = "" Then
            txtMinQty.Text = 0
        End If
        If lblNew.Visible = False Then
            'Update
            Upd = "Update items set itemdesc='" & txtItemDesc.Text & "'," &
                                     "itemabbr='" & txtItmAbbr.Text & "'," &
                                     "itemunit='" & cbUnits.Text & "'," &
                                     "itemperunit='" & txtItmUnit.Text & "'," &
                                     "vendorid='" & cmbVndID.Text & "'," &
                                     "vendoritemid='" & txtVndItemID.Text & "'," &
                                     "itemvendorunit='" & cbVndUnits.Text & "'," &
                                     "itemvendorperunit='" & txtVndItmUnit.Text & "'," &
                                     "itemvendorcost='" & txtVndItmCost.Text & "'," &
                                     "minqty='" & txtMinQty.Text & "'," &
                                     "location='" & cbLoc.Text & "'," &
                                     "timetoreceive='" & txtTm2Recv.Text & "'," &
                                     "shippinginfo='" & txtShipInfo.Text & "'," &
                                     "dept='" & txtDept.Text & "'," &
                                     "needslotnum=" & NeedsLot & " Where itemid='" & cmbItemId.Text & "'"
        Else
            'Insert
            Upd = "Insert into items Values ('" & cmbItemId.Text & "','" & txtItemDesc.Text & "','" & txtItmAbbr.Text & "','" & cbUnits.Text & "','" & txtItmUnit.Text & "','" & cmbVndID.Text & "','" & txtVndItemID.Text & "','" &
                cbVndUnits.Text & "'," & Val(txtVndItmUnit.Text) & "," & Format(Val(txtVndItmCost.Text), "0.00") & ",'" & txtMinQty.Text & "',0,'" & cbLoc.Text & "','" & txtTm2Recv.Text & "','" & txtShipInfo.Text & "','" & txtDept.Text & "'," & NeedsLot & ")"
        End If
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand
        Cmd.CommandText = Upd
        Cmd.Connection = ConnMySQL
        Cmd.ExecuteNonQuery()
        ConnMySQL.Close()
        ClearFields()
    End Sub
    Public Sub ClearFields()
        lblNew.Visible = False
        cmbItemId.Text = ""
        txtItemDesc.Text = ""
        txtItmAbbr.Text = ""
        cbUnits.Text = ""
        txtItmUnit.Text = ""
        cmbVndID.Text = ""
        txtVndItemID.Text = ""
        cbVndUnits.Text = ""
        txtVndItmUnit.Text = ""
        txtVndItmCost.Text = ""
        txtMinQty.Text = ""
        cbLoc.Text = ""
        txtTm2Recv.Text = ""
        txtShipInfo.Text = ""
        txtDept.Text = ""
        rbNo.Checked = True
        cbLoc.Items.Clear()
        cmbItemId.Items.Clear()
        cmbVndID.Items.Clear()
        Dim Sel As String = "Select * from locations Where loccanbeparent=0"
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySQL
        Cmd.CommandText = Sel
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cbLoc.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select * from Items"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbItemId.Items.Add(rs(0))
        Loop
        rs.Close()
        Cmd.CommandText = "Select * from Vendors"
        rs = Cmd.ExecuteReader
        Do While rs.Read
            cmbVndID.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySQL.Close()
        cmbItemId.Focus()

    End Sub
    Private Sub frmItemDef_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "NED.SABBATINI" AndAlso Not CLSLIMS.UID = "REMOND.YOUNG" AndAlso Not CLSLIMS.UID = "WILLIAM.BUDD" AndAlso Not CLSLIMS.UID = "L.MCLAUGHLIN" Then
            Me.Close()
            Me.Dispose()
        End If
        ClearFields()
    End Sub

    Private Sub cmbItemId_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbItemId.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        Dim Sel As String = "Select * from items Where itemid='" & cmbItemId.Text & "'"
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySQL)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            txtItemDesc.Text = rs(1)
            txtItmAbbr.Text = rs(2)
            cbUnits.Text = rs(3)
            txtItmUnit.Text = rs(4)
            cmbVndID.Text = rs(5)
            txtVndItemID.Text = rs(6)
            cbVndUnits.Text = IIf(IsDBNull(rs(7)), "", rs(7))
            txtVndItmUnit.Text = IIf(IsDBNull(rs(8)), "", rs(8))
            txtVndItmCost.Text = IIf(IsDBNull(rs(9)), "", rs(9))
            txtMinQty.Text = rs(10)
            cbLoc.Text = rs(12)
            txtTm2Recv.Text = rs(13)
            txtShipInfo.Text = rs(14)
            txtDept.Text = rs(15)
            If rs(16) Then
                rbYes.Checked = True
            Else
                rbNo.Checked = False
            End If
        Else
            lblNew.Visible = True
            txtItemDesc.Focus()
        End If
        rs.Close()
        ConnMySQL.Close()
    End Sub
End Class