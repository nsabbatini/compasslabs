﻿Imports System.IO
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel

Public Class frmBldCovidXLS
    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim fn As System.IO.StreamReader = Nothing

        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim flnme As String = ""
        If Not File.Exists("MoleDir.ini") Then
            Dim SavFileI As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("MoleDir.ini", False)
            SavFileI.WriteLine("C:\")
            SavFileI.Close()
        End If
        Dim SavFileR As StreamReader = My.Computer.FileSystem.OpenTextFileReader("MoleDir.ini")
        fd.InitialDirectory = SavFileR.ReadLine
        SavFileR.Close()
        fd.Title = "Open COVID-19 File"
        fd.Filter = "Excel files (*.xlsx)|*.xlsx"
        fd.RestoreDirectory = True
        fd.Multiselect = False
        If fd.ShowDialog = DialogResult.OK Then
            txtFileName.Text = fd.FileName
        Else
            Exit Sub
        End If
        Dim j As Integer = 0
        For i = Len(txtFileName.Text) To 1 Step -1
            If Mid(txtFileName.Text, i, 1) = "\" Then
                j = i
                Exit For
            End If
        Next i
        Dim SavDir As String = Mid(flnme, 1, j)
        Dim SavFileW As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("MoleDir.ini", False)
        SavFileW.WriteLine(SavDir)
        SavFileW.Close()

    End Sub
    Public Sub BuildWrkLst()
        Dim c As Integer = 1, Spec(500) As String, wrklst As String = "COVID19"
        Dim newBatch As String, wrklstef As String, mbatch As String, befdt As String, InsPBI As String = ""
        Dim cnt As Integer = 1
        Dim XLApp As Excel.Application
        Dim XLWBS As Excel.Workbooks
        Dim XLWB As Excel.Workbook
        Dim XLWS As Excel.Worksheet
        XLApp = New Excel.Application
        XLApp.Visible = False
        XLWBS = XLApp.Workbooks
        XLWB = XLWBS.Open(txtFileName.Text)
        XLWS = XLWB.Sheets(1)

        For i = 1 To 500
            If IfSample(XLWS.Cells(i, 2).value) Then
                Spec(c) = XLWS.Cells(i, 2).Value
                c = c + 1
            End If
        Next i

        XLApp.DisplayAlerts = False
        XLWB.Close()
        XLWBS.Close()
        XLApp.DisplayAlerts = True
        XLApp.Quit()

        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim CmdIns As New SqlCommand
        CmdIns.Connection = Conn

        'Batch Effective date used on all batches
        befdt = Format(Now, "yyyyMMddhhmmss")
        'Get new batch number for worklist = YMMDDSEQ
        newBatch = GetNewBatchNum(wrklst)
        'Get current Worklist Effective Date for worklist
        wrklstef = WorkListEffDate(wrklst)
        'Get Sequential batch number for batch
        mbatch = GetMBATCH()
        ListBox1.Items.Add(wrklst & ": " & newBatch & ", " & mbatch)
        'List of samples for particular Worklist

        For i = 1 To 500
            If Spec(i) <> "" Then
                InsPBI = "Insert into PBI (WL, BATCH, BEFDT, PBIx, PBITYPE, PSPECNO, PCONT, QCSPECNO, QCID, PBIRTXT) " &
                         "Values ('" & wrklst & "','" & newBatch & "','" & befdt & "'," & cnt & ",1,'" & Spec(i) & "','01',null,null,null)"
                CmdIns.CommandText = InsPBI
                CmdIns.ExecuteNonQuery()
                CmdIns.CommandText = "Delete From QWL Where PSPECNO='" & Spec(i) & "' And WL='" & wrklst & "'"
                CmdIns.ExecuteNonQuery()
                cnt = cnt + 1
            End If
        Next i
        If cnt > 1 Then
            Dim insPB As String = "Insert into PB (WL, BATCH, BEFDT, BACT, PBUID, WEFDT, PBNOS, PBBQCRUN, PBEQCRUN, MBATCH, PBCLOSE, PBCLOSEID, PBCLOSEDT) " &
                  "Values ('" & wrklst & "','" & newBatch & "','" & befdt & "',1,'" & CLSLIMS.UID & "','" & wrklstef & "'," & cnt - 1 & ",null,null," & mbatch & ",0,null,null)"
            CmdIns.CommandText = insPB
            CmdIns.ExecuteNonQuery()
        End If
        Me.Close()
        Me.Dispose()
    End Sub
    Private Function IfSample(ByRef samp As String) As Boolean
        IfSample = False
        If samp = "" Then
            Exit Function
        End If
        Dim Sel As String = "Select pspecno From pspec Where psact=1 and PSPECNO = '" & samp & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            If rs.Read() Then
                IfSample = True
            End If
            rs.Close()
            Cmd.Dispose()
            Conn.Close()
            Conn.Dispose()
        End Using

    End Function
    Private Function Eligible(wrk As String, wrkef As String, specno As String) As Boolean
        Eligible = False
        Dim sel As String = "Select count(*) From qwl Where wl='" & wrk & "' and wefdt='" & wrkef & "' and pspecno='" & specno & "'"
        Dim ConnE As New SqlConnection(CLSLIMS.strCon)
        Dim CmdE As New SqlCommand(sel, ConnE)
        Dim rse As SqlDataReader = CmdE.ExecuteReader
        If rse.Read() Then
            If rse(0) > 0 Then
                Eligible = True
            End If
        End If
        rse.Close()
        ConnE.Close()
        CmdE.Dispose()
    End Function
    Public Function GetNewBatchNum(ByRef wrklst As String) As String
        GetNewBatchNum = ""
        Dim preBatch = Mid(Format(Now, "yyyy"), 4, 1) & Format(Now, "MMdd")
        Dim sel As String = "Select Max(BATCH) From PB Where Substring(BATCH,1,5)='" & preBatch & "' and wl='" & wrklst & "'"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        rsf.Read()
        If IsDBNull(rsf(0)) Then
            GetNewBatchNum = preBatch & "100"
        Else
            GetNewBatchNum = preBatch & (Mid(rsf(0), Len(rsf(0)) - 2, 3) + 1)
        End If
        rsf.Close()
        ConnF.Close()
    End Function
    Public Function WorkListEffDate(ByRef wrklst As String) As String
        WorkListEffDate = ""
        Dim sel As String = "Select MAX(wefdt) From WL Where WL='" & wrklst & "'"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        rsf.Read()
        WorkListEffDate = rsf(0)
        rsf.Close()
        ConnF.Close()
    End Function
    Public Function GetMBATCH() As String
        GetMBATCH = ""
        Dim sel As String = "Select SYSVALVAL + 1 From SYSVAL Where SYSVALKEY='MBATCH'"
        Dim upd As String = "Update SYSVAL Set SYSVALVAL = SYSVALVAL + 1 Where SYSVALKEY='MBATCH'"
        Dim ConnF As New SqlConnection(CLSLIMS.strCon)
        ConnF.Open()
        Dim CmdF As New SqlCommand(sel, ConnF)
        Dim rsf As SqlDataReader = CmdF.ExecuteReader
        rsf.Read()
        GetMBATCH = rsf(0)
        rsf.Close()
        Dim CmdUpd As New SqlCommand
        CmdUpd.Connection = ConnF
        CmdUpd.CommandText = upd
        CmdUpd.ExecuteNonQuery()
        ConnF.Close()
    End Function

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnBuild_Click(sender As Object, e As EventArgs) Handles btnBuild.Click
        BuildWrkLst()
    End Sub
End Class