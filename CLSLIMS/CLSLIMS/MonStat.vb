﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports CLSLIMS.BadFlag

Public Class MonStat

    Private Sub txtMonYY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartDt.KeyPress

        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtStartDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtStartDt.Text) Then
                MsgBox("Invalid Start Date.")
                txtEndDt.Focus()
            End If
        End If
    End Sub
    Private Sub txtEndDt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndDt.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtEndDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtEndDt.Text) Then
                MsgBox("Invalid End Date.")
                txtEndDt.Focus()
            End If
        End If
    End Sub

    Private Sub btnRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRpt.Click

        Dim dFirst As Date, dLast As Date, arrCmt(50) As String, sFirst As String, sLast As String, x As String = ""

        lstMonStat.Items.Clear()
        lstXLS.Items.Clear()
        dFirst = CDate(txtStartDt.Text) : dLast = CDate(txtEndDt.Text)
        sFirst = DatePart(DateInterval.Year, dFirst) & IIf(DatePart(DateInterval.Month, dFirst) < 10, "0", "") & DatePart(DateInterval.Month, dFirst) & IIf(DatePart(DateInterval.Day, dFirst) < 10, "0", "") & DatePart(DateInterval.Day, dFirst)
        sLast = DatePart(DateInterval.Year, dLast) & IIf(DatePart(DateInterval.Month, dLast) < 10, "0", "") & DatePart(DateInterval.Month, dLast) & IIf(DatePart(DateInterval.Day, dLast) < 10, "0", "") & DatePart(DateInterval.Day, dLast)
        Dim Sel As String
        Sel = "select distinct PSSTAT.PSPECNO,substring(PSPEC.PSRCVDT,5,2)+'/'+substring(PSPEC.PSRCVDT,7,2)+'/'+substring(PSPEC.PSRCVDT,1,4),PSSTAT.STATCD,ACCT.ACCT,ANAME " & _
                            "from PSSTAT, PSPEC, ACCT, PORD " & _
                            "where PSSTAT.PSPECNO = PSPEC.PSPECNO " & _
                            "and PSSTAT.ADSQ1=PSPEC.ADSQ1 and PSPEC.PSACT!=1 " & _
                            "and substring(pspec.psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "' " & _
                            "and PSPEC.PORD=PORD.PORD AND PSPEC.POEFDT=PORD.POEFDT " & _
                            "AND ACCT.ACCT=PORD.ACCT AND ACCT.AEFDT=PORD.AEFDT "

        If UCase(txtAcct.Text) <> "ALL" Then
            Sel = Sel & "And ACCT.ACCT='" & txtAcct.Text & "' "
        End If
        Sel = Sel & "Order by ACCT.ACCT"

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                lstMonStat.Items.Add(rs(0) & Space(8 - Len(rs(0))) & rs(1) & Space(14 - Len(rs(1))) & rs(2) & Space(15 - Len(rs(2))) & Mid(rs(3), 1, 6) & Space(8 - Len(Mid(rs(3), 1, 6))) & rs(4))
                x = Replace(rs(0), ",", " ") & "," & Replace(rs(1), ",", " ") & "," & Replace(rs(2), ",", " ") & "," & Replace(rs(3), ",", " ") & "," & Replace(rs(4), ",", " ") & ","
                ReDim arrCmt(50)
                arrCmt = BadFlag.GetComments(rs(0))
                If arrCmt(0) <> "" Then
                    For i = 0 To 4
                        If arrCmt(i) <> "" Then
                            lstMonStat.Items.Add(rs(0) & Space(5) & arrCmt(i))
                            x = x & Chr(34) & Replace(arrCmt(i), ",", " ") & Chr(34) & ","
                        End If
                    Next i
                End If
                lstMonStat.Items.Add("-----------------------------------------------------------------------------")
                lstXLS.Items.Add(x)
            Loop
            rs.Close()
            Conn.Close()
        End Using

    End Sub


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub MonStat_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        txtStartDt.Focus()
    End Sub

    Private Sub MonStat_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtStartDt.Text = ""
        txtEndDt.Text = ""
        lstMonStat.Items.Clear()
        lstXLS.Items.Clear()
        txtStartDt.Focus()
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click

        Dim i As Integer
        If System.IO.File.Exists("C:\CLSLIMS\MonStat.txt") Then
            System.IO.File.Delete("C:\CLSLIMS\MonStat.txt")
        End If
        Dim flMonStat As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\MonStat.txt", False)
        For i = 0 To lstMonStat.Items.Count - 1
            flMonStat.WriteLine(lstMonStat.Items(i))
        Next i
        flMonStat.Close()
        Dim procID As Integer

        procID = Shell("C:\Windows\system32\notepad.exe c:\clslims\Monstat.txt", AppWinStyle.NormalFocus)

        SendKeys.SendWait("^(p)")
        Threading.Thread.Sleep(2000)
        SendKeys.Send("~")
        Threading.Thread.Sleep(2000)
        SendKeys.Send("%(f)")
        Threading.Thread.Sleep(1000)
        SendKeys.Send("x")
        Me.Close()
    End Sub

    Private Sub btnXLS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnXLS.Click
        Dim AppPath As String = ""
        Dim dialog As New FolderBrowserDialog()
        dialog.RootFolder = Environment.SpecialFolder.Desktop
        dialog.SelectedPath = "C:\"
        dialog.Description = "Select Application Configuration Files Path"
        If dialog.ShowDialog() = DialogResult.OK Then
            AppPath = dialog.SelectedPath
        End If
        If System.IO.File.Exists(AppPath & "\AffidavitRpt.csv") Then
            System.IO.File.Delete(AppPath & "\AffidavitRpt.csv")
        End If
        Dim file As System.IO.StreamWriter
        file = My.Computer.FileSystem.OpenTextFileWriter(AppPath & "\AffidavitRpt.csv", True)
        file.WriteLine("Specimen Number,Date,Flag,Acct Number,Acct Name,Comment One,Comment Two,Comment Three, Comment Four, Comment Five")
        For i = 0 To lstXLS.Items.Count - 1
            file.WriteLine(lstXLS.Items(i))
        Next i
        file.Close()
        Me.Close()
    End Sub


End Class