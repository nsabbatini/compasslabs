﻿Public Class frmSOEGrid
    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        ClearCLick()
    End Sub
    Public Sub ClearCLick()
        cbOralAmp.Checked = False
        cbOralAlk.Checked = False
        cbOralBenz.Checked = False
        cbOralBup.Checked = False
        cbOralCOC.Checked = False
        cbOralFent.Checked = False
        cbOralHero.Checked = False
        cbOralMethAmp.Checked = False
        cbOralMethy.Checked = False
        cbOralMtd.Checked = False
        cbOralOpi.Checked = False
        cbOralOpiAnal.Checked = False
        cbOralOxy.Checked = False
        cbOralPhen.Checked = False
        cbOralPro.Checked = False
        cbOralSkel.Checked = False
        cbOralTap.Checked = False
        cbOralTHC.Checked = False
        cbOralTram.Checked = False
        cbOralGaba.Checked = False
        cbOralPregaba.Checked = False
        cbOralSedHyp.Checked = False

    End Sub

    Private Sub btnOrd_Click(sender As Object, e As EventArgs) Handles btnOrd.Click
        QuickOE.lbOC.Items.Clear()

        If cbOralAlk.Checked Then
            QuickOE.lbOC.Items.Add("S349000" & " - " & QuickOE.GetOCAbbr("S349000"))
        End If
        If cbOralAmp.Checked Then
            QuickOE.lbOC.Items.Add("S352000" & " - " & QuickOE.GetOCAbbr("S352000"))
        End If
        If cbOralADSer.Checked Then
            QuickOE.lbOC.Items.Add("S354000" & " - " & QuickOE.GetOCAbbr("S354000"))
        End If
        If cbOralAntiTri.Checked Then
            QuickOE.lbOC.Items.Add("S355000" & " - " & QuickOE.GetOCAbbr("S355000"))
        End If
        If cbOralAnti.Checked Then
            QuickOE.lbOC.Items.Add("S356000" & " - " & QuickOE.GetOCAbbr("S356000"))
        End If
        If cbOralAntiEpi.Checked Then
            QuickOE.lbOC.Items.Add("S357000" & " - " & QuickOE.GetOCAbbr("S357000"))
        End If
        If cbOralAntiPsych.Checked Then
            QuickOE.lbOC.Items.Add("S358000" & " - " & QuickOE.GetOCAbbr("S358000"))
        End If
        If cbOralBenz.Checked Then
            QuickOE.lbOC.Items.Add("S361000" & " - " & QuickOE.GetOCAbbr("S361000"))
        End If
        If cbOralBup.Checked Then
            QuickOE.lbOC.Items.Add("S363000" & " - " & QuickOE.GetOCAbbr("S363000"))
        End If
        If cbOralTHC.Checked Then
            QuickOE.lbOC.Items.Add("S365000" & " - " & QuickOE.GetOCAbbr("S365000"))
        End If
        If cbOralCOC.Checked Then
            QuickOE.lbOC.Items.Add("S306020" & " - " & QuickOE.GetOCAbbr("S306020"))
        End If
        If cbOralFent.Checked Then
            QuickOE.lbOC.Items.Add("S369000" & " - " & QuickOE.GetOCAbbr("S369000"))
        End If
        If cbOralGaba.Checked Then
            QuickOE.lbOC.Items.Add("S370000" & " - " & QuickOE.GetOCAbbr("S370000"))
        End If
        If cbOralHero.Checked Then
            QuickOE.lbOC.Items.Add("S371000" & " - " & QuickOE.GetOCAbbr("S371000"))
        End If
        If cbOralMethAmp.Checked Then
            QuickOE.lbOC.Items.Add("S375000" & " - " & QuickOE.GetOCAbbr("S375000"))
        End If
        If cbOralMethy.Checked Then
            QuickOE.lbOC.Items.Add("S377000" & " - " & QuickOE.GetOCAbbr("S377000"))
        End If
        If cbOralMtd.Checked Then
            QuickOE.lbOC.Items.Add("S305040" & " - " & QuickOE.GetOCAbbr("S305040"))
        End If
        If cbOralOpi.Checked Then
            QuickOE.lbOC.Items.Add("S380000" & " - " & QuickOE.GetOCAbbr("S380000"))
        End If
        If cbOralOpiAnal.Checked Then
            QuickOE.lbOC.Items.Add("S382000" & " - " & QuickOE.GetOCAbbr("S382000"))
        End If
        If cbOralOxy.Checked Then
            QuickOE.lbOC.Items.Add("S383000" & " - " & QuickOE.GetOCAbbr("S383000"))
        End If
        If cbOralPhen.Checked Then
            QuickOE.lbOC.Items.Add("S385000" & " - " & QuickOE.GetOCAbbr("S385000"))
        End If
        If cbOralPregaba.Checked Then
            QuickOE.lbOC.Items.Add("S387000" & " - " & QuickOE.GetOCAbbr("S387000"))
        End If
        If cbOralPro.Checked Then
            QuickOE.lbOC.Items.Add("S388000" & " - " & QuickOE.GetOCAbbr("S388000"))
        End If
        If cbOralSedHyp.Checked Then
            QuickOE.lbOC.Items.Add("S390000" & " - " & QuickOE.GetOCAbbr("S390000"))
        End If
        If cbOralSkel.Checked Then
            QuickOE.lbOC.Items.Add("S391000" & " - " & QuickOE.GetOCAbbr("S391000"))
        End If
        If cbOralTap.Checked Then
            QuickOE.lbOC.Items.Add("S393000" & " - " & QuickOE.GetOCAbbr("S393000"))
        End If
        If cbOralTram.Checked Then
            QuickOE.lbOC.Items.Add("S305120" & " - " & QuickOE.GetOCAbbr("S305120"))
        End If
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmSOEGrid_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For i = 0 To QuickOE.lbOC.Items.Count - 1
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S352000" Then cbOralAmp.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S361000" Then cbOralBenz.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S363000" Then cbOralBup.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S306020" Then cbOralCOC.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S369000" Then cbOralFent.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S371000" Then cbOralHero.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S375000" Then cbOralMethAmp.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S377000" Then cbOralMethy.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S305040" Then cbOralMtd.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S380000" Then cbOralOpi.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S382000" Then cbOralOpiAnal.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S383000" Then cbOralOxy.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S385000" Then cbOralPhen.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S388000" Then cbOralPro.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S391000" Then cbOralSkel.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S393000" Then cbOralTap.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S365000" Then cbOralTHC.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S305120" Then cbOralTram.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S349000" Then cbOralAlk.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S354000" Then cbOralADSer.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S355000" Then cbOralAntiTri.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S356000" Then cbOralAnti.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S357000" Then cbOralAntiEpi.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S358000" Then cbOralAntiPsych.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S370000" Then cbOralGaba.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S387000" Then cbOralPregaba.Checked = True
            If UCase(CLSLIMS.Piece(QuickOE.lbOC.Items(i), " ", 1)) = "S390000" Then cbOralSedHyp.Checked = True
        Next i
    End Sub
End Class