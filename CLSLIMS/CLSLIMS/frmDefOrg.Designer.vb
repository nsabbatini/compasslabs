﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDefOrg
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblNew = New System.Windows.Forms.Label()
        Me.cmbOrg = New System.Windows.Forms.ComboBox()
        Me.cmbCPT = New System.Windows.Forms.ComboBox()
        Me.cmbAbbr = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblNew
        '
        Me.lblNew.AutoSize = True
        Me.lblNew.ForeColor = System.Drawing.Color.Red
        Me.lblNew.Location = New System.Drawing.Point(466, 41)
        Me.lblNew.Name = "lblNew"
        Me.lblNew.Size = New System.Drawing.Size(47, 19)
        Me.lblNew.TabIndex = 22
        Me.lblNew.Text = "NEW"
        '
        'cmbOrg
        '
        Me.cmbOrg.FormattingEnabled = True
        Me.cmbOrg.Location = New System.Drawing.Point(157, 36)
        Me.cmbOrg.Name = "cmbOrg"
        Me.cmbOrg.Size = New System.Drawing.Size(302, 27)
        Me.cmbOrg.TabIndex = 17
        '
        'cmbCPT
        '
        Me.cmbCPT.FormattingEnabled = True
        Me.cmbCPT.Location = New System.Drawing.Point(157, 109)
        Me.cmbCPT.Name = "cmbCPT"
        Me.cmbCPT.Size = New System.Drawing.Size(140, 27)
        Me.cmbCPT.TabIndex = 21
        '
        'cmbAbbr
        '
        Me.cmbAbbr.FormattingEnabled = True
        Me.cmbAbbr.Location = New System.Drawing.Point(158, 71)
        Me.cmbAbbr.Name = "cmbAbbr"
        Me.cmbAbbr.Size = New System.Drawing.Size(139, 27)
        Me.cmbAbbr.TabIndex = 19
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(90, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 19)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "CPT:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 19)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Abbreviation:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(46, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(92, 19)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Organism:"
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(78, 177)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(76, 33)
        Me.btnNew.TabIndex = 25
        Me.btnNew.Text = "New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(194, 177)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(76, 33)
        Me.btnSave.TabIndex = 23
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(315, 177)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(76, 33)
        Me.btnExit.TabIndex = 24
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'frmDefOrg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 249)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblNew)
        Me.Controls.Add(Me.cmbOrg)
        Me.Controls.Add(Me.cmbCPT)
        Me.Controls.Add(Me.cmbAbbr)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmDefOrg"
        Me.Text = "Define Organism"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblNew As Label
    Friend WithEvents cmbOrg As ComboBox
    Friend WithEvents cmbCPT As ComboBox
    Friend WithEvents cmbAbbr As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnNew As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
End Class
