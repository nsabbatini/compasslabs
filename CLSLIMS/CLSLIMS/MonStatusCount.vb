﻿
Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports CLSLIMS.BadFlag


Public Class MonStatusCount
    Dim reporttype As String

    Private Sub txtMonYY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartDt.KeyPress

        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtStartDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtStartDt.Text) Then
                MsgBox("Invalid Start Date.")
                txtEndDt.Focus()
            End If
        End If
    End Sub
    Private Sub txtEndDt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndDt.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtEndDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtEndDt.Text) Then
                MsgBox("Invalid End Date.")
                txtEndDt.Focus()
            End If
        End If
    End Sub

    Private Sub btnRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnrpt.Click

        Dim dFirst As Date, dLast As Date, sFirst As String, sLast As String
        If txtStartDt.Text = "" Or txtEndDt.Text = "" Then
            MsgBox("Enter Date Range")
            Exit Sub
        End If

        If cbSummary.Checked = False And cbAcctSpecific.Checked = False And cbCodeSpecific.Checked = False Then
            MsgBox("Choose a report type")
            Exit Sub
        End If

        lstMonStatCnt.Items.Clear()
        lstXLS.Items.Clear()
        Try

            dFirst = CDate(txtStartDt.Text)
            dLast = CDate(txtEndDt.Text)
            sFirst = DatePart(DateInterval.Year, dFirst) & IIf(DatePart(DateInterval.Month, dFirst) < 10, "0", "") & DatePart(DateInterval.Month, dFirst) & IIf(DatePart(DateInterval.Day, dFirst) < 10, "0", "") & DatePart(DateInterval.Day, dFirst)
            sLast = DatePart(DateInterval.Year, dLast) & IIf(DatePart(DateInterval.Month, dLast) < 10, "0", "") & DatePart(DateInterval.Month, dLast) & IIf(DatePart(DateInterval.Day, dLast) < 10, "0", "") & DatePart(DateInterval.Day, dLast)
            If cbDetail.Checked = False Then
                Call PrintWithoutDetail(sFirst, sLast, dFirst, dLast)
            Else
                Call PrintWithDetail(sFirst, sLast, dFirst, dLast)
            End If

        Catch ex As Exception
            MsgBox("Invalid Date")
            Exit Sub
        End Try
    End Sub
    Sub PrintWithoutDetail(sFirst, sLast, dFirst, dLast)
        Dim Sel As String = "", x As String = ""

        If cbAcctSpecific.Checked = True Then
            reporttype = "_Acct"
            Sel = "Select  po.acct, a.aname, s.statcd, count(distinct p.pspecno), cd.stcddesc from pspec p  "
        Else
            If cbCodeSpecific.Checked = True Then
                reporttype = "_Status"
                Sel = "Select  s.statcd, po.acct, a.aname, count(distinct p.pspecno), cd.stcddesc from pspec p  "
            Else
                If cbSummary.Checked = True Then
                    reporttype = "_Default"
                    Sel = "Select  s.statcd, count(distinct p.pspecno), cd.stcddesc, po.acct, a.aname from pspec p "
                End If
            End If
        End If

        Sel = Sel & "Left Join psstat s On p.pspecno = s.pspecno And p.adsq1 = s.adsq1 "
        Sel = Sel & " Left Join pdem   d   On p.puid = d.puid And p.pefdt = d.pefdt "
        Sel = Sel & " Left Join pord   po  On po.pord = p.pord And po.poefdt = p.poefdt "
        Sel = Sel & " Left Join acct   a  On a.acct = po.acct And a.aefdt = po.aefdt "
        Sel = Sel & " Left Join statcd cd On s.statcd = cd.statcd "
        Sel = Sel & " where p.psrcvdt between " & "'" & sFirst & "' And " & "'" & sLast & "'"
        Sel = Sel & " And cd.stcdact = 1  "
        If cbAcctSpecific.Checked = True Then
            If UCase(txtAcct.Text) <> "ALL" Then
                Sel = Sel & "And a.acct='" & txtAcct.Text & "' "
            End If

            Sel = Sel & " group by po.acct, a.aname, s.statcd, cd.stcddesc "
            Sel = Sel & " HAVING COUNT(s.statcd) > 0 "
            Sel = Sel & " order by po.acct, s.statcd "
        Else
            If cbCodeSpecific.Checked = True Then
                If UCase(txtCode.Text) <> "ALL" Then
                    Sel = Sel & "And s.statcd='" & txtCode.Text & "' "
                End If
                Sel = Sel & " group by s.statcd, cd.stcddesc, po.acct, a.aname  "
                Sel = Sel & " HAVING COUNT(s.statcd) > 0 "
                Sel = Sel & " order by s.statcd, po.acct "
            Else
                If cbSummary.Checked = True Then
                    If UCase(txtSumCode.Text) <> "ALL" Then
                        Sel = Sel & "And s.statcd='" & txtSumCode.Text & "' "
                    End If
                    If UCase(txtSumAcct.Text) <> "ALL" Then
                        Sel = Sel & "And a.acct='" & txtSumAcct.Text & "' "
                    End If

                    Sel = Sel & " group by s.statcd, cd.stcddesc, po.acct, a.aname "
                    Sel = Sel & " HAVING COUNT(s.statcd) > 0 "
                    Sel = Sel & " order by s.statcd "
                End If
            End If
        End If

        Try
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                lstMonStatCnt.Items.Add((dFirst).ToString & " TO ")
                lstMonStatCnt.Items.Add((dLast).ToString)

                If cbAcctSpecific.Checked = True Then
                    lstMonStatCnt.Items.Add(LJ("Acct", 15).ToString & " " & LJ("Name", 55).ToString & " " & " " & LJ("Code", 15).ToString & " " & LJ("Count", 15).ToString & " " & LJ("Code Description", 15).ToString)
                    x = "Acct" & "," & "Name" & "," & "Code" & "," & "Count" & "," & "Code Description"
                Else
                    If cbCodeSpecific.Checked = True Then
                        lstMonStatCnt.Items.Add(LJ("Code", 15).ToString & " " & LJ("Acct", 15).ToString & " " & LJ("Acct Description", 55).ToString & " " & LJ("Count", 15).ToString & " " & LJ("Code Description", 15).ToString) ''''''''
                        x = "Code" & "," & "Acct" & "," & "Acct Description" & "," & "Count" & "," & "Code Description"
                    Else
                        If cbSummary.Checked = True Then
                            If txtSumAcct.Text <> "ALL" Then
                                lstMonStatCnt.Items.Add(LJ("Code", 15).ToString & " " & LJ("Count", 15).ToString & " " & LJ("Code Description", 55).ToString & LJ("Acct", 15).ToString & " " & LJ("Name", 55).ToString)
                                x = "Code" & "," & "Count" & "," & "Code Description" & "," & "Acct" & "," & "Acct Name"
                            Else
                                lstMonStatCnt.Items.Add(LJ("Code", 15).ToString & " " & LJ("Count", 15).ToString & " " & LJ("Code Description", 55).ToString & LJ("Acct", 15).ToString & " " & LJ("Name", 55).ToString)
                                x = "Code" & "," & "Count" & "," & "Code Description" & "," & "Acct" & "," & "Acct Name"
                            End If
                        End If
                    End If

                End If
                lstXLS.Items.Add(x)

                Conn.Open()
                Dim Cmd As New SqlCommand(Sel, Conn)
                Dim rs As SqlDataReader = Cmd.ExecuteReader
                Do While rs.Read()
                    If cbAcctSpecific.Checked = True Then
                        Dim acctname As String = IIf(IsDBNull(rs(1)), "", rs(1))
                        acctname = Replace(acctname, ",", " ")
                        Dim codename As String = Replace(rs(4), ",", " ")
                        lstMonStatCnt.Items.Add(LJ(rs(0), 15).ToString & " " & LJ(acctname, 55).ToString & " " & LJ(rs(2), 15).ToString & " " & LJ(rs(3), 15).ToString & " " & LJ(rs(4), 15).ToString)
                        x = rs(0) & "," & acctname & "," & rs(2) & "," & rs(3) & "," & codename
                    Else
                        If cbCodeSpecific.Checked = True Then
                            Dim acctname As String = IIf(IsDBNull(rs(2)), "", rs(2))
                            acctname = Replace(acctname, ",", " ")
                            Dim codename As String = Replace(rs(4), ",", " ")
                            lstMonStatCnt.Items.Add(LJ(rs(0), 15).ToString & " " & LJ(rs(1), 15).ToString & " " & LJ(acctname, 55).ToString & " " & LJ(rs(3), 15).ToString & " " & LJ(rs(4), 15).ToString)
                            x = rs(0) & "," & rs(1) & "," & acctname & "," & rs(3) & "," & rs(4)
                        Else
                            Dim acctname As String = IIf(IsDBNull(rs(4)), "", rs(4))
                            acctname = Replace(acctname, ",", " ")
                            Dim codename As String = Replace(rs(2), ",", " ")
                            lstMonStatCnt.Items.Add(LJ(rs(0), 15).ToString & " " & LJ(rs(1), 15).ToString & " " & LJ(rs(2), 55).ToString & " " & LJ(rs(3), 15).ToString & " " & LJ(acctname, 15).ToString)
                            x = rs(0) & "," & rs(1) & "," & codename & "," & rs(3) & "," & acctname
                        End If
                    End If

                    lstXLS.Items.Add(x)
                Loop
                rs.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error getting table information from Non-detail section")
        End Try

    End Sub
    Sub PrintWithDetail(sFirst, sLast, dFirst, dLast)
        Dim Sel As String = "", x As String = ""

        If cbAcctSpecific.Checked = True Then
            reporttype = "_Acct_Detail"
            Sel = "Select distinct a.acct, a.aname, s.statcd,  cd.stcddesc, p.pspecno, p.psrcvdt from pspec p "
        Else
            If cbCodeSpecific.Checked = True Then
                reporttype = "_Code_Detail"
                Sel = "Select distinct s.statcd,  cd.stcddesc, a.acct, a.aname,  p.pspecno, p.psrcvdt from pspec p "
            Else
                If cbSummary.Checked = True Then
                    reporttype = "_Default"
                    Sel = "Select  s.statcd, count(distinct p.pspecno), cd.stcddesc from pspec p "
                End If
            End If

        End If

        Sel = Sel & "Left Join psstat s On p.pspecno = s.pspecno And p.adsq1 = s.adsq1 "
        Sel = Sel & " Left Join pdem   d   On p.puid = d.puid And p.pefdt = d.pefdt "
        Sel = Sel & " Left Join pord   po  On po.pord = p.pord And po.poefdt = p.poefdt "
        Sel = Sel & " Left Join acct   a  On a.acct = po.acct And a.aefdt = po.aefdt "
        Sel = Sel & " Left Join statcd cd On s.statcd = cd.statcd "
        Sel = Sel & " where p.psrcvdt between " & "'" & sFirst & "' And " & "'" & sLast & "'"
        Sel = Sel & " And cd.stcdact = 1   "
        If cbAcctSpecific.Checked = True Then
            If UCase(txtAcct.Text) <> "ALL" Then
                Sel = Sel & "And a.acct='" & txtAcct.Text & "' "
            End If
            Sel = Sel & " order by a.acct, s.statcd "
        End If
        If cbCodeSpecific.Checked = True Then
            If UCase(txtCode.Text) <> "ALL" Then
                Sel = Sel & "And s.statcd='" & txtCode.Text & "' "
            End If
            Sel = Sel & " order by s.statcd,  a.acct "
        End If

        Try
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                lstMonStatCnt.Items.Add((dFirst).ToString & " TO ")
                lstMonStatCnt.Items.Add((dLast).ToString)

                If cbAcctSpecific.Checked = True Then
                    lstMonStatCnt.Items.Add(LJ("Acct", 10).ToString & " " & LJ("Acct Name", 55).ToString & " " & LJ("Code", 15).ToString & " " & LJ("Code Description", 55).ToString & " " & LJ("Spec No", 15).ToString & " " & LJ("Recv Date", 15).ToString)
                    x = "Acct" & "," & "Acct Name" & "," & "Code" & "," & "Code Description" & "," & "Spec No" & "," & "Recv Date"
                Else
                    lstMonStatCnt.Items.Add(LJ("Code", 10).ToString & " " & LJ("Code Description", 55).ToString & " " & LJ("Acct", 10).ToString & " " & LJ("Acct Name", 55).ToString & " " & LJ("Spec No", 15).ToString & " " & LJ("Recv Date", 15).ToString)
                    x = "Code" & "," & "Code Description" & "," & "Acct" & "," & "Acct Name" & "," & "Spec No" & "," & "Recv Date"
                End If
                lstXLS.Items.Add(x)
                Conn.Open()
                Dim Cmd As New SqlCommand(Sel, Conn)
                Dim rs As SqlDataReader = Cmd.ExecuteReader
                Do While rs.Read()
                    If cbAcctSpecific.Checked = True Then
                        Dim strOrigDate As String = rs(5)
                        Dim myCIintl As New Globalization.CultureInfo("en-US", False)
                        Dim dtNewDate As DateTime = DateTime.ParseExact(strOrigDate, "yyyyMMddHHmmss", myCIintl)
                        Dim strNewDate As String = Format(dtNewDate, "MM/dd/yyyy") 'Format(dtNewDate, "MM/dd/yyyy hh:mm:ss tt")
                        Dim acctnum As String = IIf(IsDBNull(rs(0)), "", rs(0))
                        acctnum = Replace(acctnum, ",", " ")
                        Dim acctname As String = IIf(IsDBNull(rs(1)), "", rs(1))
                        acctname = Replace(acctname, ",", " ")
                        Dim codename As String = Replace(rs(3), ",", " ")
                        lstMonStatCnt.Items.Add(LJ(acctnum, 10).ToString & " " & LJ(acctname, 55).ToString & " " & LJ(rs(2), 15).ToString & " " & LJ(rs(3), 55).ToString & " " & LJ(rs(4), 15).ToString & " " & LJ(strNewDate, 15).ToString)
                        x = rs(0) & "," & acctname & "," & rs(2) & "," & codename & "," & rs(4) & "," & strNewDate
                    Else
                        Dim strOrigDate As String = rs(5)
                        Dim myCIintl As New Globalization.CultureInfo("en-US", False)
                        Dim dtNewDate As DateTime = DateTime.ParseExact(strOrigDate, "yyyyMMddHHmmss", myCIintl)
                        Dim strNewDate As String = Format(dtNewDate, "MM/dd/yyyy") 'Format(dtNewDate, "MM/dd/yyyy hh:mm:ss tt")
                        Dim acctnum As String = IIf(IsDBNull(rs(2)), "", rs(2))
                        acctnum = Replace(acctnum, ",", " ")
                        Dim acctname As String = IIf(IsDBNull(rs(3)), "", rs(3))
                        acctname = Replace(acctname, ",", " ")
                        Dim codename As String = Replace(rs(1), ",", " ")
                        lstMonStatCnt.Items.Add(LJ(rs(0), 10).ToString & " " & LJ(rs(1), 55).ToString & " " & (LJ(acctnum, 10).ToString & " " & LJ(acctname, 55).ToString & " " & LJ(rs(4), 15).ToString & " " & LJ(strNewDate, 15).ToString))
                        x = rs(0) & "," & codename & "," & rs(2) & "," & acctname & "," & rs(4) & "," & strNewDate
                    End If
                    lstXLS.Items.Add(x)
                Loop
                rs.Close()
                Conn.Close()
            End Using
        Catch ex As Exception
            MsgBox("Error getting table information from Detail section")
        End Try

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub MonStatCount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtStartDt.Text = ""
        txtEndDt.Text = ""
        lstMonStatCnt.Items.Clear()
        lstXLS.Items.Clear()
        txtStartDt.Focus()
        txtAcct.Hide()
        lblAccount.Hide()
        txtCode.Hide()
        lblCode.Hide()
        txtSumCode.Hide()
        lblSumCode.Hide()
        txtSumAcct.Hide()
        lblSumAcct.Hide()
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click

        Dim i As Integer
        If System.IO.File.Exists("C:\CLSLIMS\MonStatCount.txt") Then
            System.IO.File.Delete("C:\CLSLIMS\MonStatCount.txt")
        End If
        Dim flMonStat As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\MonStatCount.txt", False)
        For i = 0 To lstMonStatCnt.Items.Count - 1
            flMonStat.WriteLine(lstMonStatCnt.Items(i))
        Next i
        flMonStat.Close()
        Dim procID As Integer

        procID = Shell("C:\Windows\system32\notepad.exe c:\clslims\MonstatCount.txt", AppWinStyle.NormalFocus)

        SendKeys.SendWait("^(p)")
        Threading.Thread.Sleep(2000)
        SendKeys.Send("~")
        Threading.Thread.Sleep(2000)
        SendKeys.Send("%(f)")
        Threading.Thread.Sleep(1000)
        SendKeys.Send("x")
        Me.Close()
    End Sub

    Private Sub btnXLS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnXLS.Click
        If lstXLS.Items.Count = 0 Then
            MsgBox("Please click Report before saving to Excel")
            Exit Sub
        End If

        Dim AppPath As String = ""
        Dim dialog As New FolderBrowserDialog()
        dialog.RootFolder = Environment.SpecialFolder.Desktop
        dialog.SelectedPath = "C:\"
        dialog.Description = "Select Application Configuration Files Path"
        If dialog.ShowDialog() = DialogResult.OK Then
            AppPath = dialog.SelectedPath

            If System.IO.File.Exists(AppPath & "\MonthlyStatusCodeRpt" & reporttype & ".csv") Then
                System.IO.File.Delete(AppPath & "\MonthlyStatusCodeRpt" & reporttype & ".csv")
            End If
            Dim file As System.IO.StreamWriter
            file = My.Computer.FileSystem.OpenTextFileWriter(AppPath & "\MonthlyStatusCodeRpt" & reporttype & ".csv", True)

            For i = 0 To lstXLS.Items.Count - 1
                file.WriteLine(lstXLS.Items(i))
            Next i
            file.Close()

        End If

    End Sub

    Private Sub cbAcctSpecific_CheckedChanged(sender As Object, e As EventArgs) Handles cbAcctSpecific.CheckedChanged
        If cbAcctSpecific.Checked = True Then
            txtAcct.Show()
            lblAccount.Show()
            cbDetail.Show()
            cbCodeSpecific.Checked = False
            cbSummary.Checked = False
            txtCode.Hide()
            lblCode.Hide()
            txtSumAcct.Hide()
            lblSumAcct.Hide()
            txtSumCode.Hide()
            lblSumCode.Hide()
        Else
            txtAcct.Hide()
            lblAccount.Hide()
        End If
    End Sub
    Public Function LJ(ByVal strLJ As String, ByVal iLen As Integer) As String
        LJ = ""
        If Len(strLJ) > iLen Then
            LJ = strLJ
        Else
            LJ = strLJ & Space(iLen - Len(strLJ))
        End If
    End Function

    Private Sub cbCodeSpecific_CheckedChanged(sender As Object, e As EventArgs) Handles cbCodeSpecific.CheckedChanged
        If cbCodeSpecific.Checked = True Then
            txtCode.Show()
            lblCode.Show()
            cbDetail.Show()
            cbAcctSpecific.Checked = False
            cbSummary.Checked = False
            txtAcct.Hide()
            lblAccount.Hide()
        Else
            txtCode.Hide()
            lblCode.Hide()
        End If
    End Sub

    Private Sub cbSummary_CheckedChanged(sender As Object, e As EventArgs) Handles cbSummary.CheckedChanged
        If cbSummary.Checked = True Then
            txtSumCode.Show()
            lblSumCode.Show()
            txtSumAcct.Show()
            lblSumAcct.Show()
            cbDetail.Checked = False
            cbAcctSpecific.Checked = False
            cbCodeSpecific.Checked = False
            txtAcct.Hide()
            lblAccount.Hide()
            txtCode.Hide()
            lblCode.Hide()
            cbDetail.Hide()
        Else
            txtSumCode.Hide()
            lblSumCode.Hide()
            txtSumAcct.Hide()
            lblSumAcct.Hide()

        End If
    End Sub
End Class