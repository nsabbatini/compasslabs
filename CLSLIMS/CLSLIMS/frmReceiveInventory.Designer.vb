﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReceiveInventory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReceiveInventory))
        Me.btnNewVendor = New DevExpress.XtraEditors.SimpleButton()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNewItem = New DevExpress.XtraEditors.SimpleButton()
        Me.grdInvItems = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grpItems = New DevExpress.XtraEditors.GroupControl()
        Me.dcbItems = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtQuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.GaugeControl1 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.gOnHand = New DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge()
        Me.DigitalBackgroundLayerComponent1 = New DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.dcbVendorId2 = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.dtReceived = New DevExpress.XtraEditors.DateEdit()
        Me.lblInvoiceOrPo = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtInvoiceNumber = New DevExpress.XtraEditors.TextEdit()
        Me.chkPO = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.grdInvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpItems.SuspendLayout()
        CType(Me.dcbItems.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gOnHand, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DigitalBackgroundLayerComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dcbVendorId2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtReceived.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtReceived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkPO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnNewVendor
        '
        Me.btnNewVendor.ImageOptions.Image = CType(resources.GetObject("btnNewVendor.ImageOptions.Image"), System.Drawing.Image)
        Me.btnNewVendor.Location = New System.Drawing.Point(496, 9)
        Me.btnNewVendor.Name = "btnNewVendor"
        Me.btnNewVendor.Size = New System.Drawing.Size(139, 34)
        Me.btnNewVendor.TabIndex = 26
        Me.btnNewVendor.Text = "New Vendor"
        '
        'btnExit
        '
        Me.btnExit.ImageOptions.Image = CType(resources.GetObject("btnExit.ImageOptions.Image"), System.Drawing.Image)
        Me.btnExit.Location = New System.Drawing.Point(351, 46)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(131, 32)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnExit)
        Me.GroupControl1.Controls.Add(Me.btnSave)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl1.Location = New System.Drawing.Point(0, 565)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(655, 103)
        Me.GroupControl1.TabIndex = 25
        Me.GroupControl1.Text = "Actions"
        '
        'btnSave
        '
        Me.btnSave.ImageOptions.Image = CType(resources.GetObject("btnSave.ImageOptions.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(172, 46)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(131, 32)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save"
        '
        'btnNewItem
        '
        Me.btnNewItem.ImageOptions.Image = CType(resources.GetObject("btnNewItem.ImageOptions.Image"), System.Drawing.Image)
        Me.btnNewItem.Location = New System.Drawing.Point(496, 31)
        Me.btnNewItem.Name = "btnNewItem"
        Me.btnNewItem.Size = New System.Drawing.Size(139, 34)
        Me.btnNewItem.TabIndex = 23
        Me.btnNewItem.Text = "New Item"
        '
        'grdInvItems
        '
        Me.grdInvItems.Location = New System.Drawing.Point(94, 161)
        Me.grdInvItems.MainView = Me.GridView1
        Me.grdInvItems.Name = "grdInvItems"
        Me.grdInvItems.Size = New System.Drawing.Size(538, 223)
        Me.grdInvItems.TabIndex = 22
        Me.grdInvItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.GridView1.GridControl = Me.grdInvItems
        Me.GridView1.Name = "GridView1"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Item Description"
        Me.GridColumn1.FieldName = "ItemDesc"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 173
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Item Id"
        Me.GridColumn2.FieldName = "ItemId"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 173
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Qty"
        Me.GridColumn3.FieldName = "CurrentQty"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 50
        '
        'grpItems
        '
        Me.grpItems.Controls.Add(Me.btnNewItem)
        Me.grpItems.Controls.Add(Me.grdInvItems)
        Me.grpItems.Controls.Add(Me.dcbItems)
        Me.grpItems.Controls.Add(Me.LabelControl6)
        Me.grpItems.Controls.Add(Me.txtQuan)
        Me.grpItems.Controls.Add(Me.LabelControl4)
        Me.grpItems.Controls.Add(Me.GaugeControl1)
        Me.grpItems.Controls.Add(Me.LabelControl5)
        Me.grpItems.Enabled = False
        Me.grpItems.Location = New System.Drawing.Point(0, 159)
        Me.grpItems.Name = "grpItems"
        Me.grpItems.Size = New System.Drawing.Size(653, 389)
        Me.grpItems.TabIndex = 24
        Me.grpItems.Text = "Items Received"
        '
        'dcbItems
        '
        Me.dcbItems.Location = New System.Drawing.Point(171, 37)
        Me.dcbItems.Name = "dcbItems"
        Me.dcbItems.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dcbItems.Properties.Appearance.Options.UseFont = True
        Me.dcbItems.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dcbItems.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ItemDesc", "Vendor Desc"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("itemid", "Item Id"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("CurrentQty", "CurrentQty", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[Default])})
        Me.dcbItems.Properties.DropDownRows = 20
        Me.dcbItems.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch
        Me.dcbItems.Size = New System.Drawing.Size(311, 26)
        Me.dcbItems.TabIndex = 21
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(103, 102)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(66, 19)
        Me.LabelControl6.TabIndex = 18
        Me.LabelControl6.Text = "Quantity:"
        '
        'txtQuan
        '
        Me.txtQuan.Location = New System.Drawing.Point(171, 99)
        Me.txtQuan.Name = "txtQuan"
        Me.txtQuan.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtQuan.Properties.Appearance.Options.UseFont = True
        Me.txtQuan.Size = New System.Drawing.Size(52, 26)
        Me.txtQuan.TabIndex = 17
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(507, 76)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl4.TabIndex = 16
        Me.LabelControl4.Text = "Current On-Hand"
        '
        'GaugeControl1
        '
        Me.GaugeControl1.AutoLayout = False
        Me.GaugeControl1.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.gOnHand})
        Me.GaugeControl1.Location = New System.Drawing.Point(493, 101)
        Me.GaugeControl1.Name = "GaugeControl1"
        Me.GaugeControl1.Size = New System.Drawing.Size(139, 58)
        Me.GaugeControl1.TabIndex = 15
        '
        'gOnHand
        '
        Me.gOnHand.AppearanceOff.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#0D8097")
        Me.gOnHand.AppearanceOn.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#02F0F7")
        Me.gOnHand.BackgroundLayers.AddRange(New DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent() {Me.DigitalBackgroundLayerComponent1})
        Me.gOnHand.Bounds = New System.Drawing.Rectangle(6, 6, 127, 46)
        Me.gOnHand.DigitCount = 3
        Me.gOnHand.Name = "gOnHand"
        Me.gOnHand.Padding = New DevExpress.XtraGauges.Core.Base.TextSpacing(26, 20, 26, 20)
        Me.gOnHand.Text = "00.000"
        '
        'DigitalBackgroundLayerComponent1
        '
        Me.DigitalBackgroundLayerComponent1.BottomRight = New DevExpress.XtraGauges.Core.Base.PointF2D(169.8875!, 99.9625!)
        Me.DigitalBackgroundLayerComponent1.Name = "digitalBackgroundLayerComponent1"
        Me.DigitalBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style17
        Me.DigitalBackgroundLayerComponent1.TopLeft = New DevExpress.XtraGauges.Core.Base.PointF2D(26.0!, 0!)
        Me.DigitalBackgroundLayerComponent1.ZOrder = 1000
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(110, 44)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(59, 19)
        Me.LabelControl5.TabIndex = 9
        Me.LabelControl5.Text = "Item Id:"
        '
        'dcbVendorId2
        '
        Me.dcbVendorId2.Location = New System.Drawing.Point(171, 14)
        Me.dcbVendorId2.Name = "dcbVendorId2"
        Me.dcbVendorId2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dcbVendorId2.Properties.Appearance.Options.UseFont = True
        Me.dcbVendorId2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dcbVendorId2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("VendorName", "Vendor"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("VendorId", "Vendor Id")})
        Me.dcbVendorId2.Properties.DropDownRows = 20
        Me.dcbVendorId2.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.StartsWith
        Me.dcbVendorId2.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch
        Me.dcbVendorId2.Size = New System.Drawing.Size(311, 26)
        Me.dcbVendorId2.TabIndex = 18
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(64, 105)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(105, 19)
        Me.LabelControl3.TabIndex = 23
        Me.LabelControl3.Text = "Date Received:"
        '
        'dtReceived
        '
        Me.dtReceived.EditValue = Nothing
        Me.dtReceived.Location = New System.Drawing.Point(171, 102)
        Me.dtReceived.Name = "dtReceived"
        Me.dtReceived.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtReceived.Properties.Appearance.Options.UseFont = True
        Me.dtReceived.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtReceived.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtReceived.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista
        Me.dtReceived.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[True]
        Me.dtReceived.Size = New System.Drawing.Size(159, 26)
        Me.dtReceived.TabIndex = 22
        '
        'lblInvoiceOrPo
        '
        Me.lblInvoiceOrPo.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblInvoiceOrPo.Appearance.Options.UseFont = True
        Me.lblInvoiceOrPo.Location = New System.Drawing.Point(95, 60)
        Me.lblInvoiceOrPo.Name = "lblInvoiceOrPo"
        Me.lblInvoiceOrPo.Size = New System.Drawing.Size(74, 19)
        Me.lblInvoiceOrPo.TabIndex = 21
        Me.lblInvoiceOrPo.Text = "Invoice #:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(94, 17)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(75, 19)
        Me.LabelControl1.TabIndex = 20
        Me.LabelControl1.Text = "Vendor id:"
        '
        'txtInvoiceNumber
        '
        Me.txtInvoiceNumber.Location = New System.Drawing.Point(171, 57)
        Me.txtInvoiceNumber.Name = "txtInvoiceNumber"
        Me.txtInvoiceNumber.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtInvoiceNumber.Properties.Appearance.Options.UseFont = True
        Me.txtInvoiceNumber.Size = New System.Drawing.Size(311, 26)
        Me.txtInvoiceNumber.TabIndex = 19
        '
        'chkPO
        '
        Me.chkPO.Location = New System.Drawing.Point(496, 60)
        Me.chkPO.Name = "chkPO"
        Me.chkPO.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.chkPO.Properties.Appearance.Options.UseFont = True
        Me.chkPO.Properties.Caption = "PO#"
        Me.chkPO.Size = New System.Drawing.Size(75, 23)
        Me.chkPO.TabIndex = 27
        '
        'frmReceiveInventory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 668)
        Me.Controls.Add(Me.chkPO)
        Me.Controls.Add(Me.btnNewVendor)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.grpItems)
        Me.Controls.Add(Me.dcbVendorId2)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.dtReceived)
        Me.Controls.Add(Me.lblInvoiceOrPo)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.txtInvoiceNumber)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "frmReceiveInventory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receive Inventory"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.grdInvItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpItems.ResumeLayout(False)
        Me.grpItems.PerformLayout()
        CType(Me.dcbItems.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gOnHand, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DigitalBackgroundLayerComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dcbVendorId2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtReceived.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtReceived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkPO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnNewVendor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNewItem As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents grdInvItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents grpItems As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dcbItems As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtQuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GaugeControl1 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents gOnHand As DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge
    Private WithEvents DigitalBackgroundLayerComponent1 As DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dcbVendorId2 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtReceived As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblInvoiceOrPo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtInvoiceNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkPO As DevExpress.XtraEditors.CheckEdit
End Class
