﻿Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmVndDef

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim Upd As String = ""
        If lblNew.Visible = False Then
            'Insert
            Upd = "Update Vendors set Vendorname='" & txtVndName.Text & "'," &
                                     "vendorabbr='" & txtVndAbbr.Text & "'," &
                                     "address='" & txtVndAddr1.Text & "'," &
                                     "address2='" & txtVndAddr2.Text & "'," &
                                     "city='" & txtCity.Text & "'," &
                                     "state='" & txtState.Text & "'," &
                                     "zip='" & txtZip.Text & "'," &
                                     "contact='" & txtContact.Text & "'," &
                                     "phone='" & txtPhone.Text & "'," &
                                     "fax='" & txtFax.Text & "'," &
                                     "notes='" & txtNotes.Text & "' Where vendorid='" & cmbVndID.Text & "'"
        Else
            'Update
            Upd = "Insert into Vendors Values ('" & cmbVndID.Text & "','" & txtVndName.Text & "','" & txtVndAbbr.Text & "','" & txtVndAddr1.Text & "','" & txtVndAddr2.Text & "','" & txtCity.Text & "','" &
                txtState.Text & "','" & txtZip.Text & "','" & txtContact.Text & "','" & txtPhone.Text & "','" & txtFax.Text & "','" & txtNotes.Text & "')"
        End If
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand
        Cmd.CommandText = Upd
        Cmd.Connection = ConnMySQL
        Cmd.ExecuteNonQuery()
        ConnMySQL.Close()
        ClearFields()
    End Sub

    Private Sub cmbVndID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbVndID.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        Dim Sel As String = "Select * from Vendors Where vendorid='" & cmbVndID.Text & "'"
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySQL)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            txtVndName.Text = rs(1)
            txtVndAbbr.Text = rs(2)
            txtVndAddr1.Text = rs(3)
            txtVndAddr2.Text = rs(4)
            txtCity.Text = rs(5)
            txtState.Text = rs(6)
            txtZip.Text = rs(7)
            txtContact.Text = rs(8)
            txtPhone.Text = rs(9)
            txtFax.Text = rs(10)
            txtNotes.Text = rs(11)
        Else
            lblNew.Visible = True
            txtVndName.Focus()
        End If
        rs.Close()
        ConnMySQL.Close()
    End Sub

    Private Sub frmVndDef_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "REMOND.YOUNG" Then
            Me.Close()
            Me.Dispose()
        End If
        ClearFields()
    End Sub
    Public Sub ClearFields()
        lblNew.Visible = False
        cmbVndID.Text = ""
        txtVndName.Text = ""
        txtVndAbbr.Text = ""
        txtVndAddr1.Text = ""
        txtVndAddr2.Text = ""
        txtCity.Text = ""
        txtState.Text = ""
        txtZip.Text = ""
        txtContact.Text = ""
        txtPhone.Text = ""
        txtFax.Text = ""
        txtNotes.Text = ""
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySQL
        Cmd.CommandText = "Select * from Vendors"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cmbVndID.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySQL.Close()
        cmbVndID.Focus()
    End Sub
End Class