﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAcc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.rbTrans = New System.Windows.Forms.RadioButton()
        Me.rbDOS = New System.Windows.Forms.RadioButton()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbDoll = New System.Windows.Forms.RadioButton()
        Me.rbCnts = New System.Windows.Forms.RadioButton()
        Me.gbRpt = New System.Windows.Forms.GroupBox()
        Me.gbDate = New System.Windows.Forms.GroupBox()
        Me.gbRpt.SuspendLayout()
        Me.gbDate.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbTrans
        '
        Me.rbTrans.AutoSize = True
        Me.rbTrans.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbTrans.Location = New System.Drawing.Point(10, 55)
        Me.rbTrans.Name = "rbTrans"
        Me.rbTrans.Size = New System.Drawing.Size(167, 23)
        Me.rbTrans.TabIndex = 19
        Me.rbTrans.TabStop = True
        Me.rbTrans.Text = "Transaction Date"
        Me.rbTrans.UseVisualStyleBackColor = True
        '
        'rbDOS
        '
        Me.rbDOS.AutoSize = True
        Me.rbDOS.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbDOS.Location = New System.Drawing.Point(10, 26)
        Me.rbDOS.Name = "rbDOS"
        Me.rbDOS.Size = New System.Drawing.Size(151, 23)
        Me.rbDOS.TabIndex = 18
        Me.rbDOS.TabStop = True
        Me.rbDOS.Text = "Date of Service"
        Me.rbDOS.UseVisualStyleBackColor = True
        '
        'btnRun
        '
        Me.btnRun.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRun.Location = New System.Drawing.Point(297, 207)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(75, 31)
        Me.btnRun.TabIndex = 17
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(401, 207)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 31)
        Me.btnExit.TabIndex = 16
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'dtpEnd
        '
        Me.dtpEnd.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnd.Location = New System.Drawing.Point(355, 148)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(144, 27)
        Me.dtpEnd.TabIndex = 15
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(313, 154)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(26, 19)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "to"
        '
        'dtpStart
        '
        Me.dtpStart.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStart.Location = New System.Drawing.Point(151, 148)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(142, 27)
        Me.dtpStart.TabIndex = 13
        Me.dtpStart.Value = New Date(2018, 8, 22, 0, 0, 0, 0)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(18, 150)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(111, 19)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Date Range:"
        '
        'rbDoll
        '
        Me.rbDoll.AutoSize = True
        Me.rbDoll.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbDoll.Location = New System.Drawing.Point(16, 55)
        Me.rbDoll.Name = "rbDoll"
        Me.rbDoll.Size = New System.Drawing.Size(47, 23)
        Me.rbDoll.TabIndex = 22
        Me.rbDoll.TabStop = True
        Me.rbDoll.Text = "$$"
        Me.rbDoll.UseVisualStyleBackColor = True
        '
        'rbCnts
        '
        Me.rbCnts.AutoSize = True
        Me.rbCnts.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCnts.Location = New System.Drawing.Point(16, 31)
        Me.rbCnts.Name = "rbCnts"
        Me.rbCnts.Size = New System.Drawing.Size(83, 23)
        Me.rbCnts.TabIndex = 21
        Me.rbCnts.TabStop = True
        Me.rbCnts.Text = "Counts"
        Me.rbCnts.UseVisualStyleBackColor = True
        '
        'gbRpt
        '
        Me.gbRpt.Controls.Add(Me.rbDoll)
        Me.gbRpt.Controls.Add(Me.rbCnts)
        Me.gbRpt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRpt.Location = New System.Drawing.Point(72, 25)
        Me.gbRpt.Name = "gbRpt"
        Me.gbRpt.Size = New System.Drawing.Size(200, 89)
        Me.gbRpt.TabIndex = 23
        Me.gbRpt.TabStop = False
        Me.gbRpt.Text = "Report Type"
        '
        'gbDate
        '
        Me.gbDate.Controls.Add(Me.rbTrans)
        Me.gbDate.Controls.Add(Me.rbDOS)
        Me.gbDate.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDate.Location = New System.Drawing.Point(299, 25)
        Me.gbDate.Name = "gbDate"
        Me.gbDate.Size = New System.Drawing.Size(200, 89)
        Me.gbDate.TabIndex = 24
        Me.gbDate.TabStop = False
        Me.gbDate.Text = "Date Type"
        '
        'frmAcc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(548, 258)
        Me.Controls.Add(Me.gbDate)
        Me.Controls.Add(Me.gbRpt)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.dtpEnd)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpStart)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmAcc"
        Me.Text = "Accessions"
        Me.gbRpt.ResumeLayout(False)
        Me.gbRpt.PerformLayout()
        Me.gbDate.ResumeLayout(False)
        Me.gbDate.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents rbTrans As RadioButton
    Friend WithEvents rbDOS As RadioButton
    Friend WithEvents btnRun As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents dtpEnd As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents dtpStart As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents rbDoll As RadioButton
    Friend WithEvents rbCnts As RadioButton
    Friend WithEvents gbRpt As GroupBox
    Friend WithEvents gbDate As GroupBox
End Class
