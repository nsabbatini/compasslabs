﻿Imports Compass.Global
Imports Compass.MySql.Database
Public Class frmShowOrders
    Public Property OrderId As Int16
    Private _dirPathToFIle As String = AppDomain.CurrentDomain.BaseDirectory() 'Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty

    Private Sub frmShowOrders_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False
        GridView1.OptionsView.ShowGroupPanel = False
        GridView1.OptionsView.ShowIndicator = False
        GridView1.OptionsBehavior.Editable = False
        'Me.StartPosition = FormStartPosition.Manual
        'Me.Location = Screen.GetWorkingArea(Me).Location
        Dim scr As Screen = Screen.FromPoint(Cursor.Position)

        Me.Location = New Point(scr.WorkingArea.Right - Me.Width, scr.WorkingArea.Bottom - Me.Height)
        LoadOrders()
    End Sub
    Private Sub LoadOrders()

        Dim results = GetOrdersInQue()

        grdItems.DataSource = results

    End Sub
    Private Function GetOrdersInQue() As Object

        Try


            Using getVendors As New OrdersDb()
                _fileName = _fileDir & GetOrdersInPopupWindowSql
                getVendors._sqlText = ReadSqlFile(_fileName)
                'getVendors._orderNumber = OrderNum
                Return getVendors.GetPopOrderItems.Where(Function(x) x.OrderId = OrderId).ToList()
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmGetQuedOrdersb.GetOrdersInQue", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function

End Class