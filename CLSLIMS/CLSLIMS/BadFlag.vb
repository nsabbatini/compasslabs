﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports CLSLIMS.BadFlag

Public Class BadFlag

    Private Sub BadFlag_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated


    End Sub

    Public Function GetComments(ByVal SpecNo) As Array
        Dim xSel As String = "Select PSCMTIx,PSCMTTXT from PSCMT, PSCMTI where PSCMTACT=1 AND PSCMT.PSCMTTYPE = PSCMTI.PSCMTTYPE AND PSCMT.PSCMTEFDT = PSCMTI.PSCMTEFDT " & _
                             "and PSCMT.PSPECNO = PSCMTI.PSPECNO and PSCMT.PSPECNO = '" & SpecNo & "'"
        Dim j As Integer
        Dim tmpArr(100) As String
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            j = 0
            Do While cRS.Read
                tmpArr(j) = cRS(0) & Space(5 - Len(cRS(0))) & cRS(1)
                j = j + 1
            Loop
        End Using
        GetComments = tmpArr
    End Function
    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim printer As New PCPrint()
        'Set the font we want to use
        printer.PrinterFont = New Font("Courier New", 10)
        'Set Margins
        Dim mar As New Printing.Margins(50, 50, 40, 50)
        printer.DefaultPageSettings.Margins = mar

        Dim txt As String = "Status Codes " & Format(Now, "MM/dd/yyyy hh:mm") & vbCrLf
        For i = 0 To ListBox1.Items.Count - 1
            txt = txt & vbCrLf & ListBox1.Items(i)
        Next i
        printer.TextToPrint = " " & txt
        'Issue print command
        printer.Print()

    End Sub

    Private Sub btnBuild_Click(sender As System.Object, e As System.EventArgs) Handles btnBuild.Click

        Dim strCds As String
        Select Case cmbCode.Text
            Case "ALL" : strCds = "('AFF HOLD','CARD','CMPLT','INSURANCE') "
            Case Else
                strCds = "('" & cmbCode.Text & "') "
        End Select
        Dim Sel As String = "select PSSTAT.PSPECNO,substring(PSPEC.PSRCVDT,5,2)+'/'+substring(PSPEC.PSRCVDT,7,2)+'/'+substring(PSPEC.PSRCVDT,1,4) as rcv,PSSTAT.STATCD,PORD.ACCT,ANAME " & _
                    "from PSSTAT, PSPEC, PORD, ACCT " & _
                    "where PSSTAT.PSPECNO = PSPEC.PSPECNO " & _
                    "And PSSTAT.STATCD in " & strCds & _
                    "and PSSTAT.ADSQ1=PSPEC.ADSQ1 and PSPEC.PSACT=1 " & _
                    "and PORD.ACCT=ACCT.ACCT and ACCT.AACT=1 " & _
                    "and PORD.PORD = PSPEC.PORD and PORD.POEFDT=PSPEC.POEFDT and PORD.POACT=1 " & _
                    "Order by rcv"
        Dim arrCmt(50) As String
        ListBox1.Items.Clear()
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                ListBox1.Items.Add(rs(0) & Space(10 - Len(rs(0))) & rs(1) & Space(16 - Len(rs(1))) & rs(2) & Space(3) & rs(3) & " - " & rs(4))
                ReDim arrCmt(50)
                arrCmt = GetComments(rs(0))
                If arrCmt(0) <> "" Then
                    For i = 0 To 4
                        If arrCmt(i) <> "" Then ListBox1.Items.Add(Space(5) & arrCmt(i))
                    Next i
                End If
                ListBox1.Items.Add("=====================================================================")
            Loop
            rs.Close()
            Conn.Close()
        End Using
    End Sub
End Class