﻿Imports System.Data.SqlClient
Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class frmLoc
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmLoc_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        If CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "REMOND.YOUNG" Then
            Me.Close()
            Me.Dispose()
        End If
        Dim Sel As String = "Select * from locations Where loccanbeparent=1"
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()

        Dim Cmd As New MySqlCommand(Sel, ConnMySQL)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            cbLocParent.Items.Add(rs(0))
        Loop
        rs.Close()
        ConnMySQL.Close()

    End Sub

    Private Sub txtLoc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtLoc.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        Dim Sel As String = "Select * from locations Where locid='" & txtLoc.Text & "'"
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand(Sel, ConnMySQL)
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            txtLocDesc.Text = rs(1)
            txtLocAbbr.Text = rs(2)
            txtLocSeq.Text = rs(3)
            cbLocParent.Text = rs(5)
            If rs(4) = "1" Then
                rbYes.Checked = True
            Else
                rbNo.Checked = False
            End If
        Else
            lblNew.Visible = True
            txtLocDesc.Focus()
        End If
        rs.Close()
        ConnMySQL.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim Upd As String = "", CanBeParent As Integer = 0
        If rbYes.Checked Then
            CanBeParent = 1
        Else
            CanBeParent = 0
        End If
        If lblNew.Visible = False Then
            'Insert
            Upd = "Update locations set locdesc='" & txtLocDesc.Text & "'," &
                                     "locabbr='" & txtLocAbbr.Text & "'," &
                                     "locseq='" & txtLocSeq.Text & "'," &
                                     "loccanbeparent='" & CanBeParent & "'," &
                                     "locparent='" & cbLocParent.Text & "' " &
                                     "Where locid='" & txtLoc.Text & "'"
        Else
            'Update
            Upd = "Insert into locations Values ('" & txtLoc.Text & "','" & txtLocDesc.Text & "','" & txtLocAbbr.Text & "','" & txtLocSeq.Text & "'," &
                CanBeParent & ",'" & cbLocParent.Text & "')"
        End If
        Dim ConnMySQL As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySQL.Open()
        Dim Cmd As New MySqlCommand
        Cmd.CommandText = Upd
        Cmd.Connection = ConnMySQL
        Cmd.ExecuteNonQuery()
        ConnMySQL.Close()
        lblNew.Visible = False
        txtLoc.Text = ""
        txtLocDesc.Text = ""
        txtLocAbbr.Text = ""
        txtLocSeq.Text = ""
        cbLocParent.Text = ""
        rbNo.Checked = True
        txtLoc.Focus()

    End Sub
End Class