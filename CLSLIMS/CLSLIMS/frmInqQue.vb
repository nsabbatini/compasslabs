﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.ComponentModel

Public Class frmInqQue
    Private Sub frmInqQue_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Control.CheckForIllegalCrossThreadCalls = False

        UpdGrid.RunWorkerAsync()
    End Sub
    Private Sub UpdGrid_DoWork(sender As Object, e As DoWorkEventArgs) Handles UpdGrid.DoWork
        Dim sel As String = "select ins,convert(varchar,count(distinct ispecno)),convert(varchar,count(*)) from qiul group by ins union select 'zall',convert(varchar,count(distinct ispecno)),convert(varchar,count(*)) from qiul"
        Dim ins As String = "", Total As Integer = 0, PrevTotal As Integer = 0, StrTime As Date = Now, InitCnt As Integer = -1
        btnAgain.Enabled = False
        For i = 240 To 1 Step -1
            Dim Conn As New SqlConnection("Server=10.65.1.12;Database=TNCL_PROD;User Id=sa;Password=C0mp@SS**;MultipleActiveResultSets=True")
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            lstInsQue.Items.Clear()
            Do While rs.Read()
                ins = rs(0)
                If ins = "zall" Then ins = "            Total" : Total = rs(2) : If InitCnt = -1 Then InitCnt = Val(rs(2))
                lstInsQue.Items.Add(New ListViewItem({ins, rs(1), rs(2)}))
                lblTime.Text = Now : lblCnt.Text = i : lblStats.Text = "Elaspsed Mins: " & DateDiff(DateInterval.Minute, StrTime, Now) & ", Results Processed: " & (InitCnt - Val(rs(2)))
            Loop
            rs.Close()
            Conn.Close()
            If Total = PrevTotal And Total <> 0 And PrevTotal <> 0 Then
                lstInsQue.BackColor = Color.Red
            Else
                lstInsQue.BackColor = Color.YellowGreen
            End If
            Threading.Thread.Sleep(15000)
        Next i
        'lstInsQue.Items.Add(New ListViewItem({"Time", Format(Now, "MM/dd/yyyy"), Format(Now, "HH24:mm:ss")}))
        btnAgain.Enabled = True
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub UpdGrid_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles UpdGrid.RunWorkerCompleted
        lstInsQue.Items.Clear()
        lstInsQue.Items.Add(New ListViewItem({"", "", ""}))
        lstInsQue.Items.Add(New ListViewItem({"Click 'Again' to", "continue", "monitoring."}))
        lblCnt.Text = "0"
    End Sub

    Private Sub btnAgain_Click(sender As Object, e As EventArgs) Handles btnAgain.Click
        UpdGrid.RunWorkerAsync()
    End Sub
End Class