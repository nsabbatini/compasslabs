﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class LabelPrt

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        If Val(txtPerLabel.Text) < 0 Or Val(txtPerLabel.Text) > 3 Then
            MsgBox("Invalid number of labels per Specimen Number. (" & txtPerLabel.Text & ")")
            txtPerLabel.Focus()
            Exit Sub
        End If
        'Print Labels

        Dim ipAddress As String = CLSLIMS.PtrOne     '"192.168.65.209"
        ipAddress = "192.168.65.39"       'specimen prep
        Dim port As Integer = 9100
        Dim ZPLString As String = ""
        Dim itxt As Integer = Val(txtStart.Text)

        For j = 1 To Val(txtCount.Text)    '100000 - 100599  4/27/2020
            ZPLString = "^XA" & "^LH10,0" & "^FO50,35" & "^A0N" & "^AE,N,5,5" & "^FD" & itxt & "^FS"
            'ZPLString = ZPLString & "^FO35,80" & "^BY3^BCN,90,N,N,N" & "^FD" & itxt & "^FS" & 
            ZPLString = ZPLString & "^XZ"
            itxt = itxt + 1
            Try
                'Open Connection
                Dim client As New System.Net.Sockets.TcpClient
                client.Connect(ipAddress, port)
                'Write ZPL String to Connection
                Dim writer As New System.IO.StreamWriter(client.GetStream())
                For k = 1 To Val(txtPerLabel.Text)
                    writer.Write(ZPLString)
                    writer.Flush()
                Next k
                writer.Close()
                client.Close()

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next j
        txtStart.Text = ""
        txtStart.Focus()

    End Sub

    Private Sub LabelPrt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
    End Sub
End Class