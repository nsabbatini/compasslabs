﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Public Class frmAccRcvDt
    Private Sub frmAccRcvDt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" And CLSLIMS.UID <> "NED.SABBATINI" And CLSLIMS.UID <> "JONATHON.MORGAN" And CLSLIMS.UID <> "STEVE.ALEXANDER" And CLSLIMS.UID <> "KELLY.ELMORE" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        dtpStart.Format = DateTimePickerFormat.Custom
        dtpStart.CustomFormat = "MM/dd/yyyy"
        dtpStart.Text = DateAdd(DateInterval.Month, -3, Now)
        dtpEnd.Format = DateTimePickerFormat.Custom
        dtpEnd.CustomFormat = "MM/dd/yyyy"
        dtpEnd.Text = Now
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\AccCnt.csv", False)
        Dim Title As String = "", r As String = "", p As Integer = 0, SQL As String = "", Acct As String = "", PrevAcct As String = "", cnt As Integer = 1

        Title = "Accessions by Received Date"
        file.WriteLine(",,,,," & Title)
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Sel As String = "Select Acct,count(distinct pspecno) From pspec a, pord b where a.pord=b.pord and a.poefdt=b.poefdt and psact=1 and poact=1 and acct not in ('999','PTSAMPLES','220','S220') " &
            "And substring(psrcvdt,1,8) between '" & CLSLIMS.Piece(dtpStart.Text, "/", 3) & CLSLIMS.Piece(dtpStart.Text, "/", 1) & CLSLIMS.Piece(dtpStart.Text, "/", 2) & "' And '" & CLSLIMS.Piece(dtpEnd.Text, "/", 3) & CLSLIMS.Piece(dtpEnd.Text, "/", 1) & CLSLIMS.Piece(dtpEnd.Text, "/", 2) & "' Group by Acct Order by Acct"
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read()
            file.WriteLine(rs(0) & "," & GetAcctName(rs(0)) & "," & rs(1))
            cnt = cnt + 1
        Loop
        rs.Close()
        Conn.Close()
        file.WriteLine(",,=SUM(C2:C" & cnt & ")")
        file.Close()
        System.Diagnostics.Process.Start("C:\CLSLIMS\AccCnt.csv")
        Me.Close()
        Me.Dispose()
    End Sub
    Public Function GetAcctName(ByVal Acct) As String
        GetAcctName = ""
        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where Acct = '" & Acct & "' order by aefdt desc"   ' and aact=1 "

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetAcctName = Replace(cRS(1), ",", " ")
            Else
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class