﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAcctOCs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.cbOralAmp = New System.Windows.Forms.CheckBox()
        Me.cbOralBenz = New System.Windows.Forms.CheckBox()
        Me.cbOralBup = New System.Windows.Forms.CheckBox()
        Me.cbOralTHC = New System.Windows.Forms.CheckBox()
        Me.cbOralCOC = New System.Windows.Forms.CheckBox()
        Me.cbOralFent = New System.Windows.Forms.CheckBox()
        Me.cbOralHero = New System.Windows.Forms.CheckBox()
        Me.cbOralMethAmp = New System.Windows.Forms.CheckBox()
        Me.cbOralMethy = New System.Windows.Forms.CheckBox()
        Me.cbOralOpi = New System.Windows.Forms.CheckBox()
        Me.cbOralOpiAnal = New System.Windows.Forms.CheckBox()
        Me.cbOralOxy = New System.Windows.Forms.CheckBox()
        Me.cbOralPhen = New System.Windows.Forms.CheckBox()
        Me.cbOralPro = New System.Windows.Forms.CheckBox()
        Me.cbOralSkel = New System.Windows.Forms.CheckBox()
        Me.cbOralTap = New System.Windows.Forms.CheckBox()
        Me.cbOralMtd = New System.Windows.Forms.CheckBox()
        Me.cbOralTram = New System.Windows.Forms.CheckBox()
        Me.lblAcct = New System.Windows.Forms.Label()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.dtpProfDt = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.lstData = New System.Windows.Forms.ListBox()
        Me.cbXREF = New System.Windows.Forms.CheckBox()
        Me.cbOralAlk = New System.Windows.Forms.CheckBox()
        Me.cbOralADSer = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiTri = New System.Windows.Forms.CheckBox()
        Me.cbOralAnti = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiEpi = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiPsych = New System.Windows.Forms.CheckBox()
        Me.cbOralGaba = New System.Windows.Forms.CheckBox()
        Me.cbOralPregaba = New System.Windows.Forms.CheckBox()
        Me.cbOralSedHyp = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(105, 24)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(115, 27)
        Me.txtAcct.TabIndex = 69
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 19)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Account:"
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(477, 387)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(78, 28)
        Me.btnSave.TabIndex = 73
        Me.btnSave.Text = "SAVE"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(189, 387)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(157, 28)
        Me.btnClear.TabIndex = 72
        Me.btnClear.Text = "Clear Selections"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(564, 387)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(78, 28)
        Me.btnExit.TabIndex = 71
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'cbOralAmp
        '
        Me.cbOralAmp.AutoSize = True
        Me.cbOralAmp.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAmp.Location = New System.Drawing.Point(29, 92)
        Me.cbOralAmp.Name = "cbOralAmp"
        Me.cbOralAmp.Size = New System.Drawing.Size(145, 18)
        Me.cbOralAmp.TabIndex = 74
        Me.cbOralAmp.Text = "Oral Amphetamines"
        Me.cbOralAmp.UseVisualStyleBackColor = True
        '
        'cbOralBenz
        '
        Me.cbOralBenz.AutoSize = True
        Me.cbOralBenz.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralBenz.Location = New System.Drawing.Point(299, 68)
        Me.cbOralBenz.Name = "cbOralBenz"
        Me.cbOralBenz.Size = New System.Drawing.Size(154, 18)
        Me.cbOralBenz.TabIndex = 75
        Me.cbOralBenz.Text = "Oral Benzodiazepines"
        Me.cbOralBenz.UseVisualStyleBackColor = True
        '
        'cbOralBup
        '
        Me.cbOralBup.AutoSize = True
        Me.cbOralBup.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralBup.Location = New System.Drawing.Point(299, 92)
        Me.cbOralBup.Name = "cbOralBup"
        Me.cbOralBup.Size = New System.Drawing.Size(145, 18)
        Me.cbOralBup.TabIndex = 76
        Me.cbOralBup.Text = "Oral Buprenorphine"
        Me.cbOralBup.UseVisualStyleBackColor = True
        '
        'cbOralTHC
        '
        Me.cbOralTHC.AutoSize = True
        Me.cbOralTHC.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTHC.Location = New System.Drawing.Point(299, 118)
        Me.cbOralTHC.Name = "cbOralTHC"
        Me.cbOralTHC.Size = New System.Drawing.Size(188, 18)
        Me.cbOralTHC.TabIndex = 77
        Me.cbOralTHC.Text = "Oral Cannabinoids, Natural"
        Me.cbOralTHC.UseVisualStyleBackColor = True
        '
        'cbOralCOC
        '
        Me.cbOralCOC.AutoSize = True
        Me.cbOralCOC.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralCOC.Location = New System.Drawing.Point(299, 145)
        Me.cbOralCOC.Name = "cbOralCOC"
        Me.cbOralCOC.Size = New System.Drawing.Size(101, 18)
        Me.cbOralCOC.TabIndex = 78
        Me.cbOralCOC.Text = "Oral Cocaine"
        Me.cbOralCOC.UseVisualStyleBackColor = True
        '
        'cbOralFent
        '
        Me.cbOralFent.AutoSize = True
        Me.cbOralFent.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralFent.Location = New System.Drawing.Point(299, 172)
        Me.cbOralFent.Name = "cbOralFent"
        Me.cbOralFent.Size = New System.Drawing.Size(112, 18)
        Me.cbOralFent.TabIndex = 79
        Me.cbOralFent.Text = "Oral Fentanyls"
        Me.cbOralFent.UseVisualStyleBackColor = True
        '
        'cbOralHero
        '
        Me.cbOralHero.AutoSize = True
        Me.cbOralHero.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralHero.Location = New System.Drawing.Point(299, 223)
        Me.cbOralHero.Name = "cbOralHero"
        Me.cbOralHero.Size = New System.Drawing.Size(164, 18)
        Me.cbOralHero.TabIndex = 80
        Me.cbOralHero.Text = "Oral Heroin Metabolite"
        Me.cbOralHero.UseVisualStyleBackColor = True
        '
        'cbOralMethAmp
        '
        Me.cbOralMethAmp.AutoSize = True
        Me.cbOralMethAmp.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMethAmp.Location = New System.Drawing.Point(299, 274)
        Me.cbOralMethAmp.Name = "cbOralMethAmp"
        Me.cbOralMethAmp.Size = New System.Drawing.Size(247, 18)
        Me.cbOralMethAmp.TabIndex = 81
        Me.cbOralMethAmp.Text = "Oral Methylenedioxy-Amphetamines"
        Me.cbOralMethAmp.UseVisualStyleBackColor = True
        '
        'cbOralMethy
        '
        Me.cbOralMethy.AutoSize = True
        Me.cbOralMethy.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMethy.Location = New System.Drawing.Point(299, 301)
        Me.cbOralMethy.Name = "cbOralMethy"
        Me.cbOralMethy.Size = New System.Drawing.Size(158, 18)
        Me.cbOralMethy.TabIndex = 82
        Me.cbOralMethy.Text = "Oral Methylphenidate"
        Me.cbOralMethy.UseVisualStyleBackColor = True
        '
        'cbOralOpi
        '
        Me.cbOralOpi.AutoSize = True
        Me.cbOralOpi.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOpi.Location = New System.Drawing.Point(586, 68)
        Me.cbOralOpi.Name = "cbOralOpi"
        Me.cbOralOpi.Size = New System.Drawing.Size(100, 18)
        Me.cbOralOpi.TabIndex = 83
        Me.cbOralOpi.Text = "Oral Opiates"
        Me.cbOralOpi.UseVisualStyleBackColor = True
        '
        'cbOralOpiAnal
        '
        Me.cbOralOpiAnal.AutoSize = True
        Me.cbOralOpiAnal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOpiAnal.Location = New System.Drawing.Point(586, 92)
        Me.cbOralOpiAnal.Name = "cbOralOpiAnal"
        Me.cbOralOpiAnal.Size = New System.Drawing.Size(238, 18)
        Me.cbOralOpiAnal.TabIndex = 84
        Me.cbOralOpiAnal.Text = "Oral Opioids and Opiate Analogues"
        Me.cbOralOpiAnal.UseVisualStyleBackColor = True
        '
        'cbOralOxy
        '
        Me.cbOralOxy.AutoSize = True
        Me.cbOralOxy.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOxy.Location = New System.Drawing.Point(586, 118)
        Me.cbOralOxy.Name = "cbOralOxy"
        Me.cbOralOxy.Size = New System.Drawing.Size(122, 18)
        Me.cbOralOxy.TabIndex = 85
        Me.cbOralOxy.Text = "Oral Oxycodone"
        Me.cbOralOxy.UseVisualStyleBackColor = True
        '
        'cbOralPhen
        '
        Me.cbOralPhen.AutoSize = True
        Me.cbOralPhen.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPhen.Location = New System.Drawing.Point(586, 145)
        Me.cbOralPhen.Name = "cbOralPhen"
        Me.cbOralPhen.Size = New System.Drawing.Size(136, 18)
        Me.cbOralPhen.TabIndex = 86
        Me.cbOralPhen.Text = "Oral Phencyclidine"
        Me.cbOralPhen.UseVisualStyleBackColor = True
        '
        'cbOralPro
        '
        Me.cbOralPro.AutoSize = True
        Me.cbOralPro.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPro.Location = New System.Drawing.Point(586, 199)
        Me.cbOralPro.Name = "cbOralPro"
        Me.cbOralPro.Size = New System.Drawing.Size(143, 18)
        Me.cbOralPro.TabIndex = 87
        Me.cbOralPro.Text = "Oral Propoxyphene"
        Me.cbOralPro.UseVisualStyleBackColor = True
        '
        'cbOralSkel
        '
        Me.cbOralSkel.AutoSize = True
        Me.cbOralSkel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralSkel.Location = New System.Drawing.Point(586, 250)
        Me.cbOralSkel.Name = "cbOralSkel"
        Me.cbOralSkel.Size = New System.Drawing.Size(211, 18)
        Me.cbOralSkel.TabIndex = 88
        Me.cbOralSkel.Text = "Oral Skeletal Muscle Relaxants"
        Me.cbOralSkel.UseVisualStyleBackColor = True
        '
        'cbOralTap
        '
        Me.cbOralTap.AutoSize = True
        Me.cbOralTap.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTap.Location = New System.Drawing.Point(586, 277)
        Me.cbOralTap.Name = "cbOralTap"
        Me.cbOralTap.Size = New System.Drawing.Size(123, 18)
        Me.cbOralTap.TabIndex = 89
        Me.cbOralTap.Text = "Oral Tapentadol"
        Me.cbOralTap.UseVisualStyleBackColor = True
        '
        'cbOralMtd
        '
        Me.cbOralMtd.AutoSize = True
        Me.cbOralMtd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMtd.Location = New System.Drawing.Point(299, 250)
        Me.cbOralMtd.Name = "cbOralMtd"
        Me.cbOralMtd.Size = New System.Drawing.Size(124, 18)
        Me.cbOralMtd.TabIndex = 90
        Me.cbOralMtd.Text = "Oral Methadone"
        Me.cbOralMtd.UseVisualStyleBackColor = True
        '
        'cbOralTram
        '
        Me.cbOralTram.AutoSize = True
        Me.cbOralTram.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTram.Location = New System.Drawing.Point(586, 301)
        Me.cbOralTram.Name = "cbOralTram"
        Me.cbOralTram.Size = New System.Drawing.Size(110, 18)
        Me.cbOralTram.TabIndex = 91
        Me.cbOralTram.Text = "Oral Tramadol"
        Me.cbOralTram.UseVisualStyleBackColor = True
        '
        'lblAcct
        '
        Me.lblAcct.AutoSize = True
        Me.lblAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcct.Location = New System.Drawing.Point(247, 27)
        Me.lblAcct.Name = "lblAcct"
        Me.lblAcct.Size = New System.Drawing.Size(0, 19)
        Me.lblAcct.TabIndex = 92
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(393, 387)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(78, 28)
        Me.btnPrint.TabIndex = 93
        Me.btnPrint.Text = "PRINT"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'dtpProfDt
        '
        Me.dtpProfDt.CalendarFont = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProfDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProfDt.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProfDt.Location = New System.Drawing.Point(494, 337)
        Me.dtpProfDt.Name = "dtpProfDt"
        Me.dtpProfDt.Size = New System.Drawing.Size(148, 27)
        Me.dtpProfDt.TabIndex = 95
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(374, 339)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 19)
        Me.Label2.TabIndex = 94
        Me.Label2.Text = "Profile Date:"
        '
        'PrintDoc
        '
        '
        'lstData
        '
        Me.lstData.Font = New System.Drawing.Font("Courier New", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstData.FormattingEnabled = True
        Me.lstData.ItemHeight = 17
        Me.lstData.Location = New System.Drawing.Point(625, 11)
        Me.lstData.Margin = New System.Windows.Forms.Padding(2)
        Me.lstData.Name = "lstData"
        Me.lstData.Size = New System.Drawing.Size(40, 4)
        Me.lstData.TabIndex = 96
        Me.lstData.Visible = False
        '
        'cbXREF
        '
        Me.cbXREF.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbXREF.Location = New System.Drawing.Point(38, 307)
        Me.cbXREF.Name = "cbXREF"
        Me.cbXREF.Size = New System.Drawing.Size(209, 57)
        Me.cbXREF.TabIndex = 97
        Me.cbXREF.Text = "Medications - presumptive positive (XREF)"
        Me.cbXREF.UseVisualStyleBackColor = True
        '
        'cbOralAlk
        '
        Me.cbOralAlk.AutoSize = True
        Me.cbOralAlk.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAlk.Location = New System.Drawing.Point(29, 68)
        Me.cbOralAlk.Name = "cbOralAlk"
        Me.cbOralAlk.Size = New System.Drawing.Size(181, 18)
        Me.cbOralAlk.TabIndex = 98
        Me.cbOralAlk.Text = "Oral Alkaloids Unspecified"
        Me.cbOralAlk.UseVisualStyleBackColor = True
        '
        'cbOralADSer
        '
        Me.cbOralADSer.AutoSize = True
        Me.cbOralADSer.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralADSer.Location = New System.Drawing.Point(29, 118)
        Me.cbOralADSer.Name = "cbOralADSer"
        Me.cbOralADSer.Size = New System.Drawing.Size(241, 18)
        Me.cbOralADSer.TabIndex = 99
        Me.cbOralADSer.Text = "Oral Antidepressants, Serotonergic"
        Me.cbOralADSer.UseVisualStyleBackColor = True
        '
        'cbOralAntiTri
        '
        Me.cbOralAntiTri.AutoSize = True
        Me.cbOralAntiTri.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiTri.Location = New System.Drawing.Point(29, 145)
        Me.cbOralAntiTri.Name = "cbOralAntiTri"
        Me.cbOralAntiTri.Size = New System.Drawing.Size(208, 18)
        Me.cbOralAntiTri.TabIndex = 100
        Me.cbOralAntiTri.Text = "Oral Antidepressants, Tricyclic"
        Me.cbOralAntiTri.UseVisualStyleBackColor = True
        '
        'cbOralAnti
        '
        Me.cbOralAnti.AutoSize = True
        Me.cbOralAnti.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAnti.Location = New System.Drawing.Point(29, 172)
        Me.cbOralAnti.Name = "cbOralAnti"
        Me.cbOralAnti.Size = New System.Drawing.Size(154, 18)
        Me.cbOralAnti.TabIndex = 101
        Me.cbOralAnti.Text = "Oral Antidepressants"
        Me.cbOralAnti.UseVisualStyleBackColor = True
        '
        'cbOralAntiEpi
        '
        Me.cbOralAntiEpi.AutoSize = True
        Me.cbOralAntiEpi.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiEpi.Location = New System.Drawing.Point(29, 199)
        Me.cbOralAntiEpi.Name = "cbOralAntiEpi"
        Me.cbOralAntiEpi.Size = New System.Drawing.Size(137, 18)
        Me.cbOralAntiEpi.TabIndex = 102
        Me.cbOralAntiEpi.Text = "Oral Antiepileptics"
        Me.cbOralAntiEpi.UseVisualStyleBackColor = True
        '
        'cbOralAntiPsych
        '
        Me.cbOralAntiPsych.AutoSize = True
        Me.cbOralAntiPsych.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiPsych.Location = New System.Drawing.Point(29, 226)
        Me.cbOralAntiPsych.Name = "cbOralAntiPsych"
        Me.cbOralAntiPsych.Size = New System.Drawing.Size(144, 18)
        Me.cbOralAntiPsych.TabIndex = 103
        Me.cbOralAntiPsych.Text = "Oral Antipsychotics"
        Me.cbOralAntiPsych.UseVisualStyleBackColor = True
        '
        'cbOralGaba
        '
        Me.cbOralGaba.AutoSize = True
        Me.cbOralGaba.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralGaba.Location = New System.Drawing.Point(299, 199)
        Me.cbOralGaba.Name = "cbOralGaba"
        Me.cbOralGaba.Size = New System.Drawing.Size(196, 18)
        Me.cbOralGaba.TabIndex = 104
        Me.cbOralGaba.Text = "Oral Gabapentin, Non-Blood"
        Me.cbOralGaba.UseVisualStyleBackColor = True
        '
        'cbOralPregaba
        '
        Me.cbOralPregaba.AutoSize = True
        Me.cbOralPregaba.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPregaba.Location = New System.Drawing.Point(586, 172)
        Me.cbOralPregaba.Name = "cbOralPregaba"
        Me.cbOralPregaba.Size = New System.Drawing.Size(118, 18)
        Me.cbOralPregaba.TabIndex = 105
        Me.cbOralPregaba.Text = "Oral Pregabalin"
        Me.cbOralPregaba.UseVisualStyleBackColor = True
        '
        'cbOralSedHyp
        '
        Me.cbOralSedHyp.AutoSize = True
        Me.cbOralSedHyp.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralSedHyp.Location = New System.Drawing.Point(586, 223)
        Me.cbOralSedHyp.Name = "cbOralSedHyp"
        Me.cbOralSedHyp.Size = New System.Drawing.Size(172, 18)
        Me.cbOralSedHyp.TabIndex = 106
        Me.cbOralSedHyp.Text = "Oral Sedative Hypnotics"
        Me.cbOralSedHyp.UseVisualStyleBackColor = True
        '
        'frmAcctOCs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(853, 438)
        Me.Controls.Add(Me.cbOralSedHyp)
        Me.Controls.Add(Me.cbOralPregaba)
        Me.Controls.Add(Me.cbOralGaba)
        Me.Controls.Add(Me.cbOralAntiPsych)
        Me.Controls.Add(Me.cbOralAntiEpi)
        Me.Controls.Add(Me.cbOralAnti)
        Me.Controls.Add(Me.cbOralAntiTri)
        Me.Controls.Add(Me.cbOralADSer)
        Me.Controls.Add(Me.cbOralAlk)
        Me.Controls.Add(Me.cbXREF)
        Me.Controls.Add(Me.lstData)
        Me.Controls.Add(Me.dtpProfDt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.lblAcct)
        Me.Controls.Add(Me.cbOralTram)
        Me.Controls.Add(Me.cbOralMtd)
        Me.Controls.Add(Me.cbOralTap)
        Me.Controls.Add(Me.cbOralSkel)
        Me.Controls.Add(Me.cbOralPro)
        Me.Controls.Add(Me.cbOralPhen)
        Me.Controls.Add(Me.cbOralOxy)
        Me.Controls.Add(Me.cbOralOpiAnal)
        Me.Controls.Add(Me.cbOralOpi)
        Me.Controls.Add(Me.cbOralMethy)
        Me.Controls.Add(Me.cbOralMethAmp)
        Me.Controls.Add(Me.cbOralHero)
        Me.Controls.Add(Me.cbOralFent)
        Me.Controls.Add(Me.cbOralCOC)
        Me.Controls.Add(Me.cbOralTHC)
        Me.Controls.Add(Me.cbOralBup)
        Me.Controls.Add(Me.cbOralBenz)
        Me.Controls.Add(Me.cbOralAmp)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmAcctOCs"
        Me.Text = "frmAcctOCs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtAcct As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnSave As Button
    Friend WithEvents btnClear As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents cbOralAmp As CheckBox
    Friend WithEvents cbOralBenz As CheckBox
    Friend WithEvents cbOralBup As CheckBox
    Friend WithEvents cbOralTHC As CheckBox
    Friend WithEvents cbOralCOC As CheckBox
    Friend WithEvents cbOralFent As CheckBox
    Friend WithEvents cbOralHero As CheckBox
    Friend WithEvents cbOralMethAmp As CheckBox
    Friend WithEvents cbOralMethy As CheckBox
    Friend WithEvents cbOralOpi As CheckBox
    Friend WithEvents cbOralOpiAnal As CheckBox
    Friend WithEvents cbOralOxy As CheckBox
    Friend WithEvents cbOralPhen As CheckBox
    Friend WithEvents cbOralPro As CheckBox
    Friend WithEvents cbOralSkel As CheckBox
    Friend WithEvents cbOralTap As CheckBox
    Friend WithEvents cbOralMtd As CheckBox
    Friend WithEvents cbOralTram As CheckBox
    Friend WithEvents lblAcct As Label
    Friend WithEvents btnPrint As Button
    Friend WithEvents dtpProfDt As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents PrintDoc As Printing.PrintDocument
    Friend WithEvents lstData As ListBox
    Friend WithEvents cbXREF As CheckBox
    Friend WithEvents cbOralAlk As CheckBox
    Friend WithEvents cbOralADSer As CheckBox
    Friend WithEvents cbOralAntiTri As CheckBox
    Friend WithEvents cbOralAnti As CheckBox
    Friend WithEvents cbOralAntiEpi As CheckBox
    Friend WithEvents cbOralAntiPsych As CheckBox
    Friend WithEvents cbOralGaba As CheckBox
    Friend WithEvents cbOralPregaba As CheckBox
    Friend WithEvents cbOralSedHyp As CheckBox
End Class
