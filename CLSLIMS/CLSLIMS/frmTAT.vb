﻿Imports System.Data
Imports System.Data.SqlClient
Public Class frmTAT
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs)
        lstStats.Items.Clear()
        btnCSV.Enabled = False
        Dim Cnt As Integer = 0, Avg As Double = 0.00, StdDev As Double = 0.00, Min As Integer = 0, Max As Integer = 0
        Dim Sdt As String = CLSLIMS.Piece(dtpStart.Text, "/", 3) & CLSLIMS.Piece(dtpStart.Text, "/", 1) & CLSLIMS.Piece(dtpStart.Text, "/", 2)
        Dim Edt As String = CLSLIMS.Piece(dtpEnd.Text, "/", 3) & CLSLIMS.Piece(dtpEnd.Text, "/", 1) & CLSLIMS.Piece(dtpEnd.Text, "/", 2)
        Dim Sel = "select COUNT(distinct pspecno) as Count, " &
                  "AVG(datediff(hour,cast(substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4)+'  '+substring(psrcvdt,9,2)+':'+substring(psrcvdt,11,2) as DATETIME), " &
                      "cast(substring(psrptdt,5,2)+'/'+substring(psrptdt,7,2)+'/'+substring(psrptdt,1,4)+'  '+substring(psrptdt,9,2)+':'+substring(psrptdt,11,2) as DATETIME))) as Avg, " &
                  "STDEV(datediff(hour,cast(substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4)+'  '+substring(psrcvdt,9,2)+':'+substring(psrcvdt,11,2) as DATETIME), " &
                      "cast(substring(psrptdt,5,2)+'/'+substring(psrptdt,7,2)+'/'+substring(psrptdt,1,4)+'  '+substring(psrptdt,9,2)+':'+substring(psrptdt,11,2) as DATETIME))) as StdDev, " &
                  "MIN(datediff(hour,cast(substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4)+'  '+substring(psrcvdt,9,2)+':'+substring(psrcvdt,11,2) as DATETIME), " &
                      "cast(substring(psrptdt,5,2)+'/'+substring(psrptdt,7,2)+'/'+substring(psrptdt,1,4)+'  '+substring(psrptdt,9,2)+':'+substring(psrptdt,11,2) as DATETIME))) as Min, " &
                  "MAX(datediff(hour,cast(substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4)+'  '+substring(psrcvdt,9,2)+':'+substring(psrcvdt,11,2) as DATETIME), " &
                      "cast(substring(psrptdt,5,2)+'/'+substring(psrptdt,7,2)+'/'+substring(psrptdt,1,4)+'  '+substring(psrptdt,9,2)+':'+substring(psrptdt,11,2) as DATETIME))) as Max " &
                  "From PSPEC " &
                  "Where psact=1 and psrptdt!='' and substring(psrcvdt,1,8) Between '" & Sdt & "' And '" & Edt & "' " &
                  "And pspecno Not in (select a.pspecno from pspec a, poc b where a.pspecno=b.pspecno And psact=1 And a.adsq1=b.adsq1 And oc='990000') " &
                  "And pspecno not in (select pspecno from psstat where statcd='AFF HOLD') " &
                  "And pspecno not in (select a.pspecno from pspec a, pord b Where a.pord=b.pord and a.poefdt=b.poefdt and psact=1 and poact=1 and acct in ('PTSAMPLES','220','S220','HOUSE','999'))"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            Cnt = rs(0)
            Avg = rs(1)
            StdDev = rs(2)
            Min = rs(3)
            Max = rs(4)
        End If
        rs.Close()
        Conn.Close()
        lstStats.Items.Add("         Time Period: " & dtpStart.Text & " - " & dtpEnd.Text)
        lstStats.Items.Add("      Specimen Count: " & Format(Cnt, "###,##0"))
        lstStats.Items.Add("Average TAT in Hours: " & Format(Avg, "##0.0"))
        lstStats.Items.Add("  Standard Deviation: " & Format(StdDev, "##0.00"))
        lstStats.Items.Add("Minimum TAT in Hours: " & Min)
        lstStats.Items.Add("Maximum TAT in Hours: " & Format(Max, "###,##0"))
        btnCSV.Enabled = True
    End Sub

    Private Sub frmTAT_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpStart.Format = DateTimePickerFormat.Custom
        dtpStart.CustomFormat = "MM/dd/yyyy"
        dtpStart.Text = DateAdd(DateInterval.Day, -60, Now)
        dtpEnd.Format = DateTimePickerFormat.Custom
        dtpEnd.CustomFormat = "MM/dd/yyyy"
        dtpEnd.Text = DateAdd(DateInterval.Day, -30, Now)

    End Sub

    Private Sub btnCSV_Click(sender As Object, e As EventArgs) Handles btnCSV.Click
        lstStats.Items.Add("Creating file . . .") : lstStats.Refresh()
        Dim txtFileName As String = ""
        Dim Sdt As String = CLSLIMS.Piece(dtpStart.Text, "/", 3) & CLSLIMS.Piece(dtpStart.Text, "/", 1) & CLSLIMS.Piece(dtpStart.Text, "/", 2)
        Dim Edt As String = CLSLIMS.Piece(dtpEnd.Text, "/", 3) & CLSLIMS.Piece(dtpEnd.Text, "/", 1) & CLSLIMS.Piece(dtpEnd.Text, "/", 2)
        SaveFileDialog1.Title = "Save File Dialog"
        SaveFileDialog1.InitialDirectory = "C:\CLSLIMS\"
        SaveFileDialog1.Filter = "CSV files (*.CSV)|*.CSV|CSV files (*.CSV)|*.CSV"
        SaveFileDialog1.FilterIndex = 2
        SaveFileDialog1.RestoreDirectory = True

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            txtFileName = SaveFileDialog1.FileName
        End If
        Dim file As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(txtFileName, False)
        file.WriteLine("Specimen ID,Date Reported, Date Received,Hours")
        Dim Sel = "select pspecno,cast(substring((select min(rptdt) from prpt Where pspecno=n.pspecno and acct!='999'),5,2)+'/'+substring((select min(rptdt) from prpt Where pspecno=n.pspecno and acct!='999'),7,2)+'/'+substring((select min(rptdt) from prpt Where pspecno=n.pspecno and acct!='999'),1,4)+'  '+substring((select min(rptdt) from prpt Where pspecno=n.pspecno and acct!='999'),9,2)+':'+substring((select min(rptdt) from prpt Where pspecno=n.pspecno and acct!='999'),11,2) as DATETIME) as Report," &
                  "cast(substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4)+'  '+substring(psrcvdt,9,2)+':'+substring(psrcvdt,11,2) as DATETIME) as Receive " &
                  "From PSPEC N " &
                  "Where psact=1 And psrptdt!='' and substring(psrcvdt,1,8) Between '" & Sdt & "' And '" & Edt & "' " &
                  "And pspecno Not in (select a.pspecno from pspec a, poc b where a.pspecno=b.pspecno And psact=1 And a.adsq1=b.adsq1 And oc='990000') " &
                  "And pspecno not in (select pspecno from psstat where statcd='AFF HOLD') " &
                  "And pspecno not in (select a.pspecno from pspec a, pord b Where a.pord=b.pord and a.poefdt=b.poefdt and psact=1 and poact=1 and acct in ('PTSAMPLES','220','S220','HOUSE','999'))"
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn), RecvDt As Date = Nothing, RptDt As Date = Nothing, x As String = "", DtDiff As Integer = 0, HrDiff As Integer = 0, RcvDOW As Integer = 0, RptDOW As Integer = 0
        Dim Cnt As Integer = 0, Max As Integer = 0, Min As Integer = 999, Avg As Integer = 0, TotDiff As Integer = 0
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Do While rs.Read
            RecvDt = rs(2) : RptDt = rs(1) : DtDiff = DateDiff(DateInterval.Day, RecvDt, RptDt)
            HrDiff = DateDiff(DateInterval.Hour, RecvDt, RptDt) : RcvDOW = RecvDt.DayOfWeek : RptDOW = RptDt.DayOfWeek
            If rs(0) = "1311630" Then
                x = x
            End If
            If DtDiff >= 21 Then
                HrDiff = HrDiff - 144
            ElseIf DtDiff >= 14 Then
                HrDiff = HrDiff - 96
            ElseIf DtDiff >= 7 Then
                HrDiff = HrDiff - 48
            ElseIf RcvDOW = 2 And (RptDOW = 1 Or RptDOW = 2) And HrDiff > 23 Then
                HrDiff = HrDiff - 48
            ElseIf RcvDOW = 3 And (RptDOW = 1 Or RptDOW = 2 Or RptDOW = 3) And HrDiff > 23 Then
                HrDiff = HrDiff - 48
            ElseIf RcvDOW = 4 And (RptDOW = 1 Or RptDOW = 2 Or RptDOW = 3 Or RptDOW = 4) And HrDiff > 23 Then
                HrDiff = HrDiff - 48
            ElseIf RcvDOW = 5 And (RptDOW = 1 Or RptDOW = 2 Or RptDOW = 3 Or RptDOW = 4 Or RptDOW = 5) And HrDiff > 23 Then
                HrDiff = HrDiff - 48
            End If
            If HrDiff > Max Then Max = HrDiff
            If HrDiff < Min Then Min = HrDiff
            Cnt = Cnt + 1
            TotDiff = TotDiff + HrDiff
            If rs(0) = "1311630" Then
                x = x
            End If
            file.WriteLine(rs(0) & "," & RecvDt & "," & RptDt & "," & HrDiff)
        Loop
        rs.Close()
        Conn.Close()
        file.Close()
        Avg = TotDiff / Cnt
        lstStats.Items.Add("         Time Period: " & dtpStart.Text & " - " & dtpEnd.Text)
        lstStats.Items.Add("      Specimen Count: " & Format(Cnt, "###,##0"))
        lstStats.Items.Add("     Minimum TAT Hrs: " & Format(Min, "###,##0"))
        lstStats.Items.Add("     Maximum TAT Hrs: " & Format(Max, "###,##0"))
        lstStats.Items.Add("     Average TAT Hrs: " & Format(Avg, "###,##0"))
        lstStats.Items.Add("Complete.")
    End Sub
End Class