﻿Public Class frmSOEGridInq
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub
    Public Sub ClearCLick()
        cbOralAmp.Checked = False
        cbOralAlk.Checked = False
        cbOralBenz.Checked = False
        cbOralBup.Checked = False
        cbOralCOC.Checked = False
        cbOralFent.Checked = False
        cbOralHero.Checked = False
        cbOralMethAmp.Checked = False
        cbOralMethy.Checked = False
        cbOralMtd.Checked = False
        cbOralOpi.Checked = False
        cbOralOpiAnal.Checked = False
        cbOralOxy.Checked = False
        cbOralPhen.Checked = False
        cbOralPro.Checked = False
        cbOralSkel.Checked = False
        cbOralTap.Checked = False
        cbOralTHC.Checked = False
        cbOralTram.Checked = False
        cbOralGaba.Checked = False
        cbOralPregaba.Checked = False
        cbOralSedHyp.Checked = False

    End Sub

    Private Sub frmSOEGridInq_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For i = 0 To SpcInq.lstOC.Items.Count - 1
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S352000" Then cbOralAmp.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S361000" Then cbOralBenz.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S363000" Then cbOralBup.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S306020" Then cbOralCOC.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S369000" Then cbOralFent.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S371000" Then cbOralHero.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S375000" Then cbOralMethAmp.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S377000" Then cbOralMethy.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S305040" Then cbOralMtd.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S380000" Then cbOralOpi.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S382000" Then cbOralOpiAnal.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S383000" Then cbOralOxy.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S385000" Then cbOralPhen.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S388000" Then cbOralPro.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S391000" Then cbOralSkel.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S393000" Then cbOralTap.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S365000" Then cbOralTHC.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S305120" Then cbOralTram.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S349000" Then cbOralAlk.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S354000" Then cbOralADSer.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S355000" Then cbOralAntiTri.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S356000" Then cbOralAnti.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S357000" Then cbOralAntiEpi.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S358000" Then cbOralAntiPsych.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S370000" Then cbOralGaba.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S387000" Then cbOralPregaba.Checked = True
            If CLSLIMS.Piece(SpcInq.lstOC.Items(i), " ", 1) = "S390000" Then cbOralSedHyp.Checked = True
        Next i
    End Sub

End Class