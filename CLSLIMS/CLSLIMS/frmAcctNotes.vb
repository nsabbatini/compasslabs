﻿Imports System.Data
Imports System.Data.SqlClient
Imports CLSLIMS
Public Class frmAcctNotes
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim ComDt As String = "", txtCom As String = "", Cmt() As String, iSpc As Integer = 0, i As Integer = 0
        Dim Cmd As New SqlCommand
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        'Check to see if user logged in
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        'Check to see if user is SALESSUPPORT  Christy, Amber, Pam, Jackie, Paula 5/22/2018
        If CLSLIMS.GetDept(CLSLIMS.UID) <> "SALESUPPORT" Then
            If CLSLIMS.UID <> "NED.SABBATINI" Then
                Me.Close()
                Me.Dispose()
            End If
        End If

        frmIntCom.ShowDialog()

        If CLSLIMS.IntText <> "" Then
            'Insert any internal comments
            Conn.Open()
            Cmd.Connection = Conn
            ComDt = CLSLIMS.CDT
            Cmd.CommandText = "Insert into PSCMT values ('" & CLSLIMS.AcctNum & "','I','" & ComDt & "',1,'" & CLSLIMS.UID & "','ACCT','')"
            Cmd.ExecuteNonQuery()
            txtCom = Replace(CLSLIMS.IntText, "'", " ")
            i = 1
            Cmt = Split(txtCom, vbCrLf)
            For j = 0 To UBound(Cmt)
                Do While Len(Cmt(j)) > 80
                    iSpc = CLSLIMS.FirstSpace(Mid(Cmt(j), 1, 80))
                    Cmd.CommandText = "Insert Into PSCMTI Values ('" & CLSLIMS.AcctNum & "','I','" & ComDt & "'," & i & ",'" & Mid(Cmt(j), 1, iSpc) & "')"
                    If Mid(Cmt(j), 1, iSpc) <> "" Then
                        Cmd.ExecuteNonQuery()
                    End If
                    Cmt(j) = Mid(Cmt(j), iSpc + 1)
                    i = i + 1
                Loop
                Cmd.CommandText = "Insert Into PSCMTI Values ('" & CLSLIMS.AcctNum & "','I','" & ComDt & "'," & i & ",'" & Cmt(j) & "')"
                If Cmt(j) <> "" Then
                    Cmd.ExecuteNonQuery()
                End If
                i = i + 1
            Next j
            Conn.Close()
            LoadComments()
        End If

    End Sub

    Private Sub frmAcctNotes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadComments()
    End Sub
    Public Sub LoadComments()
        Dim Dt As String = "", Tm As String = "", Usr As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "select pscmttxt,a.PSCMTEFDT,PSCMTUID from pscmt a, pscmti b where a.pspecno = b.pspecno and a.pscmttype=b.pscmttype and a.pscmttype='I' and a.pscmtefdt=b.pscmtefdt " &
              "and a.pspecno='" & CLSLIMS.AcctNum & "' and pscmtact=1 and a.pscmtorig='ACCT' order by a.pscmtefdt,pscmtix"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        lstComments.Items.Clear()
        Do While rs.Read
            Dt = rs(1) : Tm = Mid(Dt, 9, 4) : Dt = Mid(Dt, 5, 2) & "/" & Mid(Dt, 7, 2) & "/" & Mid(Dt, 1, 4) : Tm = Mid(Tm, 1, 2) & ":" & Mid(Tm, 3, 2)
            lstComments.Items.Add(Dt & " " & Tm & " " & CLSLIMS.LJ(rs(0), 80) & " " & rs(2))
        Loop
        rs.Close()
    End Sub
End Class