﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class UpdIns

    Public MC As Boolean = False, MD As Boolean = False, PIPI As Boolean = False, PUID As String = "", PEFDT As String = ""
    Public LastName As String = "", FirstName As String = "", MI As String = ""
    Private Sub txtSpecNo_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSpecNo.KeyUp
        If e.KeyCode = Keys.Enter Then

            If txtSpecNo.Text = "" Then
                Exit Sub
            End If
            FillData(txtSpecNo.Text)
        End If
    End Sub
    Public Sub ClearFields()
        lblPatName.Text = ""
        lblDOB.Text = ""
        lblSpecNo.Text = ""
        txtAddr.Text = ""
        txtAddr2.Text = ""
        txtCity.Text = ""
        txtMCNo.Text = ""
        txtMDState.Text = ""
        txtPhone.Text = ""
        txtSpecNo.Text = ""
        txtState.Text = ""
        txtZIP.Text = ""
        rbMC1.Checked = False : rbMC2.Checked = False : rbMC3.Checked = False
        rbMD1.Checked = False : rbMD2.Checked = False : rbMD3.Checked = False
        rbTP1.Checked = False : rbTP2.Checked = False : rbTP3.Checked = False

        MC = False : MD = False : PIPI = False : PUID = "" : PEFDT = "" : LastName = "" : FirstName = "" : MI = ""
        tbDemo.Focus()
    End Sub
    Public Sub FillData(ByVal SpecNo As String)
        ClearFields()
        If Not IsNumeric(SpecNo) And Mid(SpecNo, 1, 1) <> "Z" Then
            lstPatNameSearch.Height = 340
            lstPatNameSearch.Width = 955
            lstPatNameSearch.Location = New Drawing.Point(123, 45)
            lstPatNameSearch.Visible = True
            lstPatNameSearch.BringToFront()
            txtSpecNo.Text = GetSpecNoFromName(SpecNo)
            Exit Sub
        End If
        lblSpecNo.Text = SpecNo

        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "select plname,pfname,pdob,pmname,sex,pad1,pad2,pcity,pstate,pzip,pphone,a.puid,a.pefdt from pdem a, pspec b where a.puid=b.puid and a.pefdt=b.pefdt and pspecno='" & SpecNo & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            PUID = rs(11) : PEFDT = rs(12)
            LastName = rs(0) : FirstName = rs(1) : MI = rs(3)
            cmbSex.Text = rs(4) : lblPatName.Text = LastName & ", " & FirstName & " " & MI : lblDOB.Text = Mid(rs(2), 5, 2) & "/" & Mid(rs(2), 7, 2) & "/" & Mid(rs(2), 1, 4)
            txtAddr.Text = rs(5) : txtAddr2.Text = rs(6) : txtCity.Text = rs(7) : txtState.Text = rs(8) : txtZIP.Text = rs(9) : txtPhone.Text = rs(10)
        End If
        rs.Close()
        Cmd.CommandText = "Select PIMCHIC,PIMCORD From PIMC a, PSPEC b Where a.puid=b.puid and a.pefdt=b.pefdt and pspecno='" & SpecNo & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            txtMCNo.Text = rs(0)
            MC = True
            If rs(1) = 1 Then
                rbMC1.Checked = True
            ElseIf rs(1) = 2 Then
                rbMC2.Checked = True
            ElseIf rs(1) = 3 Then
                rbMC3.Checked = True
            Else
                rbMC1.Checked = False : rbMC2.Checked = False : rbMC3.Checked = False
            End If
        End If
        rs.Close()
        Cmd.CommandText = "Select PIMDRID, PIMDSTATE, PIMDORD From PIMD a, PSPEC b Where a.puid=b.puid and a.pefdt=b.pefdt and pspecno='" & SpecNo & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            txtMDNo.Text = rs(0) : txtMDState.Text = rs(1)
            MD = True
            If rs(2) = 1 Then
                rbMD1.Checked = True
            ElseIf rs(2) = 2 Then
                rbMD2.Checked = True
            ElseIf rs(2) = 3 Then
                rbMD3.Checked = True
            Else
                rbMD1.Checked = False : rbMD2.Checked = False : rbMD3.Checked = False
            End If
        End If
        rs.Close()
        Cmd.CommandText = "Select a.BINS,BINSNAME,PIPOL,PIGRP,PICHREL,PICHSSN,PICHFNM,PICHLNM,PICHDOB,PICHAD1,PICHAD2,PICHCITY,PICHSTATE,PICHZIP,PICHPHONE,PIORD From PIPI a, PSPEC b, BINS c Where a.puid=b.puid and a.pefdt=b.pefdt and a.bins=c.bins and a.binsefdt=c.binsefdt and pspecno='" & SpecNo & "'"
        rs = Cmd.ExecuteReader
        If rs.Read Then
            cmbBINS.Text = rs(0) : lblInsName.Text = rs(1) : txtPolicyNo.Text = rs(2) : txtGroupNo.Text = rs(3) : cmbRel.Text = rs(4) : txtSSN.Text = rs(5)
            txtGLName.Text = rs(7) : txtGFName.Text = rs(6) : txtGDOB.Text = rs(8) : txtGAddr.Text = rs(9) : txtGAddr2.Text = rs(10) : txtGCity.Text = rs(11)
            txtGState.Text = rs(12) : txtGZIP.Text = rs(13) : txtGPhone.Text = rs(14)
            PIPI = True
            If rs(15) = 1 Then
                rbTP1.Checked = True
            ElseIf rs(15) = 2 Then
                rbTP2.Checked = True
            ElseIf rs(15) = 3 Then
                rbTP3.Checked = True
            Else
                rbTP1.Checked = False : rbTP2.Checked = False : rbTP3.Checked = False
            End If
        End If
        rs.Close()
        Cmd.Dispose()
        Conn.Close()
    End Sub

    Private Sub txt2Addr2_TextChanged(sender As Object, e As EventArgs) Handles txt2GAddr2.TextChanged

    End Sub

    Private Sub UpdIns_Load(sender As Object, e As EventArgs) Handles Me.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If

    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If rbMC1.Checked Then
            If rbMD1.Checked Then
                MsgBox("Two Primary selected. (MC,MD)")
                Exit Sub
            ElseIf rbTP1.Checked Then
                MsgBox("Two Primary selected. (MC,3rd)")
                Exit Sub
            End If
        End If
        If rbMD1.Checked Then
            If rbTP1.Checked Then
                MsgBox("Two Primary selected. (MD,3rd)")
                Exit Sub
            End If
        End If
        If rbMC2.Checked Then
            If rbMD2.Checked Then
                MsgBox("Two Secondary selected. (MC,MD)")
                Exit Sub
            ElseIf rbTP1.Checked Then
                MsgBox("Two Secondary selected. (MC,3rd)")
                Exit Sub
            End If
        End If
        If rbMD2.Checked Then
            If rbTP2.Checked Then
                MsgBox("Two Secondary selected. (MD,3rd)")
                Exit Sub
            End If
        End If
        If rbMC3.Checked Then
            If rbMD3.Checked Then
                MsgBox("Two Tertiary selected. (MC,MD)")
                Exit Sub
            ElseIf rbTP3.Checked Then
                MsgBox("Two Tertiary selected. (MC,3rd)")
                Exit Sub
            End If
        End If
        If rbMD3.Checked Then
            If rbTP3.Checked Then
                MsgBox("Two Tertiary selected. (MD,3rd)")
                Exit Sub
            End If
        End If

        If txtMCNo.Text <> "" And Not rbMC1.Checked And Not rbMC2.Checked And Not rbMC3.Checked Then
            MsgBox("Medicare position not indicated.")
            tbMC.Focus()
            Exit Sub
        End If
        If txtMDNo.Text <> "" And Not rbMD1.Checked And Not rbMD2.Checked And Not rbMD3.Checked Then
            MsgBox("Medicaid position not indicated.")
            tbMC.Focus()
            Exit Sub
        End If
        If txtGLName.Text <> "" And Not rbTP1.Checked And Not rbTP2.Checked And Not rbTP3.Checked Then
            MsgBox("3rd Party position not indicated.")
            tbMC.Focus()
            Exit Sub
        End If

        Dim NewPEFDT As String = Format(Now, "yyyyMMddHHmmss")
        Dim SQL As String = "", pord As String = "", poefdt As String = "", MCP As Integer = 0, MDP As Integer = 0, TPP As Integer = 0
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        If rbMC1.Checked Then
            MCP = 1
        ElseIf rbMC2.Checked Then
            MCP = 2
        ElseIf rbMC3.Checked Then
            MCP = 3
        End If
        If rbMD1.Checked Then
            MDP = 1
        ElseIf rbMD2.Checked Then
            MDP = 2
        ElseIf rbMD3.Checked Then
            MDP = 3
        End If
        If rbTP1.Checked Then
            TPP = 1
        ElseIf rbTP2.Checked Then
            TPP = 2
        ElseIf rbTP3.Checked Then
            TPP = 3
        End If

        If MC Or txtMCNo.Text <> "" And MCP <> 0 Then
            SQL = "Insert into PIMC  Select '" & PUID & "','" & NewPEFDT & "',0," & MCP & ",'" & txtMCNo.Text & "','','' From PIMC Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
            Cmd.CommandText = SQL
            Cmd.ExecuteNonQuery()
        End If
        If MD Or txtMDNo.Text <> "" And MDP <> 0 Then
            SQL = "Insert into PIMD  Select '" & PUID & "','" & NewPEFDT & "',0," & MDP & ",'" & txtMDState.Text & "','" & txtMDNo.Text & "','','','' From PIMD Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
            Cmd.CommandText = SQL
            Cmd.ExecuteNonQuery()
        End If
        If PIPI Or txtGLName.Text <> "" And TPP <> 0 Then
            SQL = "Insert into PIPI  Select '" & PUID & "','" & NewPEFDT & "',0," & TPP & ",'" & cmbBINS.Text & "',(select BINSEFDT From BINS Where BINS='" & cmbBINS.Text & "' And BINSACT=1),'" & txtPolicyNo.Text & "','" & txtGroupNo.Text & "','" & cmbRel.Text & "','" & txtSSN.Text & "','" & txtGFName.Text & "','','" & txtGLName.Text & "','" & CLSLIMS.Piece(txtGDOB.Text, "/", 3) & CLSLIMS.Piece(txtGDOB.Text, "/", 1) & CLSLIMS.Piece(txtGDOB.Text, "/", 2) & "','" & txtGAddr.Text & "','" & txtGAddr2.Text & "','" & txtGCity.Text & "','','" & txtGState.Text & "','','" & txtGZIP.Text & "','" & txtGPhone.Text & "','','',''" & " From PIPI Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
            Cmd.CommandText = SQL
            Cmd.ExecuteNonQuery()
        End If
        SQL = "Insert into PDEM Select '" & PUID & "','" & NewPEFDT & "',1,'" & CLSLIMS.UID & "','" & FirstName & "','" & MI & "','" & LastName & "','" & CLSLIMS.Piece(lblDOB.Text, "/", 3) & CLSLIMS.Piece(lblDOB.Text, "/", 1) & CLSLIMS.Piece(lblDOB.Text, "/", 2) & "','','','','','','','','"
        SQL = SQL & cmbSex.Text & "','','" & txtAddr.Text & "','" & txtAddr2.Text & "','" & txtCity.Text & "','','" & txtState.Text & "','','" & txtZIP.Text & "','" & txtPhone.Text & "','','','','','','','','',''"
        Cmd.CommandText = SQL
        Cmd.ExecuteNonQuery()
        SQL = "Update PDEM Set PACT=0 Where PUID='" & PUID & "' And PEFDT='" & PEFDT & "'"
        Cmd.CommandText = SQL
        Cmd.ExecuteNonQuery()
        SQL = "Update PSPEC Set PEFDT='" & NewPEFDT & "' Where PSPECNO='" & lblSpecNo.Text & "' And PSACT=1"
        Cmd.CommandText = SQL
        Cmd.ExecuteNonQuery()
        SQL = "Select PORD,POEFDT From PSPEC Where PSPECNO='" & lblSpecNo.Text & "' And PSACT=1"
        Cmd.CommandText = SQL
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            pord = rs(0) : poefdt = rs(1)
        End If
        rs.Close()
        SQL = "Update PORD Set PEFDT='" & NewPEFDT & "' Where PORD='" & pord & "' And POEFDT='" & poefdt & "' And POACT=1"
        Cmd.CommandText = SQL
        Cmd.ExecuteNonQuery()
        Cmd.Dispose()
        Conn.Close()
        ClearFields()
        tbDemo.Focus()
        txtSpecNo.Focus()
    End Sub

    Public Function GetSpecNoFromName(ByVal PatName As String) As String

        Dim xSel As String
        xSel = "select pspecno,plname+','+pfname,c.acct,aname,req,substring(pscdta,5,2)+'/'+substring(pscdta,7,2)+'/'+substring(pscdta,3,2) " &
               "from pspec a, pord b, acct c, pdem d " &
               "where a.pord + a.poefdt = b.pord + b.poefdt And a.psact = 1 And b.poact = 1 " &
               "and a.puid + a.pefdt = d.puid + d.pefdt " &
               "and c.acct = b.acct and c.aefdt = b.aefdt " &
               "and pact=1 and plname+','+pfname like '" & PatName & "%' " &
               "order by plname+','+pfname"

        Dim strCol As String = "", strPatName As String = ""
        GetSpecNoFromName = ""
        If PatName = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strPatName = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 30) & Space(32 - Len(cRS(1))) & cRS(2) & Space(6 - Len(cRS(2))) & Mid(cRS(3), 1, 30) & Space(32 - Len(Mid(cRS(3), 1, 30))) & cRS(4) & Space(10 - Len(cRS(4))) & cRS(5)
                lstPatNameSearch.Items.Add(strPatName)
            Loop
            If lstPatNameSearch.Items.Count > 0 Then
                lstPatNameSearch.Visible = True
                lstPatNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using
    End Function

    Private Sub lstPatNameSearch_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lstPatNameSearch.MouseDoubleClick
        txtSpecNo.Text = CLSLIMS.Piece(lstPatNameSearch.SelectedItem.ToString, " ", 1)
        lstPatNameSearch.Visible = False
        FillData(txtSpecNo.Text)
    End Sub

    Private Sub UpdIns_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtSpecNo.Focus()
    End Sub
End Class