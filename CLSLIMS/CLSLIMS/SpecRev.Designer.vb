﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SpecRev
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SpecRev))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSpec = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblPatName = New System.Windows.Forms.Label()
        Me.lblSpecID = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblGender = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblDOB = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblCollDt = New System.Windows.Forms.Label()
        Me.lblRecvDt = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblSpecType = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblAcctInfo = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblMed = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lstOC = New System.Windows.Forms.ListBox()
        Me.PDF1 = New AxAcroPDFLib.AxAcroPDF()
        Me.grdCon = New System.Windows.Forms.DataGridView()
        Me.btnExit = New System.Windows.Forms.Button()
        CType(Me.PDF1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdCon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 20)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(196, 25)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Specimen Number:"
        '
        'txtSpec
        '
        Me.txtSpec.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSpec.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpec.Location = New System.Drawing.Point(228, 17)
        Me.txtSpec.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSpec.Name = "txtSpec"
        Me.txtSpec.Size = New System.Drawing.Size(240, 32)
        Me.txtSpec.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(26, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(128, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Patient Name:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(63, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 17)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Specimen ID:"
        '
        'lblPatName
        '
        Me.lblPatName.AutoSize = True
        Me.lblPatName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPatName.Location = New System.Drawing.Point(160, 72)
        Me.lblPatName.Name = "lblPatName"
        Me.lblPatName.Size = New System.Drawing.Size(122, 20)
        Me.lblPatName.TabIndex = 8
        Me.lblPatName.Text = "Last, First MI"
        '
        'lblSpecID
        '
        Me.lblSpecID.AutoSize = True
        Me.lblSpecID.Location = New System.Drawing.Point(164, 112)
        Me.lblSpecID.Name = "lblSpecID"
        Me.lblSpecID.Size = New System.Drawing.Size(56, 17)
        Me.lblSpecID.TabIndex = 9
        Me.lblSpecID.Text = "123456"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(94, 129)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 17)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Gender:"
        '
        'lblGender
        '
        Me.lblGender.AutoSize = True
        Me.lblGender.Location = New System.Drawing.Point(164, 129)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(54, 17)
        Me.lblGender.TabIndex = 11
        Me.lblGender.Text = "Female"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(63, 146)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Date of Birth:"
        '
        'lblDOB
        '
        Me.lblDOB.AutoSize = True
        Me.lblDOB.Location = New System.Drawing.Point(164, 146)
        Me.lblDOB.Name = "lblDOB"
        Me.lblDOB.Size = New System.Drawing.Size(80, 17)
        Me.lblDOB.TabIndex = 13
        Me.lblDOB.Text = "01/01/2015"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(50, 163)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 17)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Collected Date:"
        '
        'lblCollDt
        '
        Me.lblCollDt.AutoSize = True
        Me.lblCollDt.Location = New System.Drawing.Point(164, 163)
        Me.lblCollDt.Name = "lblCollDt"
        Me.lblCollDt.Size = New System.Drawing.Size(80, 17)
        Me.lblCollDt.TabIndex = 15
        Me.lblCollDt.Text = "01/01/2015"
        '
        'lblRecvDt
        '
        Me.lblRecvDt.AutoSize = True
        Me.lblRecvDt.Location = New System.Drawing.Point(164, 180)
        Me.lblRecvDt.Name = "lblRecvDt"
        Me.lblRecvDt.Size = New System.Drawing.Size(80, 17)
        Me.lblRecvDt.TabIndex = 16
        Me.lblRecvDt.Text = "01/01/2015"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(49, 180)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(105, 17)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Received Date:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(44, 197)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(110, 17)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Specimen Type:"
        '
        'lblSpecType
        '
        Me.lblSpecType.AutoSize = True
        Me.lblSpecType.Location = New System.Drawing.Point(164, 197)
        Me.lblSpecType.Name = "lblSpecType"
        Me.lblSpecType.Size = New System.Drawing.Size(42, 17)
        Me.lblSpecType.TabIndex = 19
        Me.lblSpecType.Text = "Urine"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(335, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 17)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Account Info:"
        '
        'lblAcctInfo
        '
        Me.lblAcctInfo.AutoSize = True
        Me.lblAcctInfo.Location = New System.Drawing.Point(431, 112)
        Me.lblAcctInfo.Name = "lblAcctInfo"
        Me.lblAcctInfo.Size = New System.Drawing.Size(236, 17)
        Me.lblAcctInfo.TabIndex = 21
        Me.lblAcctInfo.Text = "307 - Quick Care RSP Interventional"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(353, 146)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 17)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Physician:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(431, 146)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(192, 17)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "PH00261 - Dr. Mickey Mouse"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(273, 180)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(152, 17)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Presribed Medications:"
        '
        'lblMed
        '
        Me.lblMed.AutoSize = True
        Me.lblMed.Location = New System.Drawing.Point(431, 180)
        Me.lblMed.Name = "lblMed"
        Me.lblMed.Size = New System.Drawing.Size(221, 17)
        Me.lblMed.TabIndex = 25
        Me.lblMed.Text = "Norco, Gabapentin, Hydrocodone"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(316, 197)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(109, 17)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "Ordered Codes:"
        '
        'lstOC
        '
        Me.lstOC.FormattingEnabled = True
        Me.lstOC.ItemHeight = 16
        Me.lstOC.Location = New System.Drawing.Point(432, 201)
        Me.lstOC.Name = "lstOC"
        Me.lstOC.Size = New System.Drawing.Size(277, 52)
        Me.lstOC.TabIndex = 27
        '
        'PDF1
        '
        Me.PDF1.Enabled = True
        Me.PDF1.Location = New System.Drawing.Point(729, 112)
        Me.PDF1.Name = "PDF1"
        Me.PDF1.OcxState = CType(resources.GetObject("PDF1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.PDF1.Size = New System.Drawing.Size(546, 719)
        Me.PDF1.TabIndex = 28
        '
        'grdCon
        '
        Me.grdCon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCon.Location = New System.Drawing.Point(30, 279)
        Me.grdCon.Name = "grdCon"
        Me.grdCon.RowTemplate.Height = 24
        Me.grdCon.Size = New System.Drawing.Size(679, 552)
        Me.grdCon.TabIndex = 29
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(1151, 859)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(113, 39)
        Me.btnExit.TabIndex = 30
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'SpecRev
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1287, 911)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.grdCon)
        Me.Controls.Add(Me.PDF1)
        Me.Controls.Add(Me.lstOC)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.lblMed)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblAcctInfo)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblSpecType)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblRecvDt)
        Me.Controls.Add(Me.lblCollDt)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblDOB)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblGender)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblSpecID)
        Me.Controls.Add(Me.lblPatName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtSpec)
        Me.Controls.Add(Me.Label1)
        Me.Name = "SpecRev"
        Me.Text = "Specimen Review"
        CType(Me.PDF1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdCon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSpec As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblPatName As System.Windows.Forms.Label
    Friend WithEvents lblSpecID As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblDOB As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblCollDt As System.Windows.Forms.Label
    Friend WithEvents lblRecvDt As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblSpecType As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblAcctInfo As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblMed As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lstOC As System.Windows.Forms.ListBox
    Friend WithEvents PDF1 As AxAcroPDFLib.AxAcroPDF
    Friend WithEvents grdCon As System.Windows.Forms.DataGridView
    Friend WithEvents btnExit As System.Windows.Forms.Button
End Class
