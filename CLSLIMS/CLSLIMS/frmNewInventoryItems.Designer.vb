﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmNewInventoryItems
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewInventoryItems))
        Me.lookupVendors = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtItemDesc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cboUnits = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtItemsPerUnit = New DevExpress.XtraEditors.TextEdit()
        Me.txtItemCost = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtStockNumber = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMinQty = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCurrentQty = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.grdItems = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAddNewItem = New DevExpress.XtraEditors.SimpleButton()
        Me.DxValidationProvider1 = New DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(Me.components)
        Me.lookupDepartments = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.lookupVendors.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboUnits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemsPerUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemCost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStockNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMinQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrentQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.DxValidationProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lookupDepartments.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lookupVendors
        '
        Me.lookupVendors.Location = New System.Drawing.Point(12, 48)
        Me.lookupVendors.Name = "lookupVendors"
        Me.lookupVendors.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lookupVendors.Properties.Appearance.Options.UseFont = True
        Me.lookupVendors.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lookupVendors.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("VendorName", "Vendor name"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Name2", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[Default])})
        Me.lookupVendors.Size = New System.Drawing.Size(306, 26)
        Me.lookupVendors.TabIndex = 0
        Me.lookupVendors.TabStop = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(12, 23)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(51, 19)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Vendor"
        '
        'txtItemDesc
        '
        Me.txtItemDesc.Location = New System.Drawing.Point(13, 114)
        Me.txtItemDesc.Name = "txtItemDesc"
        Me.txtItemDesc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtItemDesc.Properties.Appearance.Options.UseFont = True
        Me.txtItemDesc.Size = New System.Drawing.Size(445, 26)
        Me.txtItemDesc.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(13, 89)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(117, 19)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Item Description"
        '
        'cboUnits
        '
        Me.cboUnits.Location = New System.Drawing.Point(476, 114)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.cboUnits.Properties.Appearance.Options.UseFont = True
        Me.cboUnits.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboUnits.Properties.Items.AddRange(New Object() {"Bottle", "Box", "Case", "EA", "g", "Kit", "L", "ml", "Pack", "Sheets"})
        Me.cboUnits.Size = New System.Drawing.Size(101, 26)
        Me.cboUnits.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(476, 89)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(36, 19)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Units"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(591, 89)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(95, 19)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "Item Per Unit"
        '
        'txtItemsPerUnit
        '
        Me.txtItemsPerUnit.Location = New System.Drawing.Point(591, 114)
        Me.txtItemsPerUnit.Name = "txtItemsPerUnit"
        Me.txtItemsPerUnit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtItemsPerUnit.Properties.Appearance.Options.UseFont = True
        Me.txtItemsPerUnit.Properties.Appearance.Options.UseTextOptions = True
        Me.txtItemsPerUnit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtItemsPerUnit.Size = New System.Drawing.Size(100, 26)
        Me.txtItemsPerUnit.TabIndex = 2
        '
        'txtItemCost
        '
        Me.txtItemCost.Location = New System.Drawing.Point(710, 114)
        Me.txtItemCost.Name = "txtItemCost"
        Me.txtItemCost.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtItemCost.Properties.Appearance.Options.UseFont = True
        Me.txtItemCost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtItemCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtItemCost.Size = New System.Drawing.Size(100, 26)
        Me.txtItemCost.TabIndex = 3
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(710, 89)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(69, 19)
        Me.LabelControl5.TabIndex = 8
        Me.LabelControl5.Text = "Item Cost"
        '
        'txtStockNumber
        '
        Me.txtStockNumber.Location = New System.Drawing.Point(12, 187)
        Me.txtStockNumber.Name = "txtStockNumber"
        Me.txtStockNumber.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtStockNumber.Properties.Appearance.Options.UseFont = True
        Me.txtStockNumber.Size = New System.Drawing.Size(135, 26)
        Me.txtStockNumber.TabIndex = 4
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(12, 162)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(111, 19)
        Me.LabelControl6.TabIndex = 10
        Me.LabelControl6.Text = "Vendor Stock #"
        '
        'txtMinQty
        '
        Me.txtMinQty.Location = New System.Drawing.Point(163, 187)
        Me.txtMinQty.Name = "txtMinQty"
        Me.txtMinQty.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtMinQty.Properties.Appearance.Options.UseFont = True
        Me.txtMinQty.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMinQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMinQty.Size = New System.Drawing.Size(100, 26)
        Me.txtMinQty.TabIndex = 5
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(163, 162)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(96, 19)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Minimum Qty"
        '
        'txtCurrentQty
        '
        Me.txtCurrentQty.Location = New System.Drawing.Point(284, 187)
        Me.txtCurrentQty.Name = "txtCurrentQty"
        Me.txtCurrentQty.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtCurrentQty.Properties.Appearance.Options.UseFont = True
        Me.txtCurrentQty.Properties.Appearance.Options.UseTextOptions = True
        Me.txtCurrentQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtCurrentQty.Size = New System.Drawing.Size(100, 26)
        Me.txtCurrentQty.TabIndex = 6
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(284, 162)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(83, 19)
        Me.LabelControl8.TabIndex = 14
        Me.LabelControl8.Text = "Current Qty"
        '
        'grdItems
        '
        Me.grdItems.Location = New System.Drawing.Point(13, 235)
        Me.grdItems.MainView = Me.GridView1
        Me.grdItems.Name = "grdItems"
        Me.grdItems.Size = New System.Drawing.Size(514, 196)
        Me.grdItems.TabIndex = 16
        Me.grdItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9})
        Me.GridView1.GridControl = Me.grdItems
        Me.GridView1.Name = "GridView1"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.FieldName = "VendorId"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Description"
        Me.GridColumn2.FieldName = "ItemDesc"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "GridColumn3"
        Me.GridColumn3.FieldName = "Units"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "GridColumn4"
        Me.GridColumn4.FieldName = "ItemPerUnit"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "GridColumn5"
        Me.GridColumn5.FieldName = "ItemCost"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "GridColumn6"
        Me.GridColumn6.FieldName = "StockNumber"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "GridColumn7"
        Me.GridColumn7.FieldName = "MinQty"
        Me.GridColumn7.Name = "GridColumn7"
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "GridColumn8"
        Me.GridColumn8.FieldName = "CurrentQty"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "GridColumn9"
        Me.GridColumn9.FieldName = "DaysReceive"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.ImageOptions.SvgImage = CType(resources.GetObject("btnAdd.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnAdd.Location = New System.Drawing.Point(545, 181)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(141, 32)
        Me.btnAdd.TabIndex = 9
        Me.btnAdd.Text = "Add Item"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnCancel)
        Me.GroupControl1.Controls.Add(Me.btnSave)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl1.Location = New System.Drawing.Point(0, 451)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(819, 131)
        Me.GroupControl1.TabIndex = 11
        Me.GroupControl1.Text = "Actions"
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.ImageOptions.SvgImage = CType(resources.GetObject("btnCancel.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnCancel.Location = New System.Drawing.Point(533, 51)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(114, 43)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        '
        'btnSave
        '
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.ImageOptions.SvgImage = CType(resources.GetObject("btnSave.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnSave.Location = New System.Drawing.Point(693, 51)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(114, 43)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save"
        '
        'btnAddNewItem
        '
        Me.btnAddNewItem.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnAddNewItem.Appearance.Options.UseFont = True
        Me.btnAddNewItem.ImageOptions.SvgImage = CType(resources.GetObject("btnAddNewItem.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.btnAddNewItem.Location = New System.Drawing.Point(545, 399)
        Me.btnAddNewItem.Name = "btnAddNewItem"
        Me.btnAddNewItem.Size = New System.Drawing.Size(176, 32)
        Me.btnAddNewItem.TabIndex = 10
        Me.btnAddNewItem.Text = "Add New Item"
        '
        'DxValidationProvider1
        '
        Me.DxValidationProvider1.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual
        '
        'lookupDepartments
        '
        Me.lookupDepartments.Location = New System.Drawing.Point(398, 186)
        Me.lookupDepartments.Name = "lookupDepartments"
        Me.lookupDepartments.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lookupDepartments.Properties.Appearance.Options.UseFont = True
        Me.lookupDepartments.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lookupDepartments.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentId", "Name1", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentName", "Department")})
        Me.lookupDepartments.Size = New System.Drawing.Size(127, 26)
        Me.lookupDepartments.TabIndex = 8
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(398, 161)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(83, 19)
        Me.LabelControl10.TabIndex = 22
        Me.LabelControl10.Text = "Department"
        '
        'frmNewInventoryItems
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(819, 582)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.lookupDepartments)
        Me.Controls.Add(Me.btnAddNewItem)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.grdItems)
        Me.Controls.Add(Me.txtCurrentQty)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.txtMinQty)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.txtStockNumber)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.txtItemCost)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.txtItemsPerUnit)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.cboUnits)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.txtItemDesc)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.lookupVendors)
        Me.Name = "frmNewInventoryItems"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Inventory Items"
        CType(Me.lookupVendors.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboUnits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemsPerUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemCost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStockNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMinQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrentQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.DxValidationProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lookupDepartments.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lookupVendors As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtItemDesc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboUnits As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtItemsPerUnit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtItemCost As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtStockNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMinQty As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCurrentQty As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddNewItem As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DxValidationProvider1 As DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider
    Friend WithEvents lookupDepartments As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
End Class
