﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.ComponentModel

Public Class frmReview
    Public CurrentListBoxIndex As Integer = 0, ASpecno As String = "", BSpecno As String = "", CSpecno As String = "", DSpecno As String = "", LastNdx As Integer = 0
    'Public UID As String = "NED.SABBATINI"
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        WatchWorker.CancelAsync()
        AWorker.CancelAsync()
        BWorker.CancelAsync()
        CWorker.CancelAsync()
        DWorker.CancelAsync()
        System.Threading.Thread.Sleep(2000)
        ClearRpts()
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click

        LoadBatch()
    End Sub

    Public Sub LoadBatch()
        btnLoad.Enabled = False
        Dim xSel As String = ""
        Dim Con As New SqlConnection(CLSLIMS.strCon)
        If txtMBN.Text <> "" Then
            xSel = "Select PSPECNO From PB A, PBI B Where BACT=1 And MBATCH='" & txtMBN.Text & "' And A.WL=B.WL And A.BATCH=B.BATCH And A.BEFDT=B.BEFDT"
        ElseIf txtFolder.Text <> "" Then
            xSel = "Select PSPECNO From XTRAYS Where FOLDER='" & txtFolder.Text & "' And PSPECNO != 'HOLDER' Order By Tray,Trayx"
        ElseIf txtTray.Text <> "" Then
            xSel = "Select PSPECNO From XTRAYS Where TRAY='" & txtTray.Text & "' And PSPECNO != 'HOLDER' Order By Trayx"
        Else
            MsgBox("Must enter MBN, Folder or Tray.")
        End If
        Con.Open()
        Dim Cmd As New SqlCommand(xSel, Con)
        Dim RS As SqlDataReader = Cmd.ExecuteReader
        Do While RS.Read
            'Load lstBatchSpecNo with sample numbers
            lstBatchSpecNo.Items.Add(RS(0))
        Loop
        RS.Close()
        Cmd.Dispose()
        Con.Close()
        ClearRpts()
        btnForward.Enabled = True
        btnBack.Enabled = True
        btnFastBack.Enabled = True
        btnFastForward.Enabled = True
        lblTotal.Text = lstBatchSpecNo.Items.Count : lblTotal.Refresh()
        lblCurrNdx.Text = 1 : lblCurrNdx.Refresh() : lblSeq.Text = 1 : lblSeq.Refresh()
        System.Threading.Thread.Sleep(1)
        'Start background worker
        WatchWorker.RunWorkerAsync()
        'Display 1st Req and 1st Report
        LoadImage(lstBatchSpecNo.Items(0))
        LoadReport(lstBatchSpecNo.Items(0))

    End Sub
    Public Function LoadImage(ByVal specno As String) As Boolean
        LoadImage = False
        Dim Con As New SqlConnection(CLSLIMS.strCon)
        Con.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Con
        Cmd.CommandText = "select '\\CMPFS1\Image\' + IMGVOL + '\' + IMGFILE From PIMG Where PSPECNO='" & specno & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader, ImgFile As String = "", SecondImgFile As String = ""
        If rs.Read Then
            ImgFile = rs(0)
        End If
        rs.Close()
        SecondImgFile = CLSLIMS.ReplacePiece(ImgFile, "\", 3, "CL-AGILENT")
        'Remove trailing "\" ???
        SecondImgFile = Mid(SecondImgFile, 1, Len(SecondImgFile) - 1)
        If ImgFile <> "" Then
            If System.IO.File.Exists(ImgFile) Then
                LoadImage = True
                ScanPDF.src = ImgFile
            Else
                If System.IO.File.Exists(SecondImgFile) Then
                    LoadImage = True
                    ScanPDF.src = SecondImgFile
                End If
            End If
        End If

    End Function
    Public Function LoadReport(ByVal specno As String) As Boolean
        LoadReport = False
        If File.Exists("C:\CLSLIMS\" & CLSLIMS.UID & "\Files\" & specno & "r.pdf") Then
            ReportPDF.src = "C:\CLSLIMS\" & CLSLIMS.UID & "\Files\" & specno & "r.pdf"
            ReportPDF.Refresh()
            LoadReport = True
            Exit Function
        End If
        Dim RptFile As String = "C:\CLSLIMS\" & CLSLIMS.UID & "\"
        Dim FileNotFound As Boolean = True
        Do While FileNotFound
            If File.Exists("C:\CLSLIMS\" & CLSLIMS.UID & "\Files\" & specno & "r.pdf") Then
                FileNotFound = False
            Else
                System.Threading.Thread.Sleep(2000)
            End If
        Loop
        LoadReport = True
        System.Threading.Thread.Sleep(2000)
        ReportPDF.src = "C:\CLSLIMS\" & CLSLIMS.UID & "\Files\" & specno & "r.pdf"
        ReportPDF.Refresh()
    End Function
    Public Sub BuildReport(ByVal specno As String)
        System.Threading.Thread.Sleep(2000)
        Dim CurrDtTm As String = Format(Now, "yyyyMMddhhmmss")
        Debug.Print("C:\CLSLIMS\ApolloPDFB.exe " & specno & " 500 r C:\CLSLIMS\" & CLSLIMS.UID & "\ " & CurrDtTm & " " & specno & " False HL7-TNCL-PDF~999~1~0~1~1~EDI")
        Shell("C:\CLSLIMS\ApolloPDFB.exe " & specno & " 500 r C:\CLSLIMS\" & CLSLIMS.UID & "\ " & CurrDtTm & " " & specno & " False HL7-TNCL-PDF~999~1~0~1~1~EDI")
        Dim FileNotFound As Boolean = True
        Do While FileNotFound
            If File.Exists("C:\CLSLIMS\" & CLSLIMS.UID & "\Files\" & specno & "r.pdf") Then
                FileNotFound = False
            Else
                System.Threading.Thread.Sleep(2000)
            End If
        Loop
        LastNdx = LastNdx + 1
        lblCurrNdx.Text = LastNdx : lblCurrNdx.Refresh()
        If Val(lblTotal.Text) = LastNdx Then
            Label1.Text = "All " & lblTotal.Text & " Reports Built."
            lblTotal.Text = ""
            lblCurrNdx.Text = ""
            Label3.Text = ""
        End If
    End Sub


    Private Sub frmReview_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        Control.CheckForIllegalCrossThreadCalls = False
    End Sub

    Public Sub ClearRpts()
        Dim myFile As String
        Dim mydir As String = "C:\CLSLIMS\" & CLSLIMS.UID & "\Files\"
        If Directory.Exists(mydir) Then
            For Each myFile In Directory.GetFiles(mydir, "*.pdf")
                File.Delete(myFile)
            Next
        Else
            Directory.CreateDirectory(mydir)
            File.Copy("C:\CLSLIMS\Apollopdfb.exe", "C:\CLSLIMS\" & CLSLIMS.UID & "\Apollopdfb.exe")
            File.Copy("C:\CLSLIMS\Datastr.ini", "C:\CLSLIMS\" & CLSLIMS.UID & "\Datastr.ini")
        End If
    End Sub
    Private Sub txtMBN_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMBN.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        LoadBatch()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        If CurrentListBoxIndex = 0 Then
            Exit Sub
        End If
        CurrentListBoxIndex = CurrentListBoxIndex - 1 : lblSeq.Text = CurrentListBoxIndex + 1 : lblSeq.Refresh()
        System.Threading.Thread.Sleep(1000)
        LoadImage(lstBatchSpecNo.Items(CurrentListBoxIndex))
        ReportPDF.src = "C:\CLSLIMS\Building.pdf"
        ReportPDF.Refresh()
        System.Threading.Thread.Sleep(1000)
        LoadReport(lstBatchSpecNo.Items(CurrentListBoxIndex))
    End Sub
    Private Sub btnFastBack_Click(sender As Object, e As EventArgs) Handles btnFastBack.Click
        If CurrentListBoxIndex < 11 Then
            CurrentListBoxIndex = 0 : lblSeq.Text = CurrentListBoxIndex + 1 : lblSeq.Refresh()
        Else
            CurrentListBoxIndex = CurrentListBoxIndex - 10 : lblSeq.Text = CurrentListBoxIndex + 1 : lblSeq.Refresh()
        End If
        System.Threading.Thread.Sleep(1000)
        LoadImage(lstBatchSpecNo.Items(CurrentListBoxIndex))
        ReportPDF.src = "C:\CLSLIMS\Building.pdf"
        ReportPDF.Refresh()
        System.Threading.Thread.Sleep(1000)
        LoadReport(lstBatchSpecNo.Items(CurrentListBoxIndex))
    End Sub
    Private Sub btnForward_Click(sender As Object, e As EventArgs) Handles btnForward.Click
        If CurrentListBoxIndex = lstBatchSpecNo.Items.Count - 1 Then
            Exit Sub
        End If
        CurrentListBoxIndex = CurrentListBoxIndex + 1 : lblSeq.Text = CurrentListBoxIndex + 1 : lblSeq.Refresh()
        System.Threading.Thread.Sleep(1000)
        LoadImage(lstBatchSpecNo.Items(CurrentListBoxIndex))
        ReportPDF.src = "C:\CLSLIMS\Building.pdf"
        ReportPDF.Refresh()
        System.Threading.Thread.Sleep(1000)
        LoadReport(lstBatchSpecNo.Items(CurrentListBoxIndex))
    End Sub
    Private Sub btnFastForward_Click(sender As Object, e As EventArgs) Handles btnFastForward.Click
        If CurrentListBoxIndex > lstBatchSpecNo.Items.Count - 11 Then
            CurrentListBoxIndex = lstBatchSpecNo.Items.Count - 1 : lblSeq.Text = CurrentListBoxIndex + 1 : lblSeq.Refresh()
        ElseIf CurrentListBoxIndex > LastNdx - 10 Then
            CurrentListBoxIndex = LastNdx : lblSeq.Text = CurrentListBoxIndex + 1 : lblSeq.Refresh()
        Else
            CurrentListBoxIndex = CurrentListBoxIndex + 10 : lblSeq.Text = CurrentListBoxIndex + 1 : lblSeq.Refresh()
        End If
        System.Threading.Thread.Sleep(1000)
        LoadImage(lstBatchSpecNo.Items(CurrentListBoxIndex))
        ReportPDF.src = "C:\CLSLIMS\Building.pdf"
        ReportPDF.Refresh()
        System.Threading.Thread.Sleep(1000)
        LoadReport(lstBatchSpecNo.Items(CurrentListBoxIndex))
    End Sub

    Private Sub AWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles AWorker.DoWork
        Debug.Print(ASpecno)
        BuildReport(ASpecno)
        ASpecno = ""
    End Sub

    Private Sub BWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles BWorker.DoWork
        Debug.Print(BSpecno)
        BuildReport(BSpecno)
        BSpecno = ""
    End Sub

    Private Sub btnCom_Click(sender As Object, e As EventArgs) Handles btnCom.Click
        Dim Con As New SqlConnection(CLSLIMS.strCon)
        Con.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Con
        Cmd.CommandText = "select pscmttxt from pscmt a, pscmti b where a.pspecno = b.pspecno and a.pscmttype=b.pscmttype and a.pscmtefdt=b.pscmtefdt " &
                              "and a.pspecno='" & lstBatchSpecNo.Items(CurrentListBoxIndex) & "' and pscmtact=1 order by a.pscmttype,a.pscmtefdt,pscmtix"
        Dim rs7 As SqlDataReader = Cmd.ExecuteReader
        frmDspCom.lstCom.Items.Clear()
        Do While rs7.Read
            frmDspCom.lstCom.Items.Add(rs7(0))
        Loop
        rs7.Close()
        frmDspCom.ShowDialog()
    End Sub

    Private Sub CWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles CWorker.DoWork
        Debug.Print(CSpecno)
        BuildReport(CSpecno)
        CSpecno = ""
    End Sub

    Private Sub DWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles DWorker.DoWork
        Debug.Print(DSpecno)
        BuildReport(DSpecno)
        DSpecno = ""
    End Sub

    Private Sub WatchWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles WatchWorker.DoWork
        Dim ndx As Integer = 0
        Do While True
            'lblCurrNdx.Text = ndx : lblCurrNdx.Refresh()
            If ndx > lstBatchSpecNo.Items.Count - 1 Then
                Exit Do
            End If
            If ASpecno = "" Then
                ASpecno = lstBatchSpecNo.Items(ndx)
                AWorker.RunWorkerAsync()
                ndx = ndx + 1
                Debug.Print("AWorker:" & ASpecno)
            End If
            If ndx > lstBatchSpecNo.Items.Count - 1 Then
                Exit Do
            End If
            If BSpecno = "" Then
                BSpecno = lstBatchSpecNo.Items(ndx)
                BWorker.RunWorkerAsync()
                ndx = ndx + 1
                Debug.Print("BWorker:" & BSpecno)
            End If
            If ndx > lstBatchSpecNo.Items.Count - 1 Then
                Exit Do
            End If
            If CSpecno = "" Then
                CSpecno = lstBatchSpecNo.Items(ndx)
                CWorker.RunWorkerAsync()
                ndx = ndx + 1
                Debug.Print("CWorker:" & CSpecno)
            End If
            If ndx > lstBatchSpecNo.Items.Count - 1 Then
                Exit Do
            End If
            If DSpecno = "" Then
                DSpecno = lstBatchSpecNo.Items(ndx)
                DWorker.RunWorkerAsync()
                ndx = ndx + 1
                Debug.Print("DWorker:" & DSpecno)
            End If
            If ndx > lstBatchSpecNo.Items.Count - 1 Then
                Exit Do
            End If
            Debug.Print("Sleep 5")
            System.Threading.Thread.Sleep(5000)
        Loop
        btnLoad.Enabled = True

    End Sub

    Private Sub txtFolder_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtFolder.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        LoadBatch()
    End Sub

    Private Sub txtTray_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTray.KeyPress
        If Asc(e.KeyChar) > 96 And Asc(e.KeyChar) < 123 Then
            e.KeyChar = UCase(e.KeyChar)
        End If
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        LoadBatch()
    End Sub

End Class