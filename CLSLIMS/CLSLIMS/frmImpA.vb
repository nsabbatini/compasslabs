﻿Imports System.IO
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class frmImpA
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnImpA_Click(sender As Object, e As EventArgs) Handles btnImpA.Click
        'Scan dir for txt files
        Dim r As String = "", Res(99) As String, Org As String = "", CT As String = "", CTN As Double = 100.0, CutOff As String = "", Det As String = "", EqRes As String = "", Spec As String = ""
        Dim X As String = "", Y As String = "", Lo As String = "", Md As String = "", Hi As String = "", Rng As String = "", AmpStatus As String = ""
        Dim fn As System.IO.StreamReader = Nothing
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim flnme As String = ""
        fd.Title = "Open Molecular Interface File"
        fd.InitialDirectory = "C:\"
        fd.Filter = "text files (*.txt)|*.txt"
        fd.RestoreDirectory = True
        fd.Multiselect = False
        If fd.ShowDialog = DialogResult.OK Then
            flnme = fd.FileName
        Else
            Exit Sub
        End If
        lstInfo.Items.Add(flnme) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        fn = My.Computer.FileSystem.OpenTextFileReader(flnme)
        r = fn.ReadLine
        'Loop until [Results]
        Do While InStr(r, "[Results]") = 0 And Not fn.EndOfStream
            r = fn.ReadLine : Debug.Print(r)
        Loop
        r = fn.ReadLine  'Read Title
        Do While Not fn.EndOfStream                      '===========  Sort by Specimen number , List box? ============
            r = fn.ReadLine
            Res = Split(r, Chr(9))
            'load variables, make comparisons, make determinations
            Spec = Res(3) : If Not CheckSpecNo(Spec) Then Continue Do
            AmpStatus = Res(25) ': If AmpStatus <> "Amp" Then Continue Do
            Org = Res(4) : CT = Res(8) : CutOff = GetCutOff(Org) : Det = "Not Detected"
            Debug.Print(Org & ", " & CT & ", " & Det & ", " & Rng)
            If CT <> "Undetermined" And CT <> "" Then
                Lo = CLSLIMS.Piece(CutOff, "^", 2) : Md = CLSLIMS.Piece(CutOff, "^", 3) : Hi = CLSLIMS.Piece(CutOff, "^", 4) : CutOff = CLSLIMS.Piece(CutOff, "^", 1)
                If Val(Trim(CT)) <= Val(Trim(CutOff)) And Trim(CT) <> "" Then Det = "Detected"
                If Val(Trim(CT)) < Val(Trim(CutOff)) And Val(Trim(CT)) >= Val(Trim(Lo)) Then
                    Rng = "Low"
                ElseIf Val(Trim(CT)) < Val(Trim(Lo)) And Val(Trim(CT)) >= Val(Trim(Md)) Then
                    Rng = "Moderate"
                ElseIf Val(Trim(CT)) <= Val(Trim(Hi)) Then
                    Rng = "High"
                End If
                If Det = "Detected" Then EqRes = Equation(CT)
            End If
            Debug.Print(Org & ", " & CT & ", " & Det & ", " & Rng)
            lstInfo.Items.Add(CLSLIMS.LJ(Spec, 10) & CLSLIMS.RJ(Org, 15) & CLSLIMS.RJ(CT, 15) & CLSLIMS.RJ(Det, 15) & CLSLIMS.LJ(Rng, 4) & CLSLIMS.RJ(EqRes, 5)) : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
            'Insert into PRES
            InsPRES(Spec, Org, CT, EqRes, Det, Rng)
            Org = "" : CT = "" : EqRes = "" : Det = "" : Rng = ""
        Loop
        fn.Close()
        lstInfo.Items.Add("Complete!") : lstInfo.SelectedIndex = lstInfo.Items.Count - 1
        'Rename(fl.FullName,pieceCLSLIMS.numberofpieces(fl.FullName,"\"))
    End Sub
    Public Sub InsPRES(ByVal Spec As String, ByVal Org As String, ByVal CT As String, ByVal EqRes As String, ByVal Det As String, ByVal Rng As String)
        Dim Ins As String = "", Upd As String = "", RC As String = "", REFDT As String = "", PRSTAT As String = "", r As String = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Debug.Print(Org & ", " & CT & ", " & Det & ", " & Rng)
        'CT
        '=============================================================================================================================
        r = GetRCInfo(Org & "-CT") : RC = CLSLIMS.Piece(r, "^", 1) : REFDT = CLSLIMS.Piece(r, "^", 2) : PRSTAT = CLSLIMS.Piece(r, "^", 3)
        ' if prior results
        Upd = "Update PRES Set PRSLT='" & CT & "',PRSTAT=2,PREDT='" & CLSLIMS.CDT & "',PREID='%Ins'  Where PSPECNO='" & Spec & "' And RC='" & RC & "' And PRACT=1"
        Cmd.CommandText = Upd : Cmd.ExecuteNonQuery()
        'QL   
        '===============================================================================================================================
        r = GetRCInfo(Org & "-QL") : RC = CLSLIMS.Piece(r, "^", 1) : REFDT = CLSLIMS.Piece(r, "^", 2) : PRSTAT = CLSLIMS.Piece(r, "^", 3)
        Upd = "Update PRES Set PRSLT='" & Det & "',PRSTAT=2,PREDT='" & CLSLIMS.CDT & "',PREID='%Ins'  Where PSPECNO='" & Spec & "' And RC='" & RC & "' And PRACT=1"
        Cmd.CommandText = Upd : Cmd.ExecuteNonQuery()
        'RANK
        '===============================================================================================================================
        r = GetRCInfo(Org & "-RANK") : RC = CLSLIMS.Piece(r, "^", 1) : REFDT = CLSLIMS.Piece(r, "^", 2) : PRSTAT = CLSLIMS.Piece(r, "^", 3)
        If RC <> "" Then
            Upd = "Update PRES Set PRSLT='" & Rng & "',PRSTAT=2,PREDT='" & CLSLIMS.CDT & "',PREID='%Ins'  Where PSPECNO='" & Spec & "' And RC='" & RC & "' And PRACT=1"
            Cmd.CommandText = Upd : Cmd.ExecuteNonQuery()
        End If
        'EQ
        '===============================================================================================================================
        r = GetRCInfo(Org & "-EQ") : RC = CLSLIMS.Piece(r, "^", 1) : REFDT = CLSLIMS.Piece(r, "^", 2) : PRSTAT = CLSLIMS.Piece(r, "^", 3)
        If RC <> "" Then
            Upd = "Update PRES Set PRSLT='" & EqRes & "',PRSTAT=2,PREDT='" & CLSLIMS.CDT & "',PREID='%Ins'  Where PSPECNO='" & Spec & "' And RC='" & RC & "' And PRACT=1"
            Cmd.CommandText = Upd : Cmd.ExecuteNonQuery()
        End If
        Conn.Close()
    End Sub
    Public Function GetRCInfo(ByVal org As String) As String
        GetRCInfo = ""   'RC^REFDT^PRSTAT
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select A.RC,B.REFDT,PRSTAT From PRES A, RC B Where A.RC=B.RC And A.REFDT=B.REFDT And PRACT=1 And RABRV='" & org & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetRCInfo = rs(0) & "^" & rs(1) & "^" & rs(2)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function GetCutOff(ByVal org As String) As String
        'Returns Cutoff^Low^Mid^High
        GetCutOff = ""
        Dim ConnMySql As New MySqlConnection(CLSLIMS.strConMySQL)
        ConnMySql.Open()
        Dim Cmd As New MySqlCommand
        Cmd.Connection = ConnMySql
        Cmd.CommandText = "Select Cutoff, Low, Mid, high From Ranges Where active=1"
        Dim rs As MySqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetCutOff = rs(0) & "^" & rs(1) & "^" & rs(2) & "^" & rs(3)
        End If
        rs.Close()
        Cmd.Dispose()
        ConnMySql.Close()
        ConnMySql.Dispose()
    End Function
    Public Function Equation(ByVal ct As String) As String
        Equation = ""
        Dim EqRslt As Double = 0.00
        EqRslt = Math.Exp((Val(ct) - 38) / -1.5)

        If EqRslt > 500000 Then
            Equation = ">500,000"
        ElseIf EqRslt < 50 Then
            Equation = "<50"
        Else
            Equation = EqRslt.ToString("G")
        End If
    End Function
    Public Function CheckSpecNo(ByVal specno As String) As Boolean
        Dim xSel As String = "Select PSPECNO From PSPEC Where PSACT=1 And PSPECNO='" & specno & "'"
        CheckSpecNo = False
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                CheckSpecNo = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function

End Class