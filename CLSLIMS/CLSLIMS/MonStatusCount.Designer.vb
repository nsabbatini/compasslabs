﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MonStatusCount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstMonStatCnt = New System.Windows.Forms.ListBox()
        Me.txtAcct = New System.Windows.Forms.TextBox()
        Me.lblAccount = New System.Windows.Forms.Label()
        Me.txtEndDt = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtStartDt = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstXLS = New System.Windows.Forms.ListBox()
        Me.btnrpt = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnXLS = New System.Windows.Forms.Button()
        Me.cbAcctSpecific = New System.Windows.Forms.CheckBox()
        Me.cbDetail = New System.Windows.Forms.CheckBox()
        Me.cbCodeSpecific = New System.Windows.Forms.CheckBox()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.txtCode = New System.Windows.Forms.TextBox()
        Me.cbSummary = New System.Windows.Forms.CheckBox()
        Me.lblSumCode = New System.Windows.Forms.Label()
        Me.lblSumAcct = New System.Windows.Forms.Label()
        Me.txtSumAcct = New System.Windows.Forms.TextBox()
        Me.txtSumCode = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lstMonStatCnt
        '
        Me.lstMonStatCnt.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstMonStatCnt.FormattingEnabled = True
        Me.lstMonStatCnt.ItemHeight = 16
        Me.lstMonStatCnt.Location = New System.Drawing.Point(6, 115)
        Me.lstMonStatCnt.Name = "lstMonStatCnt"
        Me.lstMonStatCnt.Size = New System.Drawing.Size(1503, 356)
        Me.lstMonStatCnt.TabIndex = 4
        '
        'txtAcct
        '
        Me.txtAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcct.Location = New System.Drawing.Point(671, 53)
        Me.txtAcct.Name = "txtAcct"
        Me.txtAcct.Size = New System.Drawing.Size(76, 27)
        Me.txtAcct.TabIndex = 4
        Me.txtAcct.Text = "ALL"
        '
        'lblAccount
        '
        Me.lblAccount.AutoSize = True
        Me.lblAccount.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccount.Location = New System.Drawing.Point(567, 56)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Size = New System.Drawing.Size(80, 19)
        Me.lblAccount.TabIndex = 15
        Me.lblAccount.Text = "Account:"
        '
        'txtEndDt
        '
        Me.txtEndDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEndDt.Location = New System.Drawing.Point(118, 58)
        Me.txtEndDt.Name = "txtEndDt"
        Me.txtEndDt.Size = New System.Drawing.Size(109, 27)
        Me.txtEndDt.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(31, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 19)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "End Date:"
        '
        'txtStartDt
        '
        Me.txtStartDt.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDt.Location = New System.Drawing.Point(118, 18)
        Me.txtStartDt.Name = "txtStartDt"
        Me.txtStartDt.Size = New System.Drawing.Size(109, 27)
        Me.txtStartDt.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 19)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Start Date:"
        '
        'lstXLS
        '
        Me.lstXLS.FormattingEnabled = True
        Me.lstXLS.Location = New System.Drawing.Point(1438, 34)
        Me.lstXLS.Margin = New System.Windows.Forms.Padding(2)
        Me.lstXLS.Name = "lstXLS"
        Me.lstXLS.Size = New System.Drawing.Size(59, 43)
        Me.lstXLS.TabIndex = 16
        Me.lstXLS.Visible = False
        '
        'btnrpt
        '
        Me.btnrpt.Location = New System.Drawing.Point(1122, 490)
        Me.btnrpt.Name = "btnrpt"
        Me.btnrpt.Size = New System.Drawing.Size(75, 24)
        Me.btnrpt.TabIndex = 5
        Me.btnrpt.Text = " Report"
        Me.btnrpt.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(1403, 491)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Close"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(1216, 491)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 6
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnXLS
        '
        Me.btnXLS.Location = New System.Drawing.Point(1309, 491)
        Me.btnXLS.Name = "btnXLS"
        Me.btnXLS.Size = New System.Drawing.Size(75, 23)
        Me.btnXLS.TabIndex = 7
        Me.btnXLS.Text = "Excel"
        Me.btnXLS.UseVisualStyleBackColor = True
        '
        'cbAcctSpecific
        '
        Me.cbAcctSpecific.AutoSize = True
        Me.cbAcctSpecific.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAcctSpecific.Location = New System.Drawing.Point(571, 18)
        Me.cbAcctSpecific.Name = "cbAcctSpecific"
        Me.cbAcctSpecific.Size = New System.Drawing.Size(118, 23)
        Me.cbAcctSpecific.TabIndex = 3
        Me.cbAcctSpecific.Text = "By Account"
        Me.cbAcctSpecific.UseVisualStyleBackColor = True
        '
        'cbDetail
        '
        Me.cbDetail.AutoSize = True
        Me.cbDetail.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDetail.Location = New System.Drawing.Point(35, 491)
        Me.cbDetail.Name = "cbDetail"
        Me.cbDetail.Size = New System.Drawing.Size(68, 23)
        Me.cbDetail.TabIndex = 4
        Me.cbDetail.Text = "Detail"
        Me.cbDetail.UseVisualStyleBackColor = True
        '
        'cbCodeSpecific
        '
        Me.cbCodeSpecific.AutoSize = True
        Me.cbCodeSpecific.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCodeSpecific.Location = New System.Drawing.Point(875, 18)
        Me.cbCodeSpecific.Name = "cbCodeSpecific"
        Me.cbCodeSpecific.Size = New System.Drawing.Size(151, 23)
        Me.cbCodeSpecific.TabIndex = 17
        Me.cbCodeSpecific.Text = "By Status Code"
        Me.cbCodeSpecific.UseVisualStyleBackColor = True
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(871, 56)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(113, 19)
        Me.lblCode.TabIndex = 18
        Me.lblCode.Text = "Status Code:"
        '
        'txtCode
        '
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.Location = New System.Drawing.Point(1010, 50)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(76, 27)
        Me.txtCode.TabIndex = 19
        Me.txtCode.Text = "ALL"
        '
        'cbSummary
        '
        Me.cbSummary.AutoSize = True
        Me.cbSummary.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSummary.Location = New System.Drawing.Point(304, 18)
        Me.cbSummary.Name = "cbSummary"
        Me.cbSummary.Size = New System.Drawing.Size(104, 23)
        Me.cbSummary.TabIndex = 22
        Me.cbSummary.Text = "Summary"
        Me.cbSummary.UseVisualStyleBackColor = True
        '
        'lblSumCode
        '
        Me.lblSumCode.AutoSize = True
        Me.lblSumCode.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumCode.Location = New System.Drawing.Point(300, 56)
        Me.lblSumCode.Name = "lblSumCode"
        Me.lblSumCode.Size = New System.Drawing.Size(113, 19)
        Me.lblSumCode.TabIndex = 29
        Me.lblSumCode.Text = "Status Code:"
        '
        'lblSumAcct
        '
        Me.lblSumAcct.AutoSize = True
        Me.lblSumAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumAcct.Location = New System.Drawing.Point(300, 85)
        Me.lblSumAcct.Name = "lblSumAcct"
        Me.lblSumAcct.Size = New System.Drawing.Size(80, 19)
        Me.lblSumAcct.TabIndex = 28
        Me.lblSumAcct.Text = "Account:"
        '
        'txtSumAcct
        '
        Me.txtSumAcct.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSumAcct.Location = New System.Drawing.Point(428, 82)
        Me.txtSumAcct.Name = "txtSumAcct"
        Me.txtSumAcct.Size = New System.Drawing.Size(76, 27)
        Me.txtSumAcct.TabIndex = 27
        Me.txtSumAcct.Text = "ALL"
        '
        'txtSumCode
        '
        Me.txtSumCode.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSumCode.Location = New System.Drawing.Point(428, 53)
        Me.txtSumCode.Name = "txtSumCode"
        Me.txtSumCode.Size = New System.Drawing.Size(76, 27)
        Me.txtSumCode.TabIndex = 26
        Me.txtSumCode.Text = "ALL"
        '
        'MonStatusCount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1527, 540)
        Me.Controls.Add(Me.lblSumCode)
        Me.Controls.Add(Me.lblSumAcct)
        Me.Controls.Add(Me.txtSumAcct)
        Me.Controls.Add(Me.txtSumCode)
        Me.Controls.Add(Me.cbSummary)
        Me.Controls.Add(Me.txtCode)
        Me.Controls.Add(Me.lblCode)
        Me.Controls.Add(Me.cbCodeSpecific)
        Me.Controls.Add(Me.cbDetail)
        Me.Controls.Add(Me.cbAcctSpecific)
        Me.Controls.Add(Me.btnXLS)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnrpt)
        Me.Controls.Add(Me.lstXLS)
        Me.Controls.Add(Me.txtAcct)
        Me.Controls.Add(Me.lblAccount)
        Me.Controls.Add(Me.txtEndDt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtStartDt)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstMonStatCnt)
        Me.Name = "MonStatusCount"
        Me.Text = "MonStatusCount"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstMonStatCnt As ListBox
    Friend WithEvents txtAcct As TextBox
    Friend WithEvents lblAccount As Label
    Friend WithEvents txtEndDt As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtStartDt As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lstXLS As ListBox
    Friend WithEvents btnrpt As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents btnXLS As Button
    Friend WithEvents cbAcctSpecific As CheckBox
    Friend WithEvents cbDetail As CheckBox
    Friend WithEvents cbCodeSpecific As CheckBox
    Friend WithEvents lblCode As Label
    Friend WithEvents txtCode As TextBox
    Friend WithEvents cbSummary As CheckBox
    Friend WithEvents lblSumCode As Label
    Friend WithEvents lblSumAcct As Label
    Friend WithEvents txtSumAcct As TextBox
    Friend WithEvents txtSumCode As TextBox
End Class
