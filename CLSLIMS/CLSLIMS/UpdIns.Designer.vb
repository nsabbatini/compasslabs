﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpdIns
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tabUpdIns = New System.Windows.Forms.TabControl()
        Me.tbDemo = New System.Windows.Forms.TabPage()
        Me.txtSpecNo = New System.Windows.Forms.TextBox()
        Me.lblSpecNo = New System.Windows.Forms.Label()
        Me.cmbSex = New System.Windows.Forms.ComboBox()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.txtZIP = New System.Windows.Forms.TextBox()
        Me.txtState = New System.Windows.Forms.TextBox()
        Me.txtCity = New System.Windows.Forms.TextBox()
        Me.txtAddr2 = New System.Windows.Forms.TextBox()
        Me.txtAddr = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblDOB = New System.Windows.Forms.Label()
        Me.lblPatName = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstPatNameSearch = New System.Windows.Forms.ListBox()
        Me.tbMC = New System.Windows.Forms.TabPage()
        Me.rbMC3 = New System.Windows.Forms.RadioButton()
        Me.rbMC2 = New System.Windows.Forms.RadioButton()
        Me.rbMC1 = New System.Windows.Forms.RadioButton()
        Me.txtMCNo = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbMD = New System.Windows.Forms.TabPage()
        Me.rbMD3 = New System.Windows.Forms.RadioButton()
        Me.rbMD2 = New System.Windows.Forms.RadioButton()
        Me.rbMD1 = New System.Windows.Forms.RadioButton()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtMDState = New System.Windows.Forms.TextBox()
        Me.txtMDNo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tb3rd = New System.Windows.Forms.TabPage()
        Me.rbTP3 = New System.Windows.Forms.RadioButton()
        Me.rbTP2 = New System.Windows.Forms.RadioButton()
        Me.rbTP1 = New System.Windows.Forms.RadioButton()
        Me.txtGPhone = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtGFName = New System.Windows.Forms.TextBox()
        Me.txtGLName = New System.Windows.Forms.TextBox()
        Me.lblInsName = New System.Windows.Forms.Label()
        Me.cmbBINS = New System.Windows.Forms.ComboBox()
        Me.txtSSN = New System.Windows.Forms.TextBox()
        Me.txtGDOB = New System.Windows.Forms.TextBox()
        Me.txtGZIP = New System.Windows.Forms.TextBox()
        Me.txtGState = New System.Windows.Forms.TextBox()
        Me.txtGCity = New System.Windows.Forms.TextBox()
        Me.txtGAddr2 = New System.Windows.Forms.TextBox()
        Me.txtGAddr = New System.Windows.Forms.TextBox()
        Me.cmbRel = New System.Windows.Forms.ComboBox()
        Me.txtGroupNo = New System.Windows.Forms.TextBox()
        Me.txtPolicyNo = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tb3rdB = New System.Windows.Forms.TabPage()
        Me.rb2TP3 = New System.Windows.Forms.RadioButton()
        Me.rb2TP2 = New System.Windows.Forms.RadioButton()
        Me.rb2TP1 = New System.Windows.Forms.RadioButton()
        Me.txt2GPhone = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txt2GFName = New System.Windows.Forms.TextBox()
        Me.txt2GLName = New System.Windows.Forms.TextBox()
        Me.lbl2InsName = New System.Windows.Forms.Label()
        Me.cmb2BINS = New System.Windows.Forms.ComboBox()
        Me.txt2SSN = New System.Windows.Forms.TextBox()
        Me.txt2GDOB = New System.Windows.Forms.TextBox()
        Me.txt2GZIP = New System.Windows.Forms.TextBox()
        Me.txt2GState = New System.Windows.Forms.TextBox()
        Me.txt2GCity = New System.Windows.Forms.TextBox()
        Me.txt2GAddr2 = New System.Windows.Forms.TextBox()
        Me.txt2GAddr = New System.Windows.Forms.TextBox()
        Me.cmb2Rel = New System.Windows.Forms.ComboBox()
        Me.txt2GroupNo = New System.Windows.Forms.TextBox()
        Me.txt2PolicyNo = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.tabUpdIns.SuspendLayout()
        Me.tbDemo.SuspendLayout()
        Me.tbMC.SuspendLayout()
        Me.tbMD.SuspendLayout()
        Me.tb3rd.SuspendLayout()
        Me.tb3rdB.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabUpdIns
        '
        Me.tabUpdIns.Controls.Add(Me.tbDemo)
        Me.tabUpdIns.Controls.Add(Me.tbMC)
        Me.tabUpdIns.Controls.Add(Me.tbMD)
        Me.tabUpdIns.Controls.Add(Me.tb3rd)
        Me.tabUpdIns.Controls.Add(Me.tb3rdB)
        Me.tabUpdIns.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabUpdIns.Location = New System.Drawing.Point(2, 1)
        Me.tabUpdIns.Name = "tabUpdIns"
        Me.tabUpdIns.SelectedIndex = 0
        Me.tabUpdIns.Size = New System.Drawing.Size(979, 472)
        Me.tabUpdIns.TabIndex = 0
        '
        'tbDemo
        '
        Me.tbDemo.Controls.Add(Me.txtSpecNo)
        Me.tbDemo.Controls.Add(Me.lblSpecNo)
        Me.tbDemo.Controls.Add(Me.cmbSex)
        Me.tbDemo.Controls.Add(Me.txtPhone)
        Me.tbDemo.Controls.Add(Me.txtZIP)
        Me.tbDemo.Controls.Add(Me.txtState)
        Me.tbDemo.Controls.Add(Me.txtCity)
        Me.tbDemo.Controls.Add(Me.txtAddr2)
        Me.tbDemo.Controls.Add(Me.txtAddr)
        Me.tbDemo.Controls.Add(Me.Label9)
        Me.tbDemo.Controls.Add(Me.lblDOB)
        Me.tbDemo.Controls.Add(Me.lblPatName)
        Me.tbDemo.Controls.Add(Me.Label8)
        Me.tbDemo.Controls.Add(Me.Label7)
        Me.tbDemo.Controls.Add(Me.Label6)
        Me.tbDemo.Controls.Add(Me.Label5)
        Me.tbDemo.Controls.Add(Me.Label4)
        Me.tbDemo.Controls.Add(Me.Label3)
        Me.tbDemo.Controls.Add(Me.Label2)
        Me.tbDemo.Controls.Add(Me.Label1)
        Me.tbDemo.Controls.Add(Me.lstPatNameSearch)
        Me.tbDemo.Location = New System.Drawing.Point(4, 28)
        Me.tbDemo.Name = "tbDemo"
        Me.tbDemo.Padding = New System.Windows.Forms.Padding(3)
        Me.tbDemo.Size = New System.Drawing.Size(971, 440)
        Me.tbDemo.TabIndex = 0
        Me.tbDemo.Text = "Demographics"
        Me.tbDemo.UseVisualStyleBackColor = True
        '
        'txtSpecNo
        '
        Me.txtSpecNo.Location = New System.Drawing.Point(161, 17)
        Me.txtSpecNo.Name = "txtSpecNo"
        Me.txtSpecNo.Size = New System.Drawing.Size(133, 27)
        Me.txtSpecNo.TabIndex = 0
        '
        'lblSpecNo
        '
        Me.lblSpecNo.AutoSize = True
        Me.lblSpecNo.Location = New System.Drawing.Point(312, 17)
        Me.lblSpecNo.Name = "lblSpecNo"
        Me.lblSpecNo.Size = New System.Drawing.Size(0, 19)
        Me.lblSpecNo.TabIndex = 19
        '
        'cmbSex
        '
        Me.cmbSex.FormattingEnabled = True
        Me.cmbSex.Items.AddRange(New Object() {"M", "F", "U"})
        Me.cmbSex.Location = New System.Drawing.Point(166, 94)
        Me.cmbSex.Name = "cmbSex"
        Me.cmbSex.Size = New System.Drawing.Size(48, 27)
        Me.cmbSex.TabIndex = 18
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(166, 254)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(158, 27)
        Me.txtPhone.TabIndex = 17
        '
        'txtZIP
        '
        Me.txtZIP.Location = New System.Drawing.Point(262, 222)
        Me.txtZIP.Name = "txtZIP"
        Me.txtZIP.Size = New System.Drawing.Size(106, 27)
        Me.txtZIP.TabIndex = 16
        '
        'txtState
        '
        Me.txtState.Location = New System.Drawing.Point(166, 222)
        Me.txtState.Name = "txtState"
        Me.txtState.Size = New System.Drawing.Size(48, 27)
        Me.txtState.TabIndex = 15
        '
        'txtCity
        '
        Me.txtCity.Location = New System.Drawing.Point(166, 190)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(175, 27)
        Me.txtCity.TabIndex = 14
        '
        'txtAddr2
        '
        Me.txtAddr2.Location = New System.Drawing.Point(166, 158)
        Me.txtAddr2.Name = "txtAddr2"
        Me.txtAddr2.Size = New System.Drawing.Size(334, 27)
        Me.txtAddr2.TabIndex = 13
        '
        'txtAddr
        '
        Me.txtAddr.Location = New System.Drawing.Point(166, 126)
        Me.txtAddr.Name = "txtAddr"
        Me.txtAddr.Size = New System.Drawing.Size(334, 27)
        Me.txtAddr.TabIndex = 12
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(114, 100)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 19)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Sex:"
        '
        'lblDOB
        '
        Me.lblDOB.AutoSize = True
        Me.lblDOB.Location = New System.Drawing.Point(579, 70)
        Me.lblDOB.Name = "lblDOB"
        Me.lblDOB.Size = New System.Drawing.Size(0, 19)
        Me.lblDOB.TabIndex = 9
        '
        'lblPatName
        '
        Me.lblPatName.AutoSize = True
        Me.lblPatName.Location = New System.Drawing.Point(166, 70)
        Me.lblPatName.Name = "lblPatName"
        Me.lblPatName.Size = New System.Drawing.Size(0, 19)
        Me.lblPatName.TabIndex = 8
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(469, 70)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 19)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Date of Birth:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(95, 259)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 19)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Phone:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(217, 224)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 19)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "ZIP:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(104, 224)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 19)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "State:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(112, 194)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 19)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "City:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(82, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 19)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Address:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(45, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Patient Name:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(54, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 19)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Specimen #:"
        '
        'lstPatNameSearch
        '
        Me.lstPatNameSearch.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPatNameSearch.FormattingEnabled = True
        Me.lstPatNameSearch.ItemHeight = 14
        Me.lstPatNameSearch.Location = New System.Drawing.Point(160, 50)
        Me.lstPatNameSearch.Name = "lstPatNameSearch"
        Me.lstPatNameSearch.Size = New System.Drawing.Size(717, 270)
        Me.lstPatNameSearch.TabIndex = 31
        Me.lstPatNameSearch.Visible = False
        '
        'tbMC
        '
        Me.tbMC.Controls.Add(Me.rbMC3)
        Me.tbMC.Controls.Add(Me.rbMC2)
        Me.tbMC.Controls.Add(Me.rbMC1)
        Me.tbMC.Controls.Add(Me.txtMCNo)
        Me.tbMC.Controls.Add(Me.Label10)
        Me.tbMC.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMC.Location = New System.Drawing.Point(4, 28)
        Me.tbMC.Name = "tbMC"
        Me.tbMC.Padding = New System.Windows.Forms.Padding(3)
        Me.tbMC.Size = New System.Drawing.Size(971, 440)
        Me.tbMC.TabIndex = 1
        Me.tbMC.Text = "Medicare"
        Me.tbMC.UseVisualStyleBackColor = True
        '
        'rbMC3
        '
        Me.rbMC3.AutoSize = True
        Me.rbMC3.Location = New System.Drawing.Point(376, 102)
        Me.rbMC3.Name = "rbMC3"
        Me.rbMC3.Size = New System.Drawing.Size(82, 23)
        Me.rbMC3.TabIndex = 5
        Me.rbMC3.TabStop = True
        Me.rbMC3.Text = "Tertiary"
        Me.rbMC3.UseVisualStyleBackColor = True
        '
        'rbMC2
        '
        Me.rbMC2.AutoSize = True
        Me.rbMC2.Location = New System.Drawing.Point(270, 102)
        Me.rbMC2.Name = "rbMC2"
        Me.rbMC2.Size = New System.Drawing.Size(100, 23)
        Me.rbMC2.TabIndex = 4
        Me.rbMC2.TabStop = True
        Me.rbMC2.Text = "Secondary"
        Me.rbMC2.UseVisualStyleBackColor = True
        '
        'rbMC1
        '
        Me.rbMC1.AutoSize = True
        Me.rbMC1.Location = New System.Drawing.Point(182, 102)
        Me.rbMC1.Name = "rbMC1"
        Me.rbMC1.Size = New System.Drawing.Size(82, 23)
        Me.rbMC1.TabIndex = 3
        Me.rbMC1.TabStop = True
        Me.rbMC1.Text = "Primary"
        Me.rbMC1.UseVisualStyleBackColor = True
        '
        'txtMCNo
        '
        Me.txtMCNo.Location = New System.Drawing.Point(182, 39)
        Me.txtMCNo.Name = "txtMCNo"
        Me.txtMCNo.Size = New System.Drawing.Size(184, 27)
        Me.txtMCNo.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(64, 39)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 19)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Medicare #:"
        '
        'tbMD
        '
        Me.tbMD.Controls.Add(Me.rbMD3)
        Me.tbMD.Controls.Add(Me.rbMD2)
        Me.tbMD.Controls.Add(Me.rbMD1)
        Me.tbMD.Controls.Add(Me.Label12)
        Me.tbMD.Controls.Add(Me.txtMDState)
        Me.tbMD.Controls.Add(Me.txtMDNo)
        Me.tbMD.Controls.Add(Me.Label11)
        Me.tbMD.Location = New System.Drawing.Point(4, 28)
        Me.tbMD.Name = "tbMD"
        Me.tbMD.Padding = New System.Windows.Forms.Padding(3)
        Me.tbMD.Size = New System.Drawing.Size(971, 440)
        Me.tbMD.TabIndex = 2
        Me.tbMD.Text = "Medicaid"
        Me.tbMD.UseVisualStyleBackColor = True
        '
        'rbMD3
        '
        Me.rbMD3.AutoSize = True
        Me.rbMD3.Location = New System.Drawing.Point(376, 125)
        Me.rbMD3.Name = "rbMD3"
        Me.rbMD3.Size = New System.Drawing.Size(82, 23)
        Me.rbMD3.TabIndex = 8
        Me.rbMD3.TabStop = True
        Me.rbMD3.Text = "Tertiary"
        Me.rbMD3.UseVisualStyleBackColor = True
        '
        'rbMD2
        '
        Me.rbMD2.AutoSize = True
        Me.rbMD2.Location = New System.Drawing.Point(270, 125)
        Me.rbMD2.Name = "rbMD2"
        Me.rbMD2.Size = New System.Drawing.Size(100, 23)
        Me.rbMD2.TabIndex = 7
        Me.rbMD2.TabStop = True
        Me.rbMD2.Text = "Secondary"
        Me.rbMD2.UseVisualStyleBackColor = True
        '
        'rbMD1
        '
        Me.rbMD1.AutoSize = True
        Me.rbMD1.Location = New System.Drawing.Point(182, 125)
        Me.rbMD1.Name = "rbMD1"
        Me.rbMD1.Size = New System.Drawing.Size(82, 23)
        Me.rbMD1.TabIndex = 6
        Me.rbMD1.TabStop = True
        Me.rbMD1.Text = "Primary"
        Me.rbMD1.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(121, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(50, 19)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "State:"
        '
        'txtMDState
        '
        Me.txtMDState.Location = New System.Drawing.Point(186, 24)
        Me.txtMDState.Name = "txtMDState"
        Me.txtMDState.Size = New System.Drawing.Size(34, 27)
        Me.txtMDState.TabIndex = 4
        '
        'txtMDNo
        '
        Me.txtMDNo.Location = New System.Drawing.Point(186, 67)
        Me.txtMDNo.Name = "txtMDNo"
        Me.txtMDNo.Size = New System.Drawing.Size(184, 27)
        Me.txtMDNo.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(78, 70)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(93, 19)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Medicaid #:"
        '
        'tb3rd
        '
        Me.tb3rd.Controls.Add(Me.rbTP3)
        Me.tb3rd.Controls.Add(Me.rbTP2)
        Me.tb3rd.Controls.Add(Me.rbTP1)
        Me.tb3rd.Controls.Add(Me.txtGPhone)
        Me.tb3rd.Controls.Add(Me.Label26)
        Me.tb3rd.Controls.Add(Me.Label25)
        Me.tb3rd.Controls.Add(Me.txtGFName)
        Me.tb3rd.Controls.Add(Me.txtGLName)
        Me.tb3rd.Controls.Add(Me.lblInsName)
        Me.tb3rd.Controls.Add(Me.cmbBINS)
        Me.tb3rd.Controls.Add(Me.txtSSN)
        Me.tb3rd.Controls.Add(Me.txtGDOB)
        Me.tb3rd.Controls.Add(Me.txtGZIP)
        Me.tb3rd.Controls.Add(Me.txtGState)
        Me.tb3rd.Controls.Add(Me.txtGCity)
        Me.tb3rd.Controls.Add(Me.txtGAddr2)
        Me.tb3rd.Controls.Add(Me.txtGAddr)
        Me.tb3rd.Controls.Add(Me.cmbRel)
        Me.tb3rd.Controls.Add(Me.txtGroupNo)
        Me.tb3rd.Controls.Add(Me.txtPolicyNo)
        Me.tb3rd.Controls.Add(Me.Label23)
        Me.tb3rd.Controls.Add(Me.Label22)
        Me.tb3rd.Controls.Add(Me.Label21)
        Me.tb3rd.Controls.Add(Me.Label20)
        Me.tb3rd.Controls.Add(Me.Label19)
        Me.tb3rd.Controls.Add(Me.Label18)
        Me.tb3rd.Controls.Add(Me.Label17)
        Me.tb3rd.Controls.Add(Me.Label16)
        Me.tb3rd.Controls.Add(Me.Label15)
        Me.tb3rd.Controls.Add(Me.Label14)
        Me.tb3rd.Controls.Add(Me.Label13)
        Me.tb3rd.Location = New System.Drawing.Point(4, 28)
        Me.tb3rd.Name = "tb3rd"
        Me.tb3rd.Padding = New System.Windows.Forms.Padding(3)
        Me.tb3rd.Size = New System.Drawing.Size(971, 440)
        Me.tb3rd.TabIndex = 3
        Me.tb3rd.Text = "3rd Party"
        Me.tb3rd.UseVisualStyleBackColor = True
        '
        'rbTP3
        '
        Me.rbTP3.AutoSize = True
        Me.rbTP3.Location = New System.Drawing.Point(393, 402)
        Me.rbTP3.Name = "rbTP3"
        Me.rbTP3.Size = New System.Drawing.Size(82, 23)
        Me.rbTP3.TabIndex = 38
        Me.rbTP3.TabStop = True
        Me.rbTP3.Text = "Tertiary"
        Me.rbTP3.UseVisualStyleBackColor = True
        '
        'rbTP2
        '
        Me.rbTP2.AutoSize = True
        Me.rbTP2.Location = New System.Drawing.Point(287, 402)
        Me.rbTP2.Name = "rbTP2"
        Me.rbTP2.Size = New System.Drawing.Size(100, 23)
        Me.rbTP2.TabIndex = 37
        Me.rbTP2.TabStop = True
        Me.rbTP2.Text = "Secondary"
        Me.rbTP2.UseVisualStyleBackColor = True
        '
        'rbTP1
        '
        Me.rbTP1.AutoSize = True
        Me.rbTP1.Location = New System.Drawing.Point(199, 402)
        Me.rbTP1.Name = "rbTP1"
        Me.rbTP1.Size = New System.Drawing.Size(82, 23)
        Me.rbTP1.TabIndex = 36
        Me.rbTP1.TabStop = True
        Me.rbTP1.Text = "Primary"
        Me.rbTP1.UseVisualStyleBackColor = True
        '
        'txtGPhone
        '
        Me.txtGPhone.Location = New System.Drawing.Point(180, 360)
        Me.txtGPhone.Name = "txtGPhone"
        Me.txtGPhone.Size = New System.Drawing.Size(118, 27)
        Me.txtGPhone.TabIndex = 35
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(115, 363)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(59, 19)
        Me.Label26.TabIndex = 34
        Me.Label26.Text = "Phone:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(386, 76)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(45, 19)
        Me.Label25.TabIndex = 33
        Me.Label25.Text = "First:"
        '
        'txtGFName
        '
        Me.txtGFName.Location = New System.Drawing.Point(437, 73)
        Me.txtGFName.Name = "txtGFName"
        Me.txtGFName.Size = New System.Drawing.Size(182, 27)
        Me.txtGFName.TabIndex = 31
        '
        'txtGLName
        '
        Me.txtGLName.Location = New System.Drawing.Point(180, 73)
        Me.txtGLName.Name = "txtGLName"
        Me.txtGLName.Size = New System.Drawing.Size(182, 27)
        Me.txtGLName.TabIndex = 30
        '
        'lblInsName
        '
        Me.lblInsName.AutoSize = True
        Me.lblInsName.Location = New System.Drawing.Point(342, 36)
        Me.lblInsName.Name = "lblInsName"
        Me.lblInsName.Size = New System.Drawing.Size(90, 19)
        Me.lblInsName.TabIndex = 29
        Me.lblInsName.Text = "123456789"
        '
        'cmbBINS
        '
        Me.cmbBINS.FormattingEnabled = True
        Me.cmbBINS.Location = New System.Drawing.Point(180, 33)
        Me.cmbBINS.Name = "cmbBINS"
        Me.cmbBINS.Size = New System.Drawing.Size(137, 27)
        Me.cmbBINS.TabIndex = 28
        '
        'txtSSN
        '
        Me.txtSSN.Location = New System.Drawing.Point(490, 323)
        Me.txtSSN.Name = "txtSSN"
        Me.txtSSN.Size = New System.Drawing.Size(170, 27)
        Me.txtSSN.TabIndex = 27
        '
        'txtGDOB
        '
        Me.txtGDOB.Location = New System.Drawing.Point(180, 326)
        Me.txtGDOB.Name = "txtGDOB"
        Me.txtGDOB.Size = New System.Drawing.Size(118, 27)
        Me.txtGDOB.TabIndex = 26
        '
        'txtGZIP
        '
        Me.txtGZIP.Location = New System.Drawing.Point(266, 296)
        Me.txtGZIP.Name = "txtGZIP"
        Me.txtGZIP.Size = New System.Drawing.Size(96, 27)
        Me.txtGZIP.TabIndex = 25
        '
        'txtGState
        '
        Me.txtGState.Location = New System.Drawing.Point(180, 296)
        Me.txtGState.Name = "txtGState"
        Me.txtGState.Size = New System.Drawing.Size(33, 27)
        Me.txtGState.TabIndex = 24
        '
        'txtGCity
        '
        Me.txtGCity.Location = New System.Drawing.Point(180, 263)
        Me.txtGCity.Name = "txtGCity"
        Me.txtGCity.Size = New System.Drawing.Size(207, 27)
        Me.txtGCity.TabIndex = 23
        '
        'txtGAddr2
        '
        Me.txtGAddr2.Location = New System.Drawing.Point(180, 234)
        Me.txtGAddr2.Name = "txtGAddr2"
        Me.txtGAddr2.Size = New System.Drawing.Size(313, 27)
        Me.txtGAddr2.TabIndex = 22
        '
        'txtGAddr
        '
        Me.txtGAddr.Location = New System.Drawing.Point(180, 209)
        Me.txtGAddr.Name = "txtGAddr"
        Me.txtGAddr.Size = New System.Drawing.Size(313, 27)
        Me.txtGAddr.TabIndex = 21
        '
        'cmbRel
        '
        Me.cmbRel.FormattingEnabled = True
        Me.cmbRel.Items.AddRange(New Object() {"SE", "SP", "PA", "GU", "CH", "OT", "UN"})
        Me.cmbRel.Location = New System.Drawing.Point(180, 163)
        Me.cmbRel.Name = "cmbRel"
        Me.cmbRel.Size = New System.Drawing.Size(48, 27)
        Me.cmbRel.TabIndex = 19
        '
        'txtGroupNo
        '
        Me.txtGroupNo.Location = New System.Drawing.Point(180, 136)
        Me.txtGroupNo.Name = "txtGroupNo"
        Me.txtGroupNo.Size = New System.Drawing.Size(182, 27)
        Me.txtGroupNo.TabIndex = 12
        '
        'txtPolicyNo
        '
        Me.txtPolicyNo.Location = New System.Drawing.Point(180, 106)
        Me.txtPolicyNo.Name = "txtPolicyNo"
        Me.txtPolicyNo.Size = New System.Drawing.Size(182, 27)
        Me.txtPolicyNo.TabIndex = 11
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(314, 326)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(179, 19)
        Me.Label23.TabIndex = 10
        Me.Label23.Text = "Social Security Number:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(70, 326)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(104, 19)
        Me.Label22.TabIndex = 9
        Me.Label22.Text = "Date of Birth:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(230, 296)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(39, 19)
        Me.Label21.TabIndex = 8
        Me.Label21.Text = "ZIP:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(124, 296)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(50, 19)
        Me.Label20.TabIndex = 7
        Me.Label20.Text = "State:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(132, 266)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 19)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "City:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(102, 212)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(72, 19)
        Me.Label18.TabIndex = 5
        Me.Label18.Text = "Address:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(41, 76)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(133, 19)
        Me.Label17.TabIndex = 4
        Me.Label17.Text = "Card Holder Last:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(73, 166)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(101, 19)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Relationship:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(98, 136)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(76, 19)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Group #:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(101, 106)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(73, 19)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Policy #:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(18, 41)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(156, 19)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Insurance Company:"
        '
        'tb3rdB
        '
        Me.tb3rdB.Controls.Add(Me.rb2TP3)
        Me.tb3rdB.Controls.Add(Me.rb2TP2)
        Me.tb3rdB.Controls.Add(Me.rb2TP1)
        Me.tb3rdB.Controls.Add(Me.txt2GPhone)
        Me.tb3rdB.Controls.Add(Me.Label24)
        Me.tb3rdB.Controls.Add(Me.Label27)
        Me.tb3rdB.Controls.Add(Me.txt2GFName)
        Me.tb3rdB.Controls.Add(Me.txt2GLName)
        Me.tb3rdB.Controls.Add(Me.lbl2InsName)
        Me.tb3rdB.Controls.Add(Me.cmb2BINS)
        Me.tb3rdB.Controls.Add(Me.txt2SSN)
        Me.tb3rdB.Controls.Add(Me.txt2GDOB)
        Me.tb3rdB.Controls.Add(Me.txt2GZIP)
        Me.tb3rdB.Controls.Add(Me.txt2GState)
        Me.tb3rdB.Controls.Add(Me.txt2GCity)
        Me.tb3rdB.Controls.Add(Me.txt2GAddr2)
        Me.tb3rdB.Controls.Add(Me.txt2GAddr)
        Me.tb3rdB.Controls.Add(Me.cmb2Rel)
        Me.tb3rdB.Controls.Add(Me.txt2GroupNo)
        Me.tb3rdB.Controls.Add(Me.txt2PolicyNo)
        Me.tb3rdB.Controls.Add(Me.Label29)
        Me.tb3rdB.Controls.Add(Me.Label30)
        Me.tb3rdB.Controls.Add(Me.Label31)
        Me.tb3rdB.Controls.Add(Me.Label32)
        Me.tb3rdB.Controls.Add(Me.Label33)
        Me.tb3rdB.Controls.Add(Me.Label34)
        Me.tb3rdB.Controls.Add(Me.Label35)
        Me.tb3rdB.Controls.Add(Me.Label36)
        Me.tb3rdB.Controls.Add(Me.Label37)
        Me.tb3rdB.Controls.Add(Me.Label38)
        Me.tb3rdB.Controls.Add(Me.Label39)
        Me.tb3rdB.Location = New System.Drawing.Point(4, 28)
        Me.tb3rdB.Name = "tb3rdB"
        Me.tb3rdB.Size = New System.Drawing.Size(971, 440)
        Me.tb3rdB.TabIndex = 4
        Me.tb3rdB.Text = "3rd Party  (Two)"
        Me.tb3rdB.UseVisualStyleBackColor = True
        '
        'rb2TP3
        '
        Me.rb2TP3.AutoSize = True
        Me.rb2TP3.Location = New System.Drawing.Point(398, 389)
        Me.rb2TP3.Name = "rb2TP3"
        Me.rb2TP3.Size = New System.Drawing.Size(82, 23)
        Me.rb2TP3.TabIndex = 69
        Me.rb2TP3.TabStop = True
        Me.rb2TP3.Text = "Tertiary"
        Me.rb2TP3.UseVisualStyleBackColor = True
        '
        'rb2TP2
        '
        Me.rb2TP2.AutoSize = True
        Me.rb2TP2.Location = New System.Drawing.Point(292, 389)
        Me.rb2TP2.Name = "rb2TP2"
        Me.rb2TP2.Size = New System.Drawing.Size(100, 23)
        Me.rb2TP2.TabIndex = 68
        Me.rb2TP2.TabStop = True
        Me.rb2TP2.Text = "Secondary"
        Me.rb2TP2.UseVisualStyleBackColor = True
        '
        'rb2TP1
        '
        Me.rb2TP1.AutoSize = True
        Me.rb2TP1.Location = New System.Drawing.Point(204, 389)
        Me.rb2TP1.Name = "rb2TP1"
        Me.rb2TP1.Size = New System.Drawing.Size(82, 23)
        Me.rb2TP1.TabIndex = 67
        Me.rb2TP1.TabStop = True
        Me.rb2TP1.Text = "Primary"
        Me.rb2TP1.UseVisualStyleBackColor = True
        '
        'txt2GPhone
        '
        Me.txt2GPhone.Location = New System.Drawing.Point(185, 347)
        Me.txt2GPhone.Name = "txt2GPhone"
        Me.txt2GPhone.Size = New System.Drawing.Size(118, 27)
        Me.txt2GPhone.TabIndex = 66
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(120, 350)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(59, 19)
        Me.Label24.TabIndex = 65
        Me.Label24.Text = "Phone:"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(391, 63)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(45, 19)
        Me.Label27.TabIndex = 64
        Me.Label27.Text = "First:"
        '
        'txt2GFName
        '
        Me.txt2GFName.Location = New System.Drawing.Point(442, 60)
        Me.txt2GFName.Name = "txt2GFName"
        Me.txt2GFName.Size = New System.Drawing.Size(182, 27)
        Me.txt2GFName.TabIndex = 63
        '
        'txt2GLName
        '
        Me.txt2GLName.Location = New System.Drawing.Point(185, 60)
        Me.txt2GLName.Name = "txt2GLName"
        Me.txt2GLName.Size = New System.Drawing.Size(182, 27)
        Me.txt2GLName.TabIndex = 62
        '
        'lbl2InsName
        '
        Me.lbl2InsName.AutoSize = True
        Me.lbl2InsName.Location = New System.Drawing.Point(347, 23)
        Me.lbl2InsName.Name = "lbl2InsName"
        Me.lbl2InsName.Size = New System.Drawing.Size(90, 19)
        Me.lbl2InsName.TabIndex = 61
        Me.lbl2InsName.Text = "123456789"
        '
        'cmb2BINS
        '
        Me.cmb2BINS.FormattingEnabled = True
        Me.cmb2BINS.Location = New System.Drawing.Point(185, 20)
        Me.cmb2BINS.Name = "cmb2BINS"
        Me.cmb2BINS.Size = New System.Drawing.Size(137, 27)
        Me.cmb2BINS.TabIndex = 60
        '
        'txt2SSN
        '
        Me.txt2SSN.Location = New System.Drawing.Point(495, 310)
        Me.txt2SSN.Name = "txt2SSN"
        Me.txt2SSN.Size = New System.Drawing.Size(170, 27)
        Me.txt2SSN.TabIndex = 59
        '
        'txt2GDOB
        '
        Me.txt2GDOB.Location = New System.Drawing.Point(185, 313)
        Me.txt2GDOB.Name = "txt2GDOB"
        Me.txt2GDOB.Size = New System.Drawing.Size(118, 27)
        Me.txt2GDOB.TabIndex = 58
        '
        'txt2GZIP
        '
        Me.txt2GZIP.Location = New System.Drawing.Point(271, 283)
        Me.txt2GZIP.Name = "txt2GZIP"
        Me.txt2GZIP.Size = New System.Drawing.Size(96, 27)
        Me.txt2GZIP.TabIndex = 57
        '
        'txt2GState
        '
        Me.txt2GState.Location = New System.Drawing.Point(185, 283)
        Me.txt2GState.Name = "txt2GState"
        Me.txt2GState.Size = New System.Drawing.Size(33, 27)
        Me.txt2GState.TabIndex = 56
        '
        'txt2GCity
        '
        Me.txt2GCity.Location = New System.Drawing.Point(185, 250)
        Me.txt2GCity.Name = "txt2GCity"
        Me.txt2GCity.Size = New System.Drawing.Size(207, 27)
        Me.txt2GCity.TabIndex = 55
        '
        'txt2GAddr2
        '
        Me.txt2GAddr2.Location = New System.Drawing.Point(185, 221)
        Me.txt2GAddr2.Name = "txt2GAddr2"
        Me.txt2GAddr2.Size = New System.Drawing.Size(313, 27)
        Me.txt2GAddr2.TabIndex = 54
        '
        'txt2GAddr
        '
        Me.txt2GAddr.Location = New System.Drawing.Point(185, 196)
        Me.txt2GAddr.Name = "txt2GAddr"
        Me.txt2GAddr.Size = New System.Drawing.Size(313, 27)
        Me.txt2GAddr.TabIndex = 53
        '
        'cmb2Rel
        '
        Me.cmb2Rel.FormattingEnabled = True
        Me.cmb2Rel.Items.AddRange(New Object() {"SE", "SP", "PA", "GU", "CH", "OT", "UN"})
        Me.cmb2Rel.Location = New System.Drawing.Point(185, 150)
        Me.cmb2Rel.Name = "cmb2Rel"
        Me.cmb2Rel.Size = New System.Drawing.Size(48, 27)
        Me.cmb2Rel.TabIndex = 52
        '
        'txt2GroupNo
        '
        Me.txt2GroupNo.Location = New System.Drawing.Point(185, 123)
        Me.txt2GroupNo.Name = "txt2GroupNo"
        Me.txt2GroupNo.Size = New System.Drawing.Size(182, 27)
        Me.txt2GroupNo.TabIndex = 51
        '
        'txt2PolicyNo
        '
        Me.txt2PolicyNo.Location = New System.Drawing.Point(185, 93)
        Me.txt2PolicyNo.Name = "txt2PolicyNo"
        Me.txt2PolicyNo.Size = New System.Drawing.Size(182, 27)
        Me.txt2PolicyNo.TabIndex = 50
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(319, 313)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(179, 19)
        Me.Label29.TabIndex = 49
        Me.Label29.Text = "Social Security Number:"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(75, 313)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(104, 19)
        Me.Label30.TabIndex = 48
        Me.Label30.Text = "Date of Birth:"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(235, 283)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(39, 19)
        Me.Label31.TabIndex = 47
        Me.Label31.Text = "ZIP:"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(129, 283)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(50, 19)
        Me.Label32.TabIndex = 46
        Me.Label32.Text = "State:"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(137, 253)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(42, 19)
        Me.Label33.TabIndex = 45
        Me.Label33.Text = "City:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(107, 199)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(72, 19)
        Me.Label34.TabIndex = 44
        Me.Label34.Text = "Address:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(46, 63)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(133, 19)
        Me.Label35.TabIndex = 43
        Me.Label35.Text = "Card Holder Last:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(78, 153)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(101, 19)
        Me.Label36.TabIndex = 42
        Me.Label36.Text = "Relationship:"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(103, 123)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(76, 19)
        Me.Label37.TabIndex = 41
        Me.Label37.Text = "Group #:"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(106, 93)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(73, 19)
        Me.Label38.TabIndex = 40
        Me.Label38.Text = "Policy #:"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(23, 28)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(156, 19)
        Me.Label39.TabIndex = 39
        Me.Label39.Text = "Insurance Company:"
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(735, 475)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 31)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "SAVE"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(825, 475)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 31)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'UpdIns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(981, 512)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.tabUpdIns)
        Me.Name = "UpdIns"
        Me.Text = "Update Insurance Info"
        Me.tabUpdIns.ResumeLayout(False)
        Me.tbDemo.ResumeLayout(False)
        Me.tbDemo.PerformLayout()
        Me.tbMC.ResumeLayout(False)
        Me.tbMC.PerformLayout()
        Me.tbMD.ResumeLayout(False)
        Me.tbMD.PerformLayout()
        Me.tb3rd.ResumeLayout(False)
        Me.tb3rd.PerformLayout()
        Me.tb3rdB.ResumeLayout(False)
        Me.tb3rdB.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents tabUpdIns As TabControl
    Friend WithEvents tbDemo As TabPage
    Friend WithEvents tbMC As TabPage
    Friend WithEvents tbMD As TabPage
    Friend WithEvents tb3rd As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents lblDOB As Label
    Friend WithEvents lblPatName As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents txtZIP As TextBox
    Friend WithEvents txtState As TextBox
    Friend WithEvents txtCity As TextBox
    Friend WithEvents txtAddr2 As TextBox
    Friend WithEvents txtAddr As TextBox
    Friend WithEvents cmbSex As ComboBox
    Friend WithEvents lblSpecNo As Label
    Friend WithEvents txtMCNo As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtMDState As TextBox
    Friend WithEvents txtMDNo As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents btnSave As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents txtSpecNo As TextBox
    Friend WithEvents lstPatNameSearch As ListBox
    Friend WithEvents cmbRel As ComboBox
    Friend WithEvents txtGroupNo As TextBox
    Friend WithEvents txtPolicyNo As TextBox
    Friend WithEvents txtSSN As TextBox
    Friend WithEvents txtGDOB As TextBox
    Friend WithEvents txtGZIP As TextBox
    Friend WithEvents txtGState As TextBox
    Friend WithEvents txtGCity As TextBox
    Friend WithEvents txtGAddr2 As TextBox
    Friend WithEvents txtGAddr As TextBox
    Friend WithEvents lblInsName As Label
    Friend WithEvents cmbBINS As ComboBox
    Friend WithEvents Label25 As Label
    Friend WithEvents txtGFName As TextBox
    Friend WithEvents txtGLName As TextBox
    Friend WithEvents txtGPhone As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents rbMC3 As RadioButton
    Friend WithEvents rbMC2 As RadioButton
    Friend WithEvents rbMC1 As RadioButton
    Friend WithEvents rbMD3 As RadioButton
    Friend WithEvents rbMD2 As RadioButton
    Friend WithEvents rbMD1 As RadioButton
    Friend WithEvents rbTP3 As RadioButton
    Friend WithEvents rbTP2 As RadioButton
    Friend WithEvents rbTP1 As RadioButton
    Friend WithEvents tb3rdB As TabPage
    Friend WithEvents rb2TP3 As RadioButton
    Friend WithEvents rb2TP2 As RadioButton
    Friend WithEvents rb2TP1 As RadioButton
    Friend WithEvents txt2GPhone As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents txt2GFName As TextBox
    Friend WithEvents txt2GLName As TextBox
    Friend WithEvents lbl2InsName As Label
    Friend WithEvents cmb2BINS As ComboBox
    Friend WithEvents txt2SSN As TextBox
    Friend WithEvents txt2GDOB As TextBox
    Friend WithEvents txt2GZIP As TextBox
    Friend WithEvents txt2GState As TextBox
    Friend WithEvents txt2GCity As TextBox
    Friend WithEvents txt2GAddr2 As TextBox
    Friend WithEvents txt2GAddr As TextBox
    Friend WithEvents cmb2Rel As ComboBox
    Friend WithEvents txt2GroupNo As TextBox
    Friend WithEvents txt2PolicyNo As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
End Class
