﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmAnthem

    Private Sub txtMonYY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartDt.KeyPress

        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtStartDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtStartDt.Text) Then
                MsgBox("Invalid Start Date.")
                txtEndDt.Focus()
            End If
        End If
    End Sub
    Private Sub txtEndDt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndDt.KeyPress
        If Asc(e.KeyChar) <> 13 Then
            Exit Sub
        End If
        If txtEndDt.Text = "" Then
            Exit Sub
        Else
            If Not IsDate(txtEndDt.Text) Then
                MsgBox("Invalid End Date.")
                txtEndDt.Focus()
            End If
        End If
    End Sub
    Private Sub frmAnthem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtStartDt.Text = ""
        txtEndDt.Text = ""
        lstAnthem.Items.Clear()
        txtStartDt.Focus()
    End Sub

    Private Sub btnRpt_Click(sender As Object, e As EventArgs) Handles btnRpt.Click
        Dim dFirst As Date, dLast As Date, arrCmt(50) As String, sFirst As String, sLast As String, x As String = ""

        lstAnthem.Items.Clear()

        dFirst = CDate(txtStartDt.Text) : dLast = CDate(txtEndDt.Text)
        sFirst = DatePart(DateInterval.Year, dFirst) & IIf(DatePart(DateInterval.Month, dFirst) < 10, "0", "") & DatePart(DateInterval.Month, dFirst) & IIf(DatePart(DateInterval.Day, dFirst) < 10, "0", "") & DatePart(DateInterval.Day, dFirst)
        sLast = DatePart(DateInterval.Year, dLast) & IIf(DatePart(DateInterval.Month, dLast) < 10, "0", "") & DatePart(DateInterval.Month, dLast) & IIf(DatePart(DateInterval.Day, dLast) < 10, "0", "") & DatePart(DateInterval.Day, dLast)
        Dim Sel As String
        Sel = "select pspecno,substring(psrcvdt,5,2)+'/'+substring(psrcvdt,7,2)+'/'+substring(psrcvdt,1,4),substring(pscdta,5,2)+'/'+substring(pscdta,7,2)+'/'+substring(pscdta,1,4) from pspec where puid+pefdt in (select puid+pefdt from pipi where BINS='ANTHEM') and psact=1 and substring(psrcvdt,1,8) between '" & sFirst & "' and '" & sLast & "'"

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim rs As SqlDataReader = Cmd.ExecuteReader
            Do While rs.Read()
                lstAnthem.Items.Add(rs(0) & Space(8 - Len(rs(0))) & rs(1) & Space(14 - Len(rs(1))) & rs(2))
                'lstAnthem.Items.Add("-----------------------------------------------------------------------------")
            Loop
            rs.Close()
            Conn.Close()
        End Using
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Dim i As Integer
        If System.IO.File.Exists("C:\CLSLIMS\Anthem.txt") Then
            System.IO.File.Delete("C:\CLSLIMS\Anthem.txt")
        End If
        Dim flMonStat As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("C:\CLSLIMS\Anthem.txt", False)
        For i = 0 To lstAnthem.Items.Count - 1
            flMonStat.WriteLine(lstAnthem.Items(i))
        Next i
        flMonStat.Close()
        Dim procID As Integer

        procID = Shell("C:\Windows\system32\notepad.exe c:\clslims\Anthem.txt", AppWinStyle.NormalFocus)

        SendKeys.SendWait("^(p)")
        Threading.Thread.Sleep(2000)
        SendKeys.Send("~")
        Threading.Thread.Sleep(2000)
        SendKeys.Send("%(f)")
        Threading.Thread.Sleep(1000)
        SendKeys.Send("x")
        Me.Close()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class