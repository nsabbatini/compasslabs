﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSOEGridInq
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.cbOralSedHyp = New System.Windows.Forms.CheckBox()
        Me.cbOralPregaba = New System.Windows.Forms.CheckBox()
        Me.cbOralGaba = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiPsych = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiEpi = New System.Windows.Forms.CheckBox()
        Me.cbOralAnti = New System.Windows.Forms.CheckBox()
        Me.cbOralAntiTri = New System.Windows.Forms.CheckBox()
        Me.cbOralADSer = New System.Windows.Forms.CheckBox()
        Me.cbOralAlk = New System.Windows.Forms.CheckBox()
        Me.cbOralTram = New System.Windows.Forms.CheckBox()
        Me.cbOralMtd = New System.Windows.Forms.CheckBox()
        Me.cbOralTap = New System.Windows.Forms.CheckBox()
        Me.cbOralSkel = New System.Windows.Forms.CheckBox()
        Me.cbOralPro = New System.Windows.Forms.CheckBox()
        Me.cbOralPhen = New System.Windows.Forms.CheckBox()
        Me.cbOralOxy = New System.Windows.Forms.CheckBox()
        Me.cbOralOpiAnal = New System.Windows.Forms.CheckBox()
        Me.cbOralOpi = New System.Windows.Forms.CheckBox()
        Me.cbOralMethy = New System.Windows.Forms.CheckBox()
        Me.cbOralMethAmp = New System.Windows.Forms.CheckBox()
        Me.cbOralHero = New System.Windows.Forms.CheckBox()
        Me.cbOralFent = New System.Windows.Forms.CheckBox()
        Me.cbOralCOC = New System.Windows.Forms.CheckBox()
        Me.cbOralTHC = New System.Windows.Forms.CheckBox()
        Me.cbOralBup = New System.Windows.Forms.CheckBox()
        Me.cbOralBenz = New System.Windows.Forms.CheckBox()
        Me.cbOralAmp = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(662, 301)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(83, 34)
        Me.btnExit.TabIndex = 132
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'cbOralSedHyp
        '
        Me.cbOralSedHyp.AutoSize = True
        Me.cbOralSedHyp.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralSedHyp.Location = New System.Drawing.Point(573, 172)
        Me.cbOralSedHyp.Name = "cbOralSedHyp"
        Me.cbOralSedHyp.Size = New System.Drawing.Size(172, 18)
        Me.cbOralSedHyp.TabIndex = 160
        Me.cbOralSedHyp.Text = "Oral Sedative Hypnotics"
        Me.cbOralSedHyp.UseVisualStyleBackColor = True
        '
        'cbOralPregaba
        '
        Me.cbOralPregaba.AutoSize = True
        Me.cbOralPregaba.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPregaba.Location = New System.Drawing.Point(573, 121)
        Me.cbOralPregaba.Name = "cbOralPregaba"
        Me.cbOralPregaba.Size = New System.Drawing.Size(118, 18)
        Me.cbOralPregaba.TabIndex = 159
        Me.cbOralPregaba.Text = "Oral Pregabalin"
        Me.cbOralPregaba.UseVisualStyleBackColor = True
        '
        'cbOralGaba
        '
        Me.cbOralGaba.AutoSize = True
        Me.cbOralGaba.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralGaba.Location = New System.Drawing.Point(286, 148)
        Me.cbOralGaba.Name = "cbOralGaba"
        Me.cbOralGaba.Size = New System.Drawing.Size(196, 18)
        Me.cbOralGaba.TabIndex = 158
        Me.cbOralGaba.Text = "Oral Gabapentin, Non-Blood"
        Me.cbOralGaba.UseVisualStyleBackColor = True
        '
        'cbOralAntiPsych
        '
        Me.cbOralAntiPsych.AutoSize = True
        Me.cbOralAntiPsych.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiPsych.Location = New System.Drawing.Point(16, 175)
        Me.cbOralAntiPsych.Name = "cbOralAntiPsych"
        Me.cbOralAntiPsych.Size = New System.Drawing.Size(144, 18)
        Me.cbOralAntiPsych.TabIndex = 157
        Me.cbOralAntiPsych.Text = "Oral Antipsychotics"
        Me.cbOralAntiPsych.UseVisualStyleBackColor = True
        '
        'cbOralAntiEpi
        '
        Me.cbOralAntiEpi.AutoSize = True
        Me.cbOralAntiEpi.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiEpi.Location = New System.Drawing.Point(16, 148)
        Me.cbOralAntiEpi.Name = "cbOralAntiEpi"
        Me.cbOralAntiEpi.Size = New System.Drawing.Size(137, 18)
        Me.cbOralAntiEpi.TabIndex = 156
        Me.cbOralAntiEpi.Text = "Oral Antiepileptics"
        Me.cbOralAntiEpi.UseVisualStyleBackColor = True
        '
        'cbOralAnti
        '
        Me.cbOralAnti.AutoSize = True
        Me.cbOralAnti.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAnti.Location = New System.Drawing.Point(16, 121)
        Me.cbOralAnti.Name = "cbOralAnti"
        Me.cbOralAnti.Size = New System.Drawing.Size(154, 18)
        Me.cbOralAnti.TabIndex = 155
        Me.cbOralAnti.Text = "Oral Antidepressants"
        Me.cbOralAnti.UseVisualStyleBackColor = True
        '
        'cbOralAntiTri
        '
        Me.cbOralAntiTri.AutoSize = True
        Me.cbOralAntiTri.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAntiTri.Location = New System.Drawing.Point(16, 94)
        Me.cbOralAntiTri.Name = "cbOralAntiTri"
        Me.cbOralAntiTri.Size = New System.Drawing.Size(208, 18)
        Me.cbOralAntiTri.TabIndex = 154
        Me.cbOralAntiTri.Text = "Oral Antidepressants, Tricyclic"
        Me.cbOralAntiTri.UseVisualStyleBackColor = True
        '
        'cbOralADSer
        '
        Me.cbOralADSer.AutoSize = True
        Me.cbOralADSer.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralADSer.Location = New System.Drawing.Point(16, 67)
        Me.cbOralADSer.Name = "cbOralADSer"
        Me.cbOralADSer.Size = New System.Drawing.Size(241, 18)
        Me.cbOralADSer.TabIndex = 153
        Me.cbOralADSer.Text = "Oral Antidepressants, Serotonergic"
        Me.cbOralADSer.UseVisualStyleBackColor = True
        '
        'cbOralAlk
        '
        Me.cbOralAlk.AutoSize = True
        Me.cbOralAlk.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAlk.Location = New System.Drawing.Point(16, 17)
        Me.cbOralAlk.Name = "cbOralAlk"
        Me.cbOralAlk.Size = New System.Drawing.Size(181, 18)
        Me.cbOralAlk.TabIndex = 152
        Me.cbOralAlk.Text = "Oral Alkaloids Unspecified"
        Me.cbOralAlk.UseVisualStyleBackColor = True
        '
        'cbOralTram
        '
        Me.cbOralTram.AutoSize = True
        Me.cbOralTram.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTram.Location = New System.Drawing.Point(573, 250)
        Me.cbOralTram.Name = "cbOralTram"
        Me.cbOralTram.Size = New System.Drawing.Size(110, 18)
        Me.cbOralTram.TabIndex = 150
        Me.cbOralTram.Text = "Oral Tramadol"
        Me.cbOralTram.UseVisualStyleBackColor = True
        '
        'cbOralMtd
        '
        Me.cbOralMtd.AutoSize = True
        Me.cbOralMtd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMtd.Location = New System.Drawing.Point(286, 199)
        Me.cbOralMtd.Name = "cbOralMtd"
        Me.cbOralMtd.Size = New System.Drawing.Size(124, 18)
        Me.cbOralMtd.TabIndex = 149
        Me.cbOralMtd.Text = "Oral Methadone"
        Me.cbOralMtd.UseVisualStyleBackColor = True
        '
        'cbOralTap
        '
        Me.cbOralTap.AutoSize = True
        Me.cbOralTap.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTap.Location = New System.Drawing.Point(573, 226)
        Me.cbOralTap.Name = "cbOralTap"
        Me.cbOralTap.Size = New System.Drawing.Size(123, 18)
        Me.cbOralTap.TabIndex = 148
        Me.cbOralTap.Text = "Oral Tapentadol"
        Me.cbOralTap.UseVisualStyleBackColor = True
        '
        'cbOralSkel
        '
        Me.cbOralSkel.AutoSize = True
        Me.cbOralSkel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralSkel.Location = New System.Drawing.Point(573, 199)
        Me.cbOralSkel.Name = "cbOralSkel"
        Me.cbOralSkel.Size = New System.Drawing.Size(211, 18)
        Me.cbOralSkel.TabIndex = 147
        Me.cbOralSkel.Text = "Oral Skeletal Muscle Relaxants"
        Me.cbOralSkel.UseVisualStyleBackColor = True
        '
        'cbOralPro
        '
        Me.cbOralPro.AutoSize = True
        Me.cbOralPro.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPro.Location = New System.Drawing.Point(573, 148)
        Me.cbOralPro.Name = "cbOralPro"
        Me.cbOralPro.Size = New System.Drawing.Size(143, 18)
        Me.cbOralPro.TabIndex = 146
        Me.cbOralPro.Text = "Oral Propoxyphene"
        Me.cbOralPro.UseVisualStyleBackColor = True
        '
        'cbOralPhen
        '
        Me.cbOralPhen.AutoSize = True
        Me.cbOralPhen.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralPhen.Location = New System.Drawing.Point(573, 94)
        Me.cbOralPhen.Name = "cbOralPhen"
        Me.cbOralPhen.Size = New System.Drawing.Size(136, 18)
        Me.cbOralPhen.TabIndex = 145
        Me.cbOralPhen.Text = "Oral Phencyclidine"
        Me.cbOralPhen.UseVisualStyleBackColor = True
        '
        'cbOralOxy
        '
        Me.cbOralOxy.AutoSize = True
        Me.cbOralOxy.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOxy.Location = New System.Drawing.Point(573, 67)
        Me.cbOralOxy.Name = "cbOralOxy"
        Me.cbOralOxy.Size = New System.Drawing.Size(122, 18)
        Me.cbOralOxy.TabIndex = 144
        Me.cbOralOxy.Text = "Oral Oxycodone"
        Me.cbOralOxy.UseVisualStyleBackColor = True
        '
        'cbOralOpiAnal
        '
        Me.cbOralOpiAnal.AutoSize = True
        Me.cbOralOpiAnal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOpiAnal.Location = New System.Drawing.Point(573, 41)
        Me.cbOralOpiAnal.Name = "cbOralOpiAnal"
        Me.cbOralOpiAnal.Size = New System.Drawing.Size(238, 18)
        Me.cbOralOpiAnal.TabIndex = 143
        Me.cbOralOpiAnal.Text = "Oral Opioids and Opiate Analogues"
        Me.cbOralOpiAnal.UseVisualStyleBackColor = True
        '
        'cbOralOpi
        '
        Me.cbOralOpi.AutoSize = True
        Me.cbOralOpi.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralOpi.Location = New System.Drawing.Point(573, 17)
        Me.cbOralOpi.Name = "cbOralOpi"
        Me.cbOralOpi.Size = New System.Drawing.Size(100, 18)
        Me.cbOralOpi.TabIndex = 142
        Me.cbOralOpi.Text = "Oral Opiates"
        Me.cbOralOpi.UseVisualStyleBackColor = True
        '
        'cbOralMethy
        '
        Me.cbOralMethy.AutoSize = True
        Me.cbOralMethy.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMethy.Location = New System.Drawing.Point(286, 250)
        Me.cbOralMethy.Name = "cbOralMethy"
        Me.cbOralMethy.Size = New System.Drawing.Size(158, 18)
        Me.cbOralMethy.TabIndex = 141
        Me.cbOralMethy.Text = "Oral Methylphenidate"
        Me.cbOralMethy.UseVisualStyleBackColor = True
        '
        'cbOralMethAmp
        '
        Me.cbOralMethAmp.AutoSize = True
        Me.cbOralMethAmp.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralMethAmp.Location = New System.Drawing.Point(286, 223)
        Me.cbOralMethAmp.Name = "cbOralMethAmp"
        Me.cbOralMethAmp.Size = New System.Drawing.Size(247, 18)
        Me.cbOralMethAmp.TabIndex = 140
        Me.cbOralMethAmp.Text = "Oral Methylenedioxy-Amphetamines"
        Me.cbOralMethAmp.UseVisualStyleBackColor = True
        '
        'cbOralHero
        '
        Me.cbOralHero.AutoSize = True
        Me.cbOralHero.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralHero.Location = New System.Drawing.Point(286, 172)
        Me.cbOralHero.Name = "cbOralHero"
        Me.cbOralHero.Size = New System.Drawing.Size(164, 18)
        Me.cbOralHero.TabIndex = 139
        Me.cbOralHero.Text = "Oral Heroin Metabolite"
        Me.cbOralHero.UseVisualStyleBackColor = True
        '
        'cbOralFent
        '
        Me.cbOralFent.AutoSize = True
        Me.cbOralFent.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralFent.Location = New System.Drawing.Point(286, 121)
        Me.cbOralFent.Name = "cbOralFent"
        Me.cbOralFent.Size = New System.Drawing.Size(112, 18)
        Me.cbOralFent.TabIndex = 138
        Me.cbOralFent.Text = "Oral Fentanyls"
        Me.cbOralFent.UseVisualStyleBackColor = True
        '
        'cbOralCOC
        '
        Me.cbOralCOC.AutoSize = True
        Me.cbOralCOC.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralCOC.Location = New System.Drawing.Point(286, 94)
        Me.cbOralCOC.Name = "cbOralCOC"
        Me.cbOralCOC.Size = New System.Drawing.Size(101, 18)
        Me.cbOralCOC.TabIndex = 137
        Me.cbOralCOC.Text = "Oral Cocaine"
        Me.cbOralCOC.UseVisualStyleBackColor = True
        '
        'cbOralTHC
        '
        Me.cbOralTHC.AutoSize = True
        Me.cbOralTHC.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralTHC.Location = New System.Drawing.Point(286, 67)
        Me.cbOralTHC.Name = "cbOralTHC"
        Me.cbOralTHC.Size = New System.Drawing.Size(188, 18)
        Me.cbOralTHC.TabIndex = 136
        Me.cbOralTHC.Text = "Oral Cannabinoids, Natural"
        Me.cbOralTHC.UseVisualStyleBackColor = True
        '
        'cbOralBup
        '
        Me.cbOralBup.AutoSize = True
        Me.cbOralBup.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralBup.Location = New System.Drawing.Point(286, 41)
        Me.cbOralBup.Name = "cbOralBup"
        Me.cbOralBup.Size = New System.Drawing.Size(145, 18)
        Me.cbOralBup.TabIndex = 135
        Me.cbOralBup.Text = "Oral Buprenorphine"
        Me.cbOralBup.UseVisualStyleBackColor = True
        '
        'cbOralBenz
        '
        Me.cbOralBenz.AutoSize = True
        Me.cbOralBenz.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralBenz.Location = New System.Drawing.Point(286, 17)
        Me.cbOralBenz.Name = "cbOralBenz"
        Me.cbOralBenz.Size = New System.Drawing.Size(154, 18)
        Me.cbOralBenz.TabIndex = 134
        Me.cbOralBenz.Text = "Oral Benzodiazepines"
        Me.cbOralBenz.UseVisualStyleBackColor = True
        '
        'cbOralAmp
        '
        Me.cbOralAmp.AutoSize = True
        Me.cbOralAmp.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbOralAmp.Location = New System.Drawing.Point(16, 41)
        Me.cbOralAmp.Name = "cbOralAmp"
        Me.cbOralAmp.Size = New System.Drawing.Size(145, 18)
        Me.cbOralAmp.TabIndex = 133
        Me.cbOralAmp.Text = "Oral Amphetamines"
        Me.cbOralAmp.UseVisualStyleBackColor = True
        '
        'frmSOEGridInq
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(825, 343)
        Me.Controls.Add(Me.cbOralSedHyp)
        Me.Controls.Add(Me.cbOralPregaba)
        Me.Controls.Add(Me.cbOralGaba)
        Me.Controls.Add(Me.cbOralAntiPsych)
        Me.Controls.Add(Me.cbOralAntiEpi)
        Me.Controls.Add(Me.cbOralAnti)
        Me.Controls.Add(Me.cbOralAntiTri)
        Me.Controls.Add(Me.cbOralADSer)
        Me.Controls.Add(Me.cbOralAlk)
        Me.Controls.Add(Me.cbOralTram)
        Me.Controls.Add(Me.cbOralMtd)
        Me.Controls.Add(Me.cbOralTap)
        Me.Controls.Add(Me.cbOralSkel)
        Me.Controls.Add(Me.cbOralPro)
        Me.Controls.Add(Me.cbOralPhen)
        Me.Controls.Add(Me.cbOralOxy)
        Me.Controls.Add(Me.cbOralOpiAnal)
        Me.Controls.Add(Me.cbOralOpi)
        Me.Controls.Add(Me.cbOralMethy)
        Me.Controls.Add(Me.cbOralMethAmp)
        Me.Controls.Add(Me.cbOralHero)
        Me.Controls.Add(Me.cbOralFent)
        Me.Controls.Add(Me.cbOralCOC)
        Me.Controls.Add(Me.cbOralTHC)
        Me.Controls.Add(Me.cbOralBup)
        Me.Controls.Add(Me.cbOralBenz)
        Me.Controls.Add(Me.cbOralAmp)
        Me.Controls.Add(Me.btnExit)
        Me.Name = "frmSOEGridInq"
        Me.Text = "frmSOEGridInq"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As Button
    Friend WithEvents cbOralSedHyp As CheckBox
    Friend WithEvents cbOralPregaba As CheckBox
    Friend WithEvents cbOralGaba As CheckBox
    Friend WithEvents cbOralAntiPsych As CheckBox
    Friend WithEvents cbOralAntiEpi As CheckBox
    Friend WithEvents cbOralAnti As CheckBox
    Friend WithEvents cbOralAntiTri As CheckBox
    Friend WithEvents cbOralADSer As CheckBox
    Friend WithEvents cbOralAlk As CheckBox
    Friend WithEvents cbOralTram As CheckBox
    Friend WithEvents cbOralMtd As CheckBox
    Friend WithEvents cbOralTap As CheckBox
    Friend WithEvents cbOralSkel As CheckBox
    Friend WithEvents cbOralPro As CheckBox
    Friend WithEvents cbOralPhen As CheckBox
    Friend WithEvents cbOralOxy As CheckBox
    Friend WithEvents cbOralOpiAnal As CheckBox
    Friend WithEvents cbOralOpi As CheckBox
    Friend WithEvents cbOralMethy As CheckBox
    Friend WithEvents cbOralMethAmp As CheckBox
    Friend WithEvents cbOralHero As CheckBox
    Friend WithEvents cbOralFent As CheckBox
    Friend WithEvents cbOralCOC As CheckBox
    Friend WithEvents cbOralTHC As CheckBox
    Friend WithEvents cbOralBup As CheckBox
    Friend WithEvents cbOralBenz As CheckBox
    Friend WithEvents cbOralAmp As CheckBox
End Class
