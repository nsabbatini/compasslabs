﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class AcctInq
    Public Active As Boolean = False, AEFDT As String = "", Acct As String = ""
    Private Sub txtAcct_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcct.KeyUp
        If e.KeyCode = Keys.Enter Then
            ClearFields()
            FillAcct()
        End If
    End Sub

    Private Sub FillAcct()
        If Not IsNumeric(Mid(txtAcct.Text, 2)) And Len(txtAcct.Text) > 1 And txtAcct.Text <> "PTSAMPLES" And txtAcct.Text <> "HOUSE" Then
            txtAcct.Text = GetAcctFromName(txtAcct.Text)
            Exit Sub
        End If
        If txtAcct.Text = "" Then Exit Sub
        Active = CheckAcctActive(txtAcct.Text)
        Dim Sel As String = "", UsedReqs As String = ""
        If Active Then
            Sel = "Select ANAME,ACITY,ASTATE,APHONE,AFAX,ASALESRG,AMISC1,AEFDT,AMISC2,ANAME2,AAD1,AAD2,AZIP,AMISC3 from ACCT where ACCT='" & txtAcct.Text & "' And AACT=1"
        Else
            Sel = "Select ANAME,ACITY,ASTATE,APHONE,AFAX,ASALESRG,AMISC1,AEFDT,AMISC2,ANAME2,AAD1,AAD2,AZIP,AMISC3 from ACCT where ACCT='" & txtAcct.Text & "' Order By AEFDT DESC"
        End If
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read() Then
            Acct = txtAcct.Text
            CLSLIMS.AcctNum = Acct
            lblAcct.Text = txtAcct.Text & " - " & rs(0)
            lblAddr1.Text = rs(10)
            lblAddr2.Text = rs(11)
            If Not Active Then lblAcct.BackColor = Color.Red
            If rs(9) <> "" Then lblAcct.Text = lblAcct.Text & "(" & rs(9) & ")"
            lblCityST.Text = rs(1) & ", " & rs(2) & "  " & rs(12)
            lblPhone.Text = rs(3)
            lblFax.Text = rs(4)
            If rs(5) <> "" Then
                lblSls.Text = rs(5) & " - " & GetSalesman(rs(5))
                lblSlsPhone.Text = GetSalesmanPhone(rs(5))
            End If
            lblColl.Text = GetProc(rs(6))
            lblColl2.Text = GetProc(rs(13))
            AEFDT = rs(7)
            lblUpdate.Text = Mid(AEFDT, 5, 2) & "/" & Mid(AEFDT, 7, 2) & "/" & Mid(AEFDT, 1, 4)
            If IsChiral(txtAcct.Text) Then
                lblChiral.Text = "Auto Chiral Reflex"
            Else
                lblChiral.Text = ""
            End If
            rs.Close()
            btnGrid.Enabled = True
            'Cmd.CommandText = "Select OC from ACCTOC Where ACCT='" & txtAcct.Text & "' And AEFDT='" & AEFDT & "' And oc != '800000'"
            'Dim rs2 As SqlDataReader = Cmd.ExecuteReader
            'Dim OC As String = ""
            'Do While rs2.Read
            '   OC = OC & rs2(0) & ","
            'Loop
            'rs2.Close()
            'If OC <> "" Then OC = Mid(OC, 1, Len(OC) - 1)
            'lblOC.Text = OC
            Cmd.CommandText = "Select PHYFNAME,PHYMNAME,PHYLNAME From ACCTPHY, DOCTOR Where ACCT='" & txtAcct.Text & "' And AEFDT='" & AEFDT & "' And ACCTPHY.PHY = DOCTOR.PHY and PHYACT=1"
            Dim rs3 As SqlDataReader = Cmd.ExecuteReader
            lstPhy.Items.Clear()
            Do While rs3.Read
                lstPhy.Items.Add(rs3(2) & ", " & rs3(0) & " " & rs3(1))
            Loop
            rs3.Close()
            Cmd.CommandText = "select YREQLUID,substring(YREQLDT,1,8),Count(*),Min(REQ),Max(REQ) From YREQL Where ACCT='" & txtAcct.Text & "' Group By YREQLUID,substring(YREQLDT,1,8) Order By substring(YREQLDT,1,8) DESC"
            Dim rs4 As SqlDataReader = Cmd.ExecuteReader
            Do While rs4.Read
                UsedReqs = GetUsedReqs(txtAcct.Text, rs4(3), rs4(4))
                lstReqs.Items.Add(CLSLIMS.RJ(rs4(3) & " - " & rs4(4), 18) & " -- " & CLSLIMS.RJ(UsedReqs, 5) & " - " & Mid(rs4(1), 5, 2) & "/" & Mid(rs4(1), 7, 2) & "/" & Mid(rs4(1), 3, 2) & " - " & rs4(0))
            Loop
            rs4.Close()
            Cmd.CommandText = "Select rpque From acctrpque Where AEFDT='" & AEFDT & "' And ACCT='" & txtAcct.Text & "'"
            Dim rs5 As SqlDataReader = Cmd.ExecuteReader
            Dim RPM As String = ""
            Do While rs5.Read
                RPM = RPM & rs5(0) & ","
            Loop
            If Len(RPM) > 0 Then RPM = Mid(RPM, 1, Len(RPM) - 1)
            rs5.Close()
            lblRpt.Text = RPM
            Cmd.CommandText = "select min(aefdt) from acct where acct='" & txtAcct.Text & "'"
            Dim rs6 As SqlDataReader = Cmd.ExecuteReader
            If rs6.Read Then
                lblCreate.Text = Mid(rs6(0), 5, 2) & "/" & Mid(rs6(0), 7, 2) & "/" & Mid(rs6(0), 1, 4)
            End If
            rs6.Close()

            btnImgA.Text = btnImgA.Text & IIf(CLSLIMS.ImgFile(txtAcct.Text, "A"), "*", "") : btnImgA.Enabled = True
            btnImgB.Text = btnImgB.Text & IIf(CLSLIMS.ImgFile(txtAcct.Text, "B"), "*", "") : btnImgB.Enabled = True
            btnImgC.Text = btnImgC.Text & IIf(CLSLIMS.ImgFile(txtAcct.Text, "C"), "*", "") : btnImgC.Enabled = True
            btnImgD.Text = btnImgD.Text & IIf(CLSLIMS.ImgFile(txtAcct.Text, "D"), "*", "") : btnImgD.Enabled = True
            btnImgE.Text = btnImgE.Text & IIf(CLSLIMS.ImgFile(txtAcct.Text, "E"), "*", "") : btnImgE.Enabled = True
            btnImgF.Text = btnImgF.Text & IIf(CLSLIMS.ImgFile(txtAcct.Text, "F"), "*", "") : btnImgF.Enabled = True
            btnImgG.Text = btnImgG.Text & IIf(CLSLIMS.ImgFile(txtAcct.Text, "G"), "*", "") : btnImgG.Enabled = True
            If NotesFound Then
                btnNotes.Text = "NOTES*"
            End If
        Else
                MsgBox("Account not found.")
            ClearFields()
        End If
        rs.Close()
        Conn.Close()
        txtAcct.Text = ""
        txtAcct.Focus()
    End Sub

    Public Function GetVol(ByVal acct As String) As String
        GetVol = ""  'YYYYMM;cnt^YYYYMM;cnt^YYYYMM;cnt^YYYYMM;cnt^
        Dim first As String = Format(DateAdd(DateInterval.Month, -4, Now), "yyyyMM01")
        Dim last As String = Format(Now, "yyyyMMdd")
        Dim SQL As String = "Select substring(A.PSRCVDT,1,6) as dt,COUNT(*) as cnt " &
                            "From PORD C, PSPEC A " &
                            "Where C.PORD = A.PORD And C.POEFDT = A.POEFDT " &
                            "And substring(A.PSRCVDT,1,8) Between '" & first & "' and '" & last & "' " &
                            "And C.ACCT = '" & acct & "' " &
                            "And POACT=1 And PSACT=1 " &
                            "Group by substring(A.PSRCVDT,1,6) " &
                            "Order by substring(A.PSRCVDT,1,6) desc "
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(SQL, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                GetVol = GetVol & cRS(0) & ";" & cRS(1) & "^"
            Loop
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Public Function GetUsedReqs(ByVal acct As String, ByVal min As Integer, ByVal max As Integer) As String
        GetUsedReqs = ""
        Dim sel As String = "Select Count(distinct pspecno) From PSPEC Where PSPECNO between '" & min & "' and '" & max & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetUsedReqs = cRS(0)
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using
    End Function
    Public Function IsChiral(ByVal acct As String) As Boolean
        IsChiral = False
        Dim sel As String = "Select FLEXP From FL Where FL Like '%CHIRAL%' And FLACT=1", x As String = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                x = cRS(0)
                For i = 2 To CLSLIMS.NumberofPieces(x, "=")
                    If acct = Replace(Mid(CLSLIMS.Piece(x, "=", i), 1, InStr(CLSLIMS.Piece(x, "=", i), ")") - 1), Chr(34), "") Then
                        IsChiral = True
                        Exit Do
                    End If
                Next i
            Loop
            cRS.Close()
            Cmd.Dispose()
        End Using

    End Function
    Public Function NotesFound() As Boolean
        NotesFound = False
        Dim sel As String = "Select Count(*) From PSCMT Where PSPECNO ='" & CLSLIMS.AcctNum & "' And PSCMTACT=1", x As String = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                If cRS(0) > 0 Then
                    NotesFound = True
                End If
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using

    End Function
    Public Function CheckAcctActive(ByVal acct As String) As Boolean
        CheckAcctActive = False
        Dim sel As String = "Select COUNT(*) From ACCT Where ACCT='" & acct & "' And AACT=1"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(sel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                If cRS(0) > 0 Then CheckAcctActive = True
            End If
            cRS.Close()
            Cmd.Dispose()
        End Using

    End Function
    Private Sub ClearFields()

        lblAddr1.Text = "" : lblAddr2.Text = "" : lblCityST.Text = "" : lblPhone.Text = "" : lblCreate.Text = "" : lblUpdate.Text = ""
        lblFax.Text = "" : lblColl.Text = "" : lblSls.Text = "" : lblOC.Text = "" : lblSlsPhone.Text = ""
        lblVol.Text = "" : lblVolMo.Text = ""
        lstPhy.Items.Clear()
        lstReqs.Items.Clear()
        lstAcctNameSearch.Items.Clear()
        lstAcctNameSearch.Visible = False
        lblRpt.Text = ""
        txtAcct.Focus()
        lblChiral.Text = ""
        btnImgA.Text = "New Acct Application"
        btnImgB.Text = "Profile Authorization"
        btnImgC.Text = "Physician Acknw + Bill"
        btnImgD.Text = "Web Authorization"
        btnImgE.Text = "Direct Bill"
        btnImgF.Text = "Misc"
        btnImgG.Text = "MOU"
        lblAcct.Text = ""
        lblAcct.BackColor = Color.White
        btnGrid.Enabled = False
        btnImgA.Enabled = False
        btnImgB.Enabled = False
        btnImgC.Enabled = False
        btnImgD.Enabled = False
        btnImgE.Enabled = False
        btnImgF.Enabled = False
        btnImgG.Enabled = False
        btnNotes.Text = "NOTES"
    End Sub
    Private Function GetAcctFromName(ByVal AcctName As String) As String

        Dim xSel As String
        xSel = "select acct,aname " &
               "from acct " &
               "where upper(aname) like '" & UCase(AcctName) & "%' and aact=1 " &
               "union " &
               "select acct,+'(I)'+aname from acct a where upper(aname) like '" & UCase(AcctName) & "%' and aact=0 " &
               "and aefdt=(select max(aefdt) from acct b where a.acct=b.acct) and substring(acct,1,1) not in ('C','D') " &
               "order by aname"
        ' and aact=1 
        Dim strAcct As String = ""
        GetAcctFromName = ""
        If AcctName = "" Then Exit Function
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            Do While cRS.Read
                strAcct = cRS(0) & Space(10 - Len(cRS(0))) & Mid(cRS(1), 1, 30)
                lstAcctNameSearch.Items.Add(strAcct)
            Loop
            If lstAcctNameSearch.Items.Count > 0 Then
                lstAcctNameSearch.Visible = True
                lstAcctNameSearch.Focus()
            End If
            cRS.Close()
            Conn.Close()
        End Using

    End Function
    Private Sub lstAcctNameSearch_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles lstAcctNameSearch.MouseDoubleClick
        txtAcct.Text = Piece(lstAcctNameSearch.SelectedItem.ToString, " ", 1)
        lstAcctNameSearch.Visible = False
        FillAcct()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Function GetSalesman(ByVal sls As String) As String
        Dim xSel As String = "Select SYSLISTOPT from SYSLIST where SYSLISTKEY='ASALESRG' AND SYSLISTVAL = '" & sls & "'"
        GetSalesman = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetSalesman = cRS(0)
            End If
        End Using

    End Function

    Private Function GetSalesmanPhone(ByVal sls As String) As String
        Dim xSel As String = "Select SYSLISTOPT from SYSLIST where SYSLISTKEY='SALESPH' AND SYSLISTVAL = '" & sls & "'"
        GetSalesmanPhone = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetSalesmanPhone = cRS(0)
            End If
        End Using

    End Function

    Private Function GetProc(ByVal proc As String) As String
        Dim xSel As String = "Select a.SYSLISTOPT,b.syslistopt from SYSLIST a, syslist b where a.SYSLISTKEY='PROC' and b.SYSLISTKEY='SALESPH' AND a.SYSLISTVAL = '" & proc & "' and a.syslistval=b.syslistval"
        GetProc = ""
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(xSel, Conn)
            Dim cRS As SqlDataReader = Cmd.ExecuteReader
            If cRS.Read Then
                GetProc = proc & "-" & cRS(0) & " " & cRS(1)
            End If
        End Using

    End Function
    Private Sub AcctInq_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If CLSLIMS.AcctNum = "" Then
            'ClearFields()
            txtAcct.Focus()
        End If
    End Sub

    Private Sub AcctInq_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        ClearFields()
        If CLSLIMS.AcctNum <> "" Then

            txtAcct.Text = CLSLIMS.AcctNum
            FillAcct()
        Else
            lblAcct.Text = ""
            lstAcctNameSearch.Visible = False
            txtAcct.Focus()
        End If
        Me.KeyPreview = True
    End Sub
    Public Function Piece(ByVal str As String, Optional ByVal charz As String = "^", Optional ByVal pos As Integer = 1) As String
        Dim pc As Array
        If pos < 1 Then pos = 1
        If InStr(str, charz) = 0 Then
            Piece = str
            Exit Function
        End If
        pc = Split(str, charz)
        If UBound(pc) < pos - 1 Then
            Piece = ""
        Else
            Piece = pc(pos - 1)
        End If

    End Function

    Private Sub btnImgA_Click(sender As Object, e As EventArgs) Handles btnImgA.Click
        'Dim ImgName As String = "\\Compass-scanpc\C\Compass\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "A.pdf"
        Dim ImgName As String = "\\CMPFS1\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "A.pdf"
        Dim SecondImgName As String = "\\CL-AGILENT\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "A.pdf"
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = secondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not found.")
        End If
    End Sub

    Private Sub btnImgB_Click(sender As Object, e As EventArgs) Handles btnImgB.Click
        Dim ImgName As String = "\\CMPFS1\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "B.pdf"
        Dim SecondImgName As String = "\\CL-AGILENT\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "B.pdf"
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not found.")
        End If
    End Sub

    Private Sub btnImgC_Click(sender As Object, e As EventArgs) Handles btnImgC.Click
        Dim ImgName As String = "\\CMPFS1\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "C.pdf"
        Dim SecondImgName As String = "\\CL-AGILENT\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "C.pdf"
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not found.")
        End If
    End Sub

    Private Sub btnImgD_Click(sender As Object, e As EventArgs) Handles btnImgD.Click
        Dim ImgName As String = "\\CMPFS1\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "D.pdf"
        Dim SecondImgName As String = "\\CL-AGILENT\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "D.pdf"
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not found.")
        End If
    End Sub

    Private Sub btnImgE_Click(sender As Object, e As EventArgs) Handles btnImgE.Click
        Dim ImgName As String = "\\CMPFS1\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "E.pdf"
        Dim SecondImgName As String = "\\CL-AGILENT\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "E.pdf"
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not found.")
        End If
    End Sub

    Private Sub btnImgF_Click(sender As Object, e As EventArgs) Handles btnImgF.Click
        Dim ImgName As String = "\\CMPFS1\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "F.pdf"
        Dim SecondImgName As String = "\\CL-AGILENT\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "F.pdf"
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not found.")
        End If
    End Sub

    Private Sub btnGrid_Click(sender As Object, e As EventArgs) Handles btnGrid.Click
        If Mid(Acct, 1, 1) = "S" Then

        Else
            frmOCGrid.ShowDialog()
        End If
    End Sub

    Private Sub btnNotes_Click(sender As Object, e As EventArgs) Handles btnNotes.Click
        frmAcctNotes.ShowDialog()
    End Sub

    Private Sub btnVol_Click(sender As Object, e As EventArgs) Handles btnVol.Click
        Dim r As String = GetVol(Acct)
        If r = "" Then
            lblVolMo.Text = "No volume last 4 months."
        Else
            lblVolMo.Text = Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 1), ";", 1), 1, 4) & "-" & Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 1), ";", 1), 5, 2) & " "
            lblVolMo.Text = lblVolMo.Text & Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 2), ";", 1), 1, 4) & "-" & Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 2), ";", 1), 5, 2) & " "
            lblVolMo.Text = lblVolMo.Text & Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 3), ";", 1), 1, 4) & "-" & Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 3), ";", 1), 5, 2) & " "
            lblVolMo.Text = lblVolMo.Text & Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 4), ";", 1), 1, 4) & "-" & Mid(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 4), ";", 1), 5, 2)
            lblVol.Text = CLSLIMS.RJ(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 1), ";", 2), 5)
            lblVol.Text = lblVol.Text & CLSLIMS.RJ(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 2), ";", 2), 8)
            lblVol.Text = lblVol.Text & CLSLIMS.RJ(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 3), ";", 2), 8)
            lblVol.Text = lblVol.Text & CLSLIMS.RJ(CLSLIMS.Piece(CLSLIMS.Piece(r, "^", 4), ";", 2), 8)
        End If

    End Sub

    Private Sub btnImgG_Click(sender As Object, e As EventArgs) Handles btnImgG.Click
        Dim ImgName As String = "\\CMPFS1\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "G.pdf"
        Dim SecondImgName As String = "\\CL-AGILENT\Accounts\" & Trim(Piece(lblAcct.Text, "-", 1)) & "G.pdf"
        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                If Not System.IO.File.Exists(SecondImgName) Then
                    MsgBox("Image not found.")
                Else
                    PDFView.PDF1.src = SecondImgName
                    PDFView.ShowDialog()
                End If
            Else
                PDFView.PDF1.src = ImgName
                PDFView.ShowDialog()
            End If
        Else
            MsgBox("Image not found.")
        End If
    End Sub

    Private Sub AcctInq_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        Dim rsp As Integer = 0
        If e.KeyCode = Keys.F10 Then
            If Not Active Then
                rsp = MsgBox("Are you sure you want to activate this account?", MsgBoxStyle.OkCancel)
                If rsp = 1 Then
                    'Activate Account
                    Dim Conn As New SqlConnection(CLSLIMS.strCon)
                    Conn.Open()
                    Dim Cmd As New SqlCommand
                    Cmd.CommandText = "Update ACCT Set AACT=1 Where ACCT='" & Acct & "' And AEFDT='" & AEFDT & "' And AACT=0"
                    Cmd.Connection = Conn
                    Cmd.ExecuteNonQuery()
                    Conn.Close()
                    ClearFields()
                    txtAcct.Text = Acct
                    FillAcct()
                End If
            End If
        ElseIf e.KeyCode = Keys.F11 Then
            If Active Then
                rsp = MsgBox("Are you sure you want to INACTIVATE this account?", MsgBoxStyle.OkCancel)
                If rsp = 1 Then
                    'InActivate Account
                    Dim Conn As New SqlConnection(CLSLIMS.strCon)
                    Conn.Open()
                    Dim Cmd As New SqlCommand
                    Cmd.CommandText = "Update ACCT Set AACT=0 Where ACCT='" & Acct & "' And AEFDT='" & AEFDT & "' And AACT=1"
                    Cmd.Connection = Conn
                    Cmd.ExecuteNonQuery()
                    Conn.Close()
                    ClearFields()
                    txtAcct.Text = Acct
                    FillAcct()
                End If
            End If
        End If
    End Sub
End Class