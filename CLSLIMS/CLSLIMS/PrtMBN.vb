﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class PrtMBN
    Dim WrkList As String

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click

        Call CollectData()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        PrintPreview.ShowDialog()

        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Call CollectData()

        PrintDoc.DefaultPageSettings.Margins.Left = 10
        PrintDoc.DefaultPageSettings.Margins.Right = 10
        PrintDoc.DefaultPageSettings.Margins.Top = 10
        PrintDoc.DefaultPageSettings.Margins.Bottom = 10
        PrintDoc.DefaultPageSettings.Landscape = False

        'PrintPrev.ShowDialog()
        PrintDoc.Print()

        Me.Close()
        Me.Dispose()

    End Sub
    Private Sub CollectData()
        Dim Sel As String, p As String, c As Integer = 1, xPos As String

        'Sel = "Select A.PSPECNO, TRAY, FOLDER, POS, TRAYx, C.WL From XTRAYS A, PBI B, PB C Where A.PSPECNO=B.PSPECNO And B.WL=C.WL And B.BATCH=C.BATCH And B.BEFDT=C.BEFDT And MBATCH='" & txtMBN.Text & "' Order By FOLDER, POS"
        Sel = "Select distinct A.PSPECNO, TRAY, FOLDER, POS, TRAYx, C.WL,PBIx From XTRAYS A, PBI B, PB C Where A.PSPECNO=B.PSPECNO And B.WL=C.WL And B.BATCH=C.BATCH And B.BEFDT=C.BEFDT And BACT=1 And MBATCH='" & txtMBN.Text & "' Order By PBIx"
        lstData.Items.Clear()
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand(Sel, Conn)
        Dim rs As SqlDataReader = Cmd.ExecuteReader

        Do While rs.Read()
            'Determine Tray Letter
            Dim TL As String
            Dim sq As Integer = Val(Mid(rs(1), 3, 2))
            TL = Mid("ABCDEFGHJKLMNPQRSTUVWXYZ", sq, 1)
            WrkList = rs(5)
            xPos = TL & IIf(sq < 10, "0", "") & rs(4)
            If WrkList <> "CHIRAL" Then
                p = Space(2) & CLSLIMS.RJ(rs(6), 3) & Space(3) & CLSLIMS.RJ(rs(0), 10) & Space(6) & CLSLIMS.RJ(rs(1), 15) & Space(8) & CLSLIMS.LJ(xPos, 5) & Space(2) & CLSLIMS.RJ(IIf(IsDBNull(rs(2)), "", rs(2)), 15) & Space(11) & CLSLIMS.RJ(IIf(IsDBNull(rs(3)), "", rs(3)), 6)
            Else
                p = Space(2) & CLSLIMS.RJ(rs(6), 3) & Space(3) & CLSLIMS.RJ(rs(0), 10) & Space(6) & CLSLIMS.RJ(rs(1), 15) & Space(8) & CLSLIMS.LJ(xPos, 5) & Space(2) & CLSLIMS.RJ(IIf(IsDBNull(rs(2)), "", rs(2)), 15) & Space(11) & CLSLIMS.RJ(IIf(IsDBNull(rs(0)), "", GetRslt(rs(0))), 6)
            End If
            lstData.Items.Add(p)
            c = c + 1
        Loop
        rs.Close()
        Conn.Close()
        Conn.Dispose()

    End Sub
    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        Dim Font As New Font("Courier New", 8, FontStyle.Bold)
        Dim Font5 As New Font("Courier New", 12, FontStyle.Bold)
        Dim Font2 As New Font("Tacoma", 20, FontStyle.Bold)
        Dim Font3 As New Font("Tacoma", 12, FontStyle.Bold)
        Dim Font4 As New Font("Tacoma", 10, FontStyle.Bold)

        'PrintDoc.DefaultPageSettings.Landscape = False
        'PrintDoc.DefaultPageSettings.PaperSize = New PaperSize("Custom", 600, 1600)

        Static PageNo As Integer = 1
        Dim y As Integer

        e.Graphics.DrawString("Specimen Locations", Font2, Brushes.Black, 300, 100)
        e.Graphics.DrawString("MBN: " & txtMBN.Text & "   " & WrkList, Font3, Brushes.Black, 25, 158)
        Dim LogoImage As Image = My.Resources.Compass_Logo

        e.Graphics.DrawImage(LogoImage, 25, 25)

        e.Graphics.DrawString(Format(Now, "MM/dd/yy  HH:mm"), Font4, Brushes.Black, 690, 30)
        e.Graphics.DrawString("Page: " & PageNo, Font4, Brushes.Black, 690, 48)
        If WrkList <> "CHIRAL" Then
            e.Graphics.DrawString("                               Tray                  Plate", Font5, Brushes.Black, 25, 185)
            e.Graphics.DrawString("     Specimen #     Tray     Position    Folder     Position", Font5, Brushes.Black, 25, 200)
        Else
            e.Graphics.DrawString("                               Tray                  METH", Font5, Brushes.Black, 25, 185)
            e.Graphics.DrawString("     Specimen #     Tray     Position    Folder     Result", Font5, Brushes.Black, 25, 200)
        End If
        e.Graphics.DrawLine(Pens.Black, 25, 180, 1075, 180)   'Horizonal line above headings
        e.Graphics.DrawLine(Pens.Black, 25, 220, 1075, 220)   'Horizonal line under headings

        'e.Graphics.DrawLine(Pens.Black, 65, 180, 65, 820)     'Vertical line 1
        e.Graphics.DrawLine(Pens.Black, 200, 180, 200, 1080)   'Veritcal line 2
        e.Graphics.DrawLine(Pens.Black, 320, 180, 320, 1080)   'Veritcal line 3
        e.Graphics.DrawLine(Pens.Black, 420, 180, 420, 1080)   'Veritcal line 4
        e.Graphics.DrawLine(Pens.Black, 550, 180, 550, 1080)   'Veritcal line 5
        'e.Graphics.DrawLine(Pens.Black, 875, 180, 875, 820)   'Veritcal line 6

        Dim c As Integer
        Static intStart As Integer

        y = 225 : c = 1
        For i = intStart To lstData.Items.Count - 1
            'data line
            e.Graphics.DrawString(lstData.Items(i).ToString, Font, Brushes.Black, 25, y)
            If i <> lstData.Items.Count - 1 Then
                If Mid(lstData.Items(i + 1), 1, 15) <> Space(15) Then
                    e.Graphics.DrawLine(Pens.Black, 25, y + 26, 1075, y + 26) 'Horizonal line after sample
                End If
            End If
            y = y + 30
            c = c + 1
            If c > 28 Then
                intStart = i + 1 : PageNo = PageNo + 1
                e.HasMorePages = True
                Exit For
            End If
        Next i
    End Sub
    Public Function GetRslt(ByVal spec As String) As String
        GetRslt = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select PRSLT From PRES Where PSPECNO='" & spec & "' And RC='301021' And PRACT=1"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetRslt = rs(0)
        End If
        rs.Close()
        Conn.Close()
    End Function
End Class