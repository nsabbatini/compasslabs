﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRelTray
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblName = New System.Windows.Forms.Label()
        Me.txtTray = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnRel = New System.Windows.Forms.Button()
        Me.lvInfo = New System.Windows.Forms.ListView()
        Me.colspec = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colmsg = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Patient = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(299, 38)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(0, 19)
        Me.lblName.TabIndex = 10
        '
        'txtTray
        '
        Me.txtTray.Location = New System.Drawing.Point(135, 35)
        Me.txtTray.Name = "txtTray"
        Me.txtTray.Size = New System.Drawing.Size(122, 27)
        Me.txtTray.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(66, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 19)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Tray:"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(353, 604)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(90, 33)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnRel
        '
        Me.btnRel.Location = New System.Drawing.Point(91, 604)
        Me.btnRel.Name = "btnRel"
        Me.btnRel.Size = New System.Drawing.Size(90, 33)
        Me.btnRel.TabIndex = 8
        Me.btnRel.Text = "Release"
        Me.btnRel.UseVisualStyleBackColor = True
        '
        'lvInfo
        '
        Me.lvInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colspec, Me.colmsg, Me.Patient})
        Me.lvInfo.GridLines = True
        Me.lvInfo.Location = New System.Drawing.Point(12, 85)
        Me.lvInfo.Name = "lvInfo"
        Me.lvInfo.Size = New System.Drawing.Size(590, 354)
        Me.lvInfo.TabIndex = 12
        Me.lvInfo.UseCompatibleStateImageBehavior = False
        Me.lvInfo.View = System.Windows.Forms.View.Details
        '
        'colspec
        '
        Me.colspec.Text = "Specimen#"
        Me.colspec.Width = 120
        '
        'colmsg
        '
        Me.colmsg.Text = "Message"
        Me.colmsg.Width = 250
        '
        'Patient
        '
        Me.Patient.Text = "Patient"
        Me.Patient.Width = 200
        '
        'lstInfo
        '
        Me.lstInfo.FormattingEnabled = True
        Me.lstInfo.ItemHeight = 19
        Me.lstInfo.Location = New System.Drawing.Point(13, 446)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(589, 137)
        Me.lstInfo.TabIndex = 13
        '
        'frmRelTray
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(615, 649)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.lvInfo)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtTray)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRel)
        Me.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmRelTray"
        Me.Text = "frmRelTray"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblName As Label
    Friend WithEvents txtTray As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnExit As Button
    Friend WithEvents btnRel As Button
    Friend WithEvents lvInfo As ListView
    Friend WithEvents colspec As ColumnHeader
    Friend WithEvents colmsg As ColumnHeader
    Friend WithEvents Patient As ColumnHeader
    Friend WithEvents lstInfo As ListBox
End Class
