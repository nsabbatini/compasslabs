﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Drawing.Printing
Imports clscommon.clscommon
Imports System.Data.OleDb
Imports Microsoft.Office.Core
Imports Microsoft.Office.Interop



Public Class Queue
    '  Public Shared strcon As String = "Server=10.65.1.12;Database=TNCL_PROD;User Id=sa;Password=C0mp@SS**;MultipleActiveResultSets=true"
    '   Dim myclscommon As New clscommon.clscommon

    Dim DeptValuesList As List(Of String) = New List(Of String)
    Dim TypeValuesList As List(Of String) = New List(Of String)
    Dim typeparam As String
    Dim deptparam As String
    Public hold_apspecno As String
    Dim x As Integer
    Public hold_aacctacct As String
    Public hold_aacctaefdt As String
    Public hold_dpuidefdt As String
    Public hold_dpuid As String

    Dim rownumber As Integer
    Public pindex As Integer
    Dim cRS As SqlDataReader
    Public holdpspecno As String

    Public colldatetime As DateTime
    Dim AINotesExist As String

    Public Varcrs20 As String
    Public Varcrs21 As String
    Public Varcrs22 As String
    Public Varcrs23 As String
    Public Varcrs24 As String
    Public Varcrs25 As String
    Public Varcrs26 As String
    Public Varcrs27 As String
    Public Varcrs28 As String
    Public Varcrs29 As String

    Public PatientName As String
    Dim cls As New clscommon.clscommon

    Private Sub Queue_Load(sender As Object, e As EventArgs) Handles Me.Load
        If UCase(System.Net.Dns.GetHostName) = "COMPASS-NEDPC" Then CLSLIMS.UID = "NED.SABBATINI"
        If CLSLIMS.UID = "" Then
            Login.ShowDialog()
            If CLSLIMS.UID = "" Then
                Me.Close()
                Me.Dispose()
            End If
        End If
        DeptValuesList.Clear()
        TypeValuesList.Clear()
        cbDept.Items.Clear()
        cbType.Items.Clear()

        'DeptValuesList.Add("CLIN")
        'DeptValuesList.Add("PGX")
        'DeptValuesList.Add("TOX")
        'DeptValuesList.Add("NG")
        DeptValuesList.Add("ALL")
        'For i = 0 To 4
        'cbDept.Items.Add(DeptValuesList(i))
        'Next
        cbDept.Items.Add("ALL")
        cbDept.Text = "ALL"

        TypeValuesList.Add("AFFIDAVIT")
        '   TypeValuesList.Add("REJECTS")
        TypeValuesList.Add("CALL")
        TypeValuesList.Add("ALL")
        TypeValuesList.Add("BAD_FL")
        TypeValuesList.Add("TECH")
        TypeValuesList.Add("INSURANCE")
        TypeValuesList.Add("AFF HOLD")
        TypeValuesList.Add("COVID")
        For i = 0 To 7
            cbType.Items.Add(TypeValuesList(i))
        Next


    End Sub

    Private Sub cbType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbType.SelectedIndexChanged
        Dim selectedItemValue As String = TypeValuesList.Item(cbType.SelectedIndex)
        Select Case cbType.SelectedItem
            Case "AFFIDAVIT"
                typeparam = "A"
                '     Case "REJECTS"
        '        typeparam = "R"
            Case "CALL"
                typeparam = "C"
            Case "ALL"
                typeparam = "%"
            Case "BAD_FL"
                typeparam = "BAD_FL"
            Case "TECH"
                typeparam = "TECH"
            Case "INSURANCE"
                typeparam = "INSURANCE"
            Case "AFF HOLD"
                typeparam = "AFF HOLD"
            Case "COVID"
                typeparam = "COVID"
        End Select
        If typeparam = "TECH" Or typeparam = "BAD_FL" Then
            cbDept.SelectedItem = "ALL"
            cbDept.Text = "ALL"
        End If
        Dim deptitemSelected As Integer = cbDept.SelectedIndex.ToString()
        Dim typeitemSelected As Integer = cbType.SelectedIndex.ToString()
        Call ChecktoLoadListbox(deptitemSelected, typeitemSelected)


    End Sub
    Sub ChecktoLoadListbox(ByVal deptitemSelected, ByVal typeitemSelected)
        If ((deptitemSelected <> -1) And (typeitemSelected <> -1)) Then
            cbType.Refresh()
            Try
                Call load_listHeader(typeparam, deptparam)
            Catch ex As Exception
                MsgBox("Error Loading List Header")
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End If
    End Sub
    Private Sub cbDept_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDept.SelectedIndexChanged
        Dim selectedItemValue As String = DeptValuesList.Item(cbDept.SelectedIndex)
        '  ListHeader.Refresh()

        Select Case cbDept.SelectedItem
            Case "CLIN"
                deptparam = "C"
            Case "PGX"
                deptparam = "P"
            Case "TOX"
                deptparam = "T"
            Case "NG"
                deptparam = "N"
            Case "ALL"
                deptparam = "%"
        End Select
        Dim deptitemSelected As Integer = cbDept.SelectedIndex.ToString()
        Dim typeitemSelected As Integer = cbType.SelectedIndex.ToString()
        Call ChecktoLoadListbox(deptitemSelected, typeitemSelected)

    End Sub
    Sub load_listHeader(ptypeparam, pdeptparam)

        DGView1.ColumnCount = 13
        DGView1.Columns(0).Name = "Specimen"
        DGView1.Columns(1).Name = "First name"
        DGView1.Columns(2).Name = "Last Name"
        DGView1.Columns(3).Name = "Stat Code"
        DGView1.Columns(4).Name = "Acct"
        DGView1.Columns(5).Name = "Acct Name"
        DGView1.Columns(6).Name = "Receive Date"
        DGView1.Columns(7).Name = "Sales Rep"
        DGView1.Columns(8).Name = "Acct Eff Date"
        DGView1.Columns(9).Name = "Patient ID"
        DGView1.Columns(10).Name = "Pt Eff Date"
        DGView1.Columns(11).Name = "Processor"
        DGView1.Columns(12).Name = "Aff Sent"

        DGView1.Columns(8).Visible = False
        DGView1.Columns(9).Visible = False
        DGView1.Columns(10).Visible = False

        Call ClearDetail()
        Dim sql As String = ""

        If typeparam = "BAD_FL" Or typeparam = "TECH" Then
            sql = "Select p.pspecno , d.pfname, d.plname, s.statcd, po.acct, a.aname, d.puid, d.pefdt, a.acct, a.aefdt, substring(p.psrcvdt,1,8), y.syslistopt, a.amisc1  From pspec p "
            sql = sql & "Left Join psstat s on p.pspecno = s.pspecno And p.adsq1 = s.adsq1 "
            sql = sql & "Left Join pdem d   On p.puid = d.puid And p.pefdt = d.pefdt "
            sql = sql & "Left Join pord po  On po.pord = p.pord And po.poefdt = p.poefdt "
            sql = sql & " Left Join acct a  On a.acct = po.acct And a.aefdt = po.aefdt "
            sql = sql & " Left outer Join syslist y  On a.asalesrg = y.syslistval "
            sql = sql & "where "
            sql = sql & "p.psact = 1 "
            sql = sql & "And po.poact = 1 and y.syslistkey = 'ASALESRG'"
            sql = sql & " And s.statcd Like " & "'%" & ptypeparam & "'"
            sql = sql & " order by substring(p.psrcvdt,1,8) asc"
        Else

            sql = "Select p.pspecno , d.pfname, d.plname, s.statcd, po.acct, a.aname, d.puid, d.pefdt, a.acct, a.aefdt, substring(p.psrcvdt,1,8), y.syslistopt, a.amisc1  From pspec p "
            sql = sql & "Left Join psstat s on p.pspecno = s.pspecno And p.adsq1 = s.adsq1 "
            sql = sql & "Left Join pdem d   On p.puid = d.puid And p.pefdt = d.pefdt "
            sql = sql & "Left Join pord po  On po.pord = p.pord And po.poefdt = p.poefdt "
            sql = sql & " Left Join acct a  On a.acct = po.acct And a.aefdt = po.aefdt "
            sql = sql & " Left outer Join syslist y  On a.asalesrg = y.syslistval "
            sql = sql & "where "
            sql = sql & "p.psact = 1 "
            sql = sql & "And po.poact = 1 and y.syslistkey = 'ASALESRG'"
            'sql = sql & " And substring(s.statcd, 1, 2) Like " & " '" & pdeptparam & ptypeparam & "'"
            sql = sql & " And s.statcd= " & " '" & ptypeparam & "'"
            sql = sql & " order by substring(p.psrcvdt,1,8) asc"
        End If
        DGView1.Rows.Clear()
        Me.Cursor = Cursors.WaitCursor
        Dim x As Integer = 0
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Try
                Dim Cmd As New SqlCommand(sql, Conn)
                Dim SF2 As SqlDataReader = Cmd.ExecuteReader
                Do While SF2.Read()
                    If SF2(3).Substring(1, 1) <> "I" And SF2(3).Substring(1, 1) <> "R" Then
                        Call CheckforAI(SF2(0))
                        Dim format As String = "yyyyMMdd"
                        Dim testdate As String = IIf(IsDBNull(SF2(10)), "", SF2(10))
                        If (Not String.IsNullOrEmpty(testdate)) Then
                            Dim colldate As Date = DateTime.ParseExact(testdate, format, CultureInfo.CurrentCulture)
                            DGView1.Rows.Add(SF2(0), SF2(1), SF2(2), SF2(3), SF2(4), SF2(5), colldate, SF2(11), SF2(9), SF2(6), SF2(7), SF2(12), AINotesExist)
                            Call LookupEffDate(SF2(8), x)
                        End If
                        x = x + 1
                    End If
                Loop
            Catch ex As Exception
                MsgBox("Error Loading Grid")
            End Try
        End Using

        Me.Refresh()
        Me.Cursor = Cursors.Default

        'ListHeader.Visible = True
        'ListHeader.Focus()
    End Sub
    Sub LookupEffDate(acct, x)
        Dim Sql As String = "Select Top 1 aefdt from acct where acct = '" & acct & "' order by aefdt asc"
        Using Conn2 As New SqlConnection(CLSLIMS.strCon)
            Dim Cmd2 As New SqlCommand(Sql, Conn2)
            Conn2.Open()
            Dim SF3 As SqlDataReader = Cmd2.ExecuteReader
            Do While SF3.Read()
                Dim Format As String = "yyyyMMddHHmmss"
                Dim testdate = IIf(IsDBNull(SF3(0)), "", SF3(0))
                If (Not String.IsNullOrEmpty(testdate)) Then
                    Dim mydate As DateTime = DateTime.ParseExact(testdate, Format, CultureInfo.CurrentCulture)
                    If mydate > DateTime.Now.AddDays(-31) Then
                        DGView1.Rows(x).DefaultCellStyle.BackColor = Color.Yellow
                    End If
                End If
            Loop
            Conn2.Close()
        End Using
    End Sub
    Sub listdetail()

        Dim sql = "Select  "
        sql = sql & "p.pspecno, "   '0
        sql = sql & "p.pscdta, "    '1
        sql = sql & "p.psctma, "    '2
        sql = sql & "s.statcd, "    '3
        sql = sql & "a.aname, "     '4
        sql = sql & "po.reqphy, "   '5
        sql = sql & "po.acct, "      '6
        sql = sql & "p.psrcvdt, "     '7
        sql = sql & "a.aphone, "       '8
        sql = sql & "a.afax "          '9
        sql = sql & "From PSPEC p "
        sql = sql & "Left outer Join PSSTAT s On p.pspecno = s.pspecno And p.adsq1 = s.adsq1 "
        sql = sql & "Left outer Join PORD po  On po.pord = p.pord And po.poefdt = p.poefdt "
        sql = sql & "Left outer Join ACCT a   On a.acct = po.acct And a.aefdt = po.aefdt "
        sql = sql & "where p.pspecno = " & "'" & hold_apspecno & "' "
        sql = sql & "And p.psact = 1 "
        sql = sql & "And po.poact = 1 "
        sql = sql & "And p.psact = 1 "
        sql = sql & "And a.acct = " & "'" & hold_aacctacct & "' "
        sql = sql & "And a.aefdt = " & "'" & hold_aacctaefdt & "' "

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Try

                Dim Cmd As New SqlCommand(sql, Conn)
                Dim cRS As SqlDataReader = Cmd.ExecuteReader
                Do While cRS.Read()
                    Varcrs20 = IIf(IsDBNull(cRS(0)), " ", (cRS(0)))
                    Varcrs21 = IIf(IsDBNull(cRS(1)), " ", (cRS(1)))
                    Varcrs22 = IIf(IsDBNull(cRS(2)), " ", (cRS(2)))
                    Varcrs23 = IIf(IsDBNull(cRS(3)), " ", (cRS(3)))
                    Varcrs24 = IIf(IsDBNull(cRS(4)), " ", (cRS(4)))
                    Varcrs25 = IIf(IsDBNull(cRS(5)), " ", (cRS(5)))
                    Varcrs21 = IIf(IsDBNull(cRS(1)), "", (cRS(1)))
                    Varcrs22 = IIf(IsDBNull(cRS(2)), "", (cRS(2)))
                    Varcrs28 = IIf(IsDBNull(cRS(8)), "", (cRS(8)))
                    Varcrs29 = IIf(IsDBNull(cRS(9)), "", (cRS(9)))
                    lblspecnum.Text = (cRS(0))
                    holdpspecno = (cRS(0))
                    lblaname.Text = (cRS(4))
                    lblreqphy.Text = Varcrs25

                    If Len(Varcrs22) < 6 Then
                        Varcrs22 = Varcrs22.PadRight(6, "0")
                    End If
                    colldatetime = Nothing
                    If Varcrs21 <> "" And Varcrs22 <> "" Then
                        Dim input As String = Varcrs21 & Varcrs22
                        Dim format As String = "yyyyMMddHHmmss"
                        colldatetime = DateTime.ParseExact(input, format, CultureInfo.CurrentCulture)
                        lblcolldatetime.Text = colldatetime
                    Else
                        lblcolldatetime.Text = ""
                    End If
                    If Varcrs28 <> "" Then
                        lblacctphone.Text = Varcrs28
                    Else
                        lblacctphone.Text = " "
                    End If
                    If Varcrs29 <> "" Then
                        lblfax.Text = Varcrs29
                    Else
                        lblacctphone.Text = " "
                    End If
                    Varcrs27 = IIf(IsDBNull(cRS(7)), " ", (cRS(7)))
                    If Varcrs27 <> "" Then
                        Dim input As String = Varcrs27
                        Dim format As String = "yyyyMMddHHmmss"
                        Dim recvdatetime As DateTime = DateTime.ParseExact(input, format, CultureInfo.CurrentCulture)
                        lblrecvdatetime.Text = recvdatetime
                    Else
                        lblcolldatetime.Text = " "
                    End If

                Loop
                cRS.Close()
            Catch ex As Exception
                MsgBox("Error in Detail")

            End Try
            If hold_dpuid <> "" Then
                sql = ""
                sql = "Select "
                sql = sql & "d.pfname, "    '0
                sql = sql & "d.plname, "    '1
                sql = sql & "d.pdob, "      '2
                sql = sql & "d.page, "      '3
                sql = sql & "d.sex, "       '4
                sql = sql & "d.prace, "       '5
                sql = sql & "d.pcity, "     '6
                sql = sql & "d.pstate, "     '7
                sql = sql & "d.pzip, "     '8
                sql = sql & "d.pphone "     '9

                sql = sql & "from PDEM d  left join pspec p On p.puid = d.puid And p.pefdt = d.pefdt "
                ' sql = sql & "where d.puid = " & "'" & adempuid(ListHeader.SelectedIndex) & "' "
                sql = sql & "where d.puid = " & "'" & hold_dpuid & "' "
                '  sql = sql & "And d.pefdt = " & "'" & adempefdt(ListHeader.SelectedIndex) & "' "
                sql = sql & "And d.pefdt = " & "'" & hold_dpuidefdt & "' "
                sql = sql & "AND p.pspecno = " & "'" & hold_apspecno & "' "
                sql = sql & "and p.psact = 1 "


                Try
                    Dim Cmd2 As New SqlCommand(sql, Conn)
                    Dim cRS2 As SqlDataReader = Cmd2.ExecuteReader
                    lblpflname.Text = ""
                    lblpdob.Text = ""
                    lblprace.Text = ""

                    lblage.Text = ""
                    lblsex.Text = ""

                    Do While cRS2.Read()

                        Dim firstlast As String = IIf(IsDBNull(cRS2(0)), " ", (cRS2(0)) & " " & IIf(IsDBNull(cRS2(1)), " ", (cRS2(1))))
                        lblpflname.Text = ""
                        lblpflname.Text = firstlast
                        PatientName = firstlast
                        lblsex.Text = IIf(IsDBNull(cRS2(4)), " ", (cRS2(4)))
                        lblprace.Text = IIf(IsDBNull(cRS2(5)), " ", (cRS2(5)))
                        Dim Address As String = IIf(IsDBNull(cRS2(6)), ", ", (cRS2(6)) & " " & IIf(IsDBNull(cRS2(7)), " ", (cRS2(7)) & " " & IIf(IsDBNull(cRS2(8)), " ", (cRS2(8)))))
                        lblAddress.Text = Address

                        Dim input As String = IIf(IsDBNull(cRS2(2)), " ", (cRS2(2)))
                        Dim format As String = "yyyyMMdd"
                        If input <> "" Then
                            Dim dobdatetime As DateTime = DateTime.ParseExact(input, format, CultureInfo.CurrentCulture)
                            lblpdob.Text = dobdatetime
                            lblage.Text = IIf(IsDBNull(cRS2(3)), " ", (cRS2(3)))
                            Dim patientage = Today.Year - dobdatetime.Year
                            If (dobdatetime > Today.AddYears(-patientage)) Then patientage -= 1
                            lblage.Text = patientage
                        End If

                    Loop
                Catch ex As Exception
                    MsgBox("Error in demographics")
                End Try
            End If

            lststatcodes.Items.Clear()

            sql = ""
            sql = "Select "
            sql = sql & "p.statcd, s.stcddesc "    '0
            sql = sql & "from pspec ps "
            sql = sql & "Left Join psstat p on ps.pspecno = p.pspecno and ps.adsq1 = p.adsq1 "
            sql = sql & "left join statcd s on p.statcd = s.statcd And s.stcdact = 1 "
            sql = sql & "where ps.psact = 1 and "
            sql = sql & "p.pspecno = " & "'" & hold_apspecno & "' "
            Try
                Dim Cmd3 As New SqlCommand(sql, Conn)
                Dim cRS3 As SqlDataReader = Cmd3.ExecuteReader

                Do While cRS3.Read()
                    lststatcodes.Items.Add(cRS3(0).ToString & " - " & (cRS3(1).ToString))
                Loop
            Catch ex As Exception
                MsgBox("error loading status codes")
            End Try
            Conn.Close()
            Call LoadInternalNotes() '(pindex)
        End Using

    End Sub

    Public Sub CheckforAI(ppspecno)
        AINotesExist = ""

        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()

            Dim sql As String
            sql = "Select count(*)  "
            sql = sql & "from pscmt c  "
            sql = sql & "where c.pspecno = " & "'" & ppspecno & "' "
            sql = sql & "and c.pscmttype = 'I' and PSCMTORIG = 'AI' and PSCMTACT=1"
            sql = "Select count(*) from pscmt a, pscmti b where a.pspecno='" & ppspecno & "' and a.pscmttype='I' and pscmtorig='AI' and pscmtact=1 " &
                "and a.pspecno=b.pspecno and a.pscmttype=b.pscmttype and a.pscmtefdt=b.pscmtefdt and pscmttxt = 'Affidavit sent.'"
            Try
                Dim Cmd5 As New SqlCommand(sql, Conn)
                Dim cRS5 As SqlDataReader = Cmd5.ExecuteReader

                Do While cRS5.Read()
                    If cRS5(0) > 0 Then
                        AINotesExist = "*"
                    End If
                Loop
            Catch ex As Exception
                MsgBox("Error loading comments")
            End Try
            sql = "Select count(*) from pscmt a, pscmti b where a.pspecno='" & ppspecno & "' and a.pscmttype='I' and pscmtorig='AI' and pscmtact=1 " &
                "and a.pspecno=b.pspecno and a.pscmttype=b.pscmttype and a.pscmtefdt=b.pscmtefdt and pscmttxt = 'Verified in TEVIX. No Aff sent.'"
            Dim Cmd6 As New SqlCommand(sql, Conn)
            Dim rs6 As SqlDataReader = Cmd6.ExecuteReader
            If rs6.Read Then
                If rs6(0) > 0 Then
                    AINotesExist = AINotesExist & "T"
                End If
            End If
            Conn.Close()
        End Using
    End Sub

    Public Sub LoadInternalNotes()
        lstintcom.Items.Clear()
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()

            Dim sql As String
            sql = "Select pscmttxt, PSCMTORIG "
            sql = sql & "from pscmt c left join pscmti i On c.pspecno =  i.pspecno and "
            sql = sql & "c.pscmttype = i.pscmttype and c.pscmtefdt = i.pscmtefdt "
            sql = sql & "where c.pspecno = " & "'" & hold_apspecno & "' "
            sql = sql & "and c.pscmttype = 'I' and PSCMTACT=1"
            Try
                Dim Cmd4 As New SqlCommand(sql, Conn)
                Dim cRS4 As SqlDataReader = Cmd4.ExecuteReader

                Do While cRS4.Read()
                    lstintcom.Items.Add(cRS4(0).ToString & " ")
                Loop

            Catch ex As Exception
                MsgBox("Error loading comments")
            End Try
            Conn.Close()
        End Using
    End Sub
    Sub ClearDetail()
        lblpflname.Text = ""
        lblpdob.Text = ""
        lblage.Text = ""
        lblsex.Text = ""
        lblprace.Text = ""
        lblspecnum.Text = ""
        lblaname.Text = ""
        lblreqphy.Text = ""
        lblrecvdatetime.Text = ""
        lblcolldatetime.Text = ""
        lblAddress.Text = ""
        lblacctphone.Text = ""
        lblfax.Text = ""
        lststatcodes.Items.Clear()
        lstintcom.Items.Clear()

    End Sub
    Public Function LJ(ByVal strLJ As String, ByVal iLen As Integer) As String
        LJ = ""
        If Len(strLJ) > iLen Then
            LJ = strLJ
        Else
            LJ = strLJ & Space(iLen - Len(strLJ))
        End If
    End Function

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub btnPDF_Click(sender As Object, e As EventArgs) Handles btnPDF.Click
        Dim y = DGView1.CurrentCell.RowIndex
        hold_apspecno = DGView1.Rows(y).Cells(0).Value

        Dim Sel As String
        Dim ImgName As String = ""
        Sel = "select '\\CMPFS1\Image\' + IMGVOL + '\' + IMGFILE From PIMG Where PSPECNO='" & hold_apspecno & "'"
        Using Conn As New SqlConnection(CLSLIMS.strCon)
            Conn.Open()
            Dim Cmd As New SqlCommand(Sel, Conn)
            Dim cRSpdf As SqlDataReader = Cmd.ExecuteReader
            If cRSpdf.Read Then
                ImgName = cRSpdf(0)
            End If
            cRSpdf.Close()
            Conn.Close()
        End Using

        If ImgName <> "" Then
            If Not System.IO.File.Exists(ImgName) Then
                MsgBox("Requisition not found")
            Else
                Process.Start(ImgName)
            End If
        Else
            MsgBox("Requisition not found")
        End If
        '   End If

    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Notes.Click
        x = DGView1.CurrentCell.RowIndex
        hold_apspecno = DGView1.Rows(x).Cells(0).Value
        holdpspecno = hold_apspecno
        Dim DetailForm As DetailNotes
        DetailForm = New DetailNotes()
        DetailForm.ShowDialog()
        'PatientName = ""
        Call LoadInternalNotes()
        Call CheckforAI(hold_apspecno)
        DGView1.Rows(x).Cells(12).Value = AINotesExist
    End Sub
    Private Sub DGView1_DoubleClick(sender As Object, e As EventArgs) Handles DGView1.DoubleClick
        Call ClearDetail()
        Call ContinueDoubleClick()
    End Sub
    Sub ContinueDoubleClick()
        x = DGView1.CurrentCell.RowIndex
        hold_dpuid = ""
        hold_apspecno = DGView1.Rows(x).Cells(0).Value
        hold_aacctacct = DGView1.Rows(x).Cells(4).Value
        hold_aacctaefdt = DGView1.Rows(x).Cells(8).Value

        IIf(IsDBNull(DGView1.Rows(x).Cells(9).Value), "", DGView1.Rows(x).Cells(9).Value)
        If Not String.IsNullOrEmpty(DGView1.Rows(x).Cells(9).Value.ToString) Then
            '    If DGView1.Rows(x).Cells(9).ToString <> "" Then
            hold_dpuid = DGView1.Rows(x).Cells(9).Value
            hold_dpuidefdt = DGView1.Rows(x).Cells(10).Value
            '   End If
        End If

        Call listdetail()
    End Sub


    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub btnExport_Click_1(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim xlApp As Microsoft.Office.Interop.Excel.Application
        Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
        Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim i As Integer
        Dim j As Integer

        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = xlWorkBook.Sheets("sheet1")

        Me.Cursor = Cursors.WaitCursor
        For i = 0 To DGView1.RowCount - 2
            For j = 0 To DGView1.ColumnCount - 1
                If i = 0 Then
                    For k As Integer = 1 To DGView1.Columns.Count
                        xlWorkSheet.Cells(1, k) = DGView1.Columns(k - 1).HeaderText
                    Next k
                End If
                xlWorkSheet.Cells(i + 2, j + 1) = DGView1(j, i).Value.ToString()
            Next j
            'Add comments NS 06/23/2020
            xlWorkSheet.Cells(i + 2, 14) = GetFirstTwoComments(DGView1(0, i).Value.ToString)
        Next i
        Dim savePath As String = Nothing
        Using sd As New SaveFileDialog
            With sd
                .RestoreDirectory = True
                .Filter = "Excel XLS Files(*.xls)|*.xls" '|Excel Macro Embedded Files(*.xlsm)|*.xlsm|Excel XLSX Files(*.xlsx)|*.xlsx"
                .FilterIndex = 3
                If .ShowDialog = DialogResult.OK Then
                    savePath = .FileName
                End If
            End With
        End Using

        If savePath IsNot Nothing AndAlso savePath.Trim <> "" Then
            xlWorkBook.SaveAs(savePath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
        End If
        xlWorkBook.Close(True, misValue, misValue)
        xlApp.Quit()
        ' xlWorkSheet.SaveAs("C:\vbexcel.xlsx")
        ' xlWorkBook.Close()
        ' xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

        Me.Cursor = Cursors.Default
    End Sub
    Public Function GetFirstTwoComments(ByVal spec As String) As String
        GetFirstTwoComments = ""
        Dim ActCom As String = ""
        Debug.Print(spec)
        Dim Con As New SqlConnection(CLSLIMS.strCon)
        Con.Open()
        Dim Sel As String = "Select PSCMTTXT From PSCMTI A, PSCMT B Where A.PSPECNO='" & spec & "' And PSCMTACT=1 And A.PSPECNO=B.PSPECNO And A.PSCMTEFDT=B.PSCMTEFDT And A.PSCMTTYPE='I' And B.PSCMTTYPE='I' Order By A.PSCMTEFDT,PSCMTIx"
        Dim Cmd As New SqlCommand(Sel, Con)
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        Dim comtext As String = "", prevCommCode As String = "", comseq As Integer = 0, comcode As String = ""
        Do While rs.Read()
            ActCom = ActCom + rs(0) + " "
            comseq = comseq + 1
            If comseq = 2 Then
                Exit Do
            End If
        Loop
        rs.Close()
        Con.Close()
        GetFirstTwoComments = ActCom
        Debug.Print(spec & " " & GetFirstTwoComments)
    End Function

End Class




