﻿Imports CLSLIMS.CLSLIMS
Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class frmSlsProcList
    Private Sub frmSlsProcList_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        If lstSlsProcLst.Items.Count = 0 Then
            Dim Sel As String = ""
        Sel = "Select ACCT, ANAME From ACCT Where aact=1 Order By ANAME"
        lstSlsProcLst.Items.Clear()
            Using Conn As New SqlConnection(CLSLIMS.strCon)
                Conn.Open()
                Dim Cmd As New SqlCommand(Sel, Conn)
                Dim rs As SqlDataReader = Cmd.ExecuteReader
                Do While rs.Read()
                    lstSlsProcLst.Items.Add(CLSLIMS.LJ(rs(0), 10) & CLSLIMS.LJ(rs(1), 50) & CLSLIMS.LJ(GetSalesman(rs(0)), 30) & CLSLIMS.LJ(GetProc(rs(0)), 30) & CLSLIMS.LJ(GetProc2(rs(0)), 30))
                Loop
                rs.Close()
                Conn.Close()
            End Using
        End If
    End Sub
    Public Function GetSalesman(ByVal acct As String) As String
        GetSalesman = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select asalesrg,syslistopt from acct,syslist where aact=1 and asalesrg=syslistval and syslistkey='ASALESRG' and acct='" & acct & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetSalesman = rs(0) & "-" & rs(1)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function GetProc(ByVal acct As String) As String
        GetProc = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select amisc1,syslistopt from acct,syslist where aact=1 and amisc1=syslistval and syslistkey='PROC' and acct='" & acct & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetProc = rs(0) & "-" & rs(1)
        End If
        rs.Close()
        Conn.Close()
    End Function
    Public Function GetProc2(ByVal acct As String) As String
        GetProc2 = ""
        Dim Conn As New SqlConnection(CLSLIMS.strCon)
        Conn.Open()
        Dim Cmd As New SqlCommand
        Cmd.Connection = Conn
        Cmd.CommandText = "Select amisc3,syslistopt from acct,syslist where aact=1 and amisc3=syslistval and syslistkey='PROC' and acct='" & acct & "'"
        Dim rs As SqlDataReader = Cmd.ExecuteReader
        If rs.Read Then
            GetProc2 = rs(0) & "-" & rs(1)
        End If
        rs.Close()
        Conn.Close()
    End Function

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class