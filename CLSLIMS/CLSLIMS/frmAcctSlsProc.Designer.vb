﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAcctSlsProc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvData = New System.Windows.Forms.ListView()
        Me.AccountNum = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AccountName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SalesmanNum = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SalesmanName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProcessorNum = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProcessorName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AccountState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Active = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnBuildXLS = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.colRDt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'lvData
        '
        Me.lvData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.AccountNum, Me.AccountName, Me.SalesmanNum, Me.SalesmanName, Me.ProcessorNum, Me.ProcessorName, Me.AccountState, Me.Active, Me.colRDt})
        Me.lvData.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvData.GridLines = True
        Me.lvData.Location = New System.Drawing.Point(12, 26)
        Me.lvData.Name = "lvData"
        Me.lvData.Size = New System.Drawing.Size(1153, 259)
        Me.lvData.TabIndex = 0
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        '
        'AccountNum
        '
        Me.AccountNum.Text = "Account #"
        Me.AccountNum.Width = 80
        '
        'AccountName
        '
        Me.AccountName.Text = "Account Name"
        Me.AccountName.Width = 300
        '
        'SalesmanNum
        '
        Me.SalesmanNum.Text = "Salesman #"
        Me.SalesmanNum.Width = 90
        '
        'SalesmanName
        '
        Me.SalesmanName.Text = "Salesman Name"
        Me.SalesmanName.Width = 150
        '
        'ProcessorNum
        '
        Me.ProcessorNum.Text = "Processor#"
        Me.ProcessorNum.Width = 90
        '
        'ProcessorName
        '
        Me.ProcessorName.Text = "Processor Name"
        Me.ProcessorName.Width = 150
        '
        'AccountState
        '
        Me.AccountState.Text = "Acount State"
        Me.AccountState.Width = 103
        '
        'Active
        '
        Me.Active.Text = "Active"
        Me.Active.Width = 65
        '
        'btnBuildXLS
        '
        Me.btnBuildXLS.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuildXLS.Location = New System.Drawing.Point(473, 300)
        Me.btnBuildXLS.Name = "btnBuildXLS"
        Me.btnBuildXLS.Size = New System.Drawing.Size(98, 34)
        Me.btnBuildXLS.TabIndex = 1
        Me.btnBuildXLS.Text = "Build XLS"
        Me.btnBuildXLS.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(594, 300)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(98, 34)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "EXIT"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'colRDt
        '
        Me.colRDt.Text = "Panel Renewal"
        Me.colRDt.Width = 110
        '
        'frmAcctSlsProc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1171, 346)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnBuildXLS)
        Me.Controls.Add(Me.lvData)
        Me.Name = "frmAcctSlsProc"
        Me.Text = "Accounts-Salesmen-Processors"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lvData As ListView
    Friend WithEvents btnBuildXLS As Button
    Friend WithEvents AccountNum As ColumnHeader
    Friend WithEvents AccountName As ColumnHeader
    Friend WithEvents SalesmanNum As ColumnHeader
    Friend WithEvents SalesmanName As ColumnHeader
    Friend WithEvents ProcessorNum As ColumnHeader
    Friend WithEvents ProcessorName As ColumnHeader
    Friend WithEvents btnExit As Button
    Friend WithEvents AccountState As ColumnHeader
    Friend WithEvents Active As ColumnHeader
    Friend WithEvents colRDt As ColumnHeader
End Class
