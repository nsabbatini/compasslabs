﻿Partial Public Class GetSqlFileLocationAndName
    Private _dirLocation As String
    Private _folderName As String
    Public Property DirectoryLocation() As String
        Get
            Return _dirLocation
        End Get
        Set(value As String)
            _dirLocation = value
        End Set
    End Property
    Public Property FolderName() As String
        Get
            Return _folderName
        End Get
        Set(value As String)
            _folderName = value
        End Set
    End Property
    Public Property GetSqlFileLocation() As String
        Get
            Return DirectoryLocation & FolderName
        End Get
        Set(value As String)

        End Set
    End Property



End Class
