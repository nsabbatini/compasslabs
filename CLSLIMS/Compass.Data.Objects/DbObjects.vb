﻿Imports Compass.MySql.Database
Imports Compass.Global
Public Module DbObjects
    Private Property FullPathToSqlFile As String
    Public Function GetTableItems() As Object

        Try

            Using getInventory As New GetInventoryDb()
                FullPathToSqlFile = SetSqlFileNameAndLocation

                getInventory._sqlText = ReadSqlFile(FullPathToSqlFile)

                Return getInventory.GetInventory.ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMainOrderForm.GetInventoryDb", ex.StackTrace.ToString)
            End Using
        End Try


    End Function
End Module
