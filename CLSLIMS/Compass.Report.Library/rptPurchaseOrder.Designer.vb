<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class rptPurchaseOrder
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    Private WithEvents PageFooter As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rptPurchaseOrder))
        Me.PageHeader = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.TextBox1 = New GrapeCity.ActiveReports.SectionReportModel.TextBox()
        Me.TextBox2 = New GrapeCity.ActiveReports.SectionReportModel.TextBox()
        Me.PageFooter = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        Me.ReportHeader1 = New GrapeCity.ActiveReports.SectionReportModel.ReportHeader()
        Me.Picture1 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.CrossSectionBox1 = New GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.CrossSectionLine1 = New GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine()
        Me.CrossSectionLine2 = New GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine()
        Me.CrossSectionLine3 = New GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine()
        Me.Shape3 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape4 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape5 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape6 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape7 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape9 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape10 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape11 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape12 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.ReportFooter1 = New GrapeCity.ActiveReports.SectionReportModel.ReportFooter()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.RichTextBox1 = New GrapeCity.ActiveReports.SectionReportModel.RichTextBox()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape8 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Barcode1 = New GrapeCity.ActiveReports.SectionReportModel.Barcode()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Height = 0!
        Me.PageHeader.Name = "PageHeader"
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label15, Me.Label16, Me.Line2, Me.TextBox1, Me.TextBox2})
        Me.Detail.Height = 0.219!
        Me.Detail.Name = "Detail"
        '
        'Label15
        '
        Me.Label15.DataField = "ItemDesc"
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0.198!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = ""
        Me.Label15.Text = "Label15"
        Me.Label15.Top = 0!
        Me.Label15.Width = 4.698!
        '
        'Label16
        '
        Me.Label16.DataField = "orderqty"
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 4.949!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "text-align: right"
        Me.Label16.Text = "Label16"
        Me.Label16.Top = 0!
        Me.Label16.Width = 0.948!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 0.083!
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0.2!
        Me.Line2.Width = 7.866!
        Me.Line2.X1 = 0.083!
        Me.Line2.X2 = 7.949!
        Me.Line2.Y1 = 0.2!
        Me.Line2.Y2 = 0.2!
        '
        'TextBox1
        '
        Me.TextBox1.CurrencyCulture = New System.Globalization.CultureInfo("en-US")
        Me.TextBox1.DataField = "unitprice"
        Me.TextBox1.Height = 0.2!
        Me.TextBox1.Left = 6.053!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.OutputFormat = resources.GetString("TextBox1.OutputFormat")
        Me.TextBox1.Style = "text-align: right"
        Me.TextBox1.Text = "TextBox1"
        Me.TextBox1.Top = 0.019!
        Me.TextBox1.Width = 0.8960004!
        '
        'TextBox2
        '
        Me.TextBox2.CurrencyCulture = New System.Globalization.CultureInfo("en-US")
        Me.TextBox2.DataField = "itemcost"
        Me.TextBox2.Height = 0.2!
        Me.TextBox2.Left = 7.05!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.OutputFormat = resources.GetString("TextBox2.OutputFormat")
        Me.TextBox2.Style = "text-align: right"
        Me.TextBox2.Text = "TextBox2"
        Me.TextBox2.Top = 0!
        Me.TextBox2.Width = 0.8440003!
        '
        'PageFooter
        '
        Me.PageFooter.Height = 0!
        Me.PageFooter.Name = "PageFooter"
        '
        'ReportHeader1
        '
        Me.ReportHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Picture1, Me.Label1, Me.Label2, Me.Label3, Me.Shape1, Me.Line1, Me.Label4, Me.Label5, Me.Label6, Me.Label7, Me.Label8, Me.Label9, Me.Label10, Me.Label11, Me.Label12, Me.Label13, Me.Label14, Me.CrossSectionBox1, Me.Shape2, Me.Label19, Me.Label20, Me.Label21, Me.Label22, Me.CrossSectionLine1, Me.CrossSectionLine2, Me.CrossSectionLine3, Me.Shape3, Me.Shape4, Me.Label24, Me.Shape5, Me.Label25, Me.Shape6, Me.Label26, Me.Shape7, Me.Label27, Me.Shape9, Me.Shape10, Me.Shape11, Me.Shape12, Me.Label17, Me.Label18, Me.Label36, Me.Label37})
        Me.ReportHeader1.Height = 4.031!
        Me.ReportHeader1.Name = "ReportHeader1"
        '
        'Picture1
        '
        Me.Picture1.Height = 1.0!
        Me.Picture1.ImageData = CType(resources.GetObject("Picture1.ImageData"), System.IO.Stream)
        Me.Picture1.Left = 0.083!
        Me.Picture1.Name = "Picture1"
        Me.Picture1.Top = 0!
        Me.Picture1.Width = 4.083!
        '
        'Label1
        '
        Me.Label1.Height = 0.388!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 5.291!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "color: SteelBlue; font-size: 22pt; font-weight: bold"
        Me.Label1.Text = "Purchase Order"
        Me.Label1.Top = 0!
        Me.Label1.Width = 2.448!
        '
        'Label2
        '
        Me.Label2.Height = 0.2!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 5.291!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = ""
        Me.Label2.Text = "Date"
        Me.Label2.Top = 0.553!
        Me.Label2.Width = 1.0!
        '
        'Label3
        '
        Me.Label3.Height = 0.2!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 5.291!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = ""
        Me.Label3.Text = "P.O Number #"
        Me.Label3.Top = 0.873!
        Me.Label3.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.5730001!
        Me.Shape1.Left = 6.531!
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 0.5!
        Me.Shape1.Width = 1.0!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 6.531!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0.753!
        Me.Line1.Width = 1.0!
        Me.Line1.X1 = 6.531!
        Me.Line1.X2 = 7.531!
        Me.Line1.Y1 = 0.753!
        Me.Line1.Y2 = 0.753!
        '
        'Label4
        '
        Me.Label4.DataField = "PoDate"
        Me.Label4.Height = 0.2!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 6.583!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "text-align: right"
        Me.Label4.Text = "Label4"
        Me.Label4.Top = 0.553!
        Me.Label4.Width = 0.8960004!
        '
        'Label5
        '
        Me.Label5.DataField = "PONumber"
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 6.583!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 12pt; font-weight: bold; text-align: right"
        Me.Label5.Text = "Label5"
        Me.Label5.Top = 0.8!
        Me.Label5.Width = 0.8960004!
        '
        'Label6
        '
        Me.Label6.Height = 0.2729999!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 0.083!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "background-color: SteelBlue; color: White; font-size: 14pt"
        Me.Label6.Text = " Vendor"
        Me.Label6.Top = 1.458!
        Me.Label6.Width = 2.916!
        '
        'Label7
        '
        Me.Label7.Height = 0.2729999!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 5.032!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "background-color: SteelBlue; color: White; font-size: 14pt"
        Me.Label7.Text = "Ship To"
        Me.Label7.Top = 1.458!
        Me.Label7.Width = 2.916!
        '
        'Label8
        '
        Me.Label8.DataField = "ClientName"
        Me.Label8.Height = 0.2!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 5.032!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = ""
        Me.Label8.Text = "Compass Laboratory Services"
        Me.Label8.Top = 1.812!
        Me.Label8.Width = 2.916!
        '
        'Label9
        '
        Me.Label9.DataField = "Address"
        Me.Label9.Height = 0.2!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 5.032!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = ""
        Me.Label9.Text = "1910 Nonconnah Blvd, Suite 108"
        Me.Label9.Top = 2.073!
        Me.Label9.Width = 2.916!
        '
        'Label10
        '
        Me.Label10.DataField = "CityStateZip"
        Me.Label10.Height = 0.2!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 5.032!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = ""
        Me.Label10.Text = "Memphis, TN 38132"
        Me.Label10.Top = 2.344!
        Me.Label10.Width = 2.916!
        '
        'Label11
        '
        Me.Label11.DataField = "VendorName"
        Me.Label11.Height = 0.2!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 0.083!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = ""
        Me.Label11.Text = "Label11"
        Me.Label11.Top = 1.812!
        Me.Label11.Width = 2.916!
        '
        'Label12
        '
        Me.Label12.DataField = "VAddress"
        Me.Label12.Height = 0.2!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0.083!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = ""
        Me.Label12.Text = "Label12"
        Me.Label12.Top = 2.073!
        Me.Label12.Width = 2.916!
        '
        'Label13
        '
        Me.Label13.DataField = "VCityStateZip"
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0.083!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = ""
        Me.Label13.Text = "Label13"
        Me.Label13.Top = 2.344!
        Me.Label13.Width = 2.916!
        '
        'Label14
        '
        Me.Label14.DataField = "VContact"
        Me.Label14.Height = 0.2!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.083!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = ""
        Me.Label14.Text = "Label14"
        Me.Label14.Top = 2.667!
        Me.Label14.Width = 2.916!
        '
        'CrossSectionBox1
        '
        Me.CrossSectionBox1.Bottom = 0.00000002980232!
        Me.CrossSectionBox1.Left = 0.083!
        Me.CrossSectionBox1.LineWeight = 1.0!
        Me.CrossSectionBox1.Name = "CrossSectionBox1"
        Me.CrossSectionBox1.Radius = New GrapeCity.ActiveReports.Controls.CornersRadius(0!, Nothing, Nothing, Nothing, Nothing)
        Me.CrossSectionBox1.Right = 7.948!
        Me.CrossSectionBox1.Top = 3.614!
        '
        'Shape2
        '
        Me.Shape2.BackColor = System.Drawing.Color.SteelBlue
        Me.Shape2.Height = 0.4170001!
        Me.Shape2.Left = 0.08400001!
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 3.614!
        Me.Shape2.Width = 7.865001!
        '
        'Label19
        '
        Me.Label19.Height = 0.2!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 0.199!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "color: White; text-align: center"
        Me.Label19.Text = "Description"
        Me.Label19.Top = 3.716!
        Me.Label19.Width = 4.698!
        '
        'Label20
        '
        Me.Label20.Height = 0.2!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 4.949!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "color: White; text-align: center"
        Me.Label20.Text = "QTY"
        Me.Label20.Top = 3.716!
        Me.Label20.Width = 1.0!
        '
        'Label21
        '
        Me.Label21.Height = 0.2!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 6.0!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "color: White; text-align: center"
        Me.Label21.Text = "Unit Price"
        Me.Label21.Top = 3.716!
        Me.Label21.Width = 1.0!
        '
        'Label22
        '
        Me.Label22.Height = 0.2!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 6.949!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "color: White; text-align: center"
        Me.Label22.Text = "Total"
        Me.Label22.Top = 3.716!
        Me.Label22.Width = 1.0!
        '
        'CrossSectionLine1
        '
        Me.CrossSectionLine1.Bottom = 0!
        Me.CrossSectionLine1.Left = 4.949!
        Me.CrossSectionLine1.LineWeight = 1.0!
        Me.CrossSectionLine1.Name = "CrossSectionLine1"
        Me.CrossSectionLine1.Top = 3.614!
        '
        'CrossSectionLine2
        '
        Me.CrossSectionLine2.Bottom = 0!
        Me.CrossSectionLine2.Left = 5.949!
        Me.CrossSectionLine2.LineWeight = 1.0!
        Me.CrossSectionLine2.Name = "CrossSectionLine2"
        Me.CrossSectionLine2.Top = 3.614!
        '
        'CrossSectionLine3
        '
        Me.CrossSectionLine3.Bottom = 0!
        Me.CrossSectionLine3.Left = 7.0!
        Me.CrossSectionLine3.LineWeight = 1.0!
        Me.CrossSectionLine3.Name = "CrossSectionLine3"
        Me.CrossSectionLine3.Top = 3.614!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.6559999!
        Me.Shape3.Left = 0.083!
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape3.Top = 2.958!
        Me.Shape3.Width = 7.866!
        '
        'Shape4
        '
        Me.Shape4.BackColor = System.Drawing.Color.SteelBlue
        Me.Shape4.Height = 0.313!
        Me.Shape4.Left = 0.08400001!
        Me.Shape4.Name = "Shape4"
        Me.Shape4.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape4.Top = 2.958!
        Me.Shape4.Width = 1.948!
        '
        'Label24
        '
        Me.Label24.Height = 0.2!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 0.198!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "color: White; text-align: center"
        Me.Label24.Text = "SALES PERSON"
        Me.Label24.Top = 3.019!
        Me.Label24.Width = 1.73!
        '
        'Shape5
        '
        Me.Shape5.BackColor = System.Drawing.Color.SteelBlue
        Me.Shape5.Height = 0.313!
        Me.Shape5.Left = 2.032!
        Me.Shape5.Name = "Shape5"
        Me.Shape5.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape5.Top = 2.958!
        Me.Shape5.Width = 1.969!
        '
        'Label25
        '
        Me.Label25.Height = 0.2!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 2.094!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "color: White; text-align: center"
        Me.Label25.Text = "QUOTE NUMBER"
        Me.Label25.Top = 3.019!
        Me.Label25.Width = 1.844!
        '
        'Shape6
        '
        Me.Shape6.BackColor = System.Drawing.Color.SteelBlue
        Me.Shape6.Height = 0.313!
        Me.Shape6.Left = 4.001!
        Me.Shape6.Name = "Shape6"
        Me.Shape6.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape6.Top = 2.958!
        Me.Shape6.Width = 1.999!
        '
        'Label26
        '
        Me.Label26.Height = 0.2!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 4.053!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "color: White; text-align: center"
        Me.Label26.Text = "DEPARTMENT"
        Me.Label26.Top = 3.019!
        Me.Label26.Width = 1.896!
        '
        'Shape7
        '
        Me.Shape7.BackColor = System.Drawing.Color.SteelBlue
        Me.Shape7.Height = 0.313!
        Me.Shape7.Left = 6.0!
        Me.Shape7.Name = "Shape7"
        Me.Shape7.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape7.Top = 2.958!
        Me.Shape7.Width = 1.948!
        '
        'Label27
        '
        Me.Label27.Height = 0.2!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 6.053!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "color: White; text-align: center"
        Me.Label27.Text = "TERMS"
        Me.Label27.Top = 3.019!
        Me.Label27.Width = 1.896!
        '
        'Shape9
        '
        Me.Shape9.Height = 0.6559999!
        Me.Shape9.Left = 0.085!
        Me.Shape9.Name = "Shape9"
        Me.Shape9.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape9.Top = 2.958!
        Me.Shape9.Width = 1.947!
        '
        'Shape10
        '
        Me.Shape10.Height = 0.6559999!
        Me.Shape10.Left = 2.032!
        Me.Shape10.Name = "Shape10"
        Me.Shape10.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape10.Top = 2.958!
        Me.Shape10.Width = 1.969!
        '
        'Shape11
        '
        Me.Shape11.Height = 0.6559999!
        Me.Shape11.Left = 4.001!
        Me.Shape11.Name = "Shape11"
        Me.Shape11.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape11.Top = 2.958!
        Me.Shape11.Width = 1.999!
        '
        'Shape12
        '
        Me.Shape12.Height = 0.6559999!
        Me.Shape12.Left = 6.0!
        Me.Shape12.Name = "Shape12"
        Me.Shape12.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape12.Top = 2.958!
        Me.Shape12.Width = 1.947!
        '
        'Label17
        '
        Me.Label17.DataField = "SalesPerson"
        Me.Label17.Height = 0.2!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 0.146!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = ""
        Me.Label17.Text = "Label17"
        Me.Label17.Top = 3.333!
        Me.Label17.Width = 1.833!
        '
        'Label18
        '
        Me.Label18.DataField = "QuoteNumber"
        Me.Label18.Height = 0.2!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 2.094!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = ""
        Me.Label18.Text = "Label18"
        Me.Label18.Top = 3.333!
        Me.Label18.Width = 1.844!
        '
        'Label36
        '
        Me.Label36.DataField = "Department"
        Me.Label36.Height = 0.2!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 4.053!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = ""
        Me.Label36.Text = "Label36"
        Me.Label36.Top = 3.333!
        Me.Label36.Width = 1.896!
        '
        'Label37
        '
        Me.Label37.DataField = "PaymentTerms"
        Me.Label37.Height = 0.2!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 6.053!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = ""
        Me.Label37.Text = "Label37"
        Me.Label37.Top = 3.333!
        Me.Label37.Width = 1.823!
        '
        'ReportFooter1
        '
        Me.ReportFooter1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label23, Me.Line3, Me.Label28, Me.RichTextBox1, Me.Label29, Me.Label30, Me.Label31, Me.Label32, Me.Shape8, Me.Label33, Me.Label34, Me.Label35, Me.Line4, Me.Line5, Me.Barcode1})
        Me.ReportFooter1.Height = 2.604584!
        Me.ReportFooter1.KeepTogether = True
        Me.ReportFooter1.Name = "ReportFooter1"
        '
        'Label23
        '
        Me.Label23.Height = 0.2!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 0.08200001!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-weight: bold"
        Me.Label23.Text = "Signature:"
        Me.Label23.Top = 1.206!
        Me.Label23.Width = 0.9480001!
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 1.03!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 1.406!
        Me.Line3.Width = 4.001!
        Me.Line3.X1 = 1.03!
        Me.Line3.X2 = 5.031!
        Me.Line3.Y1 = 1.406!
        Me.Line3.Y2 = 1.406!
        '
        'Label28
        '
        Me.Label28.Height = 0.2!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 0.083!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-weight: bold"
        Me.Label28.Text = "Purchasing Agent"
        Me.Label28.Top = 0.9160001!
        Me.Label28.Width = 1.646!
        '
        'RichTextBox1
        '
        Me.RichTextBox1.AutoReplaceFields = True
        Me.RichTextBox1.DataField = "Notes"
        Me.RichTextBox1.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.RichTextBox1.Height = 0.479!
        Me.RichTextBox1.Left = 0.085!
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.RTF = resources.GetString("RichTextBox1.RTF")
        Me.RichTextBox1.Top = 0.271!
        Me.RichTextBox1.Width = 4.323!
        '
        'Label29
        '
        Me.Label29.Height = 0.2!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0.085!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = ""
        Me.Label29.Text = "COMMENTS OR SPECIAL INSTRUCTIONS"
        Me.Label29.Top = 0!
        Me.Label29.Width = 4.323!
        '
        'Label30
        '
        Me.Label30.Height = 0.2!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 5.948!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "text-align: right"
        Me.Label30.Text = "SUBTOTAL:"
        Me.Label30.Top = 0!
        Me.Label30.Width = 1.0!
        '
        'Label31
        '
        Me.Label31.Height = 0.2!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 5.948!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "text-align: right"
        Me.Label31.Text = "SALES TAX:"
        Me.Label31.Top = 0.377!
        Me.Label31.Width = 1.0!
        '
        'Label32
        '
        Me.Label32.Height = 0.2!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 5.948!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "text-align: right"
        Me.Label32.Text = "TOTAL:"
        Me.Label32.Top = 0.716!
        Me.Label32.Width = 1.0!
        '
        'Shape8
        '
        Me.Shape8.Height = 0.969!
        Me.Shape8.Left = 7.0!
        Me.Shape8.Name = "Shape8"
        Me.Shape8.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape8.Top = 0!
        Me.Shape8.Width = 0.9489994!
        '
        'Label33
        '
        Me.Label33.DataField = "SubTotal"
        Me.Label33.Height = 0.2!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 7.103!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "text-align: right"
        Me.Label33.Text = "Label33"
        Me.Label33.Top = 0!
        Me.Label33.Width = 0.7909999!
        '
        'Label34
        '
        Me.Label34.DataField = "Salestax"
        Me.Label34.Height = 0.2!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 7.085001!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "text-align: right"
        Me.Label34.Text = "Label34"
        Me.Label34.Top = 0.377!
        Me.Label34.Width = 0.7910008!
        '
        'Label35
        '
        Me.Label35.DataField = "TotalSum"
        Me.Label35.Height = 0.2!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 7.103!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "text-align: right"
        Me.Label35.Text = "Label35"
        Me.Label35.Top = 0.716!
        Me.Label35.Width = 0.7909999!
        '
        'Line4
        '
        Me.Line4.Height = 0!
        Me.Line4.Left = 7.001001!
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 0.26!
        Me.Line4.Width = 0.947999!
        Me.Line4.X1 = 7.001001!
        Me.Line4.X2 = 7.949!
        Me.Line4.Y1 = 0.26!
        Me.Line4.Y2 = 0.26!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 7.0!
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 0.635!
        Me.Line5.Width = 0.948!
        Me.Line5.X1 = 7.0!
        Me.Line5.X2 = 7.948!
        Me.Line5.Y1 = 0.635!
        Me.Line5.Y2 = 0.635!
        '
        'Barcode1
        '
        Me.Barcode1.CaptionPosition = GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below
        Me.Barcode1.DataField = "PONumber"
        Me.Barcode1.Font = New System.Drawing.Font("Courier New", 8.0!)
        Me.Barcode1.Height = 0.6354167!
        Me.Barcode1.Left = 1.03!
        Me.Barcode1.Name = "Barcode1"
        Me.Barcode1.QuietZoneBottom = 0!
        Me.Barcode1.QuietZoneLeft = 0!
        Me.Barcode1.QuietZoneRight = 0!
        Me.Barcode1.QuietZoneTop = 0!
        Me.Barcode1.Style = GrapeCity.ActiveReports.SectionReportModel.BarCodeStyle.Code_128auto
        Me.Barcode1.Text = "PONumber"
        Me.Barcode1.Top = 1.7!
        Me.Barcode1.Width = 1.854!
        '
        'rptPurchaseOrder
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.001001!
        Me.Sections.Add(Me.ReportHeader1)
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        Me.Sections.Add(Me.ReportFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private WithEvents ReportHeader1 As GrapeCity.ActiveReports.SectionReportModel.ReportHeader
    Private WithEvents ReportFooter1 As GrapeCity.ActiveReports.SectionReportModel.ReportFooter
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents CrossSectionBox1 As GrapeCity.ActiveReports.SectionReportModel.CrossSectionBox
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents CrossSectionLine1 As GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine
    Private WithEvents CrossSectionLine2 As GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine
    Private WithEvents CrossSectionLine3 As GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Shape3 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape4 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape5 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape6 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape7 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents RichTextBox1 As GrapeCity.ActiveReports.SectionReportModel.RichTextBox
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape8 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents TextBox1 As GrapeCity.ActiveReports.SectionReportModel.TextBox
    Private WithEvents TextBox2 As GrapeCity.ActiveReports.SectionReportModel.TextBox
    Private WithEvents Shape9 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape10 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape11 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape12 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture1 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Barcode1 As GrapeCity.ActiveReports.SectionReportModel.Barcode
End Class
