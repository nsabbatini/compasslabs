Imports GrapeCity.ActiveReports
Imports GrapeCity.ActiveReports.Document
Imports Compass.MySql.Database
Imports Compass.Global
Imports GrapeCity.ActiveReports.Document.Section

Public Class rptCurrentInventory

    Private Sub rptCurrentInventory_ReportStart(sender As Object, e As EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.Orientation = PageOrientation.Landscape
        Me.PageSettings.Margins.Top = 0.1
        Me.PageSettings.Margins.Bottom = 0.5
        Me.PageSettings.Margins.Left = 0.1
        Me.PageSettings.Margins.Right = 0
    End Sub
End Class
