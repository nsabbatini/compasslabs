Imports GrapeCity.ActiveReports
Imports GrapeCity.ActiveReports.Document
Imports GrapeCity.ActiveReports.Document.Section

Public Class rptPOBarCodes
    Public _barcode As String
    Public _description As String
    Private Sub rptPOBarCodes_DataInitialize(sender As Object, e As EventArgs) Handles Me.DataInitialize
        Me.Fields.Add("BarCode")
        Me.Fields.Add("Description")
    End Sub

    Private Sub rptPOBarCodes_FetchData(sender As Object, eArgs As FetchEventArgs) Handles Me.FetchData
        Me.Fields("BarCode").Value = _barcode
        Me.Fields("Description").Value = _description
    End Sub

    Private Sub rptPOBarCodes_ReportStart(sender As Object, e As EventArgs) Handles Me.ReportStart
        Me.PageSettings.Orientation = PageOrientation.Portrait
        Me.PageSettings.Margins.Top = 0
        Me.PageSettings.Margins.Bottom = 0
        Me.PageSettings.Margins.Left = 0
        Me.PageSettings.Margins.Right = 0
    End Sub
End Class
