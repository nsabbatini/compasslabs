<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rptPackingSlip
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    Private WithEvents PageFooter As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rptPackingSlip))
        Me.PageHeader = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.PageFooter = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        Me.ReportInfo1 = New GrapeCity.ActiveReports.SectionReportModel.ReportInfo()
        Me.ReportHeader1 = New GrapeCity.ActiveReports.SectionReportModel.ReportHeader()
        Me.Picture1 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.ReportFooter1 = New GrapeCity.ActiveReports.SectionReportModel.ReportFooter()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label2, Me.Label3, Me.Label4, Me.Label5, Me.Label6, Me.Label7, Me.Label8, Me.Label9, Me.Label10, Me.Label11, Me.Label12, Me.Label13, Me.Line1, Me.Label19, Me.Label20})
        Me.PageHeader.Height = 2.448!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label2
        '
        Me.Label2.Height = 0.262!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.24!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 16pt; font-weight: bold"
        Me.Label2.Text = "Order #: "
        Me.Label2.Top = 0.062!
        Me.Label2.Width = 0.9790001!
        '
        'Label3
        '
        Me.Label3.DataField = "OrderNumber"
        Me.Label3.Height = 0.262!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 1.302!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 16pt; font-weight: bold"
        Me.Label3.Text = "Label3"
        Me.Label3.Top = 0.062!
        Me.Label3.Width = 1.0!
        '
        'Label4
        '
        Me.Label4.Height = 0.2!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.24!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 12pt"
        Me.Label4.Text = "Account #: "
        Me.Label4.Top = 0.531!
        Me.Label4.Width = 0.875!
        '
        'Label5
        '
        Me.Label5.DataField = "AcctNumber"
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 1.302!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 12pt"
        Me.Label5.Text = "Label5"
        Me.Label5.Top = 0.531!
        Me.Label5.Width = 1.0!
        '
        'Label6
        '
        Me.Label6.DataField = "ClientName"
        Me.Label6.Height = 0.2!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 1.01!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 12pt"
        Me.Label6.Text = "Label6"
        Me.Label6.Top = 0.791!
        Me.Label6.Width = 5.979!
        '
        'Label7
        '
        Me.Label7.DataField = "ContactName"
        Me.Label7.Height = 0.2!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 1.01!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 12pt"
        Me.Label7.Text = "Label7"
        Me.Label7.Top = 0.9910001!
        Me.Label7.Width = 3.01!
        '
        'Label8
        '
        Me.Label8.DataField = "Address"
        Me.Label8.Height = 0.2!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 1.01!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 12pt"
        Me.Label8.Text = "Label7"
        Me.Label8.Top = 1.191!
        Me.Label8.Width = 4.947001!
        '
        'Label9
        '
        Me.Label9.DataField = "CityStateZip"
        Me.Label9.Height = 0.2!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 1.01!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 12pt"
        Me.Label9.Text = "Label9"
        Me.Label9.Top = 1.391!
        Me.Label9.Width = 4.042!
        '
        'Label10
        '
        Me.Label10.Height = 0.1979167!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 1.01!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 12pt; text-align: right"
        Me.Label10.Text = "Phone:"
        Me.Label10.Top = 1.719!
        Me.Label10.Width = 0.6150001!
        '
        'Label11
        '
        Me.Label11.Height = 0.2!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 1.01!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 12pt; text-align: right"
        Me.Label11.Text = "Fax:"
        Me.Label11.Top = 1.919!
        Me.Label11.Width = 0.615!
        '
        'Label12
        '
        Me.Label12.DataField = "Phone"
        Me.Label12.Height = 0.2!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 1.698!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 12pt"
        Me.Label12.Text = "Label12"
        Me.Label12.Top = 1.719!
        Me.Label12.Width = 2.322!
        '
        'Label13
        '
        Me.Label13.DataField = "Fax"
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 1.698!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 12pt"
        Me.Label13.Text = "Label12"
        Me.Label13.Top = 1.919!
        Me.Label13.Width = 2.322!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0.062!
        Me.Line1.LineWeight = 5.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 2.417!
        Me.Line1.Width = 7.888!
        Me.Line1.X1 = 0.062!
        Me.Line1.X2 = 7.95!
        Me.Line1.Y1 = 2.417!
        Me.Line1.Y2 = 2.417!
        '
        'Label19
        '
        Me.Label19.Height = 0.2!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 6.449!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-weight: bold; text-align: right"
        Me.Label19.Text = "Qty"
        Me.Label19.Top = 2.217!
        Me.Label19.Width = 0.5400004!
        '
        'Label20
        '
        Me.Label20.Height = 0.2!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 1.01!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold"
        Me.Label20.Text = "Description"
        Me.Label20.Top = 2.217!
        Me.Label20.Width = 1.0!
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label14, Me.Label15, Me.Label16, Me.Label17, Me.Label18})
        Me.Detail.Height = 0.2083334!
        Me.Detail.Name = "Detail"
        '
        'Label14
        '
        Me.Label14.DataField = "Line"
        Me.Label14.Height = 0.2!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.219!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: right"
        Me.Label14.Text = "Label14"
        Me.Label14.Top = 0!
        Me.Label14.Width = 0.479!
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0.698!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = ""
        Me.Label15.Text = ")"
        Me.Label15.Top = 0!
        Me.Label15.Width = 0.08300006!
        '
        'Label16
        '
        Me.Label16.DataField = "ItemDesc"
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 1.01!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = ""
        Me.Label16.Text = "Label16"
        Me.Label16.Top = 0!
        Me.Label16.Width = 4.042!
        '
        'Label17
        '
        Me.Label17.DataField = "Qty"
        Me.Label17.Height = 0.2!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 6.449!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "text-align: right"
        Me.Label17.Text = "Label17"
        Me.Label17.Top = 0!
        Me.Label17.Width = 0.54!
        '
        'Label18
        '
        Me.Label18.Height = 0.2!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 7.051!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = ""
        Me.Label18.Text = "[__]"
        Me.Label18.Top = 0!
        Me.Label18.Width = 0.8959994!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.ReportInfo1})
        Me.PageFooter.Height = 0.2291667!
        Me.PageFooter.Name = "PageFooter"
        '
        'ReportInfo1
        '
        Me.ReportInfo1.FormatString = "Page {PageNumber} of {PageCount} on {RunDateTime}"
        Me.ReportInfo1.Height = 0.2!
        Me.ReportInfo1.Left = 0.057!
        Me.ReportInfo1.Name = "ReportInfo1"
        Me.ReportInfo1.Style = "text-align: center"
        Me.ReportInfo1.Top = 0!
        Me.ReportInfo1.Width = 7.885!
        '
        'ReportHeader1
        '
        Me.ReportHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Picture1, Me.Label1})
        Me.ReportHeader1.Height = 1.822917!
        Me.ReportHeader1.Name = "ReportHeader1"
        '
        'Picture1
        '
        Me.Picture1.Height = 1.0!
        Me.Picture1.ImageData = CType(resources.GetObject("Picture1.ImageData"), System.IO.Stream)
        Me.Picture1.Left = 0.06200001!
        Me.Picture1.Name = "Picture1"
        Me.Picture1.Top = 0!
        Me.Picture1.Width = 7.885001!
        '
        'Label1
        '
        Me.Label1.Height = 0.6680001!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.06200004!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 12pt; text-align: center"
        Me.Label1.Text = "1910 Nonconnah Blvd, Suite 108" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Memphis, TN 38132" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(901) 348-5774" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 1.115!
        Me.Label1.Width = 7.885!
        '
        'ReportFooter1
        '
        Me.ReportFooter1.Height = 0!
        Me.ReportFooter1.Name = "ReportFooter1"
        '
        'rptPackingSlip
        '
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.0!
        Me.Sections.Add(Me.ReportHeader1)
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        Me.Sections.Add(Me.ReportFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private WithEvents ReportHeader1 As GrapeCity.ActiveReports.SectionReportModel.ReportHeader
    Private WithEvents Picture1 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents ReportFooter1 As GrapeCity.ActiveReports.SectionReportModel.ReportFooter
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents ReportInfo1 As GrapeCity.ActiveReports.SectionReportModel.ReportInfo
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
End Class
