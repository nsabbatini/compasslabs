Imports GrapeCity.ActiveReports
Imports GrapeCity.ActiveReports.Document
Imports GrapeCity.ActiveReports.Document.Section

Public Class rptOpenPurchaseOrders
    Private Sub rptOpenPurchaseOrders_ReportStart(sender As Object, e As EventArgs) Handles Me.ReportStart
        Me.PageSettings.Orientation = PageOrientation.Portrait
        Me.PageSettings.Margins.Top = "0.1"
        Me.PageSettings.Margins.Bottom = "0.5"
        Me.PageSettings.Margins.Left = "0.2"
        Me.PageSettings.Margins.Right = 0
    End Sub
End Class
