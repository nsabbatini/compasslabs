Imports GrapeCity.ActiveReports
Imports GrapeCity.ActiveReports.Document
Imports GrapeCity.ActiveReports.Document.Section

Public Class rptPackingSlip
    Public Property OrderNumber As String
    Public Property AcctNumber As String
    Public Property ClientName As String
    Public Property ContactName As String
    Public Property Address As String
    Public Property CityStateZip As String
    Public Property Phone As String
    Public Property Fax As String
    Private Sub rptPackingSlip_DataInitialize(sender As Object, e As EventArgs) Handles MyBase.DataInitialize

        Me.Fields.Add("OrderNumber")
        Me.Fields.Add("AcctNumber")
        Me.Fields.Add("ClientName")
        Me.Fields.Add("ContactName")
        Me.Fields.Add("Address")
        Me.Fields.Add("CityStateZip")
        Me.Fields.Add("Phone")
        Me.Fields.Add("Fax")

    End Sub

    Private Sub rptPackingSlip_FetchData(sender As Object, eArgs As FetchEventArgs) Handles MyBase.FetchData

        Me.Fields("OrderNumber").Value = OrderNumber
        Me.Fields("AcctNumber").Value = AcctNumber
        Me.Fields("ClientName").Value = ClientName
        Me.Fields("ContactName").Value = ContactName
        Me.Fields("Address").Value = Address
        Me.Fields("CityStateZip").Value = CityStateZip
        Me.Fields("Phone").Value = Phone
        Me.Fields("Fax").Value = Fax

    End Sub

    Private Sub rptPackingSlip_ReportStart(sender As Object, e As EventArgs) Handles MyBase.ReportStart

        Me.PageSettings.Orientation = PageOrientation.Portrait
        Me.PageSettings.Margins.Top = "0.1"
        Me.PageSettings.Margins.Bottom = "0.5"
        Me.PageSettings.Margins.Left = "0.2"
        Me.PageSettings.Margins.Right = 0

    End Sub
End Class
