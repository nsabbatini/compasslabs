Imports GrapeCity.ActiveReports.Document.Section

Public Class rptPurchaseOrder
    Public _ClientName As String
    Public _Address As String
    Public _CityStateZip As String
    Public _VendorName As String
    Public _VAddress As String
    Public _VCityStateZip As String
    Public _VContact As String
    Public _PONumber As String
    Public _SubTotal As Double
    Public _SalesTax As Double
    Public _TotalSum As Double
    Public _Notes As String
    Public _SalesPerson As String
    Public _QuoteNumber As String
    Public _Department As String
    Public _PaymentTerms As String

    Public Sub rptPurchaseOrder_DataInitialize(sender As Object, e As EventArgs) Handles Me.DataInitialize
        Me.Fields.Add("ClientName")
        Me.Fields.Add("Address")
        Me.Fields.Add("CityStateZip")
        Me.Fields.Add("VendorName")
        Me.Fields.Add("VAddress")
        Me.Fields.Add("VCityStateZip")
        Me.Fields.Add("VContact")
        Me.Fields.Add("PONumber")
        Me.Fields.Add("PoDate")
        Me.Fields.Add("SubTotal")
        Me.Fields.Add("SalesTax")
        Me.Fields.Add("TotalSum")
        Me.Fields.Add("Notes")
        Me.Fields.Add("SalesPerson")
        Me.Fields.Add("QuoteNumber")
        Me.Fields.Add("Department")
        Me.Fields.Add("PaymentTerms")
    End Sub
    Private Sub rptPurchaseOrder_FetchData(sender As Object, eArgs As FetchEventArgs) Handles Me.FetchData
        Me.Fields("ClientName").Value = _ClientName
        Me.Fields("Address").Value = _Address
        Me.Fields("CityStateZip").Value = _CityStateZip
        Me.Fields("VendorName").Value = _VendorName
        Me.Fields("VAddress").Value = _VAddress
        Me.Fields("VCityStateZip").Value = _VCityStateZip
        Me.Fields("VContact").Value = _VContact
        Me.Fields("PONumber").Value = _PONumber
        Me.Fields("PoDate").Value = DateTime.Now().ToShortDateString()
        Me.Fields("SubTotal").Value = _SubTotal.ToString("C")
        Me.Fields("SalesTax").Value = _SalesTax.ToString("C")
        Me.Fields("TotalSum").Value = _TotalSum.ToString("C")
        Me.Fields("Notes").Value = _Notes
        Me.Fields("SalesPerson").Value = _SalesPerson
        Me.Fields("Department").Value = _Department
        Me.Fields("QuoteNumber").Value = _QuoteNumber
        Me.Fields("PaymentTerms").Value = _PaymentTerms

    End Sub

    Private Sub rptPurchaseOrder_ReportStart(sender As Object, e As EventArgs) Handles Me.ReportStart
        Me.PageSettings.Orientation = PageOrientation.Portrait
        Me.PageSettings.Margins.Top = "0.1"
        Me.PageSettings.Margins.Bottom = "0.5"
        Me.PageSettings.Margins.Left = "0.2"
        Me.PageSettings.Margins.Right = 0
    End Sub
End Class
