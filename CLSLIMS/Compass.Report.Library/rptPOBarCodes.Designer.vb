<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rptPOBarCodes
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    Private WithEvents PageFooter As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rptPOBarCodes))
        Me.PageHeader = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.PageFooter = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        Me.ReportHeader1 = New GrapeCity.ActiveReports.SectionReportModel.ReportHeader()
        Me.Barcode1 = New GrapeCity.ActiveReports.SectionReportModel.Barcode()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.ReportFooter1 = New GrapeCity.ActiveReports.SectionReportModel.ReportFooter()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Height = 0!
        Me.PageHeader.Name = "PageHeader"
        '
        'Detail
        '
        Me.Detail.Height = 0!
        Me.Detail.Name = "Detail"
        '
        'PageFooter
        '
        Me.PageFooter.Height = 0!
        Me.PageFooter.Name = "PageFooter"
        '
        'ReportHeader1
        '
        Me.ReportHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Barcode1, Me.Label1})
        Me.ReportHeader1.Height = 0.9791667!
        Me.ReportHeader1.Name = "ReportHeader1"
        '
        'Barcode1
        '
        Me.Barcode1.DataField = "BarCode"
        Me.Barcode1.Font = New System.Drawing.Font("Courier New", 8.0!)
        Me.Barcode1.Height = 0.506!
        Me.Barcode1.Left = 0.154!
        Me.Barcode1.Name = "Barcode1"
        Me.Barcode1.QuietZoneBottom = 0!
        Me.Barcode1.QuietZoneLeft = 0!
        Me.Barcode1.QuietZoneRight = 0!
        Me.Barcode1.QuietZoneTop = 0!
        Me.Barcode1.Style = GrapeCity.ActiveReports.SectionReportModel.BarCodeStyle.Code_128auto
        Me.Barcode1.Text = "FEDBOXMED,10"
        Me.Barcode1.Top = 0.057!
        Me.Barcode1.Width = 1.846!
        '
        'Label1
        '
        Me.Label1.DataField = "Description"
        Me.Label1.Height = 0.312!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.154!
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = CType(New GrapeCity.ActiveReports.PaddingEx(0, 0, 0, 0), GrapeCity.ActiveReports.PaddingEx)
        Me.Label1.Style = "font-size: 8pt; text-align: center"
        Me.Label1.Text = "Label1"
        Me.Label1.Top = 0.615!
        Me.Label1.Width = 1.846!
        '
        'ReportFooter1
        '
        Me.ReportFooter1.Height = 0!
        Me.ReportFooter1.Name = "ReportFooter1"
        '
        'rptPOBarCodes
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 2.0!
        Me.Sections.Add(Me.ReportHeader1)
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        Me.Sections.Add(Me.ReportFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private WithEvents ReportHeader1 As GrapeCity.ActiveReports.SectionReportModel.ReportHeader
    Private WithEvents Barcode1 As GrapeCity.ActiveReports.SectionReportModel.Barcode
    Private WithEvents ReportFooter1 As GrapeCity.ActiveReports.SectionReportModel.ReportFooter
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
End Class
