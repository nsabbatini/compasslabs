﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uClientInfo
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uClientInfo))
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtFax = New DevExpress.XtraEditors.TextEdit()
        Me.txtPhone = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtZip = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.txtState = New DevExpress.XtraEditors.TextEdit()
        Me.txtCity = New DevExpress.XtraEditors.TextEdit()
        Me.txtAddress = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtContactName = New DevExpress.XtraEditors.TextEdit()
        Me.txtAddress2 = New DevExpress.XtraEditors.TextEdit()
        Me.grdLookup = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.grdItems = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtQty = New DevExpress.XtraEditors.TextEdit()
        Me.dcbItems = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.cboCourier = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPhone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtZip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtContactName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAddress2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdLookup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dcbItems.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboCourier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(22, 314)
        Me.LabelControl8.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(35, 16)
        Me.LabelControl8.TabIndex = 52
        Me.LabelControl8.Text = "Phone"
        '
        'txtFax
        '
        Me.txtFax.Location = New System.Drawing.Point(201, 334)
        Me.txtFax.Margin = New System.Windows.Forms.Padding(2)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtFax.Properties.Appearance.Options.UseFont = True
        Me.txtFax.Size = New System.Drawing.Size(127, 30)
        Me.txtFax.TabIndex = 51
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(22, 334)
        Me.txtPhone.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtPhone.Properties.Appearance.Options.UseFont = True
        Me.txtPhone.Size = New System.Drawing.Size(142, 30)
        Me.txtPhone.TabIndex = 50
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(253, 255)
        Me.LabelControl7.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(50, 16)
        Me.LabelControl7.TabIndex = 49
        Me.LabelControl7.Text = "Zip Code"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(201, 255)
        Me.LabelControl6.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl6.TabIndex = 48
        Me.LabelControl6.Text = "State"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(22, 255)
        Me.LabelControl5.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(21, 16)
        Me.LabelControl5.TabIndex = 47
        Me.LabelControl5.Text = "City"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(22, 201)
        Me.LabelControl4.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(88, 16)
        Me.LabelControl4.TabIndex = 46
        Me.LabelControl4.Text = "Suite / Address"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(22, 141)
        Me.LabelControl3.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(46, 16)
        Me.LabelControl3.TabIndex = 45
        Me.LabelControl3.Text = "Address"
        '
        'txtZip
        '
        Me.txtZip.Location = New System.Drawing.Point(253, 275)
        Me.txtZip.Margin = New System.Windows.Forms.Padding(2)
        Me.txtZip.Name = "txtZip"
        Me.txtZip.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtZip.Properties.Appearance.Options.UseFont = True
        Me.txtZip.Size = New System.Drawing.Size(101, 30)
        Me.txtZip.TabIndex = 44
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridLookUpEdit1View.DetailHeight = 239
        Me.GridLookUpEdit1View.FixedLineWidth = 1
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Client"
        Me.GridColumn1.FieldName = "Aname"
        Me.GridColumn1.MinWidth = 13
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 50
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Account #"
        Me.GridColumn2.FieldName = "Acct"
        Me.GridColumn2.MinWidth = 13
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 50
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(201, 314)
        Me.LabelControl9.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(21, 16)
        Me.LabelControl9.TabIndex = 53
        Me.LabelControl9.Text = "Fax"
        '
        'txtState
        '
        Me.txtState.Location = New System.Drawing.Point(201, 275)
        Me.txtState.Margin = New System.Windows.Forms.Padding(2)
        Me.txtState.Name = "txtState"
        Me.txtState.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtState.Properties.Appearance.Options.UseFont = True
        Me.txtState.Size = New System.Drawing.Size(49, 30)
        Me.txtState.TabIndex = 43
        '
        'txtCity
        '
        Me.txtCity.Location = New System.Drawing.Point(22, 275)
        Me.txtCity.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtCity.Properties.Appearance.Options.UseFont = True
        Me.txtCity.Size = New System.Drawing.Size(175, 30)
        Me.txtCity.TabIndex = 42
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(22, 161)
        Me.txtAddress.Margin = New System.Windows.Forms.Padding(2)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtAddress.Properties.Appearance.Options.UseFont = True
        Me.txtAddress.Size = New System.Drawing.Size(333, 30)
        Me.txtAddress.TabIndex = 40
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(22, 13)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(76, 16)
        Me.LabelControl2.TabIndex = 39
        Me.LabelControl2.Text = "Client Lookup"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(22, 79)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(43, 16)
        Me.LabelControl1.TabIndex = 38
        Me.LabelControl1.Text = "Contact"
        '
        'txtContactName
        '
        Me.txtContactName.Location = New System.Drawing.Point(22, 99)
        Me.txtContactName.Margin = New System.Windows.Forms.Padding(2)
        Me.txtContactName.Name = "txtContactName"
        Me.txtContactName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtContactName.Properties.Appearance.Options.UseFont = True
        Me.txtContactName.Size = New System.Drawing.Size(333, 30)
        Me.txtContactName.TabIndex = 37
        '
        'txtAddress2
        '
        Me.txtAddress2.Location = New System.Drawing.Point(22, 221)
        Me.txtAddress2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.txtAddress2.Properties.Appearance.Options.UseFont = True
        Me.txtAddress2.Size = New System.Drawing.Size(333, 30)
        Me.txtAddress2.TabIndex = 41
        '
        'grdLookup
        '
        Me.grdLookup.EditValue = "Select a Client"
        Me.grdLookup.Location = New System.Drawing.Point(22, 30)
        Me.grdLookup.Margin = New System.Windows.Forms.Padding(2)
        Me.grdLookup.Name = "grdLookup"
        Me.grdLookup.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.grdLookup.Properties.Appearance.Options.UseFont = True
        Me.grdLookup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.grdLookup.Properties.DisplayMember = "Aname"
        Me.grdLookup.Properties.PopupView = Me.GridLookUpEdit1View
        Me.grdLookup.Properties.ValueMember = "Acct"
        Me.grdLookup.Size = New System.Drawing.Size(333, 30)
        Me.grdLookup.TabIndex = 36
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnAdd)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.grdItems)
        Me.GroupControl1.Controls.Add(Me.txtQty)
        Me.GroupControl1.Controls.Add(Me.dcbItems)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.cboCourier)
        Me.GroupControl1.Location = New System.Drawing.Point(364, 0)
        Me.GroupControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(482, 505)
        Me.GroupControl1.TabIndex = 54
        Me.GroupControl1.Text = "Enter Orders"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.ImageOptions.Image = CType(resources.GetObject("btnAdd.ImageOptions.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(287, 73)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(118, 37)
        Me.btnAdd.TabIndex = 27
        Me.btnAdd.Text = "Add"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(244, 64)
        Me.LabelControl12.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(18, 13)
        Me.LabelControl12.TabIndex = 26
        Me.LabelControl12.Text = "Qty"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(5, 67)
        Me.LabelControl11.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl11.TabIndex = 25
        Me.LabelControl11.Text = "Items"
        '
        'grdItems
        '
        Me.grdItems.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.grdItems.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(2)
        Me.grdItems.Location = New System.Drawing.Point(2, 116)
        Me.grdItems.MainView = Me.GridView1
        Me.grdItems.Margin = New System.Windows.Forms.Padding(2)
        Me.grdItems.Name = "grdItems"
        Me.grdItems.Size = New System.Drawing.Size(478, 387)
        Me.grdItems.TabIndex = 24
        Me.grdItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5})
        Me.GridView1.DetailHeight = 239
        Me.GridView1.FixedLineWidth = 1
        Me.GridView1.GridControl = Me.grdItems
        Me.GridView1.Name = "GridView1"
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Line #"
        Me.GridColumn6.FieldName = "Line"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 0
        Me.GridColumn6.Width = 54
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Item"
        Me.GridColumn3.FieldName = "ItemDesc"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 442
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Id"
        Me.GridColumn4.FieldName = "Id"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Qty"
        Me.GridColumn5.FieldName = "Qty"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        Me.GridColumn5.Width = 109
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(244, 81)
        Me.txtQty.Margin = New System.Windows.Forms.Padding(2)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtQty.Properties.Appearance.Options.UseFont = True
        Me.txtQty.Size = New System.Drawing.Size(35, 26)
        Me.txtQty.TabIndex = 23
        '
        'dcbItems
        '
        Me.dcbItems.Location = New System.Drawing.Point(4, 81)
        Me.dcbItems.Margin = New System.Windows.Forms.Padding(2)
        Me.dcbItems.Name = "dcbItems"
        Me.dcbItems.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dcbItems.Properties.Appearance.Options.UseFont = True
        Me.dcbItems.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dcbItems.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ItemDesc", "Vendor Desc"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Item Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("CurrentQty", "CurrentQty", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[Default])})
        Me.dcbItems.Properties.DropDownRows = 20
        Me.dcbItems.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch
        Me.dcbItems.Size = New System.Drawing.Size(236, 26)
        Me.dcbItems.TabIndex = 22
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(4, 22)
        Me.LabelControl10.Margin = New System.Windows.Forms.Padding(2)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(35, 13)
        Me.LabelControl10.TabIndex = 1
        Me.LabelControl10.Text = "Courier"
        '
        'cboCourier
        '
        Me.cboCourier.Location = New System.Drawing.Point(4, 37)
        Me.cboCourier.Margin = New System.Windows.Forms.Padding(2)
        Me.cboCourier.Name = "cboCourier"
        Me.cboCourier.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.cboCourier.Properties.Appearance.Options.UseFont = True
        Me.cboCourier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboCourier.Properties.Items.AddRange(New Object() {"UPS Ground", "Fedex Ground", "Fedex Overnight"})
        Me.cboCourier.Size = New System.Drawing.Size(152, 26)
        Me.cboCourier.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.ImageOptions.Image = CType(resources.GetObject("btnSave.ImageOptions.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(201, 435)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(138, 41)
        Me.btnSave.TabIndex = 55
        Me.btnSave.Text = "Save Order"
        '
        'btnCancel
        '
        Me.btnCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.btnCancel.Appearance.Options.UseFont = True
        Me.btnCancel.ImageOptions.Image = CType(resources.GetObject("btnCancel.ImageOptions.Image"), System.Drawing.Image)
        Me.btnCancel.Location = New System.Drawing.Point(26, 435)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(138, 41)
        Me.btnCancel.TabIndex = 56
        Me.btnCancel.Text = "Cancel Order"
        '
        'uClientInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.txtFax)
        Me.Controls.Add(Me.txtPhone)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.txtZip)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.txtState)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.txtContactName)
        Me.Controls.Add(Me.txtAddress2)
        Me.Controls.Add(Me.grdLookup)
        Me.Name = "uClientInfo"
        Me.Size = New System.Drawing.Size(848, 507)
        CType(Me.txtFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPhone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtZip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtContactName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAddress2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdLookup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dcbItems.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboCourier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPhone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtZip As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtContactName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAddress2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grdLookup As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtQty As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dcbItems As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboCourier As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
End Class
