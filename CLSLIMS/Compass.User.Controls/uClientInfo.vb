﻿Imports System.Windows.Forms
Imports Compass.Global
Imports Compass.MySql.Database
Imports Compass.Report.Library
Imports Compass.Sql.Database
Imports GrapeCity.ActiveReports
Public Class uClientInfo
    Public _fileName As String
    Public _fileDir As String
    Public _ClientName As String
    Public _ContactName As String
    Public _Address As String
    Public _CityStateZip As String
    Public _Phone As String
    Public _Fax As String
    Private _dtInventory As DataTable = New DataTable
    Private _line As Int16 = 0
    Private _orderNumber As Int16
    Public _OrderSaved As Boolean = False

    Public Sub uClientInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        grdLookup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True

        dcbItems.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        GridView1.OptionsView.ShowGroupPanel = False
        GridView1.OptionsView.ShowIndicator = False

    End Sub
    Public Sub LoadTheForm()

        Dim client = GetClientInformation()

        grdLookup.Properties.DataSource = client
        grdLookup.Properties.DisplayMember = "Aname"
        grdLookup.Properties.ValueMember = "Acct"

    End Sub
    Private Sub LoadItems()

        Dim results = GetInventoryDb()

        dcbItems.Properties.DataSource = results
        dcbItems.Properties.DisplayMember = "ItemDesc"
        dcbItems.Properties.ValueMember = "Id"


    End Sub
    Private Sub AddItemsToGrid()
        Dim arow As DataRow



        With _dtInventory

            If Not CheckIfAlreadyInGrid(dcbItems.EditValue) Then
                _line += 1

                arow = .NewRow
                arow(0) = _line
                arow(1) = dcbItems.SelectedText
                arow(2) = dcbItems.EditValue
                arow(3) = txtQty.Text

                .Rows.Add(arow)
            Else
                MsgBox("This item already exits in the grid!", MsgBoxStyle.Information, "Item Exists")
                Exit Sub
            End If

        End With

        grdItems.DataSource = _dtInventory.DefaultView


    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs)
        If txtQty.Text = String.Empty OrElse Not IsNumeric(txtQty.Text) Then
            MsgBox("Must enter a numeric value in the quantity field!", MsgBoxStyle.Critical, "Invalid value")
            txtQty.Focus()
            Exit Sub
        End If
        AddItemsToGrid()
        ClearFormAfterAdd()
    End Sub
    Private Sub ClearFormAfterAdd()
        dcbItems.EditValue = DBNull.Value
        dcbItems.SelectedText = ""
        dcbItems.Focus()
        txtQty.Text = ""
    End Sub
    Private Function GetInventoryDb() As Object

        Try

            Using getInventory As New GetInventoryDb()

                If CustomerAcctType = "M" Then
                    _fileName = _fileDir & GetMoleclarSql
                Else
                    _fileName = _fileDir & GetNonMolecularSql
                End If
                getInventory._sqlText = ReadSqlFile(_fileName)

                Return getInventory.GetInventory.ToList

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMainOrderForm.GetInventoryDb", ex.StackTrace.ToString)
            End Using
        End Try


    End Function
    Private Function GetClientInformation() As Object

        Try

            Using getInventory As New GetClientInfoDb()

                _fileName = _fileDir & GetClientInformationSql
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.SetClientInfo.ToList()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "uClientInfo.GetClientInformation", ex.StackTrace.ToString)
            End Using
        End Try


    End Function
    Private Sub grdLookup_EditValueChanged_1(sender As Object, e As EventArgs) Handles grdLookup.EditValueChanged
        Try
            Dim arow = grdLookup.GetSelectedDataRow

            If arow Is Nothing Then
                Exit Sub
            End If

            _ClientName = arow.GetType().GetProperty("Aname")?.GetValue(arow)
            txtContactName.Text = arow.GetType().GetProperty("Acontact")?.GetValue(arow)
            _ContactName = txtContactName.Text.Trim()
            txtAddress.Text = arow.GetType().GetProperty("Aad1")?.GetValue(arow)
            txtAddress2.Text = arow.GetType().GetProperty("Aad2")?.GetValue(arow)
            _Address = txtAddress.Text.Trim() & " " & txtAddress2.Text.Trim()
            txtCity.Text = arow.GetType().GetProperty("Acity")?.GetValue(arow)
            txtFax.Text = arow.GetType().GetProperty("Afax")?.GetValue(arow)
            _Fax = txtFax.Text
            txtPhone.Text = arow.GetType().GetProperty("Aphone")?.GetValue(arow)
            _Phone = txtPhone.Text
            txtState.Text = arow.GetType().GetProperty("Astate")?.GetValue(arow)
            txtZip.Text = arow.GetType().GetProperty("Azip")?.GetValue(arow)
            _CityStateZip = txtCity.Text.Trim() & ", " & txtState.Text.Trim().ToUpper & " " & txtZip.Text.Trim()


            CustomerAcctNumber = arow.GetType().GetProperty("Acct")?.GetValue(arow)
            CustomerAcctType = Convert.ToString(arow.GetType().GetProperty("Acct")?.GetValue(arow)).Substring(0, 1)

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "uClientInfo.GetClientInformation", ex.StackTrace.ToString)
            End Using
        End Try
    End Sub
    Public Sub ClearForm()
        txtContactName.Text = ""
        txtAddress.Text = ""
        txtAddress2.Text = ""
        txtCity.Text = ""
        txtFax.Text = ""
        txtPhone.Text = ""
        txtState.Text = ""
        txtZip.Text = ""

        CustomerAcctNumber = ""
        CustomerAcctType = ""
        _dtInventory.Clear()
        grdLookup.EditValue = Nothing

        LoadTheForm()
    End Sub

    Private Sub txtQty_KeyDown_1(sender As Object, e As KeyEventArgs) Handles txtQty.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtQty.Text = String.Empty OrElse Not IsNumeric(txtQty.Text) Then
                MsgBox("Must enter a numeric value in the quantity field!", MsgBoxStyle.Critical, "Invalid value")
                txtQty.Focus()
                Exit Sub
            End If
            AddItemsToGrid()
            ClearFormAfterAdd()
        End If
    End Sub

    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCourier.SelectedIndexChanged
        If _dtInventory.Rows.Count <= 0 Then
            _dtInventory = CreateData()
        End If
        LoadItems()
    End Sub
    Private Function CreateData() As DataTable
        _dtInventory.TableName = "Orders"

        Dim _dtItems As DataTable = New DataTable
        _dtItems.Columns.Add("Line", GetType(Int16))
        _dtItems.Columns.Add("ItemDesc", GetType(String))
        _dtItems.Columns.Add("Id", GetType(Int16))
        _dtItems.Columns.Add("Qty", GetType(Int16))

        Return _dtItems

    End Function

    Private Sub btnAdd_Click_1(sender As Object, e As EventArgs) Handles btnAdd.Click
        If txtQty.Text = String.Empty OrElse Not IsNumeric(txtQty.Text) Then
            MsgBox("Must enter a numeric value in the quantity field!", MsgBoxStyle.Critical, "Invalid value")
            txtQty.Focus()
            Exit Sub
        End If
        AddItemsToGrid()
        ClearFormAfterAdd()
    End Sub
    Private Function CheckIfAlreadyInGrid(ByVal theId As Int16) As Boolean

        For i As Integer = 0 To GridView1.DataRowCount - 1
            If theId = CType(GridView1.GetRowCellValue(i, "Id"), Int16) Then
                Return True
            End If
        Next

    End Function

    Private Sub dcbItems_EditValueChanged_1(sender As Object, e As EventArgs) Handles dcbItems.EditValueChanged
        txtQty.Focus()
        txtQty.Text = ""
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If SaveOrder() Then
            _OrderSaved = True

            PrintPackingSlip()
        End If
    End Sub
    Private Function SaveOrder() As Boolean
        Dim NewOrd As Boolean = False, TodayNow As String = Format(Now, "yyyyMMddHHmmss"), Status As String = "0", IStatus As String = "0", HSEQ As Integer = 0
        Dim arr As New ArrayList


        If GridView1.DataRowCount = 0 Then
            MsgBox("No items ordered, enter at least one before trying to save!", MsgBoxStyle.Exclamation, "No Data")
            Exit Function
        End If

        Try
            'generate the new order number
            Using getOrderNumber As New GetNewOrderNumber()
                _orderNumber = getOrderNumber.GetOrderNumber
                If _orderNumber = 0 Then
                    MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
                End If
            End Using


            _fileName = _fileDir & InsertOrderSql


            Using saveOrders As New OrdersDb() With {._sqlText = ReadSqlFile(_fileName)}
                arr.Add(grdLookup.EditValue)
                arr.Add(_ClientName)
                arr.Add(txtContactName.Text)
                arr.Add(txtAddress.Text)
                arr.Add(txtAddress2.Text)
                arr.Add(txtCity.Text)
                arr.Add(txtState.Text.ToUpper)
                arr.Add(txtZip.Text)
                arr.Add(txtPhone.Text)
                arr.Add(txtFax.Text)
                arr.Add(cboCourier.Text)
                arr.Add(LoggedinUserName)
                If LoggedinUserName = "REMOND.YOUNG" Then
                    arr.Add(99)
                Else
                    arr.Add(1)
                End If
                arr.Add(_orderNumber)

                Try
                    saveOrders.SaveOrders(arr)
                Catch ex As Exception
                    Using recorderror As New ErrorLog()
                        recorderror.LogError(ex.Message.ToString, "uClientInfo.SaveOrder", ex.StackTrace.ToString)
                    End Using
                End Try
            End Using

            ''save the ordered 
            Dim orderId = GetMaxOrdersId()
            arr.Clear()

            Try
                _fileName = _fileDir & InsertOrderItemsSql
                Using items As New OrdersDb() With {._sqlText = ReadSqlFile(_fileName)}
                    For i = 0 To GridView1.DataRowCount - 1
                        arr.Add(orderId)
                        arr.Add(CType(GridView1.GetRowCellValue(i, "Id"), Int16))
                        arr.Add(CType(GridView1.GetRowCellValue(i, "Qty"), Int16))
                        items.SaveOrderedItems(arr)
                        arr.Clear()
                    Next
                End Using
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "uClientInfo.SaveOrderItems", ex.StackTrace.ToString)
                End Using
            End Try


            _fileName = _fileDir & SetItemsCurrentQtySql
            Dim x As Int16
            'reduce the onhand inventory
            Using items As New InventoryControl() With {._sqlText = ReadSqlFile(_fileName)}
                For i = 0 To GridView1.DataRowCount - 1
                    items._itemId = CType(GridView1.GetRowCellValue(i, "Id"), Int16)
                    items._qty = CType(GridView1.GetRowCellValue(i, "Qty"), Int16)
                    x = items.ReduceInventory
                Next
                If Not x = GridView1.DataRowCount Then
                    'something went wrong
                End If
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmMainOrderForm.SaveOrder", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "An error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
            Return False
        End Try

        _line = 0

        Return True


    End Function
    Private Function GetMaxOrdersId() As Int16
        Try

            Using getOrders As New OrdersDb()

                _fileName = _fileDir & OrdersSql
                getOrders._sqlText = ReadSqlFile(_fileName)
                Return getOrders.GetOrders.Max(Function(x) x.OrderId)

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "uClientInfo.GetClientInformation", ex.StackTrace.ToString)
            End Using
        End Try

    End Function
    Private Sub PrintPackingSlip()
        Using rpt As New rptPackingSlip() With {.DataSource = _dtInventory, .DataMember = "Orders"}

            rpt.AcctNumber = CustomerAcctNumber
            rpt.OrderNumber = _orderNumber
            rpt.ClientName = grdLookup.SelectedText
            rpt.ContactName = txtContactName.Text
            rpt.Address = txtAddress.Text
            rpt.CityStateZip = txtCity.Text & ", " & txtState.Text & " " & txtZip.Text
            rpt.Phone = txtPhone.Text
            rpt.Fax = txtFax.Text

            rpt.Run()

            Dim sectionDocument = rpt.Document

            sectionDocument.Print(True, True, False)

        End Using

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ClearForm()

    End Sub
End Class
