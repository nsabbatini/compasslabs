﻿Imports MySql.Data.MySqlClient

Public Class PurchaseOrders
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Private mdsSet As DataSet
    Public _sqlText As String
    Public _vendorId As String
    Public _poNumber As String
    Public _itemId As Int16
    Public _qty As Int16
    Public _terms As String
    Public _notes As String
    Public _department As Int16
    Public _total As Double
    Public _lineItemCost As Double
    Public _unitCost As Double
    Public _shipTo As Int16
    Public _salesTax As Double
    Public _pnumber As Int16
    Public _salesPerson As String
    Public _quoteNumber As String
    Public _paymentTerms As String
    Public _needsapproval As Int16
    Public _nonInventory As Int16 = 0
    Public _itemDescription As String = String.Empty

    Private x As Int16 = 0
    ''' <summary>
    ''' 'Reconcile PO
    ''' </summary>
    ''' 
    Public _did As Int16 = 0
    Public _pid As Int16 = 0
    Public _orderQty As Int16
    Public _receivedQty As Int16
    Public _backOrdered As Int16
    Public _received As Int16
    Public _status As Int16

    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then

                'mdsReader.Close()


                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function GetPurchaseOrder() As IEnumerable(Of GetPurchaseOrders)

        Dim poItems As List(Of GetPurchaseOrders) = New List(Of GetPurchaseOrders)

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdsReader = mdscmd.ExecuteReader

            While mdsReader.Read()

                Dim poModel = New GetPurchaseOrders With {
                    .PId = mdsReader.GetValue(0),
                    .PoNumber = mdsReader.GetValue(2),
                    .TotalPo = mdsReader.GetValue(1),
                    .VId = mdsReader.GetValue(3),
                    .PoDate = mdsReader.GetValue(4),
                    .PoTerms = mdsReader.GetValue(5),
                    .Notes = mdsReader.GetValue(6),
                    .Department = mdsReader.GetValue(7),
                    .Status = mdsReader.GetValue(8),
                    .Approved = CType(mdsReader.GetValue(9), Int16),
                    .ShipTo = mdsReader.GetValue(10),
                    .SalesTax = mdsReader.GetValue(11),
                    .SalesPerson = mdsReader.GetValue(12),
                    .QuoteNumber = mdsReader.GetValue(13),
                    .PaymentTerms = mdsReader.GetValue(14),
                    .NonInventory = mdsReader.GetValue(15)
                    }


                poItems.Add(poModel)

            End While

            Return poItems

        End Using

    End Function
    Public Function GetPurchaseOrderDetail() As IEnumerable(Of GetPurchaseOrderDetails)

        Dim podetails As List(Of GetPurchaseOrderDetails) = New List(Of GetPurchaseOrderDetails)

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}
            mdsReader = mdscmd.ExecuteReader

            While mdsReader.Read()
                Dim podetailModel = New GetPurchaseOrderDetails With {
                    .DId = mdsReader.GetValue(0),
                    .PId = mdsReader.GetValue(1),
                    .ItemId = mdsReader.GetValue(2),
                    .OrderQty = mdsReader.GetValue(3),
                    .ItemCost = mdsReader.GetValue(4),
                    .UnitCost = mdsReader.GetValue(5),
                    .BackOrdered = mdsReader.GetValue(6),
                    .NonInventory = mdsReader.GetValue(7)
                }

                podetails.Add(podetailModel)

            End While

            Return podetails

        End Using
    End Function
    Public Function InsertPurchaseOrder() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@PoNumber", MySqlDbType.VarChar).Value = _poNumber
            mdscmd.Parameters.Add("@VId", MySqlDbType.Int16).Value = _vendorId
            mdscmd.Parameters.Add("@Terms", MySqlDbType.VarChar).Value = _terms
            mdscmd.Parameters.Add("@Notes", MySqlDbType.VarChar).Value = _notes
            mdscmd.Parameters.Add("@Department", MySqlDbType.VarChar).Value = _department
            mdscmd.Parameters.Add("@Total", MySqlDbType.Float).Value = _total
            mdscmd.Parameters.Add("@ShipTo", MySqlDbType.Int16).Value = _shipTo
            mdscmd.Parameters.Add("@SalesTax", MySqlDbType.Float).Value = _salesTax
            mdscmd.Parameters.Add("@SalesPerson", MySqlDbType.String).Value = _salesPerson
            mdscmd.Parameters.Add("@QuoteNumber", MySqlDbType.String).Value = _quoteNumber
            mdscmd.Parameters.Add("@PaymentTerms", MySqlDbType.String).Value = _paymentTerms
            mdscmd.Parameters.Add("@NeedsApproval", MySqlDbType.Int16).Value = _needsapproval
            mdscmd.Parameters.Add("@NonInventory", MySqlDbType.Int16).Value = _nonInventory

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.InsertPurchaseOrder", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function UpdatePurchaseOrder() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@Notes", MySqlDbType.VarChar).Value = _notes
            mdscmd.Parameters.Add("@Department", MySqlDbType.VarChar).Value = _department
            mdscmd.Parameters.Add("@Total", MySqlDbType.Float).Value = _total
            mdscmd.Parameters.Add("@SalesTax", MySqlDbType.Float).Value = _salesTax
            mdscmd.Parameters.Add("@SalesPerson", MySqlDbType.String).Value = _salesPerson
            mdscmd.Parameters.Add("@QuoteNumber", MySqlDbType.String).Value = _quoteNumber
            mdscmd.Parameters.Add("@PaymentTerms", MySqlDbType.String).Value = _paymentTerms
            mdscmd.Parameters.Add("@PId", MySqlDbType.String).Value = _pid

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.UpdatePurchaseOrder", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function InsertPurchaseOrderDetails() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@ItemId", MySqlDbType.Int16).Value = _itemId
            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int16).Value = _qty
            mdscmd.Parameters.Add("@ItemCost", MySqlDbType.Float).Value = _lineItemCost
            mdscmd.Parameters.Add("@UnitCost", MySqlDbType.Float).Value = _unitCost
            mdscmd.Parameters.Add("@ItemDesc", MySqlDbType.VarChar, 100).Value = _itemDescription

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.InsertPurchaseOrderDetails", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function UpdatePurchaseOrderDetails() As Boolean

        Using mdscmd As New MySqlCommand("UpdatePoDetails", MyConnection) With {.CommandType = CommandType.StoredProcedure}

            mdscmd.Parameters.Add("@ItemId", MySqlDbType.Int16).Value = _itemId
            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int16).Value = _qty
            mdscmd.Parameters.Add("@ItemCost", MySqlDbType.Float).Value = _lineItemCost
            mdscmd.Parameters.Add("@UnitCost", MySqlDbType.Float).Value = _unitCost
            mdscmd.Parameters.Add("@ItemDesc", MySqlDbType.VarChar, 400).Value = _itemDescription
            mdscmd.Parameters.Add("@DId", MySqlDbType.Int16).Value = _did
            mdscmd.Parameters.Add("@PId", MySqlDbType.Int16).Value = _pid

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.UpdatePurchaseOrderDetails", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function GetNextPONumber() As Int16
        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            Try
                mdsReader = mdscmd.ExecuteReader
                If mdsReader.HasRows Then
                    mdsReader.Read()

                    GetNextPONumber = mdsReader.GetValue(0)
                End If
                mdsReader.Close()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.GetNextPONumber", ex.StackTrace.ToString)
                End Using
                mdsReader.Close()
                Return False
            End Try

        End Using

        Return GetNextPONumber

    End Function
    Public Function GetPurchaseOrderItems() As DataSet

        Try
            Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}
                mdscmd.Parameters.Add("@PoNumber", MySqlDbType.Int16).Value = _pnumber

                Using mdsAdapter As New MySqlDataAdapter(mdscmd)
                    mdsSet = New DataSet
                    mdsAdapter.Fill(mdsSet, "Items")
                End Using

            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "PurchaseOrders.GetPurchaseOrderItems", ex.StackTrace.ToString)
            End Using
        End Try


        Return mdsSet


    End Function
    Public Function ReconcilePo() As IEnumerable(Of ReconcilePurchaseOrder)

        Dim podetails As List(Of ReconcilePurchaseOrder) = New List(Of ReconcilePurchaseOrder)

        Try
            Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}


                mdsReader = mdscmd.ExecuteReader

                While mdsReader.Read()
                    Dim podetailModel = New ReconcilePurchaseOrder With {
                        .DId = CShort(mdsReader.GetValue(0)?.ToString()),
                        .PId = CShort(mdsReader.GetValue(1)?.ToString()),
                        .OrderQty = CShort(mdsReader.GetValue(2)?.ToString()),
                        .UnitPrice = CDbl(mdsReader.GetValue(3)?.ToString()),
                        .ItemCost = CDbl(mdsReader.GetValue(4)?.ToString()),
                        .ItemDesc = mdsReader.GetValue(5)?.ToString(),
                        .ReceivedQty = CShort(mdsReader.GetValue(6)?.ToString()),
                        .BackOrdered = mdsReader.GetValue(7)?.ToString(),
                        .Received = mdsReader.GetValue(8)?.ToString(),
                        .ItemId = CShort(mdsReader.GetValue(9)?.ToString()),
                        .PONumber = mdsReader.GetValue(10)?.ToString(),
                        .VendorId = CShort(mdsReader.GetValue(11)?.ToString()),
                        .ExpirationDate = CShort(mdsReader.GetValue(12)?.ToString()),
                        .Units = mdsReader.GetValue(13)?.ToString()
                    }

                    podetails.Add(podetailModel)

                End While

                Return podetails

            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "PurchaseOrders.ReconcilePo", ex.StackTrace.ToString)
            End Using
        End Try


    End Function
    Public Function ReceiveInventoryFromPo() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@DId", MySqlDbType.Int16).Value = _did
            mdscmd.Parameters.Add("@PId", MySqlDbType.Int16).Value = _pid
            mdscmd.Parameters.Add("@OrderQty", MySqlDbType.Int16).Value = _orderQty
            mdscmd.Parameters.Add("@ReceivedQty", MySqlDbType.Int16).Value = _receivedQty
            mdscmd.Parameters.Add("@BackOrdered", MySqlDbType.Int16).Value = _backOrdered
            mdscmd.Parameters.Add("@Received", MySqlDbType.Int16).Value = _received
            mdscmd.Parameters.Add("@ItemId", MySqlDbType.Int16).Value = _itemId

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.ReceiveInventoryFromPo", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True
    End Function
    Public Function SetStatusPurchaseOrder() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@PId", MySqlDbType.Int16).Value = _pid
            mdscmd.Parameters.Add("@Status", MySqlDbType.Int16).Value = _status


            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.SetStatusPurchaseOrder", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True
    End Function
    Public Function SetNewQuantityItems() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@ItemId", MySqlDbType.Int16).Value = _itemId
            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int16).Value = _qty


            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.SetNewQuantityItems", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True
    End Function
    Public Function InsertNewPurchaseOrderDetails() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@ItemId", MySqlDbType.Int16).Value = _itemId
            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int16).Value = _qty
            mdscmd.Parameters.Add("@PId", MySqlDbType.Int16).Value = _pid

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.InsertPurchaseOrderDetails", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function GetPoHistoryDetails() As DataSet
        Dim mdsSet As New DataSet

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
                .CommandType = CommandType.Text}

            Try
                Using mdsAdapter As New MySqlDataAdapter(mdscmd)
                    mdsAdapter.Fill(mdsSet, "MasterDetail")
                End Using

            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "PurchaseOrders.GetPoHistoryDetails", ex.StackTrace.ToString)
                End Using
            End Try
        End Using

        Return mdsSet

    End Function
End Class
