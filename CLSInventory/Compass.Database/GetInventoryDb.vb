﻿Imports MySql.Data.MySqlClient

Public Class GetInventoryDb
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Public _sqlText As String
    Public _vendorId As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Not mdsReader Is Nothing Then
                    mdsReader.Close()
                End If

                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub

    Public Function GetInventory() As IEnumerable(Of GetInventoryItems)

        Dim invItems As List(Of GetInventoryItems) = New List(Of GetInventoryItems)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text
        }
            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim inventoryModel = New GetInventoryItems With {
                        .Id = Convert.ToInt16(mdsReader.GetValue(0)?.ToString()),
                        .VId = Convert.ToInt16(mdsReader.GetValue(1)?.ToString()),
                        .DeptId = Convert.ToInt16(mdsReader.GetValue(2)?.ToString()),
                        .ItemDesc = mdsReader.GetValue(3)?.ToString().ToUpper,
                        .ItemUnit = mdsReader.GetValue(4)?.ToString(),
                        .ItemPerUnit = Convert.ToInt16(mdsReader.GetValue(5)?.ToString()),
                        .Vendoritemid = mdsReader.GetValue(6)?.ToString(),
                        .ItemVendorCost = Convert.ToDouble(mdsReader.GetValue(7)?.ToString()),
                        .MinQty = Convert.ToInt16(mdsReader.GetValue(8)?.ToString()),
                        .CurrentQty = Convert.ToInt64(mdsReader.GetValue(9)?.ToString()),
                        .Location = mdsReader.GetValue(10)?.ToString(),
                        .NeedsLotNum = CType(mdsReader.GetValue(11)?.ToString(), Boolean),
                        .Discontinued = CType(mdsReader.GetValue(12)?.ToString(), Boolean)
                }


                invItems.Add(inventoryModel)
            End While

            Return invItems.ToList()

        End Using
    End Function
    Public Function GetInventoryForUpdate() As IEnumerable(Of GetInventoryItemsForUpdate)

        Dim invItems As List(Of GetInventoryItemsForUpdate) = New List(Of GetInventoryItemsForUpdate)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text
        }
            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim inventoryModel = New GetInventoryItemsForUpdate With {
                        .itemid = mdsReader.GetValue(1)?.ToString(),
                        .ItemDesc = mdsReader.GetValue(2)?.ToString().ToUpper,
                        .ItemUnit = mdsReader.GetValue(6)?.ToString(),
                        .ItemVendorCost = Convert.ToDouble(mdsReader.GetValue(5)?.ToString()),
                        .MinQty = Convert.ToInt16(mdsReader.GetValue(4)?.ToString()),
                        .CurrentQty = Convert.ToInt16(mdsReader.GetValue(3)?.ToString()),
                        .Id = Convert.ToInt16(mdsReader.GetValue(0)?.ToString()),
                        .VendorName = Convert.ToString(mdsReader.GetValue(7)?.ToString())
                }

                invItems.Add(inventoryModel)
            End While

            Return invItems.ToList()

        End Using
    End Function
    Public Function GetInventoryForGrid() As DataSet

        Dim dsSet As New DataSet

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text
        }

            Using mdsAdapter As New MySqlDataAdapter(mdscmd)

                mdsAdapter.Fill(dsSet, "Items")
            End Using

        End Using

        Return dsSet


    End Function

    Public Function GetVendorInfo() As IEnumerable(Of GetVendorInfo)

        Dim lstVendor As List(Of GetVendorInfo) = New List(Of GetVendorInfo)


        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text
        }
            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim vendorModel = New GetVendorInfo With {
                    .Id = mdsReader.GetValue(0),
                    .VendorName = mdsReader.GetValue(1)?.ToString.ToUpper,
                    .Address = mdsReader.GetValue(2)?.ToString,
                    .Address2 = mdsReader.GetValue(3)?.ToString,
                    .City = mdsReader.GetValue(4)?.ToString,
                    .State = mdsReader.GetValue(5)?.ToString,
                    .Zip = mdsReader.GetValue(6)?.ToString,
                    .Contact = mdsReader.GetValue(7)?.ToString,
                    .Phone = mdsReader.GetValue(8)?.ToString,
                    .Fax = mdsReader.GetValue(9)?.ToString,
                    .Notes = mdsReader.GetValue(10)?.ToString
                }


                lstVendor.Add(vendorModel)
            End While

            Return lstVendor.ToList()

        End Using


    End Function

End Class
