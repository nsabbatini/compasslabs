﻿Imports MySql.Data.MySqlClient
Public Class Departments
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Private mdsSet As DataSet
    Public _sqlText As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Not mdsReader Is Nothing Then
                    mdsReader.Close()
                End If
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function GetDepartmentInfo() As IEnumerable(Of GetDepartments)

        Dim deptDetails As List(Of GetDepartments) = New List(Of GetDepartments)

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}
            mdsReader = mdscmd.ExecuteReader

            While mdsReader.Read()
                Dim deptModel = New GetDepartments With {
                    .DepartmentId = mdsReader.GetValue(0),
                    .DepartmentName = mdsReader.GetValue(1)
                }

                deptDetails.Add(deptModel)

            End While

            Return deptDetails

        End Using
    End Function
End Class
