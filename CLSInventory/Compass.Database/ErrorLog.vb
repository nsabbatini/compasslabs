﻿Imports MySql.Data.MySqlClient
Imports Compass.Global
Public Class ErrorLog
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Public _sqlText As String
    Public _vendorId As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                ' mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Sub LogError(ByVal errorMsg As String, ByVal whereOccured As String, Optional ByVal otherNotes As String = "")

        Using mdscmd As New MySqlCommand(ErrorSql, MyConnection) With {.CommandType = CommandType.Text}
            mdscmd.Parameters.AddWithValue("@ErrorMsg", errorMsg)
            mdscmd.Parameters.AddWithValue("@WhereOccured", whereOccured)
            mdscmd.Parameters.AddWithValue("@Notes", otherNotes)

            Try
                mdscmd.ExecuteNonQuery()

            Catch ex As Exception

            Finally
                SendErrorEmail(errorMsg, whereOccured, otherNotes)

            End Try

        End Using

    End Sub

End Class
