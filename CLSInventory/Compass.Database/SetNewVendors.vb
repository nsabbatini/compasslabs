﻿Imports MySql.Data.MySqlClient

Public Class SetNewVendors
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Public _sqlText As String

    Public Property VendorId As String
    Public Property VendorName As String
    Public Property VendorAbbr As String
    Public Property Address As String = ""
    Public Property Address2 As String = ""
    Public Property City As String = ""
    Public Property State As String = ""
    Public Property Zip As String = ""
    Public Property Contact As String = ""
    Public Property Phone As String = ""
    Public Property Fax As String = ""
    Public Property Notes As String = ""
    Public Property vId As Int16
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                'mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function SaveNewVendor() As Boolean

        Try


            Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

                mdscmd.Parameters.Add("@VendorName", MySqlDbType.VarChar, 100).Value = VendorName

                mdscmd.Parameters.Add("@Address", MySqlDbType.VarChar, 20).Value = Address

                mdscmd.Parameters.Add("@Address2", MySqlDbType.VarChar, 20).Value = Address2

                mdscmd.Parameters.Add("@City", MySqlDbType.VarChar, 20).Value = City

                mdscmd.Parameters.Add("@State", MySqlDbType.VarChar, 2).Value = State

                mdscmd.Parameters.Add("@Zip", MySqlDbType.VarChar, 10).Value = Zip

                mdscmd.Parameters.Add("@Contact", MySqlDbType.VarChar, 50).Value = Contact

                mdscmd.Parameters.Add("@Phone", MySqlDbType.VarChar, 15).Value = Phone

                mdscmd.Parameters.Add("@Fax", MySqlDbType.VarChar, 15).Value = Fax

                mdscmd.Parameters.Add("@Notes", MySqlDbType.VarChar, 400).Value = Notes

                mdscmd.ExecuteNonQuery()

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "SetNewVendors.SaveNewVendor", ex.StackTrace.ToString)
            End Using
        End Try

        Return True

    End Function
    Public Function UpdateVendors() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}


            mdscmd.Parameters.Add("@VendorName", MySqlDbType.VarChar, 100).Value = StrConv(VendorName, vbProperCase)

            mdscmd.Parameters.Add("@Address", MySqlDbType.VarChar, 20).Value = StrConv(Address, vbProperCase)

            mdscmd.Parameters.Add("@Address2", MySqlDbType.VarChar, 20).Value = Address2

            mdscmd.Parameters.Add("@City", MySqlDbType.VarChar, 20).Value = StrConv(City, vbProperCase)

            mdscmd.Parameters.Add("@State", MySqlDbType.VarChar, 2).Value = State

            mdscmd.Parameters.Add("@Zip", MySqlDbType.VarChar, 10).Value = Zip

            mdscmd.Parameters.Add("@Contact", MySqlDbType.VarChar, 50).Value = StrConv(Contact, vbProperCase)

            mdscmd.Parameters.Add("@Phone", MySqlDbType.VarChar, 15).Value = Phone

            mdscmd.Parameters.Add("@Fax", MySqlDbType.VarChar, 15).Value = Fax

            mdscmd.Parameters.Add("@Notes", MySqlDbType.VarChar, 400).Value = StrConv(Notes, vbProperCase)

            mdscmd.Parameters.Add("@Id", MySqlDbType.Int16).Value = vId

            Try
                mdscmd.ExecuteNonQuery()

            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "SetNewVendors.UpdateVendors", ex.StackTrace.ToString)
                End Using
                Return False
            End Try


        End Using

        Return True


    End Function

End Class
