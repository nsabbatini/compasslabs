﻿Imports MySql.Data.MySqlClient
Imports Compass.Global

Public Class OpenMySqlDatabase
    Implements IDisposable

    Private _gstrSqldbConn As String
    Public GoSqlSession As MySqlConnection
    Public SqlFile As String

    Public Sub New()
        MyBase.New()
        StartSql()
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                'mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub

    Public Function StartSql() As Boolean

        Dim connstr = MySqlConn

        If (GoSqlSession IsNot Nothing) Then
            If GoSqlSession.State = ConnectionState.Open Then Return True
        End If

        _gstrSqldbConn = connstr

        GoSqlSession = New MySqlConnection(_gstrSqldbConn)

        Try
            GoSqlSession.Open()
            GoSqlSession.Close()
            GoSqlSession.Open()
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function
    Public ReadOnly Property MyConnection As MySqlConnection
        Get
            Return GoSqlSession
        End Get
    End Property
End Class
