﻿Imports MySql.Data.MySqlClient

Public Class InventoryControl
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Public _sqlText As String
    Public _vendorId As String
    Public _orderNumber As Int16
    Public _itemId As Int16
    Public _qty As Int16
    Private x As Int16 = 0
    Public _departmentId As Int16
    Public _reasonId As Int16
    Public _userName As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Not mdsReader Is Nothing Then
                    mdsReader.Close()

                End If
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub

    Public Function ReduceInventory() As Int16

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("Id", MySqlDbType.Int16).Value = _itemId
            mdscmd.Parameters.Add("Qty", MySqlDbType.Int16).Value = _qty

            Try
                mdscmd.ExecuteNonQuery()
                x += 1
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "InventoryControl.ReduceInventory", ex.StackTrace.ToString)
                End Using
            End Try

        End Using

        Return x

    End Function
    Public Function TrackingInformation() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@UserName", MySqlDbType.VarChar, 100).Value = _userName
            mdscmd.Parameters.Add("@Reasonid", MySqlDbType.Int16).Value = _reasonId
            mdscmd.Parameters.Add("@DeptId", MySqlDbType.Int16).Value = _departmentId

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "InventoryControl.ReduceInventory", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True


    End Function
    Public Function TrackingInformationDetails(ByVal ar As ArrayList) As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@Id", MySqlDbType.Int16).Value = ar(0)
            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int16).Value = ar(1)
            mdscmd.Parameters.Add("@TrackId", MySqlDbType.Int64).Value = ar(2)

            Try
                mdscmd.ExecuteNonQuery()
                x += 1
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "InventoryControl.ReduceInventory", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function DeleteVendor() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@Id", MySqlDbType.Int16).Value = _vendorId

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "InventoryControl.DeleteVendor", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function DeleteInventoryItems() As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@Id", MySqlDbType.Int16).Value = _itemId

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "InventoryControl.DeleteInventoryItems", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function SaveInventoryItems(ByVal ar As ArrayList) As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = .CommandType.Text}
            mdscmd.Parameters.Add("@ItemId", MySqlDbType.VarChar, 20).Value = ar(1).ToString.Substring(0, 3).ToUpper
            mdscmd.Parameters.Add("@VendorId", MySqlDbType.Int16).Value = ar(0)
            mdscmd.Parameters.Add("@ItemDesc", MySqlDbType.VarChar, 400).Value = ar(1)
            mdscmd.Parameters.Add("@Units", MySqlDbType.VarChar, 20).Value = ar(2)
            mdscmd.Parameters.Add("@ItemPerUnit", MySqlDbType.Int16).Value = ar(3)
            mdscmd.Parameters.Add("@ItemCost", MySqlDbType.Double).Value = ar(4)
            mdscmd.Parameters.Add("@StockNumber", MySqlDbType.VarChar, 100).Value = ar(5)
            mdscmd.Parameters.Add("@MinQty", MySqlDbType.Int16).Value = ar(6)
            mdscmd.Parameters.Add("@CurrentQty", MySqlDbType.Int16).Value = ar(7)
            mdscmd.Parameters.Add("@DaysReceive", MySqlDbType.Int16).Value = ar(8)
            mdscmd.Parameters.Add("@Abbr", MySqlDbType.VarChar, 5).Value = ar(1).ToString.Substring(0, 3).ToUpper
            mdscmd.Parameters.Add("@DeptId", MySqlDbType.Int16).Value = _departmentId

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "InventoryControl.SaveInventoryItems", ex.StackTrace.ToString)
                End Using
                Return False
            End Try


        End Using

        Return True

    End Function
End Class
