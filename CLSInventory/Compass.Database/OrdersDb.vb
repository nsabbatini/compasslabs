﻿Imports MySql.Data.MySqlClient

Public Class OrdersDb
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Public _sqlText As String
    Public _vendorId As String
    Public _orderNumber As Int16
    Public _itemId As Int16
    Public _qty As Int16

    Private x As Int16 = 0
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Not mdsReader Is Nothing Then
                    mdsReader.Close()
                End If

            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function GetOrders() As IEnumerable(Of GetOrdersDb)

        Dim orders As List(Of GetOrdersDb) = New List(Of GetOrdersDb)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text}

            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim orderModel = New GetOrdersDb With {
                    .OrderId = mdsReader.GetValue(0),
                    .OrderNumber = mdsReader.GetValue(1),
                    .Acct = mdsReader.GetValue(2)?.ToString(),
                    .Aname = mdsReader.GetValue(3)?.ToString().Replace("Covid-19", "").Replace("(Covid-19)", "").Replace("Covid", ""),
                    .Contact = mdsReader.GetValue(4)?.ToString(),
                    .Address1 = mdsReader.GetValue(5)?.ToString(),
                    .Address2 = mdsReader.GetValue(6)?.ToString(),
                    .City = mdsReader.GetValue(7)?.ToString(),
                    .State = mdsReader.GetValue(8)?.ToString(),
                    .Zip = mdsReader.GetValue(9)?.ToString(),
                    .Phone = mdsReader.GetValue(10)?.ToString(),
                    .Fax = mdsReader.GetValue(11)?.ToString(),
                    .Courier = mdsReader.GetValue(12)?.ToString(),
                    .TrackingNum = mdsReader.GetValue(13)?.ToString(),
                    .Weight = mdsReader.GetValue(14)?.ToString(),
                    .NumOfPkg = mdsReader.GetValue(15)?.ToString(),
                    .UID = mdsReader.GetValue(16)?.ToString(),
                    .OrderDate = mdsReader.GetValue(17)?.ToString(),
                    .ApproveDate = mdsReader.GetValue(18)?.ToString(),
                    .FulFilledDate = mdsReader.GetValue(19)?.ToString(),
                    .ShippedDate = mdsReader.GetValue(20)?.ToString(),
                    .ReceivedDate = mdsReader.GetValue(21)?.ToString(),
                    .OrderStatus = mdsReader.GetValue(22)?.ToString()
                }

                orders.Add(orderModel)
            End While

            Return orders.ToList()

        End Using



    End Function
    Public Function GetOrdersItems() As IEnumerable(Of GetOrderItemsDb)

        Dim orderItems As List(Of GetOrderItemsDb) = New List(Of GetOrderItemsDb)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text}

            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim orderItemsModel = New GetOrderItemsDb With {
                    .Id = mdsReader.GetValue(0),
                    .Orderid = mdsReader.GetValue(1),
                    .ItemID = mdsReader.GetValue(2),
                    .Qty = mdsReader.GetValue(3)
                }

                orderItems.Add(orderItemsModel)
            End While

            Return orderItems.ToList()

        End Using



    End Function
    Public Function GetPrintOrdersItems() As IEnumerable(Of GetOrderItemsForPrint)

        Dim orderPrintItems As List(Of GetOrderItemsForPrint) = New List(Of GetOrderItemsForPrint)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text}

            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim orderprintModel = New GetOrderItemsForPrint With {
                    .OrderNumber = mdsReader.GetValue(0),
                    .OrderId = mdsReader.GetValue(1),
                    .ClientName = mdsReader.GetValue(2)
                }


                orderPrintItems.Add(orderprintModel)
            End While


            Return orderPrintItems.ToList()

        End Using



    End Function
    Public Function GetPopOrderItems() As IEnumerable(Of GetOrderItemsForPopUp)

        Dim orderItems As List(Of GetOrderItemsForPopUp) = New List(Of GetOrderItemsForPopUp)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text}

            'mdscmd.Parameters.AddWithValue("OrderNum", _orderNumber)

            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim orderItemsModel = New GetOrderItemsForPopUp With {
                    .OrderId = mdsReader.GetValue(0),
                    .ItemDesc = mdsReader.GetValue(1),
                    .QtyOrdered = mdsReader.GetValue(2),
                    .CurrentQty = mdsReader.GetValue(3),
                    .Units = mdsReader.GetValue(4)
                }


                orderItems.Add(orderItemsModel)
            End While


            Return orderItems.ToList()

        End Using


    End Function
    Public Function SaveOrders(ByVal ar As ArrayList) As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}
            mdscmd.Parameters.Add("@Customer", MySqlDbType.VarChar, 100).Value = ar(0)
            mdscmd.Parameters.Add("@Name", MySqlDbType.VarChar, 100).Value = ar(1)
            mdscmd.Parameters.Add("@Contact", MySqlDbType.VarChar, 100).Value = ar(2)
            mdscmd.Parameters.Add("@Address1", MySqlDbType.VarChar, 100).Value = ar(3)
            mdscmd.Parameters.Add("@Address2", MySqlDbType.VarChar, 100).Value = ar(4)
            mdscmd.Parameters.Add("@City", MySqlDbType.VarChar, 100).Value = ar(5)
            mdscmd.Parameters.Add("@State", MySqlDbType.VarChar, 2).Value = ar(6)
            mdscmd.Parameters.Add("@Zip", MySqlDbType.VarChar, 10).Value = ar(7)
            mdscmd.Parameters.Add("@Phone", MySqlDbType.VarChar, 20).Value = ar(8)
            mdscmd.Parameters.Add("@Fax", MySqlDbType.VarChar, 20).Value = ar(9)
            mdscmd.Parameters.Add("@Courier", MySqlDbType.VarChar, 20).Value = ar(10)
            mdscmd.Parameters.Add("@UID", MySqlDbType.VarChar, 100).Value = ar(11)
            mdscmd.Parameters.Add("@OrderStatus", MySqlDbType.Int16).Value = ar(12)
            mdscmd.Parameters.Add("@OrderNumber", MySqlDbType.Int16).Value = ar(13)

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "OrdersDb.SaveOrders", ex.StackTrace.ToString)
                End Using
                Return False
            End Try
        End Using

        Return True

    End Function
    Public Function SaveOrderedItems(ByVal ar As ArrayList) As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@OrderId", MySqlDbType.Int16).Value = ar(0)
            mdscmd.Parameters.Add("@ItemID", MySqlDbType.Int16).Value = ar(1)
            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int16).Value = ar(2)


            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "OrdersDb.SaveOrderedItems", ex.StackTrace.ToString)
                End Using
                Return False
            End Try
        End Using

        Return True
    End Function
    Public Function ApproveOrder(ByVal OrderId As Int16) As Boolean

        Using mdscmd As New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@OrderId", MySqlDbType.Int16).Value = OrderId

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "OrdersDb.ApproveOrder", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
End Class
