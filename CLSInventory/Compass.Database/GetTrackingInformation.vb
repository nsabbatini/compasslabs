﻿Imports MySql.Data.MySqlClient
Public Class GetTrackingInformation
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Public _sqlText As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Not mdsReader Is Nothing Then
                    mdsReader.Close()

                End If
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function GetTracking() As IEnumerable(Of GetItemTracking)

        Dim invItems As List(Of GetItemTracking) = New List(Of GetItemTracking)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text
        }
            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim inventoryModel = New GetItemTracking With {
                    .TrackingId = mdsReader.GetValue(0),
                    .UserName = mdsReader.GetValue(1),
                    .DateRemoved = mdsReader.GetValue(2),
                    .ReasonId = mdsReader.GetValue(3),
                    .DepartmentId = mdsReader.GetValue(4)
                }

                invItems.Add(inventoryModel)
            End While

            Return invItems.ToList()

        End Using
    End Function
    Public Function GetTrackingDetails() As IEnumerable(Of GetItemTrackingDetails)

        Dim invItems As List(Of GetItemTrackingDetails) = New List(Of GetItemTrackingDetails)()

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text
        }
            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()
                Dim inventoryModel = New GetItemTrackingDetails With {
                    .DetailId = mdsReader.GetValue(0),
                    .TrackingId = mdsReader.GetValue(1),
                    .ItemId = mdsReader.GetValue(2),
                    .Qty = mdsReader.GetValue(3)
                }

                invItems.Add(inventoryModel)
            End While

            Return invItems.ToList()

        End Using
    End Function
End Class
