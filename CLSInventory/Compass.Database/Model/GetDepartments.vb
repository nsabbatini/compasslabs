﻿Partial Public Class GetDepartments
    Private _departmentId As Int16
    Private _departmentName As String
    Public Property DepartmentId As Int16
        Get
            Return _departmentId
        End Get
        Set(value As Int16)
            _departmentId = value
        End Set
    End Property
    Public Property DepartmentName As String
        Get
            Return _departmentName
        End Get
        Set(value As String)
            _departmentName = value
        End Set
    End Property

End Class
