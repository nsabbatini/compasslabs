﻿Partial Public Class ReasonCodes
    Private _reasonId As Int16
    Private _reasonName As String
    Public Property ReasonId() As Int16
        Get
            Return _reasonId
        End Get
        Set(value As Int16)
            _reasonId = value
        End Set
    End Property
    Public Property ReasonName() As String
        Get
            Return _reasonName
        End Get
        Set(ByVal value As String)
            _reasonName = value
        End Set
    End Property

End Class
