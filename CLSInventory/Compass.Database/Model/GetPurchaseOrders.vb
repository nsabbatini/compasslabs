﻿Partial Public Class GetPurchaseOrders

    Public Property PId As Int16
    Public Property PoNumber As String = String.Empty
    Public Property VId As Int16
    Public Property PoDate As String = String.Empty
    Public Property PoTerms As String = String.Empty
    Public Property Notes As String = String.Empty
    Public Property Department As Int16
    Public Property Status As Int16
    Public Property Approved As Boolean
    Public Property TotalPo As Double
    Public Property ShipTo As Int16
    Public Property SalesTax As Double
    Public Property SalesPerson As String = String.Empty
    Public Property QuoteNumber As String = String.Empty
    Public Property PaymentTerms As String = String.Empty
    Public Property NonInventory As Int16

End Class
Partial Public Class GetPurchaseOrderDetails

    Public Property DId As Int16
    Public Property PId As Int16
    Public Property ItemId As Int16
    Public Property OrderQty As Int16
    Public Property ItemCost As Double
    Public Property UnitCost As Double
    Public Property UnitPrince As Double
    Public Property ReceivedQty As Int16
    Public Property BackOrdered As Boolean
    Public Property Received As Boolean
    Public Property NonInventory As String

End Class
Partial Public Class ReconcilePurchaseOrder

    Public Property DId As Int16
    Public Property PId As Int16
    Public Property OrderQty As Int16
    Public Property UnitPrice As Double
    Public Property ItemCost As Double
    Public Property ItemDesc As String
    Public Property ReceivedQty As Int16
    Public Property BackOrdered As Boolean
    Public Property Received As Boolean
    Public Property ItemId As Int16
    Public Property PONumber As String
    Public Property VendorId As Int16
    Public Property ExpirationDate As Int16
    Public Property Units As String

End Class
