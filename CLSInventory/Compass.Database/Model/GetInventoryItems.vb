﻿Partial Public Class GetInventoryItems
    Public Property Id As Int16
    Public Property VId As Int16
    Public Property DeptId As Int16
    ' Public Property itemid As String
    Public Property ItemDesc As String
    'Public Property ItemAbbr As String
    Public Property ItemUnit As String
    Public Property ItemPerUnit As Int16
    'Public Property VendorId As String
    Public Property Vendoritemid As String
    'Public Property ItemVendorUnit As String
    'Public Property ItemVendorPerUnit As Int64
    Public Property ItemVendorCost As Double
    Public Property MinQty As Int16
    Public Property CurrentQty As Int64
    Public Property Location As String
    'Public Property TimeToReceive As String
    ' Public Property ShippingInfo As String
    'Public Property Dept As String
    Public Property NeedsLotNum As Boolean
    Public Property Discontinued As Boolean


End Class
Partial Public Class GetInventoryItemsForUpdate

    Public Property itemid As String
    Public Property ItemDesc As String
    Public Property ItemUnit As String
    Public Property ItemVendorCost As Double
    Public Property MinQty As Int16
    Public Property CurrentQty As Int16
    Public Property Id As Int16
    Public Property VendorName As String

End Class
Partial Public Class GetVendorInfo

    Public Property Id As Int16
    'Public Property VendorId As String
    Public Property VendorName As String
    ' Public Property VendorAbbr As String
    Public Property Address As String
    Public Property Address2 As String
    Public Property City As String
    Public Property State As String
    Public Property Zip As String
    Public Property Contact As String
    Public Property Phone As String
    Public Property Fax As String
    Public Property Notes As String




End Class
