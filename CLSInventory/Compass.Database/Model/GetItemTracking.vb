﻿Partial Public Class GetItemTracking

    Private _Id As Int16
    Public Property TrackingId() As Int16
        Get
            Return _Id
        End Get
        Set(ByVal value As Int16)
            _Id = value
        End Set
    End Property
    Private _UserName As String
    Public Property UserName() As String
        Get
            Return _UserName
        End Get
        Set(ByVal value As String)
            _UserName = value
        End Set
    End Property
    Private _dateRemoved As String
    Public Property DateRemoved() As String
        Get
            Return _dateRemoved
        End Get
        Set(ByVal value As String)
            _dateRemoved = value
        End Set
    End Property
    Private _reasonId As Int16
    Public Property ReasonId() As Int16
        Get
            Return _reasonId
        End Get
        Set(ByVal value As Int16)
            _reasonId = value
        End Set
    End Property
    Private _departmentId As Int16
    Public Property DepartmentId() As Int16
        Get
            Return _departmentId
        End Get
        Set(ByVal value As Int16)
            _departmentId = value
        End Set
    End Property
End Class
Partial Public Class GetItemTrackingDetails

    Private _Id As Int16
    Public Property DetailId() As Int16
        Get
            Return _Id
        End Get
        Set(ByVal value As Int16)
            _Id = value
        End Set
    End Property
    Private _trackingId As Int16
    Public Property TrackingId() As Int16
        Get
            Return _trackingId
        End Get
        Set(ByVal value As Int16)
            _trackingId = value
        End Set
    End Property
    Private _itemId As Int16
    Public Property ItemId() As Int16
        Get
            Return _itemId
        End Get
        Set(ByVal value As Int16)
            _itemId = value
        End Set
    End Property
    Private _qty As Int16
    Public Property Qty() As Int16
        Get
            Return _qty
        End Get
        Set(ByVal value As Int16)
            _qty = value
        End Set
    End Property
End Class
