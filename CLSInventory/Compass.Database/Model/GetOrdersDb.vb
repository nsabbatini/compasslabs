﻿Partial Public Class GetOrdersDb

    Public Property OrderId As Int16
    Public Property OrderNumber As Int16
    Public Property Acct As String
    Public Property Aname As String
    Public Property Contact As String
    Public Property Address1 As String
    Public Property Address2 As String
    Public Property City As String
    Public Property State As String
    Public Property Zip As String
    Public Property Phone As String
    Public Property Fax As String
    Public Property Courier As String
    Public Property TrackingNum As String
    Public Property Weight As String
    Public Property NumOfPkg As String
    Public Property UID As String
    Public Property OrderDate As String
    Public Property ApproveDate As String
    Public Property FulFilledDate As String
    Public Property ShippedDate As String
    Public Property ReceivedDate As String
    Public Property OrderStatus As Int16



End Class

Partial Public Class GetOrderItemsDb

    Public Property Id As Int16
    Public Property Orderid As Int16
    Public Property ItemID As Int16
    Public Property Qty As Int16


End Class
Partial Public Class GetOrderItemsForPrint
    Public Property OrderNumber As Int16
    Public Property OrderId As Int16
    Public Property ClientName As String

End Class
Partial Public Class GetOrderItemsForPopUp

    Public Property OrderId As Int16
    Public Property ItemDesc As String
    Public Property QtyOrdered As Int16
    Public Property CurrentQty As Int16
    Public Property Units As String


End Class
