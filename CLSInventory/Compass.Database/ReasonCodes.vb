﻿Imports MySql.Data.MySqlClient
Public Class ReasonCodes
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Private mdsSet As DataSet
    Public _sqlText As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Not mdsReader Is Nothing Then
                    mdsReader.Close()
                End If
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function GetReasonCodesInfo() As IEnumerable(Of ReasonCodes)

        Dim reasonDetails As List(Of ReasonCodes) = New List(Of ReasonCodes)

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}
            mdsReader = mdscmd.ExecuteReader

            While mdsReader.Read()
                Dim reasonModel = New ReasonCodes With {
                    .ReasonId = mdsReader.GetValue(0),
                    .ReasonName = mdsReader.GetValue(1)
                }

                reasonDetails.Add(reasonModel)

            End While

            Return reasonDetails

        End Using
    End Function
End Class
