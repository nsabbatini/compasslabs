﻿Imports MySql.Data.MySqlClient

Public Class ReceiveInventory
    Implements IDisposable
    Private MyConnection As MySqlConnection
    Private mdsReader As MySqlDataReader
    Public _sqlText As String
    Public _vendorId As String

    Public Property Quantity As Int64
    Public Property ItemId As String
    Public Property InvoiceNumber As String
    Public Property DateReceived As String
    Public Property ReceivedByUser As String
    Public Property ItemDescription As String
    Public Property Vendor As String
    Public Property MinQty As Int16
    Public Property VendorsCost As Double
    Public Property ItemUnit As String

    Private xcnt As Int16

    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenMySqlDatabase = New OpenMySqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                'mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function UpdateInventoryReceived() As Integer

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
            .CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int16).Value = Quantity

            mdscmd.Parameters.Add("@ItemId", MySqlDbType.VarChar, 20).Value = ItemId

            mdscmd.Parameters.Add("@ItemDesc", MySqlDbType.VarChar, 100).Value = ItemDescription


            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "ReceiveInventory.UpdateInventoryReceived", ex.StackTrace.ToString)
                End Using
                Return 0
            End Try
            xcnt += 1
        End Using

        Return xcnt


    End Function
    Public Function ReceiveInventoryItems() As Boolean

        Dim dt As DateTime = DateReceived

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
                .CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@InvoiceNumber", MySqlDbType.VarChar, 30).Value = InvoiceNumber

            mdscmd.Parameters.Add("@DateReceived", MySqlDbType.Date).Value = Convert.ToString(dt.ToString("yyyy-MM-dd"))

            mdscmd.Parameters.Add("@ReceivedBy", MySqlDbType.VarChar, 40).Value = ReceivedByUser

            mdscmd.Parameters.Add("@Vendor", MySqlDbType.VarChar, 100).Value = Vendor

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "ReceiveInventory.UpdateInventoryReceived", ex.StackTrace.ToString)
                End Using
                Return False
            End Try

        End Using

        Return True

    End Function
    Public Function GetReceiveHistoryDb() As DataSet
        Dim mdsSet As New DataSet

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
                .CommandType = CommandType.Text}

            Try
                Using mdsAdapter As New MySqlDataAdapter(mdscmd)
                    mdsAdapter.Fill(mdsSet, "MasterDetail")
                End Using

            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "ReceiveInventory.GetReceiveHistoryDb", ex.StackTrace.ToString)
                End Using
            End Try
        End Using

        Return mdsSet

    End Function
    Public Function GetItemsDb() As DataSet
        Dim mdsSet As New DataSet

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
                .CommandType = CommandType.Text}

            Try
                Using mdsAdapter As New MySqlDataAdapter(mdscmd)
                    mdsAdapter.Fill(mdsSet, "Items")
                End Using

            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "ReceiveInventory.GetItemsDb", ex.StackTrace.ToString)
                End Using
            End Try
        End Using

        Return mdsSet

    End Function
    Public Function SaveCurrentQtyDb(ByVal ar As ArrayList) As Integer

        Using mdscmd = New MySqlCommand(_sqlText, MyConnection) With {
                    .CommandType = CommandType.Text}

            mdscmd.Parameters.Add("@Id", MySqlDbType.Int16).Value = ar(0)
            mdscmd.Parameters.Add("@Qty", MySqlDbType.Int64).Value = ar(1)
            mdscmd.Parameters.Add("@MinQty", MySqlDbType.Int16).Value = ar(2)
            mdscmd.Parameters.Add("@VendorCost", MySqlDbType.Decimal).Value = ar(3)
            mdscmd.Parameters.Add("@ItemUnit", MySqlDbType.VarChar, 20).Value = ar(4)

            Try
                mdscmd.ExecuteNonQuery()
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "ReceiveInventory.SaveCurrentQtyDb", ex.StackTrace.ToString)
                End Using
                Return 0
            End Try
            xcnt += 1
        End Using

        Return xcnt

    End Function

End Class
