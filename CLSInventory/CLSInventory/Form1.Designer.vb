﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ReceiveInventory = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ReceiveInventory
        '
        Me.ReceiveInventory.Location = New System.Drawing.Point(23, 44)
        Me.ReceiveInventory.Name = "ReceiveInventory"
        Me.ReceiveInventory.Size = New System.Drawing.Size(122, 65)
        Me.ReceiveInventory.TabIndex = 0
        Me.ReceiveInventory.Text = "Receive Inventory"
        Me.ReceiveInventory.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 546)
        Me.Controls.Add(Me.ReceiveInventory)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compass Lab Services"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReceiveInventory As Button
End Class
