﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReceiveInventory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReceiveInventory))
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btnExit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.grpItems = New DevExpress.XtraEditors.GroupControl()
        Me.grdInvItems = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.dcbItems = New DevExpress.XtraEditors.LookUpEdit()
        Me.btnAddItems = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtQuan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.GaugeControl1 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.gOnHand = New DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge()
        Me.DigitalBackgroundLayerComponent1 = New DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.dtReceived = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtInvoiceNumber = New DevExpress.XtraEditors.TextEdit()
        Me.dcbVendorId2 = New DevExpress.XtraEditors.LookUpEdit()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.grpItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpItems.SuspendLayout()
        CType(Me.grdInvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dcbItems.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQuan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gOnHand, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DigitalBackgroundLayerComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtReceived.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtReceived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dcbVendorId2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.GroupControl1)
        Me.PanelControl1.Controls.Add(Me.grpItems)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.dtReceived)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.txtInvoiceNumber)
        Me.PanelControl1.Controls.Add(Me.dcbVendorId2)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(653, 614)
        Me.PanelControl1.TabIndex = 0
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnExit)
        Me.GroupControl1.Controls.Add(Me.btnSave)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupControl1.Location = New System.Drawing.Point(2, 469)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(649, 143)
        Me.GroupControl1.TabIndex = 16
        Me.GroupControl1.Text = "Actions"
        '
        'btnExit
        '
        Me.btnExit.ImageOptions.Image = CType(resources.GetObject("btnExit.ImageOptions.Image"), System.Drawing.Image)
        Me.btnExit.Location = New System.Drawing.Point(335, 63)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(131, 32)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit"
        '
        'btnSave
        '
        Me.btnSave.ImageOptions.Image = CType(resources.GetObject("btnSave.ImageOptions.Image"), System.Drawing.Image)
        Me.btnSave.Location = New System.Drawing.Point(169, 63)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(131, 32)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save"
        '
        'grpItems
        '
        Me.grpItems.Controls.Add(Me.grdInvItems)
        Me.grpItems.Controls.Add(Me.dcbItems)
        Me.grpItems.Controls.Add(Me.btnAddItems)
        Me.grpItems.Controls.Add(Me.LabelControl6)
        Me.grpItems.Controls.Add(Me.txtQuan)
        Me.grpItems.Controls.Add(Me.LabelControl4)
        Me.grpItems.Controls.Add(Me.GaugeControl1)
        Me.grpItems.Controls.Add(Me.LabelControl5)
        Me.grpItems.Enabled = False
        Me.grpItems.Location = New System.Drawing.Point(0, 168)
        Me.grpItems.Name = "grpItems"
        Me.grpItems.Size = New System.Drawing.Size(653, 300)
        Me.grpItems.TabIndex = 15
        Me.grpItems.Text = "Items Received"
        '
        'grdInvItems
        '
        Me.grdInvItems.Location = New System.Drawing.Point(94, 124)
        Me.grdInvItems.MainView = Me.GridView1
        Me.grdInvItems.Name = "grdInvItems"
        Me.grdInvItems.Size = New System.Drawing.Size(538, 171)
        Me.grdInvItems.TabIndex = 22
        Me.grdInvItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.GridView1.GridControl = Me.grdInvItems
        Me.GridView1.Name = "GridView1"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Item Description"
        Me.GridColumn1.FieldName = "ItemDesc"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 173
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Item Id"
        Me.GridColumn2.FieldName = "ItemId"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 173
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Qty"
        Me.GridColumn3.FieldName = "CurrentQty"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 50
        '
        'dcbItems
        '
        Me.dcbItems.Location = New System.Drawing.Point(171, 37)
        Me.dcbItems.Name = "dcbItems"
        Me.dcbItems.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.dcbItems.Properties.Appearance.Options.UseFont = True
        Me.dcbItems.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dcbItems.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ItemDesc", "Vendor Desc"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("itemid", "Item Id"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("CurrentQty", "CurrentQty", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[Default])})
        Me.dcbItems.Properties.DropDownRows = 20
        Me.dcbItems.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.dcbItems.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        Me.dcbItems.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.dcbItems.Size = New System.Drawing.Size(311, 24)
        Me.dcbItems.TabIndex = 21
        '
        'btnAddItems
        '
        Me.btnAddItems.ImageOptions.Image = CType(resources.GetObject("btnAddItems.ImageOptions.Image"), System.Drawing.Image)
        Me.btnAddItems.Location = New System.Drawing.Point(310, 74)
        Me.btnAddItems.Name = "btnAddItems"
        Me.btnAddItems.Size = New System.Drawing.Size(115, 33)
        Me.btnAddItems.TabIndex = 19
        Me.btnAddItems.Text = "Add Item"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(94, 82)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(60, 18)
        Me.LabelControl6.TabIndex = 18
        Me.LabelControl6.Text = "Quantity:"
        '
        'txtQuan
        '
        Me.txtQuan.Location = New System.Drawing.Point(171, 79)
        Me.txtQuan.Name = "txtQuan"
        Me.txtQuan.Size = New System.Drawing.Size(100, 24)
        Me.txtQuan.TabIndex = 17
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(512, 40)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(111, 18)
        Me.LabelControl4.TabIndex = 16
        Me.LabelControl4.Text = "Current On-Hand"
        '
        'GaugeControl1
        '
        Me.GaugeControl1.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.gOnHand})
        Me.GaugeControl1.Location = New System.Drawing.Point(496, 60)
        Me.GaugeControl1.Name = "GaugeControl1"
        Me.GaugeControl1.Size = New System.Drawing.Size(139, 58)
        Me.GaugeControl1.TabIndex = 15
        '
        'gOnHand
        '
        Me.gOnHand.AppearanceOff.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#0D8097")
        Me.gOnHand.AppearanceOn.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#02F0F7")
        Me.gOnHand.BackgroundLayers.AddRange(New DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent() {Me.DigitalBackgroundLayerComponent1})
        Me.gOnHand.Bounds = New System.Drawing.Rectangle(6, 6, 127, 46)
        Me.gOnHand.DigitCount = 3
        Me.gOnHand.Name = "gOnHand"
        Me.gOnHand.Padding = New DevExpress.XtraGauges.Core.Base.TextSpacing(26, 20, 26, 20)
        Me.gOnHand.Text = "00.000"
        '
        'DigitalBackgroundLayerComponent1
        '
        Me.DigitalBackgroundLayerComponent1.BottomRight = New DevExpress.XtraGauges.Core.Base.PointF2D(169.8875!, 99.9625!)
        Me.DigitalBackgroundLayerComponent1.Name = "digitalBackgroundLayerComponent1"
        Me.DigitalBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style17
        Me.DigitalBackgroundLayerComponent1.TopLeft = New DevExpress.XtraGauges.Core.Base.PointF2D(26.0!, 0!)
        Me.DigitalBackgroundLayerComponent1.ZOrder = 1000
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(98, 40)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(56, 18)
        Me.LabelControl5.TabIndex = 9
        Me.LabelControl5.Text = "Item Id:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(55, 114)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(99, 18)
        Me.LabelControl3.TabIndex = 12
        Me.LabelControl3.Text = "Date Received:"
        '
        'dtReceived
        '
        Me.dtReceived.EditValue = Nothing
        Me.dtReceived.Location = New System.Drawing.Point(171, 111)
        Me.dtReceived.Name = "dtReceived"
        Me.dtReceived.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtReceived.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtReceived.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista
        Me.dtReceived.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[True]
        Me.dtReceived.Size = New System.Drawing.Size(159, 24)
        Me.dtReceived.TabIndex = 11
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(45, 69)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(109, 18)
        Me.LabelControl2.TabIndex = 10
        Me.LabelControl2.Text = "Invoice Number:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(88, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 18)
        Me.LabelControl1.TabIndex = 9
        Me.LabelControl1.Text = "Vendor id:"
        '
        'txtInvoiceNumber
        '
        Me.txtInvoiceNumber.Location = New System.Drawing.Point(171, 66)
        Me.txtInvoiceNumber.Name = "txtInvoiceNumber"
        Me.txtInvoiceNumber.Size = New System.Drawing.Size(311, 24)
        Me.txtInvoiceNumber.TabIndex = 8
        '
        'dcbVendorId2
        '
        Me.dcbVendorId2.Location = New System.Drawing.Point(171, 23)
        Me.dcbVendorId2.Name = "dcbVendorId2"
        Me.dcbVendorId2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.dcbVendorId2.Properties.Appearance.Options.UseFont = True
        Me.dcbVendorId2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dcbVendorId2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("VendorName", "Vendor"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("VendorId", "Vendor Id")})
        Me.dcbVendorId2.Properties.DropDownRows = 20
        Me.dcbVendorId2.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.dcbVendorId2.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete
        Me.dcbVendorId2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.dcbVendorId2.Size = New System.Drawing.Size(311, 24)
        Me.dcbVendorId2.TabIndex = 6
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "CLIMS"
        Me.NotifyIcon1.Visible = True
        '
        'frmReceiveInventory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 614)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "frmReceiveInventory"
        Me.Opacity = 0.88R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receive Inventory"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.grpItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpItems.ResumeLayout(False)
        Me.grpItems.PerformLayout()
        CType(Me.grdInvItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dcbItems.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQuan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gOnHand, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DigitalBackgroundLayerComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtReceived.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtReceived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dcbVendorId2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents dcbVendorId2 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtInvoiceNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtReceived As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grpItems As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GaugeControl1 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents gOnHand As DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge
    Private WithEvents DigitalBackgroundLayerComponent1 As DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnAddItems As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtQuan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnExit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dcbItems As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents grdInvItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents NotifyIcon1 As NotifyIcon
End Class
