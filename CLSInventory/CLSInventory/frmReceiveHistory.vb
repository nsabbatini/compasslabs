﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Sql.Database
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Public Class frmReceiveHistory
    Private _dirPathToFIle As String = Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private Sub frmReceiveHistory_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        GetDataSetForGrid()

    End Sub
    Private Sub GetDataSetForGrid()

        _fileName = _fileDir & GetReceiveHistorySql
        Dim dsMaster As New DataSet

        Using getHistory As New ReceiveInventory() With {._sqlText = ReadSqlFile(_fileName)}

            dsMaster = getHistory.GetReceiveHistoryDb

            Dim keyColumn As DataColumn = dsMaster.Tables(0).Columns("id")
            Dim foreignKeyColumn As DataColumn = dsMaster.Tables(1).Columns("receive_id")
            dsMaster.Relations.Add("ReceiveItems", keyColumn, foreignKeyColumn)

            grdHistory.DataSource = dsMaster.Tables(0)
            grdHistory.ForceInitialize()

        End Using

    End Sub

    Private Sub grdHistory_ViewRegistered(sender As Object, e As DevExpress.XtraGrid.ViewOperationEventArgs) Handles grdHistory.ViewRegistered
        Dim view As GridView = CType(e.View, GridView)

        For Each col As GridColumn In view.Columns
            If col.FieldName = "item_received" Then
                col.Caption = "Item Received"
            ElseIf col.FieldName = "qty_received" Then
                col.Caption = "Quantity Received"
                col.Width = 75
            ElseIf col.FieldName = "OnHand" Then
                col.Caption = "Currently On-Hand"
            ElseIf col.FieldName = "detail_id" Then
                col.Visible = False
            ElseIf col.FieldName = "receive_id" Then
                col.Visible = False
            End If
        Next
    End Sub
End Class