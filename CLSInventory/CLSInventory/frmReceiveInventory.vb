﻿Imports Compass.MySql.Database
Imports Compass.Global
Imports Compass.Sql.Database
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Repository

Public Class frmReceiveInventory
    Private _dirPathToFIle As String = Application.StartupPath.Replace("bin\Debug", "").Replace("bin\Release", "")
    Private _fileDir = _dirPathToFIle & "SqlFiles\"
    Private _fileName As String = String.Empty
    Private _dtInventory As DataTable = New DataTable
    Private Sub FrmReceiveInventory_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LoadVendorCombo()
        ValidateTheUser()
        _dtInventory = CreateData()


    End Sub
    Private Function ValidateTheUser() As Boolean

        Using validate As New AuthHelper() With {._userName = "L.MCLAUGHLIN", ._password = "Compass2020"}
            If validate.CheckUserCreds Then
                LoggedinUserName = "L.MCLAUGHLIN"
            End If

        End Using

    End Function
    Private Sub LoadVendorCombo()

        Try
            Dim vendors = GetVendorDb()

            dcbVendorId2.Properties.DataSource = vendors
            dcbVendorId2.Properties.DisplayMember = "VendorName"
            dcbVendorId2.Properties.ValueMember = "VendorId"

            dtReceived.EditValue = DateTime.Now.ToString("MM/dd/yyyy")


        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.LoadVendorCombo", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "The error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Sub
    Private Function GetVendorDb() As Object

        Try

            Using getVendors As New GetInventoryDb()
                _fileName = _fileDir & VendorSqlFile
                getVendors._sqlText = ReadSqlFile(_fileName)
                Return getVendors.GetVendorInfo
            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.GetVendorDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "The error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function

    Private Function GetInventoryDb() As Object

        Try

            Using getInventory As New GetInventoryDb()

                _fileName = _fileDir & InventorySqlItems
                getInventory._sqlText = ReadSqlFile(_fileName)
                Return getInventory.GetInventory.Where(Function(x) x.VendorId = dcbVendorId2.EditValue).ToList

            End Using

        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.GetInventoryDb", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "The error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try


    End Function
    Private Sub dcbVendorId2_EditValueChanged(sender As Object, e As EventArgs) Handles dcbVendorId2.EditValueChanged

        If _dtInventory.Rows.Count <= 0 Then
            _dtInventory = CreateData()
        End If

        Try
            Dim results = GetInventoryDb()

            dcbItems.Properties.DataSource = results
            dcbItems.Properties.DisplayMember = "ItemDesc"
            dcbItems.Properties.ValueMember = "itemid"

            grpItems.Enabled = True

            txtInvoiceNumber.Focus()


        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.dcbVendorId_SelectionChangeCommitted", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "The error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click

        If GridView1.DataRowCount > 0 Then
            Dim x = MessageBox.Show(Me, "You have unsaved received items, are you sure you want to exit and loose the data?", "Unsaved Items", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If x.Yes Then
                Me.Close()
            End If
        Else
            Me.Close()
        End If

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If dcbVendorId2.SelectedText = String.Empty OrElse dcbVendorId2.SelectedText = "" Then
            MsgBox("Must select a vendor!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If txtInvoiceNumber.Text = String.Empty OrElse txtInvoiceNumber.Text = "" Then
            MsgBox("Must enter an invoice number to continue!", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        InsertReceivedItemsInfo()


        _fileName = _fileDir & ReceiveInventorySql
        Dim x As Int16
        Try
            Using updateInv As New ReceiveInventory() With {._sqlText = ReadSqlFile(_fileName)}
                For i = 0 To GridView1.DataRowCount - 1
                    updateInv.ItemId = CType(GridView1.GetRowCellValue(i, "ItemId"), String)
                    updateInv.Quantity = CType(GridView1.GetRowCellValue(i, "CurrentQty"), Int16)
                    updateInv.ItemDescription = CType(GridView1.GetRowCellValue(i, "ItemDesc"), String)
                    x = updateInv.UpdateInventoryReceived()
                Next

                If x = GridView1.DataRowCount Then
                    MsgBox("Inventory Received Successfully", MsgBoxStyle.Information, "Success")
                    ClearFormAfterSave()
                Else
                    'TODO: something went wrong
                End If
            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.btnSave_Click", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "The error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try



    End Sub
    Private Sub InsertReceivedItemsInfo()

        _fileName = _fileDir & ReceiveItemsSql

        Try
            Using receive As New ReceiveInventory() With {
                 .InvoiceNumber = txtInvoiceNumber.Text.Trim,
                 .DateReceived = dtReceived.Text,
                 ._sqlText = ReadSqlFile(_fileName),
                 .ReceivedByUser = LoggedinUserName,
                 .Vendor = dcbVendorId2.SelectedText
                }

                If Not receive.ReceiveInventoryItems() Then
                    MessageBox.Show(Me, "An error occured and has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
                End If

            End Using
        Catch ex As Exception
            Using recorderror As New ErrorLog()
                recorderror.LogError(ex.Message.ToString, "frmReceiveInventory.btnSave_Click", ex.StackTrace.ToString)
            End Using
            MessageBox.Show(Me, "The error has been logged and the IT team has been notified that an error occured.", "Error Occured", MessageBoxButtons.OK)
        End Try



    End Sub
    Private Sub ClearFormAfterSave()


        dcbItems.Properties.DataSource = Nothing
        txtQuan.Text = String.Empty
        dcbVendorId2.SelectedText = Nothing

        txtInvoiceNumber.Text = String.Empty

        gOnHand.Text = "000"

        _dtInventory.Clear()


    End Sub

    Private Sub dcbItems_EditValueChanged(sender As Object, e As EventArgs) Handles dcbItems.EditValueChanged

        gOnHand.Text = dcbItems.GetColumnValue("CurrentQty")


    End Sub

    Private Sub txtQuan_KeyDown(sender As Object, e As KeyEventArgs) Handles txtQuan.KeyDown
        If e.KeyCode = Keys.Enter Then

            AddItemsToGrid()
            ClearFormAfterAdd()
        End If
    End Sub

    Private Sub ClearFormAfterAdd()

        dcbItems.SelectedText = String.Empty
        dcbItems.Focus()
        txtQuan.Text = String.Empty

    End Sub

    Private Sub AddItemsToGrid()
        Dim arow As DataRow

        With _dtInventory
            arow = .NewRow
            arow(0) = dcbItems.SelectedText
            arow(1) = dcbItems.EditValue
            arow(2) = txtQuan.Text

            .Rows.Add(arow)
        End With

        grdInvItems.DataSource = _dtInventory.DefaultView


    End Sub
    Private Function CreateData() As DataTable

        Dim _dtItems As DataTable = New DataTable
        _dtItems.Columns.Add("ItemDesc", GetType(String))
        _dtItems.Columns.Add("ItemId", GetType(String))
        _dtItems.Columns.Add("CurrentQty", GetType(Int16))

        Return _dtItems

    End Function

    Private Sub btnAddItems_Click(sender As Object, e As EventArgs) Handles btnAddItems.Click


        If dcbItems.SelectedText = String.Empty OrElse dcbItems.SelectedText = "" Then
            MsgBox("Must select an item to receive!", MsgBoxStyle.Critical, "Quantity Field")
            dcbItems.Focus()
            Exit Sub
        End If

        If txtQuan.Text = String.Empty OrElse txtQuan.Text = "" OrElse Not IsNumeric(txtQuan.Text) Then
            MsgBox("Must enter a numeric value in the quantity field!", MsgBoxStyle.Critical, "Quantity Field")
            txtQuan.Text = String.Empty
            txtQuan.Focus()
            Exit Sub
        End If
        AddItemsToGrid()
        ClearFormAfterAdd()

    End Sub


End Class