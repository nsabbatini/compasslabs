﻿SELECT currentqty INTO @cqty from items where itemid = @ItemId;

set @aqty = (@cqty + @Qty);

UPDATE ITEMS 
SET CURRENTQTY = @aqty
WHERE ITEMID = @ItemId;


SELECT MAX(id) into @maxid FROM received_items;

INSERT INTO received_items_details(receive_id, item_received, qty_received) VALUES(@maxid, @ItemDesc, @Qty);

