﻿Imports System.Data.SqlClient
Imports Compass.Global
Public Class AuthorizeUser
    Implements IDisposable
    Private MyConnection As SqlConnection
    Private mdsReader As SqlDataReader
    Public _sqlText As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenSqlDatabase = New OpenSqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                ' mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function LogInUser() As IEnumerable(Of GetUsers)

        Dim users As List(Of GetUsers) = New List(Of GetUsers)

        Using mdscmd = New SqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()

                Dim usermodel = New GetUsers With {
                    .UPWD = mdsReader.GetValue(1),
                    .UFNAME = mdsReader.GetValue(2),
                    .ULNAME = mdsReader.GetValue(3),
                    .UID = mdsReader.GetValue(0)
                }

                users.Add(usermodel)
            End While

            Return users.ToList()

        End Using

    End Function

End Class
