﻿Imports System.Data.SqlClient
Imports Compass.Global
Imports Compass.MySql.Database

Public Class GetNewOrderNumber
    Implements IDisposable
    Private MyConnection As SqlConnection
    Private mdsReader As SqlDataReader
    Public _sqlText As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenSqlDatabase = New OpenSqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                ' mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub

    Public Function GetOrderNumber() As Int16
        Dim ordernumber As Int16 = 0

        Using mdscmd As New SqlCommand("dbo.xTNCL_NEXTVAL", MyConnection) With {.CommandType = CommandType.StoredProcedure}
            mdscmd.Parameters.Add("SYSVALKEY", SqlDbType.VarChar).Value = "Orders"
            mdscmd.Parameters.Add("SYSVALVAL", SqlDbType.Float).Value = 0
            mdscmd.Parameters("SYSVALVAL").Direction = ParameterDirection.Output

            Try
                mdscmd.ExecuteNonQuery()
                ordernumber = Convert.ToInt16(mdscmd.Parameters("SYSVALVAL").Value.ToString)
            Catch ex As Exception
                Using recorderror As New ErrorLog()
                    recorderror.LogError(ex.Message.ToString, "GetNewOrderNumber.GetOrderNumber", ex.StackTrace.ToString)
                End Using
            End Try

        End Using

        Return ordernumber

    End Function

End Class
