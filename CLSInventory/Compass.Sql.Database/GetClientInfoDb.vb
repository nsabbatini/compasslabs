﻿Imports System.Data.SqlClient
Imports Compass.Global
Public Class GetClientInfoDb
    Implements IDisposable
    Private MyConnection As SqlConnection
    Private mdsReader As SqlDataReader
    Public _sqlText As String
    Public Sub New()
        MyBase.New()
        Using connectionInfo As OpenSqlDatabase = New OpenSqlDatabase()
            MyConnection = connectionInfo.GoSqlSession
        End Using
    End Sub

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                ' mdsReader.Close()
                'mdscmd.Dispose()
            End If
        Catch ex As Exception
            'ignore errorMessage
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        Dispose(False)
    End Sub
    Public Function SetClientInfo() As IEnumerable(Of GetClients)

        Dim clients As List(Of GetClients) = New List(Of GetClients)

        Using mdscmd = New SqlCommand(_sqlText, MyConnection) With {.CommandType = CommandType.Text}

            mdsReader = mdscmd.ExecuteReader()

            While mdsReader.Read()

                Dim clientmodel = New GetClients With {
                    .Acct = mdsReader.GetValue(0),
                    .Aefdt = mdsReader.GetValue(1),
                    .Aact = mdsReader.GetValue(2),
                    .Aname = mdsReader.GetValue(3),
                    .Aname2 = mdsReader.GetValue(4),
                    .Aad1 = mdsReader.GetValue(5),
                    .Aad2 = mdsReader.GetValue(6),
                    .Acity = mdsReader.GetValue(7),
                    .Acnty = mdsReader.GetValue(8),
                    .Astate = mdsReader.GetValue(9),
                    .Acntry = mdsReader.GetValue(10),
                    .Azip = mdsReader.GetValue(11),
                    .Aphone = mdsReader.GetValue(12),
                    .Afax = mdsReader.GetValue(13),
                    .Acontact = mdsReader.GetValue(14),
                    .Aemail = mdsReader.GetValue(15)
                }


                clients.Add(clientmodel)
            End While

            Return clients.ToList()

        End Using

    End Function

End Class
