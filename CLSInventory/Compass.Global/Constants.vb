﻿Imports System.Net.Mail
Imports System.Security.Cryptography
Imports System.Text
Public Module Constants
    ''' <summary>
    ''' 'Used to insert error information
    ''' </summary>
    ''' <returns></returns>
    Public Const ErrorSql As String = "INSERT INTO errorlog(ErrorMsg, WhereOccured, OtherNotes, DateOccured)" &
        "VALUES(@ErrorMsg, @WhereOccured, @Notes, CURRENT_TIMESTAMP)"


    ''' <summary>
    ''' the name of the sql files that are read and passed to the class for execution 
    ''' </summary>
    ''' <returns></returns>
    ''' 
    Public Const VendorSqlFile As String = "VendorsInfo.sql"
    Public Const InventorySqlItems As String = "GetInventoryItems.sql"
    Public Const OrdersSql As String = "GetOrders.sql"
    Public Const OrderItemsSql As String = "GetOrderItems.sql"
    Public Const AuthUserSql As String = "AuthUser.sql"
    Public Const ReceiveInventorySql As String = "UpdateReceivedItems.sql"
    Public Const ReceiveItemsSql As String = "ReceiveInventory.sql"
    Public Const GetReceiveHistorySql As String = "GetReceiveHistory.sql"
    Public Const GetItemsForUpdateQtySql As String = "UpdateCurrentInventory.sql"
    Public Const SaveCurrentQtySql As String = "SaveCurrentQty.sql"
    Public Const SaveVendorsSql As String = "InsertNewVendors.sql"
    Public Const GetOrdersForPrintSql As String = "GetOrderForPrint.sql"
    Public Const GetOrderForReqsSql As String = "GetOrderForReqs.sql"
    Public Const GetOrderForPickingSql As String = "GetOrdersForPicking.sql"
    Public Const GetOrderForShippingSql As String = "GetOrdersForShipping.sql"
    Public Const GetOrdersInPopupWindowSql As String = "GetOrdersForPopup.sql"
    Public Const GetClientInformationSql As String = "GetClientInfo.sql"
    Public Const GetMoleclarSql As String = "GetMolecularItems.sql"
    Public Const GetNonMolecularSql As String = "GetNonMolecularItems.sql"
    Public Const SetItemsCurrentQtySql As String = "SetNewInventoryLevel.sql"
    Public Const InsertPoSql As String = "InsertPurchaseOrder.sql"
    Public Const InsertPoDetailsSql As String = "InsertPurchaseOrderDetails.sql"
    Public Const GetNextPurchaseNumberSql As String = "GetNextPoNumber.sql"
    Public Const GetPurchaseOrdersSql As String = "GetPurchaseOrder.sql"
    Public Const GetPurchaseOrderDetailsSql As String = "GetPurchaseOrderDetails.sql"
    Public Const GetPurchaseOrderItemsSql As String = "GetPurchaseOrderItems.sql"
    Public Const GetReconcilePoSql As String = "ReconcilePurchaseOrderSql.sql"
    Public Const UpdatePurchaseOrderDetails As String = "UpdatePurchaseOrderDetails.sql"
    Public Const UpdatePurchaseOrderMainSql As String = "UpdatePurchaseOrderMain.sql"
    Public Const UpdateItemsFromPoSql As String = "UpdateItemsFromPo.sql"
    Public Const DeleteVendorsSql As String = "DeleteVendor.sql"
    Public Const UpdateVendorsSql As String = "UpdateVendors.sql"
    Public Const InsertNewItemPodSql As String = "InsertNewItemIntoPod.sql"
    Public Const GetPoHistorySql As String = "GetPoHistory.sql"
    Public Const InsertNewItemsSql As String = "InsertNewItems.sql"
    Public Const GetItemsForInvUpdateSql As String = "GetItemsForInvUpdate.sql"
    Public Const DeleteInventoryItemsSql As String = "DeleteInventoryItems.sql"
    Public Const GetDepartmentsSql As String = "GetDepartments.sql"
    Public Const GetReasonsSql As String = "GetReasons.sql"
    Public Const InsertOrderSql As String = "InsertOrder.sql"
    Public Const InsertOrderItemsSql As String = "InsertOrderItems.sql"
    Public Const ApproveOrderedItemsSql As String = "ApproveOrderedItems.sql"
    Public Const GetReasonsCodeSql As String = "GetReasonsCode.sql"
    Public Const SetTrackingInformationSql As String = "SetTrackingInformation.sql"
    Public Const GetTrackingSql As String = "GetTracking.sql"
    Public Const GetTrackingDetailsSql As String = "GetTrackingDetails.sql"
    Public Const SetTrackingInformationDetailsSql As String = "SetTrackingInformationDetails.sql"
    Public Const UpdatePurchaseOrderSql As String = "UpdatePurchaseOrder.sql"
    Public Const UpdateEditedPODetailSql As String = "UpdateEditedPODetail.sql"



    ''' <summary>
    ''' 'connection strings
    ''' </summary>
    ''' <returns></returns>
    ''' 


    'Dev Db
    'Public Const MySqlConn As String = "Server=localhost;Port=3306;Database=compass;Uid=root;Pwd=6KA2wfpdWs1A;AllowBatch=True;allowuservariables=true;"

    'Production Db
    Public Property MySqlConn As String = "Server=192.168.65.100;Port=3306;Database=compass;Uid=nsabbatini;Pwd=Compass1910;AllowBatch=True;allowuservariables=true;"

    'production Sql Db
    Public Const SqlConn As String = "Server=tcp:10.65.1.12;Initial Catalog=TNCL_PROD;User Id=sa;Password=C0mp@SS**;Integrated Security=False;MultipleActiveResultSets=true"

    ''' <summary>
    ''' username and password for email smtpclient
    ''' </summary>
    Public Const SmtpUser As String = "Info@CompassLabServices.com"
    Public Const SmtpPassword As String = "CompassLab2020"

    ''' <summary>
    ''' 'Who is logged in
    ''' </summary>
    ''' <returns></returns>
    ''' 
    Public Property LoggedinUserName As String

    ''' <summary>
    ''' Client info from sql database
    ''' </summary>
    ''' <returns></returns>
    Public Property CustomerAcctNumber As String
    Public Property CustomerAcctType As String
    Public Property CustomerBusinessName As String

    ''' <summary>
    ''' 'PO stuff
    ''' </summary>
    ''' <returns></returns>
    Public Property VendorId As Int16
    Public Property PONumber As String
    Public Property SalesPerson As String = String.Empty
    Public Property QuoteNumber As String = String.Empty
    Public Property Department As String = String.Empty
    Public Property PaymentTerms As String = String.Empty

    Public Function ReadSqlFile(ByVal theFile As String)

        Dim sqltext = My.Computer.FileSystem.ReadAllText(theFile)

        Return sqltext

    End Function
    Public Function SendErrorEmail(ByVal ErrorMsg As String, ByVal WhereOccured As String, Optional ByVal Notes As String = "") As Boolean
        Dim msg As String
        Dim result

        Dim stb As StringBuilder = New StringBuilder


        Using mailMESSAGE As MailMessage = New MailMessage()
            mailMESSAGE.From = New MailAddress("Info@CompassLabServices.com")
            'mailMESSAGE.To.Add(New MailAddress("it_support@COMPASSLABSERVICES.COM"))
            mailMESSAGE.To.Add(New MailAddress("lmclaughlin@CompassLabServices.com"))
            mailMESSAGE.Priority = MailPriority.High

            mailMESSAGE.IsBodyHtml = True
            mailMESSAGE.Subject = "CLIMS Error Logged"

            stb.Append("IT Staff,<br/><br/>")
            stb.Append("The following error has been logged in the database error log table<br/><br/>")
            stb.Append("Error occured on form: " & WhereOccured & "<br/>")
            stb.Append("<hr/>")
            stb.Append("Error Message: " & ErrorMsg & "<br/>")
            stb.Append("<hr/><br/><br/>")
            stb.Append("Notes: " & Notes)

            mailMESSAGE.Body = stb.ToString

            Using smtpCLIENT As SmtpClient = New SmtpClient() With {.Host = "compasslabservices-com.mail.protection.outlook.com", .Port = 25}
                Try
                    smtpCLIENT.Credentials = New System.Net.NetworkCredential(SmtpUser, SmtpPassword, "smtp.office365.com")
                    smtpCLIENT.Send(mailMESSAGE)
                Catch smtpEx As SmtpException
                    msg = smtpEx.ToString
                    'Return False
                Catch ex As Exception
                    msg = ex.ToString
                    'Return False
                Finally

                End Try
            End Using
        End Using

        Return True

    End Function
    Public Function SendPdfForApproval(ByVal strAttachment As String) As Boolean
        Dim msg As String
        Dim stb As StringBuilder = New StringBuilder


        Using mailMESSAGE As MailMessage = New MailMessage()
            mailMESSAGE.From = New MailAddress("Info@CompassLabServices.com")
            mailMESSAGE.To.Add(New MailAddress("kellyelmore@CompassLabServices.com"))
            mailMESSAGE.Priority = MailPriority.High
            mailMESSAGE.Attachments.Add(New Attachment(strAttachment))
            mailMESSAGE.IsBodyHtml = True
            mailMESSAGE.Subject = "Purchase order needs approved"

            stb.Append("Purchasing Agent,<br/><br/>")
            stb.Append("The attached purchase order needs your approval.<br/><br/>")

            mailMESSAGE.Body = stb.ToString

            Using smtpCLIENT As SmtpClient = New SmtpClient() With {.Host = "compasslabservices-com.mail.protection.outlook.com", .Port = 25}
                Try
                    smtpCLIENT.Credentials = New System.Net.NetworkCredential(SmtpUser, SmtpPassword, "smtp.office365.com")
                    smtpCLIENT.Send(mailMESSAGE)
                Catch smtpEx As SmtpException
                    msg = smtpEx.ToString
                    'Return False
                Catch ex As Exception
                    msg = ex.ToString
                    'Return False
                Finally

                End Try
            End Using
        End Using

        Return True

    End Function
    Public Function MD5Hash(ByVal source As String) As String
        Dim provider As System.Security.Cryptography.MD5CryptoServiceProvider = New System.Security.Cryptography.MD5CryptoServiceProvider

        Dim sourceBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(source)
        Dim hashBytes As Byte() = provider.ComputeHash(sourceBytes)

        Dim hexString As System.Text.StringBuilder = New System.Text.StringBuilder(32)
        Dim i As Integer = 0

        Do While i < hashBytes.Length
            hexString.Append(hashBytes(i).ToString("X2"))
            i += 1
        Loop
        Return hexString.ToString()
    End Function
    Public Function GenerateSHA256String(inputString As String) As String
        Dim sha256 As SHA256 = SHA256Managed.Create()
        Dim bytes As Byte() = Encoding.UTF8.GetBytes(inputString)
        Dim hash As Byte() = sha256.ComputeHash(bytes)
        Return GetStringFromHash(hash)
    End Function
    Private Function GetStringFromHash(hash As Byte()) As String
        Dim result As New StringBuilder()
        For i As Integer = 0 To hash.Length - 1
            result.Append(hash(i).ToString("X2"))
        Next
        Return result.ToString()
    End Function

    Public Function Scramble(ByVal Text As String,
Optional ByVal Add5 As Boolean = False,
Optional ByVal ReverseIt As Boolean = False) As String
        Dim i As Integer
        Dim Reversed As String
        Dim Result As String

        Reversed = StrReverse(Trim(Text))

        For i = 1 To Len(Trim(Text))
            If Add5 Then
                Result = Result + StrReverse(Format(Asc(Mid(Text, i, 1)) + 5, "000"))
            Else
                Result = Result + StrReverse(Format(Asc(Mid(Text, i, 1)), "000"))
            End If
        Next i

        If Add5 Then
            Result = Result & "F"
        End If

        If ReverseIt Then
            Text = "X" & StrReverse(Result)
        Else
            Text = "Y" & Result
        End If

        Scramble = Text
        Exit Function

    End Function
    Public Function Unscramble(ByVal Text As String) As String
        Dim Result As String
        Dim i As Integer
        Dim Reversed As String
        Dim Pre As String
        Dim Post As String
        Dim ReverseIt As Boolean
        Dim Add5 As Boolean

        Pre = Left(Text, 1)

        If Pre = "X" Or Pre = "Y" Then
            Text = Mid(Text, 2, Len(Text) - 1)
        End If

        If Pre = "X" Then   'it is reversed
            Post = Left(Text, 1)
            If Post = "F" Then
                Text = Right(Text, Len(Text) - 1)
            Else
                Text = Left(Text, Len(Text))
            End If
            ' Text = Mid(Text, 2, Len(Text) - 1)
            Reversed = StrReverse(Trim(Text))
        Else
            Post = Right(Text, 1)
            If Post = "F" Then
                Text = Left(Text, Len(Text) - 1)
            Else
                Text = Left(Text, Len(Text))
            End If
            Reversed = Trim(Text)
        End If


        For i = 1 To Len(Trim(Reversed)) Step 3
            If Post = "F" Then
                Result = Result + Chr(StrReverse(Mid(Trim(Reversed), i, 3)) - 5)
            Else
                Result = Result + Chr(StrReverse(Mid(Trim(Reversed), i, 3)))
            End If
        Next i

        Unscramble = Result
        Exit Function

    End Function
    Public Enum POStatus As Integer
        Complete = 1
        Pending = 2
        PartialOrder = 3
        Ordered = 4
        Void = 5
    End Enum
End Module
